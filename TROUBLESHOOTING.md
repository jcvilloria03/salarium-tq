# Troubleshooting Guide

In the course of operation, the log files might show errors 

```
[<datetime>: ERROR/MainProcess] <Task Name>[task id] raised unexpected: <Error name here>
```
If the log shows the following errors, here's how to troubleshoot:

## Problem:  PeerError('error details')

The Task Queue connects to different APIs.  A PeerError signifies a problem with the API being accessed by the task.  

If error details is ```Internal Server Error``` then the said API endpoint should be where the troubleshooting is.

If the error details is ```Request took to long...``` the connection of TQ to the API in question might be the cause OR the API endpoint is not up to begin with.


## Problem:  TaskError('error details')

This isn't an issue.  It usually is due to bad inputs (for example: ```Company ID Mismatch```, or ```Employee not found```).  

TQ  will retry the task if the task has ```retry``` setting.

Best just notify ops as this could be a misconfig with the data of the company/employee or what not.

## Problem:  CRITICAL/MainProcess Unrecoverable error: PreconditionFailed...

For example:
```
[2018-12-05 11:04:43,704: CRITICAL/MainProcess] Unrecoverable error: PreconditionFailed(406, "PRECONDITION_FAILED - inequivalent arg 'x-expires' for queue 'dev_attendance_calculation.archived' in vhost '/': received none but current is the value '2073600000' of type 'signedint'", (50, 10), 'Queue.declare')
```

Solution:  This is known to work -- 
* go to rabbitmq admin  UI 
* go to ```Queues```
* Find and delete the queue in the error message: as in the above ```dev_attendance_calculation.archived```
* DO NOT DELETE the main message queue! (which in the case above should be ```dev_attendance_calculation```)

