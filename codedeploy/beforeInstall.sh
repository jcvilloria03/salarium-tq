#!/bin/bash

cd /srv/tq-app
if [ "$DEPLOYMENT_GROUP_NAME" == "tq-app-ft" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-ft-tq-app.env .env
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "tq-app-dev" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-d3-tq-app.env .env
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "tq-app-stg" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-stg-tq-app.env .env
fi
if [ "$DEPLOYMENT_GROUP_NAME" == "tq-app-demo" ]
then
    aws s3 cp s3://v3-stg-codedeploy-env/v3-demo-tq-app.env .env
fi
