#!/bin/bash

# Re-Setup Environment, since the original venv was deleted prior to deployment
#cd /srv/tq-app_0
#mv /srv/tq-app_0/venv /srv/tq-app/venv
#virtualenv venv -p /usr/bin/python3.7
#. venv/bin/activate
#pip install pipenv
#pipenv install
cd /srv/tq-app
virtualenv venv -p /usr/bin/python3.7
. venv/bin/activate
pip install pyyaml python-dotenv iso3166 requests celery celery-message-consumer gevent pipenv

# Remove NPM Installed Files. AWS Codedeploy script required and installtion using NPM which is not necessary in this app. Deleteting these files after deploy
cd /srv/tq-app/
rm -rf package-lock.json node_modules