#!/bin/bash

#Disable NGINX Entry
#rm /etc/nginx/sites-enabled/tq-app.conf
#service nginx reload

#Disable Supervisor Process
supervisorctl stop consumer_app:*
#supervisorctl stop tasks_runner:*

#Clean DocumentRoot
mv /srv/tq-app /srv/tq-app_0
mkdir /srv/tq-app
chown -R slmuser:slmuser /srv/tq-app /srv/tq-app_0
