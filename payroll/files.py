"""Payroll related file processing classes"""
import csv
from core import logger
from payroll import constants, errors

log = logger.get_logger()


class MissingRequiredParametersError(Exception):
    """Exception class for missing parameters"""

    def __init__(self, params):
        super().__init__({"code": "VE-100",
                          "message": "Missing Parameters",
                          "detail": {"parameters": params}
                          })


class MissingHeadersException(Exception):
    """Exception class for missing CSV file headers.

    The list of missing header names should be passed as the first argument.  I.e.
    >>> missing_headers = ['h1', 'h2']
    >>> raise MissingHeadersException(missing_headers)
    """


class InvalidHeadersException(Exception):
    """Exception class for Invalid CSV file headers.

    The list of invalid header names should be passed as the first argument.  I.e.
    >>> invalid_headers = ['h1', 'h2']
    >>> raise InvalidHeadersException(invalid_headers)
    """


class FileProcessor:
    """Superclass for all file processor objects"""

    def __init__(self, context=None, save_result_per_emp=False):
        self._source = None
        self._result = None
        self._errors = {}
        self._status = constants.STATUS_NEW
        self._context = context or {}
        self._save_per_emp = save_result_per_emp

    @property
    def status(self):
        """Returns processing status"""
        return self._status

    @property
    def result(self):
        """Returns processing results if any."""
        if self.status == constants.STATUS_DONE:
            return self._result
        return None

    @property
    def per_employee_result_saving(self):
        """Property to save the result per employee"""
        return self._save_per_emp

    @property
    def errors(self):
        """Returns formatted validation errors dict"""
        return {
            "name": "validation-errors",
            "errors": self._errors
        }

    @property
    def context(self):
        """Returns context object/dict used in file processing."""
        return self._context

    def iterate_source(self, iterable):
        """Call this to start the validation"""
        reader = csv.DictReader(iterable)
        try:
            self._check_header(reader.fieldnames)
        except MissingHeadersException as mhe:
            self._status = constants.STATUS_VALIDATION_ERRORS_FOUND
            self.add_error_list("1", [{
                "code": "VE-110",
                "name": "Missing Headers",
                "details": [{
                    "parameters": mhe.args[0]
                }]
            }])
            return  # stop processing altogether
        except InvalidHeadersException as ihe:
            self._status = constants.STATUS_VALIDATION_ERRORS_FOUND
            self.add_error_list("1", [{
                "code": "VE-111",
                "name": "Invalid Headers",
                "details": [{
                    "parameters": ihe.args[0]
                }]
            }])
            return  # stop processing altogether
        line_index = 2
        valid_lines = 0
        for row in reader:
            if row and any(row.values()):
                valid = self.validate(line_index, row)
                if valid:
                    valid_lines += 1
            line_index += 1

        if not valid_lines:
            self.add_error_list("0", [
                {
                    "code": "VE-203",
                    "name": "Processing Error",
                    "details": [
                        {
                            "description": "No record found in template",
                        }]
                }
            ])
            self._status = constants.STATUS_VALIDATION_ERRORS_FOUND
            return

        if self._errors:  # if there are errors stored during validation
            self._status = constants.STATUS_VALIDATION_ERRORS_FOUND
            return

        self._status = constants.STATUS_VALIDATED
        self.process()

    def add_error_list(self, index, errorList):
        """Add error list for indicated file index/line number"""
        if self._errors.get(index, []):
            self._errors[index].extend(errorList)
        else:
            self._errors[index] = errorList

    def done(self):
        """Returns true if validation/processing is done."""
        return self._status in [constants.STATUS_DONE,
                                constants.STATUS_PROCESSING_ERRORS_FOUND,
                                constants.STATUS_VALIDATION_ERRORS_FOUND]

    def has_errors(self):
        """Returns True if processing has errors"""
        return len(self._errors) > 0

    def validate(self, line_index, data):
        """Validate file line index"""
        return self._validate(line_index, data)

    def process(self):
        """Process"""
        self._status = constants.STATUS_PROCESSING
        self._process()

    # override these in the subclasses
    def _check_header(self, data):
        """Subclasses MUST override this method to define header validation.  Subclass
        implementations must throw MissingHeadersException or InvalidHeadersException as the
        case may be."""

    def _validate(self, line_index, data):
        """Subclasses MUST override this method to define validation of file entry
        in indicated line index"""

    def _process(self):
        # default behavior is to set the status
        self._status = constants.STATUS_DONE


class PayrollItemsFileProcessor(FileProcessor):
    """Superclass of all file upload processing that involves additional payroll amounts.
    """

    def __init__(self, columns, type_column_name,
                 optional_columns=None, context=None, save_result_per_emp=False,
                 context_lookup_key="payrollGroupEmployees"):
        super().__init__(context, save_result_per_emp)
        self.columns = columns
        self._lookup = self.context.get(context_lookup_key, {})
        self._temp_results = {}
        self._type_column_name = type_column_name
        self._optional_columns = set(optional_columns) if optional_columns else set()

    def _check_header(self, data):
        headers = set(data)
        missing = self.columns - headers
        if self._optional_columns:
            missing = missing - self._optional_columns
        # missing
        if missing:
            raise MissingHeadersException(list(missing))

        extra = headers - self.columns
        if self._optional_columns:
            extra = extra - self._optional_columns
        if extra:
            raise InvalidHeadersException(list(extra))

    def _validate(self, line_index, data):
        if not data:
            return False
        d_fname = data.get("First Name")
        d_mname = data.get("Middle Name")
        d_lname = data.get("Last Name")
        meta_errors = []
        # check existence
        missing_keys = [
            key
            for key in data
            if (not data.get(key) and
                key not in self._optional_columns
               )
        ]
        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        # check employee info
        d_empid = data.get("Employee ID")
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname}
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if emp_data:
                uid, fname, mname, lname, _ = emp_data # _ for active status
                if (((d_fname and d_lname) and (d_fname != fname or d_lname != lname)) or
                        (d_mname and d_mname != mname)):
                    meta_errors.append(errors.information_mismatch(d_emp_data))
            else:
                meta_errors.append(errors.id_not_found(d_emp_data))

        amount_value = 0
        if "Amount" in data:
            amount = data.get("Amount")
            try:
                amount_value = float(str(amount).replace(',', ''))
            except ValueError:
                meta_errors.append(errors.invalid_format([{"parameters": ["Amount"],
                                                           "expectedFormat": "number"}]))

        taxable = False
        if "Taxable" in data:
            taxable_str = str(data.get("Taxable", "")).lower().strip()
            if taxable_str not in ('yes', 'no'):
                meta_errors.append(errors.invalid_value([{"parameters": ["Taxable"],
                                                          "expectedValue": "Yes/No"}]))
            else:
                taxable = taxable_str == 'yes'

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False

        self._add_tally(str(uid), data.get(self._type_column_name), amount_value, taxable)
        return True

    def _add_tally(self, uid, name, amount, taxable):
        entry = self._temp_results.get(uid, {})
        category_entry = {}
        index = "taxable"
        if not taxable:
            index = "nontaxable"

        category_entry = entry.get(index, {})
        current_amount = category_entry.get(name, 0)

        category_entry[name] = current_amount + amount

        entry[index] = category_entry
        self._temp_results[uid] = entry

    def _process(self):
        self._result = dict(self._temp_results)
        del self._temp_results
        self._status = constants.STATUS_DONE


class UploadItemsFileProcessor(FileProcessor):
    """Superclass of all file upload processing that involves additional payroll amounts.
    """

    def __init__(self, columns, type_column_name,
                 optional_columns=None, context=None, save_result_per_emp=False,
                 context_lookup_key="payrollGroupEmployees"):
        super().__init__(context, save_result_per_emp)
        self.columns = columns
        self._lookup = self.context.get(context_lookup_key, {})
        self._temp_results = []
        self._type_column_name = type_column_name
        self._optional_columns = set(optional_columns) if optional_columns else set()

    def _check_header(self, data):
        headers = set(data)
        missing = self.columns - headers
        if self._optional_columns:
            missing = missing - self._optional_columns
        # missing
        if missing:
            raise MissingHeadersException(list(missing))


    def _validate(self, line_index, data): # pragma: no cover
        #This needs to be implemented
        return False

    def _add_result(self, item):
        self._temp_results.append(item)

    def _process(self):
        self._result = self._temp_results
        del self._temp_results
        self._status = constants.STATUS_DONE
