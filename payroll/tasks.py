"""Tasks"""
# pylint: disable=unused-import
from payroll._tasks.taskutils import update_job, add_job_error, add_job_result, delete_job_error_items_by_job_id, delete_job_result_items_by_job_id
from payroll._tasks.payroll_tasks import (
    get_payroll_group_employees_min_data,
    get_company_day_hour_rates,
    validate_payroll_related_file,
    iterate_payroll_employees,
    get_attendance_service_data,
    get_employee_payroll_loan_items,
    get_payroll_settings,
    prepare_per_employee_compute,
    prepare_per_employee_special_compute,
    load_attendance_data,
    load_contribution_record,
    load_employee_loan_amortizations,
    compute_pay,
    compute_special_pay,
    compute_final_pay,
    open_payroll,
    save_payroll_item,
    revert_payroll_status,
    create_payroll_register,
    payroll_employee_reopen,
    payroll_employee_close,
    payroll_reopen,
    update_amortizations,
    prepare_per_employee_after_effects,
    report_ok,
    get_company_employees,
    get_company_payroll_groups
)

from payroll._tasks.payslip_tasks import(
    generate_payslips,
    generate_employee_payslip,
    save_payslip_info,
    get_payrollgroup_emails,
    email_employee_payslip,
    create_payslips_activity,
    trigger_download_multiple_payslips,
    clear_payslip_sent_at,
    get_company_logo_data,
)

from payroll._tasks.file_tasks import (
    zip_files,
    trigger_email_file_link,
    save_other_income_items,
    validate_other_income_upload,
    validate_annual_earning_upload,
    save_annual_earning_items
)

#task chainers --  for consumer calls
from payroll._tasks.payroll_tasks import (
    validate_attendance_file,
    validate_payroll_items_file,
    run_payroll,
    run_special_payroll,
    run_final_payroll,
    run_projected_final_pay,
    get_attendance_data,
    close_payroll,
    reopen_payroll,
    email_zipped_payroll_register,
    process_employee_other_income,
    process_employee_annual_earning,
)

from payroll._tasks.payslip_tasks import (
    generate_payroll_payslips,
    email_zipped_payslips,
    send_payslips
)

from payroll._tasks.payroll_loan_tasks import (
    save_loan_item,
    get_company_loan_types,
    validate_loans_upload,
    process_employee_loans,
)

from payroll._tasks.sync_disbursement_tasks import (
    sync_disbursement_workflow,
    get_all_accounts_task,
    sync_payroll_disbursement_task
)

from payroll._tasks.generate_employee_masterfile_tasks import (
    generate_employee_masterfile,
    initialize_job,
    generate_personal_masterlist,
    generate_payroll_masterlist,
    generate_timeattendance_masterlist,
    zip_masterfile
)
