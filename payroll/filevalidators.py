"""Module that contains references to the different file processors.
"""
# pylint: disable=unused-import
import datetime

from payroll import datautil, constants, files, errors
from payroll._validators.payroll_related_validators import (
    AttendanceFileProcessor,
    AllowancesFileUploadValidator,
    BonusesFileUploadValidator,
    CommissionsFileUploadValidator,
    DeductionsFileUploadValidator
)

from payroll._validators.employee_allowances_validator import (
    EmployeeAllowancesFileUploadValidator
)

from payroll._validators.employee_bonuses_validator import (
    EmployeeBonusFileUploadValidator
)

from payroll._validators.employee_commissions_validator import (
    EmployeeCommissionsFileUploadValidator
)

from payroll._validators.employee_deductions_validator import (
    EmployeeDeductionsFileUploadValidator
)

from payroll._validators.employee_adjustments_validator import (
    EmployeeAdjustmentsFileUploadValidator
)

from payroll._validators.employee_loans_validator import (
    EmployeeLoansFileUploadValidator
)

from payroll._validators.employee_annual_earning_validator import (
    EmployeeAnnualEarningFileUploadValidator
)

from payroll._validators.final_payroll_related_validators import (
    FinalAttendanceFileProcessor
)

FILE_PROCESSORS = {
    constants.WORKFLOW_VALIDATE_ATTENDANCE_FILE:
        AttendanceFileProcessor,
    constants.WORKFLOW_VALIDATE_ALLOWANCE_FILE:
        AllowancesFileUploadValidator,
    constants.WORKFLOW_VALIDATE_BONUS_FILE:
        BonusesFileUploadValidator,
    constants.WORKFLOW_VALIDATE_COMMISSION_FILE:
        CommissionsFileUploadValidator,
    constants.WORKFLOW_VALIDATE_DEDUCTION_FILE:
        DeductionsFileUploadValidator,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_ALLOWANCE_FILE:
        EmployeeAllowancesFileUploadValidator,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_DEDUCTION_FILE:
        EmployeeDeductionsFileUploadValidator,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_ADJUSTMENT_FILE:
        EmployeeAdjustmentsFileUploadValidator,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_COMMISSION_FILE:
        EmployeeCommissionsFileUploadValidator,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_LOAN_FILE:
        EmployeeLoansFileUploadValidator,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_BONUS_FILE:
        EmployeeBonusFileUploadValidator,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_ANNUAL_EARNING_FILE:
        EmployeeAnnualEarningFileUploadValidator,
    constants.WORKFLOW_VALIDATE_FINAL_ATTENDANCE_FILE:
        FinalAttendanceFileProcessor
}

def get_processor(task_name, context=None):
    """Returns the file validator object for given task name and context"""
    cls = FILE_PROCESSORS.get(task_name, None)
    return cls(context=context) if cls else None
