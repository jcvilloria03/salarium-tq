"""Employee Deductions"""

import datetime

from payroll import files, errors, apicalls
from core import authz
import json, celeryconfig

class EmployeeDeductionsFileUploadValidator(files.UploadItemsFileProcessor):
    """Employee Deductions Update/Upload"""
    FREQUENCY = ["Every pay of the month", "First pay of the month",
                 "Last pay of the month"]
    FREQUENCY_LOWER = ["every pay of the month", "first pay of the month",
                       "last pay of the month"]

    def __init__(self, context):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Deduction Type",
                    "Amount Deducted",
                    "Recurring?",
                    "Frequency",
                    "Start Date",
                    "Valid To"
                ]),
            type_column_name="Deduction Type",
            optional_columns=["Last Name", "First Name", "Middle Name", "Valid To"],
            context=context,
            save_result_per_emp=False,
            context_lookup_key="companyEmployees"
        )
        self._conditional_columns = []

    def _get_matching_other_income_item(self, name):
        matches = [match
                   for match in self.context.get("typeList")
                   if (str(match.get("name", "")).replace(' ', '_').lower() ==
                       str(name).replace(' ', '_').lower())
                   ]
        if not matches:
            return None
        return matches[0]

    def _validate(self, line_index, data):
        if not data:
            return False
        meta_errors = []
        missing_keys = []
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname
                     }
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if not emp_data:
                meta_errors.append(errors.id_not_found(d_emp_data))
            else:
                uid = emp_data[0]
        else:
            missing_keys.append("Employee ID")

        deduction_type = data.get("Deduction Type")
        deduction_id = None
        if not deduction_type:
            missing_keys.append("Deduction Type")
        else:
            match = self._get_matching_other_income_item(deduction_type)
            if not match:
                meta_errors.append(errors.processing_error(["Deduction Type does not exist"]))
            else:
                deduction_id = match.get("id")

        amount_str = str(data.get("Amount Deducted")).replace(',', '')
        amount = 0
        if not amount_str:
            missing_keys.append("Amount Deducted")
        else:
            try:
                amount = float(amount_str)
                if amount < 1:
                    raise ValueError("Invalid amount")
            except ValueError:
                meta_errors.append(errors.invalid_value([{"parameters": ["Amount Deducted"],
                                                          "expectedFormat": "number",
                                                          "expectedValue": {"min": 1}}]))

        recurring_str = str(data.get("Recurring?", "")).strip().lower()
        recurring = False
        if not recurring_str:
            missing_keys.append("Recurring?")
        elif recurring_str not in ("yes", "no"):
            meta_errors.append(errors.invalid_value([{"parameters": ["Recurring?"],
                                                      "expectedValue": "Yes/No"}]))
        else:
            recurring = recurring_str == "yes"

        frequency = None
        if recurring:
            frequency_str = str(
                data.get("Frequency")).strip().lower()
            if not frequency_str:
                missing_keys.append("Frequency")
            elif frequency_str not in self.FREQUENCY_LOWER:
                meta_errors.append(errors.invalid_value([{"parameters": ["Frequency"],
                                                          "expectedValue": "/".join(self.FREQUENCY)
                                                         }]))
            else:
                frequency = str(data.get("Frequency"))

        start_date_str = str(data.get("Start Date", "")).strip().lower()
        start_date = None
        if not start_date_str:
            missing_keys.append("Start Date")
        else:
            try:
                start_date = datetime.datetime.strptime(
                    start_date_str, "%m/%d/%Y")
            except ValueError:
                meta_errors.append(errors.invalid_format([{"parameters": ["Start Date"],
                                                           "expectedFormat": "MM/DD/YYYY"}]))

        valid_to_str = str(data.get("Valid To", "")).strip().lower()
        valid_to = None
        if valid_to_str:
            try:
                valid_to = datetime.datetime.strptime(valid_to_str, "%m/%d/%Y")
                if start_date and valid_to < start_date:
                    meta_errors.append(errors.processing_error([
                        "The valid to date must be a date same or after valid from date."
                    ]))
            except ValueError:
                meta_errors.append(errors.invalid_format([{"parameters": ["Valid To"],
                                                           "expectedFormat": "MM/DD/YYYY"}]))

        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        # Authz Validation
        if self.context.get("authzDataScope"):
            authz_data_scope = json.loads(self.context.get("authzDataScope"))
            # Fetch employee data
            api_path = celeryconfig.APIS[celeryconfig.API_EMPLOYEE_BASIC_INFO] % uid
            employee_data = apicalls.get_raw_data(api_path)
            if employee_data:
                employee_data = json.loads(employee_data)
                authz_error = "No Access to Employee " + d_empid
                if not authz.is_employee_authorized(authz_data_scope, employee_data):
                    meta_errors.append(
                        errors.data_scope_error([authz_error])
                    )
                # Payroll Group Validation
                try:
                  payroll_group_id = employee_data['payroll_group']['id']
                except (TypeError, KeyError):
                  payroll_group_id = None
                finally:
                  if payroll_group_id == None:
                    meta_errors.append(
                        errors.data_scope_error(["No payroll group is assigned to employee."])
                    )

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False

        self._add_result(
            {
                "type_id": deduction_id,
                "recipients": {"employees": [uid]},
                "amount": amount,
                "recurring": int(recurring),
                "frequency": str(frequency).upper().replace(' ', '_') if frequency else None,
                "valid_from": start_date.strftime("%Y-%m-%d"),
                "valid_to": valid_to.strftime("%Y-%m-%d") if valid_to else None,
            }
        )

        return True
