"""Employee Adjustments File Upload"""
from ast import Try
import datetime

from payroll import files, errors, apicalls
from core import authz
import json, celeryconfig

class EmployeeAdjustmentsFileUploadValidator(files.UploadItemsFileProcessor):
    """Employee Adjustments Update/Upload"""

    def __init__(self, context):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Adjustment Type",
                    "Amount",
                    "Payroll Date",
                    "Include to Special Pay Run?",
                    "Reason",
                ]),
            type_column_name="Adjustment Type",
            optional_columns=["Last Name", "First Name", "Middle Name"],
            context=context,
            save_result_per_emp=False,
            context_lookup_key="companyEmployees"
        )
        #get adjustment types
        self.adjustment_types = [
            str(x.get("name", "")).replace('_', ' ').lower().title()
            for x in self.context.get("typeList", [])
        ]
        self._conditional_columns = []

    def _get_matching_other_income_item(self, name):
        matches = [match
                   for match in self.context.get("typeList")
                   if (str(match.get("name", "")).replace(' ', '_').lower() ==
                       str(name).replace(' ', '_').lower())
                   ]
        if not matches:
            return None
        return matches[0]

    def _validate(self, line_index, data):
        if not data:
            return False
        meta_errors = []
        missing_keys = []
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname}
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if not emp_data:
                meta_errors.append(errors.id_not_found(d_emp_data))
            else:
                uid = emp_data[0]
        else:
            missing_keys.append("Employee ID")

        adjustment_type = data.get("Adjustment Type")
        adjustment_id = None
        if not adjustment_type:
            missing_keys.append("Adjustment Type")
        else:
            match = self._get_matching_other_income_item(adjustment_type)
            if not match:
                meta_errors.append(errors.invalid_value(
                    [{"parameters": ["Adjustment Type"],
                      "expectedValue": "/".join(self.adjustment_types)}]
                ))
            else:
                adjustment_id = match.get("id")

        amount_str = str(data.get("Amount")).replace(',', '')
        amount = 0
        if not amount_str or amount_str == "0":
            missing_keys.append("Amount")
        else:
            try:
                amount = float(amount_str)
            except ValueError:
                meta_errors.append(
                    errors.invalid_format(
                        [{"parameters": ["Amount"], "expectedFormat": "number"}]
                    )
                )

        special_pay_run_str = str(
            data.get("Include to Special Pay Run?", "")).strip().lower()
        special_pay_run = False
        if not special_pay_run_str:
            missing_keys.append("Include to Special Pay Run?")
        elif special_pay_run_str not in ("yes", "no"):
            meta_errors.append(
                errors.invalid_value(
                    [{"parameters": ["Include to Special Pay Run?"],
                      "expectedValue": "Yes/No"}]
                )
            )
        else:
            special_pay_run = special_pay_run_str == "yes"

        payroll_date_str = str(data.get("Payroll Date", "")).strip().lower()
        payroll_date = None
        if not payroll_date_str:
            missing_keys.append("Payroll Date")
        else:
            try:
                payroll_date = datetime.datetime.strptime(
                    payroll_date_str, "%m/%d/%Y")
            except ValueError:
                meta_errors.append(
                    errors.invalid_format([{"parameters": ["Payroll Date"],
                                            "expectedFormat": "MM/DD/YYYY"}])
                )

        reason = str(data.get("Reason", "")).strip()
        if not reason:
            missing_keys.append("Reason")

        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        # Authz Validation
        if self.context.get("authzDataScope"):
            authz_data_scope = json.loads(self.context.get("authzDataScope"))
            # Fetch employee data
            api_path = celeryconfig.APIS[celeryconfig.API_EMPLOYEE_BASIC_INFO] % uid
            employee_data = apicalls.get_raw_data(api_path)
            if employee_data:
                employee_data = json.loads(employee_data)
                authz_error = "No Access to Employee " + d_empid
                if not authz.is_employee_authorized(authz_data_scope, employee_data):
                    meta_errors.append(
                        errors.data_scope_error([authz_error])
                    )
                # Payroll Group Validation
                payroll_group_id = None
                try:
                  payroll_group_id = employee_data['payroll_group']['id']
                except (TypeError, KeyError):
                  payroll_group_id = None
                finally:
                  if payroll_group_id == None:
                    meta_errors.append(
                        errors.data_scope_error(["No payroll group is assigned to employee."])
                    )

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False

        self._add_result(
            {
                "type_id": adjustment_id,
                "amount": amount,
                "reason": reason,
                "recipients": {"employees": [uid]},
                "release_details": [{
                    "date": payroll_date.strftime("%Y-%m-%d"),
                    "disburse_through_special_pay_run": special_pay_run,
                    "status": False
                }]
            }
        )

        return True
