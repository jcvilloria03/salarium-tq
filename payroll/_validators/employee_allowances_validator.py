"""Employee Allowances File Upload"""
import datetime

from payroll import files, errors, apicalls
from core import authz
import json, celeryconfig

class EmployeeAllowancesFileUploadValidator(files.UploadItemsFileProcessor):
    """Employee Allowances Update/Upload"""
    CREDITING_FREQ = ["Daily", "Per Pay", "First Pay of the Month",
                      "Last Pay of the Month", "Per Month"]
    CREDITING_FREQ_LOWER = ["daily", "per pay", "first pay of the month",
                            "last pay of the month", "per month"]

    def __init__(self, context):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Allowance Type",
                    "Amount",
                    "Special Pay Run?",
                    "Recurring?",
                    "Crediting Frequency",
                    "Prorated?",
                    "Prorated By?",
                    "Prorate Entitlement On Paid Leave?",
                    "Prorate Entitlement On Unpaid Leave?",
                    "Never Expires?",
                    "Valid From?",
                    "Valid Until?"
                ]),
            type_column_name="Allowance Type",
            optional_columns=["Last Name", "First Name", "Middle Name"],
            context=context,
            save_result_per_emp=False,
            context_lookup_key="companyEmployees"
        )
        self._conditional_columns = []

    def _get_matching_other_income_item(self, name):
        matches = [match
                   for match in self.context.get("typeList")
                   if (str(match.get("name", "")).replace(' ', '_').lower() ==
                       str(name).replace(' ', '_').lower())
                   ]
        if not matches:
            return None
        return matches[0]

    def _validate(self, line_index, data):
        if not data:
            return False
        meta_errors = []
        missing_keys = []
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname}
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if not emp_data:
                meta_errors.append(errors.id_not_found(d_emp_data))
            else:
                uid = emp_data[0]
        else:
            missing_keys.append("Employee ID")

        allowance_type = data.get("Allowance Type")
        allowance_id = None
        if not allowance_type:
            missing_keys.append("Allowance Type")
        else:
            match = self._get_matching_other_income_item(allowance_type)
            if not match:
                meta_errors.append(errors.processing_error(["Allowance Type does not exist"]))
            else:
                allowance_id = match.get("id")

        amount_str = str(data.get("Amount")).replace(',', '')
        amount = 0
        if not amount_str or amount_str == "0":
            missing_keys.append("Amount")
        else:
            try:
                amount = float(amount_str)
            except ValueError:
                meta_errors.append(
                    errors.invalid_format(
                        [{"parameters": ["Amount"], "expectedFormat": "number"}]
                    )
                )

        special_pay_run_str = str(
            data.get("Special Pay Run?", "")).strip().lower()
        special_pay_run = False
        if not special_pay_run_str:
            missing_keys.append("Special Pay Run?")
        elif special_pay_run_str not in ("yes", "no"):
            meta_errors.append(
                errors.invalid_value(
                    [{"parameters": ["Special Pay Run?"],
                      "expectedValue": "Yes/No"}]
                )
            )
        else:
            special_pay_run = special_pay_run_str == "yes"

        recurring_str = str(data.get("Recurring?", "")).strip().lower()
        recurring = False
        if not recurring_str:
            missing_keys.append("Recurring?")
        elif recurring_str not in ("yes", "no"):
            meta_errors.append(
                errors.invalid_value([{"parameters": ["Recurring?"], "expectedValue": "Yes/No"}])
            )
        else:
            recurring = recurring_str == "yes"

        crediting_frequency = None
        if recurring:
            crediting_frequency_str = str(
                data.get("Crediting Frequency")).strip().lower()
            if not crediting_frequency_str:
                missing_keys.append('Crediting Frequency')
            elif crediting_frequency_str not in self.CREDITING_FREQ_LOWER:
                meta_errors.append(
                    errors.invalid_value(
                        [{"parameters": ["Crediting Frequency"],
                          "expectedValue": "/".join(self.CREDITING_FREQ)}]
                    )
                )
            else:
                crediting_frequency = str(data.get("Crediting Frequency"))
        prorated = False
        if recurring:
            prorated_str = str(data.get("Prorated?", "")).strip().lower()
            if not prorated_str:
                missing_keys.append("Prorated?")
            elif prorated_str not in ("yes", "no"):
                meta_errors.append(
                    errors.invalid_value([{"parameters": ["Prorated?"], "expectedValue": "Yes/No"}])
                )
            else:
                prorated = prorated_str == "yes"

        prorated_by = None
        prorate_entitlement_on_paid_leave = False
        prorate_entitlement_on_unpaid_leave = False
        if recurring and prorated:
            prorated_by_str = str(data.get("Prorated By?", "")).strip().lower()
            if not prorated_by_str:
                missing_keys.append("Prorated By?")
            elif prorated_by_str not in ("prorated by hour", "prorated by day"):
                meta_errors.append(
                    errors.invalid_value([{"parameters": ["Prorated?"],
                                           "expectedValue": "Prorated by Hour/Prorated by Day"}])
                )
            else:
                prorated_by = data.get("Prorated By?")

            prorate_entitlement_paid_str = str(
                data.get("Prorate Entitlement On Paid Leave?", "")).strip().lower()
            if not prorate_entitlement_paid_str:
                missing_keys.append("Prorate Entitlement On Paid Leave?")
            elif prorate_entitlement_paid_str not in ("yes", "no"):
                meta_errors.append(
                    errors.invalid_value([{"parameters": ["Prorate Entitlement On Paid Leave?"],
                                           "expectedValue": "Yes/No"}])
                )
            else:
                prorate_entitlement_on_paid_leave = prorate_entitlement_paid_str == "yes"

            prorate_entitlement_unpaid_str = str(
                data.get("Prorate Entitlement On Unpaid Leave?", "")).strip().lower()
            if not prorate_entitlement_unpaid_str:
                missing_keys.append("Prorate Entitlement On Unpaid Leave?")
            elif prorate_entitlement_unpaid_str not in ("yes", "no"):
                meta_errors.append(
                    errors.invalid_value([{"parameters": ["Prorate Entitlement On Unpaid Leave?"],
                                           "expectedValue": "Yes/No"}])
                )
            else:
                prorate_entitlement_on_unpaid_leave = prorate_entitlement_unpaid_str == "yes"

        never_expires = True
        if recurring:
            never_expires_str = str(
                data.get("Never Expires?", "")).strip().lower()
            if not never_expires_str:
                missing_keys.append("Never Expires?")
            elif never_expires_str not in ("yes", "no"):
                meta_errors.append(
                    errors.invalid_value([{"parameters": ["Never Expires?"],
                                           "expectedValue": "Yes/No"}])
                )
            else:
                never_expires = never_expires_str == "yes"

        valid_from_str = str(data.get("Valid From?", "")).strip().lower()
        valid_from = None
        if not valid_from_str:
            missing_keys.append("Valid From?")
        else:
            try:
                valid_from = datetime.datetime.strptime(
                    valid_from_str, "%m/%d/%Y")
            except ValueError:
                meta_errors.append(
                    errors.invalid_format([{"parameters": ["Valid From?"],
                                            "expectedFormat": "MM/DD/YYYY"}])
                )

        valid_to_str = str(data.get("Valid Until?", "")).strip().lower()
        valid_to = None
        if not never_expires:
            if not valid_to_str:
                missing_keys.append("Valid Until?")
            else:
                try:
                    valid_to = datetime.datetime.strptime(valid_to_str, "%m/%d/%Y")
                except ValueError:
                    meta_errors.append(
                        errors.invalid_format([{"parameters": ["Valid Until?"],
                                                "expectedFormat": "MM/DD/YYYY"}])
                    )

        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        # Authz Validation
        if self.context.get("authzDataScope"):
            authz_data_scope = json.loads(self.context.get("authzDataScope"))
            # Fetch employee data
            api_path = celeryconfig.APIS[celeryconfig.API_EMPLOYEE_BASIC_INFO] % uid
            employee_data = apicalls.get_raw_data(api_path)
            if employee_data:
                employee_data = json.loads(employee_data)
                authz_error = "No Access to Employee " + d_empid
                if not authz.is_employee_authorized(authz_data_scope, employee_data):
                    meta_errors.append(
                        errors.data_scope_error([authz_error])
                    )
                # Payroll Group Validation
                payroll_group_id = None
                try:
                  payroll_group_id = employee_data['payroll_group']['id']
                except (TypeError, KeyError):
                  payroll_group_id = None
                finally:
                  if payroll_group_id == None:
                    meta_errors.append(
                        errors.data_scope_error(["No payroll group is assigned to employee."])
                    )

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False
        else:
            entitled_when = None
            if prorate_entitlement_on_paid_leave and prorate_entitlement_on_unpaid_leave:
                entitled_when = "ON_PAID_AND_UNPAID_LEAVE"
            elif prorate_entitlement_on_paid_leave:
                entitled_when = "ON_PAID_LEAVE"
            elif prorate_entitlement_on_unpaid_leave:
                entitled_when = "ON_UNPAID_LEAVE"

            self._add_result({
                "type_id": allowance_id,
                "recipients": {"employees": [uid]},
                "amount": amount,
                "recurring": int(recurring),
                "credit_frequency":
                    crediting_frequency.upper().replace(' ', '_') if crediting_frequency else None,
                "prorated": int(prorated),
                "prorated_by": prorated_by.upper().replace(' ', '_') if prorated_by else None,
                "entitled_when": entitled_when,
                "valid_from": valid_from.strftime("%Y-%m-%d"),
                "valid_to": valid_to.strftime("%Y-%m-%d") if valid_to else None,
                "release_details": [
                    {
                        "disburse_through_special_pay_run": special_pay_run
                    }
                ]
            })

        return True
