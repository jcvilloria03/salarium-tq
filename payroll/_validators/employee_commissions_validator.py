"""Commission validator"""

import datetime

from payroll import errors, files, apicalls
from core import authz
import json, celeryconfig


class EmployeeCommissionsFileUploadValidator(files.UploadItemsFileProcessor):
    """Employee Commission Update/Upload"""
    CREDITING_FREQ = ["Daily", "Per Pay", "First Pay of the Month",
                      "Last Pay of the Month", "Per Month"]
    CREDITING_FREQ_LOWER = ["daily", "per pay", "first pay of the month",
                            "last pay of the month", "per month"]

    def __init__(self, context):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Commission Type",
                    "Amount",
                    "Release Date",
                    "Special Pay Run?",
                ]),
            type_column_name="Commission Type",
            optional_columns=["Last Name", "First Name", "Middle Name"],
            context=context,
            save_result_per_emp=False,
            context_lookup_key="companyEmployees"
        )
        self._conditional_columns = []

    def _get_matching_other_income_item(self, name):
        matches = [match
                   for match in self.context.get("typeList")
                   if (str(match.get("name", "")).replace(' ', '_').lower() ==
                       str(name).replace(' ', '_').lower())
                   ]
        if not matches:
            return None
        return matches[0]

    def _validate(self, line_index, data):
        if not data:
            return False
        meta_errors = []
        missing_keys = []
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname
                     }
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if not emp_data:
                meta_errors.append(errors.id_not_found(d_emp_data))
            else:
                uid = emp_data[0]
        else:
            missing_keys.append("Employee ID")

        commission_type = data.get("Commission Type")
        commission_id = None
        if not commission_type:
            missing_keys.append("Commission Type")
        else:
            match = self._get_matching_other_income_item(commission_type)
            if not match:
                meta_errors.append(errors.processing_error(["Commission Type does not exist"]))
            else:
                commission_id = match.get("id")

        amount_str = str(data.get("Amount")).replace(",", "").strip()
        amount = 0
        if not amount_str:
            missing_keys.append("Amount")
        else:
            try:
                amount = float(amount_str)
                if amount < 1:
                    raise ValueError("Invalid value")
            except ValueError:
                meta_errors.append(
                    errors.invalid_value(
                        [{"parameters": ["Amount"],
                          "expectedFormat": "number",
                          "expectedValue": {"min": 1}}]
                    )
                )

        release_date_str = str(data.get("Release Date")).strip().lower()
        release_date = None
        if not release_date_str:
            missing_keys.append("Release Date")
        else:
            try:
                release_date = datetime.datetime.strptime(
                    release_date_str, "%m/%d/%Y"
                )
            except ValueError:
                meta_errors.append(
                    errors.invalid_format([
                        {"parameters": ["Release Date"], "expectedFormat": "MM/DD/YYYY"}
                    ])
                )

        special_pay_run_str = str(
            data.get("Special Pay Run?", "")).strip().lower()
        special_pay_run = False
        if not special_pay_run_str:
            missing_keys.append("Special Pay Run?")
        elif special_pay_run_str not in ("yes", "no"):
            meta_errors.append(
                errors.invalid_value(
                    [{"parameters": ["Special Pay Run?"],
                      "expectedValue": "Yes/No"}]
                )
            )
        else:
            special_pay_run = special_pay_run_str == "yes"

        #TODO: validation
        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        # Authz Validation
        if self.context.get("authzDataScope"):
            authz_data_scope = json.loads(self.context.get("authzDataScope"))
            # Fetch employee data
            api_path = celeryconfig.APIS[celeryconfig.API_EMPLOYEE_BASIC_INFO] % uid
            employee_data = apicalls.get_raw_data(api_path)
            if employee_data:
                employee_data = json.loads(employee_data)
                authz_error = "No Access to Employee " + d_empid
                if not authz.is_employee_authorized(authz_data_scope, employee_data):
                    meta_errors.append(
                        errors.data_scope_error([authz_error])
                    )

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False

        self._add_result(
            {
                "type_id": commission_id,
                "amount": amount,
                "percentage": 0,
                "recipients": {
                    "employees": [
                        uid
                    ]
                },
                "release_details": [
                    {
                        "date": release_date.strftime("%Y-%m-%d"),
                        "disburse_through_special_pay_run": special_pay_run,
                        "status": 0
                    }
                ]
            }
        )

        return True
