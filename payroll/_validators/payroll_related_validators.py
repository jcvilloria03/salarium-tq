"""Payroll Compute related file validators"""

import datetime

from core import logger
from payroll import files, errors, constants, datautil

logger = logger.get_logger()

class AttendanceFileProcessor(files.FileProcessor):
    """File processor for attendance file uploads"""

    NONTIME_REQUIRED_HEADERS = [
        "Employee ID",
        "Last Name",
        "First Name",
        "Middle Name",
        "Timesheet Date",
    ]
    TIME_REQUIRED_HEADERS = ["Scheduled Hours", "Regular"]
    ALL_HEADERS = NONTIME_REQUIRED_HEADERS + TIME_REQUIRED_HEADERS

    def __init__(self, context=None):
        super(AttendanceFileProcessor, self).__init__(context, True)
        self._temp_results = {}
        self._lookup = self.context.get("payrollGroupEmployees", {})
        self._lookup2 = {}
        for empid, data in self._lookup.items():
            uid, fname, mname, lname, active = data
            # build reverse lookup table
            full_name = str(fname)
            if mname:
                full_name += " " + str(mname)
            if lname:
                full_name += " " + str(lname)
            self._lookup2[str(uid)] = (empid, full_name, active)

        self._detected_employees = set()
        self._hashes = set()
        self._all_days = set()
        self._inactive_employees = dict()
        start_date = self._context.get("startDate")
        end_date = self._context.get("endDate")
        if not start_date or not end_date:
            raise ValueError('startDate and/or endDate is/are not defined')

        self.sdate = datetime.datetime.strptime(start_date, "%Y-%m-%d")
        self.edate = datetime.datetime.strptime(end_date, "%Y-%m-%d")

        delta = self.edate - self.sdate
        for i in range(delta.days + 1):
            self._all_days.add(
                (self.sdate + datetime.timedelta(days=i)).strftime("%Y-%m-%d"))

    def _check_header(self, data):
        missing = set(self.ALL_HEADERS) - set(data)
        if missing:
            raise files.MissingHeadersException(list(missing))

        # get context
        day_hour_types = set(self.context.get("dayHourTypes", []))
        if not day_hour_types:
            return  # bypass checking

        day_hour_types.add('undertime')  # builtin types
        day_hour_types.add('uh')
        day_hour_types.add('tardy')
        day_hour_types.add('overbreak')  # end builtin types
        err_headers = [
            x
            for x in data
            if x not in self.ALL_HEADERS and str(x).lower() not in day_hour_types
        ]
        if err_headers:
            raise files.InvalidHeadersException(err_headers)

    def _validate(self, line_index, data):
        missing = []
        invalid_date_format = []
        invalid_date_value = []
        invalid_hour_format = []
        invalid_hour_value = []

        uid, fname, mname, lname, active = None, None, None, None, False
        start_date, end_date = None, None
        sql_date = None
        meta_errors = []
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or "",
                      "Last Name": d_lname}

        if not d_empid:
            missing.append("Employee ID")
        else:
            emp_data = self._lookup.get(d_empid)
            logger.debug(emp_data)
            if emp_data:
                uid, fname, mname, lname, active = emp_data
                logger.debug(emp_data)
                if active:
                    self._detected_employees.add(str(d_empid))
                if not d_fname:
                    missing.append("First Name")
                if not d_lname:
                    missing.append("Last Name")
                if (((d_fname and d_lname) and (d_fname != fname or d_lname != lname)) or
                        (d_mname and d_mname != mname)):
                    meta_errors.append(errors.information_mismatch(d_emp_data))
                if not active:
                    self._inactive_employees[d_empid] = d_emp_data
                    return True  # do not continue since this is an inactive record already

            else:
                meta_errors.append(errors.id_not_found(d_emp_data))

        d_date = data.get("Timesheet Date")
        if not d_date:
            missing.append("Timesheet Date")
        else:
            tdate = None
            try:
                tdate = datetime.datetime.strptime(d_date, "%m/%d/%Y")
                sql_date = tdate.strftime("%Y-%m-%d")
            except ValueError:
                invalid_date_format.append("Timesheet Date")

            if tdate is not None:
                tdate = tdate.date()
            lhash = "{} - {}".format(str(d_empid), str(tdate))
            if lhash in self._hashes:
                details = dict(d_emp_data)
                details["Timesheet Date"] = d_date
                meta_errors.append(errors.duplicate_data(details))
            else:
                self._hashes.add(lhash)

            # check date range
            sdate = self.sdate.date()
            edate = self.edate.date()

            if tdate is not None and not sdate <= tdate <= edate:
                invalid_date_value.append("Timesheet Date")

        if not data.get("Scheduled Hours"):
            missing.append("Scheduled Hours")

        # check hours
        time_keys = set(data.keys()) - set(self.NONTIME_REQUIRED_HEADERS)

        for time_key in time_keys:
            # check hour format
            duration = str(data.get(time_key, "00:00")).strip()

            if (duration in constants.ZERO_DURATION_VALUES and
                    time_key != "Scheduled Hours"):
                continue

            if constants.DURATION_FORMAT.match(duration):
                hour, minute = str(duration).split(":")
                hour = int(hour)
                minute = int(minute)
                if (
                        not (0 <= hour <= 24)
                        or not (0 <= minute <= 59)
                        or (hour == 24 and minute > 0)
                ):
                    invalid_hour_value.append(time_key)
                transform_key = str(time_key).upper().replace(' ', '_')

                # if transform_key == "TARDY": --tardy will now be on its own
                #     transform_key = "UNDERTIME"
                self._add_time_tally(str(uid), sql_date,
                                     transform_key, hour, minute)
            else:
                invalid_hour_format.append(time_key)

        if (data.get("Scheduled Hours") in constants.ZERO_DURATION_VALUES) and uid and sql_date:
            self._add_time_tally(
                str(uid),
                sql_date,
                "SCHEDULED_HOURS",
                0,
                0
            )

        #check absent
        self._detect_absent(str(uid), sql_date)

        if missing:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing}]))

        invalid_formats = []
        if invalid_date_format:
            invalid_formats.append(
                {"parameters": invalid_date_format, "expectedFormat": "MM/DD/YYYY"}
            )

        if invalid_hour_format:
            invalid_formats.append({"parameters": invalid_hour_format, "expectedFormat": "HH:MM"})

        if invalid_formats:
            meta_errors.append(errors.invalid_format(invalid_formats))

        invalid_values = []
        if invalid_date_value:
            invalid_values.append({"parameters": invalid_date_value,
                                   "expectedValue": {"min": start_date, "max": end_date},})
        if invalid_hour_value:
            invalid_values.append(
                {"parameters": invalid_hour_value,
                 "expectedValue": {"min": "00:00", "max": "24:00"},
                 "expectedFormat": "HH:MM"}
            )

        if invalid_values:
            meta_errors.append(errors.invalid_value(invalid_values))

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False
        return True

    def _add_time_tally(self, emp_id, date, key, hour, minute):
        emp = self._temp_results.get(emp_id, {})
        date_item = emp.get(date, {})
        att_code_value = date_item.get(key, 0)
        add_value = hour + (float(minute) / 60.0)
        date_item[key] = float(att_code_value) + add_value
        emp[date] = date_item
        self._temp_results[emp_id] = emp

    def _detect_absent(self, emp_id, date):
        emp = self._temp_results.get(emp_id, {})
        date_item = emp.get(date, {})
        if len(date_item) == 1 and \
            "SCHEDULED_HOURS" in date_item and \
            date_item.get("SCHEDULED_HOURS", 0) > 0:
            #absent condition
            date_item["ABSENT"] = date_item.get("SCHEDULED_HOURS")
            emp[date] = date_item
            self._temp_results[emp_id] = emp


    def _process(self):
        # detect inactive employees
        if self._inactive_employees:
            self.add_error_list(
                "inactive_employees",
                [errors.processing_error(list(self._inactive_employees.values()))]
            )
        # detect missing employees
        pg_employees = set(self._lookup.keys()) - \
            set(self._inactive_employees.keys())
        missing_employees = pg_employees - self._detected_employees
        if missing_employees:
            missing_data = []
            emp_id_list = sorted(list(missing_employees))
            for emp_id in emp_id_list:
                lookup = self._lookup.get(str(emp_id))
                if lookup:
                    full_name = (str(lookup[1]) +
                                 ((" " + str(lookup[2])) if lookup[2] else "") +
                                 ((" " + str(lookup[3])) if lookup[3] else "")
                                 )
                    missing_data.append({
                        "employeeId": lookup[0],
                        "fullName": full_name
                    })
            self.add_error_list(
                "missing_employees",
                [errors.processing_error(
                    [{"description": "Missing Payroll Group Employees", "details": missing_data}]
                )]
            )

        if self._temp_results:
            self._result = dict(self._temp_results)
        del self._temp_results
        self._status = constants.STATUS_DONE


# Payroll Items
class AllowancesFileUploadValidator(files.PayrollItemsFileProcessor):
    """Validator/Processor class for Allowances file"""
    TAXABLE_STR_OPTIONS_LOWER = ['yes', 'no',
                                 'reduce to taxable income', 'add as non taxable income']
    TAXABLE_STR_OPTIONS = ["Yes", "No", "Reduce to Taxable Income", "Add as Non Taxable Income"]

    def __init__(self, context=None):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Allowance Type",
                    "Amount",
                    "Taxable",
                ]
            ),
            type_column_name="Allowance Type",
            optional_columns=["Middle Name"],
            context=context,
            save_result_per_emp=False,
        )

    def _validate(self, line_index, data):
        """Override _validate to handle non yes/no Taxable"""
        if not data:
            return False

        meta_errors = []
        # check existence
        missing_keys = [
            key
            for key in data
            if (not data.get(key) and
                key not in self._optional_columns
               )
        ]
        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        # check employee info
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname}
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if emp_data:
                uid, fname, mname, lname, _ = emp_data
                if (((d_fname and d_lname) and (d_fname != fname or d_lname != lname)) or
                        (d_mname and d_mname != mname)):
                    meta_errors.append(errors.information_mismatch(d_emp_data))
            else:
                meta_errors.append(errors.id_not_found(d_emp_data))

        amount_value = 0
        if "Amount" in data:
            amount = data.get("Amount")
            try:
                amount_value = float(str(amount).replace(',', ''))
            except ValueError:
                meta_errors.append(
                    errors.invalid_format(
                        [{"parameters": ["Amount"], "expectedFormat": "number"}]
                    )
                )

        taxable_index = "taxable"
        if "Taxable" in data:
            taxable_str = str(data.get("Taxable", "")).lower().strip()
            if taxable_str not in self.TAXABLE_STR_OPTIONS_LOWER:
                meta_errors.append(
                    errors.invalid_value(
                        [{"parameters": ["Taxable"],
                          "expectedValue": "/".join(self.TAXABLE_STR_OPTIONS)}]
                    )
                )
            else:
                if taxable_str == "yes":
                    taxable_index = "taxable"
                elif taxable_str == "no":
                    taxable_index = "nontaxable"
                elif taxable_str == "reduce to taxable income":
                    taxable_index = "reduceToTaxableIncome"
                elif taxable_str == "add as non taxable income":
                    taxable_index = "addAsNontaxableIncome"

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False

        self._add_tally(str(uid), data.get(self._type_column_name), amount_value, taxable_index)
        return True

    def _add_tally(self, uid, name, amount, index): #pylint: disable=arguments-differ
        entry = self._temp_results.get(uid, {})
        category_entry = {}

        category_entry = entry.get(index, {})
        current_amount = category_entry.get(name, 0)

        category_entry[name] = current_amount + amount

        entry[index] = category_entry
        self._temp_results[uid] = entry


class BonusesFileUploadValidator(files.PayrollItemsFileProcessor):
    """Validator/Processor class for Bonus file"""

    def __init__(self, context=None):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Bonus Type",
                    "Amount",
                    "Taxable",
                ]
            ),
            type_column_name="Bonus Type",
            optional_columns=["Middle Name"],
            context=context,
            save_result_per_emp=False,
        )


class CommissionsFileUploadValidator(files.PayrollItemsFileProcessor):
    """Validator/Processor class for commissions file"""

    def __init__(self, context=None):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Commission Type",
                    "Amount",
                    "Taxable",
                ]
            ),
            type_column_name="Commission Type",
            optional_columns=["Middle Name"],
            context=context,
            save_result_per_emp=False,
        )


class DeductionsFileUploadValidator(files.PayrollItemsFileProcessor):
    """Validator/Processor file for Deductions"""

    def __init__(self, context=None):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Deduction Type",
                    "Amount",
                ]
            ),
            type_column_name="Deduction Type",
            optional_columns=["Middle Name"],
            context=context,
            save_result_per_emp=False,
        )
