"""Employee Bonus upload validator"""
import datetime
from payroll import files, errors, apicalls
from core import authz
import json, celeryconfig

class EmployeeBonusFileUploadValidator(files.UploadItemsFileProcessor):
    """Employee Bonus Update/Upload"""
    AMOUNT_BASIS = ["Current Basic Salary", "Average of Basic Salary", "Gross Salary", "Basic Pay"]
    AMOUNT_BASIS_LOWER = ["current basic salary", "average of basic salary", "gross salary", "basic pay"]

    STATUS = ["Disbursed", "Not yet released"]
    STATUS_LOWER = ["disbursed", "not yet released"]

    BONUS_FREQUENCY_ONE_TIME = 'ONE_TIME'
    BONUS_FREQUENCY_PERIODIC = 'PERIODIC'

    BONUS_BASIS_FIXED = 'FIXED'
    BONUS_BASIS_SALARY_BASED = 'SALARY_BASED'

    def __init__(self, context):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Bonus Type",
                    "Amount",
                    "Amount Basis",
                    "Percentage of Monthly Salary",
                    "Release Number",
                    "Release Date",
                    "Disburse through Special Pay Run?",
                    "Coverage From",
                    "Coverage To",
                    "Status"
                ]),
            type_column_name="Bonus Type",
            optional_columns=["Last Name", "First Name", "Middle Name"],
            context=context,
            save_result_per_emp=False,
            context_lookup_key="companyEmployees"
        )
        self._conditional_columns = []
        self._periodic_entries_hash = dict()

    def _get_matching_other_income_item(self, name):
        matches = [match
                   for match in self.context.get("typeList")
                   if (str(match.get("name", "")).replace(' ', '_').lower() ==
                       str(name).replace(' ', '_').lower())
                   ]
        if not matches:
            return None
        return matches[0]

    def _store_repeated_periodic_entry(self, uid, bonus_id, number):
        #check first
        key = "{uid}-{bid}".format(uid=uid, bid=bonus_id)

        entry = self._periodic_entries_hash.get(key, set()) # type: set
        if number in entry:
            return False
        else:
            entry.add(number)
            self._periodic_entries_hash[key] = entry
            return True

    def _is_multiple_releases(self, uid, bonus_id):
        key = "{uid}-{bid}".format(uid=uid, bid=bonus_id)
        return key in self._periodic_entries_hash

    def _validate(self, line_index, data):
        if not data:
            return False
        meta_errors = []
        missing_keys = []
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname
                     }
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if not emp_data:
                meta_errors.append(errors.id_not_found(d_emp_data))
            else:
                uid = emp_data[0]
        else:
            missing_keys.append("Employee ID")

        bonus_type = data.get("Bonus Type").strip()
        bonus_id = None
        bonus_frequency = None
        bonus_basis = None
        if not bonus_type:
            missing_keys.append("Bonus Type")
        else:
            match = self._get_matching_other_income_item(bonus_type)
            if not match:
                meta_errors.append(errors.processing_error(["Bonus Type does not exist"]))
            else:
                bonus_id = match.get("id")
                bonus_frequency = match.get("frequency")
                bonus_basis = match.get("basis")

        amount_str = str(data.get("Amount")).replace(",", "").strip()
        amount = 0
        if bonus_basis == self.BONUS_BASIS_FIXED:
            if not amount_str:
                missing_keys.append("Amount")
            else:
                try:
                    amount = float(amount_str)
                    if amount < 1:
                        raise ValueError('Invalid value')
                except ValueError:
                    meta_errors.append(
                        errors.invalid_value(
                            [{"parameters": ["Amount"],
                            "expectedFormat": "number",
                            "expectedValue": {"min": 1}}]
                        )
                    )

        amount_basis = str(data.get("Amount Basis", "")).strip()
        if bonus_basis == self.BONUS_BASIS_SALARY_BASED:
            if not amount_basis:
                missing_keys.append("Amount Basis")
            else:
                if amount_basis.lower() not in self.AMOUNT_BASIS_LOWER:
                    meta_errors.append(
                        errors.invalid_value(
                            [{"parameters": ["Amount Basis"],
                              "expectedValue": "/".join(self.AMOUNT_BASIS)}]
                        )
                    )
        else:
            amount_basis = ''

        msp_str = str(data.get("Percentage of Monthly Salary", "")).strip()
        msp = 0
        if bonus_basis == self.BONUS_BASIS_SALARY_BASED:
            if not msp_str:
                missing_keys.append("Percentage of Monthly Salary")
            else:
                try:
                    msp = float(msp_str)
                    if msp < 1:
                        raise ValueError('Invalid value')
                except ValueError:
                    meta_errors.append(
                        errors.invalid_value(
                            [{"parameters": ["Percentage of Monthly Salary"],
                            "expectedFormat": "number",
                            "expectedValue": {"min": 1}}]
                        )
                    )

        release_number_str = str(data.get("Release Number")).strip()
        release_number = 0
        if bonus_frequency == self.BONUS_FREQUENCY_PERIODIC:
            if not release_number_str:
                missing_keys.append("Release Number")
            else:
                try:
                    release_number = int(release_number_str)
                    if release_number < 1:
                        raise ValueError('Invalid value')
                    if not self._store_repeated_periodic_entry(
                        uid, bonus_id, release_number):
                        meta_errors.append(
                            errors.processing_error([
                                "Release Number already exist. "
                                "Please specify a diferent release number for this entry"
                            ])
                        )
                except ValueError:
                    meta_errors.append(
                        errors.invalid_value(
                            [{"parameters": ["Release Number"],
                            "expectedFormat": "number",
                            "expectedValue": {"min": 1}}]
                        )
                    )


        release_date_str = str(data.get("Release Date")).strip().lower()
        release_date = None
        if not release_date_str:
            missing_keys.append("Release Date")
        else:
            try:
                release_date = datetime.datetime.strptime(
                    release_date_str, "%m/%d/%Y"
                )
            except ValueError:
                meta_errors.append(
                    errors.invalid_format([
                        {"parameters": ["Release Date"], "expectedFormat": "MM/DD/YYYY"}
                    ])
                )

        special_pay_run_str = str(
            data.get("Disburse through Special Pay Run?", "")).strip().lower()
        special_pay_run = False
        if not special_pay_run_str:
            missing_keys.append("Disburse through Special Pay Run?")
        elif special_pay_run_str not in ("yes", "no"):
            meta_errors.append(
                errors.invalid_value(
                    [{"parameters": ["Disburse through Special Pay Run?"],
                      "expectedValue": "Yes/No"}]
                )
            )
        else:
            special_pay_run = special_pay_run_str == "yes"

        prorate_based_on_tenure = bonus_basis == self.BONUS_BASIS_SALARY_BASED

        coverage_from_str = str(data.get("Coverage From")).strip().lower()
        coverage_from_date = None
        coverage_to_str = str(data.get("Coverage To")).strip().lower()
        coverage_to_date = None
        if prorate_based_on_tenure and match:
            if not coverage_from_str:
                missing_keys.append("Coverage From")
            else:
                try:
                    coverage_from_date = datetime.datetime.strptime(
                        coverage_from_str, "%m/%d/%Y"
                    )
                except ValueError:
                    meta_errors.append(
                        errors.invalid_format([
                            {"parameters": ["Coverage From"], "expectedFormat": "MM/DD/YYYY"}
                        ])
                    )

            if not coverage_to_str:
                missing_keys.append("Coverage To")
            else:
                try:
                    coverage_to_date = datetime.datetime.strptime(
                        coverage_to_str, "%m/%d/%Y"
                    )
                except ValueError:
                    meta_errors.append(
                        errors.invalid_format([
                            {"parameters": ["Coverage To"], "expectedFormat": "MM/DD/YYYY"}
                        ])
                    )

            if coverage_from_date and coverage_to_date:
                if coverage_to_date < coverage_from_date:
                    meta_errors.append(
                        errors.processing_error([
                            "Coverage To date must be a date same or after the Coverage From date"
                        ])
                    )
                # elif self._is_multiple_releases(uid, bonus_id) and \
                #     (coverage_to_date - coverage_from_date).days > 364:
                #      meta_errors.append(
                #             errors.processing_error([
                #                "Coverage From and To must cover a period of one (1) year "
                #                "in accordance with the coverage from date"
                #             ])
                #         )

        status_str = str(data.get("Status", "")).strip()
        status = False
        if not status_str:
            missing_keys.append("Status")
        else:
            if status_str.lower() not in self.STATUS_LOWER:
                meta_errors.append(
                    errors.invalid_value(
                        [{"parameters": ["Status"],
                            "expectedValue": "/".join(self.STATUS)}]
                    )
                )
            else:
                status = status_str == "Disbursed"

        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        # Authz Validation
        if self.context.get("authzDataScope"):
            authz_data_scope = json.loads(self.context.get("authzDataScope"))
            # Fetch employee data
            api_path = celeryconfig.APIS[celeryconfig.API_EMPLOYEE_BASIC_INFO] % uid
            employee_data = apicalls.get_raw_data(api_path)
            if employee_data:
                employee_data = json.loads(employee_data)
                authz_error = "No Access to Employee " + d_empid
                if not authz.is_employee_authorized(authz_data_scope, employee_data):
                    meta_errors.append(
                        errors.data_scope_error([authz_error])
                    )

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False

        self._add_result(
            {
                "type_id": bonus_id,
                "recipients": {
                    "employees": [
                        uid
                    ]
                },
                "auto_assign": False,
                "basis": str(amount_basis).upper().replace(' ', '_'),
                "percentage": msp,
                "amount": amount,
                "release_details": [
                    {
                        "date": release_date.strftime("%Y-%m-%d"),
                        "disburse_through_special_pay_run": special_pay_run,
                        "prorate_based_on_tenure": prorate_based_on_tenure,
                        "coverage_from": coverage_from_date.strftime("%Y-%m-%d") if coverage_from_date else None,
                        "coverage_to": coverage_to_date.strftime("%Y-%m-%d") if coverage_to_date else None,
                        "status": status
                    }
                ]
            }
        )
        return True
