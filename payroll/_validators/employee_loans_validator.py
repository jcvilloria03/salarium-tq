"""Employee Loans File Upload"""
import datetime
import json
from payroll import files, errors, apicalls

class EmployeeLoansFileUploadValidator(files.UploadItemsFileProcessor):
    """Government loans validator"""
    GOV_LOAN_TYPES = {
        "sss": ["salary", "calamity", "educational", "emergency", "stock investment"],
        "pag-ibig": ["multi purpose", "housing", "short term", "calamity"]
    }

    PAYMENT_SCHEMES = {
        "MONTHLY": ["Every pay of the month"],
        "SEMI_MONTHLY": ["First pay of the month",
                         "Last pay of the month",
                         "Every pay of the month"
                        ],
        "FORTNIGHTLY": [
                        "First pay of the month",
                        "Last pay of the month",
                        "Every pay of the month"
                       ],
        "WEEKLY": ["Every pay of the month",
                   "First pay of the month",
                   "Last pay of the month",
                   "First and last pay of the month"
                   ],
    }

    HEADERS = {
        'government' : set([
            "Employee ID",
            "Last Name",
            "First Name",
            "Middle Name",
            "Loan Type",
            "Loan Category",
            "Reference Number",
            "Date Issued",
            "Repayment Start Date",
            "Payment Scheme",
            "Amount of Loan",
            "Monthly Amortization",
            "Loan Terms (Months)"
        ]),
        'user-defined' : set([
            "Employee ID",
            "Last Name",
            "First Name",
            "Middle Name",
            "Loan Type",
            "Repayment Start Date",
            "Payment Scheme",
            "Amount of Loan",
            "Monthly Amortization",
            "Loan Terms (Months)"
        ])
    }

    def __init__(self, context):
        super().__init__(
            columns=None, #determine later
            type_column_name="Loan Type",
            optional_columns=["Last Name", "First Name", "Middle Name"],
            context=context,
            save_result_per_emp=False,
            context_lookup_key="companyEmployees"
        )
        self.loan_type = None

    def _check_header(self, data): #override
        headers = set(data)
        if "Reference Number" not in headers and\
            "Date Issued" not in headers and\
            "Loan Category" not in headers:  #this is a user-defined
            self.columns = self.HEADERS['user-defined']
            self.loan_type = 'user-defined'
        else:
            self.columns = self.HEADERS['government']
            self.loan_type = 'government'
        missing = self.columns - headers
        if self._optional_columns:
            missing = missing - self._optional_columns
        # missing
        if missing:
            raise files.MissingHeadersException(list(missing))

    def _get_payroll_frequency(self, uid):
        groups = self.context.get("companyPayrollGroups")
        matches = [x for x in groups if int(uid) in x.get("employee_ids", [])]
        if matches:
            return matches[0].get("payroll_frequency")
        return None

    def _get_loan_type_id(self, loan_name):
        types = self.context.get("typeList")
        matches = []
        if self.loan_type == 'government':
            matches = [
                x for x in types
                if (str(x.get("name")).lower() == loan_name and
                    int(x.get("company_id")) == 0
                   )
            ]
        else:
            matches = [
                x for x in types
                if (str(x.get("name")).lower() == loan_name and
                    int(x.get("company_id")) > 0
                   )
            ]
        if matches:
            return matches[0].get("id")
        return None

    def _validate(self, line_index, data):
        if not data:
            return False

        meta_errors = []
        missing_keys = []
        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname
                     }
        uid = None
        sss_num = None
        hdmf_num = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if not emp_data:
                meta_errors.append(errors.id_not_found(d_emp_data))
            else:
                uid = emp_data[0]
                sss_num = emp_data[5]
                hdmf_num = emp_data[6]
        else:
            missing_keys.append("Employee ID")

        date_issued = None
        loan_category = None
        reference_num = None

        loan_type_str = str(data.get("Loan Type")).lower().strip()
        loan_category_str = str(data.get("Loan Category")).lower().strip()
        #get type id from the typeList
        type_id = self._get_loan_type_id(loan_type_str)
        if not loan_type_str:
            missing_keys.append("Loan Type")
        elif self.loan_type == 'government' and loan_type_str not in ["sss", "pag-ibig"]:
            meta_errors.append(errors.invalid_value([{"parameters": ["Loan Type"],
                                                      "expectedValue": "SSS/Pag-IBIG"}]))
        elif self.loan_type == 'user-defined' and type_id is None:
            meta_errors.append(errors.processing_error(["Loan type does not exist."]))
        #gov specific headers
        if self.loan_type == 'government':
            if not loan_category_str:
                missing_keys.append("Loan Category")
            elif (loan_type_str == "sss" and
                  loan_category_str not in self.GOV_LOAN_TYPES[loan_type_str]):
                meta_errors.append(
                    errors.invalid_value(
                        [{"parameters": ["Loan Category"],
                          "expectedValue": "Salary/Calamity/Educational/Emergency/Stock investment"
                         }]
                    )
                )
            elif (loan_type_str == "pag-ibig" and
                  loan_category_str not in self.GOV_LOAN_TYPES[loan_type_str]):
                meta_errors.append(
                    errors.invalid_value(
                        [{"parameters": ["Loan Category"],
                          "expectedValue": "Multi Purpose/Housing/Short term/Calamity"}]
                    )
                )
            else:
                loan_category = loan_category_str.capitalize()

            reference_num = str(data.get("Reference Number")).strip()
            if not reference_num:
                missing_keys.append("Reference Number")

            date_issued_str = str(data.get("Date Issued")).strip()

            if not date_issued_str:
                missing_keys.append("Date Issued")
            else:
                try:
                    date_issued = datetime.datetime.strptime(date_issued_str, "%m/%d/%Y")
                except ValueError:
                    meta_errors.append(
                        errors.invalid_format(
                            [{"parameters": ["Date Issued"],
                              "expectedFormat": "MM/DD/YYYY"}]
                        )
                    )
        #end gov specific headers
        repayment_start_date_str = str(data.get("Repayment Start Date")).strip()
        repayment_start_date = None
        if not repayment_start_date_str:
            missing_keys.append("Repayment Start Date")
        else:
            try:
                repayment_start_date = datetime.datetime.strptime(
                    repayment_start_date_str, "%m/%d/%Y")
            except ValueError:
                meta_errors.append(errors.invalid_format([{"parameters": ["Repayment Start Date"],
                                                           "expectedFormat": "MM/DD/YYYY"}]))

        payment_scheme = str(data.get("Payment Scheme")).strip().lower().capitalize()

        payroll_frequency = None
        if uid:
            payroll_frequency = self._get_payroll_frequency(uid)
        if not payment_scheme:
            missing_keys.append("Payment Scheme")
        elif payroll_frequency and payment_scheme not in self.PAYMENT_SCHEMES[payroll_frequency]:
            meta_errors.append(
                errors.invalid_value(
                    [{"parameters": ["Payment Scheme"],
                      "expectedValue": "/".join(self.PAYMENT_SCHEMES[payroll_frequency])}]
                )
            )

        amount_str = str(data.get("Amount of Loan")).strip().replace(',', '')
        amount = 0
        if not amount_str:
            missing_keys.append("Amount of Loan")
        else:
            try:
                amount = float(amount_str)
                if amount <= 0:
                    raise ValueError("Invalid value")
            except ValueError:
                meta_errors.append(
                    errors.processing_error([
                        "Invalid value for field Amount of Loan. "
                        "This field can only accept numeric values greater than 0."
                    ])
                )

        amortization_str = str(data.get("Monthly Amortization")).strip().replace(',', '')
        amortization = 0

        loan_terms_str = str(data.get("Loan Terms (Months)", ''))
        loan_terms = 0

        if loan_terms_str:
            try:
                loan_terms = int(loan_terms_str)
                if loan_terms < 1:
                    raise ValueError("Invalid value")
            except ValueError:
                meta_errors.append(
                    errors.processing_error([
                        "Invalid value for field Loan Terms (Months). "
                        "This field can only accept numeric values greater than 0."
                    ])
                )
        else:
            missing_keys.append("Loan Terms (Months)")

        if amortization_str:
            try:
                amortization = float(amortization_str)
                if amortization <= 0:
                    raise ValueError("Invalid value")
            except ValueError:
                meta_errors.append(
                    errors.processing_error([
                        "Invalid value for field Monthly Amortization. "
                        "This field can only accept numeric values greater than 0."
                    ])
                )
        else:
            missing_keys.append("Monthly Amortization")

        term = loan_terms
        if (loan_terms > 0 and amortization > 0):
            amortization = amount/float(loan_terms)
            rounded_amort = round(amortization, 2)
            if rounded_amort - amortization < 0:
                rounded_amort += 0.01 #convert the excess to the next decimal place (round up)
            amortization = rounded_amort

        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))


        #try to get the loan preview if no validation error
        if not meta_errors:
            result = {}
            if self.loan_type == 'government':
                result = {
                    "company_id": self.context.get("companyId"),
                    "employee_id": uid,
                    "employee_id_no": sss_num if loan_type_str == "sss" else hdmf_num,
                    "type_id": type_id,
                    "subtype": str(loan_category).upper().replace(' ', '_'),
                    "reference_no": reference_num,
                    "created_date": date_issued.strftime("%Y-%m-%d"),
                    "payment_start_date":
                        (repayment_start_date or date_issued).strftime("%Y-%m-%d"),
                    "payment_scheme": str(payment_scheme).upper().replace(' ', '_'),
                    "total_amount": amount,
                    "monthly_amortization":  round(amortization, 2),
                    "term": term
                }
            else:
                #file upload has no created date column
                #created date is repayment start date (for now)
                created_date = repayment_start_date
                result = {
                    "company_id": self.context.get("companyId"),
                    "employee_id": uid,
                    "type_id": type_id,
                    "created_date": created_date.strftime("%Y-%m-%d"),
                    "payment_start_date": repayment_start_date.strftime("%Y-%m-%d"),
                    "payment_scheme": str(payment_scheme).upper().replace(' ', '_'),
                    "total_amount": amount,
                    "monthly_amortization":  round(amortization, 2),
                    "term": term
                }
            try:
                apicalls.validate_loan_item_preview(result)
                self._add_result(result)
            except Exception as exc: # pylint: disable=broad-except
                errlist = []
                try:
                    err = json.loads(str(exc))
                    if isinstance(err, dict):
                        for _, v in err.items():
                            if isinstance(v, list):
                                errlist.extend(v)
                            else:
                                errlist.append(v)
                    else:
                        errlist.append(err)
                except: # pylint: disable=bare-except
                    errlist.append(str(exc))

                meta_errors.append(
                    errors.processing_error(errlist)
                )

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False
        return True
