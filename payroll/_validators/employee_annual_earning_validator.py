"""Employee Annual Earning File Upload"""
import datetime
import celeryconfig
from payroll import files, errors, apicalls
import json
class EmployeeAnnualEarningFileUploadValidator(files.UploadItemsFileProcessor):
    DOUBLE_OPTIONALS = {
        "Basic Salary/Statutory Minimum Wage (MWE)": 'current_basic_salary_or_statutory_minimum_wage' ,
        "Holiday Pay (MWE)": 'current_holiday_pay_mwe',
        "Overtime Pay (MWE)": 'current_overtime_pay_mwe',
        "Night Shift Differential (MWE)": 'current_nt_differential_mwe',
        "De Minimis Benefits": 'current_de_minimis_benefits',
        "Employee SSS Contribution": 'current_employee_sss_contribution',
        "Employee PhilHealth Contribution": 'current_employee_philhealth_contribution',
        "Employee Pag-IBIG Contribution": 'current_employee_pagibig_contribution',
        "Non-Taxable Salaries & Other Forms of Compensation": 'current_non_taxable_salaries_and_other_forms_of_compensation',
        "13th Month Pay and Other Benefits": 'current_13th_month_pay_and_other_benefits',
        "Basic Salary": 'current_basic_salary',
        "Others Supplementary": 'current_others_supplementary',
        "Others Regular": 'current_others_regular',
        "Taxable Commission": 'current_taxable_commission',
        "Taxable Overtime Pay": 'current_taxable_ot_pay',
        "Premium Paid on Health and Hospital Insurance": 'current_premium_paid_on_health_and_hospital_insurance',
        "Tax Withheld": 'current_tax_withheld',
        "Previous Employer Gross Compensation Income": 'prev_gross_compensation_income',
        "Previous Employer Basic Salary/Statutory Minimum Wage (MWE)": 'prev_basic_salary_or_statutory_minimum_wage',
        "Previous Employer Holiday Pay (MWE)": 'prev_holiday_pay_mwe',
        "Previous Employer Overtime Pay (MWE)": 'prev_overtime_pay_mwe',
        "Previous Employer Night Shift Differential (MWE)": 'prev_nt_differential_mwe',
        "Previous Employer Non-Taxable 13th Month Pay & Other Benefits": 'prev_non_taxable_13th_month_pay_and_other_benefits',
        "Previous Employer De Minimis Benefits": 'prev_de_minimis_benefits',
        "Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution": 'prev_sss_gsis_phic_pagibig_contribution',
        "Previous Employer Non-Taxable Salaries & Other Forms of Compensation": 'prev_non_taxable_salaries_and_other_forms_of_compensation',
        "Previous Employer Total Non-Taxable Compensation Income": 'prev_total_non_taxable_compensation_income',
        "Previous Employer Taxable 13th Month Pay & Other Benefits": 'prev_taxable_13th_month_pay_and_other_benefits',
        "Previous Employer Taxable Salaries & Other Forms of Compensation": 'prev_taxable_salaries_and_other_forms_of_compensation',
        "Previous Employer Total Taxable Compensation Income": 'prev_total_taxable_compensation_income',
        "Previous Employer Premium Paid on Health and Hospital Insurance": 'prev_premium_paid_on_health_and_hospital_insurance',
        "Previous Employer Tax Withheld": 'prev_tax_withheld'
    }

    BUSINESS_ADDRESS =  {
        "Previous Employer TIN": 'prev_employer_tin',
        "Previous Employer Business Name": 'prev_employer_business_name',
        "Previous Employer Business Address": 'prev_employer_business_address',
        "Previous Employer ZIP Code": 'prev_employer_zip_code'
    }

    BUSINESS_PERIOD = {
        "Previous Employer From Period": "prev_date_from",
        "Previous Employer To Period": "prev_date_to",
    }

    def __init__(self, context):
        super().__init__(
            columns=set(
                [
                    "Employee ID",
                    "Last Name",
                    "First Name",
                    "Middle Name",
                    "Year",
                    "Basic Salary/Statutory Minimum Wage (MWE)",
                    "Holiday Pay (MWE)",
                    "Overtime Pay (MWE)",
                    "Night Shift Differential (MWE)",
                    "De Minimis Benefits",
                    "Employee SSS Contribution",
                    "Employee PhilHealth Contribution",
                    "Employee Pag-IBIG Contribution",
                    "Non-Taxable Salaries & Other Forms of Compensation",
                    "13th Month Pay and Other Benefits",
                    "Basic Salary",
                    "Others Supplementary",
                    "Others Regular",
                    "Taxable Commission",
                    "Taxable Overtime Pay",
                    "Premium Paid on Health and Hospital Insurance",
                    "Tax Withheld",
                    "Previous Employer TIN",
                    "Previous Employer Business Name",
                    "Previous Employer From Period",
                    "Previous Employer To Period",
                    "Previous Employer Business Address",
                    "Previous Employer ZIP Code",
                    "Previous Employer Gross Compensation Income",
                    "Previous Employer Basic Salary/Statutory Minimum Wage (MWE)",
                    "Previous Employer Holiday Pay (MWE)",
                    "Previous Employer Overtime Pay (MWE)",
                    "Previous Employer Night Shift Differential (MWE)",
                    "Previous Employer Non-Taxable 13th Month Pay & Other Benefits",
                    "Previous Employer De Minimis Benefits",
                    "Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",
                    "Previous Employer Non-Taxable Salaries & Other Forms of Compensation",
                    "Previous Employer Total Non-Taxable Compensation Income",
                    "Previous Employer Taxable 13th Month Pay & Other Benefits",
                    "Previous Employer Taxable Salaries & Other Forms of Compensation",
                    "Previous Employer Total Taxable Compensation Income",
                    "Previous Employer Premium Paid on Health and Hospital Insurance",
                    "Previous Employer Tax Withheld"
                ]
            ),
            type_column_name=None,
            optional_columns=["Last Name", "First Name", "Middle Name"],
            context=context,
            save_result_per_emp=False,
            context_lookup_key="companyEmployees"
        )
        self.current_year = int(datetime.datetime.now().year)

    def _check_numeric(self, data, item, error_list):
        invalid_params_list = []
        for (key, value) in self.DOUBLE_OPTIONALS.items():
            data_str = data.get(key, "")
            if data_str:
                try:
                    item[value] = float(data_str)
                except ValueError:
                    invalid_params_list.append(key)

        if invalid_params_list:
            error_list.append(
                errors.invalid_value(
                    [{"parameters": invalid_params_list,
                      "expectedFormat": "number",
                     }]
                )
            )
        return item, error_list

    def _validate(self, line_index, data):
        if not data:
            return False
        meta_errors = []
        missing_keys = []

        d_empid = str(data.get("Employee ID", '')).strip()
        d_fname = str(data.get("First Name", '')).strip()
        d_mname = str(data.get("Middle Name", '')).strip()
        d_lname = str(data.get("Last Name", '')).strip()
        d_emp_data = {"Employee Id": d_empid,
                      "First Name": d_fname,
                      "Middle Name": d_mname or '',
                      "Last Name": d_lname
                     }
        uid = None
        if d_empid:
            emp_data = self._lookup.get(d_empid)
            if not emp_data:
                meta_errors.append(errors.id_not_found(d_emp_data))
            else:
                uid = emp_data[0]
        else:
            missing_keys.append("Employee ID")

        # Authz Validation
        if self.context.get("authzDataScope"):
            authz = self.context.get("authzDataScope")
            authz = json.loads(authz)
            # Fetch employee data
            api_path = celeryconfig.APIS[celeryconfig.API_EMPLOYEE_BASIC_INFO] % uid
            employee_data = apicalls.get_raw_data(api_path)
            if employee_data:
                employee_data = json.loads(employee_data)
                authz_error = "No Access to Employee " + d_empid
                if employee_data['company_id'] and 'COMPANY' in authz:
                    if not employee_data['company_id'] in authz['COMPANY']:
                        meta_errors.append(
                            errors.processing_error([authz_error])
                        )
                if employee_data['department_id'] and 'DEPARTMENT' in authz:
                    if not employee_data['department_id'] in authz['DEPARTMENT']:
                        meta_errors.append(
                            errors.processing_error([authz_error])
                        )
                if employee_data['position_id'] and 'POSITION' in authz:
                    if not employee_data['position_id'] in authz['POSITION']:
                        meta_errors.append(
                            errors.processing_error([authz_error])
                        )
                if employee_data['location_id'] and 'LOCATION' in authz:
                    if not employee_data['location_id'] in authz['LOCATION']:
                        meta_errors.append(
                            errors.processing_error([authz_error])
                        )
                if employee_data['time_attendance']['team_id'] and 'TEAM' in authz:
                    if not employee_data['time_attendance']['team_id'] in authz['TEAM']:
                        meta_errors.append(
                            errors.processing_error([authz_error])
                        )
                if employee_data['payroll']['payroll_group_id'] and 'PAYROLL_GROUP' in authz:
                    if not employee_data['payroll']['payroll_group_id'] in authz['PAYROLL_GROUP']:
                        meta_errors.append(
                            errors.processing_error([authz_error])
                        )
        year_str = data.get("Year")

        update_item = {
            "employee_id": uid,
            "company_id": self.context.get("companyId")
        }

        if not year_str:
            missing_keys.append("Year")
        else:
            try:
                update_item['year'] = int(year_str)
                if update_item['year'] < 1900 or update_item['year'] > self.current_year:
                    raise ValueError()
            except ValueError:
                meta_errors.append(
                    errors.processing_error(['Invalid value for Year.  This field can only accept a valid year.'])
                )
        #check the previous employer data
        tin_str = str(data.get("Previous Employer TIN", "")).strip()
        business_name_str = str(data.get("Previous Employer Business Name", "")).strip()
        business_address_str = str(data.get("Previous Employer Business Address", "")).strip()
        zip_code = str(data.get("Previous Employer ZIP Code", "")).strip()
        date_from =  str(data.get("For The Period From")).strip()
        date_to = str(data.get("For The Period To")).strip()

        if business_name_str and not tin_str:
            meta_errors.append(
                errors.processing_error(["Previous Employer TIN is required"])
            )
        if not business_name_str and tin_str:
            meta_errors.append(
                errors.processing_error(["Previous Employer Business Name is required"])
            )

        if business_name_str and not date_from:
            meta_errors.append(
                errors.processing_error(["Previous Employer From Period is required"])
            )

        if business_name_str and not date_to:
             meta_errors.append(
                errors.processing_error(["Previous Employer To Period is required"])
            )

        #TODO: check date from and date to values if they are valid dates
        #TODO: check if from date < to date
        if business_name_str:
            period_from_str = str(data.get("Previous Employer From Period", "")).strip().lower()
            period_from = None
            if not period_from_str:
                missing_keys.append("Previous Employer From Period")
            else:
                try:
                    period_from = datetime.datetime.strptime(
                        period_from_str, "%m/%d/%Y")
                except ValueError:
                    meta_errors.append(
                        errors.invalid_format([{"parameters": ["Previous Employer From Period"],
                                                "expectedFormat": "MM/DD/YYYY"}])
                    )
            period_to_str =  str(data.get("Previous Employer To Period", "")).strip().lower()
            period_to = None
            if not period_to_str:
                missing_keys.append("Previous Employer To Period")
            else:
                try:
                    period_to = datetime.datetime.strptime(
                        period_to_str, "%m/%d/%Y")
                except ValueError:
                    meta_errors.append(
                        errors.invalid_format([{"parameters": ["Previous Employer To Period"],
                                                "expectedFormat": "MM/DD/YYYY"}])
                    )

            if period_from and period_to and (period_from > period_to):
                meta_errors.append(
                    errors.processing_error(["Your Previous Employer To Period date should not be earlier than Previous Employer From Period date"])
                )
            if period_from and period_to: #add to update item
                update_item['prev_date_from'] = period_from.strftime("%Y-%m-%d")
                update_item['prev_date_to'] = period_to.strftime("%Y-%m-%d")

        if business_name_str:
            update_item['prev_employer_business_name'] = business_name_str
        if tin_str:
            update_item['prev_employer_tin'] = tin_str
        if business_address_str:
            update_item['prev_employer_business_address'] = business_address_str
        if zip_code:
            update_item['prev_employer_zip_code'] = zip_code

        update_item, meta_errors = self._check_numeric(data, update_item, meta_errors)
        if missing_keys:
            meta_errors.insert(0, errors.missing_parameters([{"parameters": missing_keys}]))

        if meta_errors:
            self.add_error_list(str(line_index), meta_errors)
            return False

        self._add_result(update_item)
        return True
