"""Main app"""
from __future__ import absolute_import

import os
import time

#
from celery import Celery
from event_consumer.handlers import AMQPRetryConsumerStep
from core import logger
from payroll import dao

logger = logger.get_logger()

# instantiate celery object

#TODO: REmove this and timezone info should be company specific
os.environ["TZ"] = "Asia/Manila"
time.tzset()

app = Celery('tasks')



# import celery config
app.config_from_object('pr_config')
app.conf.update({
    'task_routes' : ([('payroll.tasks.*', {'queue': 'payroll'}),
                      ('payroll.tasks.*', {'queue': 'payroll-inner'}),
                      ('payslip.tasks.*', {'queue': 'payroll'})],)

})

app.steps['consumer'].add(AMQPRetryConsumerStep)

#tasks.attendance.workflows.workflow('a1', 'b1')

dao.redis_connect()

if __name__ == '__main__':
    try:
        app.start()
    except Exception as err:
        logger.critical(str(err))
        raise err
