import redis

import pr_config

class RedisConnect(object):
    conn = None
    session = None

def redis_connect():
    """Create a pool connection to Redis
    """
    RedisConnect.conn = redis.Redis(
        host=pr_config.REDIS_HOST, port=pr_config.REDIS_PORT,
        db=pr_config.REDIS_DATABASE)
