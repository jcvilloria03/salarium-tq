"""Configuration module"""
from core import config as global_cfg
import celeryconfig

global_cfg.find_env_vars()

class Env:
    """Payroll environment vars and constants"""
    # constants
    PAYROLL_CONSUMER_ROUTING_KEY = (
        global_cfg.getenv("PAYROLL_CONSUMER_ROUTING_KEY") or "PAYROLL_CONSUMER"
    )

    JOBS_API_BASE_URL = global_cfg.getenv("JM_API_BASE_URL") or ""
    JOBS_API_URLS = {
        "create-job": ("/jobs", "POST"),
        "update-job": ("/jobs/{job_id}", "PATCH"),
        "delete-job-errors-by-job-id": ("/job-errors/{job_id}/items", "DELETE"),
        "delete-job-results-by-job-id": ("/job-results/{job_id}/items", "DELETE"),
        "get-job": ("/jobs/{job_id}", "GET"),
        "get-job-with-results": ("/jobs/{job_id}?include=results", "GET"),
        "create-job-error": ("/job-errors", "POST"),
        "create-job-result": ("/job-results", "POST"),
        "get-job-result": ("/job-results/{job_result_id}", "GET"),
        "get-job-results": ("/job-results?filter[job_id]={job_id}", "GET"),
        "delete-job-error": ("/job-errors/{job_error_id}", "DELETE")
    }

    DAY_HOUR_RATES_URL = "{domain}/{path}".format(
        domain=celeryconfig.TA_API_DOMAIN, path="/company/{company_id}/day_hour_rates"
    )

    NOTIFICATION_API_CREATE_ACTIVITY = "{domain}/{path}".format(
        domain=global_cfg.getenv('NT_API_DOMAIN'),
        path="activity"
    )
    PAYROLL_GROUP_EMPLOYEES_URL = "{domain}/{path}".format(
        domain=celeryconfig.CP_API_DOMAIN,
        path="payroll_group/{payroll_group_id}/employees"
             "?start_date={start_date}&end_date={end_date}",
    )
    EMPLOYEES_BY_IDS = "{domain}/{path}".format(
        domain=celeryconfig.CP_API_DOMAIN,
        path="company/{company_id}/employees_by_ids",
    )
    GET_COMPANY_PAYROLL_GROUPS_URL = "{domain}/{path}".format(
        domain=celeryconfig.CP_API_DOMAIN,
        path="company/{company_id}/payroll_groups"
    )

    GET_USERIDS_USING_EMAILS_URL = "{domain}/{path}".format(
        domain=celeryconfig.CP_API_DOMAIN,
        path="company/{company_id}/users/email"
    )

    COMPANY_API_GET_LOGO_DATA = "{domain}/{path}".format(
        domain=celeryconfig.CP_API_DOMAIN,
        path="company/{company_id}/logo"
    )

    PAYROLL_API_GET_PAYROLL_SETTINGS = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/settings"
    )

    PAYROLL_API_GET_PAYROLL_SETTINGS_GENERIC = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payrolls/{payroll_id}/settings"
    )

    PAYROLL_API_GET_SPECIAL_PAYROLL_SETTINGS = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payrolls/{payroll_id}/settings"
             "?include=employees"
    )

    SAVE_PROJECTED_FINAL_PAY_ITEM = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="final_pay/projection/save"
    )

    PAYROLL_API_GET_PAYROLL_SETTINGS_GENERIC_WITH_EMPLOYEES = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payrolls/{payroll_id}/settings"
             "?include=employees"
    )

    PAYROLL_API_GET_SPECIAL_PAYROLL_EMPLOYEE_SETTINGS = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payrolls/{payroll_id}/employees/settings?ids={employee_id}"
    )

    PAYROLL_API_GET_FINAL_PAYROLL_EMPLOYEE_SETTINGS = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="final_pay/{payroll_id}/employees/settings?ids={employee_id}"
    )

    PAYROLL_API_GET_PRGROUP_EMPLOYEE_SETTINGS = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/employee/{employee_id}/settings"
    )

    PAYROLL_API_GET_EMPLOYEE_PAYROLL_RELATED_LOAN_ITEMS = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/employee/{employee_id}/loan_items"
    )

    PAYROLL_API_OPEN_PAYROLL = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/open"
    )

    PAYROLL_API_DOWNLOAD_MULTIPLE_PAYSLIPS = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payslip/download_multiple"
    )

    PAYROLL_API_CREATE_PAYROLL_REGISTER = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="/payroll_register/{payroll_id}"
    )

    PAYROLL_API_EMPLOYEE_CLOSE = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/employee/{employee_id}/close"
    )

    PAYROLL_API_EMPLOYEE_REOPEN = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/employee/{employee_id}/open"
    )

    PAYROLL_API_REOPEN = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/reopen"
    )

    PAYROLL_API_GET_OTHER_INCOME_TYPE = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="/company/{company_id}/other_income_types/{other_income_type}"
    )

    PAYROLL_API_CLEAR_PAYSLIP_FLAG = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/clear_payslip_sent_flag"
    )

    PAYROLL_LOAN_API_GET_AMORTIZATIONS = "{domain}/{path}".format(
        domain=global_cfg.getenv("PL_API_DOMAIN"),
        path="company/{company_id}/amortizations_in_date_range"
    )

    PAYROLL_LOAN_API_SAVE_LOAN_ITEM = "{domain}/{path}".format(
        domain=global_cfg.getenv("PL_API_DOMAIN"),
        path="payroll_loan/create_direct"
    )

    PAYROLL_LOAN_GET_COMPANY_LOAN_TYPES = "{domain}/{path}".format(
        domain=global_cfg.getenv("PL_API_DOMAIN"),
        path="company/{company_id}/payroll_loan_types"
    )

    PAYROLL_LOAN_API_UPDATE_AMORTIZATIONS = "{domain}/{path}".format(
        domain=global_cfg.getenv("PL_API_DOMAIN"),
        path="company/{company_id}/amortizations_bulk_update"
    )

    PAYROLL_LOAN_API_VALIDATE_LOAN_PREVIEW = "{domain}/{path}".format(
        domain=global_cfg.getenv("PL_API_DOMAIN"),
        path="payroll_loan/validate"
    )

    PAYROLL_API_EMAIL = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="email"
    )

    PAYROLL_API_GET_PAYROLL_JOB = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll_job/{uuid}"
    )

    PAYROLL_API_BULK_CREATE_OTHER_INCOME = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="philippine/company/{company_id}/{income_type}/bulk_create"
    )

    PAYROLL_API_BULK_SET_ANNUAL_EARNING = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="company/{company_id}/annual_earning/bulk_set"
    )

    PREVIEW_PAYSLIP_URL = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll_calculation/payslip_preview?payroll_id={payroll_id}&employee_id={employee_id}"
    )

    CREATE_PAYSLIP_URL = "{domain}/payslips".format(domain=celeryconfig.PR_API_DOMAIN)

    ATTENDANCE_SERVICE_GET_ATTENDANCE_URL = "{domain}/{path}".format(
        domain=celeryconfig.AS_API_DOMAIN,
        path="attendance/records"
    )

    CONTRIBUTION_RECORD_URL = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="previous_contributions"
             "?employee_id={employee_id}&payroll_posting_date={payroll_posting_date}")

    SAVE_PAYROLL_ITEM_URL = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll_calculation"
    )

    SET_PAYROLL_STATUS_URL = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}"
    )

    REVERT_PAYROLL_STATUS_URL = "{domain}/{path}".format(
        domain=celeryconfig.PR_API_DOMAIN,
        path="payroll/{payroll_id}/revert"
    )

    SALPAY_PAYROLL_DISBURSEMENTS_URL = "{domain}/{path}".format(
        domain=celeryconfig.SI_API_DOMAIN,
        path="disbursements"
    )

    SB_SUBCRIPTION_CUSTOMERS_URL = "{domain}/{path}".format(
        domain=celeryconfig.SB_API_DOMAIN,
        path="subscriptions/customers"
    )

    NOTIFICATION_API_EMAIL = "{domain}/{path}".format(
        domain=global_cfg.getenv('NT_API_DOMAIN'),
        path="notification/masterfile"
    )

    PAYSLIP_BUCKET = global_cfg.getenv('PAYSLIP_BUCKET') or 'v3-payslips-local' #TODO: remove the or
    ZIPPED_PAYSLIP_BUCKET = global_cfg.getenv('ZIPPED_PAYSLIP_BUCKET') or 'v3-zipped-payslips-local'
