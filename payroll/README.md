# Payroll Calculator Tasks


## For Devs -- Creating tasks steps.

1.  Go to ```/payroll/_tasks``` package. 
2.  Create your task file.  For example:  ```my_tasks.py```

```python
from core.app import app

@app.task
def my_custom_task():
    print("Hi!")
```

3.  Edit ```payroll/_tasks/__init__.py``` and add import your tasks from there.

```python
. . .
from payroll._tasks.my_tasks import my_custom_task
. . .

```

This ensures that your tasks will be included when Celery inspects ```payroll/tasks.py``` for tasks
to register.


## For Devs -- Creating workflows steps.

1.  First decide:  is the workflow to be handled as task? (In which case it should be in tasks) or 
just a definition of workflow using tasks?

2.  If the latter, then same steps as with ```payroll/_tasks```, do your code in ```payroll/_workflows```,
then do not forget to import your workflow in ```payroll/_workflows/__init__.py```


## _tasks and _workflows

This should be private modules and they should be used only within payroll.  

## Tasks and Their Payloads