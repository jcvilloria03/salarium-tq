"""Message consumer tasks"""

import json
import uuid

from event_consumer.handlers import message_handler

from core import logger

from payroll import tasks
from payroll import constants, config

logger = logger.get_logger()

TASKS_DIR = {
    constants.WORKFLOW_VALIDATE_ATTENDANCE_FILE: tasks.validate_attendance_file,
    constants.WORKFLOW_VALIDATE_ALLOWANCE_FILE: tasks.validate_payroll_items_file,
    constants.WORKFLOW_VALIDATE_BONUS_FILE: tasks.validate_payroll_items_file,
    constants.WORKFLOW_VALIDATE_COMMISSION_FILE: tasks.validate_payroll_items_file,
    constants.WORKFLOW_VALIDATE_DEDUCTION_FILE: tasks.validate_payroll_items_file,

    constants.WORKFLOW_VALIDATE_EMPLOYEE_ALLOWANCE_FILE: tasks.process_employee_other_income,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_ADJUSTMENT_FILE: tasks.process_employee_other_income,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_BONUS_FILE: tasks.process_employee_other_income,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_DEDUCTION_FILE: tasks.process_employee_other_income,
    constants.WORKFLOW_VALIDATE_EMPLOYEE_COMMISSION_FILE: tasks.process_employee_other_income,

    constants.WORKFLOW_VALIDATE_EMPLOYEE_ANNUAL_EARNING_FILE:
        tasks.process_employee_annual_earning,

    constants.WORKFLOW_VALIDATE_EMPLOYEE_LOAN_FILE: tasks.process_employee_loans,

    constants.WORKFLOW_COMPUTE_PAYROLL: tasks.run_payroll,
    constants.WORKFLOW_COMPUTE_SPECIAL_PAYROLL: tasks.run_special_payroll,
    constants.WORKFLOW_COMPUTE_FINAL_PAYROLL: tasks.run_final_payroll,
    constants.WORKFLOW_COMPUTE_PROJECTED_FINAL_PAY: tasks.run_projected_final_pay,
    constants.WORKFLOW_CLOSE_PAYROLL: tasks.close_payroll,
    constants.WORKFLOW_REOPEN_PAYROLL: tasks.reopen_payroll,

    constants.WORKFLOW_AS_ATTENDANCE_DATA: tasks.get_attendance_data,

    constants.WORKFLOW_GENERATE_PAYROLL_PAYSLIPS: tasks.generate_payroll_payslips,
    constants.WORKFLOW_EMAIL_ZIPPED_PAYSLIPS: tasks.email_zipped_payslips,
    constants.WORKFLOW_SEND_PAYSLIPS: tasks.send_payslips,
    constants.WORKFLOW_EMAIL_ZIPPED_PAYROLL_REGISTER: tasks.email_zipped_payroll_register,

    constants.WORKFLOW_SYNC_DISBURSEMENT: tasks.sync_disbursement_workflow,

    constants.WORKFLOW_GENERATE_EMPLOYEE_MASTERFILE: tasks.generate_employee_masterfile,

}

@message_handler(config.Env.PAYROLL_CONSUMER_ROUTING_KEY)
def consume_payroll_task(body):
    """Process single calculation given an employee uid and date"""
    data = json.loads(body) or dict()
    task_name = ""
    # if no jobid
    if not data.get("jobId"):
        data["jobId"] = "INT:%s" % str(uuid.uuid4())

    if data:
        task_name = data.get("name")

    func = TASKS_DIR.get(task_name)
    if func:
        logger.info("Executing %s workflow with assigned ID: %s", task_name, data.get("jobId"))
        func(data)
    else:
        logger.warning("Undefined workflow for task name: %s", task_name)
