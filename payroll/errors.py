"""Error utilities"""

def _error_dict(code, name, details):
    return {
        "code": code, "name": name, "details": details
    }

def missing_parameters(details):
    """VE-100 Error"""
    return _error_dict("VE-100", "Missing Parameters", details)

def invalid_format(details):
    """VE-101 Error"""
    return _error_dict("VE-101", "Invalid Format", details)

def invalid_value(details):
    """VE-102 Error"""
    return _error_dict("VE-102", "Invalid Value", details)

def duplicate_data(details):
    """VE-103 Error"""
    return _error_dict("VE-103", "Duplicate Data", details)

def id_not_found(details):
    """VE-200 Error"""
    return _error_dict("VE-200", "ID Not Found", details)

def information_mismatch(details):
    """VE-201 Error"""
    return _error_dict("VE-201", "Information Mismatch", details)

def processing_error(details):
    """VE-203"""
    return _error_dict("VE-203", "Processing Error", details)

def data_scope_error(details):
    """VE-203"""
    return _error_dict("VE-203", "Unauthorized", details)
