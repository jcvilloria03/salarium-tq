"""Payroll related API calls"""

import json
import uuid

import requests

import celeryconfig
import ast
from core import exceptions, logger
from payroll import config
from payroll import dao

logger = logger.get_logger()


def _exec_request(url, method, headers=None, params=None, data=None, form_data=None):
    try:
        response = requests.request(
            method=method, url=url, headers=headers, params=params, json=data, data=form_data,
            timeout=(5, 300))
        if response.status_code >= 500:  # peer error
            raise exceptions.PeerError(
                'API endpoint ({}) responded with a server error response'.format(url))

        if response.text:
            json_data = response.json()
            if isinstance(json_data, list):
                if response.status_code >= 400:
                    tmpmsg = json_data[0].get("message") or str(response.text)
                    obj = ast.literal_eval(tmpmsg)
                    for err_key, err_msg in obj[0].items():
                        tmpstr = ''
                        for ele in err_msg:
                            tmpstr += ele
                        message = err_key + ': ' + tmpstr
                    raise exceptions.RequestError(message)
                return json_data
            else:
                if response.status_code >= 400:
                    #logger.warning(response.text)
                    message = json_data.get("message") or str(response.text)
                    raise exceptions.RequestError(message)
                return json_data
        return {}
    except requests.exceptions.Timeout as exc:
        raise exceptions.PeerError(str(exc))
    except requests.exceptions.RequestException as exc:
        raise exceptions.PeerError(str(exc))


def get_raw_data(url): # pragma: no cover
    """Get Raw data"""
    try:
        response = requests.get(url)
        if response.status_code >= 500:  # peer error
            raise exceptions.PeerError(
                'API endpoint ({}) responded with a server error response'.format(url))

        return response.text
    except requests.exceptions.Timeout as exc:
        raise exceptions.PeerError(str(exc))
    except requests.exceptions.RequestException as exc:
        raise exceptions.PeerError(str(exc))


def update_job(job_id, status=None, tasks_count=None, tasks_done=None):
    """API call for updating JOB status for JMAPI"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["update-job"]
    attributes = {}
    if status is not None:
        attributes["status"] = status
    if tasks_count is not None:
        attributes["tasksCount"] = tasks_count
    if tasks_done is not None:
        attributes["tasksDone"] = tasks_done
    body = {
        "data": {
            "type": "jobs",
            "id": job_id,
            "attributes": attributes
        }
    }
    url = "{base}{path}".format(base=base_path, path=path)
    url = url.format(job_id=job_id)
    response = _exec_request(url, method, data=body)
    return response

def delete_job_error_items_by_job_id(job_id):
    """API call for deleting JOB errors for JMAPI, per parent job id"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["delete-job-errors-by-job-id"]
    url = "{base}{path}".format(base=base_path, path=path)
    url = url.format(job_id=job_id)
    response = _exec_request(url, method)
    return response

def delete_job_result_items_by_job_id(job_id):
    """API call for deleting JOB results for JMAPI, per parent job id"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["delete-job-results-by-job-id"]
    url = "{base}{path}".format(base=base_path, path=path)
    url = url.format(job_id=job_id)
    response = _exec_request(url, method)
    return response

def add_job_error(job_id, description, error_id=None): # pragma: no cover
    """API call for adding job error for JMAPI"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["create-job-error"]
    attributes = {"jobId": job_id, "description": description}
    body = {
        "data": {
            "type": "job-errors",
            "attributes": attributes
        }
    }
    if error_id is not None:
        body["meta"] = {
            "useRangeKey": str(error_id)
        }
    url = "{base}{path}".format(base=base_path, path=path)
    try:
        response = _exec_request(url, method, data=body)
    except exceptions.RequestError:  # ignore
        response = {}

    return response


def delete_job_error(job_error_id): # pragma: no cover
    """API call for deleting job_error"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["delete-job-error"]
    url = "{base}{path}".format(base=base_path,
                                path=path.format(job_error_id=job_error_id))
    try:
        response = _exec_request(url, method)
    except exceptions.RequestError:  # ignore
        response = {}
    return response


def add_job_result(job_id, result, result_id=None): # pragma: no cover
    """API call for adding job results (JMAPI)"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["create-job-result"]
    attributes = {"jobId": job_id, "result": result}
    body = {
        "data": {
            "type": "job-results",
            "attributes": attributes
        }
    }
    if result_id is not None:
        body["meta"] = {
            "useRangeKey": str(result_id)
        }
    url = "{base}{path}".format(base=base_path, path=path)
    response = _exec_request(url, method, data=body)

    logger.info(str({
        "task": "apicalls.add_job_result",
        "job_id": str(job_id),
        "url": str(url),
        "body": json.dumps(body),
        "response": json.dumps(response),
    }))

    return response

def get_result_from_redis(key):
    return dao.RedisConnect.conn.get(key)

def add_job_result_with_redis(job_id, result, result_id=None): # pragma: no cover
    """
    Ticket: https://gitlab.salarium.com/salarium/development/t-and-a-manila/-/issues/8401
    API call for adding job results (JMAPI).
    This will save the body data in redis and send
    redis key to jm-api, then jm-api will fetch the
    data from redis.
    """
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["create-job-result"]
    attributes = {"jobId": job_id, "result": result}
    body = {
        "data": {
            "type": "job-results",
            "attributes": attributes
        }
    }
    if result_id is not None:
        body["meta"] = {
            "useRangeKey": str(result_id)
        }
    redis_key = 'job-results-api-payload:{}'.format(str(uuid.uuid4()))
    # saving body data in redis
    dao.RedisConnect.conn.set(redis_key, json.dumps(body), ex=600)
    payload = {
        "fetch_payload_from_redis": True,
        "redis_key": redis_key
    }
    url = "{base}{path}".format(base=base_path, path=path)
    response = _exec_request(url, method, data=payload)

    logger.info(str({
        "task": "apicalls.add_job_result",
        "job_id": str(job_id),
        "url": str(url),
        "body": json.dumps(body),
        "payload": json.dumps(payload),
        "response": json.dumps(response),
    }))

    return response
    


def get_company_logo(company_id):  # pragma: no cover
    """Get company logo data"""
    api_path = config.Env.COMPANY_API_GET_LOGO_DATA.format(
        company_id=company_id)
    result = _exec_request(api_path, 'GET')
    return result


def get_company_employees(company_id):
    """API call for getting company employees"""
    api_path = celeryconfig.APIS[celeryconfig.API_COMPANY_EMPLOYEES]
    url = api_path % str(company_id)
    # Paginated fetch
    current_page = 0
    per_page = 100
    lookup = {}  # dict with employee_company_id as the key
    is_done = False
    while is_done is False:
        current_page += 1
        query_string = f"?page={current_page}&per_page={per_page}"
        result = _exec_request(url + query_string, 'GET')

        if result:
            data = result.get("data", [])
            # and value is tuple containing employee_uid, fname, mname, lastname
            for item in data:
                lookup[item.get("employee_id")] = (
                    item.get("id"),
                    item.get("first_name"),
                    item.get("middle_name"),
                    item.get("last_name"),
                    item.get("active") in ("active", True))

        current_page = result.get("current_page")
        last_page = result.get("last_page")
        is_done = current_page >= last_page

    return lookup

    raise exceptions.PeerError(
        'No company employee info available for indicated company id')

def get_company_employees_with_filters(company_id, include=None, status=None, mode=None, page=1, per_page=10, authz_data_scope=None, search_term=None):
    """API call for getting company employees, with filters"""
    api_path = celeryconfig.APIS[celeryconfig.API_COMPANY_EMPLOYEES]
    url = api_path % str(company_id)

    if search_term != '':
        query_string = f"?page={page}&per_page={per_page}&keyword={search_term}"
    else:
        query_string = f"?page={page}&per_page={per_page}"

    if include:
        query_string += "&include=" + include

    if status:
        query_string += "&status=" + status

    if mode:
        query_string += "&mode=" + mode

    if authz_data_scope:
        query_string += "&authz_data_scope=" + json.dumps(authz_data_scope)

    result = _exec_request(url + query_string, 'GET')
    if result:
        return result

    raise exceptions.PeerError(
        '[apicalls.get_company_employees_with_filters] No company employee info available for indicated company id')


def get_company_employees_related_ids(company_id):
    """API call for getting company employees"""
    api_path = celeryconfig.APIS[celeryconfig.API_COMPANY_EMPLOYEES]
    url = api_path % str(company_id)
    # Paginated fetch
    current_page = 0
    per_page = 100
    lookup = {}  # dict with employee_company_id as the key
    is_done = False

    while is_done is False:
        current_page += 1
        query_string = f"?page={current_page}&per_page={per_page}"
        result = _exec_request(url + query_string, 'GET')

        if result:
            data = result.get("data", [])

            for employee in data:
                lookup[employee.get('id')] = {
                    'employee_id': employee.get('employee_id'),
                    'account_id': employee.get('account_id'),
                    'company_id': employee.get('company_id'),
                    'department_id': employee.get('department_id'),
                    'position_id': employee.get('position_id'),
                    'location_id': employee.get('location_id'),
                    'team_id': employee.get('team_id'),
                    'payroll_group_id': employee.get('payroll_group').get('id') \
                        if employee.get('payroll_group') else None
                }

        current_page = result.get("current_page")
        last_page = result.get("last_page")
        is_done = current_page >= last_page

    return lookup

    raise exceptions.PeerError(
        'No company employee info available for indicated company id')


def get_company_employees_with_gov_ids(company_id):
    """API call for getting company employees"""
    api_path = celeryconfig.APIS[celeryconfig.API_COMPANY_EMPLOYEES]
    url = api_path % str(company_id)
    # Paginated fetch
    current_page = 0
    per_page = 100
    lookup = {}  # dict with employee_company_id as the key
    # and value is tuple containing employee_uid, fname, mname,
    # lastname, active, sss-id, pagibig-id, philhealth
    is_done = False

    while is_done is False:
        current_page += 1
        query_string = f"?mode=MINIMAL&page={current_page}&per_page={per_page}"
        result = _exec_request(url + query_string, 'GET')

        if result:
            data = result.get("data", [])

            for item in data:
                lookup[item.get("employee_id")] = (item.get("id"),
                                                item.get("first_name"),
                                                item.get("middle_name"),
                                                item.get("last_name"),
                                                item.get("active", False) in (
                                                    'active', True),
                                                item.get("payroll", {}).get(
                                                    "sss_number"),
                                                item.get("payroll", {}).get(
                                                    "hdmf_number"),
                                                item.get("payroll", {}).get(
                                                    "philhealth_number")
                                                )
        current_page = result.get("current_page")
        last_page = result.get("last_page")
        is_done = current_page >= last_page
    return lookup

    raise exceptions.PeerError(
        'No company employee info available for indicated company id')


def get_company_payroll_groups(company_id):
    """API call for getting company employees"""
    api_path = config.Env.GET_COMPANY_PAYROLL_GROUPS_URL.format(
        company_id=company_id)
    result = _exec_request(api_path, 'GET')
    if result:
        data = result.get("data", [])
        return data
    raise exceptions.PeerError(
        'No company employee info available for indicated company id')


def get_company_day_hour_rates(company_id):  # pragma: no cover
    """API call for getting company day hour rates"""
    api_path = str(config.Env.DAY_HOUR_RATES_URL).format(company_id=company_id)
    result = _exec_request(api_path, 'GET')
    return result


# payroll_group_id, start_date, end_date):
def get_payroll_group_amortizations(company_id):  # pragma: no cover
    """Get Payroll group amortizations API call"""
    api_path = str(config.Env.PAYROLL_LOAN_API_GET_AMORTIZATIONS).format(
        company_id=company_id)
    result = _exec_request(api_path, 'GET')
    return result


def get_payroll_group_employees(company_id, payroll_group_id,
                                start_date, end_date, include=None, active_only=False):
    """Getting payroll group employees"""
    api_path = str(config.Env.PAYROLL_GROUP_EMPLOYEES_URL).format(payroll_group_id=payroll_group_id,
                                                                  start_date=start_date,
                                                                  end_date=end_date)
    result = _exec_request(api_path, 'GET')
    if result and include is not None:
        result_data = result.get("data", [])
        new_data = []
        for item in result_data:
            # check if company_id is the same!
            if str(item.get("company_id")) != str(company_id):
                raise exceptions.PeerError(
                    "Mismatched company_id for payroll_group_id")
            if active_only and item.get("active", False) not in ("active", True):
                continue
            new_item = {k: v for (k, v) in item.items() if k in include}
            new_data.append(new_item)
        return {"data": new_data}

    return result

def get_employees_by_ids(company_id, employee_ids, mode="DEFAULT"):
    """Get Employees by IDs"""
    api_path = str(config.Env.EMPLOYEES_BY_IDS).format(company_id=company_id)
    payload = {
        "ids": employee_ids,
        "mode": mode
    }

    return _exec_request(api_path, 'POST', data=payload)

def get_employee_payroll_related_loan_items(payroll_id, employee_id):
    """API call for loan items"""
    api_path = config.Env.PAYROLL_API_GET_EMPLOYEE_PAYROLL_RELATED_LOAN_ITEMS.format(
        payroll_id=payroll_id,
        employee_id=employee_id
    )
    result = _exec_request(api_path, 'GET')
    included = result.get("included", [])
    loans = [
        {**x.get("attributes", {}), **{"id": x.get("id")}}
        for x in included
        if x.get("type", "") == "loans"
    ]
    amortizations = [
        {**x.get("attributes", {}), **{"id": x.get("id")}}
        for x in included
        if x.get("type", "") == "amortizations"
    ]
    return loans, amortizations


def update_amortizations(company_id, data):  # pragma: no cover
    """Bulk update amortization call"""
    api_path = config.Env.PAYROLL_LOAN_API_UPDATE_AMORTIZATIONS.format(
        company_id=company_id
    )
    result = _exec_request(api_path, 'PATCH', data=data)
    return result


def get_attendance_data(employee_uid, start_date, end_date):  # pragma: no cover
    """API call for getting attendance entries for employee"""
    api_path = config.Env.ATTENDANCE_SERVICE_GET_ATTENDANCE_URL
    payload = {
        "ids": [int(employee_uid)],
        "start_date": start_date,
        "end_date": end_date,
        "page_size": 31,
        "mode": "SIMPLIFIED"  # so everything is here
    }

    result = _exec_request(api_path, 'POST', data=payload)
    return result.get("data", [])


def get_payroll_job(job_id):  # pragma: no cover
    """Get payroll job"""
    api_path = config.Env.PAYROLL_API_GET_PAYROLL_JOB.format(uuid=job_id)
    result = _exec_request(api_path, 'GET')
    return result


def get_payroll_settings(prid):  # pragma: no cover
    """API call to get payroll settings given id"""
    api_path = config.Env.PAYROLL_API_GET_PAYROLL_SETTINGS.format(
        payroll_id=prid)

    result = _exec_request(api_path, 'GET')
    return result


def get_payroll_settings_generic(prid, with_employees=False): # pragma: no cover
    """API call to get payroll settings given id"""
    api_path = ''
    if not with_employees:
        api_path = config.Env.PAYROLL_API_GET_PAYROLL_SETTINGS_GENERIC.format(
            payroll_id=prid)
    else:
        api_path = config.Env.PAYROLL_API_GET_PAYROLL_SETTINGS_GENERIC_WITH_EMPLOYEES.format(
            payroll_id=prid)

    result = _exec_request(api_path, 'GET')
    return result


def open_payroll(prid):  # pragma: no cover
    """API call to open payroll with given id"""
    api_path = config.Env.PAYROLL_API_OPEN_PAYROLL.format(payroll_id=prid)
    result = _exec_request(api_path, 'POST')
    return result


def get_job(job_id):  # pragma: no cover
    """Get Job detail api call"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["get-job"]

    url = "{base}{path}".format(base=base_path, path=path)
    result = _exec_request(url.format(job_id=job_id), method)
    return result


def get_job_with_results(job_id): # pragma: no cover
    """Get job detail with results"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["get-job-with-results"]

    url = "{base}{path}".format(base=base_path, path=path)
    result = _exec_request(url.format(job_id=job_id), method)
    return result


def get_job_results(job_id=None, page_url=None): # pragma: no cover
    """Get job results API call"""
    url = None
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["get-job-results"]
    if page_url:
        url = "{base}{path}".format(base=base_path, path=page_url)
    elif job_id:
        url = "{base}{path}".format(base=base_path, path=path)
        url = url.format(job_id=job_id)
    result = _exec_request(url, method)
    return result


def get_payroll_employee_settings(payroll_id, employee_id): # pragma: no cover
    """Get payroll employee information"""
    url = config.Env.PAYROLL_API_GET_PRGROUP_EMPLOYEE_SETTINGS.format(
        payroll_id=payroll_id,
        employee_id=employee_id
    )
    result = _exec_request(url, 'GET')
    return result


def get_final_payroll_employee_settings(payroll_id, employee_id): # pragma: no cover
    """Get payroll employee information"""
    url = config.Env.PAYROLL_API_GET_FINAL_PAYROLL_EMPLOYEE_SETTINGS.format(
        payroll_id=payroll_id,
        employee_id=employee_id
    )
    result = _exec_request(url, 'GET')
    return result


def get_employee_attendance(attendance_job_id, employee_id): # pragma: no cover
    """Get employee computed attendance from job API"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["get-job-result"]
    url = "{base}{path}".format(base=base_path, path=path)
    job_result_id = "{}:{}".format(attendance_job_id, employee_id)
    result = _exec_request(url.format(job_result_id=job_result_id), method)
    return result


def get_contribution_record(employee_id, posting_date): # pragma: no cover
    """Get contribution record"""
    url = config.Env.CONTRIBUTION_RECORD_URL.format(
        employee_id=employee_id,
        payroll_posting_date=posting_date
    )
    result = _exec_request(url, 'GET')
    return result


def save_payroll_item(item):  # pragma: no cover
    """Save payroll item API call"""
    url = config.Env.SAVE_PAYROLL_ITEM_URL
    result = _exec_request(url, 'POST', data=item)
    return result


def create_payslip(payload):  # pragma: no cover
    """Create payslip API call"""
    url = config.Env.CREATE_PAYSLIP_URL
    result = _exec_request(url, 'POST', data=payload)
    return result


def set_payroll_status(payroll_id, status): # pragma: no cover
    """Set payroll status"""
    url = config.Env.SET_PAYROLL_STATUS_URL.format(
        payroll_id=payroll_id
    )
    payload = {
        "status": status
    }
    result = _exec_request(url, 'PATCH', data=payload)
    return result


def revert_payroll_status(payroll_id): # pragma: no cover
    """Revert payroll status"""
    url = config.Env.REVERT_PAYROLL_STATUS_URL.format(
        payroll_id=payroll_id
    )
    result = _exec_request(url, 'POST')
    return result


def email(payload): # pragma: no cover
    """Call Email endpoint"""
    url = config.Env.PAYROLL_API_EMAIL
    data = {
        "data": {
            "type": "email",
            "attributes": payload
        }
    }
    result = _exec_request(url, "POST", data=data)
    return result


def create_payroll_register(payroll_id): # pragma: no cover
    """Create payroll register"""
    url = config.Env.PAYROLL_API_CREATE_PAYROLL_REGISTER.format(
        payroll_id=payroll_id)
    result = _exec_request(url, "POST")
    return result


def create_activity(userids, activity_type, params): # pragma: no cover
    """Post to notifications api new activity"""
    url = config.Env.NOTIFICATION_API_CREATE_ACTIVITY
    payload = {
        "owner_id": 0,  # System generated
        "skip_email_notification": True,
        "type": activity_type,
        "affected_users": userids,
        "params": json.dumps(params)
    }

    result = _exec_request(url, "POST", data=payload)
    return result


def get_userids(company_id, user_emails):
    """Get user ids given email"""
    url = config.Env.GET_USERIDS_USING_EMAILS_URL.format(company_id=company_id)
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    payload = {
        "values": ",".join(user_emails)
    }
    result = _exec_request(url, "POST", headers=headers, form_data=payload)
    data = result.get("data", [])
    return [x.get("id") for x in data if x.get("id")]


def trigger_download_multiple(payroll_id, created_by):
    """Trigger download multiple payslips endpoint"""
    url = config.Env.PAYROLL_API_DOWNLOAD_MULTIPLE_PAYSLIPS
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    payload = {
        "payroll_id": payroll_id,
        "user_id": created_by
    }
    result = _exec_request(url, "POST", headers=headers, form_data=payload)
    return result


def payroll_employee_close(payroll_id, employee_id):  # pragma: no cover
    """Payroll employee close"""
    url = config.Env.PAYROLL_API_EMPLOYEE_CLOSE.format(
        payroll_id=payroll_id,
        employee_id=employee_id)
    result = _exec_request(url, "POST")
    return result


def payroll_employee_reopen(payroll_id, employee_id):  # pragma: no cover
    """Payroll employee reopen"""
    url = config.Env.PAYROLL_API_EMPLOYEE_REOPEN.format(
        payroll_id=payroll_id, employee_id=employee_id
    )
    result = _exec_request(url, "POST")
    return result


def payroll_reopen(payroll_id):  # pragma: no cover
    """Payroll reopen"""
    url = config.Env.PAYROLL_API_REOPEN.format(payroll_id=payroll_id)
    result = _exec_request(url, "POST")
    return result


def get_company_other_income_types(company_id, other_income_type):
    """API call for getting the other income types"""
    url = config.Env.PAYROLL_API_GET_OTHER_INCOME_TYPE.format(company_id=company_id,
                                                              other_income_type=other_income_type)
    result = _exec_request(url, "GET")
    data = result.get("data", [])
    cleaned = [x for x in data if not x.get("deleted_at")]
    return cleaned


def reset_payroll_payslips_sent_at(payroll_id):  # pragma: no cover
    """Reset payroll payslips API call"""
    url = config.Env.PAYROLL_API_CLEAR_PAYSLIP_FLAG.format(
        payroll_id=payroll_id)
    result = _exec_request(url, "POST")
    return result


def bulk_create_income_type(company_id, income_type, payload):  # pragma: no cover
    """Bulk create other income type api call"""
    url = config.Env.PAYROLL_API_BULK_CREATE_OTHER_INCOME.format(
        company_id=company_id, income_type=income_type)
    result = _exec_request(url, 'POST', data=payload)
    return result


def bulk_create_annual_earnings(company_id, payload):  # pragma: no cover
    """Bulk set annual earnings"""
    url = config.Env.PAYROLL_API_BULK_SET_ANNUAL_EARNING.format(
        company_id=company_id
    )
    result = _exec_request(url, 'POST', data=payload)
    return result

def validate_loan_item_preview(params):
    """Call loan item preview save"""
    url = config.Env.PAYROLL_LOAN_API_VALIDATE_LOAN_PREVIEW
    result = _exec_request(url, 'POST', form_data=params)
    # return the "uid" only
    return result


def save_loan_item(params):  # pragma: no cover
    """Save loan item to db -- must be called after preview call"""
    params['no_validate'] = "true"
    url = config.Env.PAYROLL_LOAN_API_SAVE_LOAN_ITEM
    result = _exec_request(url, 'POST', form_data=params)
    #return the "uid" only
    logger.debug(result)
    return result


def get_company_loan_types(company_id):  # pragma: no cover
    """Get all company loan types"""
    url = config.Env.PAYROLL_LOAN_GET_COMPANY_LOAN_TYPES.format(
        company_id=company_id)
    result = _exec_request(url, 'GET')
    return result.get("data", [])


def get_special_payroll_settings(prid, for_projection = False, employee_id = None):  # pragma: no cover
    """API call to get payroll settings given id"""
    api_path = config.Env.PAYROLL_API_GET_SPECIAL_PAYROLL_SETTINGS.format(
        payroll_id=prid)

    if for_projection:
        api_path = api_path + '&for_projection=1'

    if employee_id:
        api_path = api_path + ('&employee_id={employee_id}'.format(employee_id=employee_id))

    result = _exec_request(api_path, 'GET')
    return result


def get_special_payroll_employee_settings(payroll_id, employee_id):  # pragma: no cover
    """Get payroll employee information"""
    url = config.Env.PAYROLL_API_GET_SPECIAL_PAYROLL_EMPLOYEE_SETTINGS.format(
        payroll_id=payroll_id,
        employee_id=employee_id
    )
    result = _exec_request(url, 'GET')
    return result

def save_projected_final_pay_item(item):  # pragma: no cover
    """Save projected Final Pay"""
    url = config.Env.SAVE_PROJECTED_FINAL_PAY_ITEM
    result = _exec_request(url, 'POST', data=item)
    return result

def sync_payroll_disburesments(payload):
    url = config.Env.SALPAY_PAYROLL_DISBURSEMENTS_URL
    result = _exec_request(url, 'POST', data=payload)
    return result

def get_subscriptions():
    url = config.Env.SB_SUBCRIPTION_CUSTOMERS_URL
    result = _exec_request(url, 'GET')
    return result

def email_masterfile(email, firstName, url): # pragma: no cover
    """Call Email endpoint"""
    api_path = config.Env.NOTIFICATION_API_EMAIL
    payload = {
        "email": email,
        "firstName": firstName,
        "url": url
    }

    result = _exec_request(api_path, 'POST', data=payload)
    return result

def get_job_result(job_id, employee_id):
    """Get job-result from job API"""
    base_path = config.Env.JOBS_API_BASE_URL
    path, method = config.Env.JOBS_API_URLS["get-job-result"]
    url = "{base}{path}".format(base=base_path, path=path)
    job_result_id = "{}:{}".format(job_id, employee_id)
    result = _exec_request(url.format(job_result_id=job_result_id), method)
    return result
