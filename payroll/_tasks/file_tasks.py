"""File related tasks"""
import os
import os.path
import shutil
import tempfile
import zipfile

import boto3

import payroll._tasks.taskutils as taskutils
from payroll import apicalls, filevalidators
from core import exceptions, logger, fileutils
from core.app import app

logger = logger.get_logger()

@app.task(**taskutils.task_params("zip_files"))
def zip_files(params):
    """Zip files task"""
    # create boto3 downloader
    modified_params = dict(params)
    job_id = params.get("jobId")
    s3 = boto3.client('s3')
    s3_bucket = params.get("sourceBucket")
    zip_bucket = params.get("targetBucket")
    files = params.get("files", [])
    # get payslips
    tmp_dir = tempfile.gettempdir()
    tmp_subdir = str(job_id).replace('-', '').replace(':', '')
    workdir = "{dir}/{subdir}".format(dir=tmp_dir, subdir=tmp_subdir)
    try:

        if not os.path.exists(workdir):
            os.mkdir(workdir)
        file_track = {}
        file_ids = []
        for f in files:
            tmp_file = "{workdir}/{pdf_file}".format(
                workdir=workdir, pdf_file=f.get("displayName"))
            s3.download_file(s3_bucket, f.get("fileUri"), tmp_file)
            file_track[tmp_file] = f.get("displayName")
            file_ids.append(f.get("id"))

        # create zip file
        zipFileName = "{workdir}/{zip_file}".format(
            workdir=workdir, zip_file=params.get("targetFileKey"))
        #check zip dir path
        zipDirPath = zipFileName[:zipFileName.rfind('/')]
        if not os.path.exists(zipDirPath):
            os.makedirs(zipDirPath)

        zf = zipfile.ZipFile(zipFileName, mode='w')

        for fileloc, arcname in file_track.items():
            zf.write(fileloc, arcname=arcname)
        zf.close()
        modified_params["zippedFilesCount"] = len(file_track)
        modified_params["fileIds"] = file_ids
        s3.upload_file(zipFileName, zip_bucket, params.get("targetFileKey"))
        # cleanup then send trigger email
        shutil.rmtree(workdir, ignore_errors=True)
    except Exception as exc:
        raise exceptions.PeerError(str(exc))

    return modified_params

@app.task(**taskutils.task_params("trigger_email_file_link"))
def trigger_email_file_link(params):
    """Trigger email task"""
    email_params = dict(params)
    new_params = {
        "name": params.get("emailType"),
        "uri": params.get("targetFileKey"),
        "firstName": params.get("userFirstName")
    }
    email_params.update(new_params)
    del email_params["emailType"]

    apicalls.email(email_params)
    # update job_details
    taskutils.result(params.get("jobId"), {"triggered": True}, "email")

#task chains
@app.task(**taskutils.task_params("get_company_other_incomes"))
def get_company_other_incomes(params):
    """Get the list of other income types"""
    other_income_type = params.get("otherIncomeType")
    companyId = params.get("companyId")
    data = apicalls.get_company_other_income_types(companyId, other_income_type)
    params["typeList"] = data
    return params

@app.task(**taskutils.task_params("validate_other_income_upload"))
def validate_other_income_upload(params):
    """Get the file"""
    task_name = params.get("name")
    bucket = params.get("fileBucket")
    index = params.get("fileKey")
    content = ""
    filebytes = None
    job_id = params.get("jobId")

    try:
        filebytes = fileutils.download_file(bucket, index)
    except:
        raise exceptions.PeerError(
            "Error downloading file from source %s/%s" % (bucket, index)
        )

    content = fileutils.decode(filebytes)
    del filebytes
    data = str(content).splitlines()
    if len(data) <= 1:
        raise ValueError("No record found on template")

    context = dict(params)
    processor = filevalidators.get_processor(task_name, context=context)
    # set default job count
    if processor:
        processor.iterate_source(data)
        if processor.done():
            #logger.debug("Processor result: " + str(processor.result))
            if processor.has_errors():
                taskutils.error(job_id, processor.errors)
            else:
                params["result"] = processor.result
                save_other_income_items.apply_async(args=(params,), queue='payroll')

    else:
        raise ValueError("No file processors registered for task.")


@app.task(**taskutils.task_params("save_other_income_items"))
def save_other_income_items(params):
    """Save the result"""
    other_income_type = str(params.get("otherIncomeType"))
    income_type = other_income_type
    if other_income_type.endswith('_type'):
        income_type = other_income_type.split('_')[0]
    apicalls.bulk_create_income_type(params.get("companyId"), income_type, params.get("result"))
    job_id = params.get("jobId")
    taskutils.job_update(job_id, None, 1, 1)


@app.task(**taskutils.task_params("validate_annual_earning_upload"))
def validate_annual_earning_upload(params):
    """Validate annual earning upload file"""
    task_name = params.get("name")
    bucket = params.get("fileBucket")
    index = params.get("fileKey")
    content = ""
    filebytes = None
    job_id = params.get("jobId")

    try:
        filebytes = fileutils.download_file(bucket, index)
    except:
        raise exceptions.PeerError(
            "Error downloading file from source %s/%s" % (bucket, index)
        )

    content = fileutils.decode(filebytes)
    del filebytes
    data = str(content).splitlines()
    if len(data) <= 1:
        raise ValueError("No record found on template")

    context = dict(params)
    processor = filevalidators.get_processor(task_name, context=context)
    # set default job count
    if processor:
        processor.iterate_source(data)
        if processor.done():
            #logger.debug("Processor result: " + str(processor.result))
            if processor.has_errors():
                taskutils.error(job_id, processor.errors)
            else:
                params["result"] = processor.result
                save_annual_earning_items.apply_async(args=(params,), queue='payroll')

    else:
        raise ValueError("No file processors registered for task.")


@app.task(**taskutils.task_params(" save_annual_earning_item"))
def save_annual_earning_items(params):
    """Save the results"""
    data = apicalls.bulk_create_annual_earnings(params.get("companyId"), params.get("result"))
    job_id = params.get("jobId")
    taskutils.result(job_id, data, None)
