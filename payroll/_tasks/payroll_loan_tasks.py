"""Loan upload tasks"""
from celery import chain

from core import exceptions, fileutils, logger, authz
from core.app import app
from payroll import apicalls, filevalidators

import payroll._tasks.taskutils as taskutils
import payroll._tasks.payroll_tasks as pr_tasks

logger = logger.get_logger()


#task chainers
def process_employee_loans(params): # pragma: no cover
    """Workflow for government loans processing"""
    params["activeOnly"] = True
    chain(
        pr_tasks.get_company_employees.s(params),
        pr_tasks.get_company_payroll_groups.s(),
        get_company_loan_types.s(),
        validate_loans_upload.s(),
    ).apply_async(queue='payroll')



@app.task(**taskutils.task_params("validate_loans_upload"))
def validate_loans_upload(params):
    """File loans validator"""
    task_name = params.get("name")
    bucket = params.get("fileBucket")
    index = params.get("fileKey")
    content = ""
    filebytes = None
    job_id = params.get("jobId")

    employees_related_ids = params.get("employeesRelatedIds")
    authz_data_scope = params.get("authzDataScope")

    try:
        filebytes = fileutils.download_file(bucket, index)
    except:
        raise exceptions.PeerError(
            "Error downloading file from source %s/%s" % (bucket, index)
        )

    content = fileutils.decode(filebytes)
    del filebytes
    data = str(content).splitlines()
    if len(data) <= 1:
        raise ValueError("No record found on template")

    context = dict(params)
    processor = filevalidators.get_processor(task_name, context=context)
    if processor:
        processor.iterate_source(data)
        if processor.done():
            #logger.debug("Processor result: " + str(processor.result))
            if processor.has_errors():
                taskutils.error(job_id, processor.errors)
            else:  #try to save each item
                if authz_data_scope:
                    unauthorized_employees = []

                    for loan in processor.result:
                        employee = employees_related_ids.get(str(loan.get('employee_id')))
                        is_authorized = authz.is_employee_authorized(authz_data_scope, employee)

                        if not is_authorized:
                            unauthorized_employees.append(employee.get('employee_id'))

                    if unauthorized_employees:
                        employee_list = ', '.join(unauthorized_employees)

                        raise ValueError(
                            'No access to the following employees: %s' % (employee_list,))

                entries = len(processor.result)
                taskutils.job_update(job_id, None, entries, None)

                for loan in processor.result:
                    emp_params = dict(params)
                    emp_params["result"] = loan
                    save_loan_item.apply_async(args=(emp_params,), queue='payroll')

    else:
        raise ValueError("No file processors registered for task.")

@app.task(**taskutils.task_params("save_loan_item"))
def save_loan_item(params): # pragma: no cover
    """Save loan task"""
    result = params.get("result")
    apicalls.save_loan_item(result)
    taskutils.job_update(params.get("jobId"), None, None, "+1")

@app.task(**taskutils.task_params("get_company_loan_types"))
def get_company_loan_types(params): # pragma: no cover
    """Get loan types"""
    company_id = params.get("companyId")
    loan_types = apicalls.get_company_loan_types(company_id)
    params["typeList"] = loan_types
    return params
