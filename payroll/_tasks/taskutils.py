"""
Task utilities, job/error/result updating
"""
#pylint: disable=abstract-method
from celery import Task

from core import logger, exceptions
from core.app import app
from payroll import apicalls, constants
logger = logger.get_logger()


class PayrollTask(Task): # pragma: no cover
    """Superclass of all Payroll related tasks/steps"""

    def on_failure(self, exc, task_id, args, kwargs, einfo=None):
        """Override failure reporting func"""
        if isinstance(args[0], dict):  # first param is a standard dict task param
            params = args[0]
            # is there an employeeUID?
            error_id = None
            if params:
                error_id = params.get("employeeUID") or params.get("employeeId")
            job_id = params.get("jobId")

            if params.get("name") == constants.WORKFLOW_COMPUTE_PAYROLL:
                error(
                    job_id, {"name": "task-errors", "errors": [str(exc)]}, error_id,
                    on_finished_task="payroll.tasks.open_payroll",
                    on_finished_task_args=[
                        {"jobId": job_id,
                         "payrollId": params.get("payrollId"),
                         "originalJobId": params.get("originalJobId"),
                         "doNotOpen": True},
                    ]
                )
            else:
                error(job_id, {"name": "task-errors", "errors": [str(exc)]}, error_id)


class EventReportingTask(Task): # pragma: no cover
    """REsult or Error reporting tasks superclass"""

    def on_failure(self, exc, task_id, args, kwargs, einfo=None):
        if isinstance(args[0], dict):
            params = args[0]
            job_id = params.get("jobId")
            logger.error(
                "Event Reporting failed for jobId %s - %s", job_id, str(exc))
        else:
            logger.error(
                "Event Reporting failed for taskId %s - %s", task_id, str(exc))


class PayrollEventReportingTask(Task): # pragma: no cover
    """Payroll Event Reportin Task superclass, calls revert on failure"""
    def on_failure(self, exc, task_id, args, kwargs, einfo=None):
        if isinstance(args[0], dict):
            params = args[0]
            job_id = params.get("jobId")
            logger.error(
                "Event Reporting failed for jobId %s - %s", job_id, str(exc))
            payroll_id = params.get("payrollId")
            if payroll_id:
                try:
                    apicalls.revert_payroll_status(payroll_id)
                except: #pylint: disable=bare-except
                    logger.error(
                        "Failed reverting payroll status of payroll %d", payroll_id)
        else:
            logger.error(
                "Event Reporting failed for taskId %s - %s", task_id, str(exc))


def task_params(func_name, group='payroll', superclass=PayrollTask, **kwargs):
    """Return task standard params"""
    task_name = '{group}.tasks.{fname}'.format(group=group, fname=func_name)
    params = dict(
        name=task_name,
        serializer="json",
        ignore_result=True,
        base=superclass,
        autoretry_for=(exceptions.PeerError,),
        default_retry_delay=5,
        exponential_backoff=2,
        retry_kwargs={"max_retries": 3},
        retry_jitter=True,
    )

    params.update(kwargs)
    return params


@app.task(**task_params("add_job_error", superclass=EventReportingTask))
def add_job_error(job_id, description, error_id=None, **kwargs):
    """Report job error to Jobs management API"""
    #logger.debug("job_id %s", job_id)
    #logger.debug("job error: %s", json.dumps(description))
    response = apicalls.add_job_error(job_id, description, error_id)
    # logger.info("Submitted Job Error for Job ID: %s using ErrorId: %s", job_id, error_id)
    # logger.error("Job ID: %s  - Description: %s", job_id, description)
    meta = response.get("meta", {})

    if kwargs.get("on_finished_task") and meta.get("status") == "FINISHED":
        args = None
        if kwargs.get("on_finished_task_args"):
            args = tuple(kwargs.get("on_finished_task_args"))
        app.send_task(kwargs.get("on_finished_task"),
                      args=args, queue='payroll')


@app.task(**task_params("add_job_result", superclass=EventReportingTask))
def add_job_result(job_id, res, result_id, **kwargs):
    logger.info(str({
        "task": "taskutils.add_job_result",
        "job_id": str(job_id),
        "res": str(res),
        "on_finished_task": str(kwargs.get("on_finished_task", None)),
        "on_finished_task_args": str(kwargs.get("on_finished_task_args", None)),
    }))
    """Report result to Jobs Management API"""
    response = apicalls.add_job_result_with_redis(job_id, res, result_id)
    # logger.info("Submitted Job result for Job ID: %s using ResultID: %s", job_id, result_id)
    # special check::
    # if this is a compute-payroll
    meta = response.get("meta", {})

    if kwargs.get("on_finished_task") and meta.get("status") == "FINISHED":
        args = None
        if kwargs.get("on_finished_task_args"):
            args = tuple(kwargs.get("on_finished_task_args"))
        app.send_task(kwargs.get("on_finished_task"),
                      args=args, queue='payroll')


@app.task(**task_params("update_job", superclass=EventReportingTask))
def update_job(job_id, status=None, tasks_count=None, tasks_done=None, **kwargs):
    """Update job status in JMAPI"""
    res = apicalls.update_job(job_id, status, tasks_count, tasks_done)
    # logger.info("Updated Job with ID: %s", job_id)
    status = res.get("data", {}).get("attributes", {}).get("status")

    if kwargs.get("on_finished_task") and status == "FINISHED":
        args = None
        if kwargs.get("on_finished_task_args"):
            args = tuple(kwargs.get("on_finished_task_args"))
        app.send_task(kwargs.get("on_finished_task"),
                      args=args, queue='payroll')

@app.task(**task_params("delete_job_error_items_by_job_id", superclass=EventReportingTask))
def delete_job_error_items_by_job_id(job_id, **kwargs):
    apicalls.delete_job_error_items_by_job_id(job_id)
    # logger.info("Delete Job Error Items with Job ID: %s", job_id)

@app.task(**task_params("delete_job_result_items_by_job_id", superclass=EventReportingTask))
def delete_job_result_items_by_job_id(job_id, **kwargs):
    apicalls.delete_job_result_items_by_job_id(job_id)
    # logger.info("Delete Job Result Items with Job ID: %s", job_id)

def error(job_id, description, error_id=None, **kwargs):
    """Spawn job error reporting task"""
    add_job_error.apply_async(args=(job_id, description, error_id), kwargs=kwargs, queue='payroll')

def result(job_id, res, result_id, **kwargs):
    """Spawn job result reporting task"""
    logger.info(str({
        "task": "taskutils.result",
        "job_id": str(job_id),
        "res": str(res),
        "on_finished_task": str(kwargs.get("on_finished_task", None)),
        "on_finished_task_args": str(kwargs.get("on_finished_task_args", None)),
    }))
    add_job_result.apply_async(args=(job_id, res, result_id), kwargs=kwargs, queue='payroll')

def job_update(job_id, status=None, tasks_count=None, tasks_done=None, **kwargs):
    """Spawn update job task"""
    update_job.apply_async(args=(job_id, status, tasks_count, tasks_done),
                           kwargs=kwargs,
                           queue='payroll')

def job_reset(job_id, **kwargs):
    """Spawn reset job tasks"""
    delete_job_error_items_by_job_id.apply_async(args=(job_id,), kwargs=kwargs, queue='payroll')
    delete_job_result_items_by_job_id.apply_async(args=(job_id,), kwargs=kwargs, queue='payroll')
