from core.app import app
from payroll._tasks.taskutils import task_params, job_update, result
from payroll import apicalls
from datetime import datetime
from celery import chain

from core import exceptions, logger
logger = logger.get_logger()

def sync_disbursement_workflow(params):
    chain(
        get_all_accounts_task.s(params),
        sync_payroll_disbursement_task.s()
    ).apply_async(queue='payroll')

@app.task(**task_params("get_all_accounts_task"))
def get_all_accounts_task(params):
    response = apicalls.get_subscriptions()
    subscriptions = response.get('data', [])
    start_date = None
    end_date = None
    for account in subscriptions:
        subscription = account.get('subscriptions', [])
        if len(subscription):
            subscription_start_date = datetime.strptime(subscription[0].get('start_date'), '%Y-%m-%d')
            subscription_end_date = datetime.strptime(subscription[0].get('end_date'), '%Y-%m-%d')
            if start_date is None:
                start_date = subscription_start_date
            elif subscription_start_date < start_date:
                start_date = subscription_start_date
            if end_date is None:
                end_date = subscription_end_date
            elif subscription_end_date > end_date:
                end_date = subscription_end_date

    params['start_date'] = start_date.strftime('%Y-%m-%d')
    params['end_date'] = end_date.strftime('%Y-%m-%d')

    return params

@app.task(**task_params("sync_payroll_disbursement"))
def sync_payroll_disbursement_task(params):
    logger.info(params)
    job_id = params.get('jobId')
    job_update(job_id, status='PROCESSING')
    payload = params
    while payload is not None:
        try:
            response = apicalls.sync_payroll_disburesments(payload)
        except:
            logger.error("Unable to fetch payroll disbursements")
            logger.debug(payload)
            response = {}
        synced_data = response.get('synced_data', [])
        for disbursement in synced_data:
            job_update(job_id, tasks_count='+1')
            result(job_id, res=disbursement, result_id=disbursement.get('id'))
        next_payload = response.get('next', None)

        payload = next_payload

    job_update(job_id, status='FINISHED', tasks_done='+1')

