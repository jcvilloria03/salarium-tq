"""Tasks"""
import os
import os.path
import tempfile

import boto3
import pdfkit
from celery import chain


import payroll._tasks.file_tasks as file_tasks
import payroll._tasks.taskutils as taskutils
from payroll import apicalls, config
from core import exceptions, logger
from core.app import app


logger = logger.get_logger()

@app.task(**taskutils.task_params('generate_payslips', group='payslip'))
def generate_payslips(params):
    """Generate payslips for payroll"""
    # get the payroll employees
    job_id = params.get("jobId")
    payroll_id = params.get("payrollId")

    result = apicalls.get_payroll_settings_generic(payroll_id)
    data = result.get("data", [])
    attributes = data.get("attributes")
    ids = attributes.get("employeeIds", [])

    if len(ids) > 1:
        taskutils.job_update(job_id, None, len(ids), None)

    for item in ids:
        emp_param = dict(params)
        emp_param['employeeId'] = item
        save_employee_payslip(emp_param)


@app.task(**taskutils.task_params('generate_employee_payslip', group='payslip'))
def generate_employee_payslip(params):
    """Generate payslip per employee"""
    job_id = params.get("jobId")
    employee_id = params.get("employeeId")
    payroll_id = params.get("payrollId")
    company_id = params.get("companyId")

    url = config.Env.PREVIEW_PAYSLIP_URL.format(
        payroll_id=payroll_id, employee_id=employee_id)
    #logger.debug("url :" + url)
    raw_data = apicalls.get_raw_data(url)

    logo_data = params.get("logo", {})
    placeholder = "<!-- logo-placeholder -->"
    if logo_data and placeholder in raw_data:
        img_tag = "<img id='logo' src='data:{mime};base64, {img_data}'>".format(
            mime=logo_data.get("contentType"),
            img_data=logo_data.get("data", '')
        )

        raw_data = raw_data.replace(placeholder, img_tag)
        #logger.debug(raw_data)

    path = 'company_{company_id}/{payroll_id}/payslip-{employee_id}.pdf'.format(
        company_id=company_id,
        payroll_id=payroll_id,
        employee_id=employee_id
    )

    tmp_filename = "{payroll_id}-{job_id}-{employee_id}.pdf".format(payroll_id=payroll_id,
                                                                    job_id=job_id,
                                                                    employee_id=employee_id)
    tmp_dir = tempfile.gettempdir()
    tmp_file = "{dir}/{pdf_file}".format(dir=tmp_dir, pdf_file=tmp_filename)

    s3_bucket = config.Env.PAYSLIP_BUCKET

    has_errors = False
    exception_message = None

    # for logging purposes
    job_task_params = {
        "name": params.get("name"),
        "jobId": params.get("jobId"),
        "employeeId": params.get("employeeId"),
        "startDate": params.get("startDate"),
        "endDate": params.get("endDate"),
        "companyId": params.get("companyId"),
        "payrollGroupId": params.get("payrollGroupId"),
        "payrollId": params.get("payrollId"),
        "createdBy": params.get("createdBy"),
    }

    job_task_params = str(job_task_params)

    pdf_generated = False
    retries = 0
    retry_limit = 25
    while not pdf_generated:
        retries = retries + 1
        try:
            if retries > 1:
                logger.warning(f"{payroll_id}={job_id}={employee_id} Retrying PDF Generation Attempt #: " + str(retries))
            pdfkit.from_string(raw_data, tmp_file, options={'quiet': '', 'encoding': "UTF-8"})
        except OSError as e:
            logger.warning(exception_message)
            logger.warning(f"{payroll_id}={job_id}={employee_id} PDF generation error:" + exception_message + ". PARAMS: " + job_task_params)
        except Exception as err:
            logger.critical(f"{payroll_id}={job_id}={employee_id} PDF generation error: " + str(err) + ". PARAMS: " + job_task_params)
            logger.critical(str(err))
            has_errors = True
            raise exceptions.PeerError('IO Error: ' + str(err))
        finally:
            if os.path.getsize(tmp_file) > 0 and os.path.exists(tmp_file):
                # upload to s3
                try:
                    s3 = boto3.client('s3')
                    s3.upload_file(tmp_file, s3_bucket, path)
                    params["payslipFileName"] = "{path}".format(path=path)
                    logger.info(f"{payroll_id}={job_id}={employee_id} Successfully uploaded #: " + str(retries))
                    if os.path.exists(tmp_file):
                        os.remove(tmp_file)
                    pdf_generated = True
                except IOError:
                    logger.error(f"{payroll_id}={job_id}={employee_id} IO Error on deleting file." + ". PARAMS: " + job_task_params)
                except Exception as err:
                    logger.error(f"{payroll_id}={job_id}={employee_id} Payslip S3 Upload Error: " + str(err) + ". PARAMS: " + job_task_params)
                    raise exceptions.UnexpectedError('Payslip S3 Upload Error: ' + str(err))

                return params
            else:
                pdf_generated = False

            if retries >= retry_limit:
                params_details = str({
                    'has_errors': has_errors,
                    'exception_message': exception_message,
                })
                logger.error(
                    f"{payroll_id}={job_id}={employee_id} " +
                    "Unhandled payslip generation error: " + params_details + "." +
                    " PARAMS: " + job_task_params
                )
                raise exceptions.UnexpectedError('Unhandled payslip generation error: ' + params_details)

@app.task(**taskutils.task_params("get_company_logo_data", group='payslip'))
def get_company_logo_data(params): # pragma: no cover
    """Get company logo data"""
    company_id = params.get("companyId")
    result = apicalls.get_company_logo(company_id)

    params["logo"] = result
    return params



@app.task(**taskutils.task_params("save_payslip_info", group='payslip'))
def save_payslip_info(params):
    """Save payslip info"""
    employee_id = params.get("employeeId")
    payroll_id = params.get("payrollId")
    job_id = params.get("jobId")

    job_task_params = {
        "name": params.get("name"),
        "jobId": params.get("jobId"),
        "employeeId": params.get("employeeId"),
        "startDate": params.get("startDate"),
        "endDate": params.get("endDate"),
        "companyId": params.get("companyId"),
        "payrollGroupId": params.get("payrollGroupId"),
        "payrollId": params.get("payrollId"),
        "createdBy": params.get("createdBy"),
    }

    job_task_params = str(job_task_params)

    logger.info(f"{payroll_id}={job_id}={employee_id} save_payslip_info task params: " + job_task_params)

    path = params.get("payslipFileName")
    created_by = params.get("createdBy", 0)

    payload = {
        "data": {
            "type": "payslips",
            "attributes": {
                "payrollId": int(payroll_id),
                "employeeId": int(employee_id),
                "uri": path,
                "createdBy": int(created_by)
            }
        }
    }

    logger.info(
        f"{payroll_id}={job_id}={employee_id} " +
        "save_payslip_info apicalls.create_payslip payload: " + str(payload)
    )

    apicalls.create_payslip(payload)
    taskutils.result(
        job_id, {"url": path}, str(employee_id),
        on_finished_task="payslip.tasks.clear_payslip_sent_at",
        on_finished_task_args=[{"jobId": job_id, "payrollId": payroll_id}]
    )

@app.task(**taskutils.task_params("clear_payslip_sent_at",
                                  group='payslip',
                                  superclass=taskutils.PayrollEventReportingTask))
def clear_payslip_sent_at(params):
    """Clear payslip sent at"""
    payroll_id = params.get("payrollId")
    apicalls.reset_payroll_payslips_sent_at(payroll_id)

@app.task(**taskutils.task_params("trigger_download_multiple_payslips",
                                  group='payslip',
                                  superclass=taskutils.PayrollEventReportingTask))
def trigger_download_multiple_payslips(params):
    """Callback for triggering zip/email multiple payslip"""
    logger.debug("Triggering multiple payslips zip creation and email")
    job = apicalls.get_job(params.get("jobId"))
    relationships = job.get("data", {}).get("relationships", {})
    errors = relationships.get("errors") or {}
    if not errors.get("data"):
        payroll_id = params.get("payrollId")
        created_by = params.get("createdBy")
        apicalls.trigger_download_multiple(payroll_id, created_by)
    else:
        logger.info("Download multiple payslips not triggered")

def save_employee_payslip(params): # pragma: no cover
    """Save employee payslip chain"""
    chain(
        generate_employee_payslip.s(params),
        save_payslip_info.s(),
    ).apply_async(queue='payroll')


def generate_payroll_payslips(params): # pragma: no cover
    """Generate payrol payslips chain"""
    chain(
        get_company_logo_data.s(params),
        generate_payslips.s()
    ).apply_async(queue='payroll')


def email_zipped_payslips(params): # pragma: no cover
    """Email zipped payslips task chain"""
    chain(
        file_tasks.zip_files.s(params),
        file_tasks.trigger_email_file_link.s()
    ).apply_async(queue='payroll')



@app.task(**taskutils.task_params('get_payrollgroup_emails', group='payslip'))
def get_payrollgroup_emails(params):
    """Generate payslips for payroll"""
    # get the payroll employees
    #logger.debug(params)
    job_id = params.get("jobId")
    company_id = params.get("companyId")
    payroll_group_id = params.get("payrollGroupId")
    start_date = params.get("startDate")
    end_date = params.get("endDate")

    employeeIds = params.get("employeeIds")

    if payroll_group_id and start_date and end_date:
        result = apicalls.get_payroll_group_employees(
            company_id,
            payroll_group_id,
            start_date,
            end_date,
            include=["id", "email", "first_name"],
        )
    else:
        result = apicalls.get_employees_by_ids(
            company_id,
            employeeIds
        )

    employee_ids_to_notify_ess = params.get("employeeIdsToNotifyEss")
    notify_ess = len(employee_ids_to_notify_ess) > 0

    #filtersave
    data = [x for x in result.get("data", []) if x.get("id") in employeeIds]
    task_count = len(data)

    if notify_ess:
        task_count = task_count + 1

    taskutils.job_update(job_id, None, task_count, None)

    for item in data:
        emp_param = dict(params)
        emp_param['employeeId'] = item.get("id")
        emp_param['email'] = item.get("email")
        emp_param['firstName'] = item.get("first_name")
        email_employee_payslip.apply_async(args=(emp_param,), queue='payroll')

    if notify_ess:
        employees_to_notify = filter(
            lambda x: x.get("id") in employee_ids_to_notify_ess, result.get("data", [])
        )

        emails_to_notify = map(lambda x: x.get("email"), employees_to_notify)
        params["userEmails"] = list(emails_to_notify)

        create_payslips_activity.apply_async(args=(params,), queue='payroll')


@app.task(**taskutils.task_params('email_employee_payslip', group='payslip'))
def email_employee_payslip(params):
    """Email task"""
    email = params.get("email")
    payroll_date = params.get("payrollDate")
    email_params = {
        "name": params.get("emailType") or "email-payslip",
        "payrollDate": payroll_date,
        "firstName": params.get("firstName"),
        "email": email
    }
    apicalls.email(email_params)
    taskutils.job_update(params.get("jobId"), None, None, "+1")


@app.task(**taskutils.task_params('create_payslips_activity', group='payslip'))
def create_payslips_activity(params):
    """Create ESS notification"""
    emails = params.get("userEmails")
    payroll_date = params.get("payrollDate")
    company_id = params.get("companyId")
    userids = apicalls.get_userids(company_id, emails)
    #create activity
    apicalls.create_activity(
        userids,
        "payslips.new",
        {
            "message": "Payslip available",
            "payrollDate": payroll_date
        }
    )
    taskutils.job_update(params.get("jobId"), None, None, "+1")

def send_payslips(params): # pragma: no cover
    """Send payslips task chain.  Params contains:
        * name:  (Constant) 'send-payslips'
        * payrollGroupId: (int) The Payroll group id
        * companyId: (int) Company Id
        * startDate: (str) YYYY-MM-DD (Payroll info)
        * endDate: (str) YYYY-MM-DD (Payroll info)
        * payrollDate: (str) YYYY-MM-DD (Payroll info)
        * employeeIds: ([int]) Array containing target employeeIds
        * emailType: (Constant) 'email-payslip'  - Email type in PR-API email endpoint
        * notifyEss: (bool, default True) Flag to trigger ESS notification

    Parameters:
        :param params: Dictionary of parameters

    """
    chain(
        get_payrollgroup_emails.s(params)
    ).apply_async(queue='payroll')
