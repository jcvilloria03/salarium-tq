"""Tasks"""
import datetime

from celery import chain
import json
import uuid

import salarium_payroll
from core import exceptions, fileutils, logger
from core.app import app
from payroll import datautil, apicalls, filevalidators, constants
from attendance import calls

import payroll._tasks.file_tasks as file_tasks
import payroll._tasks.taskutils as taskutils

logger = logger.get_logger()


@app.task(**taskutils.task_params('get_payroll_group_employees_min_data'))
def get_payroll_group_employees_min_data(params):
    """Get the lookup for company-employees"""
    payroll_id = params.get("payrollId")
    result = apicalls.get_payroll_settings_generic(
        prid=payroll_id,
        with_employees=True
    )

    data = result.get("data", {})
    attributes = data.get("attributes", {})
    employees = attributes.get("employees", [])

    lookup = {}  # dict with employee_company_id as the key
    for item in employees:
        lookup[item.get("employee_id")] = (
            item.get("id"),
            item.get("first_name"),
            item.get("middle_name"),
            item.get("last_name"),
            item.get("active") in ('active', True),
        )

    params["payrollGroupEmployees"] = lookup

    return params


@app.task(**taskutils.task_params("get_company_day_hour_rates"))
def get_company_day_hour_rates(params):
    """Task that gets company_day_hour_rates"""
    company_id = params.get("companyId")
    dhlist_data = apicalls.get_company_day_hour_rates(company_id)
    dhlist = dhlist_data.get("data", [])
    params["dayHourTypes"] = None
    if dhlist:
        abbrevs = []
        for item in dhlist:
            abbrev = str(item.get("abbreviation"))
            if "leave" in abbrev:
                abbrev = abbrev.replace("_", " ")
            abbrevs.append(abbrev)
        params["dayHourTypes"] = abbrevs
    return params


@app.task(**taskutils.task_params("validate_payroll_related_file"))
def validate_payroll_related_file(params):
    """Get the file"""
    task_name = params.get("name")
    bucket = params.get("fileBucket")
    index = params.get("fileKey")
    content = ""
    filebytes = None
    job_id = params.get("jobId")

    try:
        filebytes = fileutils.download_file(bucket, index)
    except:
        raise exceptions.PeerError(
            "Error downloading file from source %s/%s" % (bucket, index)
        )

    content = fileutils.decode(filebytes)
    del filebytes

    data = str(content).splitlines()
    if len(data) <= 1:
        raise ValueError("File has no contents")

    context = dict(params)
    processor = filevalidators.get_processor(task_name, context=context)
    if processor:
        processor.iterate_source(data)
        if processor.done():
            #logger.debug("Processor result: " + str(processor.result))
            total_tasks = 0
            if processor.has_errors():
                total_tasks += 1
            if processor.per_employee_result_saving and processor.result:
                total_tasks += len(processor.result)
            else:
                total_tasks += 1

            if total_tasks > 1:
                taskutils.job_update(job_id, None, total_tasks, None)

            if not params.get("saveResult", True):
                return params

            if processor.result:
                if processor.per_employee_result_saving:
                    for k, v in processor.result.items():
                        taskutils.result(job_id, {k: v}, k)
                else:
                    taskutils.result(job_id, processor.result, None)
            if processor.has_errors():  # this is a dict
                taskutils.error(job_id, processor.errors, 1)
    else:
        raise ValueError("No file processors registered for task.")
    return params


@app.task(**taskutils.task_params("iterate_payroll_employees"))
def iterate_payroll_employees(params):
    """Iterate over all employees and get attendance data for each employee"""
    # update job details taskCount to the number of payroll employees
    job_id = params.get("jobId")
    payroll_type = params.get("payrollType")

    if payroll_type == 'final':
        payroll_id = params.get("payrollId")
        payroll_settings = apicalls.get_payroll_settings_generic(
            prid=payroll_id,
            with_employees=True
        )

        payroll_settings_data = payroll_settings.get("data", {})
        payroll_attributes = payroll_settings_data.get("attributes", {})
        payroll_emps = payroll_attributes.get("employees", [])
        tasks_count = len(payroll_emps)
        taskutils.job_update(job_id, None, tasks_count, 0)

        for emp in payroll_emps:
            emp_data = dict()
            termination_info = emp.get("termination_information", {})

            emp_data["jobId"] = job_id
            emp_data["startDate"] = termination_info.get("start_date_basis")
            emp_data["endDate"] = termination_info.get("last_date")
            emp_data["employeeUID"] = emp.get("id")
            emp_data["employeeID"] = emp.get("employee_id")
            fname = emp.get("first_name")
            mname = emp.get("middle_name", None)
            lname = emp.get("last_name")

            middle = ""
            if mname:
                middle = " " + str(mname)
            emp_data["fullName"] = str(fname) + middle + " " + str(lname)

            get_attendance_service_data.apply_async(
                args=(emp_data,), queue='payroll')
    else:
        payroll_emps = dict(params.get("payrollGroupEmployees"))
        tasks_count = len(payroll_emps)
        taskutils.job_update(job_id, None, tasks_count, 0)
        # setup up params for individual iterations
        data = dict(params)
        del data["payrollGroupEmployees"]
        # iterate through each employee one by one
        for emp_id, value in payroll_emps.items():
            uid, fname, mname, lname, _ = value
            emp_data = dict(data)
            emp_data["employeeUID"] = uid
            emp_data["employeeID"] = emp_id
            middle = ""
            if mname:
                middle = " " + str(mname)
            emp_data["fullName"] = str(fname) + middle + " " + str(lname)
            get_attendance_service_data.apply_async(
                args=(emp_data,), queue='payroll')


@app.task(**taskutils.task_params("get_employee_payroll_loan_items"))
def get_employee_payroll_loan_items(params):
    logger.info(str({
        "task": "get_employee_payroll_loan_items",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "employeeUID": str(params.get("employeeUID")),
        "debug_id": str(params.get("debug_id")),
    }))
    """Task for getting loans and amortizations of employee for payroll"""
    uid = params.get("employeeUID")
    payroll_id = params.get("payrollId")
    loans, amortizations = apicalls.get_employee_payroll_related_loan_items(
        payroll_id, uid)
    params["loansToCredit"] = loans
    params["amortizations"] = amortizations
    return params


@app.task(**taskutils.task_params("get_attendance_service_data"))
def get_attendance_service_data(params):
    logger.info(str({
        "task": "get_attendance_service_data",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "employeeUID": str(params.get("employeeUID")),
        "debug_id": str(params.get("debug_id")),
    }))
    """Task for getting the attendance data of an employee from AS API"""
    job_id = params.get("attendanceJobId") or params.get("jobId")
    start_date = params.get('payrollSettings', {}).get('attendanceStartDate') or params.get("startDate") or \
        params.get("payrollSettings", {}).get("payrollStartDate")
    end_date = params.get('payrollSettings', {}).get('attendanceEndDate') or params.get("endDate") or \
        params.get("payrollSettings", {}).get("payrollEndDate")
    uid = params.get("employeeUID")
    # company_id = params.get("companyId")
    if not start_date or not end_date or not uid:
        raise ValueError('Missing required task params')
    records = apicalls.get_attendance_data(uid, start_date, end_date)
    # parse through each entry and get
    entries = {}
    for item in records:
        converted = {}
        att_date = item.get("date")
        attendance = item.get("hr_types", {})  # type: dict

        total_scheduled_hours = item.get("scheduled_hours")  #this is in minutes
        if not attendance:
            total_scheduled_hours = 0
        #check if the day is RD
        is_rest_day = False
        total_attendance = {}
        for _, list_entry in attendance.items():
            for key, value in list_entry.items():
                if 'RD' in key.split('+'):
                    is_rest_day = True
                cur_value = total_attendance.get(key, 0) or 0
                total_attendance[key] = cur_value + value  #in minutes!
        if is_rest_day:
            total_scheduled_hours = 0

        for key, value in total_attendance.items(): # convert into hours
            converted[str(key).upper().replace(' ', '_')] = float(value)/60.0

        if total_scheduled_hours:
            converted["SCHEDULED_HOURS"] = float(total_scheduled_hours)/60.0
        else:
            converted["SCHEDULED_HOURS"] = 0

        entries[att_date] = converted

    sdate = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    edate = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    all_days = set()
    delta = edate - sdate
    for i in range(delta.days + 1):
        all_days.add((sdate + datetime.timedelta(days=i)).strftime("%Y-%m-%d"))
    # check if all the dates are in
    missing_dates = all_days - set(entries)
    # if missing
    error = {}
    if missing_dates:
        employee_uid = params.get('employeeUID')
        # get employee info
        employee_info = calls._get_employee_info(employee_uid)
        timesheet_required = employee_info.get('time_attendance', {}).get('timesheet_required')
        hours_per_day = employee_info.get('payroll_group').get('hours_per_day')  # returns str, eg:- '8.00'
        scheduled_hours = float(hours_per_day)  # convert str hours to float
        attendance_type = 'ABSENT' if timesheet_required else 'REGULAR'
        for date in missing_dates:
            entries[date] = {
                attendance_type: scheduled_hours,  # get type of attendance
                'SCHEDULED_HOURS': scheduled_hours
            }

    if error:
        taskutils.error(job_id, error, str(uid))
        if params.get("name") == constants.WORKFLOW_COMPUTE_PAYROLL:
            raise ValueError('Incomplete attendance')
    else:
        taskutils.result(job_id, {str(uid): entries}, str(uid))
        if params.get("name") == constants.WORKFLOW_COMPUTE_PAYROLL:
            params['attendanceData'] = entries
            return params
    return None

@app.task(**taskutils.task_params("get_final_payroll_employees_data"))
def get_final_payroll_employees_data(params):
    payroll_id = params.get("payrollId")
    payroll = apicalls.get_special_payroll_settings(payroll_id)
    employees = payroll.get("data", {}).get("attributes", {}).get("employees", [])
    lookup = {}
    for employee in employees:
        termination_info = employee.get("termination_information", {})
        start_date = termination_info.get("start_date_basis", "")
        end_date = termination_info.get("last_date", "")
        if not start_date or not end_date:
            raise ValueError('Missing termination information in data')
        lookup[str(employee.get("employee_id"))] = (
            employee.get("id"),
            employee.get("first_name"),
            employee.get("middle_name"),
            employee.get("last_name"),
            start_date,
            end_date
        )

    params["finalPayrollEmployees"] = lookup
    return params

def validate_attendance_file(params): # pragma: no cover
    """Validate attendance file task"""
    if params.get("payrollType", "regular") == "final":
        #modify name
        params["name"] = constants.WORKFLOW_VALIDATE_FINAL_ATTENDANCE_FILE
        chain(
            get_final_payroll_employees_data.s(params),
            get_company_day_hour_rates.s(),
            validate_payroll_related_file.s(),
        ).apply_async(queue='payroll')
    else:
        chain(
            get_payroll_group_employees_min_data.s(params),
            get_company_day_hour_rates.s(),
            validate_payroll_related_file.s(),
        ).apply_async(queue='payroll')


def validate_payroll_items_file(params): # pragma: no cover
    """Validate payroll items file"""
    chain(
        get_payroll_group_employees_min_data.s(params),
        validate_payroll_related_file.s(),
    ).apply_async(queue='payroll')


def run_payroll(params): # pragma: no cover
    """Run payroll chain"""
    chain(
        get_payroll_settings.s(params), prepare_per_employee_compute.s(),
    ).apply_async(queue='payroll')

def run_special_payroll(params): # pragma: no cover
    """Run special payroll chain"""
    chain(
        prepare_per_employee_special_compute.s(params),
    ).apply_async(queue='payroll')

def run_final_payroll(params): # pragma: no cover
    """Run final payroll chain"""
    chain(
        prepare_per_employee_final_compute.s(params)
    ).apply_async(queue='payroll')

def run_projected_final_pay(params): # pragma: no cover
    """Run Final Payroll"""
    jobId = params.get("jobId")
    parentJobId = params.get("parentJobId")
    jobIdToUse = parentJobId or jobId

    payroll_settings = apicalls.get_special_payroll_settings(
        prid=0,
        for_projection=True,
        employee_id=params.get("employee_id")
    )
    pr_settings = payroll_settings.get("data", {})

    taskutils.job_update(jobIdToUse, None, 1, 0)

    emp_params = dict(params)
    emp_params["originalJobId"] = emp_params["jobId"]
    emp_params["jobId"] = jobIdToUse
    emp_params["employeeUID"] = emp_params["employee_id"]
    emp_params["payrollSettings"] = pr_settings

    compute_projected_final_pay.apply_async(args=(emp_params,), queue='payroll')

    return None


def close_payroll(params): # pragma: no cover
    """Close payroll after effect"""
    params["effect"] = "close"
    chain(
        get_payroll_settings.s(params),
        prepare_per_employee_after_effects.s()
    ).apply_async(queue='payroll')


def reopen_payroll(params): # pragma: no cover
    """Reopen payroll chain"""
    params["effect"] = "reopen"
    chain(
        get_payroll_settings.s(params),
        prepare_per_employee_after_effects.s()
    ).apply_async(queue='payroll')


def get_attendance_data(params): # pragma: no cover
    """Chain task for getting attendance data of payroll group emps"""
    params["activeOnly"] = True
    if params["payrollType"] == 'final':
        chain(
            iterate_payroll_employees.s(params)
        ).apply_async(queue='payroll')
    else:
        chain(
            get_payroll_group_employees_min_data.s(params),
            iterate_payroll_employees.s()
        ).apply_async(queue='payroll')


def process_employee_other_income(params): # pragma: no cover
    """Process employee other income uploads task chainer"""
    task_name = params.get("name")
    other_income_type = str(
        list(set(task_name.split('-')) - {'validate', 'employee', 'file'})[0])
    other_income_type = "{}_type".format(other_income_type)
    params['otherIncomeType'] = other_income_type
    chain(
        get_company_employees.s(params),
        file_tasks.get_company_other_incomes.s(),
        file_tasks.validate_other_income_upload.s(),
    ).apply_async(queue='payroll')

def process_employee_annual_earning(params): # pragma: no cover
    """Annual earning upload workflow"""
    chain(
        get_company_employees.s(params),
        file_tasks.validate_annual_earning_upload.s()
    ).apply_async(queue='payroll')

@app.task(**taskutils.task_params("get_company_employees"))
def get_company_employees(params):
    """Get Company employees"""
    company_id = params.get("companyId")
    if params.get("name") == constants.WORKFLOW_VALIDATE_EMPLOYEE_LOAN_FILE:
        data = apicalls.get_company_employees_with_gov_ids(company_id)
    else:
        data = apicalls.get_company_employees(company_id)
    params["companyEmployees"] = data
    params["employeesRelatedIds"] = apicalls.get_company_employees_related_ids(company_id)
    return params

@app.task(**taskutils.task_params("get_company_payroll_groups"))
def get_company_payroll_groups(params):
    """Get Company employees"""
    company_id = params.get("companyId")
    data = apicalls.get_company_payroll_groups(company_id)
    params["companyPayrollGroups"] = data
    return params

@app.task(**taskutils.task_params("get_payroll_settings"))
def get_payroll_settings(params):
    """Task to get the payroll setting given a payrollId"""
    payroll_id = params.get("payrollId")
    payroll_settings = apicalls.get_payroll_settings_generic(payroll_id)
    logger.info("get_payroll_settings: payroll_id: " + str(payroll_id) + " job_id: " + str(params.get("jobId")))

    data = payroll_settings.get("data", {})
    # get the attributes
    attributes = data.get("attributes")
    if attributes:
        params["payrollSettings"] = attributes

    return params


@app.task(**taskutils.task_params("prepare_per_employee_compute"))
def prepare_per_employee_compute(params):
    """Iterates over each employee to prepare data for payroll computation"""
    # get employees
    jobId = params.get("jobId")
    logger.info("get_payroll_settings: job_id: " + str(params.get("jobId")))
    parentJobId = params.get("parentJobId")
    jobIdToUse = parentJobId or jobId
    pr_settings = params.get("payrollSettings", {})
    # check if this is a recalc in which case, this will be in payload
    empIds = params.get("employeeIds", [])
    empIds = set(empIds)
    if not empIds:
        empIds = pr_settings.get("employeeIds", [])
    # TODO: check if the payload empIds are valid
    # load the the extra ABCD data.
    allowances = _load_job_result_map(params.get("allowancesJobId"))
    bonuses = _load_job_result_map(params.get("bonusesJobId"))
    commissions = _load_job_result_map(params.get("commissionsJobId"))
    deductions = _load_job_result_map(params.get("deductionsJobId"))
    task_count = len(empIds)
    taskutils.job_update(jobIdToUse, None, task_count, 0)
    if parentJobId:
        taskutils.job_reset(jobIdToUse)
    debug_id = str(uuid.uuid4())
    for emp_id in empIds:
        allowance = allowances.get(str(emp_id))
        bonus = bonuses.get(str(emp_id))
        commission = commissions.get(str(emp_id))
        deduction = deductions.get(str(emp_id))

        emp_params = dict(params)
        emp_params["debug_id"] = debug_id
        emp_params["originalJobId"] = emp_params["jobId"]
        emp_params["jobId"] = jobIdToUse
        emp_params["employeeUID"] = str(emp_id)
        emp_params["allowances"] = allowance
        emp_params["bonuses"] = bonus
        emp_params["commissions"] = commission
        emp_params["deductions"] = deduction
        _employee_compute_workflow(emp_params)

    return None

@app.task(**taskutils.task_params("prepare_per_employee_special_compute"))
def prepare_per_employee_special_compute(params):
    """Iterates over each employee to prepare data for payroll computation"""
    print('prepare_per_employee_special_compute')
    # get employees
    jobId = params.get("jobId")
    parentJobId = params.get("parentJobId")
    jobIdToUse = parentJobId or jobId

    payroll_settings = apicalls.get_special_payroll_settings(params.get("payrollId"))
    print('payroll_settings')
    print(payroll_settings)
    pr_settings = payroll_settings.get("data", {})

    # check if this is a recalc in which case, this will be in payload
    empIds = pr_settings.get("attributes", {}).get("employeeIds", [])

    # TODO: check if the payload empIds are valid
    #

    empIds = set(empIds)
    task_count = len(empIds)
    taskutils.job_update(jobIdToUse, None, task_count, 0)
    if parentJobId:
        taskutils.job_reset(jobIdToUse)

    for emp_id in empIds:
        emp_params = dict(params)
        emp_params["originalJobId"] = emp_params["jobId"]
        emp_params["jobId"] = jobIdToUse
        emp_params["employeeUID"] = str(emp_id)
        emp_params["payrollId"] = params.get("payrollId")
        emp_params["payrollSettings"] = payroll_settings

        compute_special_pay.apply_async(args=(emp_params,), queue='payroll-inner')

    return None

@app.task(**taskutils.task_params("prepare_per_employee_final_compute"))
def prepare_per_employee_final_compute(params):
    """Iterates over each employee to prepare data for payroll computation"""
    # get employees
    jobId = params.get("jobId")
    parentJobId = params.get("parentJobId")
    jobIdToUse = parentJobId or jobId

    payroll_settings = apicalls.get_special_payroll_settings(params.get("payrollId"))
    pr_settings = payroll_settings.get("data", {})

    # check if this is a recalc in which case, this will be in payload
    empIds = pr_settings.get("attributes", {}).get("employeeIds", [])

    # TODO: check if the payload empIds are valid
    #

    task_count = len(empIds)
    taskutils.job_update(jobIdToUse, None, task_count, 0)
    if parentJobId:
        taskutils.job_reset(jobIdToUse)

    for emp_id in empIds:
        emp_params = dict(params)
        emp_params["originalJobId"] = emp_params["jobId"]
        emp_params["jobId"] = jobIdToUse
        emp_params["employeeUID"] = str(emp_id)
        emp_params["payrollId"] = params.get("payrollId")
        emp_params["payrollSettings"] = pr_settings

        compute_final_pay.apply_async(args=(emp_params,), queue='payroll-inner')

    return None

@app.task(**taskutils.task_params("prepare_per_employee_after_effects"))
def prepare_per_employee_after_effects(params):
    """Iterates over each employee to prepare data for payroll computation"""
    # get employees
    effect = params.get("effect", "close")  # or reopen
    jobId = params.get("jobId")
    pr_settings = params.get("payrollSettings", {})
    empIds = pr_settings.get("employeeIds", [])
    # TODO: check if the payload empIds are valid
    # load the the extra ABCD data.
    empIds = set(empIds)
    task_count = len(empIds)
    if task_count > 1:
        taskutils.job_update(jobId, None, task_count, 0)
    for emp_id in empIds:
        emp_params = dict(params)
        emp_params["employeeUID"] = emp_id
        if effect == "close":
            _employee_close_workflow(emp_params, pr_settings.get("payrollType", None))
        elif effect == "reopen":
            _employee_reopen_workflow(emp_params, pr_settings.get("payrollType", None))


def _employee_compute_workflow(params: dict):
    attendance_source = params.get("payrollSettings", {}).\
                        get("payrollJobs", {}).\
                        get("ATTENDANCE", {}).\
                        get("job_file")
    if (params.get("originalJobId") != params.get("jobId") and
            attendance_source == 'as-api-data'): #this is a regenerate job  with as-api-data source
        chain(
            load_employee_data.s(params),
            get_attendance_service_data.s(),
            load_contribution_record.s(),
            get_employee_payroll_loan_items.s(),
            compute_pay.s(),
        ).apply_async(queue='payroll-inner')
    else:
        chain(
            load_employee_data.s(params),
            load_attendance_data.s(),
            load_contribution_record.s(),
            get_employee_payroll_loan_items.s(),
            compute_pay.s(),
        ).apply_async(queue='payroll-inner')

def _employee_reopen_workflow(params, payroll_type=None): # pragma: no cover
    if (payroll_type == 'special'):
        chain(
            payroll_employee_reopen.s(params),
            report_ok.s()
        ).apply_async(queue='payroll-inner')
    else:
        chain(
            get_employee_payroll_loan_items.s(params),
            update_amortizations.s(),
            payroll_employee_reopen.s(),
            report_ok.s()
        ).apply_async(queue='payroll-inner')


def _employee_close_workflow(params, payroll_type=None): # pragma: no cover
    if (payroll_type == 'special'):
        chain(
            payroll_employee_close.s(params),
            report_ok.s()
        ).apply_async(queue='payroll-inner')
    else:
        chain(
            get_employee_payroll_loan_items.s(params),
            update_amortizations.s(),
            payroll_employee_close.s(),
            report_ok.s()
        ).apply_async(queue='payroll-inner')


@app.task(**taskutils.task_params("update_amortizations"))
def update_amortizations(params):
    """Amortizations update"""
    # get companyId and amortization ids/amounts list
    effect = params.get("effect", "close")
    company_id = params.get("companyId")
    amortizations = params.get("amortizations")
    data = []
    if effect == "close":
        data = [
            {"id": int(x.get("id")), "amount_collected": float(
                x.get("amountDue"))}
            for x in amortizations
        ]
    elif effect == "reopen":
        data = [
            {"id": int(x.get("id")), "amount_collected": float(
                x.get("amountDue")) * -1}
            for x in amortizations
        ]

    if data:
        apicalls.update_amortizations(company_id, data)
    return params


@app.task(**taskutils.task_params("payroll_employee_close"))
def payroll_employee_close(params):
    """Per employee close call"""
    payroll_id = params.get("payrollId")
    employee_id = params.get("employeeUID")
    apicalls.payroll_employee_close(payroll_id, employee_id)
    return params


@app.task(**taskutils.task_params("payroll_employee_reopen"))
def payroll_employee_reopen(params):
    """Reopen"""
    payroll_id = params.get("payrollId")
    employee_id = params.get("employeeUID")
    apicalls.payroll_employee_reopen(payroll_id, employee_id)
    return params


@app.task(**taskutils.task_params("report_ok"))
def report_ok(params):
    """Report generic success"""
    effect = params.get("effect")
    if effect == "reopen":
        # check if job is finished
        taskutils.job_update(
            params.get("jobId"), None, None, "+1",
            on_finished_task="payroll.tasks.payroll_reopen",
            on_finished_task_args=[{"payrollId": params.get("payrollId")}]
        )
    else:
        taskutils.job_update(params.get("jobId"), None, None, "+1")


@app.task(**taskutils.task_params("payroll_reopen"))
def payroll_reopen(params):
    """Reopen task"""
    payroll_id = params.get("payrollId")
    apicalls.payroll_reopen(payroll_id)


# compute attendance tasks
@app.task(**taskutils.task_params("load_employee_data"))
def load_employee_data(params):
    logger.info(str({
        "task": "load_employee_data",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "employeeUID": str(params.get("employeeUID")),
        "debug_id": str(params.get("debug_id")),
    }))
    """Gets payroll employee data"""
    payroll_id = params.get("payrollId")
    employee_id = params.get("employeeUID")
    # call employee api
    result = apicalls.get_payroll_employee_settings(payroll_id, employee_id)
    data = result.get("data", {})
    attributes = data.get("attributes")
    if attributes:
        aabcd = dict(attributes.get("aabcd", {}))
        if aabcd:
            del attributes['aabcd']
        params["employeeInfo"] = attributes
        params["aabcd"] = aabcd
    else:
        raise ValueError("No employee info found")

    return params


# compute attendance tasks
@app.task(**taskutils.task_params("load_attendance_data"))
def load_attendance_data(params):
    logger.info(str({
        "task": "load_attendance_data",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "employeeUID": str(params.get("employeeUID")),
        "debug_id": str(params.get("debug_id")),
    }))
    """Gets employee attendance from AS API"""
    # call employee api
    employee_id = params.get("employeeUID")
    attendance_job_id = params.get("attendanceJobId")

    result = apicalls.get_employee_attendance(attendance_job_id, employee_id)
    data = result.get("data", {})
    attributes = data.get("attributes", {})
    attendance_data = attributes.get("result", {})
    if attendance_data:
        params["attendanceData"] = attendance_data.get(str(employee_id))
    else:
        raise ValueError("no attendance data for employee %s" % employee_id)
    return params


@app.task(**taskutils.task_params("load_contribution_record"))
def load_contribution_record(params):
    logger.info(str({
        "task": "load_contribution_record",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "employeeUID": str(params.get("employeeUID")),
        "debug_id": str(params.get("debug_id")),
    }))
    """Loads employee contribution record"""
    employee_id = params.get("employeeUID")
    posting_date = params.get("payrollSettings", {}).get("postingDate")

    contrib_records = apicalls.get_contribution_record(
        employee_id, posting_date)
    params["contributionRecord"] = contrib_records.get("data", {})
    return params


@app.task(**taskutils.task_params("load_employee_loan_amortizations"))
def load_employee_loan_amortizations(params):
    """Loads employee contribution record"""
    employee_id = params.get("employeeUID")
    posting_date = params.get("payrollSettings", {}).get("postingDate")

    contrib_records = apicalls.get_contribution_record(
        employee_id, posting_date)
    params["contributionRecord"] = contrib_records.get("data", {})
    return params


@app.task(**taskutils.task_params("compute_pay"))
def compute_pay(params):
    logger.info(str({
        "task": "compute_pay",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "employeeUID": str(params.get("employeeUID")),
        "debug_id": str(params.get("debug_id")),
    }))
    """Main computation task for compute-payroll"""
    # call employee api
    # first clear the errors if there are any left if this is a recompute job
    if params.get("originalJobId") and params.get("originalJobId") != params.get("jobId"):
        # this is a recompute task -- erase previous error if any
        try:
            apicalls.delete_job_error(
                "{job_id}:{emp}".format(
                    job_id=params.get("jobId"),
                    emp=params.get("employeeUID"))
            )
        except exceptions.RequestError:
            pass

    # start template
    employee_info = dict(params.get("employeeInfo"))
    contribution_basis = employee_info.get("contributionBasis")
    g_employeedetails = {
        "contribution_basis": contribution_basis,
        "tax_type": str(employee_info.get("taxType")).upper().replace(' ', '_'),
        "tax_rate": _convert_float(employee_info.get("taxRate")),
        "tax_status": employee_info.get("taxStatus"),
        "hrs_per_day": _convert_float(employee_info.get("hoursPerDay")),
        "basic_info": employee_info,
    }
    # cleanup
    if "taxType" in employee_info:
        del employee_info["taxType"]
    if "taxRate" in employee_info:
        del employee_info["taxRate"]
    if "taxStatus" in employee_info:
        del employee_info["taxStatus"]
    del employee_info["contributionBasis"]

    basepay_history = employee_info.get("basePay")
    # transform
    transformed_bh = []
    for item in basepay_history:
        new_item = {}
        attributes = item.get("attributes", {})
        if attributes:
            new_item["amount"] = float(attributes.get("amount", 0))
            new_item["pay_rate"] = attributes.get("unit")
            new_item["effective_date"] = attributes.get("effectiveDate")
            transformed_bh.append(new_item)

    g_basepayhistory = {"records": transformed_bh}
    del employee_info["basePay"]  # reduce data

    attendance_data = dict(params.get("attendanceData", {}))
    transformed_att = []
    for dt, value in attendance_data.items():
        new_item = {}
        new_item["date"] = dt
        sh = value.get("SCHEDULED_HOURS")
        if sh is not None:
            new_item["scheduled_hours"] = float(value.get("SCHEDULED_HOURS"))
            del value["SCHEDULED_HOURS"]
        new_item["hr_types"] = []
        for htype, hvalue in value.items():
            new_item["hr_types"].append(
                {"type": str(htype).upper(), "hours": float(hvalue)}
            )
        transformed_att.append(new_item)

    g_attendancerecord = transformed_att

    payroll_settings = dict(params.get("payrollSettings"))
    pgroup_info = payroll_settings.get("payrollGroupInfo", {})

    g_payrollgroup = {
        "pay_start_date": payroll_settings.get("payrollStartDate"),
        "pay_end_date": payroll_settings.get("payrollEndDate"),
        "attendance_start_date": payroll_settings.get("attendanceStartDate"),
        "attendance_end_date": payroll_settings.get("attendanceEndDate"),
        "posting_date": payroll_settings.get("postingDate"),
        "pay_run": payroll_settings.get("payRunDate"),

        "cutoff": pgroup_info.get("cutoffDate"),
        "cutoff_is_eom": pgroup_info.get("cutoffDateEom"),
        "posting_date1": pgroup_info.get("postingDate"),
        "posting_date1_is_eom": pgroup_info.get("postingDateEom"),

        "cutoff2": pgroup_info.get("cutoffDate2"),
        "cutoff2_is_eom": pgroup_info.get("cutoffDate2Eom"),
        "posting_date2": pgroup_info.get("postingDate2"),
        "posting_date2_is_eom": pgroup_info.get("postingDate2Eom"),

        "hrs_per_day": _convert_float(pgroup_info.get("hoursPerDay", 0)),
        "day_factor": float(pgroup_info.get("dayFactor")),
        "pay_frequency": pgroup_info.get("payrollFrequency"),
        "contribution_schedules": pgroup_info.get("contributionSchedules"),
        "compute_overbreak": pgroup_info.get("computeOverbreak", False),
        "compute_undertime": pgroup_info.get("computeUndertime", True),
        "compute_tardiness": pgroup_info.get("computeTardiness", True),
        "hour_type_multiplier": payroll_settings.get("dayHourRates"),
        "attend_period_switch": pgroup_info.get("attendPeriodSwitch", False),
        "att_first_cut_off_date": pgroup_info.get("att_first_cut_off_date"),
        "att_second_cut_off_date": pgroup_info.get("att_second_cut_off_date"),
        "att_fcd_end_of_month": pgroup_info.get("att_fcd_end_of_month"),
        "att_scd_end_of_month": pgroup_info.get("att_scd_end_of_month"),
    }

    g_previouscontributionrecord = dict(params.get("contributionRecord", {}))

    allowances = dict(params.get("allowances") or {})
    deminimis_reducetotaxable = []
    deminimis_addasnontaxable = []
    transformed_allowances = []
    for category, values in allowances.items():
        if category in ('taxable', 'nontaxable'):
            taxable = category == "taxable"
            for name, amount in values.items():
                transformed_allowances.append(
                    {"type": name, "amount": amount, "taxable": taxable}
                )
        elif category == 'reduceToTaxableIncome':
            for name, amount in values.items():
                deminimis_reducetotaxable.append(
                    {"name": name, "amount": amount, "type": "REDUCE_TAXABLE"}
                )
        elif category == "addAsNontaxableIncome":
            for name, amount in values.items():
                deminimis_addasnontaxable.append(
                    {"name": name, "amount": amount, "type": "NON_TAXABLE"}
                )

    bonuses = dict(params.get("bonuses") or {})
    transformed_bonuses = []
    for category, values in bonuses.items():
        taxable = category == "taxable"
        for name, amount in values.items():
            transformed_bonuses.append(
                {"type": name, "amount": amount, "taxable": taxable}
            )

    commissions = dict(params.get("commissions") or {})
    transformed_commissions = []
    for category, values in commissions.items():
        taxable = category == "taxable"
        for name, amount in values.items():
            transformed_commissions.append(
                {"type": name, "amount": amount, "taxable": taxable}
            )
    deductions = dict(params.get("deductions") or {})
    transformed_deductions = []
    for category, values in deductions.items():
        taxable = category == "taxable"
        for name, amount in values.items():
            transformed_deductions.append(
                {"type": name, "amount": amount, "taxable": taxable}
            )

    g_other_incomes = {
        "ALLOWANCES": transformed_allowances,
        "BONUSES": transformed_bonuses,
        "COMMISSIONS": transformed_commissions,
    }

    g_deminimis = {
        "reduceToTaxableIncome": deminimis_reducetotaxable,
        "addAsNontaxableIncome": deminimis_addasnontaxable
    }

    g_aabcd = dict(params.get("aabcd", {}))

    g_loans_to_credit = params.get("loansToCredit", [])
    g_amortizations = params.get("amortizations", [])

    g_annualize = bool(payroll_settings.get("annualize", False))
    g_annual_earnings = employee_info.get("annualEarnings", {}) or {}

    if not g_annual_earnings:
        g_annual_earnings = {}

    g_leave_credits = employee_info.get("leaveConversion", {})

    try:
        res = salarium_payroll.PayrollCalculator.calculate(
            payroll_group=g_payrollgroup,
            employee_details=g_employeedetails,
            base_pay_history=g_basepayhistory,
            attendance_record=g_attendancerecord,
            prev_contributions=g_previouscontributionrecord,
            uploaded_other_incomes=g_other_incomes,
            uploaded_deductions=transformed_deductions,
            aabcd=g_aabcd,
            loans_to_credit=g_loans_to_credit,
            loan_amortizations=g_amortizations,
            deminimis=g_deminimis,
            annualize=g_annualize,
            annual_earnings=g_annual_earnings,
            leave_credits=g_leave_credits
        )
    except Exception as exc:
        payroll_calc_params = json.dumps({
            "payroll_group": g_payrollgroup,
            "employee_details": g_employeedetails,
            "base_pay_history": g_basepayhistory,
            "attendance_record": g_attendancerecord,
            "prev_contributions": g_previouscontributionrecord,
            "aabcd": g_aabcd,
            "uploaded_other_incomes": g_other_incomes,
            "uploaded_deductions": transformed_deductions,
            "loans_to_credit": g_loans_to_credit,
            "loan_amortizations": g_amortizations,
            "deminimis": g_deminimis,
            "annualize": g_annualize,
            "annual_earnings": g_annual_earnings,
            "leave_credits": g_leave_credits
        })
        logger.error(
            f"Exception occurred during payroll calculation {payroll_calc_params} {exc}"
        )
        raise

    # end template
    logger.info(str({
        "task": "saving_computed_payroll",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "employeeUID": str(params.get("employeeUID")),
        "debug_id": str(params.get("debug_id")),
    }))
    validate_and_save_job_results(params, res)
    return res

@app.task(**taskutils.task_params("compute_special_pay"))
def compute_special_pay(params):
    """Main computation task for compute-special-payroll"""

    # first clear the errors if there are any left if this is a recompute job
    if params.get("originalJobId") and params.get("originalJobId") != params.get("jobId"):
        # this is a recompute task -- erase previous error if any
        try:
            apicalls.delete_job_error(
                "{job_id}:{emp}".format(
                    job_id=params.get("jobId"),
                    emp=params.get("employeeUID"))
            )
        except exceptions.RequestError:
            pass

    employee_settings = apicalls.get_special_payroll_employee_settings(
        params.get("payrollId"),
        params.get("employeeUID")
    )

    res = salarium_payroll.SpecialPayrollCalculator.calculate(
        payroll_details=params.get("payrollSettings"),
        employee_details=employee_settings.get("attributes", {}).get("employees", [])[0]
    )

    # end template
    taskutils.result(
        params.get("jobId"), res, params.get("employeeUID"),
        on_finished_task="payroll.tasks.open_payroll",
        on_finished_task_args=[
            {"jobId": params.get("jobId"),
             "payrollId": params.get("payrollId"),
             "originalJobId": params.get("originalJobId")}
        ]
    )

    return res

@app.task(**taskutils.task_params("compute_final_pay"))
def compute_final_pay(params):
    """Main computation task for compute-final-payroll"""

    # first clear the errors if there are any left if this is a recompute job
    if params.get("originalJobId") and params.get("originalJobId") != params.get("jobId"):
        # this is a recompute task -- erase previous error if any
        try:
            apicalls.delete_job_error(
                "{job_id}:{emp}".format(
                    job_id=params.get("jobId"),
                    emp=params.get("employeeUID"))
            )
        except exceptions.RequestError:
            pass

    employee_settings = apicalls.get_final_payroll_employee_settings(
        params.get("payrollId"),
        params.get("employeeUID")
    )

    try:
        g_payrolldetails = params.get("payrollSettings")
        g_employeedetails = employee_settings.get("data", [])[0]

        res = salarium_payroll.FinalPayrollCalculator.calculate(
            payroll_details=g_payrolldetails,
            employee_details=g_employeedetails,
        )
    except Exception as exc:
        payroll_calc_params = json.dumps({
            "payroll_details": g_payrolldetails,
            "employee_details": g_employeedetails,
        })
        logger.error(
            f"Exception occurred during final pay payroll calculation {payroll_calc_params} {exc}"
        )
        raise

    # end template
    taskutils.result(
        params.get("jobId"), res, params.get("employeeUID"),
        on_finished_task="payroll.tasks.open_payroll",
        on_finished_task_args=[
            {"jobId": params.get("jobId"),
             "payrollId": params.get("payrollId"),
             "originalJobId": params.get("originalJobId")}
        ]
    )

    return res

# compute projected final payroll
@app.task(**taskutils.task_params("compute_projected_final_pay"))
def compute_projected_final_pay(params):
    """Main computation task for compute-projected-final-pay"""
    # call employee api
    # first clear the errors if there are any left if this is a recompute job
    if params.get("originalJobId") and params.get("originalJobId") != params.get("jobId"):
        # this is a recompute task -- erase previous error if any
        try:
            apicalls.delete_job_error(
                "{job_id}:{emp}".format(
                    job_id=params.get("jobId"),
                    emp=params.get("employeeUID"))
            )
        except exceptions.RequestError:
            pass

    employee_settings = apicalls.get_final_payroll_employee_settings(0, params.get("employeeUID"))

    try:
        g_payrolldetails = params.get("payrollSettings")
        g_employeedetails = employee_settings.get("data", [])[0]

        res = salarium_payroll.FinalPayrollCalculator.calculate(
            payroll_details=g_payrolldetails,
            employee_details=g_employeedetails,
        )

        apicalls.save_projected_final_pay_item(res)
    except Exception as exc:
        payroll_calc_params = json.dumps({
            "payroll_details": g_payrolldetails,
            "employee_details": g_employeedetails
        })
        logger.error(
            f"Exception occurred during final pay projected payroll calculation {payroll_calc_params} {exc}"
        )
        raise

    # end template
    taskutils.result(
        params.get("jobId"), res, params.get("employeeUID")
    )

    return res

@app.task(**taskutils.task_params("open_payroll", superclass=taskutils.PayrollEventReportingTask))
def open_payroll(params):
    logger.info("open_payroll: " + str(params.get("jobId")))
    logger.info(str({
        "task": "open_payroll",
        "originalJobId": str(params.get("originalJobId")),
        "jobId": str(params.get("jobId")),
        "payrollId": str(params.get("payrollId")),
        "debug_id": str(params.get("debug_id")),
    }))
    """Callback for trigerring payroll opening"""
    # get job and check for errors
    job_id = params.get("jobId")
    payroll_id = params.get("payrollId")
    job = apicalls.get_job(job_id)
    relationships = job.get("data", {}).get("relationships", {})
    errors = relationships.get("errors") or {}
    if not errors.get("data") and not params.get("doNotOpen", False):
        # save payroll items
        ok = False
        #results = []
        result_id = job_id
        next_link = None
        while not ok:
            response = apicalls.get_job_results(
                job_id=result_id, page_url=next_link)
            data = []
            if response:
                data = response.get("data") or []
            for item in data:
                # get empuid from the result id -- very hacky
                resultid = item.get("id")
                _, empuid = resultid.split(':')
                attributes = item.get("attributes")

                # get result redis key
                result_key = attributes.get("result")

                # get result from redis
                result = apicalls.get_result_from_redis(result_key) if isinstance(result_key, str) else attributes.get("result")

                result["employeeId"] = empuid
                apicalls.save_payroll_item(
                    _format_payroll_result(result, payroll_id))

            links = response.get("links", {})
            next_page = links.get("next")
            if next_page:
                result_id = None
                next_link = next_page
            else:
                ok = True
        # save_items.join()

        # call create payroll register
        # apicalls.create_payroll_register(payroll_id)
        create_payroll_register.apply_async(
            args=(payroll_id,), queue='payroll')

        # Open Payroll
        apicalls.open_payroll(payroll_id)

        # call create payroll register
        # create_payroll_register.apply_async(
        #     args=(payroll_id,), queue='payroll')

    else:
        logger.warning(
            'Payroll ID {payroll_id} not opened due to errors.'.format(
                payroll_id=payroll_id)
        )
        revert_payroll_status.apply_async(args=(payroll_id,), queue='payroll')
    # if this was a recalculate job, force the original job to be status=FINISHED
    if params.get("originalJobId") and params.get("originalJobId") != params.get("jobId"):
        taskutils.job_update(params.get("originalJobId"), 'FINISHED', 0, 0)


@app.task(**taskutils.task_params("create_payroll_register",
                                  superclass=taskutils.EventReportingTask))
def create_payroll_register(payroll_id):
    """Task for saving payroll item"""
    apicalls.create_payroll_register(payroll_id)
    return True


@app.task(**taskutils.task_params("save_payroll_item", superclass=taskutils.EventReportingTask))
def save_payroll_item(item):
    """Task for saving payroll item"""
    apicalls.save_payroll_item(item)
    return True


def _load_job_result_map(job_id):
    data = {}
    if not job_id:
        return data
    job_data = apicalls.get_job_results(
        job_id=job_id)  # TODO: handle multiple pages
    included_data = job_data.get("data", None)
    if included_data:
        included_data = apicalls.get_result_from_redis(included_data) if isinstance(included_data, str) else included_data
    else:
        included_data = []
    #logger.debug(included_data)
    for item in included_data:
        result = item.get("attributes", {}).get("result")
        data.update(result)
    return data


def _format_payroll_result(result, payroll_id):
    inp = result.get("input")
    out = result.get("output")
    empuid = result.get("employeeId")
    pgroup_info = inp.get("payroll_group", {})
    basic_info = inp.get("employee_details", {}).get("basic_info")
    emp_info = basic_info or out.get("employee_details", {}).get("basic_info")

    if empuid:
        empuid = int(empuid)
        del result["employeeId"]

    return {
        "data": {
            "type": "payroll-calculation",
            "attributes": {
                "payrollId": int(payroll_id),
                "employeeId": empuid,
                "payrollGroupId": int(emp_info.get("payrollGroupId", 0)),
                "payrollStartDate": pgroup_info.get("pay_start_date"),
                "payrollEndDate": pgroup_info.get("pay_end_date"),
                "payrollPostingDate": pgroup_info.get("posting_date"),
                "data": result
            }
        }
    }


@app.task(**taskutils.task_params("revert_payroll_status", superclass=taskutils.EventReportingTask))
def revert_payroll_status(payroll_id):
    """Task for reverting status"""
    apicalls.revert_payroll_status(payroll_id)


def _convert_float(value):
    try:
        return float(value or 0)
    except ValueError:
        logger.warning("Expected numeric parameter is not float value")
        return 0

def email_zipped_payroll_register(params): # pragma: no cover
    """Task chainer for sending payroll register"""
    chain(
        file_tasks.zip_files.s(params),
        file_tasks.trigger_email_file_link.s()
    ).apply_async(queue='payroll')

def validate_and_save_job_results(params, res):
    """
    If the job-result for the specific
    job_id and employee_id already exists, then
    skip saving again to prevent duplication
    """
    if not _job_result_exists(params.get("jobId"), params.get("employeeUID")):
        taskutils.result(
            params.get("jobId"), res, params.get("employeeUID"),
            on_finished_task="payroll.tasks.open_payroll",
            on_finished_task_args=[
                {"jobId": params.get("jobId"),
                "payrollId": params.get("payrollId"),
                "debug_id": str(params.get("debug_id")),
                "originalJobId": params.get("originalJobId")}
            ]
        )

def _job_result_exists(job_id, employee_id):
    """
    If this returned successful result that means job-result
    for this employee_id and job_id already result exists
    """
    try:
        apicalls.get_job_result(job_id, employee_id)
        logger.info("Job result already exists for job_id: " + str(job_id) + " and employee_id: " + employee_id)
        return True
    except Exception as e:
        return False
