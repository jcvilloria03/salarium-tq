from datetime import datetime
import re
import uuid
import os
import zipfile
import time
from csv import DictWriter

import boto3

from core.app import app
from payroll._tasks.taskutils import task_params, job_update, result as task_result
from payroll import apicalls, dao
from datetime import datetime
from celery import chain
import pr_config

from core import exceptions, logger
logger = logger.get_logger()

def generate_employee_masterfile(params):
    chain(
        initialize_job.s(params),
    ).apply_async(queue='payroll')

@app.task(**task_params("initialize_job"))
def initialize_job(params):
    task_count = 0
    is_subscribed_to_payroll = params.get("isSubscribedToPayroll", False)
    is_subscribed_to_ta = params.get("isSubscribedToTa", False)

    if is_subscribed_to_ta and is_subscribed_to_payroll:
        task_count += 1
    elif is_subscribed_to_payroll:
        task_count += 1
    elif is_subscribed_to_ta:
        task_count += 1

    params["totalCsvFiles"] = task_count# exclude the zip_masterfile task
    job_update(params.get("jobId"), None, task_count, 0)

    if is_subscribed_to_ta and is_subscribed_to_payroll:
        chain(
            generate_personal_masterlist.s(params)
        ).apply_async(queue='payroll')
    elif is_subscribed_to_payroll:
        chain(
            generate_payroll_masterlist.s(params)
        ).apply_async(queue='payroll')
    elif is_subscribed_to_ta:
        chain(
            generate_timeattendance_masterlist.s(params)
        ).apply_async(queue='payroll')

@app.task(**task_params("generate_personal_masterlist"))
def generate_personal_masterlist(params):
    headers = [
        "Employee ID",
        "Last Name",
        "First Name",
        "Middle Name",
        "Email",
        "Employee Status",
        "Telephone Number",
        "Mobile Number",
        "Address Line 1",
        "Address Line 2",
        "Country",
        "City",
        "ZIP",
        "Birthdate",
        "Gender",
        "Date Hired",
        "Date Resigned",
        "Employment Type",
        "Position",
        "Department",
        "Rank",
        "Team",
        "Team Role",
        "Primary Location",
        "Secondary Location",
        "Timesheet Required?",
        "Entitled to Overtime?",
        "Entitled Night Differential?",
        "Entitled Unworked Regular Holiday Pay?",
        "Entitled Unworked Special Holiday Pay?",
        "Entitled Holiday Premium Pay?",
        "Entitled Rest Day Pay?",
        "Hours Per Day",
        "Tax Type",
        "Consultant Tax Rate",
        "Tax Status",
        "Base Pay",
        "Base Pay Adjustment Reason",
        "Base Pay Adjustment Effective Date",
        "Base Pay Adjustment Date",
        "Base Pay Unit",
        "Payroll Group",
        "Cost Center",
        "Payment Method",
        "Bank Name",
        "Bank Type",
        "Bank Account No.",
        "SSS Basis",
        "SSS Amount",
        "PhilHealth Basis",
        "PhilHealth Amount",
        "HDMF Basis",
        "HDMF Amount",
        "TIN",
        "RDO",
        "SSS Number",
        "HDMF Number",
        "PhilHealth Number",
        "Entitled Deminimis",
    ]

    filename_prefix = params.get("companyId")
    filename_suffix = datetime.now().strftime("%m%d%Y%H%M")
    filename = f'{filename_prefix}_employees_{filename_suffix}.csv'
    file_path = '/tmp/' + filename
    with open(file_path, 'a') as output_file:
        dict_writer = DictWriter(output_file, headers)
        dict_writer.writeheader()

        is_done = False
        current_page = 0

        # Main chunk loop
        while is_done is False:
            current_page += 1

            result = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include=None,
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_PERSONAL",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"),  # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            result_ta = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include="time_attendance",
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_TA",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"),  # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            result_payroll = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include="payroll",
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_PAYROLL",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"), # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            employee_personal_info_list = []
            raw_data = result.get("data", [])
            raw_data_ta = result_ta.get("data", [])
            raw_data_payroll = result_payroll.get("data", [])
            for (row, row_ta, row_payroll) in zip(raw_data, raw_data_ta, raw_data_payroll):
                row_ta["date_hired"] = convert_date(row_ta["date_hired"])
                employee_personal_info_list.append({
                    "Employee ID": f"\r{row['employee_id']}",
                    "Last Name": row["last_name"],
                    "First Name": row["first_name"],
                    "Middle Name": row["middle_name"],
                    "Email": row["email"],
                    "Employee Status": row["employee_status"],
                    "Telephone Number": row["telephone_number"],
                    "Mobile Number": row["mobile_number"],
                    "Address Line 1": row["address_line_1"],
                    "Address Line 2": row["address_line_2"],
                    "Country": row["country"],
                    "City": row["city"],
                    "ZIP": row["zip"],
                    "Birthdate": row["birthdate"],
                    "Gender": row["gender"],
                    "Date Hired": row_ta["date_hired"],
                    "Date Resigned": row_ta["termination_date"],
                    "Employment Type": row_ta["employment_type"],
                    "Position": row_ta["position"],
                    "Department": row_ta["department"],
                    "Rank": row_ta["rank"],
                    "Team": row_ta["team"],
                    "Team Role": row_ta["team_role"],
                    "Primary Location": row_ta["primary_location"],
                    "Secondary Location": row_ta["secondary_location"],
                    "Timesheet Required?": row_ta["timesheet_required"],
                    "Entitled to Overtime?": row_ta["entitled_to_overtime"],
                    "Entitled Night Differential?": row_ta["entitled_night_differential"],
                    "Entitled Unworked Regular Holiday Pay?": row_ta["entitled_unworked_regular_holiday_pay"],
                    "Entitled Unworked Special Holiday Pay?": row_ta["entitled_unworked_special_holiday_pay"],
                    "Entitled Holiday Premium Pay?": row_ta["entitled_holiday_premium_pay"],
                    "Entitled Rest Day Pay?": row_ta["entitled_rest_day_pay"],
                    "Hours Per Day": row_payroll["hours_per_day"],
                    "Tax Type": row_payroll["tax_type"],
                    "Consultant Tax Rate": row_payroll["consultant_tax_rate"],
                    "Tax Status": row_payroll["tax_status"],
                    "Base Pay": row_payroll["base_pay"],
                    "Base Pay Adjustment Reason": row_payroll["reason"],
                    "Base Pay Adjustment Effective Date": row_payroll["effective_date"],
                    "Base Pay Adjustment Date": row_payroll["adjustment_date"],
                    "Base Pay Unit": row_payroll["base_pay_unit"],
                    "Payroll Group": row_payroll["payroll_group"],
                    "Cost Center": row_payroll["cost_center"],
                    "Payment Method": row_payroll["payment_method"],
                    "Bank Name": row_payroll["bank_name"],
                    "Bank Type": row_payroll["bank_type"],
                    "Bank Account No.": "'"+row_payroll["bank_account_no"]+"'" if row_payroll["bank_account_no"] else None,
                    "SSS Basis": row_payroll["sss_basis"],
                    "SSS Amount": row_payroll["sss_amount"],
                    "PhilHealth Basis": row_payroll["philhealth_basis"],
                    "PhilHealth Amount": row_payroll["philhealth_amount"],
                    "HDMF Basis": row_payroll["hdmf_basis"],
                    "HDMF Amount": row_payroll["hdmf_amount"],
                    "TIN": row_payroll["tin"],
                    "RDO": row_payroll["rdo"],
                    "SSS Number": row_payroll["sss_number"],
                    "HDMF Number": row_payroll["hdmf_number"],
                    "PhilHealth Number": row_payroll["philhealth_number"],
                    "Entitled Deminimis": row_payroll["entitled_deminimis"],
                })

            dict_writer.writerows(employee_personal_info_list)

            current_page = result.get("current_page")
            last_page = result.get("last_page")

            is_done = current_page >= last_page

    # Upload file to S3, then delete
    cache_key, generated_file_key = upload_generated_file(params, filename, file_path)

    params["cacheKey"] = cache_key
    params["generatedFileKey"] = generated_file_key

    job_id = params.get("jobId")
    s3_bucket = pr_config.UPLOADS_BUCKET
    save_to_jm_api(
        job_id=job_id,
        s3_bucket=s3_bucket,
        generated_file_key=generated_file_key,
        range_key=1,
        task_name="generate_personal_masterlist"
    )

    email=params.get("email")
    firstName=params.get("firstName")
    url="employees/download?type=masterfile&uuid="+job_id

    apicalls.email_masterfile(email, firstName, url)

    return params

@app.task(**task_params("generate_timeattendance_masterlist"))
def generate_timeattendance_masterlist(params):
    headers = [
        "Employee ID",
        "Last Name",
        "First Name",
        "Middle Name",
        "Email",
        "Employee Status",
        "Telephone Number",
        "Mobile Number",
        "Address Line 1",
        "Address Line 2",
        "Country",
        "City",
        "ZIP",
        "Birthdate",
        "Gender",
        "Marital Status",
        "Date Hired",
        "Date Resigned",
        "Employment Type",
        "Position",
        "Department",
        "Rank",
        "Team",
        "Team Role",
        "Primary Location",
        "Secondary Location",
        "Timesheet Required?",
        "Entitled to Overtime?",
        "Entitled Night Differential?",
        "Entitled Unworked Regular Holiday Pay?",
        "Entitled Unworked Special Holiday Pay?",
        "Entitled Holiday Premium Pay?",
        "Entitled Rest Day Pay?",
    ]

    filename_prefix = params.get("companyId")
    filename_suffix = datetime.now().strftime("%m%d%Y%H%M")
    filename = f'{filename_prefix}_employees_tna_only_{filename_suffix}.csv'
    file_path = '/tmp/' + filename
    with open(file_path, 'a') as output_file:
        dict_writer = DictWriter(output_file, headers)
        dict_writer.writeheader()

        is_done = False
        current_page = 0

        # Main chunk loop
        while is_done is False:
            current_page += 1

            result = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include="time_attendance",
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_TA",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"),  # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            result_personal = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include=None,
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_PERSONAL",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"),  # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            employee_tna_info_list = []
            raw_data = result.get("data", [])
            raw_data_personal = result_personal.get("data", [])
            for (row, row_personal) in zip(raw_data, raw_data_personal):
                row["date_hired"] = convert_date(row["date_hired"])
                employee_tna_info_list.append({
                    "Employee ID": f"\r{row['employee_id']}",
                    "Last Name": row["last_name"],
                    "First Name": row["first_name"],
                    "Middle Name": row["middle_name"],
                    "Email": row_personal["email"],
                    "Employee Status": row_personal["employee_status"],
                    "Telephone Number": row_personal["telephone_number"],
                    "Mobile Number": row_personal["mobile_number"],
                    "Address Line 1": row_personal["address_line_1"],
                    "Address Line 2": row_personal["address_line_2"],
                    "Country": row_personal["country"],
                    "City": row_personal["city"],
                    "ZIP": row_personal["zip"],
                    "Birthdate": row_personal["birthdate"],
                    "Gender": row_personal["gender"],
                    "Marital Status": row["status"],
                    "Date Hired": row["date_hired"],
                    "Date Resigned": row["termination_date"],
                    "Employment Type": row["employment_type"],
                    "Position": row["position"],
                    "Department": row["department"],
                    "Rank": row["rank"],
                    "Team": row["team"],
                    "Team Role": row["team_role"],
                    "Primary Location": row["primary_location"],
                    "Secondary Location": row["secondary_location"],
                    "Timesheet Required?": row["timesheet_required"],
                    "Entitled to Overtime?": row["entitled_to_overtime"],
                    "Entitled Night Differential?": row["entitled_night_differential"],
                    "Entitled Unworked Regular Holiday Pay?": row["entitled_unworked_regular_holiday_pay"],
                    "Entitled Unworked Special Holiday Pay?": row["entitled_unworked_special_holiday_pay"],
                    "Entitled Holiday Premium Pay?": row["entitled_holiday_premium_pay"],
                    "Entitled Rest Day Pay?": row["entitled_rest_day_pay"],
                })

            dict_writer.writerows(employee_tna_info_list)

            current_page = result.get("current_page")
            last_page = result.get("last_page")

            is_done = current_page >= last_page

    # Upload file to S3, then delete
    cache_key, generated_file_key = upload_generated_file(params, filename, file_path)

    params["cacheKey"] = cache_key
    params["generatedFileKey"] = generated_file_key

    job_id = params.get("jobId")
    s3_bucket = pr_config.UPLOADS_BUCKET
    save_to_jm_api(
        job_id=job_id,
        s3_bucket=s3_bucket,
        generated_file_key=generated_file_key,
        range_key=2,
        task_name="generate_timeattendance_masterlist"
    )

    email=params.get("email")
    firstName=params.get("firstName")
    url="employees/download?type=masterfile&uuid="+job_id

    apicalls.email_masterfile(email, firstName, url)

    return params

@app.task(**task_params("generate_payroll_masterlist"))
def generate_payroll_masterlist(params):
    headers = [
        "Employee ID",
        "Last Name",
        "First Name",
        "Middle Name",
        "Email",
        "Employee Status",
        "Telephone Number",
        "Mobile Number",
        "Address Line 1",
        "Address Line 2",
        "Country",
        "City",
        "ZIP",
        "Birthdate",
        "Gender",
        "Marital Status",
        "Date Hired",
        "Date Resigned",
        "Employment Type",
        "Position",
        "Department",
        "Rank",
        "Primary Location",
        "Secondary Location",
        "Hours per Day",
        "Tax Type",
        "Consultant Tax Rate",
        "Tax Status",
        "Base Pay",
        "Base Pay Adjustment Reason",
        "Base Pay Adjustment Effective Date",
        "Base Pay Adjustment Date",
        "Base Pay Unit",
        "Payroll group",
        "Cost Center",
        "Payment Method",
        "Bank Name",
        "Bank Type",
        "Bank Account No.",
        "SSS Basis",
        "SSS Amount",
        "PhilHealth Basis",
        "PhilHealth Amount",
        "HDMF Basis",
        "HDMF Amount",
        "TIN",
        "RDO",
        "SSS Number",
        "HDMF Number",
        "PhilHealth Number",
        "Entitled Deminimis",
    ]

    filename_prefix = params.get("companyId")
    filename_suffix = datetime.now().strftime("%m%d%Y%H%M")
    filename = f'{filename_prefix}_employees_payroll_only_{filename_suffix}.csv'
    file_path = '/tmp/' + filename
    with open(file_path, 'a') as output_file:
        dict_writer = DictWriter(output_file, headers)
        dict_writer.writeheader()

        is_done = False
        current_page = 0

        # Main chunk loop
        while is_done is False:
            current_page += 1

            result = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include="payroll",
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_PAYROLL",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"), # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            result_personal = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include=None,
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_PERSONAL",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"),  # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            result_ta = apicalls.get_company_employees_with_filters(
                company_id=params.get("companyId"),
                include="time_attendance",
                status=params.get("employeeStatus"),
                mode="DOWNLOAD_TA",
                page=current_page,
                per_page=100,
                authz_data_scope=params.get("authzDataScope"),  # TODO: Decrypt this field
                search_term=params.get("searchTerm")
            )

            employee_payroll_info_list = []
            raw_data = result.get("data", [])
            raw_data_personal = result_personal.get("data", [])
            raw_data_ta = result_ta.get("data", [])
            for (row, row_personal, row_ta) in zip(raw_data, raw_data_personal, raw_data_ta):
                row_ta["date_hired"] = convert_date(row_ta["date_hired"])
                employee_payroll_info_list.append({
                    "Employee ID": f"\r{row['employee_id']}",
                    "Last Name": row["last_name"],
                    "First Name": row["first_name"],
                    "Middle Name": row["middle_name"],
                    "Email": row_personal["email"],
                    "Employee Status": row_personal["employee_status"],
                    "Telephone Number": row_personal["telephone_number"],
                    "Mobile Number": row_personal["mobile_number"],
                    "Address Line 1": row_personal["address_line_1"],
                    "Address Line 2": row_personal["address_line_2"],
                    "Country": row_personal["country"],
                    "City": row_personal["city"],
                    "ZIP": row_personal["zip"],
                    "Birthdate": row_personal["birthdate"],
                    "Gender": row_personal["gender"],
                    "Marital Status": row_ta["status"],
                    "Date Hired": row_ta["date_hired"],
                    "Date Resigned": row_ta["termination_date"],
                    "Employment Type": row_ta["employment_type"],
                    "Position": row_ta["position"],
                    "Department": row_ta["department"],
                    "Rank": row_ta["rank"],
                    "Primary Location": row_ta["primary_location"],
                    "Secondary Location": row_ta["secondary_location"],
                    "Hours per Day": row["hours_per_day"],
                    "Tax Type": row["tax_type"],
                    "Consultant Tax Rate": row["consultant_tax_rate"],
                    "Tax Status": row["tax_status"],
                    "Base Pay": row["base_pay"],
                    "Base Pay Adjustment Reason": row["reason"],
                    "Base Pay Adjustment Effective Date": row["effective_date"],
                    "Base Pay Adjustment Date": row["adjustment_date"],
                    "Base Pay Unit": row["base_pay_unit"],
                    "Payroll group": row["payroll_group"],
                    "Cost Center": row["cost_center"],
                    "Payment Method": row["payment_method"],
                    "Bank Name": row["bank_name"],
                    "Bank Type": row["bank_type"],
                    "Bank Account No.": "'"+row["bank_account_no"]+"'" if row["bank_account_no"] else None,
                    "SSS Basis": row["sss_basis"],
                    "SSS Amount": row["sss_amount"],
                    "PhilHealth Basis": row["philhealth_basis"],
                    "PhilHealth Amount": row["philhealth_amount"],
                    "HDMF Basis": row["hdmf_basis"],
                    "HDMF Amount": row["hdmf_amount"],
                    "TIN": row["tin"],
                    "RDO": row["rdo"],
                    "SSS Number": row["sss_number"],
                    "HDMF Number": row["hdmf_number"],
                    "PhilHealth Number": row["philhealth_number"],
                    "Entitled Deminimis": row["entitled_deminimis"],
                })

            dict_writer.writerows(employee_payroll_info_list)

            current_page = result.get("current_page")
            last_page = result.get("last_page")

            is_done = current_page >= last_page

    # Upload file to S3, then delete
    cache_key, generated_file_key = upload_generated_file(params, filename, file_path)

    params["cacheKey"] = cache_key
    params["generatedFileKey"] = generated_file_key

    job_id = params.get("jobId")
    s3_bucket = pr_config.UPLOADS_BUCKET
    save_to_jm_api(
        job_id=job_id,
        s3_bucket=s3_bucket,
        generated_file_key=generated_file_key,
        range_key=3,
        task_name="generate_payroll_masterlist"
    )

    email=params.get("email")
    firstName=params.get("firstName")
    url="employees/download?type=masterfile&uuid="+job_id

    apicalls.email_masterfile(email, firstName, url)

    return params

def upload_generated_file(params, filename, file_path):
    s3 = boto3.client('s3')
    company_id = params.get("companyId")
    generated_file_key = f'masterfiles/company_{company_id}/{filename}'
    s3_bucket = pr_config.UPLOADS_BUCKET
    s3.upload_file(
        file_path,
        s3_bucket,
        generated_file_key
    )
    os.remove(file_path)

    job_id = params.get("jobId")
    cache_key = f"zip_masterfile:{job_id}"

    dao.RedisConnect.conn.lpush(cache_key, generated_file_key)

    return cache_key, generated_file_key

def save_to_jm_api(job_id, s3_bucket, generated_file_key, range_key, task_name):
    res = {
        "task": task_name,
        "Bucket": s3_bucket,
        "Key": generated_file_key
    }

    task_result(job_id, res, range_key)

@app.task(**task_params("zip_masterfile"))
def zip_masterfile(params):
    # Add the file key to the semaphore (PREPEND)
    job_id = params.get("jobId")
    cache_key = params.get("cacheKey")
    task_file_key = params.get("generatedFileKey")

    # Get the current files uploaded, from the semaphore
    is_processing_done = False
    total_files = params.get("totalCsvFiles")
    while not is_processing_done:
        time.sleep(1)
        processed_files = dao.RedisConnect.conn.lrange(cache_key, 0, -1)
        is_processing_done = total_files == len(processed_files)

    last_file_processed = processed_files[0].decode('utf-8')
    s3_bucket = pr_config.UPLOADS_BUCKET
    if last_file_processed == task_file_key:
        # Download all files, based on the semaphore
        s3 = boto3.client('s3')
        downloaded_files = []
        for uploaded_file in processed_files:
            basename = uploaded_file.decode('utf-8').split("/")[-1]
            download_path = f"/tmp/{basename}"
            s3.download_file(s3_bucket, uploaded_file.decode('utf-8'), download_path)
            downloaded_files.append(download_path)

        # Zip the downloaded files
        company_name = params.get("companyName")
        filename_prefix = re.sub(r'[^a-z0-9]', '', company_name.lower())
        date_generated = datetime.now().strftime("%Y-%m-%d")
        filename = f'{filename_prefix}_employeemasterfile_{date_generated}.zip'
        zip_file_path = '/tmp/' + filename

        zf = zipfile.ZipFile(zip_file_path, mode='w')
        for downloaded_file_path in downloaded_files:
            basename = downloaded_file_path.split("/")[-1]
            archive_name = "_".join(basename.split("_")[0:-1]) + ".csv"
            zf.write(downloaded_file_path, arcname=archive_name)
        zf.close()

        company_id = params.get("companyId")
        zip_file_key = f'masterfiles/company_{company_id}/{filename}'
        s3.upload_file(
            zip_file_path,
            s3_bucket,
            zip_file_key
        )

        os.remove(zip_file_path)
        for generated_csv_file in downloaded_files:
            os.remove(generated_csv_file)

        res = {
            "task": "zip_masterfile",
            "Bucket": s3_bucket,
            "Key": zip_file_key
        }

        task_result(job_id, res, 0)

def convert_date(date):
    if date:
        if date.__contains__('--') == False:
            date_first = date[0:1]
            if date_first.isalpha() == True:
                str_len = len(date)
                month_abbr = date[0:3]
                day_part = date[4:6]
                year_part = date[7:str_len]
                month_num = mon(month_abbr)
                date = month_num + '/' + day_part + '/' + year_part
        else:
            date = ''

    return date

def mon(i):
    switcher={
        'Jan':'01',
        'Feb':'02',
        'Mar':'03',
        'Apr':'04',
        'May':'05',
        'Jun':'06',
        'Jul':'07',
        'Aug':'08',
        'Sep':'09',
        'Oct':'10',
        'Nov':'11',
        'Dec':'12',
    }
    return switcher.get(i,"Invalid month")
