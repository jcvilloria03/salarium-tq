"""Constants"""
import re

STATUS_NEW = 0
STATUS_VALIDATING = 1
STATUS_VALIDATION_ERRORS_FOUND = 2
STATUS_VALIDATED = 3
STATUS_PROCESSING = 4
STATUS_PROCESSING_ERRORS_FOUND = 5
STATUS_DONE = 6

DURATION_FORMAT = re.compile(r"^\d{1,2}:\d{2}$")
ZERO_DURATION_VALUES = ("0:00", "00:00", '', '0')



# consumer job names
WORKFLOW_VALIDATE_ATTENDANCE_FILE = "validate-attendance-file"
WORKFLOW_VALIDATE_FINAL_ATTENDANCE_FILE = "validate-final-attendance-file"
WORKFLOW_VALIDATE_ALLOWANCE_FILE = "validate-allowance-file"
WORKFLOW_VALIDATE_BONUS_FILE = "validate-bonus-file"
WORKFLOW_VALIDATE_COMMISSION_FILE = "validate-commission-file"
WORKFLOW_VALIDATE_DEDUCTION_FILE = "validate-deduction-file"

WORKFLOW_AS_ATTENDANCE_DATA = "validate-attendance-service-data"
WORKFLOW_COMPUTE_PAYROLL = "compute-payroll"
WORKFLOW_COMPUTE_SPECIAL_PAYROLL = "compute-special-payroll"
WORKFLOW_COMPUTE_FINAL_PAYROLL = "compute-final-payroll"
WORKFLOW_COMPUTE_PROJECTED_FINAL_PAY = "projected-final-payroll"
WORKFLOW_CLOSE_PAYROLL = "close-payroll"
WORKFLOW_REOPEN_PAYROLL = "reopen-payroll"

WORKFLOW_GENERATE_PAYROLL_PAYSLIPS = "generate-payroll-payslips"
WORKFLOW_EMAIL_ZIPPED_PAYSLIPS = "email-zipped-payslips"
WORKFLOW_SEND_PAYSLIPS = "send-payslips"

WORKFLOW_EMAIL_ZIPPED_PAYROLL_REGISTER = "zip-payroll-register"

#non payroll compute related
WORKFLOW_VALIDATE_EMPLOYEE_ALLOWANCE_FILE = "validate-employee-allowance-file"
WORKFLOW_VALIDATE_EMPLOYEE_BONUS_FILE = "validate-employee-bonus-file"
WORKFLOW_VALIDATE_EMPLOYEE_COMMISSION_FILE = "validate-employee-commission-file"
WORKFLOW_VALIDATE_EMPLOYEE_DEDUCTION_FILE = "validate-employee-deduction-file"
WORKFLOW_VALIDATE_EMPLOYEE_ADJUSTMENT_FILE = "validate-employee-adjustment-file"
WORKFLOW_VALIDATE_EMPLOYEE_LOAN_FILE = "validate-employee-loan-file"
WORKFLOW_VALIDATE_EMPLOYEE_ANNUAL_EARNING_FILE = "validate-employee-annual_earning-file"

# salpay to payroll syncing
WORKFLOW_SYNC_DISBURSEMENT = "payrolls-sync-disbursement"

# generate employee masterfile
WORKFLOW_GENERATE_EMPLOYEE_MASTERFILE = "generate-employee-masterfile"
