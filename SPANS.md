# Spans 

## Basics

A span is our basic data structure for representing time spans based on time in/time out OR start time/end time.

To initialize a span:

```python
from tasks.attendance import spans
from tasks.attendance import utils

timein = "08:00"
timeout = "17:00"

sched = spans.create_span(utils.get_span_range(timein, timeout))

```

To initialize spans made up of sequential but disjoint time ranges:

```python
from tasks.attendance import spans
from tasks.attendance import utils

# say you have a list of time in and time out records for the day
time_records = [('08:00', '09:30'), ('10:00', '12:00'), ('13:00', '17:21')]

worked_hours = spans.create_span(
    *tuple([utils.get_span_range(x,y) for (x, y) in time_records]))

```

We can then apply several Set-like/arithmetic operations with the spans.

```python
#we can subtract spans, example regular hours worked
regular = sched.intersection(worked_hours).value 

```

```python
#sample tardy computation
diff = sched - worked_hours

if diff and diff.i == sched.i: #this means there is a tardy span
    tardy = diff[0].value      #get the first span's value

```

TODO: illustrations