pipenv run celery worker -A core.app:app -l debug -l info -n worker_tasks@%h --autoscale=100,10 -P gevent -O fair --without-gossip -Q payroll,celery
