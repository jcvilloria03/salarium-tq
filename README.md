#  Salarium-TQ    
   
Salarium Task Queue and Message Consumer Services - 

# Introduction

TODO: Complete this section

# Installation
```sh
$ pipenv install
```

# Running the Consumer
```sh
pipenv run celery -A core.consumer:consumer worker -l debug -n worker_consumer@%h
```

# Running the Tasks
```sh
pipenv run celery -A core.celery:celery -l debug -n worker_tasks@%h
```

TODO: 

# Development Setup

1.  If no python in system, install Python 3 (preferrably Python 3.7)
2.  [Install *pyenv* (Simple Python Version Management](https://github.com/pyenv/pyenv#installation)

Assuming Ubuntu installation:


```
 $ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
 $ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
 $ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
 $ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bashrc
 $ exec "$SHELL"
```

3. Install *pyenv-virtualenv* plugin

```
$ git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
$ echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
$ exec "$SHELL"
```

4. Install *Python 3.6.4* using pyenv

```
$ pyenv install 3.6.4
```

5. Create virtual environment for use with Salarium-TQ


```
$ pyenv virtualenv 3.6.4 salarium-tq
```

6. Activate virtual environment

```
$ pyenv activate salarium-tq

```

7. Install pipenv (make sure you are inside the salarium-tq virtual environment)

```
$ pip install pipenv

```

8. Clone Salarium-TQ repo into your workspace

```
$ git clone git@github.com:SalariumPH/Salarium-TQ.git
```

9. Install requirements using pipenv:

```
$ cd Salarium-TQ
$ pipenv install
```

# Optional (but Recommended):  Install MS Visual Studio Code

1. [Download MS Visual Studio Code](https://code.visualstudio.com/)

2. Go to Salarium-TQ directory, make sure the salarium-tq virtual env is activated,  and start MS VS Code:

```
(salarium-tq) yourname@yourpc:~/yourworkspace/Salarium-TQ$ code .
```

3. From within MS Visual Studio Code, open the Extensions tab (from left side panel Or Ctrl+Shift+X), search for install the following useful extensions:

    * Python 2018.8.0 (will add Python coding editor confiurations)
    * autoDocstring 0.2.3 (extension for adding of python docstrings)
    * Markdown Preview Enhanced 0.3.5 (so you can preview this document properly, --from file list right click on README.md and then select ```Open in Preview``` in context menu)

4. Check/Setup Settings/Editor Config, File->Preferences->Settings, ```Ctrl+,``` or through the gear icon on the lower left of IDE:
    * Set: ```"terminal.integrated.fontSize": 13,```  (at 14, the underscore is not displayed right in Integrated Terminal).
    * Set:  ```"python.pythonPath": "/home/yourusername/.pyenv/versions/salarium-tq/bin/python",```  Or you can do this from ```Command Palette```-> ```Select Interpreter```
    * Set: ```"python.unitTest.unittestEnabled": true,       "python.unitTest.pyTestEnabled": true,``` or through ```Comman Palette```-> ```Python:Discover Unit Tests```
    * Set" ```"editor.tabSize": 4```,
    ```"editor.detectIndentation": true```,
    ```"editor.insertSpaces": true```
    * Other configs to check:  ```pylint``` options.

5. Familiarize yourself with the MS VS Code ```Command Palette``` and the following commands:
    * Format Document (```Ctrl+Shift+I```): with autopep8 installed (should have been installed earlier with ```pipenv install```), this will cleanup your code -- remove extraneous white spaces, correct indentation, etc.
    * ```Command Palette``` -> ```Discover Unit Tests``` will add ```Run Test|Debug Test``` context options to your unit test classes/methods/functions depending on which unit test framework you enable.


# Unit Testing and Code Coverage

* Code coverage using py-test: 

```sh
% py.test -cov=. tests/
```

