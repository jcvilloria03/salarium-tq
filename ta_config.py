"""Salarium TQ Celery config vars"""
from core.config import getenv, find_env_vars

find_env_vars()
def _clean_slash(val):
    return str(val).rstrip("/ ")

# The Broker URL used by the consumer and tasks runner
BROKER_URL = 'pyamqp://%s:%s@%s:%s/%s' % (getenv('RABBITMQ_LOGIN'),
                                       getenv('RABBITMQ_PASSWORD'),
                                       getenv('RABBITMQ_HOST'),
                                       getenv('RABBITMQ_PORT'),
                                       getenv('RABBITMQ_VHOST'))


# The backend persistence engine. Default is rpc://
CELERY_RESULT_BACKEND = 'rpc://'

# Time and Attendance API domain
TA_API_DOMAIN = _clean_slash(getenv("TA_API_DOMAIN") or '')

# Company API domain
CP_API_DOMAIN = _clean_slash(getenv("CP_API_DOMAIN") or '')

# HRIS API domain
HV_API_DOMAIN = _clean_slash(getenv("HV_API_DOMAIN") or '')

# Timesheet domain
TS_API_DOMAIN = _clean_slash(getenv("TS_API_DOMAIN") or '')

AS_API_DOMAIN = _clean_slash(getenv("AS_API_DOMAIN") or '')

API_EMPLOYEE_BASIC_INFO = 'EMPLOYEE_BASIC_INFO'
API_EMPLOYEE_SHIFTS = 'EMP_SHIFTS'
API_EMPLOYEE_RESTDAYS = 'EMP_REST_DAYS'
API_TIMESHEET = 'TIMESHEET'
API_UPDATE_TIMESHEET = 'TIMESHEET_UPDATE'
API_MULTI_UPDATE_TIMESHEET = 'MULTI_TIMESHEET_UPDATE'
API_TARDY_RULES = 'TARDY_RULES'
API_COMPANY_SHIFTS = 'CO_SHIFTS'
API_HOURS_WORKED = 'HOURS_WORKED'
API_COMPANY_HOLIDAYS = 'CO_HOLIDAYS'
API_COMPANY_DEFAULT_SHIFT = 'CO_DEFAULT_SHIFTS'
API_COMPANY_NIGHT_SHIFT = 'CO_NIGHT_SHIFT'
API_COMPANY_EMPLOYEES = 'CO_EMPLOYEES'
API_COMPANY_RESTDAYS = 'CO_RESTDAYS'
API_COMPANY_TIMESHEET = 'CO_TIMESHEET'
API_ATTENDANCE_RECORDS = 'AS_ATTENDANCE_RECORDS'
API_ATTENDANCE_JOB = 'AS_ATTENDANCE_JOB'
API_ATTENDANCE_JOB_ERRORS = 'AS_ATTENDANCE_JOB_ERRORS'
API_ATTENDANCE_STATUS = 'AS_ATTENDANCE_STATUS'
API_TA_SETTINGS = 'TA_SETTINGS'
API_COMPANY_EMPLOYEES_BY_IDS = 'CO_EMPLOYEES_BY_IDS'
API_COMPANY_SHIFTS_BY_EMPLOYEES = 'EMPLOYEE_SHIFTS'
API_COMPANY_EXPIRE_ATTENDANCE_STATS_CACHE = 'EXPIRE_ATTENDANCE_STATS_CACHE'
API_COMPANY_SHIFTS_ONLY_BY_EMPLOYEES = 'EMPLOYEE_SHIFTS_ONLY'
API_COMPANY_DETAIL = 'API_COMPANY_DETAIL'
API_EMPLOYEES_INTERNAL_IDS = 'API_EMPLOYEES_INTERNAL_IDS'
# The API URI definitions
APIS = {
    API_EMPLOYEE_BASIC_INFO: CP_API_DOMAIN + "/employee/%s",
    API_EMPLOYEE_SHIFTS: TA_API_DOMAIN + '/employee/%s/shifts',
    API_TIMESHEET: TS_API_DOMAIN + '/timesheet/%s',
    API_UPDATE_TIMESHEET: TS_API_DOMAIN + "/log",
    API_MULTI_UPDATE_TIMESHEET: TS_API_DOMAIN + "/log_multi",
    API_TARDY_RULES: TA_API_DOMAIN + '/company/%s/tardiness_rules',
    API_COMPANY_SHIFTS: TA_API_DOMAIN + '/company/%s/shifts_data',
    API_EMPLOYEE_RESTDAYS: TA_API_DOMAIN + '/employee/%s/rest_days',
    API_HOURS_WORKED: TA_API_DOMAIN + '/company/%s/hours_worked',
    API_COMPANY_HOLIDAYS: TA_API_DOMAIN + '/company/%s/holidays',
    API_COMPANY_DEFAULT_SHIFT : TA_API_DOMAIN + '/default_schedule',
    API_COMPANY_NIGHT_SHIFT: TA_API_DOMAIN + '/company/%s/night_shift',
    API_COMPANY_EMPLOYEES: CP_API_DOMAIN  + '/company/%s/employees',
    API_COMPANY_EMPLOYEES_BY_IDS: CP_API_DOMAIN  + '/company/%s/employees_by_ids',
    API_ATTENDANCE_RECORDS: AS_API_DOMAIN  + '/attendance/record',
    API_ATTENDANCE_JOB: AS_API_DOMAIN + '/attendance/job/%s',
    API_ATTENDANCE_JOB_ERRORS: AS_API_DOMAIN + '/attendance/job/%s/errors',
    API_ATTENDANCE_STATUS: AS_API_DOMAIN + '/attendance/%s/%s/lock_status',
    API_COMPANY_EXPIRE_ATTENDANCE_STATS_CACHE: AS_API_DOMAIN + '/attendance/company/%s/attendance_stats/expire_cache',
    API_TA_SETTINGS: TA_API_DOMAIN + '/company/%s/ta_settings',
    API_COMPANY_RESTDAYS: TA_API_DOMAIN + '/company/%s/rest_days',
    API_COMPANY_TIMESHEET: TS_API_DOMAIN + '/company/%s/timesheet',
    API_COMPANY_SHIFTS_BY_EMPLOYEES: TA_API_DOMAIN + '/company/%s/employee_shifts',
    API_COMPANY_SHIFTS_ONLY_BY_EMPLOYEES: TA_API_DOMAIN + '/company/%s/employee_shifts_only',
    API_COMPANY_DETAIL: CP_API_DOMAIN + '/company/%s',
    API_EMPLOYEES_INTERNAL_IDS: CP_API_DOMAIN + '/employee/internal_ids'
}

CELERY_IMPORTS = ('attendance.tasks', 'attendance.filetasks', 'attendance.consumer')

EXCHANGES = {
    "default": {
        "name": "amq.direct",
        "type": "direct"
    }
}

ENV = getenv('ENV_PREFIX')
CREATE_ATTENDANCE_QUEUE = "%s_create_attendance" % getenv('ENV_PREFIX')
CONSUMER_ROUTING_KEY = getenv('CONSUMER_ROUTING_KEY') or 'TQ_CONSUMER'
ATTENDANCE_EXPORT_ROUTING_KEY = getenv('ATTENDANCE_EXPORT_ROUTING_KEY') or 'TQ_ATTENDANCE_EXPORT_ROUTING_KEY'

# Celery specific configuration
CELERY_TASK_RESULT_EXPIRES = 6000
CELERY_ACCEPT_CONTENT = ['json', 'pickle']
CELERY_TASK_SERIALIZER =  "pickle"
CELERY_RESULT_SERIALIZER = "pickle"
CELERY_RESULT_BACKEND = None
CELERYD_PREFETCH_MULTIPLIER = 128

# Disable heartbeat
#BROKER_HEARTBEAT = 300
BROKER_POOL_LIMIT = 20

#AWS specific
AWS_REGION_NAME = getenv('AWS_REGION_NAME')
AWS_ACCESS_KEY_ID = getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = getenv('AWS_SECRET_ACCESS_KEY')

UPLOADS_BUCKET = getenv('UPLOADS_BUCKET')

# Database connection
DB_CONNECTION = getenv("DB_CONNECTION")
DB_HOST = getenv("DB_HOST")
DB_PORT = getenv("DB_PORT")
DB_NAME = getenv("DB_NAME")
DB_USERNAME = getenv("DB_USERNAME")
DB_PASSWORD = getenv("DB_PASSWORD")
MYSQL_POOL_RECYCLE_TIME = 3600
MYSQL_POOL_SIZE = 5
MYSQL_MAX_POOL_SIZE = 10

# Redis specific configuration
REDIS_HOST = getenv("REDIS_HOST")
REDIS_PORT = getenv("REDIS_PORT")
REDIS_DATABASE = getenv("REDIS_DATABASE")

# Chunking settings
EMPLOYEES_CHUNK_SIZE = 500
EMPLOYEES_SMALL_CHUNK_SIZE = 100
ATTENDANCE_CHUNK_SIZE = 20000
SAVE_CHUNK_SIZE = 5
EMPLOYEES_SHIFTS_CHUNK_SIZE = 1
TIMECLOCK_TABLE_NAME = getenv('TIMECLOCK_TABLE_NAME')