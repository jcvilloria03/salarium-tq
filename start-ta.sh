source .env;
pipenv run celery worker -A attendance.app:app -n ta_workers@%%h --autoscale=100,10 -P gevent -O fair --without-gossip -Q celery,"${ENV_PREFIX}_ta_calc_main","${ENV_PREFIX}_ta_calc_save","${ENV_PREFIX}_ta_calc_file","${ENV_PREFIX}_ta_calc_set_attendance","${ENV_PREFIX}_ta_calc_get_employees","${ENV_PREFIX}_ta_calc_set_time_data";
