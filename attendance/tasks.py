"""
Attendance computation workflow tasks.

This follows a conveyor belt production pattern.  The attendance computation requirements
are added on to the result object every step of the way.
"""
import sys
import datetime
import json
import copy
import itertools
import traceback
import time
from functools import wraps

import sqlalchemy
from sqlalchemy import exc
from celery import Task, chain, group
from celery import exceptions
from core import warnings, timeclockutils

import ta_config
from attendance import (calculator, calls, constants, dateutils, keys,
                        models, spans, dao)
from attendance.taskutils import (suppress_traceback, task_params,
                                  update_done_count, chunks, chunk_list,
                                  add_job_task_error, add_job_task_warning,
                                  queue_route)
from core import exceptions, logger
from core.app import app
from attendance.attendance_export import (
    AttendanceExportFile,
    AttendanceExport
)


logger = logger.get_logger()

ATTENDANCE_KEYS = (
    keys.EMP_UID,
    'employee_full_name',
    keys.DATE,
    keys.COMPANY_ID,
    'company_employee_id',
    'locked',
    'first_clock_in',
    'last_clock_out',
    'computed_attendance',
    keys.DEPARTMENT_ID,
    'department_name',
    'rank_id',
    'rank_name',
    keys.POSITION_ID,
    'position_name',
    keys.LOCATION_ID,
    'location_name',
    'scheduled_shifts',
    'attendance_records',
    keys.JOB_ID,
    'scheduled_hours'
)


def _get_default_weekday_shift(data, weekday):
    # get the sched for the weekday
    list_of_scheds = data.get(keys.DATA)
    if not list_of_scheds:
        return None
    for sched in list_of_scheds:
        if sched.get("day_of_week") == weekday:
            return sched
    return None

def _define_shift_day_type(start, end, date):
    """This method defines the shift dates dict. This dict
    determines if a specified shift is in a rest day and holiday

    Args:
        employee (dict) -- The employee attendance dict
        start (string) -- The shift start time
        end (string) -- The shift end time
        date (string) -- The attendance date
    Returns:
        (set) -- The computed next day key
    """
    next_day_key = None
    current = datetime.datetime.strptime(
        date, keys.SQL_DATE_FORMAT)
    if dateutils.convert_to_mins(end) < dateutils.convert_to_mins(start):
        # Current attendance date
        next_day = current + datetime.timedelta(1)
        # Next date or crossed-over date.
        next_day_key = datetime.datetime.strftime(
            next_day, keys.SQL_DATE_FORMAT)
    return current.strftime(keys.SQL_DATE_FORMAT), next_day_key

### ===========================================
### ===========================================
### ===========================================
### ===========================================
### ===========================================
### Here goes the all the task methods
### ===========================================
### ===========================================
### ===========================================
### ===========================================
### ===========================================
@app.task(**task_params("get_employee_info"))
@suppress_traceback
def get_employee_info(att_params):
    """This is task #1 in conveyor.  Get the employee info"""
    emp_uid = att_params.get(keys.EMP_UID)
    # additional checking if company ID already given
    company_id_check = att_params.get('company_id_check', None)
    # get employee data.
    data = calls.get_employee_info(emp_uid)

    if company_id_check is not None and int(data.get("company_id")) != int(company_id_check):
        raise exceptions.TaskError("Company ID mismatch")
    att_params.update(data)
    return att_params


@app.task(**task_params("get_company_settings"))
@suppress_traceback
def get_company_settings(att_params):
    """Adds night shift settings and other settings to att_param"""
    company_id = att_params.get(keys.COMPANY_ID)
    date = att_params.get(keys.DATE)
    if (keys.SHIFT not in att_params and
            keys.DEFAULT_SHIFT not in att_params):  # if there is no shift data
        data = calls.get_co_default_schedule(company_id)
        target_date = att_params.get(keys.DATE)
        # which day of the week is target_date
        date = datetime.datetime.strptime(target_date, "%Y-%m-%d")
        weekday = date.weekday() + 1  # add 1 since Monday=0 in datetime

        # while Monday=1 in vivify API
        target_default_shift = _get_default_weekday_shift(data, weekday)
        att_params[keys.DEFAULT_SHIFT] = target_default_shift

    if keys.NT_SETTINGS not in att_params:
        data = calls.get_night_shift_settings(company_id)
        if data.get("activated", False):
            att_params[keys.NT_SETTINGS] = data

    return att_params


@app.task(**task_params("get_company_nt_settings"))
@suppress_traceback
def get_company_nt_settings(att_params):
    """Task for getting Night shift settings"""
    company_id = att_params.get(keys.COMPANY_ID)
    if keys.NT_SETTINGS not in att_params:
        att_params[keys.NT_SETTINGS] = None

        data = calls.get_night_shift_settings(company_id) or {}
        logger.debug("NT Settings : %s", data)
        if data.get("activated", False):
            att_params[keys.NT_SETTINGS] = data

    return att_params


@app.task(**task_params("get_company_default_shifts"))
@suppress_traceback
def get_company_default_shifts(att_params):
    """Adds night shift settings and other settings to att_param"""
    company_id = att_params.get(keys.COMPANY_ID)
    data = calls.get_co_default_schedule(company_id)
    att_params['default_shifts'] = data
    return att_params


@app.task(**task_params("get_company_default_shifts"))
@suppress_traceback
def get_shifts(att_params):
    """Task: Returns attendance parameter with the shifts info"""
    shifts = calls.get_employee_shift(att_params.get(keys.EMP_UID),
                                      start_date=att_params.get(keys.DATE),
                                      end_date=att_params.get(keys.DATE))
    if shifts:
        att_params[keys.SHIFTS] = shifts  # attach shifts list to att_params
    elif not shifts and att_params.get("default_shifts"):
        data = att_params.get('default_shifts')
        target_date = att_params.get(keys.DATE)
        # which day of the week is target_date
        date = datetime.datetime.strptime(target_date, "%Y-%m-%d")
        weekday = date.weekday() + 1  # add 1 since Monday=0 in datetime
        # while Monday=1 in vivify API
        target_default_shift = _get_default_weekday_shift(data, weekday)
        att_params[keys.DEFAULT_SHIFT] = target_default_shift
        del att_params["default_shifts"]
    else:
        att_params[keys.SHIFTS] = []
    return att_params


@app.task(**task_params("get_emp_tardiness_rule"))
@suppress_traceback
def get_emp_tardiness_rule(att_params):
    """Task: get tardiness rule based on employee info"""
    #raise ValueError()

    company_id = att_params.get(keys.COMPANY_ID, None)
    emp_uid = att_params.get(keys.EMP_UID, None)
    pos_id = att_params.get(keys.POSITION_ID, None)
    dep_id = att_params.get(keys.DEPARTMENT_ID, None)
    loc_id = att_params.get(keys.LOCATION_ID, None)
    tardy_rules = att_params.get('tardiness_rules', [])
    rule = calls.get_tardiness_rule(
        company_id, emp_uid, pos_id, dep_id, loc_id, tardy_rules
    )
    att_params[keys.TARDINESS_RULE] = rule
    if tardy_rules:
        del att_params['tardiness_rules']
    return att_params


@app.task(**task_params("get_hours_worked"))
@suppress_traceback
def get_hours_worked(att_params):
    """Task: get hours worked data"""
    hours_worked = calls.get_hours_worked(att_params.get(keys.COMPANY_ID),
                                          [att_params.get(keys.EMP_UID)],
                                          att_params.get(keys.DATE),
                                          # TODO: this might cause an error
                                          att_params.get(keys.DATE),
                                          # for shifts that overlap days
                                          )
    att_params[keys.HOURS_WORKED] = hours_worked
    return att_params

@app.task(**task_params("get_timesheet"))
@suppress_traceback
def get_timesheet(att_params):
    """Task: get timesheet data if timein method is defined from params shifts"""
    # get shifts data and see if they have  allowed_time_methods
    timesheet_required = att_params.get(keys.TIMESHEET_REQUIRED, False)
    att_params['raw_timesheet'] = []

    # Allow getting of timesheet for TSNR or TSR
    data = calls.get_timesheet(att_params.get(keys.EMP_UID),
                               att_params.get(keys.DATE))
    att_params['raw_timesheet'] = data
    cleaned_pairs = []

    while data and len(data) >= 2:
        first, second, data = data[0], data[1], data[2:]
        if first.get("state") and not second.get("state"):
            cleaned_pairs.extend((copy.deepcopy(first), copy.deepcopy(second)))
        else:
            data.insert(0, copy.deepcopy(second))

    att_params[keys.TIMESHEET] = cleaned_pairs
    return att_params


@app.task(**task_params("get_day_type"))
@suppress_traceback
def get_day_type(att_params):
    """Task:  Get day type"""

    employee_uid = att_params.get(keys.EMP_UID)
    date = att_params.get(keys.DATE)
    # get the shifts and determine the days from shift data
    # i.e. if one of the shifts is an overnight, then effective dates is two days

    shifts = att_params.get(keys.SHIFTS, [])
    #default_shift = att_params.get("default_shift")
    # dict containing rest_day and holiday_type
    att_params[keys.SHIFT_DATES] = {}
    # add current date
    att_params[keys.SHIFT_DATES][date] = {}
    if shifts:
        for shift in shifts:
            start = shift.get(keys.START_TIME)
            end = shift.get(keys.END_TIME)
            if dateutils.convert_to_mins(end) < dateutils.convert_to_mins(start):
                # this means end time is now next morning
                current = datetime.datetime.strptime(
                    date, keys.SQL_DATE_FORMAT)
                next_day = current + datetime.timedelta(1)
                att_params[keys.SHIFT_DATES][datetime.datetime.strftime(
                    next_day, keys.SQL_DATE_FORMAT)] = {}

    if 'rest_days' not in att_params:
        data = calls.get_rest_days(employee_uid)
    else:
        data = att_params.get("rest_days")

    for key in att_params[keys.SHIFT_DATES]:
        restd = models.RestDayModel(employee_uid, key, api_data=data)
        att_params[keys.SHIFT_DATES][key][keys.REST_DAY] = restd.is_rest_day()

    return att_params


@app.task(**task_params("get_holiday_type"))
@suppress_traceback
def get_holiday_type(att_params):
    """Task: Get Holiday type"""

    # get the holiday types for two days
    company_id = att_params.get(keys.COMPANY_ID)
    employee_uid = att_params.get(keys.EMP_UID)
    position_id = att_params.get(keys.POSITION_ID)
    location_id = att_params.get(keys.LOCATION_ID)
    department_id = att_params.get(keys.DEPARTMENT_ID)

    #date = att_params.get(DATE)
    dates = att_params.get(keys.SHIFT_DATES)
    if 'holidays' not in att_params:
        data = calls.get_holidays(company_id)
    else:
        data = att_params.get("holidays")

    for date in dates:

        holiday = models.HolidayModel(company_id, employee_uid,
                                      position_id=position_id,
                                      department_id=department_id,
                                      location_id=location_id,
                                      date=date,
                                      api_data=data)
        att_params[keys.SHIFT_DATES][date][keys.HOLIDAY_TYPE] = holiday()
    if 'holidays' in att_params:
        del att_params['holidays']
    return att_params

@app.task(**task_params("get_company_holidays"))
@suppress_traceback
def get_company_holidays(att_params):
    """Task to return company holidays"""
    company_id = att_params.get(keys.COMPANY_ID)
    holidays = calls.get_holidays(company_id)
    att_params['holidays'] = holidays
    return att_params


@app.task(**task_params("calculate_multi"))
def calculate_multi(ids, dates, pid):
    """Run calculate workflow on all ids vs all dates"""
    print('calculate_multi')
    for emp_id in ids:
        for dt in dates:
            workflow(emp_id, dt, pid)

def workflow(emp_id, att_date, pid=None):
    """Execute worfklow chain for attendance computation"""

    att_params = {
        keys.EMP_UID: emp_id,
        keys.DATE: att_date,
        keys.JOB_ID: pid
    }

    chain(
        get_employee_info.s(att_params),
        get_shifts.s(),
        get_company_settings.s(),
        get_hours_worked.s(),
        get_timesheet.s(),
        get_emp_tardiness_rule.s(),
        get_day_type.s(),
        get_holiday_type.s(),
        compute_attendance.s()
    )()

@app.task(**task_params("get_all_employees"))
@suppress_traceback
def get_all_employees(att_param, update_count=True):
    print('attendance tasks get_all_employees')
    logger.debug("attendance tasks get_all_employees")
    """Get all employees task"""
    company_id = att_param.get(keys.COMPANY_ID)
    job_count = 0
    emp_ids = []
    emps_map = dict()
    dates = att_param.get(keys.DATES, [])
    employee_chunks = []

    # variable for the raw data from a service call
    emps = {}

    # if no employee ids are included then get all employees
    if not att_param.get(keys.IDS, []) and company_id:
        print('attendance tasks get_all_employees if')
        current_page = 0
        per_page = 100
        is_done = False
        employee_data = []
        emps = {"data":[]}
        while is_done is False:
            current_page += 1
            result = calls.get_all_employees(company_id, current_page, per_page)
            if result:
                data = result.get("data", [])
                for item in data:
                    employee_data.append(item)
            current_page = result.get("current_page")
            last_page = result.get("last_page")
            is_done = current_page >= last_page
        emps['data'] = employee_data
    elif company_id and att_param.get(keys.IDS, []):
        print('attendance tasks get_all_employees elif')
        emps_list = []
        ids = att_param.get(keys.IDS)
        # Chunk emp ids to avoid overloading
        chunk_emps_ids = chunk_list(
            ids, ta_config.EMPLOYEES_SMALL_CHUNK_SIZE)
        for emp_ids in chunk_emps_ids:
            payload = {
                keys.IDS: emp_ids
            }
            emps = calls.get_employees_by_ids(company_id, payload)
            emps_list.extend(emps.get(keys.DATA, []))

    if not emps_list:
        raise exceptions.TaskError("Employee(s) not found.")

    emp_chunks = chunk_list(emps_list, ta_config.EMPLOYEES_CHUNK_SIZE)

    # Get company time and attendance settings
    ta_settings = calls.get_company_ta_settings(company_id)
    ta_settings = ta_settings.get(keys.DATA, {})

    for emp_chunk in emp_chunks:
        employee_map = {}
        employee_ids = []

        for emp in emp_chunk:
            emp_id = str(emp.get(keys.ID))
            employee_map[emp_id] = emp
            employee_map[emp_id][keys.EMP_UID] = int(emp.get(keys.ID))

            # Buckets: This will be used as placedholders
            # for the necessary TA data.
            buckets = {
                "rest_days": [], # Prepare rest_day bucket
                "shifts": [], # Prepare shifts bucket
                "default_shift": {}, # Prepare default shift bucket
                "hours_worked": [], # Prepare hours worked bucket
                "tardiness_rules": [], # Prepare bucket for tardiness rules
                "shift_dates": {}, # Prepare bucket for shift dates and holiday flags
                "timesheet": [], # Prepare bucket for timesheet
                "raw_timesheet": [] # Prepare bucket for raw timesheet
            }

            employee_map[emp_id].update(buckets)

            # Set the key for the employee
            employee_ids.append(emp_id)

        job_count += len(list(itertools.product(employee_ids, dates)))

        employee_chunks.append({
            "attendances": set_company_attendance_record({
                    keys.IDS: employee_ids,
                    keys.DATES: dates,
                    keys.EMPLOYEES_DATA: employee_map,
                    keys.JOB_ID: att_param.get(keys.JOB_ID)
                },
                ta_settings),
            keys.NT_SETTINGS: ta_settings.get("night_shift_settings", {}),
            keys.DEFAULT_SHIFT: ta_settings.get("default_shifts", {}),
            keys.TARDINESS_RULE: ta_settings.get(keys.TARDINESS_RULES, []),
            keys.HOLIDAYS: ta_settings.get(keys.HOLIDAYS, {}),
            keys.COMPANY_ID: company_id,
            keys.DATES: dates,
            keys.JOB_ID: att_param.get(keys.JOB_ID)
        })

    # Patch Job stats
    if (att_param.get(keys.JOB_ID) and
            not str(att_param.get(keys.JOB_ID)).startswith("INT") and
            update_count):
        calls.update_job_tasks_count(att_param.get(keys.JOB_ID), job_count)

    group(
        [set_company_attendances.s(g) for g in employee_chunks]).apply_async()

@app.task(**task_params("execute_individual_workflows"))
@suppress_traceback
def execute_individual_workflows(att_param):
    """run workflow for each emp_id"""
    company_id = att_param.get(keys.COMPANY_ID)
    emp_ids = list(att_param.get(keys.IDS))
    del att_param[keys.IDS]
    dates = list(att_param.get(keys.DATES))
    del att_param[keys.DATES]

    #job_id = att_param.get(keys.JOB_ID)

    for emp in emp_ids:
        iterate_employee_date.apply_async(
            args=(dict(att_param), emp, company_id, dates))


@app.task(**task_params("iterate_employee_date"))
def iterate_employee_date(att_params, emp_uid, company_id, dates):
    """Task to run calculation of an employee per date"""
    employee_info = calls.get_employee_info(emp_uid)
    if (employee_info.get(keys.COMPANY_ID) is not None and
            int(employee_info.get(keys.COMPANY_ID)) != company_id):
        raise exceptions.TaskError("Company ID mismatch")
    att_params.update(employee_info)

    # get rest day models
    emp_rest_days = calls.get_rest_days(emp_uid)
    att_params['rest_days'] = emp_rest_days

    for dt in dates:
        calculate_company_employee.apply_async(
            args=(emp_uid, dt, dict(att_params)))


@app.task(**task_params("calculate_company_employee"))
def calculate_company_employee(emp_uid, att_date, att_params):
    """Task for computing attendance assuming company info is now provided."""
    att_params[keys.DATE] = att_date
    att_params[keys.EMP_UID] = emp_uid

    chain(
        get_shifts.s(att_params),
        get_hours_worked.s(),
        get_timesheet.s(),
        #get_tardiness_rule.s(),
        get_day_type.s(),
        get_holiday_type.s(),
        compute_attendance.s()
    )()

    #calculate_multi.apply_async(args=(emp_ids, dates, job_id))

@app.task(**task_params("calculate_company"))
def calculate_company(company_id, ids, att_dates, job_id, update_count=False):
    """Execute workflow chain for attendance computation company-wide"""
    print('attendance tasks calculate_company')
    att_param = {
        keys.DATES: att_dates,
        keys.COMPANY_ID: company_id,
        keys.JOB_ID: job_id,
        keys.IDS: ids
    }

    get_all_employees.apply_async(args=(att_param, update_count))

@app.task(**task_params("calculate_imported_data"))
def calculate_imported_data(company_id, employee_ids, employees, att_dates, job_id, update_count=False,
                            imported_codes=False, mapping=False):
    att_params = {
        keys.DATES: att_dates,
        keys.COMPANY_ID: company_id,
        keys.JOB_ID: job_id,
        keys.IDS: employee_ids,
        keys.EMPLOYEES_DATA: employees,
        keys.IMPORTED_CODES: imported_codes,
        "mapping": mapping
    }

    job_count = len(employee_ids) * len(att_dates)
    calls.update_job_tasks_count(job_id, job_count)

    chain(
        get_company_ta_settings.s(att_params) |
        set_company_attendances.s()
    ).apply_async()

@app.task(**task_params("set_time_bound_data"))
def set_time_bound_data(att_params):
    """Task: This task fills-in the time bound data or
    data that were requested by the employee like
    filed requests (hours worked), timesheet data (clock-in/out)
    or time disputes.

    Args:
        att_params (dict) -- A dict of attendances
            passed by the previous task.
    """
    chain(
        get_company_hours_worked.s(att_params) |
        get_company_timesheet.s() |
        calculate_this_company.s()
    ).apply_async()

@app.task(**task_params("get_company_ta_settings"))
@suppress_traceback
def get_company_ta_settings(att_params):
    """Task: This task retrieves all the company time and attendance related settings

    Args:
        att_params (dict) -- The passed task arguments originated from the
            first item in the conveyor belt
    Returns:
        (dict) -- The att_params with the latest data updates.
    """
    company_id = att_params.get(keys.COMPANY_ID)
    ta_settings = calls.get_company_ta_settings(company_id)
    ta_settings = ta_settings.get(keys.DATA, {})

    # Add the tardiness rules to each employee
    attendance_records = set_company_attendance_record(att_params, ta_settings)
    att_params["attendances"] = attendance_records

    # We can now opt to remove the employee data and
    # night shift settings.
    del att_params[keys.EMPLOYEES_DATA]

    # We can remove the tardiness rules settings since
    # all employees should have their respective tardiness rule
    att_params[keys.DEFAULT_SHIFT] = ta_settings.get("default_shifts", {})

    # Holidays data
    att_params[keys.HOLIDAYS] = ta_settings.get(keys.HOLIDAYS, {})
    return att_params

@app.task(**task_params("set_company_attendances"))
@suppress_traceback
def set_company_attendances(att_params):
    """Task: This task retrieves all shifts of employees in a company.
    From the get company shifts api response, the shift schedule, shift
    dates and rest days will be attached to the employee profile `att_params['employees']`.

    The criteria to include a shift of an employee depends on the defined
    shift effectivity date. Shifts are included in the employee object
    if and only if it's shift effectivity date is within range of the
    requested dates for calculation.

    Args:
        att_params (dict) -- The passed task arguments originated from the
            first item in the conveyor belt
    Returns:
        (dict) -- The att_params with the latest data updates.
    """

    employee_ids = list(set([
        attendance['id'] for attendance in att_params.get("attendances", [])
    ]))
    dates = att_params.get(keys.DATES)
    company_id = att_params.get(keys.COMPANY_ID)
    all_shifts, custom_rest_days = calls.get_company_shifts_and_rest_days(
        company_id,
        employee_ids,
        dates
    )
    nt_settings = att_params.get(keys.NT_SETTINGS)
    tardy_settings = att_params.get(keys.TARDINESS_RULE, None)
    shifts_data = all_shifts.get(keys.DATA, [])
    default_shifts = att_params.get(keys.DEFAULT_SHIFT, {})
    attendances = att_params.get("attendances", [])
    request_dates = att_params.get(keys.DATES, [])
    holidays = att_params.get(keys.HOLIDAYS, {})

    if not tardy_settings and company_id:
        # Get tardy settings for the last time before processing each attendance
        tardy_settings = calls.get_company_tardiness_rules(company_id)
    # Iterate through the employees list to attach their respective shifts
    #for employee in att_params.get(keys.EMPLOYEES_DATA, []):

    # This loops searches for an employees id if exists in
    # the employee shifts data (all_shifts)
    for attendance in attendances:
        emp_id = attendance["id"]
        date = attendance["date"]
        assigned_shifts = []
        previous_shift = None
        is_default_shift = False

        current_date = datetime.datetime.strptime(date, keys.SQL_DATE_FORMAT)
        previous_date = datetime.datetime.strftime(current_date - datetime.timedelta(1), keys.SQL_DATE_FORMAT)

        # Get user defined rest days
        # Prepare the dict for storing user defined rest days
        attendance[keys.REST_DAYS_DATA] = [urd for urd in custom_rest_days\
            if date in urd["dates"] and str(emp_id) == str(urd["employee_id"])]

        # Prepare the dict for storing shift dates
        attendance.update({ keys.SHIFT_DATES: { date: {} } })

        # check if employee id has a shift by comparing the "id"
        # key in shifts. id is the employee id of the employee.
        employee_shift = [dict(s) for s in shifts_data \
            if int(emp_id) == int(s[keys.EMPLOYEE_ID])]

        #if not employee_shift:
        #    continue

        # Check if there are assigned shifts to the employee
        # If there is, extract the schedule data.
        if employee_shift:
            employee_shift = employee_shift[0]
            # Check the dates to compute if existing in the
            # shifts assigned to this employee. if those are existing,
            # append those into the employee's shift.
            for shift in employee_shift.get(keys.SHIFTS, []):
                # From the set of dates included in the shift object,
                # check the date in this current attendance if exists in shift.dates
                # list. If yes, append that shift to this employee, this is means that
                # the requested date for calculation has a shift.
                if date in shift["dates"]:
                    shift[keys.SCHEDULE]["shift_id"] = shift["id"]
                    assigned_shifts.append(shift.get(keys.SCHEDULE, {}))

                # Check previous shift
                # if exists will be added as previous shift
                if previous_date in shift["dates"]:
                    previous_shift = shift.get(keys.SCHEDULE, {}).copy()
                    # Assign start timestamp using previous date
                    previous_shift['start_timestamp'] = dateutils.datetime_to_secs(
                        '%s %s' % (previous_date, previous_shift.get('start_time'))
                    )

                    # Assign end timestamp using previous date
                    previous_shift['end_timestamp'] = dateutils.datetime_to_secs(
                        '%s %s' % (previous_date, previous_shift.get('end_time'))
                    )

                    # Assign end timestamp when the timestamp went to other date
                    if previous_shift['end_timestamp'] < previous_shift['start_timestamp']:
                        previous_shift['end_timestamp'] = dateutils.datetime_to_secs(
                            '%s %s' % (date, previous_shift.get('end_time'))
                        )

        # assign previous shift
        attendance[keys.PREVIOUS_SHIFT] = previous_shift

        # assign the extracted shift
        attendance[keys.SHIFTS] = assigned_shifts

        # set night shift settings
        attendance[keys.NT_SETTINGS] = nt_settings

        # Check if  employee is entitled tardy,
        # since tardy settings already set from ta_settings, this will skip web call process
        emp_tardy_settings = []
        if tardy_settings:
            emp_tardy_settings = calls.get_tardiness_rule(
                company_id=attendance[keys.COMPANY_ID],
                emp_uid=emp_id,
                position_id=attendance[keys.POSITION_ID],
                department_id=attendance[keys.DEPARTMENT_ID],
                location_id=attendance[keys.LOCATION_ID],
                data=tardy_settings)

        attendance[keys.TARDINESS_RULE] = emp_tardy_settings

        # If there are no custom shifts, this employee's schedule or
        # shift is considered to be a default schedule
        if not attendance.get(keys.SHIFTS, []):
            is_default_shift = True
            # Consider checking the default shift if there's no user
            # defined shift.
            # If there's none, process and parse the default shift
            # data
            date_obj = datetime.datetime.strptime(date, "%Y-%m-%d")

            # Get the corresponding default shift based on passed weekeday
            emp_def_shift = _get_default_weekday_shift(default_shifts, date_obj.weekday() + 1)

            # Then attach the default shift to the employee.
            attendance[keys.DEFAULT_SHIFT] = emp_def_shift

            # Verify that the start and end time of a shift is crossing over
            # or night shift. This snippet will provided the relative
            # dates to the the night shift setting.
            days = _define_shift_day_type(emp_def_shift[keys.WORK_START][:5],
                emp_def_shift[keys.WORK_END][:5], date)

            # Placeholder for the shift dates. Fill in the shift dates. Shift dates
            # are composed of the current date and next date (which was added in
            # _define_shift_day_type
            is_rest_day = lambda _dt: emp_def_shift[keys.DAY_TYPE] == keys.REST_DAY \
                and _dt == date

            attendance[keys.SHIFT_DATES] = {i: \
                {keys.REST_DAY: is_rest_day(i)} for i in days if i}
        else:
            # Determine the day type values by iterating through the employee's
            # assigned shifts
            for shift in attendance[keys.SHIFTS]:
                # Verify that the start and end time of a shift is crossing over
                # or night shift. This snippet will provided the relative
                # dates to the the night shift setting.
                _, next_day_key = _define_shift_day_type(
                    shift[keys.START_TIME], shift[keys.END_TIME], date)

                if next_day_key:
                    attendance[keys.SHIFT_DATES][next_day_key] = {}

        # Determine user defined rest days and holiday
        # types in the shift dates dict for a default or
        for key in attendance[keys.SHIFT_DATES]:
            # user defined shift
            holiday_code = None

            # Determine that the current date in shift_dates is/are rest day.
            # Only determine this rest day key if there's a user defined shift
            # because, the default shift above already handles rest day.
            if attendance[keys.REST_DAYS_DATA]:
                # We will use the RestDayModel to determine if the current date
                # is a rest day or not.
                restd = models.RestDayModel(
                    emp_id, key, api_data={
                        keys.DATA: attendance[keys.REST_DAYS_DATA]
                    })
                # Store the rest day result.
                attendance[keys.SHIFT_DATES][key][keys.REST_DAY] = restd.is_rest_day()
            elif not attendance[keys.SHIFT_DATES][key].get(keys.REST_DAY, None):
                # This means that there's no rest day key. If there's none,
                # set the default value as False
                attendance[keys.SHIFT_DATES][key][keys.REST_DAY] = False

            # Let's determine the holiday type now
            if holidays:
                holiday = models.HolidayModel(
                    attendance[keys.COMPANY_ID], emp_id, date=key, api_data=holidays,
                    position_id=attendance[keys.POSITION_ID],
                    department_id=attendance[keys.DEPARTMENT_ID],
                    location_id=attendance[keys.LOCATION_ID])
                holiday_code = holiday()

            # Store holiday value if any
            attendance[keys.SHIFT_DATES][key][keys.HOLIDAY_TYPE] = holiday_code

        # Since we are no longer using this payload, might as well delete it
        #if 'rest_days' in attendance:
        #   del attendance['rest_days']


    #set_time_bound_data.apply_async(args=(att_params,))
    # Divide all attendances into chunks
    att_groups = chunk_list(
        attendances, ta_config.ATTENDANCE_CHUNK_SIZE)

    ## Formalize the chunked attendances. Insert
    ## parent key-values like the attendance dates
    ## company id, and job id
    imported_codes = None
    mapping = None
    if 'imported_codes' in att_params:
        imported_codes = att_params['imported_codes']
    if 'mapping' in att_params:
        mapping = att_params['mapping']
    att_groups = [{
            keys.DATES: att_params[keys.DATES],
            keys.COMPANY_ID: att_params[keys.COMPANY_ID],
            keys.JOB_ID: att_params[keys.JOB_ID],
            "attendances": att,
            keys.IMPORTED_CODES: imported_codes,
            "mapping": mapping or None
        } for att in att_groups]

    ## Pass all the chunked data into threads
    ## on a same task simultaneously.
    ## This approach should increase effeciency at the cost
    ## of RAM.
    group(
        [set_time_bound_data.s(g) for g in att_groups]).apply_async()

def set_employee_tardiness_rules(employee, ta_settings):
    """This method gets the tardiness rules of a company and assigns it to each
    employee.

    Args:
        att_params (dict) -- The passed task arguments originated from the
            first item in the conveyor belt
    Returns:
        (dict) -- The att_params with the latest data updates.
    """
    tardiness_rules = []
    rules = ta_settings.get(keys.TARDINESS_RULE, [])

    # Employee related fields. These will be used
    # in the condition to determine if an employee
    # is eligible for a tardiness rule.
    emp_uid = employee.get(keys.EMP_UID, None)
    pos_id = employee.get(keys.POSITION_ID, None)
    dep_id = employee.get(keys.DEPARTMENT_ID, None)
    loc_id = employee.get(keys.LOCATION_ID, None)

    # Iterate through the employees list
    for rule in rules:
        # Get the affected employees data. We want this, because
        # we want to check if the current employee is affected
        # by any tardiness rules. Tardiness rules apply by
        # employee specific definitions like: position id, location id and
        # department id.
        tardy_rules = rule.get(keys.AFFECTED_EMPLOYEES)
        for t_rule in tardy_rules:
            # Tardiness rules related fields
            _type = t_rule.get(keys.TYPE, None)
            _id = t_rule.get(keys.ID, None)
            _name = t_rule.get(keys.NAME, None)

            # Now, check if current employee data applies to
            # a tardiness rule group.
            emp_rules = calls.is_rule_applicable(
                t_rule, emp_uid, pos_id,
                dep_id, loc_id)

            # If yes, attach the rule to the employee
            if emp_rules:
                tardiness_rules.append(t_rule)

    return tardiness_rules

def set_company_attendance_record(att_params, ta_settings):
    print('attendance tasks set_company_attendance_record')
    """This method multiplies the consolidated info of each employee
    by attendance date. The computation will follow the formula nCr!.
    At this point, the consolidated data will now be called an "attendance data".

    Args:
       att_params (dict) -- The passed task arguments originated from the
           first item in the conveyor belt
    Returns:
        (dict) -- The att_params with the latest data updates.
    """
    attendance_records = []
    emp_ids = att_params.get(keys.IDS, [])
    dates = att_params.get(keys.DATES, [])
    employees = att_params.get(keys.EMPLOYEES_DATA, [])

    # Get the product of employees and dates. We want to do this
    # to lessen loops.
    attendances = list(itertools.product(emp_ids, dates))
    print('attendances')
    print(attendances)
    # Build the "attendance record". A definition of a real attendance
    # record should contain employee info, employee TA settings and
    # company TA settings.
    for item in attendances:
        # Employee id
        emp_id = item[0]
        # Attendance date
        date = item[1]

        # Get employee info using the emp_id
        employee = employees[str(emp_id)]

        # Attach the remaining TA settings
        att_record = {
            keys.DATE: date,
            "night_shift_settings": att_params.get(
                keys.NT_SETTINGS, {}),
            "tardiness_rule": set_employee_tardiness_rules(
                employee, ta_settings),
            "job_id": att_params.get(keys.JOB_ID, {}),
        }
        print('att_record')
        print(att_record)
        # Create a new attendance object with the
        # consolidated settings plus the current employee info
        # Since mapping of employee id is applied on each employee data,
        # we can retrieve the employee info by just referencing its employee
        # id in the `attendances` KEYs.
        att_record.update(employee)

        # Create the final object.
        attendance_records.append(att_record)
        print('attendance_records')
        print(attendance_records)

    # The finished attendance record
    return attendance_records

@app.task(**task_params("set_company_hours_worked"))
@suppress_traceback
def get_company_hours_worked(att_params):
    """This tasks gets and parses all employees' filed requests within
    the given company. Retrieved data are then formatted and attached
    into each respective employee in `att_params`.

    This is also-known-as "HOURS WORKED".

    Args:
        att_params (dict) -- The passed task arguments originated from the
            first item in the conveyor belt
    Returns:
        (dict) -- The att_params with the latest data updates.
    """
    # Use the api call `calls.get_company_filed_requests` to retrieve
    # employees' filed requests (hours worked).

    # The filter date ranges to get the hours worked data.
    # The start date to retreive from
    dates = [i["date"] for i in att_params.get("attendances", [])]
    if dates:
        # Make sure dates are sorted in ascending order
        dates.sort(key = lambda date: datetime.datetime.strptime(date, '%Y-%m-%d'))

    #start_date = att_params.get(keys.DATES)[0]
    start_date = dates[0]
    # The end date to retreive from
    #end_date = att_params.get(keys.DATES)[len(att_params[keys.DATES]) - 1]
    end_date = dates[len(dates) - 1]

    hours_worked = calls.get_hours_worked(
        att_params.get(keys.COMPANY_ID), att_params.get(keys.IDS),
        start_date, end_date)

    employees = att_params.get(keys.EMPLOYEES_DATA, [])
    for att in att_params.get("attendances",[]):
        employee_requests = []
        for request in hours_worked:
            emp_id = str(request[keys.EMP_ID])
            # if the current request is owned by an employee,
            # check the `date` in the request if exists in our
            # requested dates.
            if int(emp_id) == int(att[keys.ID]) and request.get(keys.DATE, None) and \
            request[keys.DATE] == att[keys.DATE]:
                employee_requests.append(request)
        att["hours_worked"] = employee_requests

    return att_params

@app.task(**task_params("get_company_timesheet"))
@suppress_traceback
def get_company_timesheet(att_params):
    """Task: get timesheet data if timein method is defined from params shifts"""
    # get shifts data and see if they have  allowed_time_methods
    company_id = att_params.get(keys.COMPANY_ID)
    dates = att_params.get(keys.DATES)
    dates.sort()
    start_date = dates[0]
    end_date = dates[len(dates) - 1]
    employee_ids = list(set([
        attendance['id'] for attendance in att_params["attendances"]]))
    timeclock_model = timeclockutils.TimeClockModel()
    timesheets = timeclock_model.get_employee_timesheet(employee_ids, start_date, end_date)
    # Iterate through the attendances to retreive their respective
    # timesheets.
    for att in att_params.get("attendances", []):
        max_clockout_rule = att.get(keys.MAX_CLOCKOUT)  # type dict or null

        cleaned_pairs = []
        att_emp = int(att[keys.EMP_UID])
        att_date = att[keys.DATE]

        # Get employee's timesheet by comparing the attendance employee id and
        # timesheet employee id
        raw_timesheet = [s for s in timesheets if att_emp == int(s[keys.EMP_UID])]

        # Sort employee's timesheet and create the attendance's raw
        # timesheet bucket.
        raw_timesheet = sorted(raw_timesheet, key=lambda x: x[keys.TIMESTAMP])

        att['raw_timesheet'] = raw_timesheet

        # Add 2 days from the attendance date to get a range of timesheets
        # from the attendance `date` + 2 days buffer.
        current_date_timestamp = int(time.mktime(time.strptime(att_date, "%Y-%m-%d")))
        consider_timestamp_allowance = constants.PRE_SHIFT_START_TIMESHEET_HOUR_ALLOWANCE * 60 * 60
        consider_timestamp_from = current_date_timestamp - consider_timestamp_allowance
        consider_timestamp_to = current_date_timestamp + constants.TWO_DAYS_IN_SECONDS

        # Lambda function to convert a timestamp into date YYYY-MM-DD then
        # compares it against the timesheets.
        # Retrieve only the timesheet that's in-range with the attendance date
        # plus 2 days buffer
        emp_timesheet = [a for a in raw_timesheet if consider_timestamp_from <= a[keys.TIMESTAMP] <= consider_timestamp_to]

        while emp_timesheet and len(emp_timesheet) >= 2:
            first = emp_timesheet[0]
            second = emp_timesheet[1]
            emp_timesheet = emp_timesheet[2:]
            if first.get("state") and not second.get("state"):
                old_max_clockout_state = second.get('max_clock_out')  # True/False/None
                if max_clockout_rule:
                    duration_seconds = int(second.get('timestamp', 0)) - int(first.get('timestamp', 0))
                    duration_minutes =  (duration_seconds / 60) if duration_seconds else 0
                    duration_hours = (duration_minutes / 60) if duration_minutes else 0
                    max_clock_out_hours = max_clockout_rule.get('hours')
                    if all([max_clock_out_hours and duration_hours, float(duration_hours) > float(max_clock_out_hours)]):
                        # max clock out reached
                        second['max_clock_out'] = True
                    else:
                        second['max_clock_out'] = False
                else:
                    # if no max clockout rule is assiged to employee
                    # then by default set max_clock_out to false
                    second['max_clock_out'] = False

                if old_max_clockout_state != second['max_clock_out']:
                    # Only update dynamo db time record if 'max_clock_out'
                    # state of attendance has changed
                    timeclock_model.update_timerecord(second)

                # only add if duration is more than 1 minute to avoid span intersection error
                duration_seconds = int(second.get('timestamp', 0)) - int(first.get('timestamp', 0))
                duration_minutes =  (duration_seconds / 60) if duration_seconds else 0

                if (duration_minutes >= 1):
                    cleaned_pairs.extend((copy.deepcopy(first), copy.deepcopy(second)))
            else:
                emp_timesheet.insert(0, copy.deepcopy(second))

        att[keys.TIMESHEET] = cleaned_pairs

    # Since this is the final data processing, we will
    # remove any unnecessary data that's not
    # needed for saving
    if keys.DATES in att_params:
        del att_params[keys.DATES]

    if "company_info" in att_params:
        del att_params["company_info"]

    if keys.IDS in att_params:
        del att_params[keys.IDS]

    return att_params

def calculate(att_params):
    print('calculate??????????????????????????????????????????????????')
    """This method performs the actuall time and attendance calculation
    given the required recipes and a wellformed json structure

    Args:
        att_params (dict) -- A dict of attendance data to compute
    """
    # Calculate the given data by using the calculator lib
    calc = calculator.get_calculator(att_params)  # , country=country_code)
    # Set the default value for a blank dict for non calculated attendance
    attendance = {} if not calc.attendance else calc.attendance  # type: dict
    print('calculate?????????????????????????????????????????????????? attendance')
    print(attendance)
    # Log if returned attendance is empty but has shifts for given date
    if not attendance and att_params.get(keys.SHIFTS, []):
        logger.info("Empty attendance calculated from: %s", att_params)

    computed_attendance = ""

    # Transform full name
    att_params["employee_full_name"] = att_params.get("full_name")
    att_params["company_employee_id"] = att_params.get("employee_id")
    att_params["locked"] = False

    total_computed_attendance = 0
    shifts_ca = {}

    # Format the accepted assigned shifts value for the front-end
    # to understand
    for key in attendance:
        current = attendance.get(key)  # type: dict
        shift_computed_attendance = 0
        for att_key in current.keys():
            current_value = current.get(att_key, 0)

            if att_key not in (constants.ATT_TARDY,
                               constants.ATT_ABSENT,
                               constants.ATT_UNDERTIME,
                               constants.ATT_OVERBREAK
                               ):
                shift_computed_attendance += current_value
                total_computed_attendance += current_value

        shifts_ca[key] = shift_computed_attendance

    # convert total_computed_attendance to HH:MM
    computed_attendance = dateutils.mins_to_readable_hours(
        total_computed_attendance
    )

    print('calculate?????????????????????????????????????????????????? computed_attendance')
    print(computed_attendance)

    # Assign the computed attendance key with its value taken
    # from get_calculator
    att_params["computed_attendance"] = computed_attendance

    # add to att_params
    att_params["attendance_records"] = attendance

    # transform shifts to scheduled_shifts:
    shifts = calc.ordered_shifts
    scheduled_shifts = []

    # if this shifts array is not empty, it is considered
    # that the current attendance is assigned with a shift.
    for shift in shifts:
        shift_id = shift.get(keys.SHIFT_ID)
        shift_type = shift.get(keys.TYPE, keys.SH_FIXED)

        shift_timesheet = calc.get_shift_timesheet(str(shift_id))
        shift_raw_timesheet = calc.get_shift_raw_timesheet(str(shift_id), shift_type)

        shift.update({
            keys.TIMESHEET: shift_timesheet or {},
            keys.RAW_TIMESHEET: shift_raw_timesheet or shift_timesheet or {},
            "computed_attendance": shifts_ca.get(str(shift_id), 0),
        })
        scheduled_shifts.append(shift)

    # @DEPRECATED: if rest-day work is detected:
    if calc.get_shift_timesheet("RD"):
        scheduled_shifts.append({
            keys.SHIFT_ID: "RD",
            keys.NAME: "Rest Day",
            keys.TIMESHEET: calc.get_shift_timesheet("RD")
        })

    default_schedule = calc.get_default_shift()

    # If there's a user defined shift, we should prioritize
    # this as a default schedule but with rest_day type
    # and with incomplete details
    if calc.is_user_defined_rest_day():
        default_schedule = calc.get_user_defined_rest_day()

    att_params["scheduled_shifts"] = {
        "assigned_shifts": scheduled_shifts,
        "default_shift": default_schedule,
        "shift_dates": att_params.get("shift_dates", None),
        # calcute for the scheduled hours
        "scheduled_hours": calc.get_scheduled_hours()
    }

    # att_params["first_clock_in"] = calc.first_clockin
    # att_params["last_clock_out"] = calc.last_clockout

    #TODO: remove
    if att_params.get(keys.LOCATION_ID, None) is None:
        att_params[keys.LOCATION_ID] = 1
        att_params["location_name"] = "Unspecified"

    # Since we are no longer using this payload, might as well delete it
    if 'rest_days' in att_params:
        del att_params['rest_days']

    # transform att_params to remove unneeded keys for payload.
    param_keys = list(att_params.keys())
    for key in param_keys:
        if key not in ATTENDANCE_KEYS:
            del att_params[key]
    return att_params

@app.task(**task_params("calculate_this_company"))
def calculate_this_company(att_params):
    """Save the passed attendances in batches and simultaneously

    Args:
        att_params (dict) -- A dict of attendances to save.
    """
    company_id = att_params.get(keys.COMPANY_ID, 0)

    attendances = att_params.get("attendances", [])
    job_id = att_params.get(keys.JOB_ID, None)

    chunks(
        attendances, ta_config.SAVE_CHUNK_SIZE,
        calculate_company_attendance, company_id, job_id, att_params['imported_codes'],
        att_params['mapping']).apply_async()

@app.task(**task_params("calculate_company_attendance"))
@suppress_traceback
def calculate_company_attendance(attendance_data, company_id, job_id, imported_codes=False, mapping=False):
    """This task iterates through the list of attendances
    """
    print('calculate_company_attendance')
    att_params = copy.deepcopy(attendance_data)
    jobs = []
    created_ids = []
    done = 0
    job_id = job_id
    added = []
    calculated = []
    updated = 0
    warnings = 0
    no_action = 0
    expire_cache = False
    current_date = datetime.datetime.now().strftime(keys.SQL_DATE_FORMAT)

    with dao.create_session() as session:
        # Retrieve the employee record for determining if
        # attendance is locked and if there are columns for updating
        stmt = session.query(models.AttendanceRecords).filter(
            models.AttendanceRecords.employee_uid.in_(
                list(set(e[keys.EMP_UID] for e in att_params))),
            models.AttendanceRecords.date.in_(
                list(set(d[keys.DATE] for d in att_params))))
        records = stmt.all()

        for att in att_params:
            if att.get(keys.JOB_ID, None):
                del att[keys.JOB_ID]

            try:
                print('calculate_company_attendance calculate')
                data = calculate(att)
                calculated.append(data)
            except Exception as e:
                add_job_task_error({
                    keys.JOB_ID: job_id,
                    keys.MESSAGE: str(e),
                    keys.DATE: att[keys.DATE],
                    keys.EMP_UID: att[keys.EMP_UID]
                })
                logger.info("Error was caused by: %s", att)
                logger.exception(e)
                continue

            # Some declaration of employee id and attendance date
            employee_uid = data["employee_uid"]
            date = data["date"]

            if date == current_date:
                expire_cache = True

            record = None
            for item in records:
                if str(employee_uid) == str(item.employee_uid) and \
                str(date) == str(item.date):
                    record = item

            # Create a job key with employee id and date combination
            # this will be used for logging purposes only
            job_key = "%s:%s" % (employee_uid, date)
            # We are uploading this in memory so that there will be only
            # one logging for a batch.
            created_ids.append(job_key)

            if not record:
                # This should not happen
                data["attendance_records"] = json.dumps(data["attendance_records"])
                data["scheduled_shifts"] = json.dumps(data["scheduled_shifts"])
                logger.info("Added: %s:%s" % (job_id, str(data)))
                added.append(models.AttendanceRecords(**data))
            elif record.locked:
                # If record is locked, don't bother updating it.
                # leave it as is otherwise, try to update the record
                # because there might be new values.
                msg = "Record %s is locked therefore, no action" % job_key
                add_job_task_warning({
                    keys.JOB_ID: job_id,
                    keys.MESSAGE: msg,
                    keys.DATE: date,
                    keys.EMP_UID: employee_uid
                })
                logger.warning(msg)
                warnings += 1
                if imported_codes:
                    update_done_count(job_id)
                continue
            else:
                # del data["date"]
                del data["employee_uid"]
                comparisons = []
                total_mins = 0
                new_attendance_codes = {}
                # Proceed with overriding the attendance codes if these variables we're set
                if imported_codes and mapping:
                    # Process each imported attendance codes
                    for i in imported_codes:
                        # Place each imported codes to each att_dates that matches
                        if (
                            (i["employee_id"]) == int(employee_uid)
                            and i["date"] == data["date"]
                            and i["hours_worked"]
                        ):
                            # Convert each attendance code to abbreviations
                            for r in i["hours_worked"]:
                                if keys.KEY_CONST_MAP.get(r["type"], None):
                                    # Finally convert the hour to mins
                                    mins = dateutils.convert_to_mins(r["time"])
                                    new_attendance_codes[keys.KEY_CONST_MAP.get(r["type"])] = mins
                                    total_mins = total_mins + mins
                            break
                    # Proceed to replace if imported codes were converted successfully
                    att_records_list = list(data['attendance_records'])
                    if new_attendance_codes:
                        if len(att_records_list) == 0:
                            att_records_list.append(
                                data['scheduled_shifts']['default_shift']['id']
                            )

                        data['attendance_records'] = {
                            att_records_list[0]: new_attendance_codes
                        }

                        # Lock the att date to finalize override
                        data['locked'] = True
                        data['computed_attendance'] = dateutils.mins_to_readable_hours(total_mins)

                del data["date"]
                # Dynamically compare all fields in the retrieved record vs the data to
                # replace. if there are no, changes, might as well skip updating
                for key, val in data.items():
                    if key in ["attendance_records", "scheduled_shifts"]:
                        comparisons.append(json.loads(getattr(record, key)) == val)
                    elif key == "locked":
                        comparisons.append(bool(getattr(record, key)) == bool(val))
                    elif key == "computed_attendance":
                        db_col = getattr(record, key)
                        param_val = val
                        try:
                            is_equal = dateutils.convert_to_mins(str(db_col)) == dateutils.convert_to_mins(param_val)
                        except:
                            is_equal = False
                        comparisons.append(is_equal)
                    else:
                        comparisons.append(getattr(record, key) == val)
                # If all comparisons or conditions are False, this means
                # that the record is eligble for updating
                if not all(comparisons):
                    logger.info("Updating: %s:%s" % (job_id, str(data)))
                    data["attendance_records"] = json.dumps(data["attendance_records"])
                    data["scheduled_shifts"] = json.dumps(data["scheduled_shifts"])
                    session.query(models.AttendanceRecords).filter(
                        models.AttendanceRecords.id == record.id).update(data)
                    session.commit()
                    updated += 1
                    logger.info("Updated: %s:%s" % (job_id, job_key))
                else:
                    no_action += 1

            # Update the job count
            update_done_count(job_id)

        try:
            logger.info("Finished: %s: %s of Total" % (job_id, len(created_ids)))
            if added:
                session.add_all(added)
                session.commit()

            session.close()
            logger.info("%s - Added: %s" % (job_id, str(len(added))))
            logger.info("%s - Updated: %s" % (job_id, str(updated)))
            logger.info("%s - Locked: %s" % (job_id, str(warnings)))
            logger.info("%s - No Action: %s" % (job_id, str(no_action)))

            if expire_cache:
                logger.info("Expire Attendance Stats Cache for Company ID: {co_id}".format(co_id=company_id))
                calls.expire_attendance_stats_cache(company_id)

        except exc.OperationalError as e:
            session.rollback()
            code, msg = e.orig.args
            logger.exception(e)
            raise exceptions.PeerError(msg)
        except exc.IntegrityError as e:
            # Catch the error code of the thrown exception
            # if the error code is equal to 1062 or Duplicate
            # entry error, try to update the record but first
            # we have to rollback the previous session to recycle
            # connection.
            session.rollback()
            code, msg = e.orig.args
            session.rollback()
            logger.exception(e)
            raise exceptions.PeerError(msg)
        except Exception as e:
            logger.exception(e)
            raise exceptions.TaskError(str(e))
    return

@app.task(**task_params("compute_attendance"))
@suppress_traceback
def compute_attendance(final_params):
    print('compute_attendanceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee')
    """TASK:  compute attendance data and save it via api call"""
    #country_code = att_params.get("country")
    #copy final params to att_params so data remains intact if retried
    att_params = copy.deepcopy(final_params)
    logger.debug(json.dumps(att_params))

    try:
        print('compute_attendance calculate')
        att_params = calculate(att_params)
    except Exception as e:
        add_job_task_error({
            keys.JOB_ID: att_params[keys.JOB_ID],
            keys.MESSAGE: str(e),
            keys.DATE: att_params[keys.DATE],
            keys.EMP_UID: att_params[keys.EMP_UID]
        })
        logger.info("Error was caused by: %s", att_params)
        logger.exception(e)
        raise exceptions.TaskError(str(e))
    # Perform the calculation

    # delete job id if it begins with INT:
    if str(att_params.get(keys.JOB_ID, "")).startswith("INT:"):
        del att_params[keys.JOB_ID]

    # @DEPRECATED: Webcall to create an attendance is disabled.
    # instead used
    # calls.create_attendance(att_params)

    # New way is to save this directly
    save_attendance.apply_async(args=(att_params,))

@app.task(**task_params("save_attendance"))
@suppress_traceback
def save_attendance(data):
    """This task iterates through the list of
    """
    jobs = []
    created_ids = []
    done = 0
    job_key = None
    job_id = data.get(keys.JOB_ID, None)
    del data[keys.JOB_ID]

    # Some declaration of employee id and attendance date
    employee_uid = data.get("employee_uid", None)
    date = data.get("date", None)

    with dao.create_session() as session:
        # Create a job key with employee id and date combination
        # this will be used for logging purposes only
        job_key = "%s:%s" % (employee_uid, date)

        # Check if the
        record = session.query(
            models.AttendanceRecords.id,
            models.AttendanceRecords.locked).filter(
                models.AttendanceRecords.employee_uid == employee_uid,
                models.AttendanceRecords.date == date).first()

        data["attendance_records"] = json.dumps(data["attendance_records"])
        data["scheduled_shifts"] = json.dumps(data["scheduled_shifts"])
        if not record:
            # This should not happen
            attendance = models.AttendanceRecords(**data)
            session.add(attendance)
            logger.info("Added: %s:%s" % (job_id, str(data)))
        elif record.locked:
            # If record is locked, don't bother updating it.
            # leave it as is otherwise, try to update the record
            # because there might be new values.
            msg = "Record %s is locked therefore, no action" % job_key
            add_job_task_warning({
                keys.JOB_ID: job_id,
                keys.MESSAGE: msg,
                keys.DATE: date,
                keys.EMP_UID: employee_uid
            })
            logger.warning(msg)
            update_done_count(job_id)
            return
        else:
            del data["date"]
            del data["employee_uid"]

            session.query(models.AttendanceRecords).filter(
                models.AttendanceRecords.id == record.id).update(data)

        update_done_count(job_id)
        logger.info("Updated: %s:%s" % (job_id, json.dumps(data)))

        try:
            logger.info("Finished: %s 1 of 1" % job_id)
            session.commit()
            session.close()
        except exc.OperationalError as e:
            # Retry for any operational errors. Operational errors
            # means that the queries associated in a transaction,
            # didn't ran at all.
            session.rollback()
            code, msg = e.orig.args
            logger.exception(e)
            raise exceptions.PeerError(msg)
        except exc.IntegrityError as e:
            # Catch the error code of the thrown exception
            # if the error code is equal to 1062 or Duplicate
            # entry error, try to update the record but first
            # we have to rollback the previous session to recycle
            # connection.
            #session.rollback()
            code, msg = e.orig.args
            session.rollback()
            logger.exception(e)
            raise exceptions.PeerError(msg)
        except Exception as e:
            logger.exception(e)
            raise exceptions.TaskError(str(e))
    return


@app.task(**task_params("export_attendance_records"))
def export_attendance_records(job_id, params):
    """
    params: {
        "job_id": "attendance_export:b31e73b4-5876-4573-977b-29891262ed5c",
        "company_id": 241,
        "filters": {
            "employee_status": [
            "active"
            ]
        },
        "employee_status": [
            "active"
        ],
        "ids": [],
        "sort_by": {
            "date": "ASC",
            "employee_name": "ASC"
        },
        "start_date": "2023-06-12",
        "end_date": "2023-07-12"
    }
    """
    AttendanceExport.set_job_status(job_id, AttendanceExport.PROGRESS)
    try:
        company = calls.get_company_detail(params['company_id'])
        tmp_filename = '/tmp/' + AttendanceExport.generate_tmp_file_name(
            params['start_date'],
            params['end_date'],
            company.get('name'),
        )
        export_attendance_to_csv(params, tmp_filename)

        # Upload generated file to s3
        s3_link = AttendanceExport.upload_file(tmp_filename)
        AttendanceExport.set_job_status(job_id, AttendanceExport.DONE, result=s3_link)
        AttendanceExport.delete(tmp_filename)
    except exceptions.NoAttendanceRecordsError as e:
        logger.exception(e)
        AttendanceExport.set_job_status(
            job_id,
            AttendanceExport.ERROR,
            str(e)
        )
    except Exception as e:
        logger.info("Error occured while exporting attendance: %s", params)
        logger.exception(e)
        AttendanceExport.set_job_status(
            job_id,
            AttendanceExport.ERROR,
            "Some issue occured while exporting attendance records"
        )
        raise exceptions.TaskError(str(e))


def export_attendance_to_csv(params, tmp_filename):
    """
    Write attendance records to the csv file in chunks
    """

    # Fetch attendance codes of all records to write them on headers
    attendance_codes = AttendanceExportFile.get_attendance_codes(params)
    append = False
    for records in AttendanceExport.get_attendance_records(params):
        export_file = AttendanceExportFile(file_path=tmp_filename, data=records, attendance_codes=attendance_codes, append=append)
        export_file.export()
        append = True
