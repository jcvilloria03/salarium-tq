"""Models"""
import json

from attendance import constants, spans, calls, dateutils

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import TypeDecorator


DeclarativeBase = declarative_base()


class json_type(TypeDecorator):
    impl = sa.String

    def process_literal_param(self, value, dialect):
        return json.dumps(value)


class date_type(TypeDecorator):
    impl = sa.String

    def process_literal_param(self, value, dialect):
        return value


class time_type(TypeDecorator):
    impl = sa.String

    def process_literal_param(self, value, dialect):
        """noop"""
        return str(value)

    def process_bind_param(self, value, dialect):
        """noop"""
        return str(value)


class datetime_type(TypeDecorator):
    impl = sa.String

    def process_literal_param(self, value, dialect):
        return value


# class BaseModel(object):  #This class brings no value to the model structure
#     def __init__(self):
#         pass
class AttendanceRecords(DeclarativeBase):

    __tablename__ = "attendance_records"

    id = sa.Column('id', sa.Integer, primary_key=True)
    employee_uid = sa.Column('employee_uid', sa.Integer)
    employee_full_name = sa.Column('employee_full_name',
        sa.String)
    date = sa.Column('date', date_type())
    company_id = sa.Column('company_id', sa.Integer)
    company_employee_id = sa.Column('company_employee_id',
        sa.String)
    locked = sa.Column('locked', sa.Integer)
    computed_attendance = sa.Column('computed_attendance',
        sa.String)
    department_id = sa.Column('department_id', sa.Integer)
    department_name = sa.Column('department_name', sa.String)
    rank_id = sa.Column('rank_id', sa.Integer)
    rank_name = sa.Column('rank_name', sa.String)
    position_id = sa.Column('position_id', sa.Integer)
    position_name = sa.Column('position_name', sa.String)
    location_id = sa.Column('location_id', sa.Integer)
    location_name = sa.Column('location_name', sa.String)
    scheduled_shifts = sa.Column('scheduled_shifts',
        json_type())
    attendance_records = sa.Column('attendance_records',
        json_type())
    created_at = sa.Column('created_at', sa.DateTime)
    updated_at = sa.Column('updated_at', sa.DateTime)

    def __getitem__(self, key):
        return getattr(self, key)


class DefaultScheduleModel(object):
    """This Model houses the business logic on how to determine the default
    schedule of a company. This can be also use to get an employee's schedule
    if there are no defined employee shifts/schedule.

    Usage:

    Constructor arguments:
        * company_id - The company_id
        * api_data (optional) - If this is provided, the api call in this class
        will not execute thus, using the provided value of `api_data`.

    Get default schedule
    $ def_sched = HolidayModel(company_id=1)

    The returned values is the same as
    $ def_sched()

    Properties:
        timein - integer. start time of this shift in mins.
        timeout - integer. end time of this shift in mins.
        total_shift_hours - the total shift hours in mins.
        day_of_week - The day of the week in integer. Possible values:
            * 1 - Monday
            * 2 - Tuesday
            * 3 - Wednesday
            * 4 - Thursday
            * 5 - Friday
            * 6 - Saturday
            * 7 - Sunday
        shift_breaks[] - Array of breaks objects entitled to this employee.
            Properties:
                duration - integer. The duration of a break in mins.
                start_time - integer. Start time of a break in mins.
                end_time - integer. End time of a break in mins.
    """

    def __init__(self, company_id, api_data=None):
        """Constructor
        """

        # This is the service call to the company default schedule api
        self.schedule = api_data

        if not api_data:
            result = calls.get_co_default_schedule(company_id)
            self.schedule = result["data"] if result and result.get(
                "data", None) else None

    def __call__(self):
        shift_list = []

        if not self.schedule:
            return shift_list

        for sched in self.schedule:
            start_break = dateutils.strip_seconds(sched["work_break_start"])
            end_break = dateutils.strip_seconds(sched["work_break_end"])
            break_hours = dateutils.get_time_difference(start_break, end_break)
            breaks = [{
                "start": dateutils.strip_seconds(sched["work_break_start"]),
                "end": dateutils.strip_seconds(sched["work_break_end"]),
                "break_hours": break_hours
            }]

            start_time = dateutils.strip_seconds(sched["work_start"])
            end_time = dateutils.strip_seconds(sched["work_end"])
            total_hours = dateutils.get_time_difference(start_time, end_time)

            shift_data = {
                "start_time": dateutils.strip_seconds(sched["work_start"]),
                "end_time": dateutils.strip_seconds(sched["work_end"]),
                "breaks": breaks,
                "type": sched["day_type"],
                "day_of_week": sched["day_of_week"],
                "total_hours": total_hours
            }

            shift_list.append(Shift(shift_data))

        return shift_list


class RestDayDescriptor(object):
    """Descriptor
    """

    def __init__(self, item):
        self.__dict__.update(item)


class RestDayModel(object):
    """This class holds the business logic and encapsulation for rest days.

    Usage:

    Instantiate this class first with employee id and
    the date to check with

    # Constructor arguments
        * employee_id - The employee_id.
        * rest_date - The  date to check.
        * api_data (optional) - If this is provided, the api call in this class
        will not execute thus, using the provided value of `api_data`.
    ex.
    restd = RestDayModel(232, "2018-01-01")

    # Check if this given date is a rest day. Returns a boolean
    restd.is_rest_day()

    # You can also use this to retrieve all rest day records of
    # an employee.

    $ restd()

    # Returns an array of objects <RestDayModel object>

    RestDayModel
    - start_date (str) start date string
    - end_date (str) end date string
    - id (int) id of this rest day
    - company_id (int) company id of this rest day

    Ex.
    [
        <RestModelDay object>,
        <RestModelDay object>,
    ]
    """

    def __init__(self, employee_id, rest_date, api_data=None):
        """Constructor

        @param{int} `employee_id` The employee id
        @param{string} `start_date` The starting date to filter with.
        @param{string} `end_date` The end date to filter with. Optional,
                                  because there can be only one date check
                                  for rest entitlement.
        """

        # Invoke the day type service to get the list of date types assigned to this
        # employee
        result = api_data

        # We don't need this anymore
        if not api_data:
            result = calls.get_rest_days(employee_id)

        # Declare passed args
        self.__date = rest_date

        # Since the response is always an array and returns only
        day_type_data = result["data"] if result and result.get(
            "data", None) else None

        # Formatting dict into object
        self.__rest_days = []
        if day_type_data:
            self.__rest_days = [RestDayDescriptor(r) for r in day_type_data]

    def is_rest_day(self):
        """This method checks if the supplied dates are
        tagged as a rest day.

        @return{set}
        """
        is_rest = False

        if not self.__rest_days:
            return is_rest

        # Iterate through each rest days and check
        # if the supplied dates are rest days
        for r in self.__rest_days:
            is_repeat = hasattr(r, "repeat")
            # Given a start_date and end_date, check if those are in range
            # for each record's `r.start_date` and `r.end_date`.
            #
            # comparison r.sd <= gs and r.ed >= ge
            #
            if r.start_date <= self.__date and r.end_date >= self.__date:
                # if record is has no repeating rest days, determine
                # if the supplied dates are in range from the record's
                # start_date and end_date
                if not is_repeat:
                    is_rest = True
                else:
                    # Get day names for each supplied dates. Then look in the
                    # `repeat.rest_days` if these day names exists.
                    rest_days = r.repeat["rest_days"]

                    # Check supplied dates if day name falls under a rest day.
                    if rest_days:
                        # is start_date equal to the items in `rest_days`
                        is_rest = dateutils.get_day_name(
                            self.__date) in rest_days
                break

        return is_rest

    def __call__(self):
        """Returns all rest days given the employee id in object form
        """
        return self.__rest_days


class HolidayModel(object):
    """This Model houses the business logic on how to determine the entitled
    holidays of an employee

    Usage:

    # Constructor arguments:
        * employee_id - The employee id.
        * date - The date of the data to retrieve.
        * company_id - The company id.
        * position_id - The position id of the employee.
        * department_id - The department id on where the employee is under.
        * location_id - The location id of where the employee is residing.
        * api_data (optional) - If this is provided, the api call in this class
        will not execute thus, using the provided value of `api_data`.

    # Get entitled holidays
    $ holiday = HolidayModel(1, employee_uid="2018-10-10")

    # Return the holiday code in string
    $ holiday()

    # Produces
    $ "RH+SH"

    # Possible outputs for the combination of RH (Regular Holiday) and SH (Special
    Holiday).

    Format:

    <number of occurrences><regular holiday code>+<number of occurences><special holiday code>

    Ex.
    1. RH
    2. SH
    3. 2RH+2SH
    4. 2SH
    5. 2RH

    etc...

    """
    # TODO: Respec this model
    # Holiday info will be queried only once and then provide employee_id, position_id, dept, etc
    # to check

    def __init__(self, company_id, employee_id=None,
                 position_id=None, department_id=None,
                 location_id=None, date=None, api_data=None):
        """Constructor
        """
        self.employee_id = employee_id
        self.position_id = position_id
        self.department_id = department_id
        self.location_id = location_id
        self.date = date

        self.__holidays = api_data
        # This is the service call to the holidays service
        if not api_data:
            self.__holidays = calls.get_holidays(company_id)

    def set_code(self, data):
        """This method forms the necessary holiday codes
        based on the passed data.

        @param{dict} `data` The data which contains the holiday
                            name and an array of its data.
        """
        holiday_code = ""
        #codes_store = []
        codes_table = {}

        # Get the 2 char and declare it as key value pair
        # where value is the array length of `value`
        for key, value in data.items():
            hcode = constants.CODES_TABLE["HOLIDAY"][key.upper()]
            codes_table[hcode] = len(value)

        # sort codes by A-Z
        sort_keys = sorted(codes_table.keys())

        # Now construct the string!
        for item in sort_keys:
            cnt = "" if codes_table[item] <= 1 else codes_table[item]
            holiday_code += str(cnt) + item + "+"

        return holiday_code[:len(holiday_code) - 1]

    def search(self, haystack):
        """Search the passed position_id, location_id, department_id,
        and employee_uid in this constructor. Searched values
        will be returned by this method as an array

        @param{list} `haystack`
        """
        bucket = []

        # Loop thourgh the passed `haystack` to search from
        for item in haystack:
            # get the needle values from this class' instance
            needle = getattr(self, "%s_id" % item["type"], None)

            # Do the comparision
            if needle and item["id"] == needle:
                bucket.append(item)
                break
            elif item["id"] is None:
                bucket.append(item)
                break
        return bucket

    def __call__(self):
        """Get the entitled holidays given the passed arguments in the
        constuctor
        """
        found = {}
        if not self.__holidays:
            return None

        # Loop through response data from holidays endpoint
        for j in self.__holidays["data"]:
            # compare the passed date. if has match start
            # the search
            if self.date == j["date"]:
                bucket = self.search(haystack=j["affected_employees"])
                # Append found items in its respective
                # holiday types stored in `bucket`
                if bucket:
                    found[j["type"]] = found.get(j["type"], []) + bucket

        if not found:
            return None

        return self.set_code(found)


class Break(object):
    """Breaks descriptor"""

    def __init__(self, break_data):
        """Constructor

        @param{dict} `break_data` The break data
        """
        self.type = None
        self.break_hours = None
        self.start = None
        self.end = None

        # EMP Schedule
        self.__dict__.update(break_data)

    def __call__(self):
        """The break type of.
        @Descriptor

        @return{string}
        """
        return self.type

    @property
    def duration(self):
        """The required duration of this break.

        @return{int}
        """
        return dateutils.convert_to_mins(self.break_hours)

    @property
    def start_time(self):
        """The required starting time of this break.

        @return{int}
        """
        return dateutils.convert_to_mins(self.start)

    @property
    def end_time(self):
        """The required end time of this break

        @return{int}
        """
        return dateutils.convert_to_mins(self.end)


# class Breaks(object):
#     """Breaks parent class"""

#     def __init__(self, break_data):
#         """Constructor

#         @param{dict} `break_data` The break data
#         """
#         # EMP Schedule
#         self._breaks = [BreaksDescriptor(i) for i in break_data]


class Shift():
    """Shift parent class"""

    def __init__(self, shift_data):
        """Constructor

        @param{string} `data` The  shift data
        """
        # The starting and end date of this schedule
        #self.start = start_date
        #self.end = end_date
        self.breaks = []
        self.type = None
        self.core_times = []
        self.start_time = None
        self.end_time = None
        self.allowed_time_methods = []
        self.total_hours = None
        # EMP Schedule
        self.__dict__.update(shift_data)

        # super().__init__(self.breaks)
        if self.breaks:
            self._breaks = [Break(i) for i in self.breaks]

    def __call__(self):
        """Retrieves the shift type of this employee.

        @return{string}
        """
        return self.type

    @property
    def core_hours(self):
        """Retrieves the shift type of this employee.

        @return{string}
        """
        core_times = []
        for t in self.core_times:
            core_times.append({
                "id": t["id"],
                "start": dateutils.convert_to_mins(t["start"]),
                "end": dateutils.convert_to_mins(t["end"])
            })

        return core_times

    @property
    def shift_breaks(self):
        """The breaks assigned to this employee

        @return{list}
        """
        return self._breaks

    @property
    def timein(self):
        """Retrieves the start time of the scheduled shift of this employee.

        @return{int}
        """
        return dateutils.convert_to_mins(self.start_time)

    @property
    def timeout(self):
        """Retrieves the end time of the scheduled shift of this employee.

        @return{int}
        """
        return dateutils.convert_to_mins(self.end_time)

    @property
    def total_shift_hours(self):
        """Retrieves the total hours of the scheduled shift of this employee.

        @return{int}
        """
        return dateutils.convert_to_mins(self.total_hours)

    @property
    def time_methods(self):
        """Retrieves the allowed time methods of this employee.

        @return{list}
        """
        return self.allowed_time_methods

    @property
    def is_required_clocking(self):
        """Returns a boolean value if this shift requires the employee
        to clock-in/clock-out

        @return{bool}
        """
        return bool(self.allowed_time_methods)


class EmployeeShiftModel(object):
    """This Model is a child class of Shifts and Breaks.
    This houses the data encapsulation and business logic
    for employee shifts and breaks.

    Usage:

    # Class instance accepts employee_uid, start date and end date.
    # start date and end date are filter values to determine the employee's shift

    # Constructor arguments:
        * employee_id - The employee id
        * start_date - Starting date range.
        * end_date - Ending date range.
        * api_data (optional) - If this is provided, the api call in this class
        will not execute thus, using the provided value of `api_data`.

    emp = EmployeeShiftModel(172, start_date="2018-10-10", end_date="2018-10-10")

    emp() | emp.shifts - Array of shift objects that can contain these possible values

    # Properties:

        timein - integer. start time of this shift in mins.

        timeout - integer. end time of this shift in mins.

        total_shift_hours - the total shift hours in mins.

        time_methods - the possible time methods that this employee
            is entitled to. Possible values: Web bundy, Bundy clock etc.

        is_required_clocking - Returns True or False. True if time_methods is
            is not null or blank otherwise, False.

        shift_breaks[] - Array of breaks objects entitled to this employee.

            Properties:

                duration - integer. The duration of a break in mins.

                start_time - integer. Start time of a break in mins.

                end_time - integer. End time of a break in mins.

    """

    #shifts = set()

    def __init__(self, employee_id, start_date,
                 end_date, api_data=None):
        """Constructor

        @param{int} `employee_id` The employee id
        @param{string} `start_date` The starting date to filter with.
        @param{string} `end_date` The end date to filter with.
        """
        result = api_data

        if not api_data:
            result = calls.get_employee_shift(employee_id, start_date=start_date,
                                              end_date=end_date)

        # Since the response is always an array and returns only
        #shift_data = result["data"] if result and result.get("data", None) else None
        self.shifts = set()
        if result:
            self.shifts = set(Shift(shift) for shift in result)

    def __call__(self):
        """Caller method: Returns the shifts of this employee in list of obj"""
        return self.shifts

    @property
    def scheduled_hours(self):
        """This method gets the sum of all total hours for all shifts

        @return{int}
        """
        return sum([i.total_shift_hours for i in self.shifts])


class HoursWorkedDescriptor(object):
    """This model houses the busines logic of hours worked.

    # Constructor arguments
        * company_id - The company_id
        * employee_ids - A list of employee ids
        * start_date - The starting date of a date range.
        * end_date - The ending date of a date range.
        * api_data (optional) - If this is provided, the api call in this class
        will not execute thus, using the provided value of `api_data`.

    Usage:
    hw = HoursWorked(company_id (int), employee_ids (list), start_date (str), end_date (str))

    hw() - Returns an array of objects with the ff. properties.

        Properties:
            hours_worked_id - ID of an hours worked record in the database.
            employee_uid - employee id. pertains to the owner of an hours worked
                record.
            hours_worked_type - Type hours worked. Possible values
                * paid_leave
                * unpaid_leave
                * regular
                * night_shift
                * overtime
                * undertime
                * overtime_night_shift
            start_date_time - starting hours worked datetime string.
            end_date_time - ending hours worked datetime string.
            hours_worked_code - The hours worked type in code format.
            spans - The computed spans based from start_datetime and end_datetime.
    """

    def __init__(self, item):
        self.__item = item

    @property
    def hours_worked_id(self):
        """Hours worked ID

        @return{int}
        """
        return self.__item["id"]

    @property
    def employee_uid(self):
        """Employee  ID

        @return{int}
        """
        return self.__item["employee_id"]

    @property
    def hours_worked_type(self):
        """Returns the record type. Possible values are:

        * paid_leave
        * unpaid_leave
        * regular
        * night_shift
        * overtime
        * undertime
        * overtime_night_shift

        @retrun{str}
        """
        return self.__item["type"]

    @property
    def start_date_time(self):
        """End date time

        @return{str}
        """
        return self.__item["start_datetime"]

    @property
    def end_date_time(self):
        """Start date time

        @return{str}
        """
        return self.__item["end_datetime"]

    @property
    def code(self):
        """Returns the hours worked code dependeing on the
        hours worked type.

        Equivalents:

        * paid_leave - PAID LEAVE
        * unpaid_leave - UNPAID LEAVE
        * regular - REGULAR
        * night_shift - NT
        * overtime - OT
        * undertime - UH
        * overtime_night_shift - NT+OT

        @retrun{str}
        """
        return constants.CODES_TABLE[self.__item["type"].upper()]

    @property
    def span(self):
        """This computes the total minutes from the range of
        start_datetime - end_datetime. if neither of start_datetime and
        end_datetime have valid values, use the time field for computation.

        Warning: Getting spans based from time only is not accurate compared
        to date + time.

        @return{int}
        """
        start_dt = self.__item["start_datetime"]
        end_dt = self.__item["end_datetime"]
        hw_time = self.__item["time"]

        # This should not happen, no start_datetime, end_datetime and time
        if not start_dt and not end_dt and not hw_time:
            return None

        # If either or both of start_datetime and end_datetime
        # are None, use the time field otherwise, return 0
        if (not start_dt or not end_dt) and hw_time:
            return spans.create_value_span(dateutils.convert_to_mins(hw_time))

        # The most applicable scenario that can happen
        # return dateutils.datetime_range_to_mins(start_dt, end_dt)
        return spans.create_span((dateutils.datetime_to_mins(start_dt),
                                  dateutils.datetime_to_mins(end_dt)))


class HoursWorkedModel(object):
    """Hours Worked"""

    def __init__(self, company_id, employee_ids=None, start_date=None,
                 end_date=None, api_data=None):
        """Constructor
        """
        self.employee_ids = employee_ids
        self.start_date = start_date
        self.end_date = end_date

        # This is the service call to the holidays service
        self.__hours_worked = api_data

        if api_data is None:
            self.__hours_worked = calls.get_hours_worked(company_id, employee_ids,
                                                         start_date, end_date)

    def __call__(self):
        """This returns the hours worked with CODES and computed spans.

        @return{list}
        """
        result = []
        if not self.__hours_worked:
            return result

        for hours_work in self.__hours_worked:
            result.append(HoursWorkedDescriptor(hours_work))

        return result


class HourCode(object):
    """HourCode"""
    def __init__(self, is_rest=False, htype=None, is_nt=False, is_ot=False):
        self._is_rest = is_rest  # type: bool
        self._is_holiday = htype is not None  # type: bool
        self._htype = htype  # type: str
        self._is_nt = is_nt
        self._is_ot = is_ot

    def __str__(self):
        code = ""
        if self._is_rest:
            code = constants.ATT_RD

        if self._is_holiday:
            code = code + "+" + self._htype

        if self._is_nt:
            code = code + "+" + constants.ATT_NT

        if self._is_ot:
            code = code + "+" + constants.ATT_OT

        if code.startswith("+"):
            code = code[1:]
        return code if code else constants.ATT_REGULAR

    def is_rest_day(self):
        """Return True if HourCode object denotes a workday"""
        return self._is_rest

    def is_holiday(self):
        """Return True if HourCode object is a holiday (any)"""
        return self._is_holiday

    def is_regular_holiday(self):
        """Return True if HourCode denotes an RH type"""
        if self._htype:
            return constants.ATT_RH in self._htype
        return False

    def is_special_holiday(self):
        """Return True if HourCode denotes an SH type"""
        if self._htype:
            return constants.ATT_SH in self._htype
        return False

    def is_night_shift(self):
        """Return True if HourCode denotes a nightshift"""
        return self._is_nt

    def is_overtime(self):
        """Return True if OT"""
        return self._is_ot
