"""Calculator module

An attendance calculator takes in a dictionary parameter which is the result of the
data conveyor belt result (see attendance.tasks).

An attendance calculator must have a compute() method which returns a dictionary containing
the day/hour types and corresponding minute value.
"""
import datetime
import os
import logging
import json
import re
#import pytz

from attendance import (
    dateutils,
    spans,
    constants,
    models,
    keys,
    factories
)
from attendance.models import RestDayModel

logger = logging.getLogger(__name__)

if os.environ.get("TQ_DEBUG", "").upper() == "TRUE":
    logger.setLevel(logging.DEBUG)

class DefaultCalculator(object):
    """Default calculator implementation"""

    def __init__(self, att_params):
        self.params = att_params
        self._attendance = {}

        # convenience attribute
        self.current_date = self.params.get(keys.DATE)
        current = datetime.datetime.strptime(
            self.current_date, keys.SQL_DATE_FORMAT)
        next_day = current + datetime.timedelta(1)
        self.next_date = datetime.datetime.strftime(
            next_day, keys.SQL_DATE_FORMAT)

        # Flag if the current computed attendance is
        # in a rest day
        self.has_rest_day_work = False

        self._previous_shift = att_params.get('previous_shift')

        # day spans
        self._day_span1 = self._create_day_span(self.current_date)
        self._day_span2 = self._create_day_span(self.next_date)
        # setup the spans
        self._nt_span = None
        self._nt_span1 = None
        self._nt_span2 = None
        self._time_shift = None
        self._ts_span = None
        self._raw_ts_span = None
        self._ts_lookup = {}
        # unpaid leave span
        self._ul_spans = {}  #
        # paid leave span
        self._pl_spans = {}  # type: spans.Span
        # regular hours entry
        self._regular_spans = {}
        self._night_shifts_spans = {}
        self._overtime_spans = {}
        self._scheduled_overtime_spans = {}
        self._overtime_night_shift_spans = {}
        self._undertime_spans = {}
        # none time span based hours_worked
        self._override_items = {}

        # Create global spans:
        # a. Night shift settings
        # b. Hours worked (Leaves, OT, UT etc..)
        # c. Timesheet

        self.is_scheduled_overtime_entitled = self._check_if_entitled_to_scheduled_overtime()
        self._create_global_spans()

        self._first_clockin = None
        self._last_clockout = None
        self._shift_ts_lookup = {}
        self._shift_dates = self.params.get(keys.SHIFT_DATES, {})

        self._holiday_entitlements = (
            self.params.get(keys.EN_RH_PAY, False),
            self.params.get(keys.EN_SH_PAY, False))

        # Create an ordered list of shifts ordered by their start time
        self._ordered_shifts = sorted(self.params.get(keys.SHIFTS, []),
            key=lambda shift: dateutils.convert_to_mins(shift['start_time']))

        self._shifts_index = {
            str(shift['shift_id']): index for index, shift in enumerate(self._ordered_shifts)
        }

        # Set date hired
        self._compute()

    def _user_defined_rd(self):
        """This method extracts the user defined object
        if the current attendance date is within
        the list of the user defined rest dates.

        Returns:
            (string) -- The date that exists in rest days.
        """
        # Get user defined rest days and put in a list
        udrd = self.params.get('rest_days', {})
        user_rds = []

        if udrd:
            for _rd in udrd:
                if self.current_date in _rd["dates"]:
                    return _rd
        return None

    def is_user_defined_rest_day(self):
        """This method returns True if the the current date is
        considered as a user defined rest day

        Returns:
            (bool) -- If True, current date is rest day otherwise False
        """
        return bool(self._user_defined_rd())

    def _is_date_restday(self, employee_uid, date):
        # check if the provided date is a rest day or not
        if employee_uid and date:
            restd = RestDayModel(employee_uid, date)
            return restd.is_rest_day()
        return False

    def _shift_allowed_on_holidays(self, shift):
        return shift.get("on_holidays", False)

    def _holidays_work_allowed(self, has_rendered_hours, shift):
        """
        If current shift is holiday then check if employee is alowed
        to render hours on holiday
        """
        return all([
            self._shift_dates[self.current_date].get("holiday_type"),
            has_rendered_hours,
            # employee are not allowed to render hours on holidays if there is no custom shift
            not shift.get("default") # check if its default shift
        ])

    def _rest_day_work_allowed(self, has_rendered_hours, shift):
        """
        If current shift is restday then check if employee is alowed
        to render hours on restday
        """
        return all([
            self._shift_dates[self.current_date].get("rest_day", False),
            has_rendered_hours,
            shift.get("on_rest_day"),
            self.params.get('rest_day_pay', False)
        ])

    def _holiday_and_rest_day_work_allowed(self, has_rendered_hours, shift):
        """
        If current shift is restday+holiday then check if employee is alowed
        to render hours on restday+holiday
        """
        return self._rest_day_work_allowed(has_rendered_hours, shift)\
            and self._holidays_work_allowed(has_rendered_hours, shift)

    def _calculate_break_span_for_over_break(self, break_span, total_break_time: int, over_break: int):
        """
        break_span: break span taken by employee
        total_break_time: total break time allowed
        over_break: over break time taken by employee
        """
        taken = 0
        for _break in break_span:
            taken += _break.value
            if taken > total_break_time:
                break_span -= spans.create_span([_break.j - over_break, _break.j])
                break
        return break_span

    def _calculate_break_span_for_under_break(self, break_span, under_break: int, shift_span):
        """
        break_span: break span taken by employee
        under_break: under break time taken by employee
        shft_span: shift time span

        creating a break span equal to total break time
        which should be removed from the shift span:-
        Eg:-
        break_span: 2022-08-15 17:55:00, 2022-08-15 18:00:00
        under_break: 55 min (employee has underbreak for 55 min)
        shift_span: 9:00 - 18:00

        Output:
        2022-08-15 17:00:00, 2022-08-15 18:00:00
        """
        last_break = break_span[-1]
        # making sure that the calculated break span is within the shift
        if (last_break.j + under_break) <= shift_span.j:
            break_span += spans.create_span([last_break.j, last_break.j + under_break])
        else:
            break_span += spans.create_span([last_break.i - under_break, last_break.i])
        return break_span

    def _get_valid_break_span(self, break_span, work_hours, total_break, shift, is_floating, shift_span):
        '''
        return the valid flexi break span
        break_span: timespan in which user can take break
        work_hours: hours rendered by user
        total_time: total allowed break time in seconds
        the returned span will be removed from the shift span
        '''
        break_taken = break_span - work_hours
        if not break_taken or not work_hours:
            break_taken = spans.create_span([break_span.i, break_span.i + total_break])
        valid_break_span = break_taken
        over_break = 0
        under_break = 0
        if break_taken.value > total_break:
            over_break = break_taken.value - total_break
        elif break_taken.value < total_break:
            under_break = total_break - break_taken.value
        if over_break > 0:
            valid_break_span = self._calculate_break_span_for_over_break(break_taken, total_break, over_break)
        elif under_break > 0:
            valid_break_span = self._calculate_break_span_for_under_break(break_taken, under_break, shift_span)
        if is_floating:
            if under_break > 0:
                floating_span =self._floating_break_span(shift, valid_break_span)
                if floating_span:
                    return floating_span
        return valid_break_span

    def get_break_total_time(self, breaks: list):
        '''
        return the total break time in seconds
        breaks: fixed|flexi|floating
        '''
        total_time = 0
        for br in breaks:
            is_paid = br.get(keys.IS_PAID_BREAK, False)
            if not is_paid:
                break_hours_in_secs = dateutils.convert_to_mins(
                    br.get(keys.BREAK_HOURS, "00:00")) * 60
                total_time += break_hours_in_secs
        return total_time

    def _floating_break_span(self, shift, floating_span):
        """
        Concat the floating under time with fixed or flexi breaks span
        shift: current shift
        floating_span: valid_flexi_break_span
        """
        floating_break_span = floating_span
        fixed_breaks, flexi_breaks, floating_breaks, total_break = self._get_breaks(
            shift)
        _fixed_breaks = self._get_break_span(
            fixed_breaks, ref_time=shift.get(keys.START_TIME), show_all=True)

        _flexi_breaks = self._get_break_span(
            flexi_breaks, ref_time=shift.get(keys.START_TIME), show_all=True
        )
        if _fixed_breaks:
            intersecting = _fixed_breaks.intersection(floating_break_span)
            if intersecting:
                floating_break_span = spans.create_span([floating_break_span.i, _fixed_breaks.j + intersecting.value])
        if _flexi_breaks:
            intersecting = _flexi_breaks.intersection(floating_break_span)
            if intersecting:
                floating_break_span = spans.create_span([floating_break_span.i, _flexi_breaks.j + intersecting.value])
        return floating_break_span

    def _is_hired(self):
        """This method sets the logic to detect
        if the employee is hired within the range
        of dates for calculation.

        Args:
            date_hired (string) -- The date hired string in YYYY-MM-DD
                format.
        """
        date_hired = self.params.get(keys.DATE_HIRED, None)

        # This should not happen because the date_hired field
        # should be supplied at all times. Defaulting to True.
        # Which means will calculate properly.
        if not date_hired or date_hired == '0000-00-00':
            return True

        date_hired = datetime.datetime.strptime(
            date_hired, "%Y-%m-%d")
        current_date = datetime.datetime.strptime(
            self.current_date, "%Y-%m-%d")

        if date_hired <= current_date:
            return True
        return False

    def _is_terminated(self):
        """This method sets the logic to detect
        if the employee is terminated on or after the
        current attendance date.

        Args:
            date_hired (string) -- The date ended string in YYYY-MM-DD
                format.
        """
        date_ended = self.params.get(keys.DATE_ENDED, None)
        if not date_ended:
            return False

        date_ended = datetime.datetime.strptime(
            date_ended, "%Y-%m-%d")
        current_date = datetime.datetime.strptime(
            self.current_date, "%Y-%m-%d")

        if date_ended < current_date:
            return True
        return False

    def _create_global_spans(self):
        print('_create_global_spans')
        print('self.is_scheduled_overtime_entitled')
        print(self.is_scheduled_overtime_entitled)
        # Create the spans for different time settings
        if self.params.get(keys.NT_SETTINGS, None):
            print('_create_global_spans night shift settings')
            self._create_night_shift_settings_span()
        if self.params.get(keys.HOURS_WORKED, []) or self.is_scheduled_overtime_entitled:
            print('_create_global_spans hours worked')
            self._create_hours_worked_spans()
        if self.params.get(keys.TIMESHEET, []) or self.params.get(keys.TIMESHEET_REQUIRED):
            print('_create_global_spans timesheet')
            self._create_ts_span()
        print('_create_global_spans self')
        print(self)

    def _check_if_entitled_to_scheduled_overtime(self):
        print('_check_if_entitled_to_scheduled_overtimeeeeeeeeeeeeeeeeeeeeeee')
        # Conditions:
        # employee is entitled to ot
        # employee has timeclocks
        # schedule type is fixed
        # employee has scheduled overtime settings
        result = False
        print('self.params')
        print(self.params)
        is_ot_entitled = self.params.get(keys.EN_OVERTIME)
        print('is_ot_entitled')
        print(is_ot_entitled)
        has_timesheet = False
        has_raw_timesheet = False
        #shifts = self.params.get(keys.SHIFTS, [])
        #print('shifts 0 keys.SCHEDULED_OVERTIME')
        #print(shifts[0][keys.SCHEDULED_OVERTIME])
        #scheduled_overtime = shifts.get(keys.SCHEDULED_OVERTIME, [])

        if self.params.get(keys.TIMESHEET, None):
            has_timesheet = len(self.params.get(keys.TIMESHEET, [])) > 1

        if self.params.get(keys.RAW_TIMESHEET, None):
            has_raw_timesheet = len(self.params.get(keys.RAW_TIMESHEET, [])) > 1

        has_timeclocks = has_timesheet or has_raw_timesheet

        print('keys.SHIFTS in self.params')
        print(keys.SHIFTS in self.params)
        print('len(self.params.get(keys.SHIFTS, []))')
        print(len(self.params.get(keys.SHIFTS, [])))
        if is_ot_entitled and has_timeclocks and keys.SHIFTS in self.params\
            and len(self.params.get(keys.SHIFTS, [])) > 0:

            for shift in self.params[keys.SHIFTS]:
                if keys.SCHEDULED_OVERTIME in shift and keys.SH_FIXED in shift[keys.TYPE]:
                    result = len(shift[keys.SCHEDULED_OVERTIME]) > 0

        #print('keys.SCHEDULED_OVERTIME')
        #print(keys.SCHEDULED_OVERTIME)
        #print('keys.SCHEDULED_OVERTIME in self.params')
        #print(keys.SCHEDULED_OVERTIME in self.params)
        #print('len(shifts[0][keys.SCHEDULED_OVERTIME])')
        #print(len(shifts[0][keys.SCHEDULED_OVERTIME]))

        if keys.SHIFTS in self.params:
            shifts = self.params.get(keys.SHIFTS, [])
            if len(shifts) > 0:
                print('len(shifts)')
                print(len(shifts))
                print('len(shifts[0])')
                print(len(shifts[0]))
                if is_ot_entitled and keys.SCHEDULED_OVERTIME in shifts[0]\
                    and len(shifts[0][keys.SCHEDULED_OVERTIME]) > 0:
                    print('inside condition')
                    result = True
            
        print('result')
        print(result)
        return result

    def _append_item(self, items, shift_id, item):
        multiple_span_types = ['Overtime', 'Scheduled Overtime', 'Paid Leave']

        if items.get(shift_id):
            if item['type_name'] in multiple_span_types:
                current_shift_items = items[shift_id]
                current_shift_items.append(item)
                items[shift_id] = list(current_shift_items)
            else:
                items[shift_id] = list(items[shift_id]).append(item)
        else:
            items[shift_id] = [item]
        return items

    def _add_override_item(self, shift_id, hour_type, value):
        mins = dateutils.convert_to_mins(value)
        if not self._override_items.get(shift_id):
            self._override_items[shift_id] = {}

        hr_id = keys.KEY_CONST_MAP.get(hour_type)
        if self._override_items[shift_id].get(hr_id) is None:
            self._override_items[shift_id][hr_id] = mins
        else:
            self._override_items[shift_id][hr_id] += mins

    def _attach_default_schedule_prefix(self, item_id):
        """This method attaches the "ds_" prefix to an
        id.

        Args:
            item_id (string) -- The id to attach a ds_ prefix
        Returns
            (string) -- The altered id.
        """
        if item_id:
            return "ds_{item_id}".format(item_id=item_id)
        return item_id

    def _strip_default_schedule_prefix(self, item_id):
        """This method strips the "ds_" prefix from an
        id.

        Args:
            item_id (string) -- The id that may contain a ds_ prefix
        Returns
            (mixed) -- The stripped id.
        """
        if item_id.startswith("ds_"):
            return item_id[3:]
        return item_id

    def _create_hours_worked_spans(self):
        print('_create_hours_worked_spans')
        print('self.is_scheduled_overtime_entitled')
        print(self.is_scheduled_overtime_entitled)
        # Ignores NT OT spans IF emp is not entitiled
        hours_worked = self.params.get(keys.HOURS_WORKED)
        paid_leaves = {}
        unpaid_leaves = {}
        regular_hours = {}
        night_shifts = {}  # should just be 1
        overtimes = {}
        overtime_night_shifts = {}
        undertimes = {}
        scheduled_overtimes = {}

        # Add scheduled overtime as hours worked
        if self.is_scheduled_overtime_entitled:
            scheduled_overtime = self._parse_shift_scheduled_overtime()
            print('scheduled_overtime')
            print(scheduled_overtime)
            print('overtimes')
            print(overtimes)
            
            
            for overtime in scheduled_overtime:
                print('overtime')
                print(overtime)
                shift_id = str(overtime[keys.SHIFT_ID])
                print('shift_id')
                print(shift_id)
                scheduled_overtimes = self._append_item(overtimes, shift_id, overtime)
                print('scheduled_overtimes')
                print(scheduled_overtimes)

        print('hours_worked')
        print(hours_worked)
        print('before forrrrrrrrrrrrrrrrrrrr')
        for item in hours_worked:
            print('in forrrrrrrrrrrrrrrrrrrr')
            shift_id = str(item.get(keys.SHIFT_ID))
            hour_type = item.get(keys.TYPE)
            print('hour_type')
            print(hour_type)

            # check start_datetime AND end_datetime
            if item.get(keys.START_DATETIME) is None and item.get(keys.END_DATETIME) is None:
                value = item.get("time")

                # if time value is not 0 then it's an override item
                if (dateutils.convert_to_sec(value) > 0):
                    self._add_override_item(shift_id, hour_type, value)

                continue

            if hour_type == keys.HR_PAID_LEAVE:
                paid_leaves = self._append_item(paid_leaves, shift_id, item)
            elif hour_type == keys.HR_UNPAID_LEAVE:
                unpaid_leaves = self._append_item(
                    unpaid_leaves, shift_id, item)
            elif hour_type == keys.HR_REGULAR:
                regular_hours = self._append_item(
                    regular_hours, shift_id, item)
            elif hour_type == keys.HR_NIGHT_SHIFT:
                night_shifts = self._append_item(night_shifts, shift_id, item)
            elif hour_type == keys.HR_OVERTIME:
                overtimes = self._append_item(overtimes, shift_id, item)
            elif hour_type == keys.HR_OT_NT:
                overtime_night_shifts = self._append_item(
                    overtime_night_shifts, shift_id, item)
            elif hour_type == keys.HR_UNDERTIME:
                undertimes = self._append_item(undertimes, shift_id, item)

        print('_create_hours_worked_spans scheduled_overtimes')
        print(scheduled_overtimes)
        self._pl_spans = self._create_hours_worked_span(paid_leaves)
        self._ul_spans = self._create_hours_worked_span(unpaid_leaves)
        self._scheduled_overtime_spans = self._create_hours_worked_span(scheduled_overtimes)
        print('_create_hours_worked_spans self._scheduled_overtime_spans')
        print(self._scheduled_overtime_spans)
        if self.params.get(keys.EN_NT_PAY):
            print('if differential')
            self._night_shifts_spans = self._create_hours_worked_span(
                night_shifts)
        if self.params.get(keys.EN_OVERTIME):
            print('if overtime')
            self._overtime_spans = self._create_hours_worked_span(overtimes)
            print('self._overtime_spans')
            print(self._overtime_spans)
        if self.params.get(keys.EN_NT_PAY) and self.params.get(keys.EN_OVERTIME):
            print('if differential and overtime')
            self._overtime_night_shift_spans = self._create_hours_worked_span(
                overtime_night_shifts)
        self._undertime_spans = self._create_hours_worked_span(undertimes)
        self._regular_spans = self._create_hours_worked_span(regular_hours)
        print('self._regular_spans')
        print(self._regular_spans)

    def _create_ts_span(self):
        # create the timesheet entries' spans
        timesheet = self._round_down_timesheet()
        ts_tuples_list = []
        while timesheet:
            start, end, timesheet = timesheet[0], timesheet[1], timesheet[2:]
            # ignore spans that have exceeded the max clock out rule
            if end.get('max_clock_out', False):
                continue
            start_ts = start.get(keys.TIMESTAMP)
            end_ts = end.get(keys.TIMESTAMP)
            new_span = spans.create_span((
                start_ts,
                end_ts
            ))

            # Check if new ts sub span is not conflicting with
            # current raw_ts_span and ts_span
            # add if not conflicting to avoid span conflict errors
            if not self._raw_ts_span:
                self._raw_ts_span = new_span
            elif not self._raw_ts_span.isconflict_with(new_span):
                self._raw_ts_span += new_span

            if not self._ts_span:
                self._ts_span = new_span
                self._ts_lookup["%s-%s" % (int(start_ts), True)] = start
                self._ts_lookup["%s-%s" % (int(end_ts), False)] = end
            elif not self._ts_span.isconflict_with(new_span):
                self._ts_span += new_span
                self._ts_lookup["%s-%s" % (int(start_ts), True)] = start
                self._ts_lookup["%s-%s" % (int(end_ts), False)] = end

        # don't forget to add regular hours start, end to _ts_lookup
        # as if they were timesheet entries.
        if self._regular_spans:
            for shift_id in self._regular_spans:
                currblock = self._regular_spans.get(shift_id)

                for currspan in currblock:
                    self._ts_lookup["%s-%s" % (int(currspan.i), True)] = {
                        keys.TIMESTAMP: currspan.i,
                        "state": True,
                        "tags": [],
                        "employee_uid": self.params.get(keys.EMP_UID)
                    }
                    self._ts_lookup["%s-%s" % (int(currspan.j), False)] = {
                        keys.TIMESTAMP: currspan.j,
                        "state": False,
                        "tags": [],
                        "employee_uid": self.params.get(keys.EMP_UID)
                    }
                if self._ts_span:
                    self._ts_span = self._ts_span.union(currblock)
                else:
                    self._ts_span = currblock
        if self._night_shifts_spans:
            for shift_id in self._night_shifts_spans:
                currblock = self._night_shifts_spans.get(shift_id)
                for currspan in currblock:
                    self._ts_lookup["%s-%s" % (int(currspan.i), True)] = {
                        keys.TIMESTAMP: currspan.i,
                        "state": True,
                        "employee_uid": self.params.get(keys.EMP_UID)
                    }
                    self._ts_lookup["%s-%s" % (int(currspan.j), False)] = {
                        keys.TIMESTAMP: currspan.j,
                        "state": False,
                        "employee_uid": self.params.get(keys.EMP_UID)
                    }
                if self._ts_span:
                    self._ts_span = self._ts_span.union(currblock)
                else:
                    self._ts_span = currblock
        if self._undertime_spans:
            for shift_id in self._undertime_spans:
                currblock = self._undertime_spans.get(shift_id)
                for currspan in currblock:
                    if self._ts_span:
                        self._ts_span = self._ts_span - currblock
        # TODO: consider OT spans here?

    def _round_down_timesheet(self):
        timesheets = self.params.get(keys.TIMESHEET)

        if timesheets:
            for timesheet in timesheets:
                if timesheet.get(keys.TIMESTAMP, None):
                    timesheet[keys.TIMESTAMP] -= timesheet[keys.TIMESTAMP] % 60

        return timesheets

    def _create_hours_worked_span(self, items):
        if items:
            new_items = {}
            for shift_id, value in items.items():
                tuple_list = []
                if value:
                    for item in value:
                        tuple_list.append((
                            dateutils.datetime_to_secs(
                                item.get(keys.START_DATETIME)),
                            dateutils.datetime_to_secs(item.get(keys.END_DATETIME))
                        ))
                    sp = spans.create_span(*tuple(tuple_list))
                    new_items[shift_id] = sp
            return new_items
        return {}

    def _create_night_shift_settings_span(self):
        self._nt_span = self._create_span(
            self.params[keys.NT_SETTINGS][keys.START],
            self.params[keys.NT_SETTINGS][keys.END],
            ref_time=self.params[keys.NT_SETTINGS][keys.START]
        )

        # Create span for current day's midnight to end of nightshift
        midnight_span = self._create_span(
            "00:00",
            self.params[keys.NT_SETTINGS][keys.END]
        )
        self._nt_span = self._nt_span.union(midnight_span)

        # break down the night span in case nightshift is
        # counter per hour
        self._nt_span1 = spans.create_span((
            dateutils.datetime_to_secs(
                self.current_date + " " + self.params[keys.NT_SETTINGS][keys.START]),
            dateutils.datetime_to_secs(
                self.next_date + " " + "00:00")
        ))

        # Add midnight span to nt span for current day
        self._nt_span1 = self._nt_span1.union(midnight_span)

        self._nt_span2 = spans.create_span((
            dateutils.datetime_to_secs(
                self.next_date + " " + "00:00"),
            dateutils.datetime_to_secs(
                self.next_date + " " + self.params[keys.NT_SETTINGS][keys.END])
        ))

        self._time_shift = self.params.get(
            keys.NT_SETTINGS, {}).get(keys.TIME_SHIFT, "")

    def _create_span(self, *args, ref_time=None):
        # ref_time should be the shift time start of the first shift date
        # for this to work properly
        # For example if the times given in the args is
        # "8:00, 17:00, 22:00, 1:00, 5:00", ref time here is 8:00 to determine
        #  1:00 and 5:00 is already the next day.
        # create list tuples from args args is a HH:MM args list
        ref_val = 0
        if ref_time:
            ref_val = dateutils.convert_to_mins(ref_time)

        list_of_tuples = list()
        time_args = list(args)
        # normalize the time by adding the proper dates
        normalized_vals = []

        for time in time_args:
            current_hhmm = dateutils.convert_to_mins(time)
            if ref_val and current_hhmm < ref_val and self.next_date:
                normalized_vals.append(self.next_date + " " + time)
            else:
                normalized_vals.append(self.current_date + " " + time)

        time_args = normalized_vals

        while time_args:
            start_time, end_time, time_args = time_args[0], time_args[1], time_args[2:]

            list_of_tuples.append(
                (dateutils.datetime_to_secs(start_time),
                 dateutils.datetime_to_secs(end_time))
            )
        return spans.create_span(*tuple(list_of_tuples))

    def _check_premium_pay(self):
        return self.params.get(keys.EN_PREMIUM_PAY)

    def add_entry(self, shift_id, index, seconds):
        """Add computed attendance entry, will perform entitlement check"""

        #if not seconds:  # Do not add zero values
        #    return

        target = {}
        shift_id = str(shift_id)
        if shift_id in self._attendance:
            target = self._attendance[shift_id]

        target[str(index)] = target.get(str(index), int(0)) + int(seconds / 60)
        self._attendance[shift_id] = target

    def get_entry(self, shift_id, index):
        """Get attendance value"""
        return self._attendance.get(str(shift_id), {}).get(index, 0)

    def _get_paid_break_span(self, breaks, ref_time=None):
        times = []

        for item in breaks:
            # Retreive flag for determining if a break is paid or not.
            # if a break is paid, we don't have to deduct breaks on
            # shift otherwise, we have to deduct
            is_paid_break = item.get(keys.IS_PAID_BREAK)
            start_time = item.get(keys.START)
            end_time = item.get(keys.END)

            # if it's not a paid break, proceed to defining the break start
            # and break then. This is in-prepartion for break deductions
            # on a shift.
            # If show_paid_breaks and is_paid_break are True, we have to
            # omit hidding the breaks. This is for only using this method
            # to check paid breaks and not use this for deduction/addition
            # of time spans.
            if is_paid_break:
                times.append(start_time)
                times.append(end_time)

        if times:
            return self._create_span(*tuple(times), ref_time=ref_time)
        return None

    def _get_break_span(self, breaks, ref_time=None, show_paid_breaks=False, show_all=False):
        times = []

        for item in breaks:
            # Retreive flag for determining if a break is paid or not.
            # if a break is paid, we don't have to deduct breaks on
            # shift otherwise, we have to deduct
            is_paid_break = item.get(keys.IS_PAID_BREAK)
            start_time = item.get(keys.START)
            end_time = item.get(keys.END)

            # if it's not a paid break, proceed to defining the break start
            # and break then. This is in-prepartion for break deductions
            # on a shift.
            # If show_paid_breaks and is_paid_break are True, we have to
            # omit hidding the breaks. This is for only using this method
            # to check paid breaks and not use this for deduction/addition
            # of time spans.
            if show_all or not is_paid_break or (is_paid_break and show_paid_breaks):
                times.append(start_time)
                times.append(end_time)

        if times:
            return self._create_span(*tuple(times), ref_time=ref_time)
        return None

    def _get_breaks(self, shift):
        # returns fixed, flexi, floating, total_secs tuple of lists, int
        breaks = shift.get(keys.BREAKS)
        fixed = []
        flexi = []
        floating = []
        total_time = 0
        for br in breaks:
            break_type = br.get(keys.TYPE, None)
            break_hours_in_secs = dateutils.convert_to_mins(
                br.get(keys.BREAK_HOURS, "00:00")) * 60
            total_time += break_hours_in_secs
            if break_type == keys.BR_FIXED:
                fixed.append(br)
            elif break_type == keys.BR_FLOATING:
                floating.append(br)
            elif break_type == keys.BR_FLEXI:
                flexi.append(br)
        return fixed, flexi, floating, total_time

    def get_user_defined_rest_day(self):
        """This method parses the assigned user defined shift
        into a default schedule type of dict structure.

        Return:
            (dict) -- The user defined shift object
        """

        if not self.is_user_defined_rest_day():
            return None

        def_shift = self.get_default_shift()

        rd_shift_id = None

        if def_shift:
            rd_shift_id = def_shift.get(keys.SHIFT_ID, None)

        if rd_shift_id is None:
            ud_rd = self._user_defined_rd()

            rd_shift_id = self._attach_default_schedule_prefix(
                str(ud_rd.get(keys.ID, None)))

        return {
            keys.ID: rd_shift_id,
            keys.TYPE: keys.SH_FIXED,
            keys.SHIFT_ID: rd_shift_id,
            keys.NAME: "RD",
            keys.DEFAULT: True,
            keys.START_TIME: None,
            keys.END_TIME: None,
            keys.BREAKS: [],
            keys.DAY_TYPE: "rest_day",
            keys.TOTAL_HOURS: None,
        }


    def _parse_default_shift(self, def_value):
        """This parses the raw default shift dict into
        a dict that's understood by the calculater

        Args:
            def_value (dict) -- The raw default shift dict
        Return:
            (dict) -- The parsed default shift
        """
        def_shift_id = self._attach_default_schedule_prefix(
            str(def_value.get(keys.ID, None)))
        name = "RD" if def_value.get("day_type", None) == 'rest_day' else keys.VAL_DEFAULT

        default_shift_data = {
            keys.ID: def_shift_id,
            keys.TYPE: keys.SH_FIXED,
            keys.SHIFT_ID: def_shift_id,
            keys.NAME: name,
            keys.DEFAULT: True,
            keys.START_TIME: dateutils.strip_seconds(def_value.get(keys.WORK_START)),
            keys.END_TIME: dateutils.strip_seconds(def_value.get(keys.WORK_END)),
            keys.BREAKS: [],
            keys.DAY_TYPE: def_value.get("day_type", None)
        }

        total_break_mins = 0
        if def_value.get(keys.WORK_BREAK_START) != def_value.get(keys.WORK_BREAK_END):

            fbreak = {
                keys.START: dateutils.strip_seconds(def_value.get(keys.WORK_BREAK_START)),
                keys.END: dateutils.strip_seconds(def_value.get(keys.WORK_BREAK_END)),
                keys.TYPE: keys.BR_FIXED
            }
            # compute break_hours
            mins_start = dateutils.convert_to_mins(fbreak.get(keys.START))
            mins_end = dateutils.convert_to_mins(fbreak.get(keys.END))
            total_mins = mins_end - mins_start
            total_break_mins += total_mins
            fbreak[keys.BREAK_HOURS] = "%02d:%02d" % (
                int(total_mins/60), total_mins % 60)
            default_shift_data[keys.BREAKS].append(fbreak)

        # compute total_hours
        start_mins = dateutils.convert_to_mins(
            default_shift_data[keys.START_TIME])

        end_mins = dateutils.convert_to_mins(
            default_shift_data[keys.END_TIME])
        if end_mins < start_mins:
            end_mins = dateutils.convert_to_mins(
                default_shift_data[keys.END_TIME], 24)

        total_shift_mins = end_mins - start_mins
        default_shift_data[keys.TOTAL_HOURS] = "%02d:%02d" % (
            int(total_shift_mins/60), total_shift_mins % 60)

        return default_shift_data

    def _parse_shift_scheduled_overtime(self):
        """This parses the raw default shift scheduled_overtime dict into
        a dict that's understood by the calculater

        Return:
            (dict) -- The parsed default shift scheduled_overtimes
        """
        shifts = self.params.get(keys.SHIFTS)
        overtimes = []
        for shift in shifts:
            shift_type = shift.get(keys.TYPE)
            scheduled_overtime = shift.get(keys.SCHEDULED_OVERTIME, [])

            if keys.SH_FIXED in shift_type and len(scheduled_overtime) > 0:
                for item in scheduled_overtime:
                    data = {
                        'employee_id': self.params[keys.ID],
                        'date': None,
                        'type': 'overtime',
                        'type_name': 'Scheduled Overtime',
                        'value': 0,
                        'time': 0,
                        'shift_id': shift[keys.SHIFT_ID],
                        'start_datetime': None,
                        'end_datetime': None
                    }

                    scheduled_overtime_date = self._get_scheduled_overtime_start_date(shift, item)

                    data['start_datetime'] = scheduled_overtime_date + ' ' + item['start']
                    data['end_datetime'] = dateutils.get_end_datetime(
                        scheduled_overtime_date, item['start'], item['end'])

                    total_mins = dateutils.get_datetime_mins_difference(data['start_datetime'], data['end_datetime'])

                    data['time'] = "%02d:%02d" % ((int(total_mins / 60), total_mins % 60))
                    data['value'] = "%d:%02d" % ((int(total_mins / 60), total_mins % 60))
                    data['date'] = scheduled_overtime_date

                    overtimes.append(data)
        return overtimes

    def _get_scheduled_overtime_start_date(self, shift, overtime):
        start_date = self.current_date
        buffer_in_hours = 4

        # shift dates
        shift_start_date = datetime.datetime.strftime(
            dateutils.parse_string_date(self.current_date), keys.SQL_DATE_FORMAT)
        shift_end_date = dateutils.get_end_datetime(
            self.current_date, shift['start_time'], shift['end_time'], keys.SQL_DATE_FORMAT)

        if (shift_end_date > shift_start_date):
            # Shift
            shift_end_time = shift['end_time']

            # Overtime
            overtime_start_time = overtime['start']

            if shift_end_time == overtime_start_time:
                start_date = shift_end_date
            else:
                shift_end_time = datetime.datetime.strptime(shift_end_time, "%H:%M")
                overtime_start_time = datetime.datetime.strptime(overtime_start_time, "%H:%M")

                difference = overtime_start_time - shift_end_time
                difference_in_hours = difference.total_seconds()/3600

                if difference_in_hours <= buffer_in_hours:
                    start_date = shift_end_date
                else:
                    start_date = shift_start_date

        return start_date

    def get_default_shift(self):
        # Convert default_shift to shift
        def_value = self.params.get("default_shift", None)
        #if def_value and def_value.get("day_type", "regular") == "rest_day":
        #    return None

        if def_value:
            return self._parse_default_shift(def_value)
        return None

    def _apply_wh_deduction(self, shift_id, deduct, dkeys):
        # Reminder: reckoning is in minutes when you get the mins
        target = self._attendance.get(str(shift_id))  # type: dict
        net_deduct = deduct / 60  # convert to mins
        if dkeys:
            for key in dkeys:
                if not net_deduct:
                    break
                value = target.get(key, 0)
                if value - net_deduct >= 0:
                    target[key] = value - net_deduct
                    net_deduct = 0
                else:
                    del target[key]  # since it is zeroed
                    net_deduct = net_deduct - value
        self._attendance[str(shift_id)] = target

    def _get_holiday_real_ot_span(self, work_span, shift_span):
        if all([work_span, shift_span]):
            if str(type(shift_span)) == "<class 'attendance.spans.MultiSpan'>":
                for i in shift_span:
                    if work_span:
                        work_span = work_span - i
            else:
                return work_span - shift_span
            return work_span

    def _get_day_type_from_code(self, date, code):
        return_data = None
        shift_dates = self._shift_dates.get(date, None)
        if all([shift_dates, date, code]):
            return_data = shift_dates.get(code, None)

        return return_data

    def _compute_night_shift_attendance_from_span(self, shift_id, span_to_process, is_ot=False):
        nt_span = self._nt_span
        shift_dates = self._shift_dates
        is_nt_entitled = self.params.get(keys.EN_NT_PAY)
        date_to_process = self.params.get(keys.DATE, None)

        if span_to_process and is_nt_entitled:
            nt_sp = span_to_process.intersection(nt_span)
            if nt_sp:
                if self._time_shift.lower() == keys.NT_CARRY_OVER.lower():
                    nt_code = self._get_code(
                            self._get_day_type_from_code(date_to_process, keys.REST_DAY),
                            self._get_day_type_from_code(date_to_process, keys.HOLIDAY_TYPE
                                if self._check_premium_pay() else None),
                            True,  # NT
                            is_ot, # OT
                    )
                    self.add_entry(shift_id, nt_code, nt_sp.value)
                    span_to_process -= nt_sp

                elif self._time_shift.lower() == keys.NT_COUNTER_PER_HOUR.lower():
                    nt_first_half = self._nt_span1.intersection(nt_sp)
                    nt_second_half = self._nt_span2.intersection(nt_sp)
                    if nt_first_half:
                        nt_code = self._get_code(
                            self._get_day_type_from_code(self.current_date, keys.REST_DAY),
                            self._get_day_type_from_code(self.current_date, keys.HOLIDAY_TYPE
                                if self._check_premium_pay() else None),
                            True,  # NT
                            is_ot,  # OT
                        )
                        self.add_entry(shift_id, nt_code, nt_first_half.value)
                        span_to_process -= nt_first_half

                    if nt_second_half:
                        is_next_day_restday = self._is_date_restday(self.params.get('employee_uid'), self.next_date)
                        nt_code = self._get_code(
                            is_next_day_restday,
                            self._get_day_type_from_code(self.next_date, keys.HOLIDAY_TYPE
                                if self._check_premium_pay() else None),
                            True,  # NT
                            is_ot,  # OT
                        )
                        self.add_entry(shift_id, nt_code, nt_second_half.value)
                        span_to_process -= nt_second_half

        return span_to_process

    def _is_timesheet_required(self):
        return self.params.get(keys.TIMESHEET_REQUIRED, False)

    def _is_holiday_pay_entitled(self):
        return (self.params.get(keys.EN_RH_PAY, False) or
                self.params.get(keys.EN_SH_PAY, False))

    def _is_holiday_type_entitled(self, holiday_type=None):
        if holiday_type is None:
            holiday_type = self._shift_dates.get(self.current_date, {}).get(keys.HOLIDAY_TYPE, None)

        rh_list = [constants.ATT_RH, constants.ATT_2RH]
        entitled = False
        if holiday_type in rh_list:
            entitled = self.params.get(keys.EN_RH_PAY, False)
        elif holiday_type == constants.ATT_SH:
            entitled = self.params.get(keys.EN_SH_PAY, False)
        elif holiday_type == constants.ATT_RH_SH:
            entitled = self.params.get(keys.EN_RH_PAY, False) and self.params.get(keys.EN_SH_PAY, False)

        return entitled

    def _get_expected_hours_to_secs(self):
        exp_hours = self.params.get(keys.HOURS_PER_DAY)
        if exp_hours:
            value = int(float(exp_hours) * 60 * 60)
            return value
        return int(0)

    def _add_unworked_entry(self, shift_id, value):
        if self._is_holiday_pay_entitled():  # TODO:do this check outside
            self.add_entry(shift_id, constants.ATT_UH, value)

    def _compute_fixed_shift(self, shift_id, shift, shift_span):
        """This method computes the attendance of type shift,
        FIXED or DEFAULT SCHEUDLE.

        Args:
            shift_id (string) -- The shift id
            shift (dict) -- The shift object
            shift_span (SimpleSpan) -- The shift span in SimpleSpan object.
        """
        shift_dates = self._shift_dates
        tardy_span = None

        # A shift's duration
        shift_time = dateutils.convert_to_mins(
            shift.get(keys.TOTAL_HOURS, "00:00")) * 60

        # Default shift data
        default_shift = shift.get(keys.DEFAULT, False)

        # The expected hours per day
        expected_work_time = self._get_expected_hours_to_secs()

        # keys is the storage of deductible keys which should be
        # arranged in order of priority
        dkeys = []
        # breaks
        fixed_breaks, flexi_breaks, floating_breaks, total_break = self._get_breaks(
            shift)
        is_rest_day = self._shift_dates.get(self.current_date, {}).get("rest_day")
        # ---------------------------
        # Handle Fixed Breaks -- remove them from the shift_span
        # ---------------------------
        _fixed_breaks = self._get_break_span(
            fixed_breaks, ref_time=shift.get(keys.START_TIME), show_all=True)
        # Remove all breaks spans from the shift span
        if _fixed_breaks:
            shift_span = shift_span - _fixed_breaks

        _flexi_breaks = self._get_break_span(
            flexi_breaks, ref_time=shift.get(keys.START_TIME), show_all=True
        )

        # Set the attendance date of an employee to ABSENT if
        # the employee is not hired or is terminated on the
        # employee's assigned shift.
        if not self._is_hired() or self._is_terminated():
            #absent_time = total_work if shift_type == keys.SH_FLEXI else shift_span.value
            if _fixed_breaks:
                for fxd_break in fixed_breaks:
                    if "is_paid" in fxd_break:
                        fixed_paid_break = fxd_break['is_paid']
                        if fixed_paid_break == True:
                            shift_span = shift_span + _fixed_breaks

            if _flexi_breaks:
                fx_breaks = flexi_breaks
                for fx_break in fx_breaks:
                    flex_break = fx_break['break_hours']
                    start_br_str = fx_break['start']
                    start_br_hr_str = start_br_str[:2]
                    start_br_min_str = start_br_str[3:5]
                    if start_br_str[:1] == '0':
                        start_br_str = start_br_str[1:2]
                    start_br_hr_int = int(start_br_hr_str)
                    start_br_min_int = int(start_br_min_str)

                    if len(flex_break) == 4:
                        flex_break_hr_str = flex_break[:1]
                        flex_break_min_str = flex_break[2:4]
                    else:
                        flex_break_hr_str = flex_break[:2]
                        flex_break_min_str = flex_break[3:5]

                    flex_break_hr_int = int(flex_break_hr_str)
                    flex_break_min_int = int(flex_break_min_str)
                    end_br_hr_int = start_br_hr_int + flex_break_hr_int
                    end_br_min_int = start_br_min_int + flex_break_min_int
                    if end_br_min_int > 59:
                        tmp_float_break_hr_int = end_br_min_int / 60
                        tmp_float_break_min_int = end_br_min_int % 60

                        if tmp_float_break_min_int > 0:
                            flex_break_min_int = flex_break_min_int + tmp_float_break_min_int

                        if tmp_float_break_hr_int > 0:
                            flex_break_min_int = flex_break_min_int + tmp_float_break_hr_int
                            end_br_hr_int = start_br_hr_int + flex_break_min_int

                    if end_br_hr_int > 23:
                        end_br_hr_int = end_br_hr_int % 24
                    elif end_br_hr_int == 24:
                        end_br_hr_int = 0
                    if end_br_hr_int < 10:
                        end_br_hr_str = str(end_br_hr_int)
                        end_br_hr_str = '0' + end_br_hr_str
                    else:
                        end_br_hr_str = str(end_br_hr_int)

                    end_br_min_str = str(end_br_min_int)
                    if end_br_min_str == '0':
                        end_br_min_str = '00'

                    fx_break.update({
                        keys.START: start_br_hr_str + ':' + start_br_min_str,
                        keys.END: end_br_hr_str + ':' + end_br_min_str
                    })

                    _flex_breaks = self._get_break_span(
                        fx_breaks, ref_time=shift.get(keys.START_TIME), show_all=True
                    )

                    if "is_paid" in fx_break:
                        flex_paid_break = fx_break['is_paid']
                        if flex_paid_break == False:
                            shift_span = shift_span - _flex_breaks

            if floating_breaks:
                for ft_break in floating_breaks:
                    float_break = ft_break['break_hours']
                    start_br_str = shift.get(keys.START_TIME)
                    start_br_hr_str = start_br_str[:2]
                    start_br_min_str = start_br_str[3:5]
                    if start_br_str[:1] == '0':
                        start_br_str = start_br_str[1:2]
                    start_br_hr_int = int(start_br_hr_str)
                    start_br_min_int = int(start_br_min_str)

                    if len(float_break) == 4:
                        float_break_hr_str = float_break[:1]
                        float_break_min_str = float_break[2:4]
                    else:
                        float_break_hr_str = float_break[:2]
                        float_break_min_str = float_break[3:5]

                    float_break_hr_int = int(float_break_hr_str)
                    float_break_min_int = int(float_break_min_str)
                    end_br_hr_int = start_br_hr_int + float_break_hr_int
                    end_br_min_int = start_br_min_int + float_break_min_int
                    if end_br_min_int > 59:
                        tmp_float_break_hr_int = end_br_min_int / 60
                        tmp_float_break_min_int = end_br_min_int % 60

                        if tmp_float_break_min_int > 0:
                            float_break_min_int = float_break_min_int + tmp_float_break_min_int

                        if tmp_float_break_hr_int > 0:
                            float_break_min_int = float_break_min_int + tmp_float_break_hr_int
                            end_br_hr_int = start_br_hr_int + float_break_min_int

                    if end_br_hr_int > 23:
                        end_br_hr_int = end_br_hr_int % 24
                    elif end_br_hr_int == 24:
                        end_br_hr_int = 0
                    if end_br_hr_int < 10:
                        end_br_hr_str = str(end_br_hr_int)
                        end_br_hr_str = '0' + end_br_hr_str
                    else:
                        end_br_hr_str = str(end_br_hr_int)

                    end_br_min_str = str(end_br_min_int)
                    if end_br_min_str == '0':
                        end_br_min_str = '00'

                    ft_break.update({
                        keys.START: start_br_hr_str + ':' + start_br_min_str,
                        keys.END: end_br_hr_str + ':' + end_br_min_str
                    })

                    _floating_breaks = self._get_break_span(
                        floating_breaks, ref_time=shift.get(keys.START_TIME), show_all=True
                    )

                    if "is_paid" in ft_break:
                        float_paid_break = ft_break['is_paid']
                        if float_paid_break == False:
                            shift_span = shift_span - _floating_breaks

            self.add_entry(
                shift_id, constants.ATT_ABSENT, shift_span.value)
            return

        # ------------------------
        # Leaves -- remove them from shift span and
        # log them as their own hours.
        # ------------------------
        # if leave is full shift leave, then store the leave and continue
        # otherwise continue computation.
        #
        # There are two types of leaves that should be handled.
        # First handling is PAID LEAVE
        pl_span = self._pl_spans.get(shift_id)
        if pl_span:
            # Tag the paid leave value as PAID_LEAVE on the attendance
            self.add_entry(shift_id,
                           constants.ATT_PAID_LEAVE,
                           shift_span.intersection(pl_span).value)
            # Deduct the approved paid leave from the shift span
            shift_span = shift_span - pl_span

        # Next handling is UN-PAID LEAVE
        ul_span = self._ul_spans.get(shift_id)
        if ul_span:
            # Tag the unpaid leave value as UNPAID_LEAVE on the attendance
            self.add_entry(shift_id,
                           constants.ATT_UNPAID_LEAVE,
                           shift_span.intersection(ul_span).value)
            # Deduct the approved unpaid leave from the shift span
            shift_span = shift_span - ul_span

        # Get paid breaks
        # Get all paid breaks
        paid_fixed_breaks = self._get_paid_break_span(
            fixed_breaks, ref_time=shift.get(keys.START_TIME))

        # Paid break handler on approved leave, applies on whole/partial paid leaves
        if paid_fixed_breaks:
            # Check if pl span intersects with break
            # and add intersecting minutes
            if pl_span and pl_span.intersection(paid_fixed_breaks):
                self.add_entry(shift_id, constants.ATT_PAID_LEAVE,
                               pl_span.intersection(paid_fixed_breaks).value)

            # Check if ul span intersects with break
            # and add intersecting minutes
            if ul_span and ul_span.intersection(paid_fixed_breaks):
                self.add_entry(shift_id, constants.ATT_UNPAID_LEAVE,
                               ul_span.intersection(paid_fixed_breaks).value)

        # Deduct the unpaid floating break
        if pl_span and floating_breaks:
            unpaid_ft_value = 0
            for ft_break in floating_breaks:
                # convert to seconds
                if not ft_break.get(keys.IS_PAID_BREAK):
                   unpaid_ft_value += dateutils.convert_to_mins(
                    ft_break.get(keys.BREAK_HOURS)) * 60

            # Deduct the unpaid break
            if unpaid_ft_value != 0:
                self.add_entry(shift_id, constants.ATT_PAID_LEAVE,
                    unpaid_ft_value * -1)

        # Deduct the unpaid flexi break
        if pl_span and flexi_breaks:
            unpaid_fx_value = 0
            for fx_break in flexi_breaks:
                # convert to seconds
                if not fx_break.get(keys.IS_PAID_BREAK):
                   unpaid_fx_value += dateutils.convert_to_mins(
                    fx_break.get(keys.BREAK_HOURS)) * 60

            # Deduct the unpaid break
            if unpaid_fx_value > 0:
                self.add_entry(shift_id, constants.ATT_PAID_LEAVE,
                    unpaid_fx_value * -1)

        # if the shift span is fully exhausted, terminate this method
        # and return null. Returning null will result to have an attendance
        # based from the recently added attendance codes (PAID_LEAVE & UNPAID_LEAVE)
        # otherwise, None
        if not shift_span or not shift_span.value:  # if span value is 0
            return

        # --------------------------------------
        # Holidays
        # --------------------------------------
        # First determine if the current attendance date is a holiday
        today_is_a_holiday = self._shift_dates.get(
            self.current_date, {}).get(keys.HOLIDAY_TYPE, None) is not None

        holiday_type = self._shift_dates.get(
            self.current_date, {}).get(
                keys.HOLIDAY_TYPE, None)

        # get the work hours based on shift span and other factors, i.e. and
        # aetc, get_work_hours should consider work_hours for Regular time spans
        work_hours = self._get_work_hours(shift_id, shift_span)
        # ---------------------------
        # Handle Fixed Breaks Continuation -- remove them from the shift_span
        # Only applies if there's no approved paid leave on the date
        # ---------------------------
        if paid_fixed_breaks:
            # Get gaps between clock in/out pairs and create spans for comparison
            gap_spans = self._get_timerecord_gaps(work_hours, shift_id)
            if gap_spans:
                # Check each gap span if there's paid breaks within
                # then extract the intersected duration and add to the work hours
                for i in gap_spans:
                    if i.intersection(paid_fixed_breaks) and not pl_span:
                        intersected_break = i.intersection(paid_fixed_breaks)
                        #Add back the break span that intersected with the gaps
                        shift_span = shift_span + intersected_break
                        work_hours = work_hours + intersected_break
            elif not gap_spans and self._ts_span:
                    # if there are no gaps, check if there's paid break included in clock in/outs
                    # then add the paid break as work hours
                if self._ts_span.intersection(paid_fixed_breaks) and not pl_span:
                    work_hours = work_hours + self._ts_span.intersection(paid_fixed_breaks)
                    shift_span = shift_span + self._ts_span.intersection(paid_fixed_breaks)

        # Extend shift_span to include (worked) paid breaks to compute for undertime
        if all([pl_span, paid_fixed_breaks]):
            if not paid_fixed_breaks.isconflict_with(pl_span):
                # We will only add back paid breaks if its not already processed from the paid leave
                if self._ts_span:
                    worked_paid_fixed_breaks = paid_fixed_breaks.intersection(self._ts_span)
                    # If paid break has time records then add that to work hours
                    if worked_paid_fixed_breaks:
                        work_hours += worked_paid_fixed_breaks
                shift_span += paid_fixed_breaks

        # If attendance is indeed in-scope of a holiday, use the
        # Holidays Factory to determine the appropriate attendance
        # code and time.
        #
        # We also have to determine if the current shift is a night
        # shift. if yes, we have to bypass this as there's a different
        # handling of NT+HOLIDAY
        holiday = factories.HolidaysFactory(
            holiday_type=holiday_type,
            holiday_entitlements=self._holiday_entitlements,
            holiday_premium_pay=self.params.get(keys.EN_PREMIUM_PAY))
        # Revised computation for RH, SH
        # Approved OT is now required to compute SH, RH codes
        ignore_tardy = today_is_a_holiday
        total_ot = None
        if self._overtime_spans:
            total_ot = next(iter(self._overtime_spans.values()))

        # Holiday Entitlements
        is_ot_entitled = self.params.get(keys.EN_OVERTIME)
        is_holiday_entitled = self._check_premium_pay()
        skip_non_holiday_computation = False

        is_custom_shift = "ds_" not in shift_id
        has_timesheet = True if self._ts_span else False
        is_entitled_to_holiday_premium = self.params.get(keys.EN_PREMIUM_PAY)
        if holiday_type == constants.ATT_REGULAR:
            today_is_a_holiday = False
        is_ot_filing_not_required_for_holiday = all([
            today_is_a_holiday,
            has_timesheet,
            is_entitled_to_holiday_premium,
            is_custom_shift
        ])
        # Revised computation for holidays
        # Approved OT is now required to compute RD+RH
        # Work hours outside assigned shift will be tagged sa Real OT
        # Overtime pay entitlement should be enabled to compute holidays
        # UH code only occurs if entitled to unworked holiday
        remaining_uncredited_shift = shift_span

        if all([today_is_a_holiday, total_ot, is_ot_entitled]):
            skip_non_holiday_computation = True
            valid_ot_hours = None
            nt_span = self._nt_span
            # Difference between TSR and TSNR is that TSR requires time records,
            # cross checked with approved OT. For TSNR just use approved OT as work hours
            if self._ts_span and total_ot:
                valid_ot_hours = self._ts_span.intersection(total_ot)

            if valid_ot_hours:
                unworked_span = None
                # Get real ot spans
                # real_ot_span = valid_ot_hours - shift_span
                real_ot_span = self._get_holiday_real_ot_span(valid_ot_hours, shift_span)
                if str(type(valid_ot_hours)) == "<class 'attendance.spans.MultiSpan'>":
                    for i in valid_ot_hours:
                        unworked_span = shift_span - i
                        if not unworked_span:
                            break
                else:
                    unworked_span = shift_span - valid_ot_hours

                # Compute OT
                if real_ot_span:
                    # this is the approved OT span outside of shift span
                    valid_ot_hours -= real_ot_span
                    if len(shift_span) > 1: # Shift with gaps means unpaid breaks
                        real_ot_span -= _fixed_breaks
                    # Process night shift with OT
                    real_ot_span = self._compute_night_shift_attendance_from_span(
                        shift_id, real_ot_span, True)

                    if real_ot_span:
                        # Compute remaining ot hours as OT
                        real_ot_span_seconds_total = 0
                        if len(real_ot_span) > 1:
                            # If multi span append seconds each span
                            for sp in real_ot_span:
                                real_ot_span_seconds_total += sp.value
                        else:
                            real_ot_span_seconds_total = real_ot_span.value

                        real_ot_code = self._get_code(
                            shift_dates.get(self.current_date, {}).get(keys.REST_DAY),
                            shift_dates.get(self.current_date, {}).get(
                                keys.HOLIDAY_TYPE) if is_holiday_entitled else None,
                            False,  # NT
                            True,  # OT
                        )
                        self.add_entry(shift_id, real_ot_code, real_ot_span_seconds_total)

                # Compute Worked Non-OT hours
                total_valid_ot_hours = valid_ot_hours  # approved OT span within the shift
                if valid_ot_hours:
                    # Process night shift without OT
                    valid_ot_hours = self._compute_night_shift_attendance_from_span(
                        shift_id, valid_ot_hours, False)

                    if valid_ot_hours:
                        holiday_attendance = holiday.get_timesheet_required(
                            valid_ot_hours,
                            shift_span,
                            total_break_time=0,
                            expected_work_time=expected_work_time)
                        holiday_duration = holiday_attendance[1]
                        code = self._get_code(
                            shift_dates.get(self.current_date).get(keys.REST_DAY),
                            shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                            False,  # NT
                            False,  # Not an OT
                        )
                        if floating_breaks:
                            for float_break in floating_breaks:
                                # convert to seconds
                                if not float_break.get(keys.IS_PAID_BREAK):
                                    holiday_duration -= dateutils.convert_to_mins(
                                    float_break.get(keys.BREAK_HOURS)) * 60

                        if flexi_breaks:
                            # Deduct flexi breaks in remaining total hours
                            for fl_break in flexi_breaks:
                                # convert to seconds
                                if not fl_break.get(keys.IS_PAID_BREAK):
                                    holiday_duration -= dateutils.convert_to_mins(
                                    fl_break.get(keys.BREAK_HOURS)) * 60

                        if holiday_attendance:
                            self.add_entry(shift_id, code, holiday_duration)
                self._compute_holiday_attendance_within_shift(shift, shift_span, remaining_uncredited_shift, total_valid_ot_hours)

            else:
                self._compute_holiday_attendance_within_shift(shift, shift_span, remaining_uncredited_shift)

        elif all([today_is_a_holiday, total_ot is None])\
            and remaining_uncredited_shift\
            and (self._is_holiday_type_entitled(holiday_type=holiday_type) or is_entitled_to_holiday_premium):
            skip_non_holiday_computation = True
            special_working_holiday = False
            self._compute_holiday_attendance_within_shift(shift, shift_span, remaining_uncredited_shift)

        if not skip_non_holiday_computation:
            # If current date has no rendered hours, the entitlement is
            # timesheet required and is not a holiday, tag the employee as
            # ABSENT.
            if work_hours is None and self._is_timesheet_required():
                # if TIMESHEET is required and there are no Work hours
                # but there's an approved leave, the remaining hours
                # should be tagged as an undertime otherwise, tagged as
                # ABSENT.
                if pl_span or ul_span:
                    self.add_entry(
                        shift_id, constants.ATT_UNDERTIME, shift_span.value)
                else:
                    # At this point this means that there's no work hours and
                    # employee is timesheet required thus tagging this
                    # attendance as ABSENT

                    shift_span_value = shift_span.value

                    # Deduct flexi breaks in remaining shift span
                    for fl_break in flexi_breaks:
                        # convert to seconds
                        if not fl_break.get(keys.IS_PAID_BREAK):
                            shift_span_value -= dateutils.convert_to_mins(
                            fl_break.get(keys.BREAK_HOURS)) * 60

                    # Deduct floating breaks in remaining shift span
                    unpaid_ft_value = 0
                    for ft_break in floating_breaks:
                        # convert to seconds
                        if not ft_break.get(keys.IS_PAID_BREAK):
                            unpaid_ft_value += dateutils.convert_to_mins(
                            ft_break.get(keys.BREAK_HOURS)) * 60

                    shift_span_value -= unpaid_ft_value

                    if shift_span_value:
                        code = constants.ATT_RD if is_rest_day else constants.ATT_ABSENT
                        if is_rest_day:
                            self.add_entry(
                                shift_id, constants.ATT_RD, 0)
                        else:
                            self.add_entry(
                                shift_id, constants.ATT_ABSENT, shift_span_value)
                return

            if is_rest_day and not today_is_a_holiday and not self._rest_day_work_allowed(work_hours, shift):
                    return self.add_entry(shift_id, constants.ATT_RD, 0)

            wh_deduction = 0  # number of minutes to deduct from final computation
            # Process undertime
            ack_undertimes = self._undertime_spans.get(shift_id)
            work_hours = work_hours - ack_undertimes
            undertime = shift_span - work_hours

            ignore_tardy = False
            if undertime and ack_undertimes:
                ignore_tardy = ack_undertimes.i <= undertime.i

            if ack_undertimes:
                undertime = ack_undertimes.union(undertime)

            # -------------------------
            # Process TARDY
            # check undertime span if span begins same as shift_span.
            # check the first span and cross check with tardiness settings
            # -------------------------
            # cross check with tardiness settings
            start_of_shift = shift_span
            if shift_span and pl_span:
                # since pl_span is removed from shift_span at this point
                # we need to add it back to determine the real start of shift
                start_of_shift = shift_span + pl_span

            tardiness = None
            tardiness_deduct = 0

            if undertime and undertime[0].i == start_of_shift.i and not ignore_tardy:
                tsettings = self.params.get(keys.TARDINESS_RULE, None)
                if tsettings:
                    # TODO: check for affected_employees if current emp
                    # is affected
                    # convert to seconds

                    # Get mins to deduct and mins tardy settings
                    # And default to 0 if value is None
                    mins_to_deduct = tsettings.get(keys.MINUTES_TO_DEDUCT)\
                        if tsettings.get(keys.MINUTES_TO_DEDUCT, None) else 0
                    mins_tardy = tsettings.get(keys.MINUTES_TARDY)\
                        if tsettings.get(keys.MINUTES_TARDY, None) else 0

                    threshold = int(mins_tardy) * 60
                    deduct = int(mins_to_deduct) * 60  # convert to seconds
                    tardy_span = undertime[0]
                    undertime = undertime - tardy_span
                    if tardy_span.value > threshold:
                        tardiness = tardy_span
                        tardiness_deduct = deduct

                        if deduct:
                            # create deduct span to be added back
                            deducted_clock_in = spans.create_span(
                                [shift_span[0].i, work_hours.i - deduct])

                            new_work_hours = []
                            counter = 0

                            for clock in work_hours:
                                clock_in = clock.i
                                clock_out = clock.j
                                if counter == 0:
                                    # Replace the first clock in with deducted threshold
                                    work_hour_span = (deducted_clock_in.j, clock_out)
                                else:
                                    # Retain everything else
                                    work_hour_span = (clock_in, clock_out)
                                counter = counter + 1
                                new_work_hours.append(work_hour_span)

                            if new_work_hours:
                                # Replace work_hours with tardy deducted
                                work_hours = spans.create_span(*tuple(new_work_hours))

                    else:  # it's as if bloke isn't late:: add the tardy_span back to work_hours
                        work_hours = work_hours + tardy_span

            (first_clock_in, last_clock_out) = self.get_first_last_time_records()

            # ---------------------------------------------
            # Flexi break handling
            if flexi_breaks:
                (work_hours, tardiness, undertime, wh_deduction) = \
                    self.handle_flexi_break(
                        work_hours, tardiness, undertime, wh_deduction,
                        flexi_breaks, first_clock_in, last_clock_out,
                        shift, shift_id)

            # ---------------------------------------------
            # Floating break handling
            if floating_breaks:
                # Define the start/end time of breaks
                for ft_break in floating_breaks:
                    ft_break.update({
                        keys.START: shift.get(keys.START_TIME),
                        keys.END: shift.get(keys.END_TIME)
                    })

                (work_hours, tardiness, undertime, wh_deduction) = \
                    self.handle_flexi_break(
                        work_hours, tardiness, undertime, wh_deduction,
                        floating_breaks, first_clock_in, last_clock_out,
                        shift, shift_id)

            # Get the night shift span if there is one
            nt_span = self._nt_span

            # NT hours is shift_span  ∩  nt_span
            if work_hours:
                nt_hours = work_hours.intersection(nt_span)

                # net work_hours hours is work_hours - nt hours
                net_work_hours = work_hours - nt_hours
                time_shift = None
                if nt_hours:
                    # this shift has an overnight setting
                    time_shift = self._time_shift
                    if time_shift == keys.NT_CARRY_OVER:
                        # use the attendance date only, other date will not apply
                        # build the hour code for this item
                        code = self._get_code(
                            shift_dates.get(self.current_date).get(keys.REST_DAY),
                            shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                            True,
                            False,  # Not an OT
                        )
                        dkeys.append(str(code))
                        # Add in attendance entry for proper tabulation
                        self.add_entry(shift_id, code, nt_hours.value)

                    elif time_shift == keys.NT_COUNTER_PER_HOUR:
                        # Get the intersection of the first nt span with the work hours
                        # and apply code for first day.
                        code1 = self._get_code(
                            shift_dates.get(self.current_date).get(keys.REST_DAY),
                            shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                            True,  # NT
                            False,  # Not an OT
                        )
                        dkeys.append(str(code1))
                        nt1 = self._nt_span1.intersection(work_hours)
                        if nt1:
                            self.add_entry(shift_id, code1, nt1.value)

                        # Get the intersection of the second nt span with the work hours
                        # and apply code for second day
                        code2 = self._get_code(
                            shift_dates.get(self.next_date, {}).get(keys.REST_DAY),
                            shift_dates.get(self.next_date, {}).get(keys.HOLIDAY_TYPE),
                            True,  # NT
                            False,  # Not an OT
                        )
                        dkeys.append(str(code2))
                        nt2 = self._nt_span2.intersection(work_hours)
                        if nt2:
                            self.add_entry(shift_id, code2, nt2.value)

                day_span1 = self._day_span1
                day_span2 = self._day_span2
                day1_work = day_span1.intersection(net_work_hours)
                day2_work = day_span2.intersection(net_work_hours)
                if day2_work and time_shift == keys.NT_CARRY_OVER:
                    day1_work = day2_work + day1_work
                    day2_work = None

                if day1_work:
                    key = self._get_code(
                        shift_dates.get(self.current_date).get(keys.REST_DAY),
                        shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                        False, False)
                    dkeys.insert(0, str(key))
                    self.add_entry(shift_id, key, day1_work.value)

                if day2_work:
                    key = self._get_code(
                        shift_dates.get(self.next_date, {}).get(keys.REST_DAY),
                        shift_dates.get(self.next_date, {}).get(keys.HOLIDAY_TYPE),
                        False, False)
                    dkeys.insert(0, str(key))
                    self.add_entry(shift_id, key, day2_work.value)

            if tardiness:
                self.add_entry(shift_id, constants.ATT_TARDY, tardiness.value - tardiness_deduct)

            # =======================
            # Determine overbreaks and undertime
            # =======================
            if undertime is not None:
                # Get the last time span
                last_time_span = None
                if work_hours:
                    last_time_span = work_hours[len(work_hours) - 1]
                overbreak = None
                _fixed_breaks = self._get_breaks(shift)[0]

                fixed_break_spans = self._get_break_span(
                    _fixed_breaks, ref_time=shift.get(keys.START_TIME), show_paid_breaks=True)

                if fixed_break_spans:
                    for ut in undertime:
                        for break_span in fixed_break_spans:
                            if (ut < break_span and tardy_span and ut > tardy_span) or \
                            (ut > break_span) and last_time_span > ut:
                                if not overbreak:
                                    overbreak = ut
                                else:
                                    overbreak = ut if overbreak == ut else ut + overbreak

                                if undertime:
                                    undertime = undertime - ut

                    if overbreak:
                    #for time_pair in work_hours:
                    #    if time_pair time_pair < fixed_breaks:
                        self.add_entry(shift_id, constants.ATT_OVERBREAK, overbreak.value)

                if undertime:
                    if not is_rest_day:
                        self.add_entry(shift_id, constants.ATT_UNDERTIME, undertime.value)
            # =======================

            # process work hours deduction: wh_deduction
            if wh_deduction:
                self._apply_wh_deduction(shift_id, wh_deduction, dkeys)

            # OT Handling here
            # check the OT span, then then check if it intersects NT settings for
            # OT_NT
            # Scheduled OT

            ot_span = self._overtime_spans.get(shift_id)
            if self._is_timesheet_required() and ot_span:
                ot_span = ot_span.intersection(self._ts_span)

            scheduled_ot_span = self._scheduled_overtime_spans.get(shift_id)
            if scheduled_ot_span:
                scheduled_ot_span = scheduled_ot_span.intersection(self._ts_span)
                if ot_span:
                    ot_span = ot_span.intersection(scheduled_ot_span)
                else:
                    ot_span = scheduled_ot_span

            ot_nt_span = None
            if ot_span and nt_span:
                ot_nt_span = ot_span.intersection(nt_span)
                if ot_nt_span:
                    ot_span = ot_span - ot_nt_span
            if ot_span:

                for sp in ot_span:
                    date_to_use = dateutils.get_date_from_timestamp(sp.i)
                    ot_key = self._get_code(
                        shift_dates.get(date_to_use, {}).get(keys.REST_DAY),
                        shift_dates.get(date_to_use, {}).get(keys.HOLIDAY_TYPE),
                        False,  # NT
                        True
                    )
                    self.add_entry(shift_id, ot_key, sp.value)
            if ot_nt_span:
                ot_nt_span1 = ot_nt_span.intersection(self._nt_span1)
                if ot_nt_span1:
                    ot_key1 = self._get_code(
                        shift_dates.get(self.current_date).get(keys.REST_DAY),
                        shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                        True, True)
                    self.add_entry(shift_id, ot_key1, ot_nt_span1.value)
                ot_nt_span2 = ot_nt_span.intersection(self._nt_span2)
                if ot_nt_span2:
                    ot_key2 = self._get_code(
                        shift_dates.get(self.next_date, {}).get(keys.REST_DAY),
                        shift_dates.get(self.next_date, {}).get(keys.HOLIDAY_TYPE),
                        True, True)
                    self.add_entry(shift_id, ot_key2, ot_nt_span2.value)

    def _get_timerecord_gaps(self, work_hours=None ,shift_id=None):
        gap_spans = []
        work_spans = work_hours

        if self._ts_span and len(self._ts_span):
            # Get ts spans and leaves as work hours for paid leaves
            if self._pl_spans.get(shift_id, None) and work_hours:
                work_spans = work_hours + self._pl_spans.get(shift_id)

            # Get ts spans and leaves as work hours for unpaid leaves
            if self._ul_spans.get(shift_id, None) and work_spans:
                work_spans = work_spans + self._ul_spans.get(shift_id)

            # Get gaps from actual time in and time out
            if work_spans and work_spans[1::2]:
                for pair in zip(work_spans[::2], work_spans[1::2]):
                    if pair[0] and pair[1]:
                        pair_out = pair[0].j
                        pair_in = pair[1].i
                        gap_spans.append(spans.create_span((pair_out, pair_in)))

        return gap_spans

    def _create_day_span(self, dt):
        current = datetime.datetime.strptime(dt, keys.SQL_DATE_FORMAT)
        next_day = current + datetime.timedelta(1)
        return spans.create_span(
            (int(current.timestamp()), int(next_day.timestamp()))
        )

    def _compute_flexi_total_shift(self, shift, shift_span, ot_spans, total_work):
        """This method calculates the total shift from a flexi shift with no core hours
        and overtime.

        Args:
            shift: dict. The shift details.
            shift_span: object. The shift time span.
            ot_spans: object. The overtime spans.
            total_work: int. The number of seconds an employee is required to work.
                This is a user defined setting in creating schedules.
        Returns:
            The total shift span otherwise, None
        """
        core_hours = shift.get(keys.CORE_TIMES, [])
        total_shift = None

        if ot_spans:
            if core_hours:
                # if there's core hours and timesheet required create
                # the total shift based from core hours + rendered hours, then
                # add the overtime span.
                core_hours_span = self._create_core_hours_span(
                    core_hours, ref_time=shift.get(keys.START_TIME))
                total_shift = core_hours_span.union(ot_spans)
            elif not core_hours and not self._is_timesheet_required():
                # If there's no core hours and timesheet is not required
                # create the shift span based on the defined required total
                # hours and add the OT span.
                shift_span = spans.create_value_span(shift)
                total_shift = shift_span + ot_spans
            else:
                total_shift = shift_span.union(ot_spans)
        else:
            if core_hours and self._is_timesheet_required() and self._ts_span:
                # If settings is with core hours and timesheet required, intersect the
                # rendered hours with the flexi - core hours to get it's total shift
                core_hours_span = self._create_core_hours_span(
                    core_hours, ref_time=shift.get(keys.START_TIME))
                total_shift = shift_span.intersection(self._ts_span)
            elif core_hours and not self._is_timesheet_required():
                # If settings is with core hours and not timesheet required,
                # get the total shift based from core hours and expected shift
                total_shift = self._create_core_hours_span(
                    core_hours, ref_time=shift.get(keys.START_TIME))
            elif not core_hours and self._is_timesheet_required() and self._ts_span:
                # If there's no core hours and timesheet is required
                # create the shift span based on the defined required total
                # hours and rendered hours (timesheet).
                total_shift = shift_span.intersection(self._ts_span)
            else:
                # If there's core hours or none and timesheet is not required,
                # create the shift span based on the defined required total
                # hours.
                total_shift = spans.create_value_span(shift)
        return total_shift

    def _determine_timesheet(self, shift_id, shift_span, shift, total_work):
        core_hours = shift.get(keys.CORE_TIMES, [])
        ot_nt_span = self._overtime_night_shift_spans.get(str(shift_id))
        ot_spans = self._overtime_spans.get(str(shift_id))
        shift_type = shift.get(keys.TYPE, keys.SH_FIXED)
        total_shift = None

        # compute for ot on a nt
        if ot_nt_span and ot_spans:
            ot_spans = ot_spans + ot_nt_span
        elif not ot_spans and ot_nt_span:
            ot_spans = ot_nt_span

        # Get the shift type to determine the total shift + ot
        #if core_hours:
        #    core_hours_span = self._create_core_hours_span(
        #        core_hours, ref_time=shift.get(keys.START_TIME))
        #    total_shift = core_hours_span.union(ot_spans)

        if shift_type == keys.SH_FLEXI:
            total_shift = self._compute_flexi_total_shift(
                shift, shift_span, ot_spans, total_work)
        else:
            # Holiday only: Filing of OT should now cover the entire shift
            shift_ot_span = shift_span.isconflict_with(ot_spans)
            if shift_ot_span:
                total_shift = shift_span.union(ot_spans)
            else:
                total_shift = shift_span + ot_spans

        if self._raw_ts_span:
            logger.debug("TS for shift: %s", str(self._ts_span))
            timesheet = []
            shift_date_coverage = [
                dateutils.get_date_from_timestamp(shift_span.i),
                dateutils.get_date_from_timestamp(shift_span.j)
            ]
            for curr_block in self._raw_ts_span:
                if curr_block.isconflict_with(total_shift):

                    first_ts_date = dateutils.get_date_from_timestamp(curr_block.i)
                    second_ts_date = dateutils.get_date_from_timestamp(curr_block.j)
                    if not self._is_timesheet_within_date_coverage(
                        first_ts_date, second_ts_date, shift_date_coverage):
                        logger.debug("Timesheet not within shift date coverage: %s", str(curr_block))
                        continue
                    timesheet.append(self._ts_lookup.get(
                        "%s-%s" % (int(curr_block.i), True)))

                    timesheet.append(self._ts_lookup.get(
                        "%s-%s" % (int(curr_block.j), False)))

            self._shift_ts_lookup[str(shift_id)] = timesheet

    def _is_timesheet_within_date_coverage(self, first_ts, second_ts, shift_coverage):
        """This method returns if timesheet spans (start and end) is within date coverage of shift
        Args:
            first_ts: start timesheet timestamp
            second_ts: end timesheet timestamp
            shift_coverage: inclusive dates of shift
        Returns:
            bool
        """
        return_data = False
        if (first_ts in shift_coverage and second_ts in shift_coverage):
            return_data = True
        return return_data

    def _create_core_hours_span(self, core_hours, ref_time=None):
        """Create span from the core_times attribute in data, core_hours is a list of
           dicts representing the core_times.
        """
        # get times
        times = []
        for core in core_hours:
            start = core.get(keys.START)
            end = core.get(keys.END)
            times.append(start)
            times.append(end)
        if not ref_time:
            ref_time = times[0]
        return self._create_span(*tuple(times), ref_time=ref_time)

    def _deduct_flexi_shift_undertime_with_leaves(self, shift_id, total_undertime_value):
        """This method substracts approved paid leaves/unpaid leaves
        from any undertime generated

        Args:
            shift_id: Shift ID
            total_undertime_value: Total undertime duration
        Returns:
            int
        """
        # Get approved leave spans
        paid_leave_span = self._pl_spans.get(shift_id)
        unpaid_leave_span = self._ul_spans.get(shift_id)

        # Subrtract any approved paid leave or unpaid leave
        # with the time span to get undertime.
        # Paid leave deduction.
        if paid_leave_span and total_undertime_value:
            total_undertime_value -= paid_leave_span.value

        # Unpaid leave deduction.
        if unpaid_leave_span and total_undertime_value:
            total_undertime_value -= unpaid_leave_span.value

        return total_undertime_value

    def _compute_flexi_shift_with_core_hours(self, shift, shift_id, core_hours, ref_time,
                                             total_work, work_hours, is_holiday):
        ot_span = self._overtime_spans.get(shift_id)
        shift_span = self.params["shift_lookup"][shift_id]
        shift_dates = self._shift_dates
        is_rest_day = self._shift_dates[self.current_date].get("rest_day")

        # OT with Night Diff
        ot_nt_span = self._overtime_night_shift_spans.get(shift_id)
        # Eligible for a night differential
        eligible_night_diff = self.params.get(keys.EN_NT_PAY)

        # Get shift dates to determine the holiday code
        holiday_type = self._shift_dates.get(
            self.current_date, {}).get(
                keys.HOLIDAY_TYPE, None)

        # Get the Timesheet Span, only consider Timesheet covered by the shift,
        # Only include OTs rendered within the Timesheet
        ts_span = None
        if self._ts_span:
            ts_span = self._ts_span.intersection(shift_span) if self._ts_span else None
            ts_ot_span = self._ts_span.intersection(ot_span) if ot_span else None
            if ts_ot_span:
                ts_span = ts_span.union(ts_ot_span) if ts_span else ts_ot_span

        # Calculate undertime here
        # This undertime is a filed undertime
        if self._undertime_spans.get(shift_id) and ts_span:
            ts_span = ts_span - self._undertime_spans.get(shift_id)

        core_hours_span = self._create_core_hours_span(
            core_hours, ref_time=ref_time)

        # If attendance is indeed in-scope of a holiday, use the
        # Holidays Factory to determine the appropriate attendance
        # code and time.
        #
        # We also have to determine if the current shift is a night
        # shift. if yes, we have to bypass this as there's a different
        # handling of NT+HOLIDAY
        holiday = factories.HolidaysFactory(
            shift_type=keys.SH_FLEXI,
            holiday_type=holiday_type,
            holiday_entitlements=self._holiday_entitlements,
            holiday_premium_pay=self.params.get(keys.EN_PREMIUM_PAY))

        remaining_uncredited_shift = spans.create_span((core_hours_span.i, core_hours_span.i + total_work))
        if self._is_holiday_type_entitled(holiday_type=holiday_type)\
            and self._is_holiday_pay_entitled():
            unworked_code = constants.ATT_UH
        elif is_rest_day:
            unworked_code = None
        elif work_hours and work_hours.value < total_work:
            unworked_code = constants.ATT_UNDERTIME
        else:
            unworked_code = constants.ATT_ABSENT

        if is_holiday and holiday_type == constants.ATT_REGULAR and not self._is_timesheet_required()\
            and self._is_holiday_pay_entitled():
            self.add_entry(shift_id, constants.ATT_REGULAR, total_work)
            return

        nt_hours = remaining_uncredited_shift.intersection(self._nt_span)
        time_shift = None
        # get core hours
        if work_hours is None or work_hours.value == 0:
            if is_rest_day and not holiday_type:
                return self.add_entry(shift_id, constants.ATT_RD, 0)
            holiday_attendance = None
            if is_holiday and self._is_timesheet_required():
                holiday_attendance = holiday.get_timesheet_required(
                    work_hours,
                    core_hours_span,
                    total_break_time=0,
                    expected_work_time=total_work)
            elif not self._is_timesheet_required():
                holiday_attendance = holiday.get_timesheet_not_required(
                    work_hours,
                    spans.create_value_span(total_work),
                    total_break_time=0,
                    expected_work_time=total_work)

            if nt_hours and holiday_attendance:
                # this shift has an overnight setting
                time_shift = self._time_shift
                if time_shift == keys.NT_CARRY_OVER:
                    self.add_entry(shift_id, *holiday_attendance)
                    return

                elif time_shift == keys.NT_COUNTER_PER_HOUR:
                    expected_shift_span = spans.create_span((core_hours_span.i, core_hours_span.i + total_work))
                    day_span1 = expected_shift_span.intersection(self._day_span1)
                    day_span2 = expected_shift_span.intersection(self._day_span2)

                    if day_span1 and shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE):
                        self.add_entry(shift_id, unworked_code, day_span1.value)
                        remaining_uncredited_shift = remaining_uncredited_shift - day_span1

                    if day_span2 and shift_dates.get(self.next_date, {}).get(keys.HOLIDAY_TYPE):
                        self.add_entry(shift_id, unworked_code, day_span2.value)
                        remaining_uncredited_shift = remaining_uncredited_shift - day_span2
            else:
                if holiday_attendance:
                    self.add_entry(shift_id, *holiday_attendance)
                    return

            if self._is_timesheet_required() and remaining_uncredited_shift and work_hours is None:
                return self.add_entry(shift_id, constants.ATT_ABSENT, remaining_uncredited_shift.value)
            elif self._is_timesheet_required():
                return self.add_entry(shift_id, constants.ATT_ABSENT, total_work)
            else:
                work_hours = remaining_uncredited_shift
                core_hours_span = core_hours_span.intersection(remaining_uncredited_shift) if remaining_uncredited_shift else None
                total_work = remaining_uncredited_shift.value if remaining_uncredited_shift else 0
        elif work_hours is None and not nt_hours and holiday_type and ot_span is None:
            self.add_entry(shift_id, unworked_code, remaining_uncredited_shift.value)

        if is_rest_day and is_holiday:
            if not self._holiday_and_rest_day_work_allowed(work_hours, shift):
                if unworked_code == constants.ATT_UH:
                    return self.add_entry(shift_id, unworked_code, remaining_uncredited_shift.value)
                return self.add_entry(shift_id, constants.ATT_RD, 0)
        if is_rest_day:
            if not self._rest_day_work_allowed(work_hours, shift):
                return self.add_entry(shift_id, constants.ATT_RD, 0)
        if is_holiday:
            if not self._holidays_work_allowed(work_hours, shift):
                return self.add_entry(shift_id, unworked_code, remaining_uncredited_shift.value)

        # Determine the OT span
        if ot_span and self._is_timesheet_required():
            if ts_span:
                # The overtime hours must be within the timesheet span
                ot_span = ts_span.intersection(ot_span)
            else:
                # Void the OT when there is no timesheet
                ot_span = None

        core_hours_worked_span = core_hours_span.intersection(ts_span) if self._is_timesheet_required() else core_hours_span

        # Determine the unworked core hours, this is must be considered as ATT_UNDERTIME
        unworked_core_hours = core_hours_span - core_hours_worked_span

        # Determine the basis for the rendered hours
        # If the ts_span exceeds the expected hours worked, use the expected hours worked
        # Otherwise, use the timesheet
        employee_rendered_hours = min((ts_span.value if ts_span else 0), total_work) if self._is_timesheet_required() else total_work

        # Get the undertime on core hours
        core_hours_undertime_value = unworked_core_hours.value if unworked_core_hours else 0

        # Get the undertime for the entire shift
        # Undertime from "unworked" Core Hours, and
        # Undertime based from the difference between the hours "outside" of Core Hours and the rendered hours of the employee
        total_shift_undertime = core_hours_undertime_value + max((total_work - core_hours_undertime_value) - employee_rendered_hours, 0)

        # Deduct the Undertime of the entire shift from the expected hours worked.
        # The remaining amount of hours is considered for the rest of the computation
        basis_rendered_hours = total_work - total_shift_undertime

        # Determine the work_span, the rendered work of the employee,
        # subject to the computation of the day/hour types, where applicable
        # E.g. The span to be considered for REGULAR, NT, etc.
        worked_span = None
        is_timesheet_earlier_than_core_hours = False
        if core_hours_span and ts_span and ts_span.i < core_hours_span.i:
            is_timesheet_earlier_than_core_hours = True

        # Legend:
        # TS = timesheet
        # CH = Core Hours
        if not self._is_timesheet_required() and basis_rendered_hours:
            if ts_span and is_timesheet_earlier_than_core_hours and not unworked_core_hours and not ot_span:
                # if TS is earlier than CH, and there are no UT in CH, consider time from clock_in
                worked_span = spans.create_span((ts_span.i, ts_span.i + basis_rendered_hours))

                # If created worked_span does not intersect with core hours, use core hours as basis
                if not worked_span.intersection(core_hours_span):
                    worked_span = None

            if not worked_span:
                worked_span = spans.create_span((core_hours_span.i, core_hours_span.i + basis_rendered_hours))
        elif ts_span and basis_rendered_hours:
            if is_timesheet_earlier_than_core_hours and not unworked_core_hours and not ot_span:
                # if TS is earlier than CH, and there are no UT in CH, consider time from clock_in
                worked_span = spans.create_span((ts_span.i, ts_span.i + basis_rendered_hours))
            elif is_timesheet_earlier_than_core_hours and not unworked_core_hours and ot_span:
                # if TS is earlier than CH, and there are no UT in CH, and there is a filed OT, consider time from end of CH
                worked_span = spans.create_span((ot_span.i -  basis_rendered_hours, ot_span.i))
            elif is_timesheet_earlier_than_core_hours and unworked_core_hours and not ot_span:
                # if TS is earlier than CH, and there are UT in CH, consider time from clock_out
                worked_span = spans.create_span((ts_span.j -  basis_rendered_hours, ts_span.j))
            elif is_timesheet_earlier_than_core_hours and unworked_core_hours and ot_span:
                # if TS is earlier than CH, and there are UT in CH, and there is a filed OT, consider time from end of CH
                worked_span = spans.create_span((core_hours_span.j -  basis_rendered_hours, core_hours_span.j))
            else:
                # if TS is equal or later than CH, and there are UT in CH, consider time from clock_in
                # if TS is later than CH, and there are UT in CH, and there is a filed OT, consider time from clock_in
                worked_span = spans.create_span((ts_span.i, ts_span.i + basis_rendered_hours))

        # Deduct Paid/Unpaid leaves from the computed undertime for the entire shift
        total_ut = self._deduct_flexi_shift_undertime_with_leaves(shift_id, total_shift_undertime)

        if total_ut and not is_rest_day:
            ut_code = constants.ATT_UNDERTIME
            if self._is_holiday_type_entitled(holiday_type=holiday_type) and \
                self._is_holiday_pay_entitled():
                ut_code = constants.ATT_UH
            self.add_entry(shift_id, ut_code, total_ut)

        # Basic overtime if employee is NOT eligible for a night differential
        if ot_span and not ot_nt_span and eligible_night_diff:
            # Check first if OT is intersecting with the night shift setting
            ot_nt_span = ot_span.intersection(self._nt_span)

            # Remove the OT+NT span from OT span
            if ot_nt_span:
                ot_span -= ot_nt_span

        # Add OT entry
        if ot_span:
            # OT must not be included in work_hours
            worked_span -= ot_span

            self.add_entry(shift_id, constants.ATT_OT, ot_span.value)

        # Add OT+NT entry
        if ot_nt_span:
            # OT+NT must not be included in work_hours
            worked_span -= ot_nt_span

            # Compute overtime in a night shift if employee is eligble
            # for a night differential
            self.add_entry(shift_id, constants.ATT_NT_OT, ot_nt_span.value)

        if self._nt_span and eligible_night_diff and worked_span:
            # Remove the OT+NT span from timesheet span.
            # OT+NT and NT should not be applied to the same span of time
            if ot_nt_span and ts_span:
                ts_span = ts_span - ot_nt_span

            night_sp = worked_span.intersection(self._nt_span)

            # Check first if OT is intersecting with the night shift setting
            ot_in_nt = ot_span.intersection(self._nt_span) if ot_span else None

            # Night Diff has a separate computation from the rest of the worked_span
            worked_span -= night_sp

            # Night shift handling
            shift_dates = self._shift_dates

            # Compute the Night Diff based on "time shift"
            if night_sp and self._time_shift == keys.NT_CARRY_OVER:
                nt_time = night_sp.value

                # if OT is inside NT setting, deduct the OT span from the NT shift.
                if nt_time and ot_in_nt:
                    nt_time -= ot_in_nt.value

                code = self._get_code(
                    shift_dates.get(self.current_date).get(keys.REST_DAY),
                    shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                    True, False, self._shift_allowed_on_holidays(shift))  # Not an OT

                self.add_entry(shift_id, code, nt_time)
            elif night_sp and self._time_shift == keys.NT_COUNTER_PER_HOUR:
                nt_span1 = night_sp.intersection(self._nt_span1)
                code1 = self._get_code(
                    shift_dates.get(self.current_date).get(keys.REST_DAY),
                    shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                    True, False, self._shift_allowed_on_holidays(shift))  # Not an OT
                if nt_span1:
                    self.add_entry(shift_id, code1, nt_span1.value)

                nt_span2 = night_sp.intersection(self._nt_span2)
                is_next_day_restday = self._is_date_restday(self.params.get('employee_uid'), self.next_date)
                code2 = self._get_code(
                    is_next_day_restday,
                    shift_dates.get(self.next_date, {}).get(keys.HOLIDAY_TYPE),
                    True, False, self._shift_allowed_on_holidays(shift))
                if nt_span2:
                    self.add_entry(shift_id, code2, nt_span2.value)

        # Compute the remaining worked_span for the other day/hour types, where applicable
        if worked_span:
            self._compute_flexi_work_hours(shift_id, worked_span.value, shift)

    def _compute_flexi_shift_with_no_core_hours(self, shift, shift_id, total_work,
                                                work_hours, is_holiday):
        """This method computes flexi shift with no core hours.

        Args:
            shift: The current shift object
            shift_id: The shift id assigned to this attendance.
            total_work: The expected total work hours of a shift.
            work_hours: The total hours rendered by an employee.
            is_holiday: A boolean parameter that denotes if the current date
                is a holiday
        """
        shift_span = self.params["shift_lookup"][shift_id]
        shift_dates = self._shift_dates
        holiday = None
        is_rest_day = self._shift_dates[self.current_date].get("rest_day")

        ot_span = self._overtime_spans.get(shift_id)
        ot_nt_span = self._overtime_night_shift_spans.get(shift_id)
        # Eligible for a night differential
        eligible_night_diff = self.params.get(keys.EN_NT_PAY)

        # override work hours if employee is TSNR to use total_hours of shift
        #if self._is_timesheet_required() is False:
        #    work_hours = spans.create_value_span(total_work)

        if work_hours and self._is_timesheet_required():
            # Limit work hours clock out when it exceeded the
            # total schedule hours (total_hours) of the shift
            clock_in_secs, clock_out_secs = (work_hours.i, work_hours.j)
            max_clock_out_secs = clock_in_secs + total_work

            if clock_out_secs > max_clock_out_secs and work_hours.value > total_work:
                work_hours = spans.create_span((clock_in_secs, max_clock_out_secs))

        # If attendance is indeed in-scope of a holiday, use the
        # Holidays Factory to determine the appropriate attendance
        # code and time.
        #
        # We also have to determine if the current shift is a night
        # shift. if yes, we have to bypass this as there's a different
        # handling of NT+HOLIDAY
        # Get shift dates to determine the holiday code
        holiday_type = self._shift_dates.get(
            self.current_date, {}).get(
                keys.HOLIDAY_TYPE, None)

        holiday = factories.HolidaysFactory(
            shift_type=keys.SH_FLEXI,
            holiday_type=holiday_type,
            holiday_entitlements=self._holiday_entitlements,
            holiday_premium_pay=self.params.get(keys.EN_PREMIUM_PAY))

        remaining_uncredited_shift = spans.create_span((shift_span.i, shift_span.i + total_work))
        unworked_code = None
        if self._is_holiday_type_entitled(holiday_type=holiday_type)\
            and self._is_holiday_pay_entitled():
            unworked_code = constants.ATT_UH
        elif is_rest_day:
            unworked_code = None
        elif work_hours and work_hours.value < total_work:
            unworked_code = constants.ATT_UNDERTIME
        else:
            unworked_code = constants.ATT_ABSENT

        nt_hours = shift_span.intersection(self._nt_span)
        time_shift = None

        if is_holiday and holiday_type == constants.ATT_REGULAR and not self._is_timesheet_required()\
            and self._is_holiday_pay_entitled():
            self.add_entry(shift_id, constants.ATT_REGULAR, total_work)
            return

        # This means there's no rendered hours
        if work_hours is None or work_hours.value == 0:
            if is_rest_day and not holiday_type:
                return self.add_entry(shift_id, constants.ATT_RD, 0)
            # Current date is holiday and if the employee is entitled for a holiday
            holiday_attendance = None
            if is_holiday and self._is_timesheet_required():
                holiday_attendance = holiday.get_timesheet_required(
                    work_hours,
                    spans.create_value_span(total_work),
                    total_break_time=0,
                    expected_work_time=total_work)
            elif not self._is_timesheet_required():
                holiday_attendance = holiday.get_timesheet_not_required(
                    work_hours,
                    spans.create_value_span(total_work),
                    total_break_time=0,
                    expected_work_time=total_work)

            if nt_hours and holiday_attendance:
                # this shift has an overnight setting
                time_shift = self._time_shift
                if time_shift == keys.NT_CARRY_OVER:
                    self.add_entry(shift_id, *holiday_attendance)
                    return

                elif time_shift == keys.NT_COUNTER_PER_HOUR:
                    expected_shift_span = spans.create_span((shift_span.i, shift_span.i + total_work))
                    day_span1 = expected_shift_span.intersection(self._day_span1)
                    day_span2 = expected_shift_span.intersection(self._day_span2)

                    if day_span1 and shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE):
                        self.add_entry(shift_id, unworked_code, day_span1.value)
                        remaining_uncredited_shift = remaining_uncredited_shift - day_span1

                    if day_span2 and shift_dates.get(self.next_date, {}).get(keys.HOLIDAY_TYPE):
                        self.add_entry(shift_id, unworked_code, day_span2.value)
                        remaining_uncredited_shift = remaining_uncredited_shift - day_span2
            else:
                if holiday_attendance:
                    self.add_entry(shift_id, *holiday_attendance)
                    return

            if self._is_timesheet_required() and remaining_uncredited_shift and work_hours is None:
                return self.add_entry(shift_id, constants.ATT_ABSENT, remaining_uncredited_shift.value)
            elif self._is_timesheet_required() and not holiday_type:
                return self.add_entry(shift_id, constants.ATT_ABSENT, total_work)
            else:
                work_hours = remaining_uncredited_shift

        if is_rest_day and is_holiday:
            if not self._holiday_and_rest_day_work_allowed(work_hours, shift):
                if remaining_uncredited_shift and unworked_code == constants.ATT_UH:
                    return self.add_entry(shift_id, unworked_code, remaining_uncredited_shift.value)
                return self.add_entry(shift_id, constants.ATT_RD, 0)
        if is_rest_day:
            if not self._rest_day_work_allowed(work_hours, shift):
                return self.add_entry(shift_id, constants.ATT_RD, 0)
        if is_holiday:
            if not self._holidays_work_allowed(work_hours, shift):
                if remaining_uncredited_shift:
                    return self.add_entry(shift_id, unworked_code, remaining_uncredited_shift.value)

        # Determine flexi shift undertime w/ the work hours value.
        # if work_hours value is less than the defined hours required and
        # timesheet is required, deduct rendered hours `work_hours` with
        # the defined hours required `total_work`
        if work_hours and work_hours.value < total_work and self._is_timesheet_required() and not is_rest_day:
            undertime_value = total_work - work_hours.value
            self.add_entry(shift_id, unworked_code, undertime_value)

        # Calculate the total time worked based on work hours and required
        # total work
        total_time_worked = min(work_hours.value if work_hours else 0, total_work)
        if total_time_worked:
            # Deduct the approved paid leave from the total worked time `total_time_worked`
            if self._pl_spans.get(shift_id) and self._is_timesheet_required():
                total_time_worked -= self._pl_spans.get(shift_id).value
                # Deduct approved UT with work_hours
                work_hours -= self._pl_spans.get(shift_id)

            # Deduct the approved unpaid leave from the total worked time `total_time_worked`
            if self._ul_spans.get(shift_id) and self._is_timesheet_required():
                total_time_worked -= self._ul_spans.get(shift_id).value
                work_hours -= self._ul_spans.get(shift_id)

            # Deduct the approved undertime from the total worked time `total_time_worked`
            if self._undertime_spans.get(shift_id):
                # Record undertime in attendance
                undertime_value = self._undertime_spans.get(shift_id).value
                self.add_entry(
                    shift_id, constants.ATT_UNDERTIME, undertime_value)
                total_time_worked -= undertime_value

                # Remove the undertime from working hours
                if work_hours:
                    work_hours -= self._undertime_spans.get(shift_id)

        if ot_span and self._is_timesheet_required():
            if self._ts_span:
                # The overtime hours must be within the timesheet span
                ot_span = self._ts_span.intersection(ot_span)
            else:
                # Void the OT when there is no timesheet
                ot_span = None

        if ot_span and not ot_nt_span and eligible_night_diff:
            ot_nt_span = ot_span.intersection(self._nt_span)

            # Remove the OT+NT span from OT span
            if ot_nt_span:
                ot_span -= ot_nt_span

        # Compute overtime on a night shift
        if ot_nt_span:
            self.add_entry(shift_id, constants.ATT_NT_OT, ot_nt_span.value)

        # Compute overtime span on non holiday dates
        if ot_span and not holiday_type:
            self.add_entry(shift_id, constants.ATT_OT, ot_span.value)

        # Compute night shift
        if eligible_night_diff:
            work_hours = self._compute_flexi_shift_nt(
                shift_id, work_hours, total_time_worked, total_work, ot_nt_span, shift)

        # This section calculates paid and unpaid leaves minus
        # the worked hours. Reason of which is to deduct the hours
        # of leave and worked hours. One use case example of this
        # is filing of half-day leave.
        work_hours_with_leave = None
        if work_hours and self._is_timesheet_required():
            # Calculate with timesheet or worked hours:
            if self._pl_spans.get(shift_id) and work_hours:
                work_hours = work_hours - self._pl_spans.get(shift_id)
            if self._ul_spans.get(shift_id) and work_hours:
                work_hours = work_hours - self._ul_spans.get(shift_id)

            if work_hours:
                work_hours_with_leave = work_hours.value
        else:
            # Calculate without timesheet. For this scenario, we have to
            # subtract the total required hours and the filed leave
            if self._pl_spans.get(shift_id) and work_hours:
                work_hours_with_leave = total_work - self._pl_spans.get(shift_id).value
            if self._ul_spans.get(shift_id) and work_hours:
                work_hours_with_leave = total_work - self._ul_spans.get(shift_id).value

        if work_hours_with_leave:
            # Work hours with deducted leaves
            self._compute_flexi_work_hours(
                shift_id, min(total_time_worked, work_hours_with_leave), shift)
        elif work_hours:
            # Work hours with no deducted leaves
            self._compute_flexi_work_hours(
                shift_id, min(total_time_worked, work_hours.value), shift)

    def _deduct_nt_to_total_hours(self, work_hours, nt_span, total_hours):
        """This method deducts the night shift time with the total hours of
        work. This is to differentiate regulars hour vs the night shift hours

        Args:
            work_hours (SimpleSpan) -- The worked hours data in span object.
            nt_span (SimeplSpan) -- The night shift data in span object.
            total_hours (int) -- The total required hours for an employee to render.
                In seconds.
        Returns:
            (int) -- The difference of total hours worked and rendered night shift.
        """
        nt_time = 0

        if work_hours:
            # ValueSpans can't use subtraction
            if isinstance(work_hours, spans.ValueSpan):
                # Disregard nt value if less than a minute
                # since having nt value of less than a minute
                # causes computed attendance to miss 1 minute
                # when converted to attendance codes and values
                nt_val = nt_span.value if nt_span.value >= 60 else 0
                work_hours = spans.create_value_span(work_hours.value - nt_val)
            else:
                work_hours = work_hours - nt_span

        if work_hours:
            # override work hours if employee is TSNR to use total_hours of shift
            if work_hours.value == total_hours:
                nt_time = nt_span.value
            else:
                # Add checker if employee's rendered hours is greater
                # than the total hours worked. if total hours is less
                # than the rendered work hours or nt hours, don't
                # subtract the total hours from it otherwise, use
                # the rendered night shift.
                if total_hours < work_hours.value:
                    nt_time = nt_span.value
                else:
                    nt_time = total_hours - work_hours.value
        else:
            nt_time = nt_span.value

        return work_hours, nt_time

    def _compute_flexi_shift_nt(self, shift_id, work_hours,
                                total_time_worked, total_work, ot_nt_span, shift):
        """Compute flexi shift night shift based on
        the defined night shift settings `self._nt_span`

        Args:
            shift_id: Int. The shift id.
            work_hours: The span of hours rendered by an employee.
            total_time_worked: Int. The number of hours currently rendered
                by an employee
        Returns:
        """
        nt_code = None
        night_shift_span = None
        first_half_nt_span = None
        second_half_nt_span = None
        within_shift = None

        if self._is_timesheet_required() and self._ts_span:
            # Based it on Timesheet
            within_shift = self._ts_span.intersection(work_hours)
        elif not self._is_timesheet_required() and work_hours:
            # Based it on Worked Hours
            within_shift = work_hours

        if within_shift:
            # Remove the OT+NT span from timesheet span.
            # OT+NT and NT should not be applied to the same span of time
            if ot_nt_span:
                within_shift = within_shift - ot_nt_span

            if self._time_shift == keys.NT_CARRY_OVER:
                # Handle CARRY OVER type of night shift.
                # Carry Over, means that the whole span of a night shift
                # will conform to the attendance of the 2nd half
                # (Given that the median is 12am) of a night shift.
                # For example: if next day is a regular holiday, the attendance for this
                # would be RH+NT

                # Intersect the timesheet span with night shift span. You will get
                # the common timeframe with this.

                # Deduct the computed night shift span to work hours to get the
                # total unworked hours.
                night_shift_span = within_shift.intersection(self._nt_span)

                # Checking if employee did do a night shift
                if night_shift_span:
                    # Deduct the night shift  hours to the total time worked
                    work_hours, night_shift_time = self._deduct_nt_to_total_hours(
                        work_hours, night_shift_span, total_time_worked)

                    # Get the night shift code
                    nt_code = self._get_code(
                        self._shift_dates.get(self.current_date).get(keys.REST_DAY),
                        self._shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                        True, False, self._shift_allowed_on_holidays(shift))  # Not an OT

                    # Generate the attendance value and code
                    self.add_entry(shift_id, nt_code, night_shift_time)
            elif self._time_shift == keys.NT_COUNTER_PER_HOUR:
                # Handle COUNTER PER HOUR type of night shift.
                # Counter per hours, means that the the 1st half of a night
                # shift (Given that the median is 12am) will conform to the
                # current date's attendance type. Same goes as well to
                # the 2nd half.

                # Compute for the 1st half of a night shift
                # Get intersction of timesheet span and the 1st night shift span
                first_half_nt_span = within_shift.intersection(self._nt_span1)
                if first_half_nt_span: # Checking if employee did do a night shift
                    # Deduct the night shift  hours to the total time worked
                    work_hours, night_shift_time = self._deduct_nt_to_total_hours(
                        work_hours, first_half_nt_span, total_time_worked)

                    # Get the night shift code for the first half night shift span
                    first_half_nt_code = self._get_code(
                        self._shift_dates.get(self.current_date).get(keys.REST_DAY),
                        self._shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
                        True, False, self._shift_allowed_on_holidays(shift))  # Not an OT

                    # Generate the attendance value and code based from the first half nt code
                    self.add_entry(shift_id, first_half_nt_code, night_shift_time)

                    total_time_worked -= night_shift_time

                # Compute for the 2nd half of a night shift
                # Get intersection of timesheet span and the 2nd night shift span
                second_half_nt_span = within_shift.intersection(self._nt_span2)
                if second_half_nt_span: # Checking if employee did do a night shift
                    # Get the night shift code
                    # Deduct the night shift  hours to the total time worked
                    work_hours, night_shift_time = self._deduct_nt_to_total_hours(
                        work_hours, second_half_nt_span, total_time_worked)

                    # Get the night shift code for the second half night shift span
                    is_next_day_restday = self._is_date_restday(self.params.get('employee_uid'), self.next_date)
                    second_half_nt_code = self._get_code(
                        is_next_day_restday,
                        self._shift_dates.get(self.next_date, {}).get(keys.HOLIDAY_TYPE),
                        True, False, self._shift_allowed_on_holidays(shift))  # Not an OT

                    # Generate the attendance value and code based from the second half nt code
                    self.add_entry(shift_id, second_half_nt_code, night_shift_time)

                    total_time_worked -= night_shift_time

        return work_hours

    def _compute_flexi_work_hours(self, shift_id, total_time_worked, shift={}):
        # Process work hours.
        # TODO handle multiple days ::
        shift_dates = self._shift_dates
        code = self._get_code(
            shift_dates.get(self.current_date).get(keys.REST_DAY),
            shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE),
            False,
            False,
            self._shift_allowed_on_holidays(shift)
            )
        self.add_entry(shift_id, code, total_time_worked)

    def _compute_flexi_shift(self, shift_id, shift, shift_span, total_work):
        print('tinatawag to?????????????????????????????????')
        # get total hours
        work_hours = None

        # Set the attendance date of an employee to ABSENT if
        # the employee is not hired or is terminated on the
        # employee's assigned shift.
        if not self._is_hired() or self._is_terminated():
            self.add_entry(
                shift_id, constants.ATT_ABSENT, total_work)
            return

        if self._regular_spans.get(shift_id):
            work_hours = shift_span.intersection(
                self._regular_spans.get(shift_id))
        elif self._ts_span:
            work_hours = shift_span.intersection(self._ts_span)

        paid_leave_span = self._pl_spans.get(shift_id)

        if paid_leave_span:
            # if filed paid leave duration is less than the defined shift
            # use its value for paid leave attendance otherwise, use the
            # defined total_work as attendance value.
            if paid_leave_span.value < total_work:
                self.add_entry(shift_id,
                               constants.ATT_PAID_LEAVE,
                               paid_leave_span.value)
            else:
                self.add_entry(shift_id,
                               constants.ATT_PAID_LEAVE,
                               total_work)
                return

            work_hours = self._pl_spans.get(shift_id).union(work_hours)

        unpaid_leave_span = self._ul_spans.get(shift_id)

        if unpaid_leave_span:
            # if filed unpaid leave duration is less than the defined shift
            # use its value for unpaid leave attendance otherwise, use the
            # defined total_work as attendance value.
            if unpaid_leave_span.value < total_work:
                self.add_entry(shift_id,
                               constants.ATT_UNPAID_LEAVE,
                               unpaid_leave_span.value)
            else:
                self.add_entry(shift_id,
                               constants.ATT_UNPAID_LEAVE,
                               total_work)
                return

            work_hours = unpaid_leave_span.union(work_hours)

        today_is_a_holiday = self._shift_dates.get(
            self.current_date, {}).get(keys.HOLIDAY_TYPE, None) is not None

        if shift.get(keys.CORE_TIMES, []):
            return self._compute_flexi_shift_with_core_hours(shift,
                                                            shift_id,
                                                            shift.get(
                                                                keys.CORE_TIMES, []),
                                                            shift.get(
                                                                keys.START_TIME
                                                            ),
                                                            total_work,
                                                            work_hours,
                                                            today_is_a_holiday)
        else:
            return self._compute_flexi_shift_with_no_core_hours(shift,
                                                                shift_id,
                                                                total_work,
                                                                work_hours,
                                                                today_is_a_holiday)

    def _check_rest_day_work(self, is_holiday, def_shift=None):
        """This method computes any work rendered during rest day
        on a default schedule.

        Args:
            is_holiday (bool) -- Flag to determine if the current date
                is a holiday.
        """
        # Defaulting to shift id = RD, should not happen
        # if the company setup is correct.
        shift_id = def_shift.get(keys.SHIFT_ID, "RD") if def_shift else "RD"
        # Flag that there's a rest day work if cursor reaches here
        self.has_rest_day_work = True
        rd_hours_value = None
        rd_ts_span = self._ts_span
        overtime_spans = self._overtime_spans

        # Set the attendance date of an employee to ABSENT if
        # the employee is not hired or is terminated on the
        # employee's assigned DEFAULT shift.
        if not self._is_hired() or self._is_terminated():
            # Terminating this method with a return response only
            # results to an ABSENT attendance.
            return

        # create day span
        day_span = spans.create_span((
            dateutils.datetime_to_secs(self.current_date + " 00:00"),
            dateutils.datetime_to_secs(self.next_date + " 00:00")
        ))

        # check if there are override items on a rest day
        if self._override_items.get(shift_id):
            self._process_override_items(shift_id)
            return

        # check for NT hours first: use_ts_span = time dispute union time_ts
        shift_dates = self._shift_dates
        if self._nt_span and self.params.get(keys.EN_NT_PAY):
            day_span = day_span - self._nt_span
            ts_nt_span = None

            if rd_ts_span:
                ts_nt_span = rd_ts_span.intersection(self._nt_span)

                if ts_nt_span:
                   rd_ts_span = rd_ts_span - ts_nt_span
            elif not self._is_timesheet_required():
                ts_nt_span = self._nt_span

            rd_nt_span = None

            ot_span_shift = overtime_spans.get(shift_id, None)

            if ts_nt_span and ot_span_shift:
                rd_nt_span = ts_nt_span.intersection(ot_span_shift)

            if rd_nt_span:
                ot_span_shift = ot_span_shift - rd_nt_span
                overtime_spans[shift_id] = ot_span_shift

                rd_nt_span_day_1 = rd_nt_span.intersection(self._day_span1)
                rd_nt_span_day_2 = rd_nt_span.intersection(self._day_span2)

                if rd_nt_span_day_1:
                    rd_nt_code_day_1 = self.get_restday_code(self.current_date, True)
                    self.add_entry(shift_id, rd_nt_code_day_1, rd_nt_span_day_1.value)

                if rd_nt_span_day_2:
                    rd_nt_code_day_2 = self.get_restday_code(self.next_date, True)
                    self.add_entry(shift_id, rd_nt_code_day_2, rd_nt_span_day_2.value)


        # check for rd_hours or Rest day hours rendered
        # with timesheet.
        rd_hours = None
        code = None
        single_ot_span = False
        if rd_ts_span:
            rd_hours = day_span.intersection(rd_ts_span)
            code = self._get_code(
                True, shift_dates.get(self.current_date).get(
                    keys.HOLIDAY_TYPE),
                False, False)

        total_ot = None
        # handle OT filed in a rest day with timesheet
        if rd_hours and overtime_spans:
            total_ot = next(iter(overtime_spans.values()))

            # Single OT handling
            if len(overtime_spans) == 1:
                for ot_shift_id in overtime_spans:
                    span_intersection = rd_hours.intersection(overtime_spans[ot_shift_id])

                    if span_intersection:
                        rd_hours -= span_intersection
                single_ot_span = True
            else:
                # Handling of multiple approved OTs in a rest day.
                for shift_id, ot_span in overtime_spans.items():
                    total_ot = ot_span

                    if ot_span:
                        rd_hours -= ot_span
                        total_ot += ot_span
        elif not rd_hours and not self._is_timesheet_required() and overtime_spans:
            # At this point employee has no timesheet and he/she
            # is not timesheet required.
            single_ot_span = True
            total_ot = next(iter(overtime_spans.values()))
        # Add RD hours
        if rd_hours and not total_ot:
            #Don't compute rest day hours without approved overtime
            self.add_entry(shift_id, constants.ATT_RD, 0)
        # RD OT !NEW. Applies to RD+OT w/ or w/o timesheet with entitlements required
        elif total_ot and self.params.get('overtime', False) and self.params.get(
                'rest_day_pay', False):

            # Get hours per day to determine limit
            if single_ot_span:
                # Time records is now required before overtime request is valid and computed
                if rd_ts_span and day_span.intersection(rd_ts_span) and self._is_timesheet_required():
                    total_ot = day_span.intersection(rd_ts_span).intersection(total_ot)
                # After intersecting with time records and TSR
                # If there's no intersection, then there's no OT to process
                # If TNSR, total_ot will be based from the actual approved OT
                if total_ot:
                    # Get Default shift hours per day settings
                    def_shift = self.get_default_shift()
                    def_shift_hours_per_day = None
                    employee_hours_per_day = None

                    if def_shift:
                        def_shift_hours_per_day = datetime.datetime.strptime(
                            def_shift['total_hours'], '%H:%M')
                        def_shift_hours_per_day = def_shift_hours_per_day.hour

                    # Get Employee 201 hours per day settings
                    if self.params.get(keys.HOURS_PER_DAY):
                        employee_hours_per_day = self.params.get(keys.HOURS_PER_DAY)
                        employee_hours_per_day = datetime.datetime.strptime(
                        employee_hours_per_day, '%H.%M')
                        employee_hours_per_day = employee_hours_per_day.hour

                    hours_per_day = employee_hours_per_day or def_shift_hours_per_day or 9
                    hours_per_day = datetime.timedelta(hours=int(hours_per_day))

                    if rd_ts_span and self._is_timesheet_required():
                        ts_span = day_span.intersection(rd_ts_span)
                        ot_with_ts_span = ts_span.intersection(total_ot)
                        ot_with_ts_span_in_seconds = 0
                        if ot_with_ts_span:
                            ot_with_ts_span_in_seconds = ot_with_ts_span.value
                        ot_duration = datetime.timedelta(seconds=ot_with_ts_span_in_seconds)
                    else:
                        ot_duration = datetime.timedelta(seconds=total_ot.value)

                    if ot_duration <= hours_per_day:
                        # ot within the duration of hours per day gets treated as normal RD hours
                        if code == constants.ATT_REGULAR:
                            code = constants.ATT_RD
                        self.add_entry(shift_id, code if code else constants.ATT_RD, total_ot.value)

                    elif ot_duration > hours_per_day:
                        # get excess duration
                        excess_duration = (ot_duration - hours_per_day)
                        excess_duration = dateutils.convert_to_mins(str(excess_duration))

                        if excess_duration:
                            excess_duration_in_sec = (excess_duration * 60)
                            self.add_entry(shift_id, code if code else constants.ATT_RD, (
                                total_ot.value - excess_duration_in_sec))
                            self.add_entry(shift_id, constants.ATT_RD_OT, excess_duration_in_sec)
                else:
                    # If no OT after intersecting with time records then no OT to process
                    self.add_entry(shift_id, constants.ATT_RD, 0)
            else:
                # Default
                self.add_entry(shift_id, constants.ATT_RD_OT, total_ot.value)

        else:
            # RD worked hours still not valid without approved O.T.
            self.add_entry(shift_id, constants.ATT_RD, 0)

    def _compute(self):
        """This is the main call"""
        shifts = self._ordered_shifts # type: list
        # Get user defined rest days and put in a list
        udrd = self.params.get('rest_days', [])
        user_rds = []
        for user_rd in udrd:
            if user_rd['start_date'] and user_rd['start_date'] not in user_rds:
                user_rds.append(user_rd['start_date'])

        is_rest_day = self._shift_dates.get(
            self.current_date, {}).get(keys.REST_DAY, False)

        # Get holiday type
        is_holiday = self._shift_dates.get(
            self.current_date, {}).get(keys.HOLIDAY_TYPE, None) is not None

        if not shifts:
            def_shift = self.get_default_shift()
            if is_rest_day:
                # if rest day is a default shift. Ex. Saturday and Sunday
                if (def_shift and def_shift.get(keys.DAY_TYPE) == keys.REST_DAY):

                    self._check_rest_day_work(is_holiday, def_shift)

                    # Append shift when there's a timesheet
                    if self.params.get(keys.RAW_TIMESHEET, None):
                        shifts.append(def_shift)

                    return
                elif self.is_user_defined_rest_day():
                    # if rest day is a user defined rest day
                    ud_rd_data = self.get_user_defined_rest_day()
                    self._check_rest_day_work(is_holiday, ud_rd_data)

                    # Append shift when there's a timesheet
                    if self.params.get(keys.RAW_TIMESHEET, None):
                        shifts.append(ud_rd_data)

                    return
            elif def_shift:
                # Not a rest day but a default schedule
                shifts.append(def_shift)

        if not shifts:  # Nothing to compute for
            return

        self.params["shift_lookup"] = {}

        for shift in shifts:
            shift_id = str(shift.get(keys.SHIFT_ID))
            shift_type = shift.get(keys.TYPE, keys.SH_FIXED)

            # check if there are override items,
            if self._override_items.get(shift_id):
                self._process_override_items(shift_id)
                continue

            # get the current shift span. This is the default span
            # for attendance that have the necessary data for calculation.
            # Possible requirements:
            # 1. Timesheet
            # 2. Has a shift
            # 3. Flexi hours with core hours
            shift_span = self._create_span(shift.get(keys.START_TIME),
                                           shift.get(keys.END_TIME),
                                           ref_time=shift.get(keys.START_TIME))

            self.params["shift_lookup"][shift_id] = shift_span

            # Get user defined required total hours to work.
            # This only works for flexi type of shift.
            total_work = dateutils.convert_to_mins(
                shift.get(keys.TOTAL_HOURS, "00:00")) * 60  # secs

            if shift.get(keys.DEFAULT, False):
                total_work = self._get_expected_hours_to_secs()

            # Determine the computation based on type of shift
            if shift_type == keys.SH_FIXED: # Fixed
                # Calculate fixed shifts
                self._compute_fixed_shift(shift_id, shift, shift_span)
            elif shift_type == keys.SH_FLEXI: # Flexi
                # Calculdate flexi shifts
                self._compute_flexi_shift(shift_id, shift, shift_span, total_work)

            # Determine the timesheet if regular or etc..
            self._determine_timesheet(shift_id, shift_span, shift, total_work)

    def _process_override_items(self, shift_id):
        override_items = self._override_items.get(shift_id)

        shift_dates = self._shift_dates
        holiday_type = shift_dates.get(
            self.current_date).get(keys.HOLIDAY_TYPE)
        rest_day = shift_dates.get(self.current_date).get(keys.REST_DAY)
        for key, value in override_items.items():
            code = None
            if key == constants.ATT_NT:
                code = self._get_code(rest_day, holiday_type, True, False)
            elif key == constants.ATT_OT:
                code = self._get_code(rest_day, holiday_type, False, True)
            elif key == constants.ATT_NT_OT:
                code = self._get_code(rest_day, holiday_type, True, True)
            elif key == constants.ATT_REGULAR:
                code = self._get_code(rest_day, holiday_type, False, False)
            else:
                code = key
            self.add_entry(shift_id, code, value * 60)

    def _get_code(self, rest, htype, nt, ot, on_holidays=None):
        # return combo codes
        if htype and not self._check_premium_pay():
            htype = None  # should ignore holiday type so it reverts to regular
        if htype and constants.ATT_REGULAR in htype:
            htype = self._clean_swh_code(htype)
        if htype and on_holidays == False:
            # should ignore holiday type if assigned shift have schedule_on_holidays false
            htype = None
        if rest and not self.params.get(keys.EN_RD_PAY):
            rest = False  # should ignore rest day so it reverts to regular
        if nt and not self.params.get(keys.EN_NT_PAY):
            nt = False
        if ot and not self.params.get(keys.EN_OVERTIME):
            ot = False
        return models.HourCode(rest, htype, nt, ot)

    def _get_work_hours(self, shift_id, shift_span):
        if self.params.get(keys.TIMESHEET_REQUIRED):
            return shift_span.intersection(self._ts_span)

        return shift_span

    def get_shift_timesheet(self, shift_id):
        """Returns timesheet entries for shift"""
        ts = self._shift_ts_lookup.get(shift_id) or []
        shift_span = None

        if not ts:
            raw_ts = self.params.get("raw_timesheet")  # type:list
            if not raw_ts:
                return ts

            if self.params.get("shift_lookup", None) and \
            self.params['shift_lookup'].get(shift_id, None):
                shift_span = self.params['shift_lookup'][shift_id]

            if not shift_span:
                return ts

            shift_date_coverage = [
                dateutils.get_date_from_timestamp(shift_span.i),
                dateutils.get_date_from_timestamp(shift_span.j)
            ]

            while raw_ts:
                first, second, raw_ts = raw_ts[0], raw_ts[1] if len(
                    raw_ts) >= 2 else {}, raw_ts[2:]
                first_ts = int(first.get("timestamp"))
                ts_date = dateutils.get_date_from_timestamp(first_ts)

                # Skip ts if ts_date is not within the current
                # and next dates
                if not ts_date in [self.current_date, self.next_date]:
                    continue

                is_ts_date_covered = ts_date in shift_date_coverage
                if first.get("state"):  # if it's a clockin
                    if second:
                        if second.get("state") is False:  # second is a clockout
                            sp = spans.create_span(
                                (first_ts, second.get("timestamp")))
                            second_ts_date = dateutils.get_date_from_timestamp(
                                second.get("timestamp"))
                            is_second_ts_date_covered = second_ts_date in shift_date_coverage
                            if (shift_span.intersection(sp) and is_ts_date_covered and
                                is_second_ts_date_covered):
                                ts.extend([first, second])
                        else:  # second is also a clockin
                            raw_ts.insert(0, second)
                            end_time = shift_span.j  # get shift_span_end
                            if first_ts < end_time:
                                sp = spans.create_span((first_ts, end_time))
                                if shift_span.intersection(sp) and is_ts_date_covered:
                                    ts.append(first)
                    else:  # there is no second
                        end_time = shift_span.j
                        if first_ts < end_time:
                            sp = spans.create_span((first_ts, end_time))
                            if shift_span.intersection(sp) and is_ts_date_covered:
                                ts.append(first)
                else:  # first is a clockout
                    if second:
                        raw_ts.insert(0, second)
                    start_time = shift_span.i
                    if start_time < first_ts:
                        sp = spans.create_span((start_time, first_ts))
                        if shift_span.intersection(sp) and is_ts_date_covered:
                            ts.append(first)
        return ts

    def get_shift_raw_timesheet(self, shift_id, shift_type):
        """Get raw timesheet for the specified shift

        Getting raw timesheet for the specified shift should follow the following conditions:

        1. Time records occurred within the shift must be included to that shift.
        2. Time records occurred before the first shift must be included to the first shift.
        3. Time records occurred after the last shift must be included to the last shift.
        4. Time records occurred within the duration of the end of shift and start of next shift
           must be included to the previous shift.

        Arguments:
            shift_id {string} -- Shift ID

        Returns:
            list
        """
        date = self.params.get(keys.DATE)
        raw_timesheet = self.params.get(keys.RAW_TIMESHEET)

        shift_span = None
        is_multishift = len(self._ordered_shifts) > 1

        # Add raw timesheet on rest days
        is_rest_day = self._shift_dates.get(self.current_date, {}).get(keys.REST_DAY, False)
        def_shift = self.get_default_shift()
        if is_rest_day and def_shift:
            rd_span = self._create_span(def_shift.get(keys.START_TIME),
                                            def_shift.get(keys.END_TIME),
                                            ref_time=def_shift.get(keys.START_TIME))
            shift_span = rd_span
        else:
            if not self.params.get('shift_lookup') or not self.params['shift_lookup'].get(shift_id):
                return []

            shift_span = self.params['shift_lookup'][shift_id]

        if not shift_span or not self._ordered_shifts:
            return []

        timesheet = []

        # Get the timestamps for the start and end of the current date
        day_start_timestamp = dateutils.datetime_to_secs('%s 00:00' % date)
        current_day_end_timestamp = day_start_timestamp + constants.SECONDS_IN_A_DAY
        day_end_timestamp = current_day_end_timestamp

        # Add another second to include max hour allowance
        post_shift_allowance_in_seconds = (
            constants.POST_SHIFT_TIMESHEET_HOUR_ALLOWANCE * constants.SECONDS_PER_HOUR + 1)

        # extend timesheet coverage when there is shift
        if all([shift_span, shift_span.j]) and shift_type == keys.SH_FIXED and not is_rest_day:
            # obtain timestamps from day end of night shift
            day_end_timestamp = (shift_span.j + post_shift_allowance_in_seconds)

        # Get the timestamps for the start and end of the current shift
        shift_start_timestamp, shift_end_timestamp = (shift_span.i, shift_span.j)

        # Check if the provided shift_id is the first shift of the day
        is_first_shift = str(self._ordered_shifts[0]['shift_id']) == shift_id

        # Check if the provided shift_id is the last shift of the day
        is_last_shift = str(self._ordered_shifts[-1]['shift_id']) == shift_id if is_multishift else True

        current_shift_index = self._shifts_index.get(shift_id)

        if (not is_multishift or is_first_shift) and shift_type == keys.SH_FIXED:
            # First shift of the day-- or only shift of the day
            # Time records occurred outside and before the first shift
            # of the day must be included to the time records of the first shift
            if self._previous_shift is None:
                timesheet.extend(filter_timesheet(raw_timesheet, day_start_timestamp, shift_start_timestamp))
            else:
                previous_end_timestamp = self._previous_shift['end_timestamp'] + post_shift_allowance_in_seconds
                timesheet.extend(filter_timesheet(raw_timesheet, previous_end_timestamp, shift_start_timestamp))
            # Add 1 second to avoid double entries
            shift_start_timestamp += 1

        # Consider early Clock INs in flexi shifts
        if shift_type == keys.SH_FLEXI:
            shift_start_timestamp -= constants.PRE_SHIFT_START_TIMESHEET_HOUR_ALLOWANCE * 60 * 60

        # Add the time records occurred within the current shift
        # to the time records of current shift
        timesheet.extend(filter_timesheet(
            raw_timesheet, shift_start_timestamp, shift_end_timestamp))
        # Add 1 second to avoid double entries
        shift_end_timestamp += 1

        if is_multishift and shift_type == keys.SH_FIXED:
            next_shift_index = current_shift_index + 1

            # Determine if there is a shift after the current shift
            if next_shift_index < len(self._ordered_shifts):
                next_shift = self._ordered_shifts[next_shift_index]
                next_shift_span = self.params['shift_lookup'].get(str(next_shift['shift_id']))

                if next_shift_span:
                    # Get the timestamps for the start and end of the next shift
                    next_shift_start_timestamp, next_shift_end_timestamp = (
                        next_shift_span.i, next_shift_span.j)

                    # Include the time records occurred after the end of the current shift and
                    # before the start of the next shift to the time records of current shift
                    timesheet.extend(filter_timesheet(
                        raw_timesheet, shift_end_timestamp, next_shift_start_timestamp))

        if is_last_shift and shift_type == keys.SH_FIXED:
            # For the last shift of the day, include the time records occurred
            # after the end of the last shift and before the end of the day
            timesheet.extend(filter_timesheet(
                    raw_timesheet, shift_end_timestamp, day_end_timestamp))

            # For restday, if the last time record is clock-in
            # and timestamp is within the current day then
            # find the first clock-out on the next day
            # i.e. Clock-in: 23:00
            #      Clock-out: 07:00(Next Day)
            if is_rest_day and timesheet and timesheet[-1]['state'] \
                and timesheet[-1]['timestamp'] < current_day_end_timestamp:
                next_date_end_timestamp = dateutils.datetime_to_secs('%s 00:00' % self.next_date) \
                        + constants.SECONDS_IN_A_DAY
                next_day_ts = filter_timesheet(raw_timesheet, current_day_end_timestamp, next_date_end_timestamp - 1)

                first_timerecord_on_next_day = next(iter(next_day_ts), None) if next_day_ts else None

                if first_timerecord_on_next_day and first_timerecord_on_next_day['state'] == False:
                    timesheet.extend([first_timerecord_on_next_day])

        return timesheet

    @property
    def ordered_shifts(self):
        """Get shifts ordered by start time"""
        return self._ordered_shifts

    @property
    def attendance(self):
        """Attendance computation dictionary"""
        return self._attendance

    def get_scheduled_hours(self):
        """This method gets the total required hours of an
        employee in a date. Total hours is retrieved from the
        shifts that are assigned to an employee in a day.

        Total hours will also be deducted with all the breaks
        in a shift.

        Args:
            N/A
        Returns:
            The computed scheduled hours in human readable time.
        """
        scheduled_hours = 0
        if self.params.get("shifts", []):
            for shift in self.params["shifts"]:
                # Get total hours in a shift
                # Convert the total hours into mins. This will be used
                # for calculations
                mins = dateutils.convert_to_mins(shift["total_hours"])

                # Create a lambda function that will accept an hour
                # in human time (HH:MM) then convert it to mins.
                to_mins = lambda hour: dateutils.convert_to_mins(hour)

                # Total the breaks in a shift.
                # Get the breaks first.
                breaks = shift.get("breaks", [])
                # Total all break hours (in minutes)
                total_breaks = [to_mins(b["break_hours"]) for b in breaks if not b.get("is_paid", False)]

                # Now to get the primary definition of a scheduled hour,
                # Deduct the total breaks with the total hours (in minutes),
                # Then get the sum of all the total hours in every shift
                scheduled_hours += mins - sum(total_breaks)
        elif self.params.get("default_shift", {}):
            # Handle the default shift calculation here.
            # Since there's no defined total hours for a default shift
            # get the duration of the default shift's work_start
            # and work_end in minutes
            shift = self.params["default_shift"]

            if shift["day_type"] != keys.REST_DAY and not self.is_user_defined_rest_day():
                # Determine if default shift will carry over to the next date
                nt_times = dateutils.is_times_carry_over_to_next_date(
                    shift["work_start"],
                    shift["work_end"],
                    self.params['date']
                )
                if nt_times:
                    work_duration_params = {
                        'start': nt_times['time1'],
                        'end': nt_times['time2'],
                        'format': "%Y-%m-%d %H:%M:%S"
                    }
                else:
                    work_duration_params = {
                        'start': shift["work_start"],
                        'end': shift["work_end"],
                        'format': "%H:%M:%S"
                    }
                work_duration = dateutils.datetime_range_to_mins(
                    work_duration_params['start'],
                    work_duration_params['end'],
                    work_duration_params['format']
                )
                # Same as the work_start and work_end, to get the break
                # duration get the work_break_start and work_break_end
                # in minutes.
                break_duration = dateutils.datetime_range_to_mins(
                    shift["work_break_start"],
                    shift["work_break_end"],
                    "%H:%M:%S"
                )

                # Do the deduction of the break duration with the
                # work duration.
                scheduled_hours = work_duration - break_duration

        # Convert the computed scheduled hours (in minutes) into
        # a human readable time. Ex. 4 hours is 400
        return scheduled_hours

    def get_restday_code(self, ot_date, is_nt):
        is_restday = False
        holiday_type = None
        is_ot = True

        shift_date = self._shift_dates.get(ot_date, {})

        if shift_date:
            holiday_type = shift_date.get(keys.HOLIDAY_TYPE)

        # Always based the day type on the current day
        is_restday = self._shift_dates.get(self.current_date).get(keys.REST_DAY)

        if ot_date == self.next_date:
            # For carry over, based the holiday on the current date
            if self._time_shift == keys.NT_CARRY_OVER:
                holiday_type = self._shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE)

        # It's either Restday or OT but can't be both
        # TODO: For Restday, check whether rendered hours exceeds the expected hours.
        # If yes, the attendance code must be RD+OT. Otherwise, RD only
        if is_restday:
            is_ot = False

        return self._get_code(is_restday, holiday_type, is_nt, is_ot)

    def get_first_last_time_records(self):
        if self._ts_span:
            if len(self._ts_span) > 1:
                first_clock_in = self._ts_span[0].i
                last_clock_out = self._ts_span[-1].j
            else:
                first_clock_in = self._ts_span.i
                last_clock_out = self._ts_span.j
        else:
            first_clock_in = 0
            last_clock_out = 0

        return (first_clock_in, last_clock_out)

    def handle_flexi_break(self,
        work_hours, tardiness, undertime,
        wh_deduction, flexi_breaks,
        first_clock_in, last_clock_out,
        shift, shift_id):

        flexi_overbreaks = 0

        for fx_break in flexi_breaks:
            br_value = dateutils.convert_to_mins(fx_break.get(keys.BREAK_HOURS)) * 60

            fx_break_is_paid = fx_break.get(keys.IS_PAID_BREAK, False)
            is_flexi_break = fx_break.get(keys.TYPE) == keys.BR_FLEXI

            fx_span = self._get_break_span(
                [fx_break], ref_time=shift.get(keys.START_TIME), show_all=True)

            net_fx_span = None

            # Break must be within the timesheet span
            if fx_span and self._is_timesheet_required() and self._ts_span:
                fx_break_spans = None

                if undertime:
                    if fx_break_spans is None:
                        fx_break_spans = fx_span.intersection(undertime)
                    else:
                        fx_break_spans += fx_span.intersection(undertime)

                if tardiness:
                    if fx_break_spans is None:
                        fx_break_spans = fx_span.intersection(tardiness)
                    else:
                        fx_break_spans += fx_span.intersection(tardiness)

                if fx_break_spans:
                    for fx_break_span in fx_break_spans:
                        # First clock-in must be earlier than the break start time
                        # and last clock-out must be later than break end time
                        if (fx_break_is_paid and not is_flexi_break) or (
                            first_clock_in < fx_break_span.i and last_clock_out > fx_break_span.j):
                                if net_fx_span is None:
                                    net_fx_span = fx_break_span
                                else:
                                    net_fx_span += fx_break_span

            # There is a break within the prescribed range
            if net_fx_span:
                # Handle multiple breaks in a shift
                for fx_break_span in net_fx_span:
                    if not undertime and not tardiness:
                        break

                    span_value = 0

                    # Determine which part of the undertime is covered by
                    # the allocated break hours. Excess hours will be considered as overbreak
                    if br_value:
                        # Get the consumed break hours
                        span_value = min([fx_break_span.value, br_value])

                        if span_value:
                            # Remove the consumed break hours from the allocated hours
                            br_value -= span_value

                            # Create consumed break span
                            br_value_span = spans.create_span((
                                fx_break_span.i,
                                fx_break_span.i + span_value
                            ))

                            if tardiness:
                                tardiness -= br_value_span

                            if undertime:
                                # Remove the consumed break span from undertime span
                                undertime -= br_value_span

                            if fx_break_is_paid:
                                work_hours = work_hours.union(br_value_span)

                    if not undertime:
                        break

                    # Capture overbreak
                    if (is_flexi_break or not fx_break_is_paid) \
                        and span_value < fx_break_span.value:
                            fx_overbreak = fx_break_span.value - span_value
                            flexi_overbreaks += fx_overbreak

                            span_i = fx_break_span.i + span_value
                            span_j = span_i + (fx_overbreak)

                            # Create overbreak span
                            fx_overbreak_span = spans.create_span(
                                (span_i,span_j)
                            )

                            # Remove the overbreak span from undertime span
                            undertime -= fx_overbreak_span

            # Remove the unused break from work_hours
            if not fx_break_is_paid and br_value:
                if work_hours:
                    deductable_span = fx_span.intersection(work_hours)

                    if deductable_span:
                        span_value = min([deductable_span.value, br_value])

                        unpaid_break_span = spans.create_span((
                            deductable_span.i,
                            deductable_span.i + span_value
                        ))

                        work_hours -= unpaid_break_span
                        br_value -= span_value

                wh_deduction += br_value

        if flexi_overbreaks:
            self.add_entry(shift_id, constants.ATT_OVERBREAK, flexi_overbreaks)

        return (work_hours, tardiness, undertime, wh_deduction)

    def _compute_holiday_attendance_within_shift(
        self,
        shift,
        shift_span,
        remaining_uncredited_shift,
        creadited_ot_span=None
        ):
        """ Compute holiday attendance within the core hours (ticket #7838)"""
        # shift: current shift
        # shift_span: current shift span
        # remaining_uncredited_shift: remaining uncredited shift after removing leaves
        # creadited_ot_span: OT span which is already credited
        shift_id = str(shift.get(keys.SHIFT_ID))
        shift_dates = self._shift_dates
        special_working_holiday = False
        # breaks
        fixed_breaks, flexi_breaks, floating_breaks, total_break = self._get_breaks(
            shift)
        is_rest_day = self._shift_dates.get(self.current_date, {}).get("rest_day")
        _flexi_breaks = self._get_break_span(
            flexi_breaks, ref_time=shift.get(keys.START_TIME), show_all=True
        )
        holiday_type = self._shift_dates.get(
            self.current_date, {}).get(
                keys.HOLIDAY_TYPE, None)

        # hours rendered by employee within shift
        shift_ts_span = shift_span.intersection(self._ts_span)
        valid_flexi_break_span= None
        valid_floating_break_span = None
        if flexi_breaks:
            valid_flexi_break_span = self._get_valid_break_span(
                _flexi_breaks, shift_ts_span,
                self.get_break_total_time(flexi_breaks),
                shift,
                False,
                shift_span
            )
            if valid_flexi_break_span:
                remaining_uncredited_shift -= valid_flexi_break_span
                if shift_ts_span:
                    shift_ts_span -= valid_flexi_break_span
        if floating_breaks:
            valid_floating_break_span = self._get_valid_break_span(
                remaining_uncredited_shift,
                shift_ts_span,
                self.get_break_total_time(floating_breaks),
                shift,
                True,
                shift_span
            )
            if valid_floating_break_span:
                remaining_uncredited_shift -= valid_floating_break_span
                if shift_ts_span:
                    shift_ts_span -= valid_floating_break_span
        if creadited_ot_span:
            # remove the span which is already credited
            shift_ts_span -= creadited_ot_span
            remaining_uncredited_shift -= creadited_ot_span

        worked_code = self._get_code(
            self._shift_dates.get(self.current_date).get("rest_day"),
            holiday_type,
            False,
            False,
            self._shift_allowed_on_holidays(shift)
        )
        if is_rest_day:
            if not self._holiday_and_rest_day_work_allowed(shift_ts_span, shift):
                worked_code = None
        elif not self._holidays_work_allowed(shift_ts_span, shift):
            # only calculate rendered hours without OT if employee has assigned
            # a custom shift on the said day and shift have on_holidays true
            worked_code = None
        if is_rest_day and not worked_code:
            # if the current day is rest day + holiday and employee is
            # not allowed to render hours then return with RD=0.
            return self.add_entry(shift_id, constants.ATT_RD, 0)
        elif is_rest_day:
            unworked_code = None
        elif self._is_holiday_type_entitled(holiday_type=holiday_type)\
        and self._is_holiday_pay_entitled():
            unworked_code = constants.ATT_UH
        elif not self._holidays_work_allowed(shift_ts_span, shift):
            unworked_code = constants.ATT_ABSENT
        elif shift_span.intersection(self._ts_span)\
        and not self._is_holiday_type_entitled(holiday_type=holiday_type):
            unworked_code = constants.ATT_UNDERTIME
        elif holiday_type == constants.ATT_REGULAR and not self._is_timesheet_required()\
        and self._is_holiday_pay_entitled() and not self._is_holiday_type_entitled(holiday_type=holiday_type):
            unworked_code = constants.ATT_REGULAR
            special_working_holiday = True
        else:
            if self._is_holiday_type_entitled(holiday_type=holiday_type):
                unworked_code = constants.ATT_UH
            else:
                unworked_code = constants.ATT_ABSENT

        # if worked_code and remaining_uncredited_shift:
        #     remaining_uncredited_shift -= shift_ts_span
        # Get the night shift span if there is one
        nt_span = self._nt_span

        # NT hours is shift_span  ∩  nt_span
        nt_hours = None
        if shift_ts_span:
            nt_hours = shift_ts_span.intersection(nt_span)
            if nt_hours and valid_flexi_break_span:
                nt_hours -= valid_flexi_break_span
            if nt_hours and valid_floating_break_span:
                nt_hours -= valid_floating_break_span
        else:
            nt_hours = remaining_uncredited_shift
            if nt_hours and creadited_ot_span:
                nt_hours -= creadited_ot_span
        # net work_hours hours is work_hours - nt hours
        time_shift = None
        if nt_hours:
            # this shift has an overnight setting
            night_worked_code = None
            is_nt_work_valid = False
            if shift_ts_span and self._holidays_work_allowed(shift_ts_span, shift):
                is_nt_work_valid = True
                night_worked_code = self._get_code(
                    self._shift_dates.get(self.current_date).get("rest_day"),
                    holiday_type,
                    True,
                    False,
                    self._shift_allowed_on_holidays(shift)
                )
            else:
                night_worked_code = unworked_code

            time_shift = self._time_shift
            if time_shift == keys.NT_CARRY_OVER:
                if night_worked_code and shift_ts_span and shift_ts_span.intersection(self._day_span1):
                    self.add_entry(shift_id, night_worked_code, shift_ts_span.intersection(self._nt_span).value)
                    remaining_uncredited_shift -= shift_ts_span.intersection(self._nt_span)
                elif unworked_code:
                    self.add_entry(shift_id, unworked_code, remaining_uncredited_shift.value if remaining_uncredited_shift else
                            constants.OT_HOURS_THRESHOLD * constants.SECONDS_PER_HOUR)
                    remaining_uncredited_shift = None

            elif time_shift == keys.NT_COUNTER_PER_HOUR:
                day_span1 = nt_hours.intersection(self._day_span1)
                day_span2 = nt_hours.intersection(self._day_span2)
                is_next_day_restday = False

                if night_worked_code and day_span1 and shift_dates.get(self.current_date).get(keys.HOLIDAY_TYPE):
                    self.add_entry(shift_id, night_worked_code, day_span1.value)
                    if remaining_uncredited_shift:
                        remaining_uncredited_shift = remaining_uncredited_shift - day_span1
                if day_span2:
                    is_next_day_restday = self._is_date_restday(self.params.get('employee_uid'), self.next_date)
                    next_day_code = None
                    if (self._ts_span and is_nt_work_valid) or not self._is_timesheet_required():
                        next_day_code = self._get_code(
                            is_next_day_restday,
                            self._shift_dates.get(self.next_date, {}).get("holiday_type"),
                            True,
                            False,
                            self._shift_allowed_on_holidays(shift)
                        )
                    else:
                        # if no hours rendered then check if day2 is also a holiday
                        # in that condition use ATT_UH else Undertime
                        next_day_code = constants.ATT_UNDERTIME
                        if self._shift_dates.get(self.next_date, {}).get("holiday_type")\
                            and self._is_holiday_type_entitled(
                                holiday_type=self._shift_dates.get(self.next_date, {}).get("holiday_type")
                                ) or creadited_ot_span:
                            next_day_code = constants.ATT_UH
                        if is_rest_day:
                            # on restdays don't count unworked hours
                            next_day_code = None
                    if next_day_code:
                        self.add_entry(shift_id, next_day_code, day_span2.value)
                    if remaining_uncredited_shift:
                        remaining_uncredited_shift = remaining_uncredited_shift - day_span2
                    day2_nt_span = self._nt_span.intersection(self._day_span2)
                    if remaining_uncredited_shift and shift_ts_span and day2_nt_span:
                        # calcuate the attendance of next day after night shift span
                        day2_remaining_uncredited_shift = self._day_span2.intersection(remaining_uncredited_shift)
                        if day2_remaining_uncredited_shift:
                            day2_after_nt = day2_remaining_uncredited_shift - day2_nt_span  # next day span after night shift
                            if day2_after_nt and shift_ts_span.intersection(day2_after_nt):
                                next_day_code = self._get_code(
                                    is_next_day_restday,
                                    self._shift_dates.get(self.next_date, {}).get("holiday_type"),
                                    False,
                                    False,
                                    self._shift_allowed_on_holidays(shift)
                                )
                                self.add_entry(shift_id, next_day_code, shift_ts_span.intersection(day2_after_nt).value)
                                remaining_uncredited_shift -= shift_ts_span.intersection(day2_after_nt)
                    if remaining_uncredited_shift and self._day_span2.intersection(remaining_uncredited_shift):
                        # calcuate the undertime on next day
                        next_day_unworked_code = constants.ATT_UNDERTIME
                        if self._shift_dates.get(self.next_date).get("holiday_type")\
                            and self._is_holiday_type_entitled(holiday_type=holiday_type):
                            next_day_unworked_code = constants.ATT_UH
                        if not is_rest_day:
                            self.add_entry(shift_id, next_day_unworked_code, self._day_span2.intersection(remaining_uncredited_shift).value)
                        remaining_uncredited_shift -= self._day_span2.intersection(remaining_uncredited_shift)
        uncredited_shift = remaining_uncredited_shift.value if remaining_uncredited_shift else 0

        # Deduct unpaid floating/flexi breaks in unworked holiday remaining total hours
        if unworked_code == constants.ATT_UH or special_working_holiday:
            shift_breaks = flexi_breaks if flexi_breaks else floating_breaks
            if shift_breaks:
                for shift_break in shift_breaks:
                    # convert to seconds
                    if not shift_break.get(keys.IS_PAID_BREAK):
                        uncredited_shift -= dateutils.convert_to_mins(
                        shift_break.get(keys.BREAK_HOURS)) * 60
        if worked_code and shift_ts_span and remaining_uncredited_shift:
            if shift_ts_span.intersection(remaining_uncredited_shift):
                self.add_entry(shift_id, worked_code, shift_ts_span.intersection(remaining_uncredited_shift).value)
                remaining_uncredited_shift -= shift_ts_span.intersection(remaining_uncredited_shift)

        if unworked_code and remaining_uncredited_shift:
            self.add_entry(shift_id, unworked_code, remaining_uncredited_shift.value if remaining_uncredited_shift else
                        constants.OT_HOURS_THRESHOLD * constants.SECONDS_PER_HOUR)
        remaining_uncredited_shift = None

        if self._is_timesheet_required() and remaining_uncredited_shift and work_hours is None:
            return self.add_entry(shift_id, constants.ATT_ABSENT, remaining_uncredited_shift.value)
        elif self._is_timesheet_required() and remaining_uncredited_shift:
            shift_span = remaining_uncredited_shift
        elif not self._is_timesheet_required():
            if remaining_uncredited_shift:
                shift_span = remaining_uncredited_shift
                work_hours = remaining_uncredited_shift

    def _clean_swh_code(self, holiday_code):
        """
        remove 'REGULAR' from holiday code if shift has SWH
        """
        new_code = re.sub(r'\d*REGULAR[+]*', '', holiday_code)
        if not new_code:
            return None
        if new_code[-1] == '+':  # to fix RH+
            new_code = new_code[:-1]
        return new_code

def filter_timesheet(timesheet, start_timestamp, end_timestamp):
    """Filter timesheet records based on start and end timestamps

    Arguments:
        timesheet {list} -- Original timesheet
        start_timestamp {int} -- Start timestamp
        end_timestamp {int} -- End timestamp

    Returns:
        list -- Filtered timesheet
    """
    return list(filter(lambda entry: entry['timestamp'] >= start_timestamp \
        and entry['timestamp'] <= end_timestamp, timesheet))


def get_calculator(att_params):
    """This is supposedly a factory function that returns the calculator per
    country with DefaultCalculator as default."""

    return DefaultCalculator(att_params)
