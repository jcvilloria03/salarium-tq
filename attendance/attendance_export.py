import os
import time
import csv
import boto3
from datetime import timedelta, datetime
from itertools import groupby
from operator import itemgetter

import json
from attendance.dao import RedisConnect
import ta_config
from attendance import (models, dao, calls)
from core import exceptions, logger

logger = logger.get_logger()


class AttendanceExportFile(object):
    EMPLOYEE_ID="Employee ID"
    EMPLOYEE_NAME="Employee Name"
    ATTENDANCE_CODES="attendance codes"
    ASSIGNED_SCHEDULE_NAMES="shift schedule names"
    ASSIGNED_SCHEDULE_NAME="Shift Schedule"
    SCHEDULE_REST_DAY_NAME="Rest Day"
    TIMESHEETS="Timesheets"
    TIMESHEETS_DATE="Date"
    TIMESHEETS_CLOCK_IN="Clock-in"
    TIMESHEETS_CLOCK_OUT="Clock-out"
    TIMESHEETS_NAME="Shift Schedule"
    TIMESHEETS_HOURS="Total Worked Hours"
    TIMESHEETS_TAGS="Tags"
    TIMESHEETS_ATTENDANCE_CODES="Atendance Codes"
    RAW_TIMESHEETS="Raw"
    RAW_TIMESHEETS_TIMESTAMP="Timestamp"
    RAW_TIMESHEETS_TAGS="Tags"
    RAW_TIMESHEETS_STATE="State"
    RAW_TIMESHEETS_NAME="Default Schedule"
    RAW_TIMESHEETS_HOURS="0:00:00"
    COLUMN_HEADER = [
        EMPLOYEE_ID,
        EMPLOYEE_NAME,
        TIMESHEETS_DATE,
        TIMESHEETS_NAME,
        TIMESHEETS_HOURS,
        TIMESHEETS_CLOCK_IN,
        TIMESHEETS_CLOCK_OUT,
        TIMESHEETS_TAGS
    ]
    DATE_SCHEDULES="Date Schedules"

    def __init__(self, file_path, data, attendance_codes, append=False):
        self.file_path = file_path
        self.__data = data
        self.append = append
        self.attendance_codes = attendance_codes

    def export(self):
        self.__write(self.__data)

    def __write(self, data):
        if not data:
            return
        mode = 'a+' if self.append else 'w+'
        with open(self.file_path, mode) as f:

            employees = self.__build_data(data)

            # Clear the variable its now redundant
            data = None

            self.__build_timesheet(employees)

            # Sort By Employee UID
            employees = sorted(employees.items(),
                key=lambda employee: (employee[1][self.EMPLOYEE_ID]))

            attendance_codes_keys = self.attendance_codes

            w = csv.DictWriter(
                f, fieldnames=self.__get_headers(attendance_codes_keys),
                delimiter=",")
            if not self.append:
                w.writeheader()

            for employee_tuple in employees:
                employee = employee_tuple[1]

                # Sort By date
                attendance_codes_by_date = sorted(employee[self.ATTENDANCE_CODES].items(),
                    key=lambda attendance_code: (attendance_code[0]))

                for date_tuple in attendance_codes_by_date:

                    date = date_tuple[0]
                    attendance_codes = date_tuple[1]

                    timesheets = employee[self.TIMESHEETS].get(date, [])

                    if timesheets:
                        timesheets = self.__finalize_timesheet(timesheets)

                        for timesheet in timesheets:
                            tags = ' - '.join(timesheet[self.TIMESHEETS_TAGS])

                            row = self.__build_row(attendance_codes_keys,
                                employee[self.EMPLOYEE_ID],
                                employee[self.EMPLOYEE_NAME],
                                date,
                                timesheet[self.TIMESHEETS_CLOCK_IN],
                                timesheet[self.TIMESHEETS_CLOCK_OUT],
                                tags,
                                timesheet[self.TIMESHEETS_ATTENDANCE_CODES],
                                timesheet[self.TIMESHEETS_NAME],
                                timesheet[self.TIMESHEETS_HOURS])

                            w.writerow(row)
                    else:
                        attendance_codes_key = None
                        for key in attendance_codes.keys():
                            attendance_codes_key = key
                        if attendance_codes_key == 'RD':
                            shift_schedule_name = self.SCHEDULE_REST_DAY_NAME
                        else:
                            shift_schedule_name = employee[self.ASSIGNED_SCHEDULE_NAME]


                        row = self.__build_row(attendance_codes_keys,
                            employee[self.EMPLOYEE_ID],
                            employee[self.EMPLOYEE_NAME],
                            date,
                            None,
                            None,
                            None,
                            attendance_codes,
                            shift_schedule_name,
                            '0:00')

                        w.writerow(row)

    def __build_data(self, data):
        employees = {}

        for row in data:

            employee_id = row['employee_uid']

            if not employee_id in employees:
                employee = {
                    self.EMPLOYEE_ID:row['company_employee_id'],
                    self.EMPLOYEE_NAME:row['employee_full_name'],
                    self.DATE_SCHEDULES: [],
                    self.TIMESHEETS:{},
                    self.ATTENDANCE_CODES:{},
                    self.ASSIGNED_SCHEDULE_NAME:'Default Schedule'
                }
            else:
                employee = employees[employee_id]

            attendance_codes_by_date = employee[self.ATTENDANCE_CODES]
            date_schedules = employee[self.DATE_SCHEDULES]
            import datetime

            date = datetime.date.strftime(row['date'], "%m/%d/%Y")
            scheduled_shifts = json.loads(row['scheduled_shifts'])
            attendance_records = json.loads(row['attendance_records'])

            date_schedules.append({
                'date': date,
                'scheduled_shifts': scheduled_shifts,
                'attendance_records': attendance_records
            })

            if not date in attendance_codes_by_date:
                attendance_codes_by_date[date] = {}

            attendance_codes = attendance_codes_by_date[date]

            for shift in attendance_records:
                for code, minutes in attendance_records[shift].items():
                    if code in attendance_codes:
                        attendance_codes[code] += minutes
                    else:
                        attendance_codes[code] = minutes

            if date_schedules:
                employee[self.DATE_SCHEDULES] = date_schedules
                employee[self.ATTENDANCE_CODES] = attendance_codes_by_date

            employees[employee_id] = employee

        return employees

    def __get_headers(self, attendance_codes_keys = []):
        headers = self.COLUMN_HEADER + attendance_codes_keys

        return headers

    def __format_shift_name(self, name = None):
        if name == 'default':
            return self.RAW_TIMESHEETS_NAME
        elif name == 'RD':
            return self.SCHEDULE_REST_DAY_NAME
        else:
            return name

    def __format_time(self, minutes):
        time = timedelta(minutes=minutes)
        formatted_time = ':'.join(str(time).split(':')[:2])
        return formatted_time

    def __build_timesheet(self, employees):
        for employee_key in employees:
            employee = employees[employee_key]
            timesheets = {}

            for shifts in employee.get(self.DATE_SCHEDULES, []):
                scheduled_shifts = shifts.get('scheduled_shifts', [])
                assigned_shifts = scheduled_shifts.get('assigned_shifts', None)
                default_shift = scheduled_shifts.get('default_shift', [])
                attendance_records = shifts.get('attendance_records', {})

                if assigned_shifts:
                    for shift in assigned_shifts:
                        attendance_shift_records = {}
                        shift_name = shift.get('name', self.RAW_TIMESHEETS_NAME)
                        employee[self.ASSIGNED_SCHEDULE_NAME] = self.__format_shift_name(shift_name)
                        working_hours = self.__format_time(shift['computed_attendance'])

                        shift_id = str(shift['shift_id'])
                        attendance_shift_id_records = attendance_records.get(shift_id, None)

                        if attendance_shift_id_records:
                            for code, minutes in attendance_shift_id_records.items():
                                attendance_shift_records[code] = minutes

                        timesheet = shift.get('raw_timesheet', [])

                        if len(shift.get('timesheet', [])) > len(timesheet):
                            timesheet = shift.get('timesheet', [])

                        if len(timesheet) > 0:
                            (clock_in, clock_out, tags) = self.__get_time_clocks(timesheet)
                            self.__append_timesheet(timesheets, shifts['date'], clock_in,
                                clock_out, tags, employee[self.ASSIGNED_SCHEDULE_NAME],
                                working_hours, attendance_shift_records)
                        else:
                            self.__append_timesheet(timesheets, shifts['date'], None,  None, [],
                                employee[self.ASSIGNED_SCHEDULE_NAME], working_hours,
                                attendance_shift_records)

            del employee[self.DATE_SCHEDULES]
            employee[self.TIMESHEETS] = timesheets

    def __get_time_clocks(self, timesheets):
        clock_in = None
        clock_out = None
        tags = []

        for timesheet in timesheets:
            state = timesheet.get("state", False)
            timesheet_tags = timesheet.get("tags", [])

            timestamp = timesheet.get("timestamp", None)
            if timestamp:
                if(clock_in is None and state == True):
                    # clock_in = utils.timestamp_to_date(timestamp, "%H:%M:%S")
                    clock_in = datetime.fromtimestamp(timestamp,).strftime("%H:%M:%S")

                if(state == False):
                    # clock_out = utils.timestamp_to_date(timestamp, "%H:%M:%S")
                    clock_out = datetime.fromtimestamp(timestamp,).strftime("%H:%M:%S")

                tags = list(set(tags + timesheet_tags))

        return (clock_in, clock_out, tags)

    def __finalize_timesheet(self, timesheets):
        sorted_timesheets = sorted(timesheets, key = itemgetter(self.TIMESHEETS_NAME))

        finalized_timesheet = []
        for key, value in groupby(sorted_timesheets, key = itemgetter(self.TIMESHEETS_NAME)):
            itemsheet = {}

            counter = 0
            for k in value:
                if not itemsheet:
                    itemsheet = k

                itemsheet[self.TIMESHEETS_CLOCK_OUT] = k[self.TIMESHEETS_CLOCK_OUT]
                if k[self.TIMESHEETS_CLOCK_IN] and counter == 0:
                    itemsheet[self.TIMESHEETS_CLOCK_IN] = k[self.TIMESHEETS_CLOCK_IN]
                    counter += 1

            if not any(timesheet[self.TIMESHEETS_NAME] == key for timesheet in finalized_timesheet):
                finalized_timesheet.append(itemsheet)

        return finalized_timesheet

    def __append_timesheet(self, timesheets, timesheet_date, clockin, clockout, tags,
        name = 'Default Schedule', work_hours = '0:00', attendance_codes = None):

        if not timesheet_date in timesheets:
            timesheets[timesheet_date] = []

        timesheets[timesheet_date].append({
            self.TIMESHEETS_CLOCK_IN:clockin,
            self.TIMESHEETS_CLOCK_OUT:clockout,
            self.TIMESHEETS_TAGS:tags,
            self.TIMESHEETS_NAME:name,
            self.TIMESHEETS_HOURS:work_hours,
            self.TIMESHEETS_ATTENDANCE_CODES:attendance_codes,
        })

    def __build_row(self, attendance_codes_keys, employee_id, employee_name, timesheet_date, clockin, clockout, tags,
        attendance_codes = {}, shifts_name = '', work_hours = ''):

        row = {
            self.EMPLOYEE_ID: employee_id,
            self.EMPLOYEE_NAME: employee_name,
            self.TIMESHEETS_DATE: timesheet_date,
            self.TIMESHEETS_NAME: shifts_name,
            self.TIMESHEETS_HOURS: work_hours,
            self.TIMESHEETS_CLOCK_IN: clockin,
            self.TIMESHEETS_CLOCK_OUT: clockout,
            self.TIMESHEETS_TAGS: tags
        }

        for code in attendance_codes_keys:

            minutes = attendance_codes.get(code, None)

            if minutes is None:
                minutes = ""
            else:
                minutes = '{:02d}:{:02d}'.format(*divmod(int(minutes), 60))

            row[code] = minutes

        return row

    def __get_attendance_codes(self, employees):
        attendance_codes_keys = []

        for employee_tuple in employees:
            employee = employee_tuple[1]

            for codes_by_date in employee[self.ATTENDANCE_CODES].items():
                attendance_codes_keys += list(set(codes_by_date[1].keys()) -
                    set(attendance_codes_keys))

        return sorted(attendance_codes_keys)

    @staticmethod
    def get_attendance_codes(params):
        attendance_codes_keys = set()

        for records in AttendanceExport.get_attendance_records(params):
            for row in records:
                attendance_records = json.loads(row['attendance_records'])
                for shift in attendance_records:
                    for code in attendance_records[shift].keys():
                        attendance_codes_keys.add(code)
        return sorted(list(attendance_codes_keys))


class AttendanceExport:
    PROGRESS = "progress"
    DONE = "done"
    ERROR = "error"

    @staticmethod
    def set_job_status(job_id, status, error='', result=''):
        body = {
            "status": status,
            "error": error,
            "result": result  # set s3 link of the file
        }
        RedisConnect.conn.set(job_id, json.dumps(body), ex=300)

    @staticmethod
    def generate_tmp_file_name(start_date, end_date, company_name):
        tmp_name = company_name + '-' + start_date + '_' + end_date + '.csv'
        return tmp_name

    @staticmethod
    def upload_file(file_path):
        """
        Upload file to s3 and return the file link
        """
        s3 = boto3.client('s3')
        destination_file_path = os.path.basename(file_path)
        s3_bucket = ta_config.UPLOADS_BUCKET
        s3.upload_file(
            file_path,
            s3_bucket,
            destination_file_path
        )
        return s3.generate_presigned_url(
            ClientMethod='get_object',
            Params={
                'Bucket': s3_bucket,
                'Key': destination_file_path
            }
        )

    @staticmethod
    def get_attendance_records(params):
        with dao.create_session() as session:
            fetch, records_found = True, False
            start_range, end_range = 0, 200  # 200 records per write
            fetch_start_time = time.time()
            job_id = params.get("job_id")
            filters = params.get('filters')
            filtered_employee_ids_by_status = calls.get_employee_internal_ids_by_status(
                params['company_id'],
                filters.get('employee_status')
            )
            if params.get('ids'):
                filtered_employee_ids_by_status = list(
                    set(filtered_employee_ids_by_status).intersection(set(params.get('ids')))
                    )
            while fetch:
                # After every 60 sec update the redis entry to prevent ttl expiry
                if time.time() - fetch_start_time >= 60:
                    fetch_start_time = time.time()
                    AttendanceExport.set_job_status(job_id, AttendanceExport.PROGRESS)
                query = session.query(models.AttendanceRecords).filter(
                    models.AttendanceRecords.company_id == params['company_id'],
                    models.AttendanceRecords.date >= params['start_date'],
                    models.AttendanceRecords.date <= params['end_date'],
                )

                query = query.filter(models.AttendanceRecords.employee_uid.in_(filtered_employee_ids_by_status))
                if filters:
                    if filters.get('department'):
                        query = query.filter(models.AttendanceRecords.department_id.in_(filters.get('department')))
                    if filters.get('location'):
                        query = query.filter(models.AttendanceRecords.location_id.in_(filters.get('location')))

                query = query.order_by(
                    models.AttendanceRecords.company_employee_id
                ).slice(start_range, start_range + end_range)

                start_range += end_range
                records = query.all()
                if not records:
                    break
                records_found = True
                yield records
            if not records_found:
                raise exceptions.NoAttendanceRecordsError(
                    "No attendance records matching supplied parameters found."
                )

    @staticmethod
    def delete(file_path):
        try:
            os.unlink(file_path)
        except Exception as e:
            logger.info("Error while deleting file: %s", file_path)
            logger.exception(e)
