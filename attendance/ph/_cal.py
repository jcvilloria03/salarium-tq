# """
# Attendance Calculator for PH
# """

# from attendance import DefaultAttendanceCalculator
# from attendance.dateutils import convert_to_mins, get_span_range
# from attendance.spans import create_span


# class PHAttendanceCalculator(DefaultAttendanceCalculator):
#     """Attendance calculator for PH setting"""

#     def calculate(self, empid, attdate, *args, **kwargs):
#         shift = self.get_shift(empid, attdate)
#         data = shift.get("data")
#         # determine schedule info
#         schedule = data.get("schedule")
#         # check if fixed
#         stype = schedule.get("type")
#         methods = schedule.get("allowed_time_methods")
#         # TODO: determine night shift
#         # with_night_shift = False

#         start_time = schedule.get("start_time")
#         end_time = schedule.get("end_time")

#         S = create_span(get_span_range(start_time, end_time))
#         breaks = schedule.get("breaks")

#         # remove the fixed
#         attendance = dict()

#         if stype == "fixed" and not methods:  # TODO: externalize this string
#            # Get breaks
#             total_deductibles = 0
#             for br in breaks or []:
#                 if br.get("type") == 'fixed':
#                     fbreak_span = create_span(
#                         get_span_range(br.get("start"), br.get("end")))
#                     S = S - fbreak_span
#                 else:
#                     # TODO: how to do Floating and Flexi breaks
#                     # for now get the mins and deduct
#                     mins = convert_to_mins(br.get("break_hours"))
#                     total_deductibles = total_deductibles + mins

#             attendance["Regular"] = S.value - total_deductibles

#         return attendance

#     def get_shift(self, empid, attdate, *args, **kwargs):
#         return dict()
