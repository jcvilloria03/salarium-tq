import json
import time
from datetime import datetime
from functools import wraps

from celery import Task, chain, group
from celery import exceptions
from core import warnings
from core import exceptions, logger
from core.app import app

from attendance import (calls, keys, spans, dao)
import ta_config

logger = logger.get_logger()

def queue_route(route):
    return "%s_%s" % (ta_config.ENV, route)

def chunk_list(l, n):
    """Divide a given array into chunks

    Args:
        l: The list to divide.
        n: The maximum number of items per chunk.

    Returns:
        (list)
    """
    n = max(1, n)
    return [l[i:i + n] for i in range(0, len(l), n)]

def chunks(l, n, c, *kwargs):
    """Divide a given array into chunks

    Args:
        l: The list to divide.
        n: The maximum number of items per chunk.
        c: The task callback to execute on every job item.
    Returns:
        A group signature object. Which can be executed with
        apply_sync()
    """
    jobs = []
    chunks = chunk_list(l, n)
    for item in chunks:
        jobs.append(c.s(item, *kwargs))
    return group(jobs)

def _log_error(pid, event, stage, message, details=None, target=None, target_date=None):
    if message.startswith("{") and message.endswith("}"):
        message = json.loads(message)

    logger.error(json.dumps({
        keys.JOB_ID: pid,
        "event": event,
        "stage": stage,
        "message": message,
        "details": details,
        "target": target,
        "target_date": target_date
    }))

def _log_warning(pid, event, stage, message, details=None, target=None, target_date=None):
    if message.startswith("{") and message.endswith("}"):
        message = json.loads(message)
    logger.warning(json.dumps({
        keys.JOB_ID: pid,
        "event": event,
        "stage": stage,
        "message": message,
        "details": details,
        "target": target,
        "target_date": target_date
    }))

def suppress_traceback(func):
    """Suppress traceback reporting"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        att_params = args[0]
        target = ""
        target_date = ""

        # Since this decorator accepts a mixed object type either
        # list or dict, then we should consider list arguments to have
        # the job_id stored within its elements.
        if type(att_params) is list and len(att_params) > 0:
            job_id = att_params[0].get(keys.JOB_ID, None)
            # Since this is a list, it doesn't make sense to pick a random
            # employee id and date.
        else:
            # Pretty much straight forward for dicts.
            job_id = att_params.get(keys.JOB_ID, None)
            target = att_params.get(keys.EMP_UID, "")
            target_date = att_params.get(keys.DATE, "")

        try:
            return func(*args, **kwargs)
        except exceptions.PeerError as exc:
            _log_warning(job_id, "Retrying", func.__name__,
                 str(exc), str(exc.__cause__),
                 target, target_date)
            raise
        except warnings.RecordLockWarning as exc:
            _log_warnings("RecordIsLockedWarning", func.__name__, job_id, str(exc),
                          str(exc.__cause__), target, target_date)
        except warnings.DeadlockWarning as exc:
            _log_warnings("DeadlockWarning", func.__name__, job_id, str(exc),
                          str(exc.__cause__), target, target_date)
            raise
        except spans.SpanConflictError as exc:
            _log_warning(job_id, "SpanConflicError", func.__name__,
                 str(exc), exc.__cause__,
                 target, target_date)
        except exceptions.TaskError as exc:
            _log_and_report_error("TaskChainError", func.__name__, job_id, str(exc),
                                  str(exc.__cause__), target, target_date)
            raise  # Ignore()
        except Exception as exc:
            _log_and_report_error("UnexpectedError", func.__name__, job_id, str(exc),
                                  str(exc.__cause__), target, target_date)
            raise  # Ignore()
    return wrapper

def _log_warnings(err, name, job_id, exc, cause, target, target_date):
    _log_warning(job_id, err, name,
                 exc, cause,
                 target, target_date)
    if not str(job_id).startswith("INT:"):
        att_params = {
            keys.JOB_ID: job_id,
            keys.MESSAGE: exc,
            keys.EMP_UID: str(target),
            keys.DATE: target_date,
        }
        add_job_task_warning.apply_async(
            args=(att_params,))

def _log_and_report_error(err, name, job_id, exc, cause, target, target_date):
    _log_error(job_id,
               'ProcessingError', name,
               exc, cause,
               target,
               target_date)
    if not str(job_id).startswith("INT:"):
        att_params = {keys.JOB_ID: job_id,
                      keys.MESSAGE: exc,
                      keys.EMP_UID: target,
                      keys.DATE: target_date
                      }
        add_job_task_error.apply_async(
            args=(att_params,))


class AttendanceTask(Task):
    """Superclass for all attendance related class"""

    def on_failure(self, exc, task_id, args, kwargs, einfo=None):
        """Override failure reporting func"""
        att_params = args[0]
        # logger.debug(args)
        job_id = None
        target = None
        target_date = None
        if not isinstance(att_params, dict):
            _log_error(task_id,
                       "TaskError", self.name,
                       str(exc), str(exc.__cause__),
                       None,
                       None)
        else:
            job_id = att_params.get(keys.JOB_ID, None)
            target = att_params.get(keys.EMP_UID)
            target_date = att_params.get(keys.DATE)
            _log_error(job_id,
                       "TaskChainError", self.name,
                       str(exc), str(exc.__cause__),
                       target,
                       target_date)


class AttendanceErrorTask(Task):
    """Superclass of all Attendance Error reporting tasks"""

    def on_failure(self, exc, task_id, args, kwargs, einfo=None):
        att_params = args[0]
        job_id = att_params.get(keys.JOB_ID, None)
        target = att_params.get(keys.EMP_UID)
        target_date = att_params.get(keys.DATE)
        _log_error(job_id,
                   "TaskChainRetryError", self.name,
                   str(exc), str(exc.__cause__),
                   target,
                   target_date)

def task_params(func_name, group='ta', superclass=AttendanceTask, **kwargs):
    """Return task standard params"""
    task_name = '{group}.tasks.{fname}'.format(group=group, fname=func_name)
    params = dict(
        name=task_name,
        serializer="pickle",
        ignore_result=True,
        base=superclass,
        autoretry_for=(exceptions.PeerError,),
        default_retry_delay=5,
        exponential_backoff=2,
        task_compression="ztsd",
        retry_kwargs={"max_retries": 3},
        retry_jitter=True,
    )
    if kwargs.get("queue", None):
        kwargs["queue"] = queue_route(kwargs["queue"])
    params.update(kwargs)
    return params

@app.task(**task_params("update_tasks_count"))
def update_tasks_count(job_id, count):
    """This method updates the job count or tasks
    that are completed.

    Args:
        job_id (string) -- The job id
    """
    key_format = "attendance_upload:{job_id}".format(job_id=job_id)

    dao.RedisConnect.conn.hincrby(key_format, 'count', count)

    dao.RedisConnect.conn.hset(key_format, 'updated_at',
        int(time.mktime(datetime.now().timetuple())))

def update_done_count(job_id):
    """This method updates the job count or tasks
    that are completed.

    Args:
        job_id (string) -- The job id
    """
    key_format = "attendance_upload:{job_id}".format(job_id=job_id)

    dao.RedisConnect.conn.hincrby(key_format, 'done', 1)

    dao.RedisConnect.conn.hset(key_format, 'updated_at',
        int(time.mktime(datetime.now().timetuple())))

@app.task(**task_params("add_job_task_error"))
def add_job_task_error(att_params):
    """Submit task errors"""
    job_id = att_params.get(keys.JOB_ID)
    message = att_params.get(keys.MESSAGE)
    target = att_params.get(keys.EMP_UID)
    target_date = att_params.get(keys.DATE)
    calls.add_job_task_error(job_id,
                             message=message,
                             target=target,
                             target_date=target_date)

@app.task(**task_params("add_job_task_warning"))
def add_job_task_warning(att_params):
    """Submit warnings"""
    job_id = att_params.get(keys.JOB_ID)
    message = att_params.get(keys.MESSAGE)
    target = att_params.get(keys.EMP_UID)
    target_date = att_params.get(keys.DATE)
    calls.add_job_task_warning(job_id, message, target, target_date)
