"""This factory object performs the business logic
for calculating holidays.
"""
from attendance import spans, keys, constants


class HolidaysFactory(object):

    def __init__(self,
                 shift_type=keys.SH_FIXED,
                 holiday_type=None,
                 holiday_entitlements=None,
                 holiday_premium_pay=False):
        """Constructor

        Args:
            shift_id                    (string) -- The shift id.
            shift_type                  (string) -- The shift type. Possible values are:
                FIXED
                FLEXI
                DEFAULT_SHIFT
            holiday_entitlements        ((bool)) -- The holiday entitlement of an employee
                if eligble for unworked regular holiday or unworked special holiday.
                The format of this parameter is.
                (<unworked holiday pay>, <unworked special holiday>).

            holiday_premium_pay         (bool) -- Flags if employee is entitled for a
                premium holiday pay
        """
        self._shift_type = shift_type
        self._holiday_type = holiday_type

        # Holiday entitlements of the employee.
        self._holiday_entitlements = holiday_entitlements
        self._is_regular_entitled, self.is_special_entitled = holiday_entitlements

        # timesheet entitlement
        self._is_premium_entitled = holiday_premium_pay

    def _get_rendered_time(self):
        """This method calculates the appropriate attendance time
        in a holiday of a default shift. Considering that there's
        a rendered hours.

        Args:
            shift_type (string) -- The shift type. Possible values are:
                fixed
                flexi
                default_shift
        Returns:
            (int) -- The computed time in seconds
        """
        # Scenarios: 1
        # if there are rendered hours use the shift span value with
        # deducted breaks
        if self._shift_type is keys.SH_FIXED or self._shift_type is keys.DEFAULT_SHIFT:
            if not self._rendered_hours_span:
                return 0
            return self._rendered_hours_span.value
        elif self._shift_type is keys.SH_FLEXI:
            # If there are no working hours, return the expected hours in a day
            if not self._rendered_hours_span:
                return self._expected_work_time

            # If the rendered hours is greater than the expected work time,
            # use the the expected work time
            if self._rendered_hours_span.value > self._expected_work_time:
                return self._expected_work_time

            # By default use the rendered hours
            return self._rendered_hours_span.value

    def _get_default_time(self):
        """This method calculates the appropriate attendance time
        in a holiday of a default shift. Considering that there's
        a rendered hours.

        Args:
            shift_span (SimpleSpan) -- The shift span of a default hours
            expected_work_time (int) -- The expected work time in seconds
            shift_type (string) -- The shift type. Possible values are:
                fixed
                flexi
                default_shift
        Returns:
            (int) -- The computed time in seconds
        """
        # Scenarios: 2,
        # If there's no rendered hours use the default definition of
        # schedule like expected shift_span, expected work time etc..
        if self._shift_type is keys.SH_FIXED:
            return self._shift_span.value
        elif self._shift_type is keys.DEFAULT_SHIFT:
            return self._expected_work_time
        elif self._shift_type is keys.SH_FLEXI:
            # The default time is the required hours per day
            # for a flexi shift
            return self._expected_work_time

    def _parse_result(self, scenarios):
        """This method parses the computed data from a
        suitable scenario then, picks the appropriate scenario
        where the criteria passes.

        Args:
            scenarios (tuple) -- A tuple data of:
                Passed scenario
                Attedance code
                Attendance time (in seconds)
        Return:
            (tuple) -- Tuple of parsed scenario (Attendance code and Attendance time)
        """
        # Print this scenario id for debugging purposes only
        scenario_id = {_id for criteria, _, _, _id in scenarios if criteria}
        attendance = {(code, time) for criteria, code, time, _ in scenarios if criteria}
        return attendance.pop() if attendance else ()

    def get_timesheet_required(self, rendered_hours_span,
                              shift_span, total_break_time=0,
                              expected_work_time=0):
        """This method determines the attendance on a holiday for
        timesheet required entitlement.

        Args:
            rendered_hour_span (SimpleSpan) -- The span of the rendered hours.
            shift_span (SimpleSpan) -- The defined shift span assigned to the
                employee.
            total_break (int) -- The total break in seconds.
            expected_work_time (int) -- The expected work duration in seconds.

        Returns:
            (tuple) -- The parsed result for the appropriate holiday attendance.
        """
        scenarios = []

        # Holiday type is required. If this is none, this means
        # that the current date is not a holiday
        if self._holiday_type is None:
            return

        self._shift_span = shift_span
        self._total_break = total_break_time
        self._expected_work_time = expected_work_time
        self._rendered_hours_span = rendered_hours_span

        # TIMESHEET IS REQUIRED AND MUST HAVE ALL HOLIDAY ENTITLEMENTS
        if self.is_holiday_type_entitled():
            scenarios = [
                # SCENARIO 1:
                # 1. timesheet required
                # 2. HAS all holiday entitlements or NONE
                # 3. IS premium entitled
                # 4. HAS rendered hours
                (
                    all([self._is_premium_entitled, self._rendered_hours_span]),
                    self._holiday_type,
                    self._get_rendered_time(),
                    "SC_1"
                ),
                # SCENARIO 2:
                # 1. timesheet required
                # 2. HAS all holiday entitlements
                # 3. IS premium entitled
                # 4. NO rendered hours
                (
                    all([self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_UH,
                    self._get_default_time(),
                    "SC_2"
                ),
                # SCENARIO 3:
                # 1. timesheet required
                # 2. HAS all holiday entitlements OR NONE
                # 3. IS NOT premium entitled
                # 4. HAS rendered hours
                (
                    all([not self._is_premium_entitled, self._rendered_hours_span]),
                    constants.ATT_REGULAR,
                    self._get_rendered_time(),
                    "SC_3"
                ),
                # SCENARIO 4:
                # 1. timesheet required
                # 2. HAS all holiday entitlements OR NONE
                # 3. IS NOT premium entitled
                # 4. NO rendered hours
                (
                    all([not self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_UH,
                    self._get_default_time(),
                    "SC_4"
                ),
            ]
        else:
            # TIMESHEET IS REQUIRED AND NO HOLIDAY ENTITLEMENTS
            scenarios = [
                # SCENARIO 5:
                # 1. timesheet required
                # 2. NO holiday entitlements
                # 3. IS premium entitled
                # 4. HAS rendered hours
                (
                    all([self._is_premium_entitled, self._rendered_hours_span]),
                    self._holiday_type,
                    self._get_rendered_time(),
                    "SC_5"
                ),
                # SCENARIO 6:
                # 1. timesheet required
                # 2. NO holiday entitlements
                # 3. is premium entitled
                # 4. NO rendered hours
                (
                    all([self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_ABSENT,
                    self._get_default_time(),
                    "SC_6"
                ),
                # SCENARIO 7:
                # 1. timesheet required
                # 2. NO holiday entitlements
                # 3. is premium entitled
                # 4. NO rendered hours
                (
                    all([not self._is_premium_entitled, self._rendered_hours_span]),
                    constants.ATT_REGULAR,
                    self._get_rendered_time(),
                    "SC_7"
                ),
                # SCENARIO 8:
                # 1. timesheet required
                # 2. NO holiday entitlements
                # 3. is premium entitled
                # 4. NO rendered hours
                (
                    all([not self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_ABSENT,
                    self._get_default_time(),
                    "SC_8"
                ),
            ]
        return self._parse_result(scenarios)

    def get_timesheet_not_required(self, rendered_hours_span,
                                   shift_span, total_break_time=0,
                                   expected_work_time=0):
        """This method preforms the calculation of holidays for
        entitlements if timesheet not required.

        Args:
            rendered_hour_span (SimpleSpan) -- The span of the rendered hours.
            shift_span (SimpleSpan) -- The defined shift span assigned to the
                employee.
            total_break (int) -- The total break in seconds.
            expected_work_time (int) -- The expected work duration in seconds.

        Returns:
            (tuple) -- The parsed result for the appropriate holiday attendance.
        """
        scenarios = []

        # Holiday type is required. If this is none, this means
        # that the current date is not a holiday
        if self._holiday_type is None:
            return

        self._rendered_hours_span = rendered_hours_span
        self._shift_span = shift_span
        self._total_break = total_break_time
        self._expected_work_time = expected_work_time

        # TIMESHEET IS NOT REQUIRED AND MUST HAVE ALL HOLIDAY ENTITLEMENTS
        if self.is_holiday_type_entitled():
            scenarios = [
                # SCENARIO 9:
                # 1. NOT timesheet required
                # 2. HAS holiday entitlements
                # 3. IS premium entitled
                # 4. HAS rendered hours
                (
                    all([self._is_premium_entitled, self._rendered_hours_span]),
                    self._holiday_type,
                    self._get_rendered_time(),
                    "SC_9"
                ),
                # SCENARIO 10:
                # 1. NOT timesheet required
                # 2. HAS holiday entitlements
                # 3. IS premium entitled
                # 4. NO rendered hours
                (
                    all([self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_UH,
                    self._get_default_time(),
                    "SC_10"
                ),
                # SCENARIO 11:
                # 1. NOT timesheet required
                # 2. HAS holiday entitlements
                # 3. NOT premium entitled
                # 4. HAS rendered hours
                (
                    all([not self._is_premium_entitled, self._rendered_hours_span]),
                    constants.ATT_REGULAR,
                    self._get_rendered_time(),
                    "SC_11"
                ),
                # SCENARIO 12:
                # 1. NOT timesheet required
                # 2. HAS holiday entitlements
                # 3. NOT premium entitled
                # 4. HAS rendered hours
                (
                    all([not self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_UH,
                    self._get_default_time(),
                    "SC_12"
                ),
            ]
        else:
            scenarios = [
                # Not entitled to holiday type
                # SCENARIO 13:
                # 1. NOT timesheet required
                # 2. NOT holiday type entitled
                # 3. NOT premium entitled
                # 4. NOT rendered hours
                (
                    all([not self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_REGULAR,
                    self._get_rendered_time(),
                    "SC_13"
                ),
                # Not entitled to holiday type
                # SCENARIO 14:
                # 1. NOT timesheet required
                # 2. NOT holiday type entitled
                # 3. NOT premium entitled
                # 4. NOT rendered hours
                (
                    all([not self._is_premium_entitled, self._rendered_hours_span]),
                    constants.ATT_REGULAR,
                    self._get_rendered_time(),
                    "SC_14"
                ),
                # Not entitled to holiday type
                # SCENARIO 14:
                # 1. NOT timesheet required
                # 2. NOT holiday type entitled
                # 3. NOT premium entitled
                # 4. NOT rendered hours
                (
                    all([self._is_premium_entitled, not self._rendered_hours_span]),
                    constants.ATT_REGULAR,
                    self._get_rendered_time(),
                    "SC_15"
                ),
                # Not entitled to holiday type
                # SCENARIO 15:
                # 1. NOT timesheet required
                # 2. NOT holiday type entitled
                # 3. NOT premium entitled
                # 4. NOT rendered hours
                (
                    all([self._is_premium_entitled, self._rendered_hours_span]),
                    self._holiday_type,
                    self._get_rendered_time(),
                    "SC_16"
                )
            ]

        return self._parse_result(scenarios)

    def is_holiday_type_entitled(self):
        """ Check if employee is entitled
            to current holiday type
        """

        rh_list = [constants.ATT_RH, constants.ATT_2RH]
        entitled = False
        if self._holiday_type in rh_list:
            entitled = self._holiday_entitlements[0]
        elif self._holiday_type == constants.ATT_SH:
            entitled = self._holiday_entitlements[1]
        elif self._holiday_type == constants.ATT_RH_SH:
            entitled = all(self._holiday_entitlements)

        return entitled
