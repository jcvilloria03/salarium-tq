from core import exceptions
from datetime import datetime
from attendance import keys

def parse_date(date, column_name, row_number):
    """Parse date from CSV to datetime.date object

    Arguments:
        date {string} -- Date string
        column_name {string} -- Column name for error logging
        row_number {int} -- Row number for error logging

    Returns:
        [datetime.date] -- datetime.date parsed from input
    """
    if not date:
        return None

    for date_format in keys.VALID_CSV_DATE_FORMATS:
        try:
            return datetime.strptime(date, date_format).date()
        except ValueError:
            pass
    raise exceptions.RowError('%s must be in (M/D/YYYY, MM/DD/YYYY OR YYYY-MM-DD) format' % (column_name,), row_number)

def parse_time(time, column_name, row_number):
    """Parse time from CSV to datetime.time object

    Arguments:
        time {string} -- Time string
        column_name {string} -- Column name for error logging
        row_number {int} -- Row number for error logging

    Returns:
        [datetime.time] -- datetime.date parsed from input
    """
    if not time:
        return None

    for time_format in keys.VALID_CSV_TIME_FORMATS:
        try:
            return datetime.strptime(time, time_format).time()
        except ValueError:
            pass
    raise exceptions.RowError('%s must be in (HH:MM, H:M, H:M AM/PM and HH:MM AM/PM) format' % (column_name,), row_number)

def parse_hours(hours, column_name, row_number):
    """Parse numeric hours from CSV to integer

    Arguments:
        hours {string} -- Hours string
        column_name {string} -- Column name for error logging
        row_number {int} -- Row number for error logging

    Returns:
        [datetime.date] -- datetime.date parsed from input
    """
    if not hours:
        return None

    if not hours.isnumeric():
        raise exceptions.RowError('%s must be a number' % (column_name,), row_number)

    parsed_hours = int(hours)

    if parsed_hours > 24:
        raise exceptions.RowError('%s must not exceed 24' % (column_name,), row_number)

    return parsed_hours


def get_column_headers_and_formatters(header_mapping):
    """Get column header names and formatters with field names as key

    Arguments:
        header_mapping {dict} -- Header mapping definition
    """
    return {
        definition[0]: (header_name, definition[1]) for header_name, definition in header_mapping.items()
    }
