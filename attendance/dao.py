import redis
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import QueuePool
from contextlib import contextmanager

import ta_config

class MySQLConnect(object):
    conn = None
    session = None

class RedisConnect(object):
    conn = None
    session = None

@contextmanager
def create_session():
    session = MySQLConnect.session()

    try:
        yield session
    except:
        session.rollback()
        raise
    finally:
        session.close()

def mysql_connect():
    """Create a connection to mysql
    """
    url = "{connection}://{user}:{password}@{host}:{port}/{dbname}".format(
        connection=ta_config.DB_CONNECTION,
        user=ta_config.DB_USERNAME,
        password=ta_config.DB_PASSWORD,
        host=ta_config.DB_HOST,
        port=ta_config.DB_PORT,
        dbname=ta_config.DB_NAME)
    engine = create_engine(url,
        echo=False,
        pool_size=ta_config.MYSQL_POOL_SIZE,
        max_overflow=ta_config.MYSQL_MAX_POOL_SIZE,
        pool_pre_ping=True,
        pool_recycle=ta_config.MYSQL_POOL_RECYCLE_TIME)

    MySQLConnect.conn = engine
    MySQLConnect.session = sessionmaker(bind=engine)

    return engine

def mysql_recycle(engine):
    """Dispose any unused connections to prevent
    Zombie or stale mysql connections
    """
    engine.dispose()

def redis_connect():
    """Create a pool connection to Redis
    """
    RedisConnect.conn = redis.Redis(
        host=ta_config.REDIS_HOST, port=ta_config.REDIS_PORT,
        db=ta_config.REDIS_DATABASE)
