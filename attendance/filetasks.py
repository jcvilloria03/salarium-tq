"""File upload processing tasks"""

import csv
import io
import collections
from datetime import datetime, timedelta
from celery import chain

from attendance import calls, dateutils, keys, parsers, constants
from attendance.tasks import calculate_imported_data
from attendance.taskutils import update_tasks_count
from core import exceptions, fileutils, logger, authz
from core.app import app
from attendance.taskutils import suppress_traceback, task_params

logger = logger.get_logger()

ATT_SKIP_ENTITLEMENT_VALIDATION = [
    constants.ATT_REGULAR,
    constants.ATT_UNPAID_LEAVE,
    constants.ATT_PAID_LEAVE,
    constants.ATT_OVERBREAK,
    constants.ATT_ABSENT,
    constants.ATT_UNDERTIME
]

ATT_IMPORT_HEADER_MAPPING = {
    # Column Name:      (mapped key, value formatter/parser function)
    'Employee Id':      ('employee_id', None),
    'Employee Name':    ('employee_name', None),
    'Timesheet Date':   ('timesheet_date', parsers.parse_date),
    'Tags':             ('tags', None),
    'Clock In':         ('clock_in', parsers.parse_time),
    'Clock Out':        ('clock_out', parsers.parse_time),
    'Regular':          ('hours_regular', parsers.parse_time),
    'OT':               ('hours_overtime', parsers.parse_time),
    'NT':               ('hours_night', parsers.parse_time),
    'NT+OT':            ('hours_night_overtime', parsers.parse_time),
    'RD':               ('hours_rest_day', parsers.parse_time),
    'SH':               ('hours_special_holiday', parsers.parse_time),
    'RH':               ('hours_regular_holiday', parsers.parse_time),
    'Overbreak':        ('hours_overbreak', parsers.parse_time),
    'Paid Leave':       ('hours_paid_leave', parsers.parse_time),
    'Unpaid Leave':     ('hours_unpaid_leave', parsers.parse_time),
    'UT':               ('hours_undertime', parsers.parse_time),
    'UH':               ('hours_unworked_holiday', parsers.parse_time),
    'Tardiness':        ('hours_tardiness', parsers.parse_time),
    'Absent':           ('hours_absent', parsers.parse_time),
    'RH+NT':            ('hours_regular_holiday_night', parsers.parse_time),
    'RH+OT':            ('hours_regular_holiday_overtime', parsers.parse_time),
    'RH+NT+OT':         ('hours_regular_holiday_night_overtime', parsers.parse_time),
    'SH+NT':            ('hours_special_holiday_night', parsers.parse_time),
    'SH+OT':            ('hours_special_holiday_overtime', parsers.parse_time),
    'SH+NT+OT':         ('hours_special_holiday_night_overtime', parsers.parse_time),
    '2RH':              ('hours_regular_holiday_regular_holiday', parsers.parse_time),
    '2RH+NT':           ('hours_regular_holiday_regular_holiday_night', parsers.parse_time),
    '2RH+OT':           ('hours_regular_holiday_regular_holiday_overtime',
                         parsers.parse_time),
    '2RH+NT+OT':        ('hours_regular_holiday_regular_holiday_night_overtime',
                         parsers.parse_time),
    'RD+SH':            ('hours_rest_day_special_holiday', parsers.parse_time),
    'RD+RH':            ('hours_rest_day_regular_holiday', parsers.parse_time),
    'RD+2RH':           ('hours_rest_day_regular_holiday_regular_holiday',
                         parsers.parse_time),
    'RD+NT':            ('hours_rest_day_night', parsers.parse_time),
    'RD+SH+NT':         ('hours_rest_day_special_holiday_night', parsers.parse_time),
    'RD+RH+NT':         ('hours_rest_day_regular_holiday_night', parsers.parse_time),
    'RD+2RH+NT':        ('hours_rest_day_regular_holiday_regular_holiday_night',
                         parsers.parse_time),
    'RD+OT':            ('hours_rest_day_overtime', parsers.parse_time),
    'RD+SH+OT':         ('hours_rest_day_special_holiday_overtime', parsers.parse_time),
    'RD+RH+OT':         ('hours_rest_day_regular_holiday_overtime', parsers.parse_time),
    'RD+2RH+OT':        ('hours_rest_day_regular_holiday_regular_holiday_overtime',
                         parsers.parse_time),
    'RD+NT+OT':         ('hours_rest_day_night_overtime', parsers.parse_time),
    'RD+SH+NT+OT':      ('hours_rest_day_special_holiday_night_overtime',
                         parsers.parse_time),
    'RD+RH+NT+OT':      ('hours_rest_day_regular_holiday_night_overtime',
                         parsers.parse_time),
    'RD+2RH+NT+OT':     ('hours_rest_day_regular_holiday_regular_holiday_night_overtime',
                         parsers.parse_time),
}


ATT_IMPORT_REQUIRED_HEADER_COLUMNS = {
    'Employee Id',
    'Timesheet Date',
    'Clock In',
    'Clock Out',
}

ATT_IMPORT_FIELD_DEFINITIONS = parsers.get_column_headers_and_formatters(ATT_IMPORT_HEADER_MAPPING)


ATT_IMPORT_HOURS_TYPE_MAPPING = {
    'hours_regular': keys.HR_REGULAR,
    'hours_overtime': keys.HR_OVERTIME,
    'hours_night': keys.HR_NIGHT_SHIFT,
    'hours_night_overtime': keys.HR_OT_NT,
    'hours_rest_day': keys.REST_DAY,
    'hours_special_holiday': keys.SPECIAL_HOLIDAY,
    'hours_regular_holiday': keys.REGULAR_HOLIDAY,
    'hours_overbreak': keys.OVERBREAK,
    'hours_paid_leave': keys.HR_PAID_LEAVE,
    'hours_unpaid_leave': keys.HR_UNPAID_LEAVE,
    'hours_undertime': keys.HR_UNDERTIME,
    'hours_unworked_holiday': keys.HR_UH,
    'hours_tardiness': keys.HR_TARDY,
    'hours_absent': keys.HR_ABSENT,
    'hours_regular_holiday_night': keys.HR_RH_NT,
    'hours_regular_holiday_overtime': keys.HR_RH_OT,
    'hours_regular_holiday_night_overtime': keys.HR_RH_NT_OT,
    'hours_special_holiday_night': keys.HR_SH_NT,
    'hours_special_holiday_overtime': keys.HR_SH_OT,
    'hours_special_holiday_night_overtime': keys.HR_SH_NT_OT,
    'hours_regular_holiday_regular_holiday': keys.HR_2RH,
    'hours_regular_holiday_regular_holiday_night': keys.HR_2RH_NT,
    'hours_regular_holiday_regular_holiday_overtime': keys.HR_2RH_OT,
    'hours_regular_holiday_regular_holiday_night_overtime': keys.HR_2RH_NT_OT,
    'hours_rest_day_special_holiday': keys.HR_RD_SH,
    'hours_rest_day_regular_holiday': keys.HR_RD_RH,
    'hours_rest_day_regular_holiday_regular_holiday': keys.HR_RD_2RH,
    'hours_rest_day_night': keys.HR_RD_NT,
    'hours_rest_day_special_holiday_night': keys.HR_RD_SH_NT,
    'hours_rest_day_regular_holiday_night': keys.HR_RD_RH_NT,
    'hours_rest_day_regular_holiday_regular_holiday_night': keys.HR_RD_2RH_NT,
    'hours_rest_day_overtime': keys.HR_RD_OT,
    'hours_rest_day_special_holiday_overtime': keys.HR_RD_SH_OT,
    'hours_rest_day_regular_holiday_overtime': keys.HR_RD_RH_OT,
    'hours_rest_day_regular_holiday_regular_holiday_overtime': keys.HR_RD_2RH_OT,
    'hours_rest_day_night_overtime': keys.HR_RD_NT_OT,
    'hours_rest_day_special_holiday_night_overtime': keys.HR_RD_SH_NT_OT,
    'hours_rest_day_regular_holiday_night_overtime': keys.HR_RD_RH_NT_OT,
    'hours_rest_day_regular_holiday_regular_holiday_night_overtime': keys.HR_RD_2RH_NT_OT
}

@app.task(**task_params("caclulate_import"))
def calculate_import(company_id, bucket, key, pid, authz_data_scope=None):
    """File processing workflow"""
    att_params = {
        keys.COMPANY_ID: company_id,
        "s3_bucket": bucket,
        "s3_key": key,
        keys.JOB_ID: pid,
        "row_errors": [],
        'authz_data_scope': authz_data_scope
    }

    chain(
        download_file.s(att_params),
        parse_file_contents.s(),
        get_employees_data.s(),
        validate_employees_data.s(),
        get_shifts_data.s(),
        collect_data.s(),
        check_row_errors.s(),
        update_timesheets.s(),
        spawn_calculate_imported_data.s()
    )()


@app.task(**task_params("download_file"))
def download_file(att_params):
    """Download file"""
    # patch job stats set to 1
    job_id = att_params.get(keys.JOB_ID)
    if not job_id.startswith('INT:'):
        update_tasks_count.apply_async(args=(job_id, 1))

    # try to download file and store in temp store
    # save temp store file path to att_params via
    # file_path key.
    s3_bucket = att_params.get("s3_bucket")
    s3_key = att_params.get("s3_key")
    try:
        contents = fileutils.download_file(s3_bucket, s3_key)
    except:
        raise exceptions.PeerError("Error downloading file from S3.")

    # logger.debug(contents)
    att_params["file_contents"] = contents.decode("utf-8") if contents else ""
    return att_params


@app.task(**task_params("parse_file_contents", serializer="pickle"))
def parse_file_contents(att_params):
    file_contents = att_params.get('file_contents')

    if not file_contents:
        raise ValueError('file_contents should not be empty')

    # Remove raw csv file contents and create list to hold raw timesheet entries
    del att_params['file_contents']
    att_params['data'] = []

    with io.StringIO(file_contents) as csv_file:
        header = csv_file.readline().strip()
        header_columns = header.split(',')
        missing_header_columns = ATT_IMPORT_REQUIRED_HEADER_COLUMNS - set(header_columns)

        accepted_headers_set = set(ATT_IMPORT_HEADER_MAPPING)
        header_columns_set = set(header_columns)

        # Validation for required fields
        invalid_columns = header_columns_set.difference(accepted_headers_set)
        if invalid_columns:
            # Add error message for FE display
            att_params['row_errors'].append(str('The following columns are not valid: ' + ', '.join(invalid_columns)))
            value = None

        if missing_header_columns:
            raise ValueError('The following columns are required: ' + ', '.join(missing_header_columns))

        # Get CSV header columns and map them to their corresponding dict keys
        mapped_header_columns = [
            ATT_IMPORT_HEADER_MAPPING[column][0] for column in header_columns \
            if column in ATT_IMPORT_HEADER_MAPPING]

        reader = csv.DictReader(csv_file, mapped_header_columns)

        # Enumerate rows starting index counting at 2 to compensate for
        # CSV header and zero indexing
        for row_number, row in enumerate(reader, start=2):
            row_data = {}

            # Parse/format columns
            for column_key, value in row.items():
                # Ignore unknown/extraneous columns
                if column_key is None:
                    continue

                (column_name, parser) = ATT_IMPORT_FIELD_DEFINITIONS[column_key]

                # Reject paid leave code
                if column_name.upper() == constants.ATT_PAID_LEAVE:
                    error = exceptions.RowError(constants.PAID_LEAVE_ERROR, row_number)
                    att_params['row_errors'].append(str(error))

                if parser:
                    # Parse value if parser function is defined
                    try:
                        value = parser(value, column_name, row_number)
                    except exceptions.RowError as e:
                        att_params['row_errors'].append(str(e))
                        value = None

                row_data[column_key] = value

            # Get columns that are not in the uploaded CSV
            missing_columns = set(ATT_IMPORT_FIELD_DEFINITIONS.keys()) - set(row_data.keys())

            # Fill missing columns with None
            for column_key in missing_columns:
                row_data[column_key] = None

            # Get and validate timesheet type
            try:
                row_data['timesheet_type'] = get_timesheet_type(row_data, row_number)

                # Add parsed row for later processing
                att_params['data'].append(row_data)
            except exceptions.RowError as e:
                att_params['row_errors'].append(str(e))

    return att_params


@app.task(**task_params("get_employees_data", serializer="pickle"))
def get_employees_data(att_params):
    """Create employee lookup"""

    # get all employees and convert to lookup table
    # to be used for validation.
    company_id = att_params.get(keys.COMPANY_ID)
    employee_ids = list(set([row['employee_id'] for row in att_params['data']]))

    att_params["employees"] = {}

    # Check if there are valid employee ids before getting employees
    if employee_ids:
        payload = {
            'uids': employee_ids
        }

        employees = calls.get_employees_by_ids(company_id, payload).get('data', [])

        # Create key-value mapping of employees data (employee_id: employee data)
        att_params["employees"] = { employee['employee_id']: employee for employee in employees }

    return att_params

@app.task(**task_params("get_tardiness_rule", serializer="pickle"))
def get_tardiness_rule(att_params):
    """Task: get tardiness rule based on employee info"""
    #raise ValueError()
    company_id = att_params.get(keys.COMPANY_ID, None)
    employee_ids = list(set([row['employee_id'] for row in att_params['data']]))
    emp_tardiness_rules = {}
    for emp_id in employee_ids:
        employee_data = att_params['employees'][emp_id] if emp_id in att_params['employees'] else None
        if employee_data:
            pos_id = employee_data.get(keys.POSITION_ID, None)
            dep_id = employee_data.get(keys.DEPARTMENT_ID, None)
            loc_id = employee_data.get(keys.LOCATION_ID, None)
            emp_uid = employee_data.get(keys.ID, None)

            tardy_rules = employee_data.get('tardiness_rules', [])
            rule = calls.get_tardiness_rule(
                company_id, emp_uid, pos_id, dep_id, loc_id, tardy_rules
            )

            emp_tardiness_rules.update({emp_id: rule})
    return emp_tardiness_rules

@app.task(**task_params("get_shifts_data", serializer="pickle"))
def get_shifts_data(att_params):
    company_id = att_params.get(keys.COMPANY_ID)
    employee_ids = [employee['id'] for employee in att_params['employees'].values()]
    employee_shifts = {};

    if len(employee_ids) > 0:
        shifts = calls.get_company_shifts_by_employee_ids(company_id, employee_ids)
        employee_shifts = { shift['employee_id']: shift for shift in shifts['data'] }
        default_shifts = calls.get_co_default_schedule(company_id)
        att_params[keys.DEFAULT_SHIFT] = default_shifts

    for employee_uid, employee in att_params['employees'].items():
        # Get system generated employee id from user defined employee id
        employee_id = att_params['employees'][employee_uid]['id']

        att_params['employees'][employee_uid]['shifts_data'] = employee_shifts.get(employee_id)

    return att_params


@app.task(**task_params("validate_employees_data", serializer="pickle"))
def validate_employees_data(att_params):
    employee_id_list = att_params[keys.EMPLOYEES_DATA].keys()
    checked_row_list = set()

    authz_data_scope = att_params.get('authz_data_scope')

    # Enumerate rows starting index counting at 2 to compensate for
    # CSV header and zero indexing
    for row_number, row in enumerate(att_params['data'], start=2):
        # Construct concatenated row values for duplicate row checking
        row_list_entry = '-'.join([str(column) for column in row.values()])

        if row['employee_id'] not in employee_id_list:
            error = exceptions.RowError("Employee ID %s does not exist." % (row['employee_id'],), row_number)

            # Collect error
            att_params['row_errors'].append(str(error))
            continue

        if row_list_entry in checked_row_list:
            # Check if the entry is already in the checked rows list
            error = exceptions.RowError('Duplicate employee(%s)-date(%s) entry' %
                    (row['employee_id'], row['timesheet_date']), row_number)

            # Collect error
            att_params['row_errors'].append(str(error))
            continue

        checked_row_list.add(row_list_entry)

        employee = att_params[keys.EMPLOYEES_DATA].get(row['employee_id'])
        should_authorize = authz_data_scope and employee

        if should_authorize and not authz.is_employee_authorized(authz_data_scope, employee):
            # Check if the entry is authorized
            error = exceptions.RowError('No access to employee %s' %
                row['employee_id'], row_number)

            # Collect error
            att_params['row_errors'].append(str(error))
            continue

    return att_params


@app.task(**task_params("collect_data", serializer="pickle"))
def collect_data(att_params):
    """Collect data for submission to services"""

    att_params['timesheets'] = []
    att_params['hours'] = []
    td_rules = get_tardiness_rule(att_params)

    # Enumerate rows starting index counting at 2 to compensate for
    # CSV header and zero indexing
    for row_number, entry in enumerate(att_params['data'], start=2):
        # Get system generated employee ID from user-generated employee ID
        employee = att_params[keys.EMPLOYEES_DATA].get(entry['employee_id'])

        # Skip non-existing employee ids. This is reported in row_errors.
        if not employee:
            continue

        shifts = []

        if employee['shifts_data']:
            shifts = employee['shifts_data']['shifts']

        # Skip invalid timesheet dates. This is reported in row_errors.
        if not entry['timesheet_date']:
            continue

        # Processing for clock-in/clock-out filled columns
        if entry['timesheet_type'] == keys.AT_CLOCKS:
            if len(shifts) > 1:
                multi_shift_dates = get_multi_shift_dates(shifts)
                timesheet_date = entry['timesheet_date'].strftime(keys.SQL_DATE_FORMAT)

                # Check if clock-in/clock-out records are being uploaded
                # for the date an employee has multiple shifts in.
                if multi_shift_dates and timesheet_date in multi_shift_dates:
                    error = exceptions.RowError(
                        'Employees on a multiple shift are only allowed to upload hours worked',
                        row_number)

                    att_params['row_errors'].append(str(error))
                    continue

            is_time_record_duped = False
            if construct_clock_in_entry(
                att_params['company_id'], entry, employee) in att_params['timesheets']:
                error = exceptions.RowError(
                    'Duplicate Clock In found on: ' + str(entry['timesheet_date']),
                    row_number)
                is_time_record_duped = True
                att_params['row_errors'].append(str(error))

            if construct_clock_out_entry(
                att_params['company_id'], entry, employee) in att_params['timesheets']:
                error = exceptions.RowError(
                    'Duplicate Clock Out found on: ' + str(entry['timesheet_date']),
                    row_number)
                is_time_record_duped = True
                att_params['row_errors'].append(str(error))

            if is_time_record_duped:
                continue

            att_params['timesheets'].append(construct_clock_in_entry(
                att_params['company_id'], entry, employee))

            att_params['timesheets'].append(construct_clock_out_entry(
                att_params['company_id'], entry, employee))

        # Processing for filled hours worked columns
        if entry['timesheet_type'] == keys.AT_HOURS:
            timesheet_date_str = entry['timesheet_date'].strftime(keys.SQL_DATE_FORMAT)
            shift_id = None

            for shift in shifts:
                # Find applicable shift for the provided timesheet date
                if timesheet_date_str in shift['dates']:
                    shift_id = shift['id']

            # If there's no shift assigned, then use default shift
            if not shift_id and keys.DEFAULT_SHIFT in att_params:
                shift_id = _replace_shift_with_default(
                    att_params[keys.DEFAULT_SHIFT], timesheet_date_str)

            # highly unlikely to happen, since default shift is part of initial setup
            if not shift_id:
                error = exceptions.RowError(
                    'Cannot find applicable shift for %s for date %s.' % (
                        entry['employee_id'], timesheet_date_str,), row_number)
                att_params['row_errors'].append(str(error))
                continue

            # Get entry keys that match
            # ATT_IMPORT_HOURS_TYPE_MAPPING keys
            inter_list = get_matched_att_import_hrs_types(entry)

            hours_worked = []
            key_error = None
            for key in inter_list:
                # get matching att code
                att_code = keys.KEY_CONST_MAP[ATT_IMPORT_HOURS_TYPE_MAPPING[key]]
                value = entry.get(key)

                # skip att keys that don't have values
                if not value:
                    continue

                entitled = is_entitled_to_hour_code(employee, att_code, td_rules)
                if entitled:
                    hours_worked.append({
                        'type': ATT_IMPORT_HOURS_TYPE_MAPPING.get(key),
                        'time': value.strftime(keys.CSV_TIME_FORMAT)
                    })
                else:
                    key_error = exceptions.RowError(
                        'Cannot assign %s to %s. Please check their entitlements first.' % (
                        att_code, entry['employee_id']), row_number)

                    att_params['row_errors'].append(str(key_error))

            if hours_worked and not key_error:
                att_params['hours'].append({
                    'employee_id': employee['id'],
                    'date': timesheet_date_str,
                    'shift_id': shift_id,
                    'hours_worked': hours_worked
                })

    # remove default shift to avoid conflict with other chain task
    if keys.DEFAULT_SHIFT in att_params:
        del att_params[keys.DEFAULT_SHIFT]

    return att_params


@app.task(**task_params("check_row_errors", serializer="pickle", bind=True))
def check_row_errors(self, att_params):
    """Check row errors that were collected in previous tasks"""
    if len(att_params['row_errors']) > 0:
        # Report collected errors
        for error in att_params['row_errors']:
            calls.add_job_task_error(att_params['job_id'], str(error))

        # Set job tasks count to 0 to invoke FAILED status on job
        update_tasks_count.apply_async(args=(att_params['job_id'], -1))

        # Halt chain
        self.request.chain = None
        return None

    return att_params


@app.task(**task_params("update_timesheets", serializer="pickle"))
def update_timesheets(att_params):
    """Collect data for submission to services"""
    for timesheet in att_params['timesheets']:
        logger.debug("Pushing timesheet entry: %s, %s, %s, %s, %s",
                timesheet['employee_id'],
                timesheet['company_id'],
                timesheet['timestamp'],
                timesheet['state'],
                timesheet['tags'])
    if len(att_params['timesheets']) > 0:
        calls.multi_update_timesheet(att_params['timesheets'])

    return att_params


@app.task(**task_params("spawn_calculate_imported_data", serializer="pickle"))
def spawn_calculate_imported_data(att_params):
    """Collect data for submission to services"""

    # Prepare parameters for calculate imported data task
    company_id = att_params['company_id']
    job_id = att_params['job_id']

    employee_ids = []
    employees = {}

    # Pluck timestamp from list of timesheets
    timestamps = [timesheet['timestamp'] for timesheet in att_params['timesheets']]

    # Set param dates if attendance codes were set
    if att_params['hours']:
        dates = []
        for i in att_params['hours']:
            dates.append(i['date'])
            date_ranges = dates
    else:
        date_ranges = get_dates_from_timestamps(timestamps, return_string=True)

    for employee in att_params[keys.EMPLOYEES_DATA].values():
        employee_id = str(employee['id'])
        employee_ids.append(employee_id)

        employees[employee_id] = {
            **employee,
            keys.EMP_UID: employee_id,
            'rest_days': [], # Prepare rest_day bucket
            'shifts': [], # Prepare shifts bucket
            'default_shift': {}, # Prepare default shift bucket
            'hours_worked': [], # Prepare hours worked bucket
            'tardiness_rules': [], # Prepare bucket for tardiness rules
            'shift_dates': {}, # Prepare bucket for shift dates and holiday flags
            'timesheet': [], # Prepare bucket for timesheet
            'raw_timesheet': [] # Prepare bucket for raw timesheet
        }

    date_ranges = list(set(date_ranges))

    calculate_imported_data.apply_async(
        args=(company_id, employee_ids, employees, date_ranges, job_id, False, att_params['hours'],
              ATT_IMPORT_HOURS_TYPE_MAPPING))

def get_timesheet_type(data, row_number):
    """Validate timesheet if clock in/clock out columns are used or
    the hours worked columns and get the corresponding timesheet type.

    Arguments:
        data {dict} -- Row data
        row_number {int} -- Row number

    Returns:
        string
    """
    has_clocks_filled_up = is_clock_columns_filled(data)
    has_hours_filled_up = is_hour_columns_filled(data)

    # Ambiguity check if both clock columns and hour columns are filled
    if has_clocks_filled_up and has_hours_filled_up:
        raise exceptions.RowError(
            'Clock in and Clock out columns cannot be filled together with hours worked columns',
            row_number)

    # Check if none of the clock columns and hour columns are filled
    if not has_clocks_filled_up and not has_hours_filled_up:
        raise exceptions.RowError(
            'Please fill in either Clock in and Clock out columns or hours worked columns',
            row_number)

    if has_clocks_filled_up and data['clock_in'] == data['clock_out']:
        date_msg = ''

        if data['timesheet_date'] is not None:
            date_msg = f'in date {data["timesheet_date"]}'

        raise exceptions.RowError(
            'Uploading time entries with similar value %s is not allowed.' % (
            date_msg,), row_number)

    if has_clocks_filled_up:
        return keys.AT_CLOCKS

    if has_hours_filled_up:
        return keys.AT_HOURS


def process_hours_worked(entry, row_number):
    """Process hours worked entries for sending to TA-API

    Arguments:
        entry {dict} -- Timesheet entry
        row_number {int} -- Row number
    """
    hours_worked = []

    for key, value in entry.items():
        if key not in ATT_IMPORT_HOURS_TYPE_MAPPING.keys() or value is None:
            continue

        hours_worked.append({
            'type': ATT_IMPORT_HOURS_TYPE_MAPPING.get(key),
            'time': value.strftime(keys.CSV_TIME_FORMAT)
        })

    return hours_worked


def is_hour_columns_filled(entry):
    filled = False
    attendance_codes = [*ATT_IMPORT_HEADER_MAPPING.values()]
    skip_headers = ['employee_id', 'employee_name', 'timesheet_date', 'tags', 'clock_in','clock_out']
    for i in attendance_codes:
        if i[0] in entry and entry[i[0]] is not None:
            if i[0] not in skip_headers:
                filled = True
    return filled

def is_clock_columns_filled(entry):
    return entry['clock_in'] is not None and \
           entry['clock_out'] is not None


def get_multi_shift_dates(shifts):
    """Get dates that occurs in two or more shifts

    Arguments:
        shifts {list} -- List of shifts

    Returns:
        list -- List of dates occuring in two or more shifts
    """
    dates = []

    for shift in shifts:
        dates.extend(shift['dates'])

    counter = collections.Counter(dates)

    # Return dates with two or more occurrences
    return [date for date, count in counter.items() if count > 1]


def get_dates_from_timestamps(timestamps, return_string=False):
    """Generate date range from list of timestamps

    Arguments:
        timestamps {list} -- List of timestamps
        return_string {boolean} -- Return date as strings
    """
    # Convert timestamps to dates and ignore duplicates using set
    dates = list({ timestamp.date() for timestamp in timestamps })
    dates.sort()

    return [
        date if not return_string else date.strftime(keys.SQL_DATE_FORMAT) \
            for date in dates
    ]


def construct_tags(tags):
    """This method constructs an array of tags from the uploaded
    tags value separated by comma.

    Params:
        tags (string) -- The tags to split.
    """
    new_tags = tags.split(",") if tags else ""
    return [tag.strip() for tag in new_tags]

def construct_clock_in_entry(company_id, entry, employee):
    return {
        'company_id': company_id,
        'employee_id': employee['id'],
        'timestamp': datetime.combine(entry['timesheet_date'], entry['clock_in']),
        'state': '1',
        'tags': construct_tags(entry['tags'])
    }


def construct_clock_out_entry(company_id, entry, employee):
    timesheet_date = entry['timesheet_date']

    if entry['clock_in'] is not None and entry['clock_out'] < entry['clock_in']:
        timesheet_date += timedelta(days=1)

    return {
        'company_id': company_id,
        'employee_id': employee['id'],
        'timestamp': datetime.combine(timesheet_date, entry['clock_out']),
        'state': '0',
        'tags': construct_tags(entry['tags'])
    }

def get_matched_att_import_hrs_types(entry):
    """ Gets list of attendance hour types in the given entry
    that match the list of allowed hour type headers

    Args:
        entry (dict) -- Row entry

    Returns:
        (list)
    """

    # Loop through order of given attendance hour codes
    # and match with accepted list of mapping
    intersection = [
        key for key in entry.keys() if key in ATT_IMPORT_HOURS_TYPE_MAPPING.keys()
    ]

    return intersection


def is_entitled_to_hour_code(employee, att_code, td_rules=None):
    """ Checks if given employee is entitled
    to given attendance code

    Args:
        employee (dict) -- Employee details
        att_code (string) -- Attendance code to be checked

    Returns
        bool
    """
    # skip checking entitlements for certain attendance codes
    if att_code in ATT_SKIP_ENTITLEMENT_VALIDATION:
        return True

    is_ot = employee.get('overtime', False)
    is_nt = employee.get('differential', False)
    is_rh = employee.get('regular_holiday_pay', False)
    is_sh = employee.get('special_holiday_pay', False)
    is_rd = employee.get('rest_day_pay', False)
    is_td = False
    if (keys.EMP_ID in employee and employee[keys.EMP_ID] in td_rules and
        td_rules[employee[keys.EMP_ID]] is not None):
        is_td = True
    is_holiday_premium = employee.get('holiday_premium_pay', False)


    is_entitled = any([
        att_code.find(constants.ATT_OT) >= 0 and is_ot,
        att_code.find(constants.ATT_NT) >= 0 and is_nt,
        att_code.find(constants.ATT_RH) >= 0 and is_rh,
        att_code.find(constants.ATT_SH) >= 0 and is_sh,
        att_code.find(constants.ATT_UH) >= 0 and is_sh,
        att_code.find(constants.ATT_UH) >= 0 and is_rh,
        att_code.find(constants.ATT_RD) >= 0 and att_code.find(constants.ATT_TARDY) < 0 and is_rd,
        att_code.find(constants.ATT_TARDY) >= 0 and is_td
    ])

    if att_code.find(constants.ATT_RH) >= 0 or att_code.find(constants.ATT_SH) >= 0:
        if att_code.find(constants.ATT_OT) >= 0 and not is_holiday_premium:
            is_entitled = False

    return is_entitled

def _replace_shift_with_default(default_shift, target_date):
    shift_id = None
    date = datetime.strptime(target_date, "%Y-%m-%d")
    weekday = date.weekday() + 1  # add 1 since Monday=0 in datetime
    target_default_shift = _get_default_weekday_shift(default_shift, weekday)
    if keys.ID in target_default_shift:
        shift_id = target_default_shift[keys.ID]

    return shift_id

def _get_default_weekday_shift(data, weekday):
    # get the sched for the weekday
    list_of_scheds = data.get(keys.DATA)
    if not list_of_scheds:
        return None
    for sched in list_of_scheds:
        if sched.get("day_of_week") == weekday:
            return sched
    return None
