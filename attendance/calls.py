"""API calls"""
import json
import logging
import os
import time
import requests
from datetime import datetime

import ta_config
from core import exceptions
from core import warnings
from attendance import keys, taskutils, dateutils

logger = logging.getLogger(__name__)


if os.environ.get("TQ_DEBUG", "").upper() == "TRUE":
    logger.setLevel(logging.DEBUG)

# Constants, table defs
EMPLOYEE_INFO = (
    'id', #employee_uid
    'full_name', #employee_full_name
    'company_id',
    'employee_id', #company_employee_id
    'rank_id',
    'rank_name',
    'position_id',
    'position_name',
    'department_id',
    'department_name',
    'location_id',
    'location_name',
    'hours_per_day',
    'active',
    'time_attendance',
    'date_hired',
    'date_ended'
)

SHIFT_INFO = (
    'id',
    'schedule',  #only return this
)

SCHEDULE_INFO_REMOVE = (
    'id',
    'company_id',
    'color',
    'tags',
    'repeat',
    'auto_assign',
)

TIME_ATTENDANCE_INFO = (
    'time_attendance_id',
    'position_id',
    'position_name',
    'team_id',
    'team_name',
    'team_role',
    'primary_location_id',
    'primary_location_name',
    'secondary_location_id',
    'secondary_location_name',
    'timesheet_required',
    'overtime',
    'differential',
    'regular_holiday_pay',
    'special_holiday_pay',
    'holiday_premium_pay',
    'rest_day_pay'
)

DEFAULT_HEADERS = {
    "Accept": "application/json"
}

ALL_SELECTORS = ("All employees", "All locations", "All departments", "All positions")
# Private funcs --  do not use directly

def _get_api_url(api_id):
    return ta_config.APIS.get(api_id)

def _patch_resource(url, params=None, headers=None):
    try:
        response = requests.patch(url, json={"data": params}, headers=headers, timeout=(5, 14))
        logger.debug("PATCH URL: %s", url)
        logger.debug("Payload: %s", {"data": params})
        response.raise_for_status()
        logger.debug("Response: %s ", response.json())
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}:{}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))


def _get_resource(url, params=None, headers=None, body=None):
    try:
        _data = None
        logger.debug("GET URL: %s", url)
        if body:
            _data = json.dumps(body)
            logger.debug("Body: %s", _data)
        if params:
            logger.debug("Params: %s", params)

        # Timeout settings
        # 15 seconds to connect
        # 60 seconds to read
        response = requests.get(
            url, data=_data, headers=headers, timeout=(15, 60),
            params=params)
        response.raise_for_status()
        logger.debug("Response: %s ", response.json())
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"
        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def _get_resource_temp(url, params=None, headers=None):
    """This func to use when dealing with an incorrect 406 usage"""
    try:
        response = requests.get(url, params=params, headers=headers, timeout=(5, 14))
        logger.debug("GET URL: %s", url)
        logger.debug("Params: %s", params)
        response.raise_for_status()
        logger.debug("Response: %s ", response.json())
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == 406:
            return None

        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('failed getting resource') from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def _post_resource(url, params=None, headers=None):
    """This is a generic method that executes POST http calls """
    try:
        logger.debug("POST URL: %s", url)
        logger.debug("Params: %s", params)
        response = requests.post(url, json=params, headers=headers, timeout=(5, 14))
        #response = requests.post(url, json={"data" : params}, headers=headers)
        logger.debug("Response: %s ", response.json())
        response.raise_for_status()
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        #DEBUG
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"
        # Record is locked
        if resp_code == 419:
            raise warnings.RecordLockWarning(e.response.text) from e
        if resp_code == 418:
            # Retry for deadlocks
            raise warnings.DeadlockWarning(e.response.text) from e

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def _post_resource_new(url, params=None, headers=None):
    """This is a generic method that executes POST http calls """
    try:
        logger.debug("POST URL: %s", url)
        logger.debug("Params: %s", params)
        #response = requests.post(url, json=params, headers=headers)
        response = requests.post(url, json={"data" : params}, headers=headers, timeout=(10, 30))
        logger.debug("Response: %s ", response.json())
        response.raise_for_status()
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        #DEBUG
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def _put_resource(url, params=None, headers=None):
    """This is a generic method that executes POST http calls """
    try:
        logger.debug("PUT URL: %s", url)
        logger.debug("Params: %s", params)
        response = requests.put(url, json=params, headers=headers, timeout=(5, 14))
        #response = requests.post(url, json={"data" : params}, headers=headers)
        logger.debug(response.text)
        logger.debug("Response: %s ", response.json())
        response.raise_for_status()
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        #DEBUG
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def _get_timesheet(emp_uid, date, start_date = None, end_date = None):
    if start_date and end_date:
        start_ts = int(time.mktime(time.strptime(start_date, "%Y-%m-%d")))
        end_ts = int(time.mktime(time.strptime(end_date, "%Y-%m-%d")))
        end_ts = end_ts = end_ts + (86399 * 2) + 1  #buffer two days?
    else:
        start_ts = int(time.mktime(time.strptime(date, "%Y-%m-%d")))
        end_ts = start_ts + (86399 * 2) + 1  #buffer two days?
    payload = {
        "start_timestamp": start_ts,
        "end_timestamp": end_ts
    }
    url = _get_api_url(ta_config.API_TIMESHEET) %  (emp_uid)
    return _get_resource(url, params=payload, headers=DEFAULT_HEADERS)

def _get_employee_info(emp_uid):
    return _get_resource(_get_api_url(ta_config.API_EMPLOYEE_BASIC_INFO) % emp_uid,
                         headers=DEFAULT_HEADERS)

def _get_tardy_rules(company_id):
    url = _get_api_url(ta_config.API_TARDY_RULES) % company_id
    data = _get_resource(url, headers=DEFAULT_HEADERS)
    return data.get("data") if data.get("data", None) else []

def is_rule_applicable(entry, emp_uid, position_id, department_id, location_id):
    """Check if the  current affected employees entry applies to the employee"""
    entryid = entry.get("id")
    entrytype = entry.get("type")
    return ((entry.get("name") in ALL_SELECTORS) or
            (entrytype == "employee" and entryid == emp_uid) or
            (entrytype == "position" and position_id and  entryid == position_id) or
            (entrytype == "department" and department_id  and entryid == department_id) or
            (entrytype == "location" and location_id and entryid == location_id))

def _prune_sched_data(sid, sched_data):
    for key in SCHEDULE_INFO_REMOVE:
        if key in sched_data:
            del sched_data[key]
    sched_data["shift_id"] = sid
    return sched_data
# -----------------------------
# Public data apis
# -----------------------------
def get_company_rest_days(company_id, employee_ids=None, dates = None):
    """This method performs a service call to get the rest days of all employees.
    in a company.

    Args:
        company_id (int) -- The company id
    Returns:
        (list) -- list of rest day data.
    """
    emp_chunks = taskutils.chunk_list(employee_ids, ta_config.EMPLOYEES_SHIFTS_CHUNK_SIZE)
    rest_days = []
    for emp_chunk in emp_chunks:
        params = {
            "data": {
                "employee_ids": emp_chunk
            }
        }
        if dates:
            params['data']['start_date'] = dates[0]
            params['data']['end_date'] = dates[-1]

        custom_rest_days = _get_resource(_get_api_url(ta_config.API_COMPANY_RESTDAYS) % company_id,
                            headers=DEFAULT_HEADERS,
                            body=params)
        custom_rest_days = custom_rest_days.get("data", [])
        rest_days = rest_days + custom_rest_days

    return rest_days

def get_company_shifts_and_rest_days(company_id, employee_ids=None, dates = None):
    """This method performs a service call to get the employee shifts and rest days.
    in a company.

    Args:
        company_id (int) -- The company id
        employee_ids (list) -- The employee ids
        dates (list) -- The dates
    Returns:
        (list) -- shifts.
        (list) -- rest days.
    """
    emp_chunks = taskutils.chunk_list(employee_ids, ta_config.EMPLOYEES_SHIFTS_CHUNK_SIZE)
    rest_days = []
    all_shifts = []
    for emp_chunk in emp_chunks:
        params = {
            "data": {
                "employee_ids": emp_chunk
            }
        }
        if dates:
            params['data']['start_date'] = dates[0]
            params['data']['end_date'] = dates[-1]

        custom_rest_days = _get_resource(_get_api_url(ta_config.API_COMPANY_RESTDAYS) % company_id,
                            headers=DEFAULT_HEADERS,
                            body=params)
        custom_rest_days = custom_rest_days.get("data", [])
        rest_days = rest_days + custom_rest_days

        current_page = 0
        per_page = 100
        is_done = False
        while is_done is False:
            current_page += 1
            params['page'] = current_page
            params['per_page'] = per_page
            response = _get_resource(_get_api_url(ta_config.API_COMPANY_SHIFTS_ONLY_BY_EMPLOYEES) % company_id,
                            headers=DEFAULT_HEADERS,
                            body=params)
            if response:

                for item in response.get(keys.DATA, []):
                    # Only append data that have a shift
                    all_shifts.append(item)
            current_page = response.get("current_page")
            last_page = response.get("last_page")
            is_done = current_page >= last_page
    all_shifts = {keys.DATA: all_shifts}
    return all_shifts, rest_days


def get_rest_days(emp_uid):
    """This method performs a service call to get the day type of an employee.
    ta api.

    @param{int} `emp_uid` The employee id
    """
    return _get_resource(_get_api_url(ta_config.API_EMPLOYEE_RESTDAYS) % emp_uid,
                         headers=DEFAULT_HEADERS)


def get_holidays(company_id):
    """This method performs a service call to get the list
    of holidays of a company.

    This can also be used to check if an employee is entitled
    to for a holiday
    ta api.

    @param{int} `company_id` The company_id
    """
    return _get_resource(
               _get_api_url(ta_config.API_COMPANY_HOLIDAYS) % company_id,
                                    headers=DEFAULT_HEADERS)


def get_employee_shift(emp_uid, **payload):
    """This method performs a service call to employee shifts
    ta api.

    @param{int} `emp_uid` The employee id
    @param{kwargs} `**payload` Possible query parameters are:
                               * start_date (string)
                               * end_date (string)
    """
    r = _get_resource(_get_api_url(ta_config.API_EMPLOYEE_SHIFTS) % emp_uid,
                      params=payload, headers=DEFAULT_HEADERS)
    data = r.get("data", [])
    #prune data to contain only schedule
    start_date = payload.get("start_date")
    cleaned_data = []
    cnt = 0
    for sched in data:
        dates = sched.get("dates")
        if start_date not in dates:
            continue
        cleaned_data.append(
            _prune_sched_data(sched.get("id", cnt),
                              sched.get("schedule")))
        cnt = cnt + 1
    return cleaned_data


def get_company_shifts(company_id):
    """This method performs a service call to get all employees
    and their assigned shifts.

    Args:
        company_id (int) -- The company id to get the shifts
    Returns:
        (list) List of employees and their shift data
    """
    return _get_resource(_get_api_url(ta_config.API_COMPANY_SHIFTS) % company_id,
                         headers=DEFAULT_HEADERS)


def get_company_shifts_by_employee_ids(company_id, employee_ids, dates=None):
    """This method performs a service call to get all employees
    and their assigned shifts.

    Args:
        company_id (int) -- The company id to get the shifts
        employee_ids ([string]) -- List of employee ids to get
    Returns:
        (list) List of employees and their shift data
    """
    emp_chunks = taskutils.chunk_list(employee_ids, ta_config.EMPLOYEES_SHIFTS_CHUNK_SIZE)

    cleaned_response = []
    for emp_chunk in emp_chunks:
        params = {
            "data": {
                "employee_ids": emp_chunk
            }
        }

        if dates:
            params['data']['start_date'] = dates[0]
            params['data']['end_date'] = dates[-1]

        response = _get_resource(_get_api_url(ta_config.API_COMPANY_SHIFTS_BY_EMPLOYEES) % company_id,
                            headers=DEFAULT_HEADERS,
                            body=params)
        EXCLUDE_SHIFT_KEYS = [
            'first_name',
            'middle_name',
            'last_name'
        ]

        for item in response.get(keys.DATA, []):
            # Clear out any irelevant keys in the shifts data
            for key in EXCLUDE_SHIFT_KEYS:
                if key in item:
                    del item[key]

            # Clear out irelevant keys in the schedule data
            if item.get(keys.SHIFTS, []):
                # The assigned schedule data to clear
                for sched in item[keys.SHIFTS]:
                    for key in SCHEDULE_INFO_REMOVE:
                        if key in sched.get(keys.SCHEDULE, {}):
                            del sched[keys.SCHEDULE][key]

                # Only append data that have a shift
                cleaned_response.append(item)
    return {keys.DATA: cleaned_response}


def get_company_ta_settings(company_id):
    """This method performs a service call to get all TA
    settings of a company. The following are the possible TA
    settings that can be attached to an employee:

    1. Company info - For now this returns the timezone info of a company.
    2. Night shift settings - This is the company defined night shift settings
    3. Default shift - This is the company defined default shift.
    4. Tardiness rules - This is the company defined tardiness rules.
    5. Holidays - This is the company defined list of holidays.

    Args:
        company_id (int) -- The company id to retrieve these settings.
    Returns:
        (dict) -- Company TA settings data
    """
    return _get_resource(_get_api_url(ta_config.API_TA_SETTINGS) % company_id,
                         headers=DEFAULT_HEADERS)

def get_hours_worked(company_id, employee_ids, start_date, end_date):
    """This method performs a service call to get the hours worked of
    an employee given the following parameters:

    @param{int} `company_id` The company id of the employee
    @param{list} `employee_ids` List of employee ids
    @param{str} `start_date`  The starting date in YYYY-MM-DD format.
    @param{str} `end_date`  The ending date in YYYY-MM-DD format.
    """
    payload = {
        "start_date": start_date,
        "end_date": end_date
    }

    body = {
        "employees_ids": employee_ids,
    }
    #build query string manually
    url = _get_api_url(ta_config.API_HOURS_WORKED) % company_id
    response = _get_resource(url, params=payload, body=body, headers=DEFAULT_HEADERS)
    return response

def get_employee_info(emp_uid):
    """Returns basic employee info via HRIS api call.

    Parameters:
        emp_uid (integer) -- Unique(System) id of employee.
    """

    data = _get_employee_info(emp_uid)  # type: dict
    cleaned_data = {key: data.get(key, None) for key in EMPLOYEE_INFO}

    #fold in time_attendance attributes
    if cleaned_data:
        time_att = data.pop("time_attendance", {})
        cleaned_time_att = {key: time_att.get(key, None) for key in TIME_ATTENDANCE_INFO}
        if cleaned_time_att:
            cleaned_data.update(cleaned_time_att)
        if time_att:
            del cleaned_data["time_attendance"]
    return cleaned_data

def get_timesheet(emp_uid, date, start_date = None, end_date = None):
    """Gets the timesheet of the employee in the given date.  Returned timesheet entries
    include entries of next day just in case employee shift spans two days.

    Parameters:
        emp_uid (integer) -- Unique(System) id of employee.
        date (str) -- Target date in YYYY-MM-DD

    Returns:
        List of timein and timeout entries with cleaned pairs algo applied.
    """
    data = _get_timesheet(emp_uid, date, start_date, end_date)
    data = sorted(data, key=lambda x: x.get("timestamp"))
    return data
    #get pairs

def get_company_timesheet(company_id, start_ts, end_ts):
    start_ts = int(time.mktime(time.strptime(start_ts, "%Y-%m-%d")))
    end_ts = start_ts + (86399 * 2) + 1  #buffer two days?
    payload = {
        "start_timestamp": start_ts,
        "end_timestamp": end_ts
    }
    url = _get_api_url(ta_config.API_COMPANY_TIMESHEET) %  (company_id)
    return _get_resource(url, params=payload, headers=DEFAULT_HEADERS)

def get_company_timesheet_by_range(company_id, start_ts, end_ts):
    # Get dates list
    list_dates = list(dateutils.get_dates_from_date_range(start_ts, end_ts))
    batch_size = 10
    data = []
    # Fetch timestamps per date to avoid timeout issues in AWS DynamoDB
    for date in range(0, len(list_dates), batch_size):
        batch_date = list_dates[date:date+batch_size]
        start_date = batch_date[0]
        end_date = batch_date[-1]
        start_ts = int(time.mktime(time.strptime(str(start_date) + " 00:00:00", "%Y-%m-%d %H:%M:%S")))
        end_ts = int(time.mktime(time.strptime(str(end_date) + " 23:59:00", "%Y-%m-%d %H:%M:%S")))
        payload = {
            "start_timestamp": start_ts,
            "end_timestamp": end_ts
        }
        url = _get_api_url(ta_config.API_COMPANY_TIMESHEET) %  (company_id)
        for r in range(0, 15): # Retry on event of Dynamo DB timeout
            logger.debug("Fetching company time records try: " + str(r+1))
            try:
                return_data = _get_resource(url, params=payload, headers=DEFAULT_HEADERS)
                data.extend(return_data)
            except Exception as e:
                time.sleep(1)
                continue
            break
    return data

def get_tardiness_rule(company_id, emp_uid=None, position_id=None,
                       department_id=None, location_id=None, data=None):
    """Gets the tardiness rule for the employee

    Parameters:
        emp_uid (integer) -- Unique(system) id for employee.
        position_id (integer) -- System id for the employee position.
        department_id (integer) -- System id for the employee department.
        location_id (integeR) -- System id for the employee location.
    """
    if not data:
        data = _get_tardy_rules(company_id)
    if data:  #get the tardy rule for the employee
        for rule in data:
            affected_employees = rule.get("affected_employees", [])
            for entry in affected_employees:
                if is_rule_applicable(entry, emp_uid, position_id, department_id, location_id):
                    return rule
    return None

def get_company_tardiness_rules(company_id):
    """REturns company tardiness rules"""
    return _get_tardy_rules(company_id)

def get_co_default_schedule(company_id):
    """This method performs a service call to get the default schedule of
    a company.

    @param{int} `company_id` The company id of the employee
    @return{dict}
    """
    return _get_resource(
        _get_api_url(ta_config.API_COMPANY_DEFAULT_SHIFT),
        params={"company_id": company_id},
        headers=DEFAULT_HEADERS)

def get_night_shift_settings(company_id):
    """Return the company night shift settings"""

    return _get_resource_temp(
        _get_api_url(ta_config.API_COMPANY_NIGHT_SHIFT) % company_id,
        headers=DEFAULT_HEADERS)

def get_all_employees(company_id, page=None, per_page=None):
    """Get ALL employees of company"""
    uri = _get_api_url(ta_config.API_COMPANY_EMPLOYEES) % company_id
    uri = uri + '?status=active,semi-active&mode=TA_BASIC'
    if page and per_page:
        uri = uri + "&page="+str(page)+"&per_page=" + str(per_page)
    return _get_resource(uri, headers=DEFAULT_HEADERS)

def get_employees_by_ids(company_id, payload):
    """Get ALL employees of company"""
    payload["status"] = "active,semi-active"
    payload["mode"] = "TA_BASIC"
    uri = _get_api_url(ta_config.API_COMPANY_EMPLOYEES_BY_IDS) % company_id
    return _post_resource(uri, params=payload, headers=DEFAULT_HEADERS)

def create_attendance(payload):
    """Create an attendance record"""
    return _post_resource(
        _get_api_url(ta_config.API_ATTENDANCE_RECORDS),
        params=payload
    )

def update_job_tasks_count(job_id, task_count):
    """Update job tasks count"""
    return _patch_resource(
        _get_api_url(ta_config.API_ATTENDANCE_JOB) % job_id,
        params={"total_tasks": task_count},
        headers=DEFAULT_HEADERS
    )

def add_job_task_error(job_id, message=None, target=None, target_date=None):
    """Add job task error details"""
    req_params = {
        "message": message,
        "employee_uid": str(target) if target else None,
        "date": target_date
    }

    return _post_resource_new(
        _get_api_url(ta_config.API_ATTENDANCE_JOB_ERRORS) % job_id,
        params=[req_params],
        headers=DEFAULT_HEADERS
    )

def add_job_task_warning(job_id, message, target, target_date):
    """Add job task error details"""
    req_params = {
        "message": message,
        "employee_uid": target,
        "date": target_date,
        "is_warning": True
    }

    return _post_resource_new(
        _get_api_url(ta_config.API_ATTENDANCE_JOB_ERRORS) % job_id,
        params=[req_params],
        headers=DEFAULT_HEADERS
    )

def update_timesheet(emp_uid, company_uid, timestamp, state, tags):
    """Call timesheet log"""
    params = {
        "employee_uid" : emp_uid,
        "company_uid": company_uid,
        "timestamp": timestamp,
        "state": state
    }

    if tags:
        params["tags"] = tags

    return _put_resource(
        _get_api_url(ta_config.API_UPDATE_TIMESHEET),
        params=params,
        headers=DEFAULT_HEADERS
    )


def multi_update_timesheet(data):
    """Call timesheet log"""
    timesheet_data = []
    timesheets = []
    for entry in data:
        company_id = str(entry['company_id'])
        employee_id = entry['employee_id']
        # Add millisecond difference to avoid duplication errors
        timestamp = str(int(entry['timestamp'].timestamp()) + 1) if str(int(entry['timestamp'].timestamp())) in timesheets else str(int(entry['timestamp'].timestamp()))
        state = True if entry['state'] == '1' else False
        tags = entry['tags']

        timesheet_data.append((employee_id, company_id, state, timestamp, tags))
        timesheets.append(str(int(entry['timestamp'].timestamp())))

    chunk_timesheet_data = taskutils.chunk_list(
        timesheet_data, ta_config.EMPLOYEES_SMALL_CHUNK_SIZE)
    
    for timesheet_data in chunk_timesheet_data:
        res = _put_resource(
            _get_api_url(ta_config.API_MULTI_UPDATE_TIMESHEET),
            headers=DEFAULT_HEADERS,
            params={
                'data': timesheet_data
            }
        )

    return res


def multi_update_hours_worked(company_id, data):
    return _post_resource(
        _get_api_url(ta_config.API_HOURS_WORKED_BULK_ACTION) % (company_id,),
        headers=DEFAULT_HEADERS,
        params={
            'data': data
        }
    )


def get_attendance_lock_status(emp_uid, date):
    return _get_resource(
        _get_api_url(ta_config.API_ATTENDANCE_STATUS) % (emp_uid, date),
        headers=DEFAULT_HEADERS
    )

def expire_attendance_stats_cache(company_id):
    """Call AS-API endpoint to expire Attendance Statistics cache, per Company
    """
    requests.post(
        _get_api_url(ta_config.API_COMPANY_EXPIRE_ATTENDANCE_STATS_CACHE) % (company_id,),
        json={},
        headers=DEFAULT_HEADERS,
        timeout=(5, 14)
    )

def get_company_detail(company_id):
    return _get_resource(
        _get_api_url(ta_config.API_COMPANY_DETAIL) % (company_id,),
        headers=DEFAULT_HEADERS
    )


def get_employee_internal_ids_by_status(company_id, employee_status):
    uri = "{uri}?company_id={company_id}".format(
            uri=_get_api_url(ta_config.API_EMPLOYEES_INTERNAL_IDS),
            company_id=company_id
        )
    if employee_status:
        employee_status_query_string = "&filter[employee_status][]=".join(employee_status)
        uri = f"{uri}&filter[employee_status][]={employee_status_query_string}"
    return _get_resource(uri, headers=DEFAULT_HEADERS)
