"""Main app"""
from __future__ import absolute_import
from multiprocessing import Process

import os
import time

#
from celery import Celery
import ta_config
from event_consumer.handlers import AMQPRetryConsumerStep
from attendance import dao, taskutils

# instantiate celery object

#TODO: REmove this and timezone info should be company specific
os.environ["TZ"] = "Asia/Manila"
time.tzset()

app = Celery('tasks')

# import celery config
app.config_from_object('ta_config')
app.conf.update({
    'task_routes' : ([
        (
            'attendance.tasks.*',
            {
                'queue': taskutils.queue_route('ta_calc_main')
            }
        ),
        (
            'attendance.tasks.get_all_employees',
            {
                'queue': taskutils.queue_route('ta_calc_get_employees')
            }
        ),
        (
            'attendance.tasks.set_company_attendances',
            {
                'queue': taskutils.queue_route('ta_calc_set_attendance')
            }
        ),
        (
            'attendance.tasks.set_time_bound_data',
            {
                'queue': taskutils.queue_route('ta_calc_set_time_data')
            }
        ),
        (
            'attendance.tasks.save_attendance',
            {
                'queue': taskutils.queue_route('ta_calc_save')
            }
        ),
        (
            'attendance.tasks.calculate_company_attendance',
             {
                'queue': taskutils.queue_route('ta_save')
             }
        ),
        (
            'attendance.filetasks.*',
            {
                'queue': taskutils.queue_route('ta_calc_file')
            }
        ),
    ],)
})
app.conf.task_default_queue = taskutils.queue_route('ta_calc_main')

app.steps['consumer'].add(AMQPRetryConsumerStep)

engine = dao.mysql_connect()
process = Process(target=dao.mysql_recycle, args=(engine,))

dao.redis_connect()

if __name__ == '__main__':
    app.start()
