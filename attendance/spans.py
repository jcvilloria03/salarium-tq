"""
Spans

A span is a time-range or a set of time ranges.  The following is the span class heirarchy:

* **Span** - Abstract class for all span types
    * **SimpleSpan** - A span with lower bound and upper bound values, Sij
    * **MultiSpan** - A set of disjoint (SimpleSpans)

    * TODO NullSpan/ValueSpan-a span with no lower bound and upperbound but can possibly have value.

Application specific spans also defined in this module:
  * FlexiSpan (for use in attendance computation)
  * FloatingSpan (this is possibly a FlexiSpan)

Creating Spans

**Simple Spans**

A basic span is defined by a lower bound value and upper bound value.

Sij = S(i,j)   where i < j

A multispan M is a collection of disjoint simple spans that do not conflict(intersect)
with each other.

M = [S1, S2, ...]

**Span Values**

SimpleSpans have values which can be accessed using the *value* property.::

  S = create_span(1,2)
  print(S.value) #1

The default value for a simple span is upper bound - lower bound ( j - i ) values.

The value of a MultiSpan is the sum of the values of the member spans.


**Span Operations: Addition**

Span addition is commutative.

Given: a < b < c < d < e < f

Adding two successive spans:
S(a,b) + S(b,c) = S(a,c)

Adding two disjoint spans results to a multispan:
S(a,b) + S(c,d) = M( S(a,b), S(c,d))

Adding two intersecting spans:
S(a,c) + S(b,d) = SpanConflictError

Adding a MultiSpan with a Single Span
M( S(a,b), S(c,d))  + S(e, f)  = M( S(a,b), S(c,d), S(e, f))  disjoint elements

M( S(a,b), S(c,d))  + S(b, c) =  S(a,d)    combine to single span

M( S(a,b), S(d,e))  + S(b, c) =  M( S(a,c), S(d,e))
    following the rule for adding two successive simple spans

TODO: Add rules for empty/value spans

**Span Operations: Subtraction**

Span subtraction, S1 - S2 = R, where R is a span that is S1 - (S1 ∩ S2)

Sutracting two spans:

S(a, d) - S(b, e) = S(a,b)

S(d, f) - S(a, e) = S(e,f)

S(a, e) - S(c, d) = M (S(a,c), S(d,e) )

M(S(a,b), S(c,d)) - S(c,d) = S(a,b)

M(S(a,c), S(e,g)) - S(b,f) = M(S(a,b), S(f,g))  #S(b,f) intersects both member spans in M

M(S(a,c), S(e,h)) - M(S(b,f), S(g, i)) = M(S(a,b), S(f,g))
   #S(e,h) is intersected by both S(b,f) and S(g,i)


**Span Operations:  Intersection**

Intersection returns the common subspans between two spans.

S(a,c) ∩ S(b, d) = S(b,c)

"""
#import copy
from abc import ABC, abstractmethod
from datetime import datetime

class SpanConflictError(TypeError):
    """Span conflict error"""
    pass

class Span(ABC):
    """Base class"""

    def isconflict_with(self, other):
        """Conflict check"""
        return False

    @abstractmethod
    def intersection(self, other):
        """Returns the common subspan between current span and other span

        Arguments:
            other {Span} -- other Span
        Returns:
            The common subspans or None if they do not intersect.

        """

        pass


class SimpleSpan(Span):
    """Simple fixed span from i to j"""

    def __init__(self, i, j):
        self._i = i
        self._j = j
        self.n = 0

    @property
    def i(self):
        """Lower bound property value"""
        return self._i

    @property
    def j(self):
        """Upper bound property value"""
        return self._j

    def __getitem__(self, i):
        if i == 0 or i == -1:
            return self
        return None

    def __len__(self):
        return 1

    def __add__(self, other):
       # if isinstance(other, VS):
       #     m = MS() + other
        if other is None:
            return self
        if isinstance(other, VS):
            return MS([self, other])
        elif isinstance(other, SS):
            if self.j == other.i:
                return SS(self.i, other.j)
            if self.i >= other.i:
                return other + self
            if self.isconflict_with(other):
                raise SpanConflictError()
            if self.j < other.i:
                return create_span((self.i, self.j), (other.i, other.j))
        elif isinstance(other, MS):
            return other + self
        raise TypeError('cannot add two spans')

    def __sub__(self, other):
        if other is None:
            return self
        if isinstance(other, VS):
            raise ValueError("cannot subtract value span")
        if isinstance(other, SS):
            if self.i >= other.i and self.i < other.j < self.j:  # overlap from left
                return create_span((other.j, self.j))
            if self.i < other.i < self.j and other.j >= self.j:  # overlap from right
                return create_span((self.i, other.i))
            if self.i >= other.i and self.j <= other.j:  # complete overlap
                return None
            if self.i < other.i and other.j < self.j:  # gap
                return create_span((self.i, other.i), (other.j, self.j))
            return self
        if isinstance(other, MS):
            copy = create_span((self.i, self.j))
            for item in other.items:
                if copy.isconflict_with(item):
                    copy = copy - item
            return copy

    def intersection(self, other):
        """Return the common span area between two spans"""
        if other is None:
            return None
        if isinstance(other, SS):
            if self.isconflict_with(other):
                return create_span((max(self.i, other.i), min(self.j, other.j)))
            else:
                return None
        if isinstance(other, MS):
            return other.intersection(self)
        return None

    def union(self, other):
        """Combine two spans"""
        if self.isconflict_with(other):
            if isinstance(other, MS):
                return other.union(self)
            if isinstance(other, SS):
                return create_span((min(self.i, other.i), max(self.j, other.j)))
        else:
            return self + other

    def isconflict_with(self, other):
        if isinstance(other, SS):
            if self.i > other.i:
                return other.isconflict_with(self)
            return ((self.i <= other.i < self.j) or
                    (self.i < other.j <= self.j))
        if isinstance(other, MS):
            return other.isconflict_with(self)
        return False

    def __str__(self):
        i_str = datetime.fromtimestamp(self.i, ).strftime("%Y-%m-%d %H:%M:%S")
        j_str = datetime.fromtimestamp(self.j, ).strftime("%Y-%m-%d %H:%M:%S")
        return "SimpleSpan: %s, %s" % (i_str, j_str)

    def __eq__(self, other):
        if isinstance(other, SS):
            return self.i == other.i and self.j == other.j

    def __lt__(self, other):
        if isinstance(other, SS):
            return self.i < other.i and self.j < other.j

    def __gt__(self, other):
        if isinstance(other, SS):
            return self.i > other.i and self.j > other.j

    @property
    def value(self):
        """Returns value of span"""
        return self.j - self.i

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n > 0:
            raise StopIteration
        self.n = self.n + 1
        return self


class ValueSpan(SimpleSpan):
    """A span that has no range but has value"""

    def __init__(self, value=0):
        super(ValueSpan, self).__init__(0, 0)
        self._val = value

    @property
    def i(self):
        """Lower bound property value"""
        return 0

    @property
    def j(self):
        """Upper bound property value"""
        return 0

    @property
    def value(self):
        return self._val

    def __add__(self, other):
        if isinstance(other, VS):
            return MS([self, other])
        return other + self

    def __sub__(self, other):
        return ValueError("cannot subtract from Value span")

    def isconflict_with(self, other):
        return False  # always false

    def union(self, other):
        return self + other

    def intersection(self, other):
        return other.intersection(self)


class MultiSpan(Span):
    """Span that represents discontigous subspans"""

    def __init__(self, items: [SimpleSpan]):
        self.n = 0
        if items:
            items.sort(key=lambda x: x.i)
            self._items = items
        else:
            self._items = []

    @property
    def items(self):
        """Items property"""
        return self._items

    @property
    def i(self):
        """Returns the minimum value"""
        if self.items:
            return self.items[0].i
        return None

    @property
    def j(self):
        """Return max value"""
        if self.items:
            return self.items[-1].j
        return None

    def __getitem__(self, i):
        return self._items[i]

    def __add__(self, other):
        items = list(self._items)  # copy
        if isinstance(other, VS):
            items.insert(0, other)
        elif isinstance(other, SS):
            if not items:
                items.append(other)
            else:
                items = _insert(items, other)
        elif isinstance(other, MS):
            for item in other.items:
                items = _insert(items, item)

        items = _normalize(items)
        if len(items) == 1:
            return items[0]
        else:
            m = MS(items)
            return m

    def __sub__(self, other):
        if other is None:
            return self
        items = []  # new
        if isinstance(other, SS):
            for item in self.items:
                if item.isconflict_with(other):
                    newitem = item - other
                    if isinstance(newitem, MS):
                        items.extend(newitem.items)
                    if isinstance(newitem, SS):
                        items.append(newitem)
                else:
                    items.append(item)

        elif isinstance(other, MS):
            queue = list(self.items)
            while queue:
                head = queue[0]
                for item2 in other.items:
                    if head and head.isconflict_with(item2):
                        head = head - item2
                if isinstance(head, SS):
                    items.append(head)
                if isinstance(head, MS):
                    items.extend(head.items)
                queue = queue[1:]

        if len(items) == 1:
            return items[0]
        elif items:
            items = _normalize(items)
            return MS(items)
        else:
            return None

    def isconflict_with(self, other):
        if isinstance(other, SS):
            for item in self.items:
                if item.isconflict_with(other):
                    return True
        elif isinstance(other, MS):
            for item1 in self.items:
                for item2 in other.items:
                    if item1.isconflict_with(item2):
                        return True
        return False

    def intersection(self, other):
        if other is None:
            return None
        items = []
        if isinstance(other, SS):
            for item in self.items:
                common = item.intersection(other)
                if common:
                    items.append(common)
        if isinstance(other, MS):
            for item1 in self.items:
                for item2 in other.items:
                    common = item1.intersection(item2)
                    if common:
                        items = _insert(items, common)

        if items:
            if len(items) == 1:
                return items[0]
            return MS(items)
        return None

    def union(self, other):
        items = list(self._items)
        if other is None:
            return MS(items)

        if isinstance(other, SS):
            # find which items other is in conflict with
            indexes = []
            for idx, item in enumerate(items):
                if item.isconflict_with(other):
                    indexes.append(idx)

            new_items = []
            new_item = other
            for idx in indexes:
                new_item = new_item.union(items[idx])

            for idx, item in enumerate(items):
                if idx not in indexes:
                    new_items = _insert(new_items, item)

            new_items = _insert(new_items, new_item)
            # normalize
            new_items = _normalize(new_items)
            if len(new_items) == 1:
                return new_items[0]
            else:
                return MS(new_items)
        if isinstance(other, MS):
            this_ms = MS(items)
            for sp in other:
                this_ms = this_ms.union(sp)
            return this_ms

    def __len__(self):
        return len(self._items)

    def __eq__(self, other):
        if isinstance(other, MS):
            if len(self._items) == len(other.items):
                for x in range(len(self._items)):
                    if self.items[x] != other.items[x]:
                        return False
                return True
        return False

    @property
    def value(self):
        """Returns total value of spans in multispan"""
        total = 0
        for item in self._items:
            total = total + item.value
        return total

    #iterator
    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n >= len(self._items):
            raise StopIteration
        result = self._items[self.n]
        self.n = self.n + 1
        return result

    def __str__(self):
        return " ".join([
            str(x) for x in self
        ])

def _insert(items, item: SimpleSpan) -> list:
    index = 0
    for member in items:
        if member.isconflict_with(item):
            raise SpanConflictError()
        if item.i < member.i:
            items.insert(index, item)
            return items
        index = index + 1
    items.append(item)
    return items


def _normalize(items):
    """Check successive items if they are spans that can be combined"""
    newitems = []
    cur_item = items[0]
    queue = items[1:]
    for q_item in queue:
        if not isinstance(q_item, VS) and cur_item.j == q_item.i:
            # combine these two and add to newitems
            cur_item = SS(cur_item.i, q_item.j)
        else:
            newitems.append(cur_item)
            cur_item = q_item
    newitems.append(cur_item)
    return newitems


# aliases
SS = SimpleSpan
MS = MultiSpan
VS = ValueSpan


def list_to_spans(spanlist: [SimpleSpan]):
    """Create a span  from list of SimpleSpans"""
    items = _normalize(spanlist)
    if not items:
        return None
    if len(items) == 1:
        return items[0]
    return MS(items)


def create_span(*args):
    """Create a span from tuples (i,j) ...."""
    items = []
    for i, j in args:
        items = _insert(items, SimpleSpan(i, j))

    return list_to_spans(items)


def create_value_span(value):
    """Create a value span"""
    return VS(value)
