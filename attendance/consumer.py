"""Message consumer tasks"""

import json
import uuid

from event_consumer.handlers import message_handler

import ta_config
from core import logger
from attendance import keys
from attendance.filetasks import calculate_import
from attendance.tasks import calculate_company, calculate_multi, export_attendance_records

logger = logger.get_logger()

SINGLE_CALCULATION_THRESHOLD = 1

@message_handler(ta_config.CONSUMER_ROUTING_KEY)
def calculate(body):
    """Process single calculation given an employee uid and date"""
    data = json.loads(body) or dict()
    if data.get(keys.NAME) == "calculate-attendance":
        calculate_attendance(data)


@message_handler(ta_config.ATTENDANCE_EXPORT_ROUTING_KEY)
def attendance_export(body):
    """Process attendance export request"""
    data = json.loads(body) or dict()
    if data.get(keys.NAME) == "export-attendance":
        job_id = data.get("job_id")
        export_attendance_records.apply_async(
            args=(job_id, data))


def calculate_attendance(data):
    """Calculate attendance routing"""
    print('calculate_attendance')
    ctype = data.get(keys.TYPE)
    print('calculate_attendance ctype')
    print(ctype)
    pid = "INT:" + str(uuid.uuid4()) if data.get(
        keys.JOB_ID, None) is None else data.get(keys.JOB_ID)
    company_id = data.get(keys.COMPANY_ID, None)
    logger.debug("Company ID is %s ", company_id)
    ids = data.get(keys.IDS, [])
    dates = data.get(keys.DATES, [])

    authz_data_scope = data.get('authz_data_scope')

    if ctype == keys.CT_MULTIPLE:
        #Since attendance bulk API already updates job count using len(emp_ids) * len(dates)
        #as soon as the job is created. We'd want to avoid calculator.py to also update the job
        #count once the job is splitted in parallel (#880) which is currently set to only trigger
        #if employee IDs are provided.
        logger.debug("joe joe joe: ")
        print('calculate_attendance company_id')
        print(company_id)
        print('calculate_attendance ids')
        print(ids)
        if company_id and ids:
            # Calculate a single entry only with a maximum of the
            # defined calculation threshold
            #if len(ids) <= SINGLE_CALCULATION_THRESHOLD and \
            #len(dates) <= SINGLE_CALCULATION_THRESHOLD:
            #    logger.debug("Single Calculation: %s", json.dumps(data))
            #    calculate_multi.apply_async(args=(ids, dates, pid))
            #else:
            logger.debug("Calculating with co_id's and emp_id's: %s", json.dumps(data))
            # Calculate by batch if the number of calculations is greater
            # than the defined threshold
            print('calculate_attendance calculate_company')
            calculate_company.apply_async(
                args=(company_id, ids, dates, pid, False))
        elif company_id and not ids:
            # Still, calculate by batch if only the company id is given
            # this means that the request is to calculate all employees
            # in a company.
            logger.debug("Calculating with co_id's: %s", json.dumps(data))
            calculate_company.apply_async(
                args=(company_id, ids, dates, pid, True))
        else:
            # Otherwise, do single calculation
            calculate_multi.apply_async(args=(ids, dates, pid))
    elif ctype == keys.CT_BATCH:
        # Other option for batch calculation
        logger.debug("Batch Calculation: %s", json.dumps(data))
        calculate_company.apply_async(args=(company_id, ids, dates, pid, True))
    elif ctype == keys.CT_FILE:
        # File calculation
        logger.debug("File Calculation: %s", json.dumps(data))
        s3_bucket = data.get("s3_bucket", None)
        s3_key = data.get("s3_key", None)
        calculate_import.apply_async(args=(company_id, s3_bucket, s3_key, pid, authz_data_scope))
    else:
        logger.warning("Unable to process message")
