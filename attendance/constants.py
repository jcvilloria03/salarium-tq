"""Attendance constants"""
ATT_SCHEDULED_HOURS = "SCHEDULED HOURS"
ATT_REGULAR = "REGULAR"
ATT_PAID_LEAVE = "PAID LEAVE"
ATT_UNPAID_LEAVE = "UNPAID LEAVE"
ATT_ABSENT = "ABSENT"
ATT_TARDY = "TARDY"
ATT_UNDERTIME = "UNDERTIME"
ATT_OT = "OT"
ATT_NT = "NT"
ATT_NT_OT = "NT+OT"
ATT_UH = "UH" #this is for unworked holiday
ATT_OVERBREAK = "OVERBREAK"

# Regular Holiday Combinations
ATT_RH = "RH"
ATT_RH_NT = ATT_RH + "+" + ATT_NT
ATT_RH_OT = ATT_RH + "+" + ATT_OT
ATT_RH_NT_OT = ATT_RH + "+" + ATT_NT + "+" + ATT_OT
ATT_2RH = "2" + ATT_RH
ATT_2RH_NT = ATT_2RH + "+" + ATT_NT
ATT_2RH_OT = ATT_2RH + "+" + ATT_OT
ATT_2RH_NT_OT = ATT_2RH + "+" + ATT_NT + "+" + ATT_OT

# Special Holiday Combinations
ATT_SH = "SH"
ATT_SH_NT = ATT_SH + "+" + ATT_NT
ATT_SH_OT = ATT_SH + "+" + ATT_OT
ATT_SH_NT_OT = ATT_SH + "+" + ATT_NT + "+" + ATT_OT
ATT_RH_SH = ATT_RH + "+" + ATT_SH# Special Holiday Combinations

# Special Working Holiday
ATT_SWH = "REGULAR"

# Rest Day Combinations
ATT_RD = "RD"
ATT_RD_OT = ATT_RD + "+" + ATT_OT
ATT_RD_NT = ATT_RD + "+" + ATT_NT
ATT_RD_NT_OT = ATT_RD + "+" + ATT_NT + "+" + ATT_OT

# RD SH
ATT_RD_SH = ATT_RD + "+" + ATT_SH
ATT_RD_SH_OT = ATT_RD + "+" + ATT_SH + "+" + ATT_OT
ATT_RD_SH_NT = ATT_RD + "+" + ATT_SH + "+" + ATT_NT
ATT_RD_SH_NT_OT = ATT_RD + "+" + ATT_SH + "+" + ATT_NT + "+" + ATT_OT

# RD RH
ATT_RD_RH = ATT_RD + "+" + ATT_RH
ATT_RD_RH_NT = ATT_RD + "+" + ATT_RH + "+" + ATT_NT
ATT_RD_RH_OT = ATT_RD + "+" + ATT_RH + "+" + ATT_OT
ATT_RD_RH_NT_OT = ATT_RD + "+" + ATT_RH + "+" + ATT_NT + "+" + ATT_OT

# RD 2RH
ATT_RD_2RH = ATT_RD + "+" + ATT_2RH
ATT_RD_2RH_NT = ATT_RD + "+" + ATT_2RH + "+" + ATT_NT
ATT_RD_2RH_OT = ATT_RD + "+" + ATT_2RH + "+" + ATT_OT
ATT_RD_2RH_NT_OT = ATT_RD + "+" + ATT_2RH + "+" + ATT_NT + "+" + ATT_OT




CODES_TABLE = {
    "HOLIDAY": {
        "SPECIAL": ATT_SH,
        "REGULAR": ATT_RH,
        "SPECIAL WORKING": ATT_REGULAR
    },
    "NIGHT_SHIFT": ATT_NT,
    "PAID_LEAVE": ATT_PAID_LEAVE,
    "UNPAID_LEAVE": ATT_UNPAID_LEAVE,
    "REGULAR": ATT_REGULAR,
    "OVERTIME": ATT_OT,
    "UNDERTIME": ATT_UH,
    "OVERTIME_NIGHT_SHIFT": ATT_NT_OT
}

VALID_KEYS = {
    ATT_SCHEDULED_HOURS,
    ATT_REGULAR,
    ATT_PAID_LEAVE,
    ATT_UNPAID_LEAVE,
    ATT_OT,
    ATT_NT,
    ATT_NT_OT,
    ATT_UH,
    ATT_RH,
    ATT_SH,
    ATT_RD,
}

SECONDS_IN_A_DAY = 86400
POST_SHIFT_TIMESHEET_HOUR_ALLOWANCE = 10
PRE_SHIFT_START_TIMESHEET_HOUR_ALLOWANCE = 1.5
TWO_DAYS_IN_SECONDS = 172799
SECONDS_PER_HOUR = 3600
OT_HOURS_THRESHOLD = 8

PAID_LEAVE_ERROR = 'Paid Leave cannot be added via batch upload attendance.' \
'Please file leaves through salarium.com/time/filed-leaves/add'
