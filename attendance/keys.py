"""keys.py

Contains string keys referenced in code.

"""
import attendance.constants as constants

# formats
SQL_DATE_FORMAT = '%Y-%m-%d'
SQL_DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
CSV_DATE_FORMAT = '%m/%d/%Y'
CSV_TIME_FORMAT = '%H:%M'

VALID_CSV_DATE_FORMATS = [
    '%m/%d/%Y',
    '%Y-%m-%d'
]

VALID_CSV_TIME_FORMATS = [
    '%H:%M',
    '%I:%M %p'
]

# dict keys
ID = "id"
IDS = "ids"
DATA = "data"
JOB_ID = "job_id"
DATE = "date"
DATES = "dates"
SHIFT_DATES = "shift_dates"
NT_SETTINGS = "night_shift_settings"
TIMESHEET = "timesheet"
RAW_TIMESHEET = "raw_timesheet"
TIMESHEET_REQUIRED = "timesheet_required"
HOURS_WORKED = "hours_worked"
SHIFT_ID = "shift_id"
TYPE = "type"
TIMESTAMP = "timestamp"

START_DATETIME = "start_datetime"
END_DATETIME = "end_datetime"

START_TIME = "start_time"
END_TIME = "end_time"

START = "start"
END = "end"

BREAKS = "breaks"
IS_PAID_BREAK = "is_paid"
NAME = "name"
DEFAULT = "default"
IS_WARNING = "is_warning"

DAY_TYPE = 'day_type'
BREAK_HOURS = "break_hours"
CORE_TIMES = "core_times"
HOLIDAY_TYPE = "holiday_type"
HOLIDAYS = "holidays"
HOURS_PER_DAY = "hours_per_day"
MINUTES_TARDY = "minutes_tardy"
MINUTES_TO_DEDUCT = "minutes_to_deduct"
REST_DAY = "rest_day"
SPECIAL_HOLIDAY = "special_holiday"
REGULAR_HOLIDAY = "regular_holiday"
OVERBREAK = "overbreak"
REST_DAYS_DATA = "rest_days"
SHIFTS = "shifts"
SHIFT = "shift"
PREVIOUS_SHIFT = "previous_shift"
SCHEDULE = "schedule"
DEFAULT_SHIFT = "default_shift"
MAX_CLOCKOUT = 'max_clockout'

TARDINESS_RULE = "tardiness_rule"
TARDINESS_RULES = "tardiness_rules"
TIME_SHIFT = "time_shift"
TOTAL_HOURS = "total_hours"
AFFECTED_EMPLOYEES = "affected_employees"
EMPLOYEE_ID = "employee_id"

WORK_BREAK_END = "work_break_end"
WORK_BREAK_START = "work_break_start"

WORK_END = "work_end"
WORK_START = "work_start"

EMP_ID = "employee_id"
EMP_UID = "employee_uid"
COMPANY_ID = "company_id"
POSITION_ID = "position_id"
DEPARTMENT_ID = "department_id"
LOCATION_ID = "location_id"
EMPLOYEES_DATA = "employees"
COMPANY_RESTDAYS = "company_rest_days"
IMPORTED_CODES = "imported_codes"

# HR_TYPES
HR_PAID_LEAVE = "paid_leave"
HR_UNPAID_LEAVE = "unpaid_leave"
HR_REGULAR = "regular"
HR_NIGHT_SHIFT = "night_shift"
HR_OVERTIME = "overtime"
HR_REST_DAY = "rest_day"
HR_UNDERTIME = "undertime"
HR_OT_NT = "overtime_night_shift"
HR_ABSENT = "absent"
HR_RD = "rest_day"
HR_SH = "special_holiday"
HR_RH = "regular_holiday"
HR_OVERBREAK = "overbreak"
HR_UH = "unworked_holiday"
HR_TARDY = "tardiness"
HR_RH_SH = "regular_holiday_special_holiday"
HR_RH_NT = "regular_holiday_night"
HR_RH_OT = "regular_holiday_overtime"
HR_RH_NT_OT = "regular_holiday_night_overtime"
HR_SH_NT = "special_holiday_night"
HR_SH_OT = "special_holiday_overtime"
HR_SH_NT_OT = "special_holiday_night_overtime"
HR_2RH = "regular_holiday_regular_holiday"
HR_2RH_NT = "regular_holiday_regular_holiday_night"
HR_2RH_OT = "regular_holiday_regular_holiday_overtime"
HR_2RH_NT_OT = "regular_holiday_regular_holiday_night_overtime"
HR_RD_SH = "rest_day_special_holiday"
HR_RD_RH = "rest_day_regular_holiday"
HR_RD_2RH = "rest_day_regular_holiday_regular_holiday"
HR_RD_NT = "rest_day_night"
HR_RD_SH_NT = "rest_day_special_holiday_night"
HR_RD_RH_NT = "rest_day_regular_holiday_night"
HR_RD_2RH_NT = "rest_day_regular_holiday_regular_holiday_night"
HR_RD_OT = "rest_day_overtime"
HR_RD_SH_OT = "rest_day_special_holiday_overtime"
HR_RD_RH_OT = "rest_day_regular_holiday_overtime"
HR_RD_2RH_OT = "rest_day_regular_holiday_regular_holiday_overtime"
HR_RD_NT_OT = "rest_day_night_overtime"
HR_RD_SH_NT_OT = "rest_day_special_holiday_night_overtime"
HR_RD_RH_NT_OT = "rest_day_regular_holiday_night_overtime"
HR_RD_2RH_NT_OT = "rest_day_regular_holiday_regular_holiday_night_overtime"

# entitlement keys
EN_OVERTIME = HR_OVERTIME
EN_NT_PAY = "differential"
EN_RH_PAY = "regular_holiday_pay"
EN_SH_PAY = "special_holiday_pay"
EN_RD_PAY = "rest_day_pay"
EN_PREMIUM_PAY = "holiday_premium_pay"

# break type
BR_FIXED = "fixed"
BR_FLEXI = "flexi"
BR_FLOATING = "floating"

# shift type
SH_FIXED = BR_FIXED
SH_FLEXI = BR_FLEXI

# VALUES
VAL_DEFAULT = DEFAULT

# CT_TYPE
CT_MULTIPLE = "multiple"
CT_BATCH = "batch"
CT_FILE = "file"

# DB Deadlock key
DEADLOCK_ERROR_CODE = "DeadlockError"

MESSAGE = "message"

DATE_HIRED = "date_hired"
DATE_ENDED = "date_ended"

SCHEDULED_OVERTIME = "scheduled_overtime"

KEY_CONST_MAP = {
    HR_NIGHT_SHIFT: constants.ATT_NT,
    HR_OT_NT: constants.ATT_NT_OT,
    HR_REGULAR: constants.ATT_REGULAR,
    HR_OVERTIME: constants.ATT_OT,
    HR_UNDERTIME: constants.ATT_UNDERTIME,
    HR_PAID_LEAVE: constants.ATT_PAID_LEAVE,
    HR_UNPAID_LEAVE: constants.ATT_UNPAID_LEAVE,
    HR_ABSENT: constants.ATT_ABSENT,
    HR_RD: constants.ATT_RD,
    HR_SH: constants.ATT_SH,
    HR_RH: constants.ATT_RH,
    HR_OVERBREAK: constants.ATT_OVERBREAK,
    HR_UH: constants.ATT_UH,
    HR_TARDY: constants.ATT_TARDY,
    HR_RH_NT: constants.ATT_RH_NT,
    HR_RH_OT: constants.ATT_RH_OT,
    HR_RH_NT_OT: constants.ATT_RH_NT_OT,
    HR_RH_SH: constants.ATT_RH_SH,
    HR_SH_NT: constants.ATT_SH_NT,
    HR_SH_OT: constants.ATT_SH_OT,
    HR_SH_NT_OT: constants.ATT_SH_NT_OT,
    HR_2RH: constants.ATT_2RH,
    HR_2RH_NT: constants.ATT_2RH_NT,
    HR_2RH_OT: constants.ATT_2RH_OT,
    HR_2RH_NT_OT: constants.ATT_2RH_NT_OT,
    HR_RD_SH: constants.ATT_RD_SH,
    HR_RD_RH: constants.ATT_RD_RH,
    HR_RD_2RH: constants.ATT_RD_2RH,
    HR_RD_NT: constants.ATT_RD_NT,
    HR_RD_SH_NT: constants.ATT_RD_SH_NT,
    HR_RD_RH_NT: constants.ATT_RD_RH_NT,
    HR_RD_2RH_NT: constants.ATT_RD_2RH_NT,
    HR_RD_OT: constants.ATT_RD_OT,
    HR_RD_SH_OT: constants.ATT_RD_SH_OT,
    HR_RD_RH_OT: constants.ATT_RD_RH_OT,
    HR_RD_2RH_OT: constants.ATT_RD_2RH_OT,
    HR_RD_NT_OT: constants.ATT_RD_NT_OT,
    HR_RD_SH_NT_OT: constants.ATT_RD_SH_NT_OT,
    HR_RD_RH_NT_OT: constants.ATT_RD_RH_NT_OT,
    HR_RD_2RH_NT_OT: constants.ATT_RD_2RH_NT_OT,
}

# Timesheet type (eg. clock in/clock out or hours worked)
AT_CLOCKS = "clocks"
AT_HOURS = "hours"

# NT Time Shift Type
NT_COUNTER_PER_HOUR = "Counter per hour"
NT_CARRY_OVER = "Carry over"