"""
Utility functions for attendance computation
"""

import calendar

from datetime import datetime
from datetime import timedelta
from datetime import date
import time


def get_day_name(_date, _format="%Y-%m-%d"):
    """Extracts a given date's day name
    """
    date_obj = datetime.strptime(_date, _format)
    return calendar.day_name[date_obj.weekday()].lower()


def get_date_from_timestamp(ts, tzinfo=None):
    """Return date string from timestamp"""
    return datetime.fromtimestamp(ts, ).strftime("%Y-%m-%d")


def datetime_to_secs(inp, _format="%Y-%m-%d %H:%M"):
    """convert datetime to seconds"""
    date_time = datetime.strptime(inp, _format)
    date_ts = time.mktime(date_time.timetuple())
    return int(date_ts)

def datetime_to_timestamp(date_str, time_str, _format="%Y-%m-%d %H:%M:%S"):
    """convert datetime to seconds"""
    date_time_str = str(date_str) + ' ' + str(time_str)
    date_time = datetime.strptime(date_time_str, _format)
    date_ts = datetime.timestamp(date_time)
    return int(date_ts)

def datetime_to_mins(date_time, _format="%Y-%m-%d %H:%M"):
    """Converts a datetime into minutes

    @param{str} `date_time` The date time string
    @{param{str} `_format` The date time format.

    @return{int}
    """
    _date_time = datetime.strptime(date_time, _format)
    _date_ts = time.mktime(_date_time.timetuple())
    return int(_date_ts) / 60


def datetime_range_to_mins(start_datetime, end_datetime, _format="%Y-%m-%d %H:%M"):
    """Converts a datetime or datetime range into minutes.

    @param{str} `start_datetime` Starting datetime
    @param{str} `end_datetime` Ending datetime
    @parma{str} `_format` Date time format. Default is %Y-%m-%d %H:%M

    @return{int}
    """
    start_date_ts = datetime_to_mins(start_datetime, _format)
    end_date_ts = datetime_to_mins(end_datetime, _format)

    return int(end_date_ts - start_date_ts)

# def get_min_diff(start_time, end_time):
#     """Get the minute difference between end_time and start_time which are
#     strings in 'hh:mm' 24 hour time format.
#     """

#     hr1, min1 = start_time.split(':')
#     hr2, min2 = end_time.split(':')

#     hr1, min1, hr2, min2 = int(hr1), int(min1), int(hr2), int(min2)

#     if hr2 < hr1:
#         hr2 = hr2 + 24

#     tm1 = (hr1 * 60) + min1
#     tm2 = (hr2 * 60) + min2
#     return tm2 - tm1


def get_span_range(start: str, end: str)->(int, int):
    """Returns i,j values for use in a Span"""
    ival = convert_to_mins(start)
    jval = convert_to_mins(end)
    if jval < ival:
        jval = convert_to_mins(end, hr_offset=24)
    return ival, jval


def convert_to_mins(time_val, hr_offset=0) -> int:
    """Convert a duration notation to minutes"""
    if not time_val:
        return 0

    time_arr = time_val.split(":")
    if len(time_arr) == 2:
        hr1, min1 = time_val.split(':')
    elif len(time_arr) == 3:
        hr1, min1, _ = time_val.split(':')

    hr1, min1 = int(hr1) + hr_offset, int(min1)
    return (hr1 * 60) + min1

def convert_to_sec(time_val, hr_offset=0) -> int:
    """Convert a duration notation to minutes"""
    if not time_val:
        return 0
    hr1, min1 = time_val.split(':')
    hr1, min1 = int(hr1) + hr_offset, int(min1)
    return (hr1 * 3600) + min1

def strip_seconds(time_val):
    """Strip seconds from time string"""
    if isinstance(time_val, str) and time_val.count(":"):
        hour, _min, _ = time_val.split(":")
        return "%s:%s" % (hour, _min)
    elif isinstance(time_val, float):
        return int(time_val)
    else:
        return time_val

def get_time_difference(time1, time2):
    """Return difference of time2-time1 """
    t1 = datetime.strptime(time1, "%H:%M")
    t2 = datetime.strptime(time2, "%H:%M")
    td = t2 - t1
    return strip_seconds(str(td))

def add_days_from_date(start_date, days):
    """This method adds days from a given date

    Args:
        start_date (str) -- The starting date to add days
        days (int) -- The number of days to add
    Return:
        (str) -- The date that's two days ahead of start_date
    """
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = start_date + timedelta(days=days)
    return end_date.strftime("%Y-%m-%d")

def get_date_range(start_date, days, op="add"):
    """This method adds days from a given date

    Args:
        start_date (str) -- The start date of the range
        end_date (str) -- The end date of the list
        days_to_add (int) -- The number of days to retrieve based
            on `start_date`
    Return:
        (list) -- The date range
    """
    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    add = lambda s, d: datetime.strftime(s + timedelta(days=d), "%Y-%m-%d")
    sub = lambda s, d: datetime.strftime(s - timedelta(days=d), "%Y-%m-%d")

    if op == "sub":
        return [sub(start_date, d) for d in range(0, days + 1)]
    else:
        return [add(start_date, d) for d in range(0, days + 1)]

def get_dates_from_date_range(start_date, end_date):
    """This method generate dates between two dates

    Args:
        start_date (str) -- The start date of the range
        end_date (str) -- The end date of the range
        days_to_add (int) -- The number of days to retrieve based
            on `start_date`
    Return:
        (list) -- Individual dates
    """
    individual_dates = []
    start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
    end_date = datetime.strptime(end_date, '%Y-%m-%d').date() + timedelta(days=2)
    individual_dates = daterange(start_date, end_date)
    return individual_dates

def daterange(date1, date2):
    for n in range(int ((date2 - date1).days)+1):
        yield date1 + timedelta(n)

def mins_to_readable_hours(mins):
    """This method converts a mins time in integer
    to a human readable time in HH:MM

    Args:
        mins (integer) -- The time in minutes
    Returns
        string of the minutes (integer) into human
        readable time. HH:MM
    """
    return "%02d:%02d" % (int(mins / 60), mins % 60)


def get_date_today(fmt=None):
    """Use this to easily get the current date

    Returns:
        Datetime object
    """
    now = datetime.now()
    if fmt:
        now = datetime.strftime(datetime.now(), fmt)
    return now

def get_end_datetime(start_date, start_time, end_time, _format="%Y-%m-%d %H:%M"):
    """Use this to easily get end time

    Args:
        start_date (str) --  The start datetime
        start_time (str) --  The start time
        end_time (str) --  The end time
    Returns:
        Datetime object
    """
    start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
    start_time = datetime.strptime(start_time, '%H:%M').time()
    end_time = datetime.strptime(end_time, '%H:%M').time()
    end_date = start_date;

    if start_time > end_time:
        end_date = start_date + timedelta(days=1)

    return datetime.strftime(datetime.combine(end_date, end_time), _format)

def get_datetime_mins_difference(start_datetime, end_datetime):
    """Return difference of end_datetime-start_datetime """
    start = datetime.strptime(start_datetime, '%Y-%m-%d %H:%M')
    end = datetime.strptime(end_datetime, '%Y-%m-%d %H:%M')

    difference = abs(end - start)
    difference_in_mins = difference.total_seconds()/60

    return difference_in_mins

def parse_string_date(date):
    return datetime.strptime(date, '%Y-%m-%d').date()

def get_datetime_from_timestamp(ts, tzinfo=None):
    """Return datetime string from timestamp"""
    return datetime.fromtimestamp(ts, ).strftime("%Y-%m-%d %H:%M")

def is_datetime_between(dt, start, end):
    dt = datetime.strptime(dt, '%Y-%m-%d %H:%M')
    start = datetime.strptime(start, '%Y-%m-%d %H:%M')
    end = datetime.strptime(end, '%Y-%m-%d %H:%M')

    return start <= dt <= end

def is_times_carry_over_to_next_date(time1, time2, shift_date):
    # Get the current date and time
    shift_date = datetime.strptime(shift_date, "%Y-%m-%d").date()
    next_date = shift_date + timedelta(days=1)

    time1 = datetime.strptime(time1, "%H:%M:%S").time()
    time2 = datetime.strptime(time2, "%H:%M:%S").time()

    # Combine the current date with the provided times
    datetime1 = datetime.combine(shift_date, time1)
    datetime2 = datetime.combine(shift_date, time2)
    # Check if the second datetime is earlier than the first datetime
    if datetime2 < datetime1:
        return {
            'time1': datetime1.strftime("%Y-%m-%d %H:%M:%S"),
            'time2': datetime.combine(next_date, time2).strftime("%Y-%m-%d %H:%M:%S")
        }
    else:
        return False
