"""
Utility functions for attendance computation
"""


# def get_min_diff(start_time, end_time):
#     """Get the minute difference between end_time and start_time which are
#     strings in 'hh:mm' 24 hour time format.
#     """

#     hr1, min1 = start_time.split(':')
#     hr2, min2 = end_time.split(':')

#     hr1, min1, hr2, min2 = int(hr1), int(min1), int(hr2), int(min2)

#     if hr2 < hr1:
#         hr2 = hr2 + 24

#     tm1 = (hr1 * 60) + min1
#     tm2 = (hr2 * 60) + min2
#     return tm2 - tm1


def get_span_range(start: str, end: str)->(int, int):
    """Returns i,j values for use in a Span"""
    ival = convert_to_mins(start)
    jval = convert_to_mins(end)
    if jval < ival:
        jval = convert_to_mins(end, hr_offset=24)
    return ival, jval


def convert_to_mins(time, hr_offset=0) -> int:
    """Convert a duration notation to minutes"""
    if not time:
        return 0
    hr1, min1 = time.split(':')
    hr1, min1 = int(hr1) + hr_offset, int(min1)
    return (hr1 * 60) + min1


# def deduct_breaks(stype, min_diff, breaks: list) -> int:
#     """Deducts break minutes"""
#     if stype == 'fixed':
#         for brk in breaks:
#             dur = convert_to_mins(brk.get("break_hours"))
#             min_diff = min_diff - dur

#         return min_diff

#     return min_diff
