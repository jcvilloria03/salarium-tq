"""Exceptions"""
import json

class Error(Exception):
    """Base class for other exceptions"""

class TaskError(Exception):
    """Base class for Task related errors"""

class DataNotFoundError(Exception):
    """Missing data exception"""

class PeerError(Exception):
    """For API call errors that are Internal Server related"""

class RequestError(Exception):
    """For API calls that are request related"""


class UnexpectedError(Exception):
    """Unexpected errors"""

class DateRangeError(Exception):
    """Raised when supplied dates are invalid"""

class RowError(Exception):
    """Row Error"""

    def __init__(self, message, row):
        super(RowError, self).__init__(message)
        self.message = message
        self.row = row

    def __str__(self):
        return json.dumps({"error": self.message, "row":self.row})

class NoAttendanceRecordsError(Exception):
    """No Attendance Records found error"""
