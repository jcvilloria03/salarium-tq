"""Config"""
import os
import yaml

from dotenv import load_dotenv, find_dotenv

def find_env_vars():
    """Load global env vars"""
    load_dotenv(find_dotenv())

def getenv(key):
    """
    Get global env variables
    """
    return os.environ.get(key)


# def yaml_load():
#     """
#     Safely load the yaml file
#     """
#     conf = None
#     try:
#         conf = yaml.safe_load(open("config/api.yaml", "r"))
#     except yaml.YAMLError as e:
#         print("Error loading YAML", e)

#     return conf


#find_env_vars()
