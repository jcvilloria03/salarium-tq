"""Warnings"""

class RecordLockWarning(Warning):
    """This exception is called if a transaction is
    trying to update locked attendance record"""
    def __init__(self, message=None):
        self.message = message

class DeadlockWarning(Warning):
    """This exception is called if identical transactions are
    trying to alter a single record simultaneously."""
    def __init__(self, message=None):
        self.message = message
