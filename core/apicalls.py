import logging
import os

import celeryconfig
import requests

from core import exceptions

logger = logging.getLogger(__name__)

if os.environ.get("TQ_DEBUG", "").upper() == "TRUE":
    logger.setLevel(logging.DEBUG)

def get_api_url(api_id):
    return celeryconfig.APIS.get(api_id)

def patch_resource(url, params=None, headers=None):
    """Patch resource"""
    try:
        response = requests.patch(url, json={"data": params}, headers=headers, timeout=(5, 180))
        logger.debug("PATCH URL: %s", url)
        #logger.debug("Payload: %s", {"data": params})
        response.raise_for_status()
        #logger.debug("Response: %s ", response.json())
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}:{}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))


def get_resource(url, params=None, headers=None):
    """Get Resource"""
    try:
        response = requests.get(url, params=params, headers=headers, timeout=(5, 180))
        logger.debug("GET URL: %s", url)
        #logger.debug("Params: %s", params)
        response.raise_for_status()
        #logger.debug("Response: %s ", response.json())
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def get_resource_temp(url, params=None, headers=None):
    """This func to use when dealing with an incorrect 406 usage"""
    try:
        response = requests.get(url, params=params, headers=headers, timeout=(5, 180))
        logger.debug("GET URL: %s", url)
        #logger.debug("Params: %s", params)
        response.raise_for_status()
        #logger.debug("Response: %s ", response.json())
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == 406:
            return None

        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('failed getting resource') from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def post_resource(url, params=None, headers=None):
    """This is a generic method that executes POST http calls """
    try:
        logger.debug("POST URL: %s", url)
        #logger.debug("Params: %s", params)
        response = requests.post(url, json=params, headers=headers, timeout=(5, 180))
        #response = requests.post(url, json={"data" : params}, headers=headers)
        #logger.debug("Response: %s ", response.json())
        response.raise_for_status()
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        #DEBUG
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def post_resource_new(url, params=None, headers=None):
    """This is a generic method that executes POST http calls """
    try:
        logger.debug("POST URL: %s", url)
        #logger.debug("Params: %s", params)
        #response = requests.post(url, json=params, headers=headers)
        response = requests.post(url, json={"data" : params}, headers=headers, timeout=(5, 180))
        #logger.debug("Response: %s ", response.json())
        response.raise_for_status()
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        #DEBUG
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))

def put_resource(url, params=None, headers=None):
    """This is a generic method that executes POST http calls """
    try:
        logger.debug("PUT URL: %s", url)
        #logger.debug("Params: %s", params)
        response = requests.put(url, json=params, headers=headers, timeout=(5, 180))
        #response = requests.post(url, json={"data" : params}, headers=headers)
        #logger.debug(response.text)
        #logger.debug("Response: %s ", response.json())
        response.raise_for_status()
        return {} if not response.text else response.json()
    except requests.Timeout as exc:
        raise exceptions.PeerError('Timeout: Request took too long to complete.') from exc
    except requests.exceptions.HTTPError as e:
        #DEBUG
        resp_code = e.response.status_code
        code = "Error"
        reason = "Internal Server Error"

        if resp_code < 500:
            message = e.response.json()
            code = message.get("status_code") or message.get("Code")
            reason = message.get("message") or message.get("Message")
        if resp_code >= 500:
            raise exceptions.PeerError('{}:{}'.format(code, reason)) from e
        raise exceptions.TaskError('{}: {}'.format(code, reason)) from e
    except Exception as e:
        raise exceptions.UnexpectedError('{}:{}'.format("UnexpectedError", str(e)))
