"""File utils module"""
#TODO: Refactor this and move to core
import boto3
import celeryconfig


def download_file(bucket, key):
    """Download file from S3 using env configured AWS credentials"""
    s3_client = boto3.client('s3',
                             region_name=celeryconfig.AWS_REGION_NAME,
                             aws_access_key_id=celeryconfig.AWS_ACCESS_KEY_ID,
                             aws_secret_access_key=celeryconfig.AWS_SECRET_ACCESS_KEY)

    obj = s3_client.get_object(Bucket=bucket, Key=key)
    body = obj['Body'].read()
    #TODO:handle large files

    return body

def decode(filebytes: bytes) -> str:
    """Try to decode bytes using UTF-8 else Latin-1 else error"""
    try:
        return filebytes.decode("utf-8")
    except UnicodeDecodeError:  #retry using latin-1
        try:
            return filebytes.decode('latin-1')
        except:
            raise ValueError("Invalid file format")
