"""File utils module"""
#TODO: Refactor this and move to core
import boto3
import celeryconfig
from boto3.dynamodb.conditions import Key, Attr
from attendance import (
    dateutils
)
from core import exceptions, logger
logger = logger.get_logger()

class TimeClockModel:

    def __init__(self) -> None:
        self.dynamo_db = boto3.resource(
            celeryconfig.DYNAMODB,
            region_name=celeryconfig.AWS_REGION_NAME,
            aws_access_key_id=celeryconfig.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=celeryconfig.AWS_SECRET_ACCESS_KEY
        )
        self.table = self.dynamo_db.Table(celeryconfig.TIMECLOCK_TABLE_NAME)

    def get_employee_timesheet(self, employee_ids, start, finish):
        """Extract time records from DynamoDB using env configured AWS credentials"""
        return_data = []
        counter = 0
        if all([not employee_ids,not start, not finish]):
            return return_data

        start_ts = dateutils.datetime_to_timestamp(start, celeryconfig.START_TIME)
        end_ts = dateutils.datetime_to_timestamp(finish, celeryconfig.END_TIME)
        end_ts += 86399 # Add 1 more day as buffer for end date
        for employee_id in employee_ids:
            counter = counter + 1
            should_continue = True
            last_evaluated_key = None
            total_emp_timeclock = 0
            while should_continue:
                query_param = {
                        "TableName": celeryconfig.TIMECLOCK_TABLE_NAME,
                        "Select": "ALL_ATTRIBUTES",
                        "KeyConditionExpression": Key('employee_uid').eq(employee_id)
                        & Key("timestamp").between(start_ts, end_ts),
                }
                if last_evaluated_key:
                    query_param['ExclusiveStartKey'] = last_evaluated_key

                response = self.table.query(**query_param)

                if response and response.get("Items"):
                    return_data += self._format_timerecords(response.get("Items"))
                    total_emp_timeclock += len(return_data)
                    last_evaluated_key = response.get('LastEvaluatedKey', None)
                    should_continue = True if last_evaluated_key else False
                else:
                    should_continue = False
            logger.info(
                "Get emp [" + str(employee_id) + "] timeclocks (" + str(start_ts) + ", " + str(end_ts) + ") | " + str(counter) + " of " + str(len(employee_ids)) + " | Total: " + str(total_emp_timeclock)
            )
        data = sorted(return_data, key=lambda x: x.get("timestamp"))
        return data

    def _format_timerecords(self, time_records):
        counter = 0
        for i in time_records:
            time_records[counter]['employee_uid'] = float(i.get('employee_uid', None))
            time_records[counter]['timestamp'] = float(i.get('timestamp', None))
            counter = counter + 1
        return time_records

    def update_timerecord(self, item):
        """Update time record in DynamoDB using env configured AWS credentials"""
        params = {
                "TableName": celeryconfig.TIMECLOCK_TABLE_NAME,
                "Item": self._format_tuple(item)
        }
        self.table.put_item(**params)

    def _format_tuple(self, item):
        """Formats the time record tuple as a dict"""
        item.update({
            'timestamp': int(item['timestamp']),
            'employee_uid': int(item['employee_uid'])
        })
        return item
