"""Logger module"""

import logging
import os
import sys

from celery.utils import log


#sys.tracebacklimit = 0
class LogWrapper:
    """LogWrapper"""
    LOGGER = None

def get_logger():
    """Return wrapped global logger"""
    if LogWrapper.LOGGER is None:
        LogWrapper.LOGGER = log.get_task_logger("SalTQ")
    if os.environ.get("TQ_DEBUG", "").upper() == "TRUE":
        LogWrapper.LOGGER.setLevel(logging.DEBUG)
    else:
        sys.tracebacklimit = 0

    return LogWrapper.LOGGER
