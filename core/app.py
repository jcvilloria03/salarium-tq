"""Main app"""
from __future__ import absolute_import

import os
import time
import logging

#
from celery import Celery
from celery.signals import setup_logging
from celery.app.log import TaskFormatter
from event_consumer.handlers import AMQPRetryConsumerStep
from core import logger

logger = logger.get_logger()

# instantiate celery object

#TODO: REmove this and timezone info should be company specific
os.environ["TZ"] = "Asia/Manila"
time.tzset()

app = Celery('tasks')

# import celery config
app.config_from_object('celeryconfig')

app.steps['consumer'].add(AMQPRetryConsumerStep)

#tasks.attendance.workflows.workflow('a1', 'b1')
app.config_from_object('celeryconfig')
app.conf.update({
    'task_routes' : ([('payroll.tasks.*', {'queue': 'payroll'}),
                      ('payslip.tasks.*', {'queue': 'payroll'})],)

})

@setup_logging.connect
def setup_task_logger(*args, **kwargs):
    """Override Celery logger"""
    logger = logging.getLogger()
    formatter = TaskFormatter(
        '%(asctime)s - %(task_id)s - %(task_name)s - %(name)s - %(levelname)s - %(message)s'
    )
    # StreamHandler

    sh = logging.StreamHandler()
    sh.setFormatter(formatter)
    logger.addHandler(sh)


if __name__ == '__main__':
    try:
        app.start()
    except Exception as err:
        logger.critical(str(err))
        raise err
