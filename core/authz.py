EMPLOYEE_SCOPE_MAPPING = [
    ('company_id', 'COMPANY'),
    ('department_id', 'DEPARTMENT'),
    ('position_id', 'POSITION'),
    ('location_id', 'LOCATION'),
    ('team_id', 'TEAM'),
    ('payroll_group_id', 'PAYROLL_GROUP')
]


def is_employee_authorized(authz_data_scope: dict, employee: dict) -> bool:
    """Check if the given authz data scope has access to the given employee

    Args:
        authz_data_scope (dict) -- Authz data scope
        employee (dict) -- Employee details

    Returns
        bool
    """
    for (scope_key, scope_name) in EMPLOYEE_SCOPE_MAPPING:
        scope_id = employee.get(scope_key, None)
        allowed_ids = authz_data_scope.get(scope_name, None)

        # special data dict format checks specifically to cover
        # for the modules payroll.bonuses and payroll.commissions
        # see commit 503d4d88b19045f5408f233e777da6df59ef451d
        if 'PAYROLL_GROUP' == scope_name and 'payroll_group' in employee \
            and employee['payroll_group'] != None and 'id' in employee['payroll_group']:
                scope_id = employee['payroll_group']['id']

        # make sure the scope_key is existing before proceeding with any checks
        if scope_id and allowed_ids and scope_id not in allowed_ids:
            return False

    return True
