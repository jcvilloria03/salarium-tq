"""fileutils test"""
from unittest import TestCase, mock
from core import fileutils

LATIN_1_CONTENT = bytes("""Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Taxable
228,Pempeña,Therese,,Actual Medical Assistance,416.665,Reduce to Taxable Income """, "latin-1")

UTF_8_CONTENT = bytes("""Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Taxable
228,Pempeña,Therese,,Actual Medical Assistance,416.665,Reduce to Taxable Income """, "utf-8")

class FileUtilsTest(TestCase):
    def test_latin_1_content(self):
        content = fileutils.decode(LATIN_1_CONTENT)
        assert content is not None
        content = fileutils.decode(UTF_8_CONTENT)
        assert content is not None