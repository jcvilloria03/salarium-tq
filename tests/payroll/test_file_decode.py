#pylint: disable=all
from unittest import TestCase
from core import fileutils

TEST_DATA = bytes("""Employee ID,Last Name,First Name,Middle Name,Email,Telephone Number,Mobile Number,Address Line 1,Address Line 2,Country,City,ZIP,Birthdate,Gender
228,Pempeña,Drake ,,payrolltesting01+drakepempena@gmail.com,4425158,(63)9954132491,1st  Street,Brgy 12,Philippines,Marilao,14500,10/10/1984,Male""", "latin-1")

class FileDecodeTest(TestCase):

    def test_decode(self):
        content = fileutils.decode(TEST_DATA)
        assert content is not None
