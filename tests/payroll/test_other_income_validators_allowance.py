"""Payroll task unit tests"""
# pylint: disable=protected-access,line-too-long
from unittest import TestCase
from payroll import filevalidators

ALLOWANCE_DATA_OK = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,No,,No,No,Yes,01/01/2019,
"""

ALLOWANCE_DATA_INVALID_AMOUNT = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST1,Brando,Marlon,Grumble,Clothing Allowance,XXXXX,No,Yes,Per Month,No,,No,No,Yes,01/01/2019,
"""

ALLOWANCE_DATA_MULTIPLE_ERRORS_1 = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST1,Brando,Marlon,Grumble,Clothing Allowance,XXXXX,No,Yes,Per Month,No,,No,No,Yes,01/01/2019,
TEST2,Hello,World,Hi,Clothing Allowance,3000.00,No,Yes,Per Month,No,,No,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Meal Allowance,2000,No,Foo,Per Month,No,,No,No,Yes,01/01/2019,
"""


ALLOWANCE_DATA_MULTIPLE_ERRORS_2 = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",Faaa,Yes,Per Month,No,,No,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Faaa,Per Month,No,,No,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Decade,No,,No,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,Faaa,,No,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",Yes,Yes,Per Month,Yes,Prorated By Decade,No,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",Yes,Yes,Per Month,Yes,Prorated By Hour,Faaa,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",Yes,Yes,Per Month,Yes,Prorated By Day,No,Faa,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",Yes,Yes,Per Month,Yes,Prorated By Day,No,No,Faaaa,01/01/2019,

"""

ALLOWANCE_DATA_MULTIPLE_ERRORS_3 =  """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,No,,No,No,Yes,13/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,No,,No,No,No,12/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,No,,No,No,No,12/01/2019,13/01/2019
"""

ALLOWANCE_DATA_OK_2 = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,Yes,Prorated By Day,Yes,No,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,Yes,Prorated By Day,No,Yes,Yes,01/01/2019,
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,Yes,Per Month,Yes,Prorated By Day,Yes,Yes,Yes,01/01/2019,
"""

ALLOWANCE_DATA_MISSING = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
,Brando,Marlon,Grumble,,,,,,,,,,,,
"""

ALLOWANCE_CREDITING_FREQUENCY_MISSING_NOT_RECURRING = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST1,Brando,Marlon,Grumble,Clothing Allowance,"3,000.00",No,No,,No,,No,No,Yes,01/01/2019,
"""

TEST_CONTEXT_ALLOWANCE = {
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True)
    },
    "typeList": [
        {
            "id": 2,
            "name": "DE_MINIMIS",
            "company_id": 0,
            "fully_taxable": False,
            "max_non_taxable": 90000,
            "type_name": "App\\Model\\PhilippineAllowanceType",
            "deleted_at": None
        },
        {
            "id": 170,
            "name": "Meal Allowance",
            "company_id": 50,
            "fully_taxable": False,
            "max_non_taxable": 90000,
            "type_name": "App\\Model\\PhilippineAllowanceType",
            "deleted_at": None
        },
        {
            "id": 171,
            "name": "Clothing Allowance",
            "company_id": 50,
            "fully_taxable": False,
            "max_non_taxable": 90000,
            "type_name": "App\\Model\\PhilippineAllowanceType",
            "deleted_at": None
        }
    ]
}

class AllowanceFileUploadValidatorTest(TestCase):
    """Allowance file upload tests"""

    def test_ok_scenario_1(self):
        """Ok test"""
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_DATA_OK.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert not validator.has_errors()

    
    def test_ok_scenario_2(self):
        """Ok test"""
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_DATA_OK_2.splitlines()
        validator.iterate_source(data)
        assert not validator.has_errors()


    def test_invalid_amount(self):
        """Invalid amount test"""
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_DATA_INVALID_AMOUNT.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        assert len(validator._errors) == 2  #no valid records and invalid amount format
    
    def test_not_recurring_missing_crediting_frequency_should_be_ok(self):
        """Invalid amount test"""
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_CREDITING_FREQUENCY_MISSING_NOT_RECURRING.splitlines()
        validator.iterate_source(data)
        assert not validator.has_errors()

    def test_multiple_errors_1(self):
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_DATA_MULTIPLE_ERRORS_1.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        assert len(validator._errors) == 4

    def test_multiple_errors_2(self):
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_DATA_MULTIPLE_ERRORS_2.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        assert len(validator._errors) == 9

    def test_multiple_errors_3(self):
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_DATA_MULTIPLE_ERRORS_3.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        assert len(validator._errors) == 4

    def test_missing_data_1(self):
        validator = filevalidators.EmployeeAllowancesFileUploadValidator(TEST_CONTEXT_ALLOWANCE)
        data = ALLOWANCE_DATA_MISSING.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        assert len(validator._errors) == 2
        
        error1 = validator._errors.get("2")[0]
        assert error1.get("name") == "Missing Parameters"
        details = error1.get("details")[0]
        params = details.get("parameters")
        assert "Employee ID" in params
        assert "Allowance Type" in params
        assert "Amount" in params
        assert "Special Pay Run?" in params
        assert "Recurring?" in params
        assert "Valid From?" in params
        

