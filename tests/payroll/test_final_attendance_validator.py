"""Payroll task unit tests"""
# pylint: disable=protected-access
from unittest import TestCase
from payroll import filevalidators

class FinalAttendanceFileValidatorTest(TestCase):
    """Final Attendance Validator tests"""

    def test_validator_validate_with_undertime_and_tardy(self):
        """Tardy and undertime test"""
        validator = filevalidators.FinalAttendanceFileProcessor(
            context={
                "finalPayrollEmployees": {
                    "FakeID": ("1", "Delfin", "Garcia", "Juat", "2019-01-01", "2019-01-30")
                },
            }
        )
        fake_line_index = 2
        fake_data = {
            "Employee ID": "FakeID",
            "Timesheet Date": "01/10/2019",
            "First Name": "Delfin",
            "Middle Name": "Garcia",
            "Last Name": "Juat",
            "Regular": "3:00",
            "Scheduled Hours": "8:00",
            "Undertime": "4:00",
            "Tardy": "1:00"
        }

        validator._validate(fake_line_index, fake_data)
        assert validator._temp_results.get("1")
        assert validator._temp_results.get("1").get("2019-01-10")
        assert validator._temp_results.get("1").get("2019-01-10").get("UNDERTIME")
        assert validator._temp_results.get("1").get("2019-01-10").get("UNDERTIME") == 4

        validator._process()
        assert "incomplete_attendance" in validator._errors
