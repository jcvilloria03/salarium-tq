"""Payroll task unit tests"""
# pylint: disable=protected-access,line-too-long
from unittest import TestCase
from payroll import filevalidators

TEST_CONTEXT_DEDUCTION = {
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True)
    },
    "typeList": [
         {
            "id": 0,
            "name": "13th Month Pay",
            "company_id": 2,
            "fully_taxable": True,
            "max_non_taxable": None,
            "basis": "FIXED",
            "frequency": "ONE_TIME",
            "type_name": "App\\Model\\PhilippineBonusType",
            "deleted_at":None
        },
        {
            "id": 7,
            "name": "14th Month Pay",
            "company_id": 2,
            "fully_taxable": True,
            "max_non_taxable": None,
            "basis": "SALARY_BASED",
            "frequency": "ONE_TIME",
            "type_name": "App\\Model\\PhilippineBonusType",
            "deleted_at":None
        },
        {
            "id": 8,
            "name": "Iggi Bonus",
            "company_id": 2,
            "fully_taxable": True,
            "max_non_taxable": None,
            "basis": "SALARY_BASED",
            "frequency": "PERIODIC",
            "type_name": "App\\Model\\PhilippineBonusType",
            "deleted_at":None
        }
    ]
}

BONUS_DATA_OK_1 = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,14th Month Pay,,Current Basic Salary,100,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

BONUS_DATA_NO_ID = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
,Brando,Marlon,Grumble,14th Month Pay,,Current Basic Salary,100,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

BONUS_DATA_ID_NOT_FOUND = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST2,Brando,Marlon,Grumble,14th Month Pay,,Current Basic Salary,100,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

BONUS_DATA_TYPE_NOT_FOUND = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,15th Month Pay,,Current Basic Salary,100,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

BONUS_DATA_AMT_BASIS_ERROR = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,14th Month Pay,,xxxx,100,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""
BONUS_DATA_INVALID_PCT = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,14th Month Pay,,Current Basic Salary,xxx,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

BONUS_DATA_OK_2 = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,1,06/20/2019,No,01/01/2019,12/31/2019,Disbursed
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,2,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

BONUS_DATA_RELEASE_NUM_NOT_OK = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,1,06/20/2019,No,01/01/2019,12/31/2019,Disbursed
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,1,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

BONUS_DATA_MULTIPLE_ERRORS = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,1,06/20/2019,No,01/01/2019,12/31/2019,Disbursed
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,2,xxxxx,foo,xxxx,xxxx,foo
"""

BONUS_EMPTY ="""
"""

BONUS_DATA_INVALID_COVERAGE = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,1,06/20/2019,No,01/01/2019,12/31/2019,Disbursed
TEST1,Brando,Marlon,Grumble,Iggi Bonus,,Current Basic Salary,100,2,12/20/2019,No,7/1/2019,1/15/2019,Not yet released
"""

# Basic Pay based
BONUS_DATA_OK_3 = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,14th Month Pay,,Basic Pay,100,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

# Basic Pay based
BONUS_DATA_OK_4 = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,13th Month Pay,2000,,,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""
class BonusFileUploadValidatorTest(TestCase):

    def test_bonus_validator_ok_1(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_OK_1.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert not validator.has_errors()

    def test_bonus_validator_id_not_found(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_ID_NOT_FOUND.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-200"

    def test_bonus_validator_type_not_found(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_TYPE_NOT_FOUND.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors.get("2")) == 1
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-203"

    def test_bonus_validator_amt_basis_not_found(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_AMT_BASIS_ERROR.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors.get("2")) == 1
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-102"

    def test_bonus_validator_pct_invalid(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_INVALID_PCT.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors.get("2")) == 1
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-102"

    def test_release_number_ok(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_OK_2.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert not validator.has_errors()

    def test_release_number_not_ok(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_RELEASE_NUM_NOT_OK.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors.get("3")) == 1
        error2 = validator._errors.get("3")[0]
        assert error2.get("code") == "VE-203"
    
    def test_multiple_errors(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_MULTIPLE_ERRORS.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors.get("3")) == 5

    def test_bonus_validator_empty(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_NO_ID.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert  validator.has_errors()

    def test_bonus_validator_invalid_coverage(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_INVALID_COVERAGE.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()

    def test_bonus_validator_ok_3(self):
        #test for Basic Pay amount basis
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_OK_3.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert not validator.has_errors()

    def test_bonus_validator_ok_4(self):
        validator = filevalidators.EmployeeBonusFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = BONUS_DATA_OK_4.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        print(validator._result)
        assert not validator.has_errors()

