"""Payroll task unit tests"""
# pylint: disable=protected-access,line-too-long,missing-docstring
from unittest import TestCase
from payroll import filevalidators
TEST_CONTEXT = {
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True)
    },
    "companyId": 123
}

ANNUAL_EARNING_OK_MIN = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_INVALID_VAL_1 = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,xxxx,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_TIN_NO_BUSINESS_NAME = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,123456789000,,,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_BUSINESS_NAME_NO_TIN = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,123456789000,,,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_NO_EMP_ID = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_WITH_PREV_EMPLOYER_OK = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,123-456-789,Acme Corp,01/01/2019,06/30/2019,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_WITH_PREV_EMPLOYER_NO_DATES = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,123-456-789,Acme Corp,,,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_WITH_PREV_EMPLOYER_INCORRECT_DATES = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,123-456-789,Acme Corp,xxx,abv,,,,,,,,,,,,,,,,,
'''

ANNUAL_EARNING_WITH_PREV_EMPLOYER_INCORRECT_DATES_2 = '''Employee ID,Last Name,First Name,Middle Name,Year,Basic Salary/Statutory Minimum Wage (MWE),Holiday Pay (MWE),Overtime Pay (MWE),Night Shift Differential (MWE),De Minimis Benefits,Employee SSS Contribution,Employee PhilHealth Contribution,Employee Pag-IBIG Contribution,Non-Taxable Salaries & Other Forms of Compensation,13th Month Pay and Other Benefits,Basic Salary,Others Supplementary,Others Regular,Taxable Commission,Taxable Overtime Pay,Premium Paid on Health and Hospital Insurance,Tax Withheld,Previous Employer TIN,Previous Employer Business Name,Previous Employer From Period,Previous Employer To Period,Previous Employer Business Address,Previous Employer ZIP Code,Previous Employer Gross Compensation Income,Previous Employer Basic Salary/Statutory Minimum Wage (MWE),Previous Employer Holiday Pay (MWE),Previous Employer Overtime Pay (MWE),Previous Employer Night Shift Differential (MWE),Previous Employer Non-Taxable 13th Month Pay & Other Benefits,Previous Employer De Minimis Benefits,"Previous Employer SSS, GSIS, PHIC & Pag-IBIG Employee Contribution",Previous Employer Non-Taxable Salaries & Other Forms of Compensation,Previous Employer Total Non-Taxable Compensation Income,Previous Employer Taxable 13th Month Pay & Other Benefits,Previous Employer Taxable Salaries & Other Forms of Compensation,Previous Employer Total Taxable Compensation Income,Previous Employer Premium Paid on Health and Hospital Insurance,Previous Employer Tax Withheld
TEST1,Brando,Marlon,Grumble,2019,,,,,,,,,,,,,,,,,,123-456-789,Acme Corp,06/30/2019,01/01/2019,,,,,,,,,,,,,,,,,
'''
class AnnualEarningFileUploadValidatorTest(TestCase):

    def test_ok_1(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_OK_MIN.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert not validator.has_errors()

    def test_invalid_numeric_value_1(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_INVALID_VAL_1.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        error2 = validator._errors.get("2")[0]
        assert error2.get("details", [])[0].get("parameters")[0] == 'Basic Salary/Statutory Minimum Wage (MWE)'
        assert error2.get("code") == "VE-102"

    def test_invalid_tin_no_business_name(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_TIN_NO_BUSINESS_NAME.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-203"

    def test_invalid_business_name_no_tin(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_BUSINESS_NAME_NO_TIN.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-203"

    def test_no_emp_id(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_NO_EMP_ID.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()

    def test_emp_with_prev_emp(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_WITH_PREV_EMPLOYER_OK.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        print(validator._result)
        assert not validator.has_errors()
        assert validator._result[0]['prev_date_from'] == '2019-01-01'
        assert validator._result[0]['prev_date_to'] == '2019-06-30'

    def test_emp_with_prev_emp_nodates(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_WITH_PREV_EMPLOYER_NO_DATES.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert validator._errors.get('2')[0].get('code') == 'VE-100'

    def test_emp_with_prev_emp_incorrect_date_1(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_WITH_PREV_EMPLOYER_INCORRECT_DATES.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert validator._errors.get('2')[0].get('code') == 'VE-101'

    def test_emp_with_prev_emp_incorrect_date_2(self):
        validator = filevalidators.EmployeeAnnualEarningFileUploadValidator(TEST_CONTEXT)
        data = ANNUAL_EARNING_WITH_PREV_EMPLOYER_INCORRECT_DATES_2.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert validator.has_errors()
        assert validator._errors.get('2')[0].get('code') == 'VE-203'
