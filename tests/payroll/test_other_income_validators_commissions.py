"""Payroll task unit tests"""
# pylint: disable=all
from unittest import TestCase
from payroll import filevalidators

TEST_CONTEXT_COMMISSION = {
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True)
    },
    "typeList": [
        {
            "id": 2,
            "name": "Commission 1",
        },
        {
            "id": 170,
            "name": "Commission 2",
        },
    ]
}

COMMISSION_DATA_OK = """Employee ID,Last Name,First Name,Middle Name,Commission Type,Amount,Release Date,Special Pay Run?
TEST1,Brando,Marlon,Grumble,Commission 1,500,01/01/2019,No
TEST1,Brando,Marlon,Grumble,Commission 2,500,01/01/2019,Yes
"""

COMMISSION_MISSING_KEYS = """Employee ID,Last Name,First Name,Middle Name,Commission Type,Amount,Release Date,Special Pay Run?
,Brando,Marlon,Grumble,,,,
"""

COMMISSION_LINE_ERRORS = """Employee ID,Last Name,First Name,Middle Name,Commission Type,Amount,Release Date,Special Pay Run?
TEST2,Brando,Marlon,Grumble,Commission 1,500,01/01/2019,No
TEST1,Brando,Marlon,Grumble,Commission 3,500,01/01/2019,No
TEST1,Brando,Marlon,Grumble,Commission 1,xxxx,01/01/2019,No
TEST1,Brando,Marlon,Grumble,Commission 1,500,13/01/2019,No
TEST1,Brando,Marlon,Grumble,Commission 1,500,01/01/2019,Faaa
TEST1,Brando,Marlon,Grumble,Commission 1,0,01/01/2019,No
"""

COMMISSION_INVALID_RELEASE_DATE = """Employee ID,Last Name,First Name,Middle Name,Commission Type,Amount,Release Date,Special Pay Run?
TEST1,Brando,Marlon,Grumble,Commission 1,500,1-Aug-19,No
"""

COMMISSION_INVALID_HEADERS = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Amount Basis,Percentage of Monthly Salary,Release Number,Release Date,Disburse through Special Pay Run?,Coverage From,Coverage To,Status
TEST1,Brando,Marlon,Grumble,14th Month Pay,,Current Basic Salary,100,,12/20/2019,No,01/01/2019,12/31/2019,Not yet released
"""

class CommissionFileUploadValidatorTest(TestCase):

    def test_ok_1(self):
        validator = filevalidators.EmployeeCommissionsFileUploadValidator(TEST_CONTEXT_COMMISSION)
        data = COMMISSION_DATA_OK.splitlines()
        print(data)
        validator.iterate_source(data)
        print(validator._errors)
        assert not validator.has_errors()

    def test_missing_keys(self):
        validator = filevalidators.EmployeeCommissionsFileUploadValidator(TEST_CONTEXT_COMMISSION)
        data = COMMISSION_MISSING_KEYS.splitlines()
        validator.iterate_source(data)
        print(validator._errors)

        assert validator.has_errors()
        assert len(validator._errors) == 2     
        error1 = validator._errors.get("2")[0]
        assert error1.get("name") == "Missing Parameters"
        details = error1.get("details")[0]
        params = details.get("parameters")
        assert "Employee ID" in params
        assert "Commission Type" in params
        assert "Amount" in params
        assert "Release Date" in params
        assert "Special Pay Run?" in params      

    def test_error_lines(self):
        validator = filevalidators.EmployeeCommissionsFileUploadValidator(TEST_CONTEXT_COMMISSION)
        data = COMMISSION_LINE_ERRORS.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        print(validator._errors)
        assert len(validator._errors) == 7
        error7 = validator._errors.get("7")[0]
        assert error7.get("code") == "VE-102"
        assert error7.get("details")[0].get("expectedValue").get("min") == 1
    
    def test_invalid_release_date(self):
        validator = filevalidators.EmployeeCommissionsFileUploadValidator(TEST_CONTEXT_COMMISSION)
        data = COMMISSION_INVALID_RELEASE_DATE.splitlines()
        print(data)
        validator.iterate_source(data)
        print(validator._errors)
        assert len(validator._errors) == 2

    def test_invalid_invalid_headers(self):
        validator = filevalidators.EmployeeCommissionsFileUploadValidator(TEST_CONTEXT_COMMISSION)
        data = COMMISSION_INVALID_HEADERS.splitlines()
        validator.iterate_source(data)
        print(validator._errors)
        assert len(validator._errors) == 1
