"""FOr testing"""
import json

def get_from_file(filename):
    """Get data from file"""
    data = None
    with open(filename) as json_file:
        data = json.load(json_file)

    return data

MOCK_EMPLOYEES_DATA_WITH_PAYROLL = get_from_file(
    "./tests/payroll/mockdata/emp_data_with_payroll.json")


MOCK_PAYROLL_GROUP_EMPLOYEES1 = get_from_file(
    "./tests/payroll/mockdata/pg_data_sample1.json"
)