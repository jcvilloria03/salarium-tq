"""Payroll task unit tests"""
# pylint: disable=protected-access,line-too-long
from unittest import TestCase
from payroll import filevalidators

TEST_CONTEXT_DEDUCTION = {
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True)
    },
    "typeList": [
        {
            "id": 2,
            "name": "Wellness",
        },
        {
            "id": 170,
            "name": "Overpaid",
        },
    ]
}

DEDUCTION_DATA_OK = """Employee ID,Last Name,First Name,Middle Name,Deduction Type,Amount Deducted,Recurring?,Frequency,Start Date,Valid To
TEST1,Brando,Marlon,Grumble,Wellness,500,Yes,Every pay of the month,01/01/2019,
TEST1,Brando,Marlon,Grumble,Overpaid,500,No,Every pay of the month,01/01/2019,02/01/2019
"""

DEDUCTION_MISSING_KEYS = """Employee ID,Last Name,First Name,Middle Name,Deduction Type,Amount Deducted,Recurring?,Frequency,Start Date,Valid To
,Brando,Marlon,Grumble,,,,,,
"""

DEDUCTION_LINE_ERRORS = """Employee ID,Last Name,First Name,Middle Name,Deduction Type,Amount Deducted,Recurring?,Frequency,Start Date,Valid To
TEST2,Brando,Marlon,Grumble,Wellness,500,Yes,Every pay of the month,01/01/2019,
TEST1,Brando,Marlon,Grumble,errrr,500,No,Every pay of the month,01/01/2019,02/01/2019
TEST1,Brando,Marlon,Grumble,Wellness,500,Yes,Every pay of the month,13/01/2019,
TEST1,Brando,Marlon,Grumble,Wellness,500,No,Every pay of the month,01/01/2019,13/01/2019
TEST1,Brando,Marlon,Grumble,Wellness,500,No,Every pay of the month,13/01/2019,
TEST1,Brando,Marlon,Grumble,Wellness,500,Faa,Every pay of the month,01/01/2019,
TEST1,Brando,Marlon,Grumble,Wellness,xxxx,Yes,Every pay of the month,01/01/2019,
TEST1,Brando,Marlon,Grumble,Wellness,500,Yes,Every pay of the decade,01/01/2019,
"""

DEDUCTION_MISSING_FREQ = """Employee ID,Last Name,First Name,Middle Name,Deduction Type,Amount Deducted,Recurring?,Frequency,Start Date,Valid To
TEST1,Brando,Marlon,Grumble,Wellness,500,Yes,,01/01/2019,
"""

DEDUCTION_INVALID_DATE_TO = """Employee ID,Last Name,First Name,Middle Name,Deduction Type,Amount Deducted,Recurring?,Frequency,Start Date,Valid To
TEST1,Brando,Marlon,Grumble,Wellness,500,Yes,Every pay of the month,01/01/2019,01/01/2018
"""

class DeductionFileUploadValidatorTest(TestCase):

    def test_ok_1(self):
        validator = filevalidators.EmployeeDeductionsFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = DEDUCTION_DATA_OK.splitlines()
        print(data)
        validator.iterate_source(data)
        print(validator._errors)
        assert not validator.has_errors()

    def test_missing_keys(self):
        validator = filevalidators.EmployeeDeductionsFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = DEDUCTION_MISSING_KEYS.splitlines()
        validator.iterate_source(data)
        print(validator._errors)

        assert validator.has_errors()
        assert len(validator._errors) == 2
        
        error1 = validator._errors.get("2")[0]
        assert error1.get("name") == "Missing Parameters"
        details = error1.get("details")[0]
        params = details.get("parameters")
        assert "Employee ID" in params
        assert "Deduction Type" in params
        assert "Amount Deducted" in params
        assert "Recurring?" in params
        assert "Frequency" not in params
        assert "Start Date" in params
        assert "Valid To" not in params
    

    def test_error_lines(self):
        validator = filevalidators.EmployeeDeductionsFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = DEDUCTION_LINE_ERRORS.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        print(validator._errors)
        assert len(validator._errors) == 9

    def test_missing_frequency(self):
        validator = filevalidators.EmployeeDeductionsFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = DEDUCTION_MISSING_FREQ.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        print(validator._errors)
        assert len(validator._errors) == 2
        #error in line 2
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-100"
        assert error2.get("details")[0].get("parameters")[0] == 'Frequency'

    def test_invalid_date_to(self):
        validator = filevalidators.EmployeeDeductionsFileUploadValidator(TEST_CONTEXT_DEDUCTION)
        data = DEDUCTION_INVALID_DATE_TO.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        print(validator._errors)
        assert len(validator._errors) == 2
        #error in line 2
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-203"
        assert error2.get("details")[0] == "The valid to date must be a date same or after valid from date."


