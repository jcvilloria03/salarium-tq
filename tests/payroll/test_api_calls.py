"""Payroll task tests"""
from unittest import TestCase, mock
from payroll import apicalls

from tests.payroll.mockdata import company_employees_data1

MOCK_COMPANY_EMPLOYEES = {
    "data": [
        {
            "id": 14743,
            "account_id": 524,
            "company_id": 711,
            "employee_id": "2608_001",
            "first_name": "Tywin",
            "middle_name": "",
            "last_name": "Lannister",
            "full_name": "Tywin Lannister",
            "email": "Salqateam01+2608_001@gmail.com",
            "gender": "male",
            "birth_date": "1984-10-15",
            "telephone_number": 4425161,
            "mobile_number": "(63)9954132494",
            "hours_per_day": None,
            "date_hired": "2010-10-16",
            "date_ended": None,
            "location_id": 631,
            "location_name": "salqajeff_location",
            "department_id": 1391,
            "department_name": "Department Parent1556085724",
            "rank_id": 1134,
            "rank_name": "Junior. Quality Assurance Analyst",
            "position_id": 10660,
            "position_name": "QA",
            "address": {
                "address_line_1": "1st  Street",
                "address_line_2": "Brgy 12",
                "city": "Baliuag",
                "zip_code": "14501",
                "country": "Philippines"
            },
            "active": "active",
            "non_taxed_other_income_limit": "90000.0000",
            "user_id": 18775,
            "payroll_group": {
                "id": 5621,
                "name": "2608_SPR&RPR",
                "company_id": 711,
                "payroll_frequency": "SEMI_MONTHLY",
                "day_factor": "260.00",
                "hours_per_day": "8.00",
                "non_working_day_option": "BEFORE",
                "pay_date": "2019-11-15 00:00:00",
                "pay_run_posting": "2019-11-10 00:00:00",
                "cut_off_date": "2019-11-10 00:00:00",
                "pay_date_2": "2019-11-30 00:00:00",
                "pay_run_posting_2": "2019-11-25 00:00:00",
                "cut_off_date_2": "2019-11-25 00:00:00",
                "fpd_end_of_month": False,
                "fpp_end_of_month": False,
                "fcd_end_of_month": False,
                "spd_end_of_month": True,
                "spp_end_of_month": False,
                "scd_end_of_month": False,
                "payroll_group_subtype_id": 5621,
                "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
                "compute_overbreak": False,
                "compute_undertime": True,
                "compute_tardiness": True,
                "created_at": "2019-10-10 13:20:23",
                "updated_at": "2019-10-10 13:20:23",
                "deleted_at": None,
                "enforce_gap_loans": True,
                "minimum_net_take_home_pay": "2000.0000"
            },
            "updated_at": {
                "date": "2019-11-15 15:09:27.000000",
                "timezone_type": 3,
                "timezone": "Asia/Manila"
            },
            "termination_information": None,
            "team_id": None
        },
        {
            "id": 14744,
            "account_id": 524,
            "company_id": 711,
            "employee_id": "2608_002",
            "first_name": "Joan",
            "middle_name": "",
            "last_name": "Castamere",
            "full_name": "Joan Castamere",
            "email": "Salqateam01+2608_002@gmail.com",
            "gender": "male",
            "birth_date": "1984-10-15",
            "telephone_number": 4425161,
            "mobile_number": "(63)9954132494",
            "hours_per_day": None,
            "date_hired": "2010-10-16",
            "date_ended": None,
            "location_id": 631,
            "location_name": "salqajeff_location",
            "department_id": 1391,
            "department_name": "Department Parent1556085724",
            "rank_id": 1134,
            "rank_name": "Junior. Quality Assurance Analyst",
            "position_id": 10660,
            "position_name": "QA",
            "address": {
                "address_line_1": "1st  Street",
                "address_line_2": "Brgy 12",
                "city": "Baliuag",
                "zip_code": "14501",
                "country": "Philippines"
            },
            "active": "inactive",
            "non_taxed_other_income_limit": "90000.0000",
            "user_id": 18776,
            "payroll_group": {
                "id": 5621,
                "name": "2608_SPR&RPR",
                "company_id": 711,
                "payroll_frequency": "SEMI_MONTHLY",
                "day_factor": "260.00",
                "hours_per_day": "8.00",
                "non_working_day_option": "BEFORE",
                "pay_date": "2019-11-15 00:00:00",
                "pay_run_posting": "2019-11-10 00:00:00",
                "cut_off_date": "2019-11-10 00:00:00",
                "pay_date_2": "2019-11-30 00:00:00",
                "pay_run_posting_2": "2019-11-25 00:00:00",
                "cut_off_date_2": "2019-11-25 00:00:00",
                "fpd_end_of_month": False,
                "fpp_end_of_month": False,
                "fcd_end_of_month": False,
                "spd_end_of_month": True,
                "spp_end_of_month": False,
                "scd_end_of_month": False,
                "payroll_group_subtype_id": 5621,
                "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
                "compute_overbreak": False,
                "compute_undertime": True,
                "compute_tardiness": True,
                "created_at": "2019-10-10 13:20:23",
                "updated_at": "2019-10-10 13:20:23",
                "deleted_at": None,
                "enforce_gap_loans": True,
                "minimum_net_take_home_pay": "2000.0000"
            },
            "updated_at": {
                "date": "2019-11-15 14:13:52.000000",
                "timezone_type": 3,
                "timezone": "Asia/Manila"
            },
            "termination_information": None,
            "team_id": None
        },
        {
            "id": 19369,
            "account_id": 524,
            "company_id": 711,
            "employee_id": "3371_001",
            "first_name": "Jean",
            "middle_name": "",
            "last_name": "Zena",
            "full_name": "Jean Zena",
            "email": "salqateam01+3371_001@gmail.com",
            "gender": "female",
            "birth_date": "1989-01-01",
            "telephone_number": 9970001,
            "mobile_number": "(63)9170000001",
            "hours_per_day": None,
            "date_hired": "2019-01-01",
            "date_ended": None,
            "location_id": 631,
            "location_name": "salqajeff_location",
            "department_id": 1391,
            "department_name": "Department Parent1556085724",
            "rank_id": 1134,
            "rank_name": "Junior. Quality Assurance Analyst",
            "position_id": 10660,
            "position_name": "QA",
            "address": {
                "address_line_1": "1650 Penafrancia",
                "address_line_2": "",
                "city": "Makati",
                "zip_code": "1208",
                "country": "Philippines"
            },
            "active": "semi-active",
            "non_taxed_other_income_limit": "90000.0000",
            "user_id": 26843,
            "payroll_group": {
                "id": 8778,
                "name": "posting date issue",
                "company_id": 711,
                "payroll_frequency": "SEMI_MONTHLY",
                "day_factor": "260.00",
                "hours_per_day": "8.00",
                "non_working_day_option": "BEFORE",
                "pay_date": "2019-09-10 00:00:00",
                "pay_run_posting": "2019-09-15 00:00:00",
                "cut_off_date": "2019-09-10 00:00:00",
                "pay_date_2": "2019-09-25 00:00:00",
                "pay_run_posting_2": "2019-10-01 00:00:00",
                "cut_off_date_2": "2019-09-25 00:00:00",
                "fpd_end_of_month": False,
                "fpp_end_of_month": False,
                "fcd_end_of_month": False,
                "spd_end_of_month": False,
                "spp_end_of_month": False,
                "scd_end_of_month": False,
                "payroll_group_subtype_id": 8778,
                "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
                "compute_overbreak": False,
                "compute_undertime": True,
                "compute_tardiness": True,
                "created_at": "2019-11-21 12:27:57",
                "updated_at": "2019-11-21 12:27:57",
                "deleted_at": None,
                "enforce_gap_loans": True,
                "minimum_net_take_home_pay": "2000.0000"
            },
            "updated_at": {
                "date": "2019-11-21 14:37:29.000000",
                "timezone_type": 3,
                "timezone": "Asia/Manila"
            },
            "termination_information": None,
            "team_id": None
        },
        {
            "id": 19370,
            "account_id": 524,
            "company_id": 711,
            "employee_id": "3371_002",
            "first_name": "Lily",
            "middle_name": "",
            "last_name": "Beth",
            "full_name": "Lily Beth",
            "email": "Salqateam01+3371_002@gmail.com",
            "gender": "female",
            "birth_date": "1989-02-01",
            "telephone_number": 9970002,
            "mobile_number": "(63)9170000002",
            "hours_per_day": None,
            "date_hired": "2019-01-02",
            "date_ended": None,
            "location_id": 631,
            "location_name": "salqajeff_location",
            "department_id": 1391,
            "department_name": "Department Parent1556085724",
            "rank_id": 1134,
            "rank_name": "Junior. Quality Assurance Analyst",
            "position_id": 10660,
            "position_name": "QA",
            "address": {
                "address_line_1": "1650 Penafrancia",
                "address_line_2": "",
                "city": "Makati",
                "zip_code": "1208",
                "country": "Philippines"
            },
            "active": "active",
            "non_taxed_other_income_limit": "90000.0000",
            "user_id": 26844,
            "payroll_group": {
                "id": 8778,
                "name": "posting date issue",
                "company_id": 711,
                "payroll_frequency": "SEMI_MONTHLY",
                "day_factor": "260.00",
                "hours_per_day": "8.00",
                "non_working_day_option": "BEFORE",
                "pay_date": "2019-09-10 00:00:00",
                "pay_run_posting": "2019-09-15 00:00:00",
                "cut_off_date": "2019-09-10 00:00:00",
                "pay_date_2": "2019-09-25 00:00:00",
                "pay_run_posting_2": "2019-10-01 00:00:00",
                "cut_off_date_2": "2019-09-25 00:00:00",
                "fpd_end_of_month": False,
                "fpp_end_of_month": False,
                "fcd_end_of_month": False,
                "spd_end_of_month": False,
                "spp_end_of_month": False,
                "scd_end_of_month": False,
                "payroll_group_subtype_id": 8778,
                "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
                "compute_overbreak": False,
                "compute_undertime": True,
                "compute_tardiness": True,
                "created_at": "2019-11-21 12:27:57",
                "updated_at": "2019-11-21 12:27:57",
                "deleted_at": None,
                "enforce_gap_loans": True,
                "minimum_net_take_home_pay": "2000.0000"
            },
            "updated_at": {
                "date": "2019-11-21 14:37:51.000000",
                "timezone_type": 3,
                "timezone": "Asia/Manila"
            },
            "termination_information": None,
            "team_id": None
        },
        {
            "id": 8091,
            "account_id": 524,
            "company_id": 711,
            "employee_id": "A001",
            "first_name": "Jeffrey",
            "middle_name": "",
            "last_name": "De Guzman",
            "full_name": "Jeffrey De Guzman",
            "email": "salqa01+A001@gmail.com",
            "gender": "male",
            "birth_date": "1993-04-25",
            "telephone_number": 4435253,
            "mobile_number": "(63)9954132469",
            "hours_per_day": None,
            "date_hired": "2010-06-28",
            "date_ended": None,
            "location_id": 631,
            "location_name": "salqajeff_location",
            "department_id": 1391,
            "department_name": "Department Parent1556085724",
            "rank_id": 1134,
            "rank_name": "Junior. Quality Assurance Analyst",
            "position_id": None,
            "position_name": None,
            "address": {
                "address_line_1": "1st street",
                "address_line_2": "2nd street",
                "city": "Caloocan",
                "zip_code": "1400",
                "country": "Philippines"
            },
            "active": "active",
            "non_taxed_other_income_limit": "90000.0000",
            "user_id": 11201,
            "payroll_group": {
                "id": 3148,
                "name": "upload_inactive_employee_test",
                "company_id": 711,
                "payroll_frequency": "SEMI_MONTHLY",
                "day_factor": "260.00",
                "hours_per_day": "8.00",
                "non_working_day_option": "BEFORE",
                "pay_date": "2019-07-15 00:00:00",
                "pay_run_posting": "2019-07-10 00:00:00",
                "cut_off_date": "2019-07-10 00:00:00",
                "pay_date_2": "2019-07-31 00:00:00",
                "pay_run_posting_2": "2019-07-25 00:00:00",
                "cut_off_date_2": "2019-07-25 00:00:00",
                "fpd_end_of_month": False,
                "fpp_end_of_month": False,
                "fcd_end_of_month": False,
                "spd_end_of_month": True,
                "spp_end_of_month": False,
                "scd_end_of_month": False,
                "payroll_group_subtype_id": 3148,
                "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
                "compute_overbreak": False,
                "compute_undertime": True,
                "compute_tardiness": True,
                "created_at": "2019-08-02 19:17:48",
                "updated_at": "2019-08-02 19:17:48",
                "deleted_at": None,
                "enforce_gap_loans": True,
                "minimum_net_take_home_pay": "2000.0000"
            },
            "updated_at": {
                "date": "2019-08-02 19:58:36.000000",
                "timezone_type": 3,
                "timezone": "Asia/Manila"
            },
            "termination_information": None,
            "team_id": None
        },
        {
            "id": 8093,
            "account_id": 524,
            "company_id": 711,
            "employee_id": "A002",
            "first_name": "Kuya",
            "middle_name": "",
            "last_name": "Jobert",
            "full_name": "Kuya Jobert",
            "email": "salqa01+A002@gmail.com",
            "gender": None,
            "birth_date": "1998-08-20",
            "telephone_number": 4425055,
            "mobile_number": "(63)9944312549",
            "hours_per_day": None,
            "date_hired": "2010-08-11",
            "date_ended": None,
            "location_id": 631,
            "location_name": "salqajeff_location",
            "department_id": 1391,
            "department_name": "Department Parent1556085724",
            "rank_id": 1134,
            "rank_name": "Junior. Quality Assurance Analyst",
            "position_id": None,
            "position_name": None,
            "address": {
                "address_line_1": "1st street",
                "address_line_2": "2nd street",
                "city": "",
                "zip_code": "1200",
                "country": "Philippines"
            },
            "active": "active",
            "non_taxed_other_income_limit": "90000.0000",
            "user_id": 11203,
            "payroll_group": {
                "id": 3148,
                "name": "upload_inactive_employee_test",
                "company_id": 711,
                "payroll_frequency": "SEMI_MONTHLY",
                "day_factor": "260.00",
                "hours_per_day": "8.00",
                "non_working_day_option": "BEFORE",
                "pay_date": "2019-07-15 00:00:00",
                "pay_run_posting": "2019-07-10 00:00:00",
                "cut_off_date": "2019-07-10 00:00:00",
                "pay_date_2": "2019-07-31 00:00:00",
                "pay_run_posting_2": "2019-07-25 00:00:00",
                "cut_off_date_2": "2019-07-25 00:00:00",
                "fpd_end_of_month": False,
                "fpp_end_of_month": False,
                "fcd_end_of_month": False,
                "spd_end_of_month": True,
                "spp_end_of_month": False,
                "scd_end_of_month": False,
                "payroll_group_subtype_id": 3148,
                "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
                "compute_overbreak": False,
                "compute_undertime": True,
                "compute_tardiness": True,
                "created_at": "2019-08-02 19:17:48",
                "updated_at": "2019-08-02 19:17:48",
                "deleted_at": None,
                "enforce_gap_loans": True,
                "minimum_net_take_home_pay": "2000.0000"
            },
            "updated_at": {
                "date": "2019-08-02 19:45:02.000000",
                "timezone_type": 3,
                "timezone": "Asia/Manila"
            },
            "termination_information": None,
            "team_id": None
        },
        {
            "id": 7950,
            "account_id": 524,
            "company_id": 711,
            "employee_id": "DM001",
            "first_name": "Monthly Reg",
            "middle_name": "",
            "last_name": "Scenario one",
            "full_name": "Monthly Reg Scenario one",
            "email": "salqateam01+DM001@gmail.com",
            "gender": "male",
            "birth_date": "1984-10-15",
            "telephone_number": 4425161,
            "mobile_number": "(63)9954132494",
            "hours_per_day": None,
            "date_hired": "2010-10-16",
            "date_ended": None,
            "location_id": 631,
            "location_name": "salqajeff_location",
            "department_id": 1391,
            "department_name": "Department Parent1556085724",
            "rank_id": 1134,
            "rank_name": "Junior. Quality Assurance Analyst",
            "position_id": None,
            "position_name": None,
            "address": {
                "address_line_1": "1st  Street",
                "address_line_2": "Brgy 12",
                "city": "Baliuag",
                "zip_code": "14501",
                "country": "Philippines"
            },
            "active": "active",
            "non_taxed_other_income_limit": "90000.0000",
            "user_id": 11025,
            "payroll_group": {
                "id": 3060,
                "name": "Semi Monthly_with_Deminimis",
                "company_id": 711,
                "payroll_frequency": "SEMI_MONTHLY",
                "day_factor": "260.00",
                "hours_per_day": "8.00",
                "non_working_day_option": "AFTER",
                "pay_date": "2019-09-15 00:00:00",
                "pay_run_posting": "2019-09-10 00:00:00",
                "cut_off_date": "2019-09-10 00:00:00",
                "pay_date_2": "2019-09-30 00:00:00",
                "pay_run_posting_2": "2019-09-25 00:00:00",
                "cut_off_date_2": "2019-09-25 00:00:00",
                "fpd_end_of_month": False,
                "fpp_end_of_month": False,
                "fcd_end_of_month": False,
                "spd_end_of_month": False,
                "spp_end_of_month": False,
                "scd_end_of_month": False,
                "payroll_group_subtype_id": 3060,
                "payroll_group_subtype_type": "App\\Model\\PhilippinePayrollGroup",
                "compute_overbreak": False,
                "compute_undertime": True,
                "compute_tardiness": True,
                "created_at": "2019-07-31 13:25:12",
                "updated_at": "2019-07-31 13:25:12",
                "deleted_at": None,
                "enforce_gap_loans": True,
                "minimum_net_take_home_pay": "2000.0000"
            },
            "updated_at": {
                "date": "2019-09-27 13:04:49.000000",
                "timezone_type": 3,
                "timezone": "Asia/Manila"
            },
            "termination_information": None,
            "team_id": None
        }],
        "total": 7,
        "per_page": 7,
        "current_page": 1,
        "last_page": 1,
        "from": 1,
        "to": 1
}

MOCK_LOAN_ITEMS_RESPONSE = {
    "data": {
        "type": "payroll-loan-items",
        "id": "test",
        "attributes": None,
        "relationships": {}
    },
    "included": [
        {"type": "loans", "id": 1, "attributes": {
            "loanName": "test", "amount": 10000}},
        {"type": "amortizations", "id": 2, "attributes": {
            "loanName": "test", "amountDue": 100}},
    ]
}

MOCK_LOAN_ITEMS_RESPONSE2 = {
    "data": {
        "type": "payroll-related-loan-items",
        "id": "/payroll/605/employee/5772/loan-items",
        "attributes": None,
        "relationships": {
            "loans": [
                {
                    "type": "loans",
                    "id": "16"
                }
            ],
            "amortizations": None
        }
    },
    "included": [
        {
            "type": "loans",
            "id": "16",
            "attributes": {
                "companyID": 491,
                "collectedToDate": "0.0000",
                "createdDate": "2019-08-15",
                "employeeID": 5772,
                "employeeIDNo": "",
                "isClosed": False,
                "paymentScheme": "EVERY_PAY_OF_THE_MONTH",
                "paymentStartDate": "2019-08-15",
                "term": 2,
                "totalAmount": "1425.5000",
                "typeID": 3,
                "loanName": "Gap",
                "monthlyAmortization": "1000.0000",
                "subtype": None,
                "referenceNo": None,
                "paymentEndDate": "2019-09-13",
                "active": False,
                "payrollID": 605
            },
            "links": {
                "self": "/loan/16"
            }
        }
    ]
}

MOCK_SYNC_DISBURSEMENT = {
    "synced_data": [
        {
            "payroll_id": 2822,
            "payroll_employee_id": 13874,
            "employee_id": 6409,
            "user_id": 9383,
            "company_id": 3652,
            "salpay_account_number": "SALPHPU-1808-4576-0471",
            "transfer_id": 602,
            "status": "failed",
            "completed_at": None,
            "id": 63
        }
    ],
    "original": {
        "start_date": "2020-01-01",
        "end_date": "2020-01-30",
        "payroll_id": 2822,
        "page": 1,
        "limit": 50,
        "payroll_page": 1,
        "payroll_limit": 50
    },
    "current": {
        "start_date": "2020-01-01",
        "end_date": "2020-01-30",
        "payroll_id": 2822,
        "page": 1,
        "limit": 50,
        "payroll_page": 1,
        "payroll_limit": 50
    },
    "next": {
        "start_date": "2020-01-01",
        "end_date": "2020-01-30",
        "payroll_id": 2848,
        "page": 1,
        "limit": 50,
        "payroll_page": 1,
        "payroll_limit": 50
    }
}

class ApiCallsTest(TestCase):
    """API Calls tests"""

    @mock.patch('requests.request')
    @mock.patch('payroll.config.Env')
    def test_update_job_ok(self, mock_env, mock_request):
        """Test if updating job ok"""
        mock_response = mock.Mock()
        mock_response.status_code = 200
        mock_response.json.return_value = {"foo": "bar"}  # does not matter

        mock_env.JOBS_API_BASE_URL = "base"
        mock_env.JOBS_API_URLS = {"update-job": ("/bar/{job_id}", 'POST')}

        mock_request.return_value = mock_response

        apicalls.update_job("123", "FINISHED", 3, 3)

        mock_body = {
            "data": {
                "type": "jobs",
                "id": "123",
                "attributes": {
                    "status": "FINISHED",
                    "tasksCount": 3,
                    "tasksDone": 3
                }
            }
        }
        mock_request.assert_called_once_with(
            headers=None, json=mock_body, method='POST', params=None, data=None,
            timeout=(5, 300), url='base/bar/123')

    @mock.patch('payroll.apicalls._exec_request', return_value=MOCK_LOAN_ITEMS_RESPONSE)
    def test_get_employee_loan_items_ok(self, mock_request):
        """Employee loan items 1"""
        loans, amort = apicalls.get_employee_payroll_related_loan_items(1, 2)
        assert len(loans) == 1
        assert len(amort) == 1
        assert loans[0]["id"] == 1
        assert loans[0]["loanName"] == "test"
        assert amort[0]["id"] == 2
        assert amort[0]["loanName"] == "test"
        mock_request.assert_called()

    @mock.patch('payroll.apicalls._exec_request', return_value=MOCK_LOAN_ITEMS_RESPONSE2)
    def test_get_employee_loan_items_ok2(self, mock_request):
        """Test employee loan items 2"""
        loans, amort = apicalls.get_employee_payroll_related_loan_items(1, 2)
        assert len(loans) == 1
        assert not amort
        assert loans[0]["id"] == "16"
        assert loans[0]["loanName"] == "Gap"
        mock_request.assert_called()

    @mock.patch('payroll.apicalls._exec_request', return_value=MOCK_COMPANY_EMPLOYEES)
    def test_get_company_employees(self, mock_request):
        lookup = apicalls.get_company_employees(711)
        assert len(lookup) == 7
        item = lookup.get("2608_002")
        assert item
        assert len(item) == 5  # five values inside
        assert not item[4]
        mock_request.assert_called()

    @mock.patch('payroll.apicalls._exec_request',
                return_value=company_employees_data1.MOCK_EMPLOYEES_DATA_WITH_PAYROLL)
    def test_get_company_employees_with_gov_ids(self, mock_request):
        """test with gov ids"""
        lookup = apicalls.get_company_employees_with_gov_ids(711)
        item = lookup.get("2608_002")
        assert item
        assert len(item) == 8  # eight values inside
        mock_request.assert_called()

    @mock.patch('payroll.apicalls._exec_request', return_value=MOCK_SYNC_DISBURSEMENT)
    def test_sync_payroll_disburesments(self, mock_request):
        """test sync payroll disbursements"""
        lookup = apicalls.sync_payroll_disburesments({
            "start_date": "2020-01-01",
            "end_date": "2020-01-30",
            "payroll_id": 2822,
            "page": 1,
            "limit": 50,
            "payroll_page": 1,
            "payroll_limit": 50
        })
        next_payload = lookup.get("next")
        assert next_payload
        mock_request.assert_called()

    @mock.patch('payroll.apicalls._exec_request', return_value={})
    def test_delete_job_result_items_by_job_id(self, mock_request):
        """test deletion of job result items by job id"""
        apicalls.delete_job_result_items_by_job_id("54427c50-4b5c-42d2-9272-91ace8efab46")
        mock_request.assert_called()

    @mock.patch('payroll.apicalls._exec_request', return_value={})
    def test_delete_job_error_items_by_job_id(self, mock_request):
        """test deletion of job error items by job id"""
        apicalls.delete_job_error_items_by_job_id("54427c50-4b5c-42d2-9272-91ace8efab46")
        mock_request.assert_called()
