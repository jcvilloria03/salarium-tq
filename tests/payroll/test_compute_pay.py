"""Unit test for computation"""
#pylint: disable=all
from unittest import TestCase, mock
import json
from payroll import tasks, apicalls
from core import logger
from tests.payroll import test_data
logger = logger.get_logger()


TEST_PARAMS = {'name': 'compute-payroll', 'payrollId': '605', 'attendanceJobId': '5ea3b171-f055-4a50-93c6-cff2fb3a9680', 'allowancesJobId': None, 'bonusesJobId': None, 'commissionsJobId': None, 'deductionsJobId': None,  'jobId': '19e1e25f-a756-4995-b360-5de75b72d69d', 'payrollSettings': {'payrollStartDate': '2019-04-11', 'payrollEndDate': '2019-04-25', 'attendanceStartDate': '2019-04-11', 'attendanceEndDate': '2019-04-25', 'postingDate': '2019-04-30', 'payRunDate': '2019-04-30', 'employeeIds': [5772, 5774], 'payrollJobs': {'ATTENDANCE': {'job_id': '5ea3b171-f055-4a50-93c6-cff2fb3a9680', 'job_file': 'Attendance GapLoan-366-3949-156319420689855d2c735edb5d0314552509.csv'}, 'CALCULATION': {'job_id': '6f67d4a0-3bd2-4248-a13f-cf7cf9009e11', 'job_file': ''}, 'RECALCULATION': {'job_id': '19e1e25f-a756-4995-b360-5de75b72d69d', 'job_file': ''}}, 'payrollGroupInfo': {'id': '1925', 'accountId': '366', 'companyId': '491', 'payrollGroupName': 'Gap Loans', 'payrollFrequency': 'SEMI_MONTHLY', 'dayFactor': 261, 'cutoffDate': '2019-01-10', 'cutoffDateEom': False, 'cutoffDate2': '2019-01-25', 'cutoffDate2Eom': False, 'postingDate': '2019-01-15', 'postingDateEom': False, 'postingDate2': '2019-01-31', 'postingDate2Eom': True, 'hoursPerDay': 8, 'contributionSchedules': {'SSS': 'EVERY_PAY', 'HDMF': 'EVERY_PAY', 'PHILHEALTH': 'EVERY_PAY'}, 'wtaxSchedule': 'EVERY_PAY', 'wtaxMethod': 'BASED_ON_ACTUAL', 'gapLoans': {'enforce': True, 'minimumNetTakeHomePay': 2}}, 'dayHourRates': {'REGULAR': 1, 'PAID_LEAVE': 1, 'UNPAID_LEAVE': 1, 'SH': 1.3, 'RH': 2, '2RH': 3, 'NT': 1.1, 'SH+NT': 1.43, 'RH+NT': 2.2, '2RH+NT': 3.3, 'OT': 1.25, 'SH+OT': 1.69, 'RH+OT': 2.6, '2RH+OT': 3.9, 'NT+OT': 1.375, 'SH+NT+OT': 1.859, 'RH+NT+OT': 2.86, '2RH+NT+OT': 4.29, 'RD': 1.3, 'RD+SH': 1.5, 'RD+RH': 2.6, 'RD+2RH': 3.9, 'RD+NT': 1.43, 'RD+SH+NT': 1.65, 'RD+RH+NT': 2.86, 'RD+2RH+NT': 4.29, 'RD+OT': 1.69, 'RD+SH+OT': 1.95, 'RD+RH+OT': 3.38, 'RD+2RH+OT': 5.07, 'RD+NT+OT': 1.859, 'RD+SH+NT+OT': 2.145, 'RD+RH+NT+OT': 3.718, 'RD+2RH+NT+OT': 5.577, 'RH+SH': 2.6, 'RH+SH+NT': 2.86, 'RD+RH+SH': 3, 'RH+SH+OT': 3.38, 'RH+SH+NT+OT': 3.718, 'RD+RH+SH+NT': 3.3, 'RD+RH+SH+OT': 3.9, 'RD+RH+SH+NT+OT': 4.29}}, 'originalJobId': '19e1e25f-a756-4995-b360-5de75b72d69d', 'employeeUID': '5772', 'allowances': None, 'bonuses': None, 'commissions': None, 'deductions': None, 'employeeInfo': {'accountId': '366', 'companyId': '491', 'payrollGroupId': '1925', 'payrollGroupName': 'Gap Loans', 'employeeId': 'GapLoan-00001', 'firstName': 'Gap', 'middleName': '', 'lastName': 'Loan', 'email': 'l.tobias+gaploan1@salarium.com', 'gender': 'male', 'birthDate': None, 'telephoneNumber': '', 'mobileNumber': 'NULL', 'hoursPerDay': '', 'dateHired': '2019-02-26', 'dateEnded': None, 'locationId': '394', 'locationName': 'Makati', 'departmentId': '3110', 'departmentName': 'Sales', 'rankId': '', 'rankName': None, 'costCenterId': '', 'costCenterName': None, 'positionId': '', 'positionName': None, 'address': '1650 Penafrancia St. Makati City', 'city': 'Makati', 'zipCode': '1226', 'country': '', 'taxStatus': 'S', 'taxType': 'Regular', 'taxRate': None, 'tin': '707-070-979-070', 'sssNumber': '00-2382038-3', 'philhealthNumber': '09-790790790-7', 'hdmfNumber': '0979-0790-7990', 'contributionBasis': {'SSS': {'type': 'GROSS', 'amount': '0.00000000'}, 'PHILHEALTH': {'type': 'GROSS', 'amount': '0.00000000'}, 'HDMF': {'type': 'GROSS', 'amount': '0.00000000'}}, 'rdo': '', 'active': False, 'nonTaxedOtherIncomeLimit': '90000.0000', 'basePay': [{'id': '5782', 'type': 'base_pay', 'attributes': {'amount': '100.00000000', 'unit': 'DAILY', 'effectiveDate': '2019-02-26'}}]}, 'aabcd': {}, 'attendanceData': {'2019-04-11': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-12': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-13': {'SCHEDULED_HOURS': 0.0}, '2019-04-14': {'SCHEDULED_HOURS': 0.0}, '2019-04-15': {'SCHEDULED_HOURS': 8.0}, '2019-04-16': {'SCHEDULED_HOURS': 8.0}, '2019-04-17': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-18': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-19': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-20': {'SCHEDULED_HOURS': 0.0}, '2019-04-21': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 0.0}, '2019-04-22': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-23': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-24': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}, '2019-04-25': {'SCHEDULED_HOURS': 8.0}}, 'contributionRecord': {'SSS': [], 'HDMF': [], 'PHILHEALTH': []}, 'loansToCredit': [{'companyID': 491, 'collectedToDate': '0.0000', 'createdDate': '2019-08-15', 'employeeID': 5772, 'employeeIDNo': '', 'isClosed': False, 'paymentScheme': 'EVERY_PAY_OF_THE_MONTH', 'paymentStartDate': '2019-08-15', 'term': 2, 'totalAmount': '1425.5000', 'typeID': 3, 'loanName': 'Gap', 'monthlyAmortization': '1000.0000', 'subtype': None, 'referenceNo': None, 'paymentEndDate': '2019-09-13', 'active': False, 'payrollID': 605, 'id': '16'}], 'amortizations': []}
TEST_PARAMS_SPECIAL = {'name':'compute-special-payroll','payrollId':'1125','companyId':'485',"payrollSettings":test_data.MOCK_DATA_SPECIAL_PAYROLL_SETTINGS,"employeeUID":592}
TEST_PARAMS_FINAL = {"name": "compute-final-payroll", "payrollId": 21, "companyId": 1, "attendanceJobId": "5d3057d9-1b06-47cf-b5df-0f28926930c8", "payrollSettings": test_data.MOCK_DATA_FINAL_PAYROLL_SETTINGS["data"]}
class ComputeTest(TestCase):

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    def test_compute_pay_1_should_not_throw_exception(self, mock_updater, mock_error, mock_result):
        res = tasks.compute_pay.s(TEST_PARAMS).apply()
        try:
            json.dumps(res.get())
        except:
            self.fail('Result is not JSON dumpable')
        mock_result.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('payroll.apicalls.get_special_payroll_employee_settings', return_value=test_data.MOCK_DATA_SPECIAL_PAYROLL_EMPLOYEE_SETTINGS)
    def test_compute_special_pay_should_not_throw_exception(self, apicalls_pe_settings, mock_updater, mock_error, mock_result):
        res = tasks.compute_special_pay.s(TEST_PARAMS_SPECIAL).apply()
        try:
            json.dumps(res.get())
        except:
            self.fail('Result is not JSON dumpable')
        mock_result.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('payroll.apicalls.get_special_payroll_employee_settings', return_value=test_data.MOCK_DATA_SPECIAL_PAYROLL_EMPLOYEE_SETTINGS_WITH_DEMINIMIS_FULLY_NON_TAXABLE)
    def test_compute_special_pay_with_deminimies_should_not_throw_exception_1(self, apicalls, mock_updater, mock_error, mock_result):
        # DEMINIMIS - Fully Non Taxable
        res = tasks.compute_special_pay.s(TEST_PARAMS_SPECIAL).apply()
        try:
            json.dumps(res.get())
        except:
            self.fail('Result is not JSON dumpable')
        mock_result.apply_async.assert_called_once()
    

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('payroll.apicalls.get_special_payroll_employee_settings', return_value=test_data.MOCK_DATA_SPECIAL_PAYROLL_EMPLOYEE_SETTINGS_WITH_DEMINIMIS_REDUCE_TO_TAXABLE)
    def test_compute_special_pay_with_deminimies_should_not_throw_exception_2(self, apicalls, mock_updater, mock_error, mock_result):
        # DEMINIMIS - Reduce to Taxable Income
        res = tasks.compute_special_pay.s(TEST_PARAMS_SPECIAL).apply()
        try:
            json.dumps(res.get())
        except:
            self.fail('Result is not JSON dumpable')
        mock_result.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('payroll.apicalls.get_final_payroll_employee_settings', return_value=test_data.MOCK_DATA_FINAL_PAYROLL_EMPLOYEE_SETTINGS)
    def test_compute_final_pay_should_not_throw_exception(
        self, apicalls_pe_settings, mock_updater, mock_error, mock_result
    ):
        res = tasks.compute_final_pay.s(TEST_PARAMS_FINAL).apply()
        try:
            json.dumps(res.get())
        except:
            self.fail('Result is not JSON dumpable')
        mock_result.apply_async.assert_called_once()
