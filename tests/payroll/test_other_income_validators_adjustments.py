"""Payroll task unit tests"""
# pylint: disable=protected-access,line-too-long
from unittest import TestCase
from payroll import filevalidators

TEST_CONTEXT_ADJUSTMENT = {
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True)
    },
    "typeList": [
        {
            "id": 3,
            "name": "TAXABLE_INCOME",
            "company_id": 0,
            "type_name": "App\\Model\\PhilippineAdjustmentType",
            "deleted_at": None
        },
        {
            "id": 4,
            "name": "NON_TAXABLE_INCOME",
            "company_id": 0,
            "type_name": "App\\Model\\PhilippineAdjustmentType",
            "deleted_at": None
        },
        {
            "id": 5,
            "name": "TAX_ADJUSTMENT",
            "company_id": 0,
            "type_name": "App\\Model\\PhilippineAdjustmentType",
            "deleted_at": None
        },
        {
            "id": 6,
            "name": "NON_INCOME",
            "company_id": 0,
            "type_name": "App\\Model\\PhilippineAdjustmentType",
            "deleted_at": None
        }
    ]
}

ADJUSTMENT_DATA_OK = """Employee ID,Last Name,First Name,Middle Name,Adjustment Type,Amount,Payroll Date,Include to Special Pay Run?,Reason
TEST1,Brando,Marlon,Grumble,taxable income,3000,01/01/2019,No,Missed
"""

ADJUSTMENT_MISSING_KEYS = """Employee ID,Last Name,First Name,Middle Name,Adjustment Type,Amount,Payroll Date,Include to Special Pay Run?,Reason
,Brando,Marlon,Grumble,,,,,
"""

ADJUSTMENT_LINE_ERRORS = """Employee ID,Last Name,First Name,Middle Name,Adjustment Type,Amount,Payroll Date,Include to Special Pay Run?,Reason
TEST2,Brando,Marlon,Grumble,taxable income,3000,01/01/2019,No,Missed
TEST1,Brando,Marlon,Grumble,non income2,3000,01/01/2019,No,Missed
TEST1,Brando,Marlon,Grumble,taxable income,xxxx,01/01/2019,No,Missed
TEST1,Brando,Marlon,Grumble,taxable income,3000,13/01/2019,No,Missed
TEST1,Brando,Marlon,Grumble,taxable income,3000,01/01/2019,faaaaa,Missed
"""

class AdjustmentFileUploadValidatorTest(TestCase):

    def test_ok_1(self):
        validator = filevalidators.EmployeeAdjustmentsFileUploadValidator(TEST_CONTEXT_ADJUSTMENT)
        data = ADJUSTMENT_DATA_OK.splitlines()
        validator.iterate_source(data)
        status_not_disbursed = validator.result[0]['release_details'][0]['status']
        print(validator._errors)
        assert not validator.has_errors()
        assert not status_not_disbursed

    def test_missing_keys(self):
        validator = filevalidators.EmployeeAdjustmentsFileUploadValidator(TEST_CONTEXT_ADJUSTMENT)
        data = ADJUSTMENT_MISSING_KEYS.splitlines()
        validator.iterate_source(data)
        print(validator._errors)

        assert validator.has_errors()
        assert len(validator._errors) == 2
        
        error1 = validator._errors.get("2")[0]
        assert error1.get("name") == "Missing Parameters"
        details = error1.get("details")[0]
        params = details.get("parameters")
        assert "Employee ID" in params
        assert "Adjustment Type" in params
        assert "Amount" in params
        assert "Payroll Date" in params
        assert "Include to Special Pay Run?" in params
        assert "Reason" in params
    

    def test_error_lines(self):
        validator = filevalidators.EmployeeAdjustmentsFileUploadValidator(TEST_CONTEXT_ADJUSTMENT)
        data = ADJUSTMENT_LINE_ERRORS.splitlines()
        validator.iterate_source(data)
        assert validator.has_errors()
        print(validator._errors)
        assert len(validator._errors) == 6
        
        #income type invalid value
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("details")[0].get("expectedValue") == 'Taxable Income/Non Taxable Income/Tax Adjustment/Non Income'





