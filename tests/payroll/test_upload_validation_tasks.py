"""Payroll task unit tests"""
# pylint: disable=protected-access,line-too-long
from unittest import TestCase, mock
from payroll import tasks

MOCK_COMPANY_EMPLOYEES = {
    "TEST01": (1, 'Luigi', 'Angeles', 'Tobias', True),
    "TEST02": (2, 'Rodolfo', 'Narciso', 'Duldulao', True)
}
MOCK_ALLOWANCES = [
    {
        "id": 2,
        "name": "DE_MINIMIS",
        "company_id": 0,
        "fully_taxable": False,
        "max_non_taxable": 90000,
        "type_name": "App\\Model\\PhilippineAllowanceType",
        "deleted_at": None
    },
    {
        "id": 170,
        "name": "Meal Allowance",
        "company_id": 50,
        "fully_taxable": False,
        "max_non_taxable": 90000,
        "type_name": "App\\Model\\PhilippineAllowanceType",
        "deleted_at": None
    },
    {
        "id": 171,
        "name": "Clothing Allowance",
        "company_id": 50,
        "fully_taxable": False,
        "max_non_taxable": 90000,
        "type_name": "App\\Model\\PhilippineAllowanceType",
        "deleted_at": None
    }
]

DATA_OK = b"""Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Special Pay Run?,Recurring?,Crediting Frequency,Prorated?,Prorated By?,Prorate Entitlement On Paid Leave?,Prorate Entitlement On Unpaid Leave?,Never Expires?,Valid From?,Valid Until?
TEST01,Tobias,Luigi,Angeles,Clothing Allowance,"3,000.00",No,Yes,Per Month,No,,No,No,Yes,01/01/2019,
"""

class FileUploadValidationTaskTest(TestCase):
    """File Upload Validation tasks tests"""

    @mock.patch('core.fileutils.download_file', return_value=DATA_OK)
    @mock.patch('payroll._tasks.file_tasks.save_other_income_items')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    def test_allowance_upload_validation_ok(self, error_updater, saver, downloader):
        """Test for allowance upload validation"""
        params = {
            "name": "validate-employee-allowance-file",
            "companyId": 50,
            "fileBucket": "bucket",
            "fileKey": "key",
            "jobId": "fooid",
            "companyEmployees": MOCK_COMPANY_EMPLOYEES,
            "otherIncomeType": "allowance_type",
            "typeList": MOCK_ALLOWANCES
        }
        tasks.validate_other_income_upload.apply(args=(params,))

        error_updater.apply_async.assert_not_called()
        saver.apply_async.assert_called()
        _, kwargs = saver.apply_async.call_args
        args = kwargs.get("args")
        result = args[0]['result']
        assert result[0]['amount'] == 3000.0
        assert result[0]['type_id'] == 171
        assert result[0]['recipients']['employees'][0] == 1
        assert result[0]['recurring'] == 1
        assert result[0]['credit_frequency'] == 'PER_MONTH'
        assert result[0]['prorated'] == 0
        assert not result[0]['prorated_by']
        assert not result[0]['entitled_when']
        assert result[0]['valid_from'] == '2019-01-01'
        assert not result[0]['valid_to']
        assert not result[0]['release_details'][0]['disburse_through_special_pay_run']
        downloader.assert_called_with("bucket", "key")
