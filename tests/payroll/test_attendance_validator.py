"""Payroll task unit tests"""
# pylint: disable=protected-access
from unittest import TestCase
from payroll import filevalidators

class AttendanceFileValidatorTest(TestCase):
    """Attendance Validator tests"""
    def test_validator_validate_with_inactive_employee(self):
        """This test is no longer relevant since no more undertime computation"""
        validator = filevalidators.AttendanceFileProcessor(
            context={
                "payrollGroupEmployees": {
                    "FakeID": ("1", "Delfin", "Garcia", "Juat", False)
                },
                "startDate": "2019-01-01",
                "endDate": "2019-01-30"
            }
        )
        fake_line_index = 2
        fake_data = {
            "Employee ID": "FakeID",
            "Timesheet Date": "01/10/2019",
            "First Name": "Delfin",
            "Middle Name": "Garcia",
            "Last Name": "Juat",
            "Regular": "4:00",
            "Scheduled Hours": "8:00",
            "Unpaid Leave": "4:00",
        }

        validator._validate(fake_line_index, fake_data)
        assert not validator._temp_results #no temp result since employee is inactive
        assert len(validator._inactive_employees) == 1


    def test_validator_validate_with_undertime_and_tardy(self):
        """Tardy and undertime test"""
        validator = filevalidators.AttendanceFileProcessor(
            context={
                "payrollGroupEmployees": {
                    "FakeID": ("1", "Delfin", "Garcia", "Juat", True)
                },
                "startDate": "2019-01-01",
                "endDate": "2019-01-30"

            }
        )
        fake_line_index = 2
        fake_data = {
            "Employee ID": "FakeID",
            "Timesheet Date": "01/10/2019",
            "First Name": "Delfin",
            "Middle Name": "Garcia",
            "Last Name": "Juat",
            "Regular": "3:00",
            "Scheduled Hours": "8:00",
            "Undertime": "4:00",
            "Tardy": "1:00"
        }

        validator._validate(fake_line_index, fake_data)
        assert validator._temp_results.get("1")
        assert validator._temp_results.get("1").get("2019-01-10")
        assert validator._temp_results.get("1").get("2019-01-10").get("UNDERTIME")
        assert validator._temp_results.get("1").get("2019-01-10").get("UNDERTIME") == 4
        assert validator._temp_results.get("1").get("2019-01-10").get("TARDY")
        assert validator._temp_results.get("1").get("2019-01-10").get("TARDY") == 1

    def test_validator_validate_with_overbreak(self):
        """Overbreak test"""
        validator = filevalidators.AttendanceFileProcessor(
            context={
                "payrollGroupEmployees": {
                    "FakeID": ("1", "Delfin", "Garcia", "Juat", True)
                },
                "startDate": "2019-01-01",
                "endDate": "2019-01-30"

            }
        )
        fake_line_index = 2
        fake_data = {
            "Employee ID": "FakeID",
            "Timesheet Date": "01/10/2019",
            "First Name": "Delfin",
            "Middle Name": "Garcia",
            "Last Name": "Juat",
            "Regular": "8:00",
            "Scheduled Hours": "8:00",
            "Overbreak": "0:15"
            
        }

        validator._validate(fake_line_index, fake_data)
        assert validator._temp_results.get("1")
        assert validator._temp_results.get("1").get("2019-01-10")
        assert validator._temp_results.get("1").get("2019-01-10").get("REGULAR")
        assert validator._temp_results.get("1").get("2019-01-10").get("REGULAR") == 8
        assert validator._temp_results.get("1").get("2019-01-10").get("OVERBREAK")
        assert validator._temp_results.get("1").get("2019-01-10").get("OVERBREAK") == 0.25

    def test_validator_validate_with_zero_sched_hours(self):
        """Test validator no scheduled hours"""
        validator = filevalidators.AttendanceFileProcessor(
            context={
                "payrollGroupEmployees": {
                    "FakeID": ("1", "Delfin", "Garcia", "Juat", True)
                },
                "startDate": "2019-01-01",
                "endDate": "2019-01-30"

            }
        )
        fake_line_index = 2
        fake_data = {
            "Employee ID": "FakeID",
            "Timesheet Date": "01/10/2019",
            "First Name": "Delfin",
            "Middle Name": "Garcia",
            "Last Name": "Juat",
            "Scheduled Hours": "0:00",
        }

        validator._validate(fake_line_index, fake_data)
        assert validator._temp_results.get("1")
        assert validator._temp_results.get("1").get("2019-01-10")
        assert validator._temp_results.get("1").get("2019-01-10").get("SCHEDULED_HOURS") is not None
        assert validator._temp_results.get("1").get("2019-01-10").get("SCHEDULED_HOURS") == 0.0

    def test_validator_validate_absent(self):
        """Test validator no scheduled hours"""
        validator = filevalidators.AttendanceFileProcessor(
            context={
                "payrollGroupEmployees": {
                    "FakeID": ("1", "Delfin", "Garcia", "Juat", True)
                },
                "startDate": "2019-01-01",
                "endDate": "2019-01-30"

            }
        )
        fake_line_index = 2
        fake_data = {
            "Employee ID": "FakeID",
            "Timesheet Date": "01/10/2019",
            "First Name": "Delfin",
            "Middle Name": "Garcia",
            "Last Name": "Juat",
            "Scheduled Hours": "8:00",
        }

        validator._validate(fake_line_index, fake_data)
        assert validator._temp_results.get("1")
        assert validator._temp_results.get("1").get("2019-01-10")
        assert validator._temp_results.get("1").get("2019-01-10").get("SCHEDULED_HOURS") is not None
        assert validator._temp_results.get("1").get("2019-01-10").get("SCHEDULED_HOURS") == 8.0

        assert validator._temp_results.get("1")
        assert validator._temp_results.get("1").get("2019-01-10")
        assert validator._temp_results.get("1").get("2019-01-10").get("ABSENT") is not None
        assert validator._temp_results.get("1").get("2019-01-10").get("ABSENT") == 8.0



