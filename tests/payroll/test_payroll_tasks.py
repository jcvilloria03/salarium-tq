# pylint: disable=all
"""Payroll task unit tests"""
from unittest import TestCase, mock, skip
import json
from payroll import tasks
from core import logger
from tests.payroll import test_data
logger = logger.get_logger()

MOCK_ATT_DATA = """Employee ID,Last Name,First Name,Middle Name,Timesheet Date,Scheduled Hours,Regular,Paid Leave,Unpaid Leave,OT,NT,NT+OT,RH,RH+NT,RH+OT,RH+NT+OT,SH,SH+NT,SH+OT,SH+NT+OT,RH+SH,RH+SH+OT,RH+SH+NT,RH+SH+NT+OT,2RH,2RH+NT,2RH+OT,2RH+NT+OT,RD,RD+SH,RD+RH,RD+2RH,RD+NT,RD+SH+NT,RD+RH+NT,RD+2RH+NT,RD+OT,RD+SH+OT,RD+RH+OT,RD+2RH+OT,RD+NT+OT,RD+SH+NT+OT,RD+RH+NT+OT,RD+2RH+NT+OT
TRAIN-001,Weber,Ma.Regina,Jones,1/1/2019,8:00,8:00,0:00,0:00,0:00,0:00,0:00,8:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00
"""

MOCK_ATT_DATA_ERROR = """Employee ID,Last Name,First Name,Middle Name,Timesheet Date,Scheduled Hours,Regular,Paid Leave,Unpaid Leave,OT,NT,NT+OT,RH,RH+NT,RH+OT,RH+NT+OT,SH,SH+NT,SH+OT,SH+NT+OT,RH+SH,RH+SH+OT,RH+SH+NT,RH+SH+NT+OT,2RH,2RH+NT,2RH+OT,2RH+NT+OT,RD,RD+SH,RD+RH,RD+2RH,RD+NT,RD+SH+NT,RD+RH+NT,RD+2RH+NT,RD+OT,RD+SH+OT,RD+RH+OT,RD+2RH+OT,RD+NT+OT,RD+SH+NT+OT,RD+RH+NT+OT,RD+2RH+NT+OT
TRAIN-001,Weber,Ma.Regina,Jonex,1/33/2019,8:00,8:00,0:00,0:00,0:00,0:00,0:00,8:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:xx
"""

MOCK_ATT_DATA_WITH_BLANKS =  """Employee ID,Last Name,First Name,Middle Name,Timesheet Date,Scheduled Hours,Regular,Paid Leave,Unpaid Leave,OT,NT,NT+OT,RH,RH+NT,RH+OT,RH+NT+OT,SH,SH+NT,SH+OT,SH+NT+OT,RH+SH,RH+SH+OT,RH+SH+NT,RH+SH+NT+OT,2RH,2RH+NT,2RH+OT,2RH+NT+OT,RD,RD+SH,RD+RH,RD+2RH,RD+NT,RD+SH+NT,RD+RH+NT,RD+2RH+NT,RD+OT,RD+SH+OT,RD+RH+OT,RD+2RH+OT,RD+NT+OT,RD+SH+NT+OT,RD+RH+NT+OT,RD+2RH+NT+OT

TRAIN-001,Weber,Ma.Regina,Jones,1/1/2019,8:00,8:00,0:00,0:00,0:00,0:00,0:00,8:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00

TRAIN-001,Weber,Ma.Regina,Jones,1/2/2019,8:00,8:00,0:00,0:00,0:00,0:00,0:00,8:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00

"""


MOCK_ATT_DATA_ALL_BLANKS =  """Employee ID,Last Name,First Name,Middle Name,Timesheet Date,Scheduled Hours,Regular,Paid Leave,Unpaid Leave,OT,NT,NT+OT,RH,RH+NT,RH+OT,RH+NT+OT,SH,SH+NT,SH+OT,SH+NT+OT,RH+SH,RH+SH+OT,RH+SH+NT,RH+SH+NT+OT,2RH,2RH+NT,2RH+OT,2RH+NT+OT,RD,RD+SH,RD+RH,RD+2RH,RD+NT,RD+SH+NT,RD+RH+NT,RD+2RH+NT,RD+OT,RD+SH+OT,RD+RH+OT,RD+2RH+OT,RD+NT+OT,RD+SH+NT+OT,RD+RH+NT+OT,RD+2RH+NT+OT



"""

MOCK_ATT_DATA_ABSENT = """Employee ID,Last Name,First Name,Middle Name,Timesheet Date,Scheduled Hours,Regular,Paid Leave,Unpaid Leave,OT,NT,NT+OT,RH,RH+NT,RH+OT,RH+NT+OT,SH,SH+NT,SH+OT,SH+NT+OT,RH+SH,RH+SH+OT,RH+SH+NT,RH+SH+NT+OT,2RH,2RH+NT,2RH+OT,2RH+NT+OT,RD,RD+SH,RD+RH,RD+2RH,RD+NT,RD+SH+NT,RD+RH+NT,RD+2RH+NT,RD+OT,RD+SH+OT,RD+RH+OT,RD+2RH+OT,RD+NT+OT,RD+SH+NT+OT,RD+RH+NT+OT,RD+2RH+NT+OT
TRAIN-001,Weber,Ma.Regina,Jones,1/1/2019,8:00,0:00,0:00,0:00,0:00,0:00,0:00,8:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00,0:00
"""

MOCK_BONUSES_DATA = """Employee ID,Last Name,First Name,Middle Name,Bonus Type,Amount,Taxable
TRAIN-001,Weber,Ma.Regina,Jones,Mid-year,2000,No
TRAIN-001,Weber,Ma.Regina,Jones,Mid-year,2000,No
TRAIN-001,Weber,Ma.Regina,Jones,Something Special,5000,Yes
"""

MOCK_DEDUCTIONS_DATA = """Employee ID,Last Name,First Name,Middle Name,Deduction Type,Amount
TRAIN-001,Weber,Ma.Regina,Jones,Gym Fees,2000
TRAIN-001,Weber,Ma.Regina,Jones,Laptop Repair,2000
TRAIN-001,Weber,Ma.Regina,Jones,Cash Advance,5000
"""


MOCK_ALLOWANCES_DATA = """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Taxable
TRAIN-001,Weber,Ma.Regina,Jones,Rice,2000,No
TRAIN-001,Weber,Ma.Regina,Jones,Laundry,2000,No
TRAIN-001,Weber,Ma.Regina,Jones,Clothing,5000,Yes
"""

MOCK_ALLOWANCES_DATA_NO_MNAME =  """Employee ID,Last Name,First Name,Middle Name,Allowance Type,Amount,Taxable
TRAIN-001,Weber,Ma.Regina,,Rice,2000,No
TRAIN-001,Weber,Ma.Regina,,Laundry,2000,No
TRAIN-001,Weber,Ma.Regina,,Clothing,5000,Yes
"""

MOCK_PG_DATA = {
  "data": {
    "type": "payroll-settings",
    "id": "2550",
    "attributes": {
      "payrollStartDate": "2019-08-27",
      "payrollEndDate": "2019-09-10",
      "attendanceStartDate": "2019-08-27",
      "attendanceEndDate": "2019-09-10",
      "postingDate": "2019-09-15",
      "payRunDate": "2019-09-13",
      "employeeIds": [
        21140
      ],
      "inactiveEmployeeIds": [],
      "payrollJobs": {
        "ATTENDANCE": {
          "job_id": "23aae416-a1b6-4348-b6a0-d607532e4995",
          "job_file": "as-api-data"
        },
        "CALCULATION": {
          "job_id": "0e677669-35a1-4aeb-995e-ebb01fb04386",
          "job_file": ""
        },
        "RECALCULATION": {
          "job_id": "7651a221-3b91-4a42-ad63-6c62aca9a9bf",
          "job_file": ""
        }
      },
      "payrollGroupInfo": {
        "id": "9117",
        "accountId": "366",
        "companyId": "491",
        "payrollGroupName": "JSTOVRISSUE-3562",
        "payrollFrequency": "SEMI_MONTHLY",
        "dayFactor": 261,
        "cutoffDate": "2019-12-10",
        "cutoffDateEom": False,
        "cutoffDate2": "2019-12-26",
        "cutoffDate2Eom": False,
        "postingDate": "2019-12-15",
        "postingDateEom": False,
        "postingDate2": "2019-12-31",
        "postingDate2Eom": True,
        "hoursPerDay": 8,
        "contributionSchedules": {
          "SSS": "EVERY_PAY",
          "HDMF": "EVERY_PAY",
          "PHILHEALTH": "EVERY_PAY"
        },
        "wtaxSchedule": "EVERY_PAY",
        "wtaxMethod": "BASED_ON_ACTUAL",
        "gapLoans": {
          "enforce": False,
          "minimumNetTakeHomePay": 0
        }
      },
      "dayHourRates": {
        "REGULAR": 1,
        "PAID_LEAVE": 1,
        "UNPAID_LEAVE": 1,
        "SH": 1.3,
        "RH": 2,
        "2RH": 3,
        "NT": 1.1,
        "SH+NT": 1.43,
        "RH+NT": 2.2,
        "2RH+NT": 3.3,
        "OT": 1.25,
        "SH+OT": 1.69,
        "RH+OT": 2.6,
        "2RH+OT": 3.9,
        "NT+OT": 1.375,
        "SH+NT+OT": 1.859,
        "RH+NT+OT": 2.86,
        "2RH+NT+OT": 4.29,
        "RD": 1.3,
        "RD+SH": 1.5,
        "RD+RH": 2.6,
        "RD+2RH": 3.9,
        "RD+NT": 1.43,
        "RD+SH+NT": 1.65,
        "RD+RH+NT": 2.86,
        "RD+2RH+NT": 4.29,
        "RD+OT": 1.69,
        "RD+SH+OT": 1.95,
        "RD+RH+OT": 3.38,
        "RD+2RH+OT": 5.07,
        "RD+NT+OT": 1.859,
        "RD+SH+NT+OT": 2.145,
        "RD+RH+NT+OT": 3.718,
        "RD+2RH+NT+OT": 5.577,
        "RH+SH": 2.6,
        "RH+SH+NT": 2.86,
        "RD+RH+SH": 3,
        "RH+SH+OT": 3.38,
        "RH+SH+NT+OT": 3.718,
        "RD+RH+SH+NT": 3.3,
        "RD+RH+SH+OT": 3.9,
        "RD+RH+SH+NT+OT": 4.29
      },
      "employees": [
        {
          "id": 21140,
          "account_id": 366,
          "company_id": 491,
          "payroll_group_id": 9117,
          "payroll_group_name": "JSTOVRISSUE-3562",
          "employee_id": "JSTOVREMP-001",
          "first_name": "Ted",
          "middle_name": "Rowe",
          "last_name": "Strickland",
          "email": "juani+dev.tedrowestrickland@salarium.com",
          "gender": "male",
          "birth_date": "1976-12-02",
          "telephone_number": 2131231,
          "mobile_number": "(21)3213213123",
          "base_pay": None,
          "base_pay_unit": None,
          "hours_per_day": "8.00",
          "day_factor": "261.00",
          "payroll_frequency": "SEMI_MONTHLY",
          "date_hired": "2001-12-27",
          "date_ended": None,
          "location_id": 394,
          "location_name": "Makati",
          "department_id": 17361,
          "department_name": "prod-defect-136",
          "rank_id": 2499,
          "rank_name": "R&F",
          "cost_center_id": 298,
          "cost_center_name": "Sample",
          "position_id": 9496,
          "position_name": "Software Developer",
          "address": {
            "address_line_1": "1650 Penafrancia St., Brgy. Valenzuela",
            "address_line_2": "",
            "city": "Makati",
            "zip_code": "1502",
            "country": "Philippines"
          },
          "tax_status": "S",
          "tax_type": "Regular",
          "consultant_tax_rate": None,
          "tin": "132-132-132-132",
          "sss_number": "23-1231231-3",
          "philhealth_number": "45-651321313-2",
          "hdmf_number": "2365-4897-8956",
          "sss_basis": "Gross Income",
          "philhealth_basis": "Gross Income",
          "hdmf_basis": "Gross Income",
          "sss_amount": None,
          "philhealth_amount": None,
          "hdmf_amount": None,
          "rdo": "047",
          "active": "active",
          "non_taxed_other_income_limit": "90000.0000"
        }
      ]
    }
  }
}

MOCK_ATTENDANCE = [
        {"date":"2019-07-26","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-07-27","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-07-28","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-07-29","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-07-30","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-07-31","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-01","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-02","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-03","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-04","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-05","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-06","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-07","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-08","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-09","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-10","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-11","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-12","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-13","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-14","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-15","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-16","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-17","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-18","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-19","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-20","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-21","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-22","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-23","hr_types":{"316":{"REGULAR":480}},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-24","hr_types":{},"scheduled_hours":480,"uid":1378},
        {"date":"2019-08-25","hr_types":{},"scheduled_hours":480,"uid":1378}
    ]

MOCK_JOBS_DATA = json.loads(
"""
{
	"links": {
		"self": "/jobs/29c03027-1cd1-435d-93d3-dd5f55d57847"
	},
	"data": {
		"type": "jobs",
		"id": "29c03027-1cd1-435d-93d3-dd5f55d57847",
		"attributes": {
			"createdAt": "1562055487",
			"expiresAt": "1569831487",
			"payload": {
				"name": "compute-payroll",
				"payrollId": "177",
				"attendanceJobId": "b8fae564-0e3c-4d3b-974d-c7247edad580",
				"allowancesJobId": null,
				"bonusesJobId": null,
				"commissionsJobId": null,
				"deductionsJobId": null
			},
			"status": "FINISHED",
			"tasksCount": "22",
			"tasksDone": "22",
			"updatedAt": "1562055509"
		},
		"relationships": {
			"errors": {
				"data": [{
					"type": "job-errors",
					"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2126"
				}],
				"links": {
					"self": "/job-errors?filter[job_id]=29c03027-1cd1-435d-93d3-dd5f55d57847"
				}
			},
			"results": {
				"data": [{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2114"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2115"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2116"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2117"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2118"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2119"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2120"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2121"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2122"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2123"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2124"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2125"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2127"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2128"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2129"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2130"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2131"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2132"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2133"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2134"
					},
					{
						"type": "job-results",
						"id": "29c03027-1cd1-435d-93d3-dd5f55d57847:2135"
					}
				],
				"links": {
					"self": "/job-results?filter[job_id]=29c03027-1cd1-435d-93d3-dd5f55d57847"
				}
			}
		}
	}
}
"""
)

MOCK_EMPLOYEE_EMAIL = {
    "id": 1,
    "first_name": "John",
    "email": "test@gmail.com"
}

def get_mock_streaming_body(data):
    mock_stream = mock.Mock()
    mock_stream.decode.return_value = data
    return mock_stream


def get_mock_employee_info(timesheet_required=False, hours_per_day='8.00'):
    mock_employee = {
        'time_attendance': {
            'timesheet_required': timesheet_required
        },
        'payroll_group': {
            'hours_per_day': hours_per_day
        }
    }
    return mock_employee


class TasksTest(TestCase):
    """Tasks tests"""

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_ATT_DATA))
    def test_validate_payroll_related_file_task_ok(self, downloader, job_updater, job_error_updater, job_result_updater):
        """Payroll related task"""
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "name": "validate-attendance-file",
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        #job_updater.apply_async.assert_called_once()
        job_result_updater.apply_async.assert_called_once()
        #job_error_updater.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_ATT_DATA_WITH_BLANKS))
    def test_validate_payroll_related_file_task_ok_with_blanks(self, downloader, job_updater, job_error_updater, job_result_updater):
        """Payroll related task"""
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "name": "validate-attendance-file",
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        #job_updater.apply_async.assert_called_once()
        job_result_updater.apply_async.assert_called_once()
        #job_error_updater.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_ATT_DATA_ERROR))
    def test_validate_payroll_related_file_task_not_ok(self, downloader, job_updater, job_error_updater):
        """Payroll related task"""
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "name": "validate-attendance-file",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        #job_updater.apply_async.assert_called_once()
        job_error_updater.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_ATT_DATA_ALL_BLANKS))
    def test_validate_payroll_related_file_task_blank_not_ok(self, downloader, job_updater, job_error_updater):
        """Payroll related task"""
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "name": "validate-attendance-file",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        #job_updater.apply_async.assert_called_once()
        job_error_updater.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_BONUSES_DATA))
    def test_validate_payroll_related_file_task_bonuses_ok(self, downloader, updater, result_updater, job_error_updater):
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "name": "validate-bonus-file",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        #updater.apply_async.assert_called_once()
        result_updater.apply_async.assert_called_once()
        job_error_updater.apply_async.assert_not_called()

    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_DEDUCTIONS_DATA))
    def test_validate_payroll_related_file_task_deduction_ok(self, downloader, updater, result_updater, job_error_updater):
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "name": "validate-deduction-file",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        #updater.apply_async.assert_called_once()
        result_updater.apply_async.assert_called_once()
        job_error_updater.apply_async.assert_not_called()

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_ALLOWANCES_DATA))
    def test_validate_payroll_related_file_task_allowance_ok(self, downloader, updater, error_updater, result_updater):
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "name": "validate-allowance-file",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        # updater.apply_async.assert_called_once()
        result_updater.apply_async.assert_called_once()

    @mock.patch('payroll.apicalls.get_payroll_settings_generic', return_value=MOCK_PG_DATA)
    def test_get_payroll_group_employees_min_data(self, mock_apicall):
        resp = tasks.get_payroll_group_employees_min_data.s({
            "jobId": "1234",
            "name": "validate-attendance-file",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "payrollId": 12,
            "companyId": 1,
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
        }).apply().get()
        mock_apicall.assert_called_once_with(prid=12, with_employees=True)
        assert resp.get("payrollGroupEmployees")

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @mock.patch('payroll._tasks.taskutils.update_job')
    @mock.patch('core.fileutils.download_file', return_value=get_mock_streaming_body(MOCK_ALLOWANCES_DATA_NO_MNAME))
    def test_validate_payroll_related_file_task_allowance_no_mname_ok(self, downloader, updater, error_updater, result_updater):
        tasks.validate_payroll_related_file.s({
            "jobId": "1234",
            "name": "validate-allowance-file",
            "fileBucket": "foo",
            "fileKey": "bar",
            "payrollGroupId": 1,
            "companyId": 1,
            "startDate": "2019-01-01",
            "endDate": "2019-01-30",
            "payrollGroupEmployees": {
                "TRAIN-001": (1, "Ma.Regina", "Jones", "Weber", True)
            }

        }).apply()

        downloader.assert_called_once_with("foo", "bar")
        # updater.apply_async.assert_called_once()
        result_updater.apply_async.assert_called_once()


    @mock.patch('payroll.apicalls.get_attendance_data', return_value=MOCK_ATTENDANCE[:-1])
    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    @skip('deprecated since ticket #8264')
    def test_get_attendance_service_data_task_ok(self, mock_error, mock_result, mock_apicall):
        params = {
            "startDate": "2019-07-26",
            "endDate": "2019-08-25",
            "employeeUID": 1378,
            "employeeID": "ID1",
            "jobId": "testjob",
            "fullName": "foo",
        }

        tasks.get_attendance_service_data.s(params).apply()
        mock_apicall.assert_called_once_with(1378, "2019-07-26", "2019-08-25")
        mock_error.apply_async.assert_called_once_with(args=(
            "testjob",
            {
                "incomplete_attendance": [
                    {
                        "code": "VE-203",
                        "name": "Processing Error",
                        "details": [
                            {
                                "employeeId": "ID1",
                                "fullName": "foo",
                                "description": "Missing Attendance Data",
                                "details": ["08/25/2019"]
                            }
                        ]
                    }
                ]
            },
            "1378"
        ), kwargs = {}, queue='payroll')


    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    def test_tuple_error_should_not_happen(self, mock_error, mock_result):
        params =  {
            'name': 'compute-payroll',
            'payrollId': '163',
            'attendanceJobId': 'cbee9903-e47d-4875-b0db-6f1cc9bae8c4',
            'allowancesJobId': None,
            'bonusesJobId': None,
            'commissionsJobId': None,
            'deductionsJobId': None,
            'jobId': 'dbdcd62e-b3f4-40b0-8ace-0a9c89072499',
            'payrollSettings': {
                'payrollStartDate': '2019-10-26',
                'payrollEndDate': '2019-11-25',
                'attendanceStartDate': '2019-10-26',
                'attendanceEndDate': '2019-11-25',
                'postingDate': '2019-11-25',
                'payRunDate': '2019-11-29',
                'employeeIds': [2136, 2137, 2138, 2139, 2140, 2141, 2142],
                'payrollJobs': {
                    'ATTENDANCE': {
                        'job_id': 'cbee9903-e47d-4875-b0db-6f1cc9bae8c4',
                        'job_file': '10-26-2019_to_11-25-2019-524-1490-156198243623975d19f5e43a879470650265.csv'
                    },
                    'CALCULATION': {
                        'job_id': 'dbdcd62e-b3f4-40b0-8ace-0a9c89072499',
                        'job_file': ''
                    },
                },
                'payrollGroupInfo': {
                    'id': '1304',
                    'accountId': '524',
                    'companyId': '711',
                    'payrollGroupName': '908_monthly',
                    'payrollFrequency': 'MONTHLY',
                    'dayFactor': 260,
                    'cutoffDate': '2019-09-25',
                    'cutoffDate2': None,
                    'hoursPerDay': 8,
                    'contributionSchedules': {
                        'SSS': 'EVERY_PAY',
                        'HDMF': 'EVERY_PAY',
                        'PHILHEALTH': 'EVERY_PAY'
                    },
                    'wtaxSchedule': 'EVERY_PAY',
                    'wtaxMethod': 'BASED_ON_ACTUAL',
                    'gapLoans': {
                        'enforce': True,
                        'minimumNetTakeHomePay': 4
                    }
                },
                'dayHourRates': {
                    'REGULAR': 1,
                    'PAID_LEAVE': 1,
                    'UNPAID_LEAVE': 1,
                    'SH': 1.3, 'RH': 2,
                    '2RH': 3,
                    'NT': 1.1,
                    'SH+NT': 1.43,
                    'RH+NT': 2.2,
                    '2RH+NT': 3.3,
                    'OT': 1.25,
                    'SH+OT': 1.69,
                    'RH+OT': 2.6,
                    '2RH+OT': 3.9,
                    'NT+OT': 1.375,
                    'SH+NT+OT': 1.859,
                    'RH+NT+OT': 2.86,
                    '2RH+NT+OT': 4.29,
                    'RD': 1.3,
                    'RD+SH': 1.5,
                    'RD+RH': 2.6,
                    'RD+2RH': 3.9,
                    'RD+NT': 1.43,
                    'RD+SH+NT': 1.65,
                    'RD+RH+NT': 2.86,
                    'RD+2RH+NT': 4.29,
                    'RD+OT': 1.69,
                    'RD+SH+OT': 1.95,
                    'RD+RH+OT': 3.38,
                    'RD+2RH+OT': 5.07,
                    'RD+NT+OT': 1.859,
                    'RD+SH+NT+OT': 2.145,
                    'RD+RH+NT+OT': 3.718,
                    'RD+2RH+NT+OT': 5.577,
                    'RH+SH': 2.6,
                    'RH+SH+NT': 2.86,
                    'RD+RH+SH': 3,
                    'RH+SH+OT': 3.38,
                    'RH+SH+NT+OT': 3.718,
                    'RD+RH+SH+NT': 3.3,
                    'RD+RH+SH+OT': 3.9,
                    'RD+RH+SH+NT+OT': 4.29
                }
            },
            'employeeUID': '2137',
            'allowances': None,
            'bonuses': None,
            'commissions': None,
            'deductions': None,
            'employeeInfo': {
                'accountId': '524',
                'companyId': '711',
                'payrollGroupId': '1304',
                'payrollGroupName': '908_monthly',
                'employeeId': 'monthly002',
                'firstName': 'Monthly Emp B',
                'middleName': '',
                'lastName': 'Scenario two',
                'email': 'salqateam01+monthly002@gmail.com',
                'gender': 'female',
                'birthDate': '1984-10-15',
                'telephoneNumber': '4425159',
                'mobileNumber': '(63)9954132492',
                'hoursPerDay': '8.00',
                'dateHired': '2010-10-16',
                'dateEnded': None,
                'locationId': '631',
                'locationName': 'salqajeff_location',
                'departmentId': '1391',
                'departmentName': 'Department Parent1556085724',
                'rankId': '1134',
                'rankName': 'Junior. Quality Assurance Analyst',
                'costCenterId': '',
                'costCenterName': None,
                'positionId': '',
                'positionName': None,
                'address': '1st  Street Brgy 12',
                'city': 'Baliuag',
                'zipCode': '14501',
                'country': 'Philippines',
                'taxStatus': 'S2',
                'taxType': 'Regular',
                'taxRate': None,
                'tin': '132-456-789-019',
                'sssNumber': '08-7451477-8',
                'philhealthNumber': '83-560045513-0',
                'hdmfNumber': '4177-3254-4806',
                'contributionBasis': {
                    'SSS': {
                        'type': 'BASIC',
                        'amount': '0.00000000'
                    },
                    'PHILHEALTH': {
                        'type': 'BASIC',
                        'amount': '0.00000000'
                    },
                    'HDMF': {
                        'type': 'BASIC',
                        'amount': '0.00000000'
                    }
                },
                'rdo': '27',
                'active': "inactive",
                'nonTaxedOtherIncomeLimit': '90000.0000',
                'basePay': [
                    {
                        'id': '2142',
                        'type': 'base_pay',
                        'attributes': {
                            'amount': '35000.00000000',
                            'unit': 'MONTHLY',
                            'effectiveDate': '2010-10-16'
                        }
                    }
                ]
            },
            'aabcd': {
                'adjustment': [
                    {
                        'type': 'adjustment',
                        'name': 'Test',
                        'reason': 'Test',
                        'category': 'NON_TAXABLE_INCOME',
                        'fully_taxable': False,
                        'max_non_taxable': 0,
                        'amount_details': {'type': 'FIXED', 'amount': 500},
                        'frequency_type': 'ONE_TIME',
                        'frequency_schedule': None,
                        'release_details': [
                            {
                                'id': 21,
                                'date': '2019-10-26',
                                'disburse_through_special_pay_run': False,
                                'status': False
                            }
                        ],
                        'meta': {
                            'id': 8,
                            'subtype_id': 5,
                            'subtype_name': 'PhilippineAdjustment',
                            'other_income_type_id': 4,
                            'other_income_type_subtype_type_id': 2
                        }
                    },
                    {
                        'type': 'adjustment',
                        'name': 'test1',
                        'reason': 'test1',
                        'category': 'NON_TAXABLE_INCOME',
                        'fully_taxable': False,
                        'max_non_taxable': 0,
                        'amount_details': {'type': 'FIXED', 'amount': 100},
                        'frequency_type': 'ONE_TIME',
                        'frequency_schedule': None,
                        'release_details': [
                            {
                                'id': 22,
                                'date': '2019-10-26',
                                'disburse_through_special_pay_run': False,
                                'status': False
                            }
                        ],
                        'meta': {
                            'id': 9,
                            'subtype_id': 6,
                            'subtype_name': 'PhilippineAdjustment',
                            'other_income_type_id': 4,
                            'other_income_type_subtype_type_id': 2
                        }
                    }
                ]
            },
            'attendanceData': {
                '2019-10-26': {'SCHEDULED_HOURS': 0.0},
                '2019-10-27': {'SCHEDULED_HOURS': 0.0},
                '2019-10-28': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-10-29': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-10-30': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-10-31': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-01': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-02': {'SCHEDULED_HOURS': 0.0},
                '2019-11-03': {'SCHEDULED_HOURS': 0.0},
                '2019-11-04': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-05': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-06': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-07': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-08': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-09': {'SCHEDULED_HOURS': 0.0},
                '2019-11-10': {'SCHEDULED_HOURS': 0.0},
                '2019-11-11': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-12': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-13': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-14': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-15': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-16': {'SCHEDULED_HOURS': 0.0},
                '2019-11-17': {'SCHEDULED_HOURS': 0.0},
                '2019-11-18': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-19': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-20': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-21': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-22': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
                '2019-11-23': {'SCHEDULED_HOURS': 0.0},
                '2019-11-24': {'SCHEDULED_HOURS': 0.0},
                '2019-11-25': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}
            },
            'contributionRecord': {'SSS': [], 'HDMF': [], 'PHILHEALTH': []}
        }

        tasks.compute_pay.s(params).apply()

    @mock.patch('payroll._tasks.payroll_tasks.create_payroll_register')
    @mock.patch('payroll._tasks.payroll_tasks.revert_payroll_status')
    @mock.patch('payroll.apicalls.get_job', return_value=MOCK_JOBS_DATA)
    @mock.patch('payroll.apicalls.get_job_results')
    @mock.patch('payroll.apicalls.save_payroll_item')
    @mock.patch('payroll.apicalls.open_payroll')
    def test_open_payroll_should_not_open_payroll(self, mock_open, mock_save, mock_get_jr, mock_get_job, mock_revert, mock_register):
        tasks.open_payroll.s({"jobId": 'fakeJobId', "payrollId":  1}).apply()
        mock_get_job.assert_called_once_with('fakeJobId')
        mock_get_jr.assert_not_called()
        mock_save.assert_not_called()
        mock_open.assert_not_called()
        mock_revert.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.add_job_result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    def test_hours_per_day_conversion_error_should_not_happen(self, mock_error, mock_result):
        params = test_data.MOCK_DATA_NO_HOURS_PER_DAY
        tasks.compute_pay.s(params).apply()
        mock_result.apply_async.assert_called_once()

    @mock.patch('payroll._tasks.taskutils.job_update')
    @mock.patch('payroll.apicalls.get_employees_by_ids', return_value={"data":[MOCK_EMPLOYEE_EMAIL]})
    @mock.patch('payroll._tasks.payslip_tasks.email_employee_payslip')
    def test_special_payroll_send_email(self, mock_send_email, mock_employee, mock_job_update):
        params = {
            "jobId": "123456",
            "companyId": 11,
            "payrollGroupId": None,
            "startDate": None,
            "endDate": None,
            "employeeIds": [MOCK_EMPLOYEE_EMAIL.get("id")],
            "employeeIdsToNotifyEss": [],
            "emailType": "email-payslip",
            "payrollDate": "2020-09-30"
        }

        tasks.get_payrollgroup_emails.s(params).apply()

        mock_employee.assert_called()
        mock_employee.assert_called()
        mock_send_email.apply_async.assert_called_once()

    @mock.patch('attendance.calls._get_employee_info', return_value=get_mock_employee_info())
    @mock.patch('payroll.apicalls.get_attendance_data', return_value=MOCK_ATTENDANCE[:-2])
    @mock.patch('payroll._tasks.taskutils.result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    def test_get_attendance_service_data_without_attendance_success(self, mock_error, mock_result, mock_apicall, mock_employee_info):
        params = {
            "startDate": "2019-07-26",
            "endDate": "2019-08-25",
            "employeeUID": 1378,
            "employeeID": "ID1",
            "jobId": "testjob",
            "fullName": "foo",
        }

        tasks.get_attendance_service_data.s(params).apply()
        mock_employee_info.assert_called_once_with(1378)
        mock_apicall.assert_called_once_with(1378, "2019-07-26", "2019-08-25")
        mock_error.assert_not_called()
        mock_result.assert_called_once()
    

    @mock.patch('attendance.calls._get_employee_info', return_value=get_mock_employee_info())
    @mock.patch('payroll.apicalls.get_attendance_data', return_value=MOCK_ATTENDANCE[:10])
    @mock.patch('payroll._tasks.taskutils.result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    def test_get_attendance_service_data_without_attendance_success_employee_tsnr(self, mock_error, mock_result, mock_apicall, mock_employee_info):
        params = {
            "startDate": "2019-07-26",
            "endDate": "2019-08-09",
            "employeeUID": 1378,
            "employeeID": "ID1",
            "jobId": "testjob",
            "fullName": "foo",
        }
        entries = {
            '2019-07-26':{'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-07-27': {'SCHEDULED_HOURS': 0},
            '2019-07-28': {'SCHEDULED_HOURS': 0},
            '2019-07-29': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-07-30': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-07-31': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-01': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-02': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-03': {'SCHEDULED_HOURS': 0},
            '2019-08-04': {'SCHEDULED_HOURS': 0},
            '2019-08-09': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-05': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-07': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-08': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-06': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0}
        }

        tasks.get_attendance_service_data.s(params).apply()
        mock_apicall.assert_called_once_with(1378, "2019-07-26", "2019-08-09")
        mock_error.assert_not_called()
        mock_employee_info.assert_called_once_with(1378)
        mock_result.assert_called_once_with('testjob', {'1378': entries}, '1378')
    

    @mock.patch('attendance.calls._get_employee_info', return_value=get_mock_employee_info(True))
    @mock.patch('payroll.apicalls.get_attendance_data', return_value=MOCK_ATTENDANCE[:10])
    @mock.patch('payroll._tasks.taskutils.result')
    @mock.patch('payroll._tasks.taskutils.add_job_error')
    def test_get_attendance_service_data_without_attendance_success_employee_tsr(self, mock_error, mock_result, mock_apicall, mock_employee_info):
        params = {
            "startDate": "2019-07-26",
            "endDate": "2019-08-09",
            "employeeUID": 1378,
            "employeeID": "ID1",
            "jobId": "testjob",
            "fullName": "foo",
        }
        entries = {
            '2019-07-26': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-07-27': {'SCHEDULED_HOURS': 0},
            '2019-07-28': {'SCHEDULED_HOURS': 0},
            '2019-07-29': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-07-30': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-07-31': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-01': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-02': {'REGULAR': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-03': {'SCHEDULED_HOURS': 0},
            '2019-08-04': {'SCHEDULED_HOURS': 0},
            '2019-08-07': {'ABSENT': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-08': {'ABSENT': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-09': {'ABSENT': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-05': {'ABSENT': 8.0, 'SCHEDULED_HOURS': 8.0},
            '2019-08-06': {'ABSENT': 8.0, 'SCHEDULED_HOURS': 8.0}
        }

        tasks.get_attendance_service_data.s(params).apply()
        mock_apicall.assert_called_once_with(1378, "2019-07-26", "2019-08-09")
        mock_error.assert_not_called()
        mock_employee_info.assert_called_once_with(1378)
        mock_result.assert_called_once_with('testjob', {'1378': entries}, '1378')
