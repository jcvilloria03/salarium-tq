# pylint: disable=all
from unittest import TestCase, mock
from payroll import constants
from payroll import tasks

TEST_CONTEXT = {
    "companyId": 123,
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True, "sss", "hdmf"),
        "TEST0001": (2, "Monthly", "Test", "Test", True, "sss1", "hdmf1"),
    },
    "companyPayrollGroups": [
        { "id": 5, "payroll_frequency": "MONTHLY",
          "employee_ids": [1]
        }
    ],
    "typeList": [
        {
            "company_id": 0,
            "id": 1,
            "is_editable": 0,
            "name": "SSS"
        },
        {
            "company_id": 0,
            "id": 2,
            "is_editable": 0,
            "name": "Pag-ibig"
        },
        {
            "company_id": 0,
            "id": 3,
            "is_editable": 0,
            "name": "Gap"
        },
        {
            "company_id": 1072,
            "id": 3,
            "is_editable": 0,
            "name": "Uber Loan"
        }
    ]
}

GOV_LOAN_OK_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""
class PayrollLoanTasksTest(TestCase):

    @mock.patch('core.fileutils.download_file',
        return_value=b'')
    def test_validate_loans_upload_no_content(self, downloader):
        mock_params = {
            "name": constants.WORKFLOW_VALIDATE_EMPLOYEE_LOAN_FILE,
            "bucket": "mockbucket",
            "fileKey": "mockfile",
            "jobId": "mockId",

        }

        with self.assertRaises(ValueError) as context:
            tasks.validate_loans_upload(mock_params)

        assert "No record found on template" in str(context.exception)

    @mock.patch('core.fileutils.download_file',
        return_value=bytes(GOV_LOAN_OK_DATA, "utf-8"))
    @mock.patch('payroll._tasks.taskutils.job_update', return_value=True)
    @mock.patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    @mock.patch('payroll._tasks.payroll_loan_tasks.save_loan_item', return_value=None)
    def test_validate_loans_upload_ok_content(self, saver, preview_saver, job_updater, downloader):
        mock_params = {
            "name": constants.WORKFLOW_VALIDATE_EMPLOYEE_LOAN_FILE,
            "bucket": "mockbucket",
            "fileKey": "mockfile",
            "jobId": "mockId",
            **TEST_CONTEXT
        }

        tasks.validate_loans_upload(mock_params)
        job_updater.assert_called_once()
        preview_saver.assert_called_once()
        saver.apply_async.assert_called_once()
