"""Payroll task unit tests"""
# pylint: disable=all
import math
from unittest import TestCase
from payroll import filevalidators
from unittest.mock import patch

TEST_CONTEXT = {
    "companyId": 123,
    "companyEmployees" : {
        "TEST1": (1, "Marlon", "Grumble", "Brando", True, "sss", "hdmf"),
        "TEST0001": (2, "Monthly", "Test", "Test", True, "sss1", "hdmf1"),
    },
    "companyPayrollGroups": [
        { "id": 5, "payroll_frequency": "MONTHLY",
          "employee_ids": [1]
        }
    ],
    "typeList": [
        {
            "company_id": 0,
            "id": 1,
            "is_editable": 0,
            "name": "SSS"
        },
        {
            "company_id": 0,
            "id": 2,
            "is_editable": 0,
            "name": "Pag-ibig"
        },
        {
            "company_id": 0,
            "id": 3,
            "is_editable": 0,
            "name": "Gap"
        },
        {
            "company_id": 1072,
            "id": 3,
            "is_editable": 0,
            "name": "Uber Loan"
        }
    ]
}

TEST_CONTEXT_2 = {
    "companyId": 123,
    "companyEmployees" : {
        "TESTINVALID-1001": (1,"Grace","Gracie", "", True, "1", "1"),
        "TESTINVALID-1002": (2,"Paul","Durham","",True,"2","2"),
        "TESTINVALID-1003": (3,"David","Boyle", "",True, "3", "3"),
        "TESTINVALID-1004": (4,"Jake","Diaz", "",True, "4", "4"),
        "TESTINVALID-1005": (5,"Burnham","Cake", "",True, "5", "5"),
        "TESTINVALID-1006": (6,"Blends","City", "",True, "6", "6"),
        "TESTINVALID-1007": (7,"Honda","Toyota", "",True, "7", "7"),
        "TESTINVALID-1008": (8,"Pizza","Hut", "", True,"8", "8"),
        "TESTINVALID-1009": (9,"Madley","Rome", "",True, "9", "9"),
        "TESTINVALID-1010": (10,"Paris","Japan", "",True, "10", "10"),
    },
    "companyPayrollGroups": [
        { "id": 5, "payroll_frequency": "MONTHLY",
          "employee_ids": [1]
        }
    ],
    "typeList": [
        {
            "company_id": 0,
            "id": 1,
            "is_editable": 0,
            "name": "SSS"
        },
        {
            "company_id": 0,
            "id": 2,
            "is_editable": 0,
            "name": "Pag-ibig"
        },
        {
            "company_id": 0,
            "id": 3,
            "is_editable": 0,
            "name": "Gap"
        },
        {
            "company_id": 1072,
            "id": 3,
            "is_editable": 0,
            "name": "Uber Loan"
        }
    ]
}

GOV_LOAN_OK_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_MISSING_ID_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_INVALID_ID_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST2,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_MISSING_LOAN_TYPE_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_INVALID_LOAN_TYPE_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,foo,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_INVALID_LOAN_CATEGORY_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,foo,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,Pag-IBIG,foo,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_MISSING_REF_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_DATE_ISSUED_ERRORS_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,fooooooooo,02/02/2019,Every pay of the month,10000,500,20
"""

GOV_REPAYMENT_ERRORS_DATA = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,fooooooo,Every pay of the month,10000,500,20
"""


GOV_TEST_MONTHLY_SCHEME = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the decade,10000,500,20
"""

GOV_TEST_SEMI_MONTHLY_SCHEME = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,First pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Last pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the decade,10000,500,20
"""

GOV_TEST_WEEKLY_SCHEME = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,First pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Last pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,First and last pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the decade,10000,500,20
"""

GOV_AMOUNTS_TEST = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,fooo,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,0,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,-10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,fooo,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000, 0,20
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000, -1000,20
"""

USER_DEFINED_OK_TEST = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,Uber Loan,02/02/2019,Every pay of the month,10000,500,20
"""


GOV_LOAN_OK_DATA_AMORT_TERMS = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Emergency,12345,01/01/2019,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,SSS,Calamity,12345,01/01/2019,02/02/2019,Every pay of the month,10000,,10
TEST1,Brando,Marlon,Grumble,SSS,Educational,12345,01/01/2019,02/02/2019,Every pay of the month,10000,1000,10
TEST1,Brando,Marlon,Grumble,SSS,Stock investment,12345,01/01/2019,02/02/2019,Every pay of the month,10000,1000,10
"""

USER_DEFINED_OK_AMORT_TERMS = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,Uber Loan,02/02/2019,Every pay of the month,10000,1000,10
TEST1,Brando,Marlon,Grumble,Uber Loan,02/02/2019,Every pay of the month,10000,500,20
TEST1,Brando,Marlon,Grumble,Uber Loan,02/02/2019,Every pay of the month,10000,1000,10
"""

GOV_LOAN_OK_DATA_AMORT_TERMS_NOT_OK = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST1,Brando,Marlon,Grumble,SSS,Salary,12345,01/01/2019,02/02/2019,Every pay of the month,10000,1000,
"""

JON_SHEN_DATA_SET_1 = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TEST0001,Monthly,Test,,SSS,Calamity,123456789,01/01/2019,10/01/2019,Every pay of the month,10000,100,0"""

TIN_BALATICO_DATA_SET_1 = """Employee ID,Last Name,First Name,Middle Name,Loan Type,Loan Category,Reference Number,Date Issued,Repayment Start Date,Payment Scheme,Amount of Loan,Monthly Amortization,Loan Terms (Months)
TESTINVALID-1001,Grace,Gracie,,Pag-ibig,Multi purpose,100000001,09/16/2019,10/01/2019,Every pay of the month,10000,416.67,24
TESTINVALID-1002,Paul,Durham,,Pag-ibig,Short term,100000002,09/16/2019,10/01/2019,Every pay of the month,10250,427.09,24
TESTINVALID-1003,David,Boyle,,Pag-ibig,Calamity,100000003,09/16/2019,10/01/2019,Every pay of the month,10500,437.5,24
TESTINVALID-1004,Jake,Diaz,,Pag-ibig,Short term,100000004,09/16/2019,10/01/2019,Every pay of the month,10750,447.92,24
TESTINVALID-1005,Burnham,Cake,,Pag-ibig,Short term,100000005,09/16/2019,10/01/2019,Every pay of the month,11000,458.34,24
TESTINVALID-1006,Blends,City,,Pag-ibig,Calamity,100000006,09/16/2019,10/01/2019,Every pay of the month,11250,468.75,24
TESTINVALID-1007,Honda,Toyota,,Pag-ibig,Short term,100000007,09/16/2019,10/01/2019,Last pay of the month,11500,479.17,24
TESTINVALID-1008,Pizza,Hut,,Pag-ibig,Multi purpose,100000008,09/16/2019,10/01/2019,First pay of the month,12250,510.42,24
TESTINVALID-1009,Madley,Rome,,Pag-ibig,Calamity,100000009,09/16/2019,10/01/2019,Every pay of the month,13000,541.67,24
TESTINVALID-1010,Paris,Japan,,Pag-ibig,Short term,100000010,09/16/2019,10/01/2019,Every pay of the month,14750,614.59,24
TESTINVALID-1002,Paul,Durham,,SSS,Salary,100000002,10/01/2019,10/01/2019,Every pay of the month,32000,1333.34,24
TESTINVALID-1005,Burnham,Cake,,SSS,Calamity,100000005,10/01/2019,10/01/2019,Every pay of the month,5000,5000,1
TESTINVALID-1006,Blends,City,,SSS,Salary,100000006,09/01/2019,09/01/2019,Every pay of the month,5000,2500,2
"""
class GovernmentLoanUploadTest(TestCase):
    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_government_ok_1(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_LOAN_OK_DATA.splitlines())
        print(validator._errors)
        assert not validator.has_errors()

    @patch('payroll.apicalls.validate_loan_item_preview', side_effect=Exception("Unknown"))
    def test_government_validator_returns_error(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_LOAN_OK_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        assert validator._errors.get('2')[0]['details'][0] == 'Unknown'


    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_government_not_ok_no_monthly(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_LOAN_OK_DATA_AMORT_TERMS.splitlines())
        print(validator._errors)
        assert validator.has_errors()

    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_user_defined_ok_1(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(USER_DEFINED_OK_TEST.splitlines())
        print(validator._errors)
        assert not validator.has_errors()

    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_user_defined_ok_2(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(USER_DEFINED_OK_AMORT_TERMS.splitlines())
        print(validator._errors)
        assert not validator.has_errors()

    def test_government_missing_amort_terms(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_LOAN_OK_DATA_AMORT_TERMS_NOT_OK.splitlines())
        print(validator._errors)
        assert validator.has_errors()

    def test_missing_id_1(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_MISSING_ID_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error1 = validator._errors.get("0")[0]
        assert error1
        assert error1.get("code") == "VE-203"
        error1_details = error1.get("details")[0]
        assert error1_details.get("description") == "No record found in template"
        error2 = validator._errors.get("2")[0]
        assert error2
        assert error2.get("code") == "VE-100"

    def test_invalid_id_1(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_INVALID_ID_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error1 = validator._errors.get("0")[0]
        assert error1
        assert error1.get("code") == "VE-203"
        error1_details = error1.get("details")[0]
        assert error1_details.get("description") == "No record found in template"
        error2 = validator._errors.get("2")[0]
        assert error2
        assert error2.get("code") == "VE-200"

    def test_invalid_loan_type(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_INVALID_LOAN_TYPE_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("code") == "VE-102"

    def test_missing_loan_type(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_MISSING_LOAN_TYPE_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("code") == "VE-100"


    def test_invalid_loan_category_1(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_INVALID_LOAN_CATEGORY_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("code") == "VE-102"
        error4 = validator._errors.get("4")[0]
        assert error4
        assert error4.get("code") == "VE-102"

    def test_missing_reference(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_MISSING_REF_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("code") == "VE-100"

    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_date_issued(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_DATE_ISSUED_ERRORS_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("code") == "VE-100"
        error4 = validator._errors.get("4")[0]
        assert error4
        assert error4.get("code") == "VE-101"

    def test_repayment(self):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_REPAYMENT_ERRORS_DATA.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("code") == "VE-100"
        error4 = validator._errors.get("4")[0]
        assert error4
        assert error4.get("code") == "VE-101"

    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_payment_scheme_monthly(self, saver):
        #Todo actual data gets Invalid payment scheme for Monthly payroll frequency
        #test monthly
        monthly_context = dict(TEST_CONTEXT)
        monthly_context["companyPayrollGroups"][0]["payroll_frequency"] = "MONTHLY"
        validator = filevalidators.EmployeeLoansFileUploadValidator(monthly_context)
        validator.iterate_source(GOV_TEST_MONTHLY_SCHEME.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error3 = validator._errors.get("3")[0]
        assert error3
        assert error3.get("code") == "VE-102"

        #test semi-monthly
        semimonthly_context = dict(TEST_CONTEXT)
        semimonthly_context["companyPayrollGroups"][0]["payroll_frequency"] = "SEMI_MONTHLY"
        validator = filevalidators.EmployeeLoansFileUploadValidator(semimonthly_context)
        validator.iterate_source(GOV_TEST_SEMI_MONTHLY_SCHEME.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors) == 1
        error5 = validator._errors.get("5")[0]
        assert error5
        assert error5.get("code") == "VE-102"


        #test semi-monthly
        semimonthly_context = dict(TEST_CONTEXT)
        semimonthly_context["companyPayrollGroups"][0]["payroll_frequency"] = "WEEKLY"
        validator = filevalidators.EmployeeLoansFileUploadValidator(semimonthly_context)
        validator.iterate_source(GOV_TEST_WEEKLY_SCHEME.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors) == 1
        error6 = validator._errors.get("6")[0]
        assert error6
        assert error6.get("code") == "VE-102"

    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_currencies(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(GOV_AMOUNTS_TEST.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        assert len(validator._errors) == 6

    def test_jon_shen_scenario_1(self):
        """Terms is 0"""
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT)
        validator.iterate_source(JON_SHEN_DATA_SET_1.splitlines())
        print(validator._errors)
        assert validator.has_errors()
        error2 = validator._errors.get("2")[0]
        assert error2.get("code") == "VE-203"
        assert error2.get("details")[0] == "Invalid value for field Loan Terms (Months). This field can only accept numeric values greater than 0."


    @patch('payroll.apicalls.validate_loan_item_preview', return_value={"uid": "ok"})
    def test_tin_balatico_scenario_1(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT_2)
        validator.iterate_source(TIN_BALATICO_DATA_SET_1.splitlines())

        assert not validator.has_errors()
        # for item in validator.result:
        #     assert int( math.ceil(item.get('total_amount')/item.get('monthly_amortization'))) == item.get("term")

    @patch('payroll.apicalls.validate_loan_item_preview', side_effect=Exception("{\"test\": [\"test\"]}"))
    def test_preview_json_string_error_scenario_2(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT_2)
        validator.iterate_source(TIN_BALATICO_DATA_SET_1.splitlines())
        assert validator.has_errors()

    @patch('payroll.apicalls.validate_loan_item_preview', side_effect=Exception("test"))
    def test_preview_string_error_scenario_2(self, saver):
        validator = filevalidators.EmployeeLoansFileUploadValidator(TEST_CONTEXT_2)
        validator.iterate_source(TIN_BALATICO_DATA_SET_1.splitlines())
        assert validator.has_errors()
