# pylint: disable=C0111
from unittest import TestCase, mock
from attendance.calls import get_timesheet, _get_timesheet
#import datetime

class MockResponse:
    def __init__(self, json_data, status_code):
        self.text = str(json_data)
        self.json_data = json_data
        self.status_code = status_code

    def raise_for_status(self):
        pass

    def json(self):
        return self.json_data


class TestGetTimeSheet(TestCase):
    """Timesheet test cases"""

    @mock.patch('attendance.calls._get_timesheet', return_value=[])
    def test_simple(self, fetcher):
        get_timesheet('1', '2018-01-01')
        fetcher.assert_called_once()

    @mock.patch('attendance.calls._get_timesheet',
                return_value=[
                    {"company_uid": "2", "employee_uid": 2,
                     "state": True, "timestamp": 1},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": False, "timestamp": 2}
                ]
                )
    def test_simple2(self, fetcher):
        data = get_timesheet(1, '2018-01-01')
        assert len(data) == 2
        fetcher.assert_called_once_with(1, '2018-01-01', None, None)


    @mock.patch('attendance.calls._get_timesheet',
                return_value=[
                    {"company_uid": "2", "employee_uid": 2,
                     "state": True, "timestamp": 1},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": True, "timestamp": 2},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": False, "timestamp": 3}
                ]
                )
    def test_simple3(self, fetcher):
        data = get_timesheet(1, '2018-01-01')
        assert len(data) == 3
        fetcher.assert_called_once_with(1, '2018-01-01', None, None)
        item1 = data[0]
        assert item1["timestamp"] == 1
        item2 = data[1]
        assert item2["timestamp"] == 2
        item3 = data[2]
        assert item3["timestamp"] == 3



    @mock.patch('attendance.calls._get_timesheet',
                return_value=[
                    {"company_uid": "2", "employee_uid": 2,
                     "state": True, "timestamp": 1},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": False, "timestamp": 2},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": False, "timestamp": 3}
                ]
                )
    def test_simple4(self, fetcher):
        data = get_timesheet(1, '2018-01-01')
        assert len(data) == 3
        fetcher.assert_called_once_with(1, '2018-01-01', None, None)
        item1 = data[0]
        assert item1["timestamp"] == 1
        item2 = data[1]
        assert item2["timestamp"] == 2


    @mock.patch('attendance.calls._get_timesheet',
                return_value=[
                    {"company_uid": "2", "employee_uid": 2,
                     "state": True, "timestamp": 1},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": False, "timestamp": 2},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": True, "timestamp": 3},
                    {"company_uid": "2", "employee_uid": 2,
                     "state": False, "timestamp": 4}
                ]
                )
    def test_simple5(self, fetcher):
        data = get_timesheet(1, '2018-01-01', None, None)
        assert len(data) == 4
        fetcher.assert_called_once_with(1, '2018-01-01', None, None)


    @mock.patch('requests.get', return_value=MockResponse(json_data=[], status_code=200))
    @mock.patch('attendance.calls._get_api_url', return_value="/timesheet/%s")
    def test__get_timesheet(self, geturl, getter):
        _get_timesheet(1, '2018-01-01')
        #timestamp = datetime.datetime.strptime('2018-01-01', '%Y-%m-%d').timestamp()
        geturl.assert_called_once()
        getter.assert_called_once()
