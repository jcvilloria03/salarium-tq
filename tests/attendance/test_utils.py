"""Utils unit tests"""
from attendance.spanutils import get_span_range

def test_get_span_range():
    """Test get span range"""
    test_cases = [
        #name, start, end, (i,j)
        ("Test 1", "08:00", "17:00", (480, 1020)),
        ("Test 2", "23:00", "06:00", (1380, 1800)),
    ]

    for name, start, end, result in test_cases:
        assert result == get_span_range(start, end), name
