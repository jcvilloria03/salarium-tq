# pylint: disable=W0212
"""Calculator unit tests"""
import unittest
from unittest import mock
import os
import time
from attendance import calculator, constants, keys
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()

# namespace for is_rest_day method from DefaultCalculator
mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"


class TestScenarioFlexiWithCoreHours(unittest.TestCase):
    """Default Calculator tests
    Flexi schedules with no core hours
    """
    def test_issue_1837(self):
        """Test specs:

        issue: https://code.salarium.com/development/t-and-a-manila/issues/1837

        1. Flexi
        2. With core hours
        3. timesheet required

        shifts: 07:00 - 22:00
        core_hours: 10:00 - 16:00
        clock_in: Thursday, November 1, 2018 8:00:00 AM GMT+08:00
        clock_out: Thursday, November 1, 2018 11:00:00 PM GMT+08:00
        """
        data = utils.get_mock("flexi_core_hours_issue_1837")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_flexi_date_hired(self):
        """Test specs:

        issue: https://code.salarium.com/development/t-and-a-manila/issues/1837

        1. Flexi
        2. With core hours
        3. timesheet required
        4. Date hire is greater than the calculated date
        """
        data = utils.get_mock("flexi_core_hours_date_hired")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_date_ended(self):
        """Test specs:

        issue: https://code.salarium.com/development/t-and-a-manila/issues/1837

        1. Flexi
        2. With core hours
        3. timesheet required
        4. Date ended is greater than the calculated date
        """
        data = utils.get_mock("flexi_core_hours_date_ended")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_with_core_hours_nt(self):
        """Test specs:

        issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/3786

        1. Flexi
        2. With core hours
        3. timesheet required
        """
        data = utils.get_mock("flexi_core_hours_nt_tsr")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_overtime_with_night_differential(self):
        """
        Test Flexi shift with overtime plus NT
        1. Flexi schedule with core hours(05:00 to 23:30)
           > Core Hours: 10:00 to 16:00
        2. TSR is True
        3. Night Time starts at 22:00 to 05:00
        4. Filed Overtime from 17:00 to 23:00(6 Hours)
        5. Employee is entitled to Overtime paid and Night Differential
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()

        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])

        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "05:00",
                keys.END_TIME: "23:30",
                keys.TOTAL_HOURS: "08:00"
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="05:00")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out")
        ])

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 17:00",
                keys.END_DATETIME: "2019-01-01 23:00"
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get(shift_id).get(constants.ATT_OT) == 5 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT_OT) == 1 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT) == None

    def test_overtime_without_night_differential(self):
        """
        Test Flexi shift with overtime
        1. Flexi schedule with core hours(05:00 to 23:30)
           > Core Hours: 10:00 to 16:00
        2. TSR is True
        3. Night Time starts at 22:00 to 05:00
        4. Filed Overtime from 17:00 to 23:00(6 Hours)
        5. Employee is entitled to Overtime paid and Night Differential
        6. Employee is NOT entitled to Night Differential
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=False)

        mock.default_shift()

        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])

        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "05:00",
                keys.END_TIME: "23:30",
                keys.TOTAL_HOURS: "08:00"
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="05:00")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out")
        ])

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 17:00",
                keys.END_DATETIME: "2019-01-01 23:00"
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get(shift_id).get(constants.ATT_OT) == 6 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT_OT) == None
        assert attendance.get(shift_id).get(constants.ATT_NT) == None

    def test_ot_nt_tsr_with_empty_ts(self):
        """
        Test Flexi shift with overtime plus NT
        1. Flexi schedule with core hours(05:00 to 23:30)
           > Core Hours: 10:00 to 16:00
        2. TSR is True
        3. Night Time starts at 22:00 to 05:00
        4. Filed Overtime from 17:00 to 23:00(6 Hours)
        5. Employee is entitled to Overtime paid and Night Differential
        6. Employee has no timesheet records
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()

        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])

        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "05:00",
                keys.END_TIME: "23:30",
                keys.TOTAL_HOURS: "08:00"
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="05:00")

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 17:00",
                keys.END_DATETIME: "2019-01-01 23:00"
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.keys()) == 1
        assert attendance.get(shift_id).get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_night_shift_tsr_nt_regular_overnight_no_nt(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 07:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "23:00",
                "total_hours": "14:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 07:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 13 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 1 * 60

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt_with_ot(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        6. Approved OT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 03:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 18:00",
                keys.END_DATETIME: "2020-02-25 03:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 4 * 60
        assert attendance.get("1").get(constants.ATT_NT_OT) == 5 * 60

    def test_flexi_night_shift_tsr_nt_regular_undertime(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-24 15:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_flexi_night_shift_tsr_nt_regular_ot_after_ts(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Undertime
        5. OT after last clock out
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-24 18:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 19:00",
                keys.END_DATETIME: "2020-02-24 23:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)
        assert not attendance.get("1").get(constants.ATT_OT)

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt_with_ot_beyond_clockout(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        6. Approved OT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-24 22:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 18:00",
                keys.END_DATETIME: "2020-02-24 23:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 4 * 60
        assert not attendance.get("1").get(constants.ATT_NT_OT)

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt_with_ot_before_clockout(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        6. Approved OT (before Clock-OUT)
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 03:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 18:00",
                keys.END_DATETIME: "2020-02-24 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 2 * 60
        assert not attendance.get("1").get(constants.ATT_NT_OT)
        assert not attendance.get("1").get(constants.ATT_NT)
    def test_conflicting_timesheet_entries(self):

        mock_data = {
            "date":"2020-02-28",
            "night_shift_settings":{
                "id":1,
                "company_id":1,
                "activated":True,
                "start":"22:00",
                "end":"06:00",
                "time_shift":"Counter per hour"
            },
            "tardiness_rule":[

            ],
            "id":1,
            "user_id":1,
            "status":"active",
            "employee_id":"1",
            "company_id":1,
            "first_name":"Test",
            "middle_name":"",
            "last_name":"Employee",
            "full_name":"Test Employee",
            "department_id":1,
            "department_name":"Research and Development",
            "position_id":1,
            "position_name":"Web QA",
            "location_id":1,
            "location_name":"Location",
            "hours_per_day":"None",
            "date_hired":"2019-03-18",
            "date_ended": None,
            "employment_type_id":1,
            "employment_type_name":"Regular",
            "timesheet_required":False,
            "overtime":True,
            "differential":True,
            "regular_holiday_pay":True,
            "special_holiday_pay":True,
            "holiday_premium_pay":True,
            "rest_day_pay":True,
            "employee_uid":1,
            "rest_days":[

            ],
            "shifts":[
                {
                    "name":"Flexi Schedule",
                    "type":"flexi",
                    "start_date":"2019-01-01",
                    "start_time":"06:00",
                    "end_time":"22:00",
                    "total_hours":"08:00",
                    "allowed_time_methods":[
                        "Bundy App",
                        "Time Sheet",
                        "Web Bundy",
                        "Biometric"
                    ],
                    "affected_employees":[
                        {
                        "id":1,
                        "schedule_id":1,
                        "type":"employee",
                        "rel_id":"None",
                        "created_at":"2019-02-07 11:39:32",
                        "updated_at":"2019-02-07 11:39:32"
                        }
                    ],
                    "breaks":[

                    ],
                    "core_times":[

                    ],
                    "shift_id":1
                }
            ],
            "default_shift":{

            },
            "hours_worked":[

            ],
            "tardiness_rules":[

            ],
            "shift_dates":{
                "2020-02-28":{
                    "rest_day":False,
                    "holiday_type":None
                }
            },
            "timesheet":[
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1582854420.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1582889280.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1582889280.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1582889280.0
                }
            ],
            "raw_timesheet":[
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "tags":[

                    ],
                    "state":True,
                    "timestamp":1582675200.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1582681860.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "tags":[

                    ],
                    "state":False,
                    "timestamp":1582707600.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1582718880.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1582767780.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1582807800.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1582854420.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1582889280.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1582889280.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1582889280.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1583114100.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1583149680.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1583200140.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1583236140.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1583373360.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1583407980.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1583459640.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1583501400.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":True,
                    "timestamp":1583805600.0
                },
                {
                    "employee_uid":1.0,
                    "company_uid":"1",
                    "state":False,
                    "timestamp":1583836620.0
                }
            ]
        }

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_with_ut(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202#note_466080

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Tardiness Rule
        5. Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "06:00",
                "end_time": "22:00",
                "total_hours": "8:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=15
        );
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "12:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 08:00:00", "clock_in"),
            ("2020-02-24 09:00:00", "clock_out"),
            ("2020-02-24 13:00:00", "clock_in"),
            ("2020-02-24 22:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert not attendance.get("1").get(constants.ATT_TARDY)
        assert not attendance.get("1").get(constants.ATT_NT_OT)
        assert not attendance.get("1").get(constants.ATT_NT)
        assert not attendance.get("1").get(constants.ATT_OT)

    def test_flexi_shift_tsr_with_core_hours_regular_no_ut_scenario1a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 08:00:00", "clock_in"),
            ("2020-02-24 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_UNDERTIME)

    def test_flexi_shift_tsr_with_core_hours_regular_with_ut_scenario2a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 10:00:00", "clock_in"),
            ("2020-02-24 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_with_ut_scenario3a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 10:00:00", "clock_in"),
            ("2020-02-24 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_with_ut_scenario4a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 10:00:00", "clock_in"),
            ("2020-02-24 16:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_with_ut_scenario5a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 08:00:00", "clock_in"),
            ("2020-02-24 10:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 7 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_no_ut_scenario1b(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 08:00:00", "clock_in"),
            ("2020-02-24 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_UNDERTIME)

    def test_flexi_shift_tsr_with_core_hours_regular_no_ut_scenario2b(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 10:00:00", "clock_in"),
            ("2020-02-24 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_no_ut_scenario2c(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 11:00:00", "clock_in"),
            ("2020-02-24 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_no_ut_scenario2d(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 11:00:00", "clock_in"),
            ("2020-02-24 16:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 5 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 3 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_no_ut_scenario2e(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. No Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 12:00:00", "clock_in"),
            ("2020-02-24 20:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_with_ut_with_unpaid_leave(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. With Undertime
        5. Paid Leave
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "unpaid_leave",
                "type_name": "Unpaid Leave",
                "time": "04:00",
                "start_datetime": "2020-02-24 12:00",
                "end_datetime": "2020-02-24 16:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 16:00:00", "clock_in"),
            ("2020-02-24 20:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60

    def test_flexi_shift_tsr_with_core_hours_regular_with_ut_with_paid_leave(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_467749

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. With Undertime
        5. Paid Leave
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "10:00",
                keys.END: "16:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2020-02-24 12:00",
                "end_datetime": "2020-02-24 16:00"
            }
        ])
        mock.timesheet([
            ("2020-02-24 16:00:00", "clock_in"),
            ("2020-02-24 20:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60

    def test_flexi_shift_24hrs_tsr_with_core_hours_nt_regular_ut_scenario1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_470859

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Night Shift
        """
        mock = utils.MockHero(
            date="2020-03-12",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "08:00",
                keys.END: "10:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-03-12 22:00:00", "clock_in"),
            ("2020-03-13 07:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 6 * 60
        assert not attendance.get("1").get(constants.ATT_REGULAR)

    def test_flexi_shift_24hrs_tsr_with_core_hours_nt_regular_ut_scenario3(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_472521

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Undertime
        """
        mock = utils.MockHero(
            date="2020-03-12",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "12:00",
                "end_time": "11:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "17:00",
                keys.END: "23:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-03-12 12:00:00", "clock_in"),
            ("2020-03-12 19:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 4 * 60

    def test_flexi_shift_24hrs_tsr_with_core_hours_nt_regular_ut_scenario4(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_472521

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Undertime
        5. Night Diff
        """
        mock = utils.MockHero(
            date="2020-03-12",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "12:00",
                "end_time": "11:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "17:00",
                keys.END: "23:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-03-12 18:00:00", "clock_in"),
            ("2020-03-13 02:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 3 * 60

    def test_flexi_shift_24hrs_tsr_with_core_hours_nt_regular_ut_scenario5(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_472573

        1. Flexi
        2. No core hours
        3. Timesheet required
        5. Night Diff
        """
        mock = utils.MockHero(
            date="2020-03-12",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "12:00",
                "end_time": "11:59",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-03-12 18:00:00", "clock_in"),
            ("2020-03-13 02:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 4 * 60

    def test_flexi_shift_24hrs_tsr_with_core_hours_nt_regular_ut_scenario6(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_472578

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Undertime
        """
        mock = utils.MockHero(
            date="2020-03-12",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "12:00",
                "end_time": "11:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "12:00",
                keys.END: "18:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-03-12 16:00:00", "clock_in"),
            ("2020-03-13 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 4 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_shift_24hrs_tsr_with_core_hours_nt_regular_ut_scenario7(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4292#note_473743

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Undertime
        """
        mock = utils.MockHero(
            date="2020-01-06",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "07:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "08:00",
                keys.END: "10:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.timesheet([
            ("2020-01-06 22:00:00", "clock_in"),
            ("2020-01-07 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 6 * 60

    def test_flexi_shift_24hrs_tsr_with_core_hours_3days_timesheet_scenario1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4402

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Undertime
        """
        mock = utils.MockHero(
            date="2020-01-28",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "12:00",
                "end_time": "11:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "14:00",
                keys.END: "20:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.timesheet([
            ("2020-01-28 13:00:00", "clock_in"),
            ("2020-01-28 19:00:00", "clock_out"),
            ("2020-01-29 13:00:00", "clock_in"),
            ("2020-01-29 20:00:00", "clock_out"),
            ("2020-01-30 15:00:00", "clock_in"),
            ("2020-01-30 23:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60

    def test_flexi_shift_24hrs_tsr_with_core_hours_3days_timesheet_scenario2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4402

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Undertime
        """
        mock = utils.MockHero(
            date="2020-01-28",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "17:00",
                "end_time": "16:59",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "19:00",
                keys.END: "23:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.timesheet([
            ("2020-01-28 08:00:00", "clock_in"),
            ("2020-01-28 16:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_shift_tsr_with_core_hours_clock_in_is_earlier_than_start_of_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4886

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Clock IN is earlier than start of Shift
        """
        mock = utils.MockHero(
            date="2020-05-27",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "10:00",
                "end_time": "23:00",
                "total_hours": "8:00"
            }
        ])
        mock.core_hours([
            {
                keys.START: "13:00",
                keys.END: "15:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.raw_timesheet([
            ("2020-05-27 08:30:00", "clock_in"),
            ("2020-05-27 21:12:00", "clock_out"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert calc.get_shift_raw_timesheet("1", "flexi") == [
            {
                "company_uid": 1,
                "employee_uid": 1,
                "state": True,
                "timestamp": 1590539400,
            },
            {
                "company_uid": 1,
                "employee_uid": 1,
                "state": False,
                "timestamp": 1590585120,
            }
        ]

    def test_flexi_with_ch_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_with_ch_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 8 * 60

    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_with_ch_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            hours_per_day=8,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_ABSENT) == 5 * 60

    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            hours_per_day=8,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day2(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1_on_holidays_false(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
    
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1_on_holidays_false_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays - False
        """
        # Attendance should not be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1_on_holidays_true(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60

    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day2(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_with_ch_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_ABSENT) == 5 * 60

    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_schedule_on_holidays_false(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_schedule_on_holidays_false_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        # Attendance should not be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_schedule_on_holidays_true(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_on_holidays_false(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift us assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
    
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_on_holidays_false_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays - False
        """
        # Attendance should not be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_on_holidays_true(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60

    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_flexi_with_ch_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_ABSENT) == 5 * 60

    def test_flexi_with_ch_day_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario5_day1_on_holidays_false(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH)

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_with_ch_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario5_day1_on_holidays_true(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays - true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "19:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "21:00",
                keys.END: "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60

    def test_flexi_with_ch_day_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "07:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.core_hours([
            {
                keys.START: "09:00",
                keys.END: "17:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
