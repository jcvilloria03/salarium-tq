# pylint: disable=W0212,C0301
"""Calculator unit tests"""
import unittest
import os
import time
from attendance import calculator, constants
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()


class TestScheduledHours(unittest.TestCase):
    """Test the scheduled hours returned"""
    
    def test_flexi_sched_with_1_break(self):
        """Test a flexi shift with 1 break
        """

        data = utils.get_mock("scheduled_hours_flexi_1_break")
        calc = calculator.get_calculator(data)
        scheduled_hours = calc.get_scheduled_hours()
        assert scheduled_hours == 420

    def test_flexi_sched_with_multi_breaks(self):
        """Test a flexi shift with multi breaks
        """
        data = utils.get_mock("scheduled_hours_flexi_multi_breaks")
        calc = calculator.get_calculator(data)
        scheduled_hours = calc.get_scheduled_hours()
        assert scheduled_hours == 360

    def test_flexi_sched_with_1_break_multi_shift(self):
        """Test a flexi shift with 1 break multi shift
        """
        data = utils.get_mock("scheduled_hours_flexi_1_break_multi_shift")
        calc = calculator.get_calculator(data)
        scheduled_hours = calc.get_scheduled_hours()
        assert scheduled_hours == 720

    def test_flexi_sched_with_multi_break_multi_shift(self):
        """Test a flexi shift with multi breaks and multi shift
        """
        data = utils.get_mock("scheduled_hours_flexi_multi_breaks_multi_shift")
        calc = calculator.get_calculator(data)
        scheduled_hours = calc.get_scheduled_hours()
        assert scheduled_hours == 630

    def test_fixed_sched_no_break(self):
        """Test a fixed shift with no breaks
        """
        data = utils.get_mock("scenario_1_simple_regular_shift")
        calc = calculator.get_calculator(data)
        scheduled_hours = calc.get_scheduled_hours()
        assert scheduled_hours == 720

    def test_default_shift(self):
        """Test a fixed shift with no breaks
        """
        data = utils.get_mock("scheduled_hours_default_shift")
        calc = calculator.get_calculator(data)
        scheduled_hours = calc.get_scheduled_hours()
        assert scheduled_hours == 480
