# pylint: disable=W0201
"""Test data generators/fixtures"""

from types import MethodType


class DictWrapper(dict):
    """Wrapper for dict to be used in dynamic method adding"""
    pass


def generate_data(root, **kwargs):
    """Create a dict containing data simulating REST service responses
        """

    data = dict()
    for key, val in kwargs.items():
        data[key] = val

    return DictWrapper({root: data})


def create_mock_fixed_shift(start_time, end_time, allowed_time_methods=None) -> DictWrapper:
    """Generate fixed shift data"""
    data = generate_data(
        "data",
        schedule={
            "type": "fixed",
            "start_time": start_time,
            "end_time": end_time,
            "allowed_time_methods": allowed_time_methods,
        }
    )
    return data


def create_fixedshift_withbreaks(start_time, end_time,
                                 allowed_time_methods=None, breaks=None) -> DictWrapper:
    """Creates a fixed shift data with a with_break() method"""
    data = create_mock_fixed_shift(start_time, end_time, allowed_time_methods)
    data.get("data").get("schedule")["breaks"] = breaks
    data.with_break = MethodType(_with_break, data)
    return data


def _with_break(self, btype, start, end, break_hours) -> dict:
    data = self.get("data")
    schedule = data.get("schedule")
    breaks = schedule.get("breaks")

    if not breaks:
        breaks = []
    breaks.append({
        "type": btype,
        "start": start,
        "end": end,
        "break_hours": break_hours
    })

    schedule["breaks"] = breaks
    return self


class MockAttParams(object):
    def __init__(self, **kwargs):
        self._params = {
            "employee_uid": 8124,
            "date": None,
            "pid": "c137579c-52dd-4bb7-9bfd-d31bf2f9dc0a",
            "id": 8124,
            "full_name": "Brad Jones Estrada",
            "company_id": 769,
            "employee_id": "Sep2018-006",
            "rank_id": 1134,
            "rank_name": "Rank",
            "position_id": 919,
            "position_name": "Staff",
            "department_id": 1156,
            "department_name": "HR",
            "location_id": None,
            "location_name": None,
            "hours_per_day": "12.00",
            "active": True,
            "time_attendance_id": 839,
            "team_id": 525,
            "team_name": "Engineering",
            "team_role": "",
            "primary_location_id": 1,
            "primary_location_name": "2018 Location",
            "secondary_location_id": None,
            "secondary_location_name": None,
            "timesheet_required": True,
            "overtime": True,
            "differential": True,
            "regular_holiday_pay": True,
            "special_holiday_pay": True,
            "holiday_premium_pay": True,
            "rest_day_pay": True,
            "shifts": [],
            "default_shift": None,
            "night_shift_settings": None,
            "hours_worked": [],
            "timesheet": [],
            "tardiness_rule": None,
            "shift_dates": {}
        }
        if kwargs:
            self._params.update(kwargs)
    
    def with_shift(self, **kwargs):
        self._params["shifts"].append(kwargs)
        return self

    def with_default_shift(self, **kwargs):
        self._params["default_shift"] = kwargs
        return self
    
    def with_hours_worked(self, **kwargs):
        self._params["hours_worked"].append(kwargs)
        return self

    def with_tardiness_rule(self, **kwargs):
        self._params["tardiness_rule"] = kwargs
        return self

    def with_shift_date_details(self, dt, **kwargs):
        self._params["shift_dates"][dt] = kwargs
        return self
        

class MockShiftParams(object):
    def __init__(self, **kwargs):
        self._params = {
            "shift_id": None,
            "name": "test",
            "type": None,
            "start_date": "2018-03-04",
            "start_time": None,
            "end_time": None,
            "total_hours": None,
            "allowed_time_methods": [],
            "affected_employees": [],
            "breaks": [],
            "core_times": []
        }
        if kwargs:
            self._params.update(kwargs)

    def with_break(self, **kwargs):
        self._params["breaks"].append(kwargs)
        return self

    def with_core_time(self, **kwargs):
        self._params["core_times"].append(kwargs)
        return self

    def with_affected_employees(self, **kwargs):
        self._params["affected_employees"].append(kwargs)
        return self

    def with_allowed_time_methods(self, *args):
        self._params["allowed_time_methods"].extend(list(args))

    @property
    def value(self):
        return self._params