# pylint: disable=E1102,C0111
"""
Basic fixed shift tests
"""
from unittest import TestCase, mock

from tests.attendance.utils import get_mock

from attendance.models import DefaultScheduleModel

SERVICE_NAMESPACE = "attendance.calls.get_co_default_schedule"
mock_get = get_mock('default_schedule')

class DefaultScheduleModelTest(TestCase):

    @mock.patch(SERVICE_NAMESPACE, return_value=mock_get)
    def  test_basic_fetch(self, mockeroo):
        types = [
            "undertime",
            "paid_leave",
            "unpaid_leave",
            "overtime",
            "overtime_night_shift",
            "regular",
            "rest_day"
        ]
        sched = DefaultScheduleModel(769)
        mockeroo.assert_called_once()
        for i in sched():
            assert i.type in types
            assert i.breaks
            assert i.day_of_week
            assert i.timein
            assert i.timeout
            assert i.total_shift_hours

    @mock.patch(SERVICE_NAMESPACE, return_value=None)
    def  test_error_fetch(self, mockeroo):
        sched = DefaultScheduleModel(1)
        assert not sched()
        mockeroo.assert_called_once()

    @mock.patch(SERVICE_NAMESPACE, return_value={"data": []})
    def  test_blank_fetch(self, mockeroo):
        sched = DefaultScheduleModel(1)
        assert not sched()
        mockeroo.assert_called_once()

    @mock.patch(SERVICE_NAMESPACE, return_value=mock_get)
    def  test_breaks(self, mockeroo):
        sched = DefaultScheduleModel(5)
        mockeroo.assert_called_once()
        for i in sched():
            for b in i.shift_breaks:
                assert b.duration
                assert b.start_time
                assert b.end_time
