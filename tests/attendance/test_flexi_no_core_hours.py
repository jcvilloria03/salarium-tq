# pylint: disable=W0212
"""Calculator unit tests"""
import unittest
from unittest import mock
import os
import time
from attendance import calculator, constants, keys
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams
# namespace for is_rest_day method from DefaultCalculator
mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"
os.environ["TZ"] = "Asia/Manila"
time.tzset()


class TestScenarioFlexiNoCoreHours(unittest.TestCase):
    """Default Calculator tests
    Flexi schedules with no core hours
    """
    def test_ot_timesheet_not_required(self):
        """Test specs:

        1. Flexi
        2. No core hours
        3. timesheet not required
        4. Has OT
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hours_timesheet_not_required_has_ot")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == int(3.716666667 * 60)

    def test_ot_timesheet_required(self):
        """test specs:

        1. flexi
        2. no core hours
        3. timesheet required
        4. has ot
        """
        # Timesheet 08:30 to 20:45
        # OT Request 17:30 to 21:00 (3.5 hours)
        # Employee's last clock-out(20:45) is earlier than the OT Request End Time(21:00)
        data = utils.get_mock("scenario_16c_flexi_without_core_hours_timesheet_required_has_ot")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

        # Valid OT from 17:30 to 20:45(3.25 hours). The remaining 15 minutes/0.25hour will be ignored
        assert attendance.get("1").get(constants.ATT_OT) == 3.25 * 60

    def test_ot_timesheet_required_with_ut(self):
        """test specs:

        1. flexi
        2. no core hours
        3. timesheet required
        4. Has ot
        5. Has timesheet undertime
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hours_timesheet_required_has_ot_with_ut")
        # Timesheet Nov-01-2018 08:30 to 10:00(2 Hours)
        # OT Request 17:30 to 21:00 (3.5 hours)
        # Shift Nov-01-2018 07:00 to 20:00
        # Expected Hours 08:00
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1.5 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 6.5 * 60
        # The OT span is outside the actual hours worked span
        assert attendance.get("1").get(constants.ATT_OT) == None

    def test_nt_rh_timesheet_required_on_holdays_false(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. Night shift with carry over settings
        5. Next day is regular holiday
        6. Shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        data = utils.get_mock("flexi_no_core_hours_nt_rh_carry_over_timesheet_required")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_NT) == 6 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
    
    def test_nt_rh_timesheet_required_on_holdays_true(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. Night shift with carry over settings
        5. Next day is regular holiday
        6. Shift has on_holidays - True
        """
        data = utils.get_mock("flexi_no_core_hours_nt_rh_carry_over_timesheet_required")
        data["shifts"][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 2 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT) == 6 * 60

    def test_timesheet_required_on_holidays_false(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        data = utils.get_mock("flexi_no_core_hours_nt_rh_carry_over_timesheet_required")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_NT) == 6 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
    
    def test_timesheet_required_on_holidays_true(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. shift has on_holidays - True
        """
        data = utils.get_mock("flexi_no_core_hours_nt_rh_carry_over_timesheet_required")
        data["shifts"][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 2 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT) == 6 * 60

    def test_with_paid_leave_half_day_nt(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. File half day paid_leave
        5. Night Shift
        """
        data = utils.get_mock("flexi_no_core_hours_nt_paid_leave_half_day")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 4 * 60

    def test_with_unpaid_leave_half_day_nt(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. File half day unpaid_leave
        5. Night Shift
        """
        data = utils.get_mock("flexi_no_core_hours_nt_unpaid_leave_half_day")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 4 * 60

    def test_with_unpaid_leave_half_day_nt_ut_carry_over(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. File half day unpaid_leave
        5. Night Shift - Carry Over
        6. With Undertime
        """
        data = utils.get_mock("flexi_no_core_hours_nt_unpaid_leave_half_day_ut_carry_over")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_with_unpaid_leave_half_day_nt_ut_counter_per_hr(self, restd):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet required
        4. File half day unpaid_leave
        5. Night Shift - Carry Over
        6. With Undertime
        """
        data = utils.get_mock("flexi_no_core_hours_nt_unpaid_leave_half_day_ut_counter_per_hr")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60

    def test_with_unpaid_leave_half_day_regular(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet not required
        4. File half day unpaid_leave
        5. Night Shift - Carry Over
        6. With Undertime
        """
        data = utils.get_mock("flexi_no_core_hours_unpaid_leave_half_day")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60

    def test_with_paid_leave_tsnr_day_shift(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet not required
        4. Approved paid leave
        """
        data = utils.get_mock("scenario_23a_flexi_without_core_hour_day_shift_has_paid_leave_tsnr")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 7 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60

    def test_with_ot_tsnr_day_shift(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet not required
        4. Approved overtime
        """
        data = utils.get_mock("scenario_23a_flexi_without_core_hour_day_shift_has_ot_tsnr")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 14 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 1.5 * 60

    def test_with_ut_tsnr_day_shift(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet not required
        4. Approved undertime
        """
        data = utils.get_mock("scenario_23a_flexi_without_core_hour_day_shift_has_ut_tsnr")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 12 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60

    def test_with_ul_tsnr_day_shift(self):
        """test specs:

        1. Flexi
        2. No core hours
        3. Timesheet not required
        4. Approved undertime
        """
        data = utils.get_mock("scenario_23a_flexi_without_core_hour_day_shift_has_unpaid_leave_tsnr")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 7 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60

    def test_flexi_date_hired(self):
        """Test specs:

        issue: https://code.salarium.com/development/t-and-a-manila/issues/1837

        1. Flexi
        2. With no core hours
        3. timesheet required
        4. Date hire is greater than the calculated date
        """
        data = utils.get_mock("flexi_no_core_hours_date_hired")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_date_ended(self):
        """Test specs:

        issue: https://code.salarium.com/development/t-and-a-manila/issues/1837

        1. Flexi
        2. With no core hours
        3. timesheet required
        4. Date ended is greater than the calculated date
        """
        data = utils.get_mock("flexi_no_core_hours_date_ended")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_night_shift_tsr_nt_regular(self):
        """Test specs:

        Issue: https://code.salarium.com/development/t-and-a-manila/issues/3006

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "20:00",
                "end_time": "07:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2019-01-01 20:30:00", "clock_in"),
            ("2019-01-02 07:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 6.5 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 1.5 * 60
        assert attendance.get("1").get(constants.ATT_NT) + \
            attendance.get("1").get(constants.ATT_REGULAR) == 480

    def test_flexi_night_shift_time_dispute_tsnr_nt_scenario1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/3000

        1. Flexi
        2. With no core hours
        3. Timesheet Not required
        4. Night shift schedule
        5. With Time Dispute: 10:45pm - 6:45am
        """
        mock = utils.MockHero(
            date="2019-02-25",
            timesheet_required=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "22:00",
                "end_time": "07:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2019-02-25 22:45:00", "clock_in"),
            ("2019-02-26 06:45:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 7.25 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 0.75 * 60

    def test_flexi_night_shift_time_dispute_tsnr_nt_scenario2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/3000

        1. Flexi
        2. With no core hours
        3. Timesheet Not required
        4. Night shift schedule
        5. With Time Dispute: 10:45pm - 5:45am
        """
        mock = utils.MockHero(
            date="2019-02-25",
            timesheet_required=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "22:00",
                "end_time": "07:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2019-02-25 22:45:00", "clock_in"),
            ("2019-02-26 05:45:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_night_shift_tsr_nt_with_regular(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/3786

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "20:00",
                "end_time": "06:00",
                "total_hours": "10:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 8 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) + \
            attendance.get("1").get(constants.ATT_REGULAR) == 600

    def test_flexi_night_shift_tsr_nt_with_regular2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/3786

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "20:00",
                "end_time": "05:00",
                "total_hours": "9:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) + \
            attendance.get("1").get(constants.ATT_REGULAR) == 540

    def test_flexi_day_shift_tsnr_with_night_shift_clock_pair(self):
        """  Test specs:

        https://code.salarium.com/salarium/production-operations/issues/353
        https://code.salarium.com/salarium/development/t-and-a-manila/issues/3999


        1. Flexi 6-10pm
        2. No core hours
        3. Employee TSNR
        4. Company has night shift schedule
        5. Has time record outside shift 11:58:25 - 11:58:36
        6. No approved OT request
        """

        mock_data = {
            'date': '2020-01-16',
            'night_shift_settings': {
                'id': 1,
                'company_id': 1,
                'activated': True,
                'start': '22:00',
                'end': '06:00',
                'time_shift': 'Counter per hour'
            },
            'tardiness_rule': [],
            'id': 1,
            'user_id': 1,
            'status': 'active',
            'employee_id': '1',
            'company_id': 1,
            'first_name': 'Mock',
            'middle_name': '',
            'last_name': 'Employee',
            'full_name': 'Mock Employee',
            'department_id': 1,
            'department_name': 'Department',
            'position_id': 1,
            'position_name': 'Position',
            'location_id': 1,
            'location_name': 'Office',
            'hours_per_day': None,
            'date_hired': '2019-08-27',
            'date_ended': None,
            'employment_type_id': 1,
            'employment_type_name': 'Probationary',
            'timesheet_required': False,
            'overtime': False,
            'differential': False,
            'regular_holiday_pay': True,
            'special_holiday_pay': True,
            'holiday_premium_pay': False,
            'rest_day_pay': False,
            'employee_uid': 1,
            'rest_days': [],
            'shifts': [{
                'name': 'Flexi Schedule',
                'type': 'flexi',
                'start_date': '2019-01-28',
                'start_time': '06:00',
                'end_time': '22:00',
                'total_hours': '08:00',
                'allowed_time_methods': ['Bundy App', 'Time Sheet', 'Web Bundy', 'Biometric'],
                'affected_employees': [{
                'id': 1,
                'schedule_id': 1,
                'type': 'employee',
                'rel_id': None,
                'created_at': '2019-02-08 04:02:32',
                'updated_at': '2019-02-08 04:02:32'
                }],
                'breaks': [],
                'core_times': [],
                'shift_id': 1
            }],
            'default_shift': {},
            'hours_worked': [],
            'tardiness_rules': [],
            'shift_dates': {
                '2020-01-16': {
                'rest_day': False,
                'holiday_type': None
                }
            },
            'timesheet': [{
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579190305.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579190316.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579215674.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579262105.0
            }],
            'raw_timesheet': [{
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1578970673.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579005253.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579060604.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579095163.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579190305.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579190316.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579215674.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579262105.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579490288.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579523446.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579577026.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579607159.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579663188.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579695318.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1579748283.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1579787625.0
            }]
        }
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_overtime_with_night_differential(self):
        """
        Test Flexi shift with overtime plus NT
        1. Flexi schedule without core hours(05:00 to 23:30)
        2. TSR false
        3. Night Time starts at 22:00 to 05:00
        4. Filed Overtime from 17:00 to 23:00(6 Hours)
        5. Employee is entitled to Overtime paid and Night Differential
        """

        shift_id = "1"
        shift_date = "2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "05:00",
                keys.END_TIME: "23:30",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="05:00")

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 17:00",
                keys.END_DATETIME: "2019-01-01 23:00"
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get(shift_id).get(constants.ATT_OT) == 5 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT) == None
        assert attendance.get(shift_id).get(constants.ATT_NT_OT) == 1 * 60

    def test_overtime_without_night_differential(self):
        """
        Test Flexi shift with overtime
        1. Flexi schedule without core hours(05:00 to 23:30)
        2. TSR false
        3. Night Time starts at 22:00 to 05:00
        4. Filed Overtime from 17:00 to 23:00(6 Hours)
        5. Employee is entitled to Overtime paid
        5. Employee is NOT entitled to Night Differential
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=False)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "05:00",
                keys.END_TIME: "23:30",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="05:00")

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 17:00",
                keys.END_DATETIME: "2019-01-01 23:00"
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get(shift_id).get(constants.ATT_OT) == 6 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT) == None
        assert attendance.get(shift_id).get(constants.ATT_NT_OT) == None

    def test_ot_nt_tsr_with_empty_ts(self):
        """
        Test Flexi shift with overtime plus NT
        1. Flexi schedule without core hours(05:00 to 23:30)
        2. TSR True
        3. Night Time starts at 22:00 to 05:00
        4. Filed Overtime from 17:00 to 23:00(6 Hours)
        5. Employee is entitled to Overtime paid and Night Differential
        6. Employee has no timesheet records
        """

        shift_id = "1"
        shift_date = "2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "05:00",
                keys.END_TIME: "23:30",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="05:00")

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 17:00",
                keys.END_DATETIME: "2019-01-01 23:00"
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.keys()) == 1
        assert attendance.get(shift_id).get(constants.ATT_ABSENT) == 8 * 60

    def test_ot_nt_tsr_with_ts(self):
        """
        Test Flexi shift with overtime plus NT
        1. Flexi schedule without core hours(05:00 to 23:30)
        2. TSR True
        3. Night Time starts at 22:00 to 05:00
        4. Filed Overtime from 17:00 to 01:00(8 Hours)
        5. Employee is entitled to Overtime paid and Night Differential
        6. Timesheet 08:00 to 23:00
        """

        shift_id = "1"
        shift_date = "2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "05:00",
                keys.END_TIME: "23:30",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="05:00")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out")
        ])

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 17:00",
                keys.END_DATETIME: "2019-01-02 01:00"
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get(shift_id).get(constants.ATT_OT) == 5 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT_OT) == 1 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT) == None

    @mock.patch(mock_is_restday, return_value=False)
    def test_ut_nt_tsr_with_ts_cph(self, restd):
        """
        Test Flexi shift with overtime plus NT
        1. Flexi schedule without core hours(05:00 to 23:30)
        2. TSR True
        3. Night Time starts at 22:00 to 06:00 Counter Per Hour
        4. Filed Overtime from 17:00 to 01:00(8 Hours)
        5. Employee is entitled to Overtime paid and Night Differential
        6. Timesheet 08:00 to 23:00
        """

        shift_id = "1"
        shift_date = "2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "17:00",
                keys.END_TIME: "05:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00")

        mock.timesheet([
            ("2019-01-01 17:35:00", "clock_in"),
            ("2019-01-02 01:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (4 * 60) + 25
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 35
        assert attendance.get(shift_id).get(constants.ATT_NT) == 3 * 60
        assert attendance.get(shift_id).get(constants.ATT_NT_OT) == None

    def test_reg_nt_tsnr_without_ts_carry_over(self):
        """
        Test Flexi shift with Reg and NT
        1. Flexi schedule without core hours(20:00 to 07:00).
           Required Hours is 8
        2. Timesheet not required
        3. Night Time settings
           > Starts at 22:00 to 06:00
           > Carry Over
        4. No timesheets
        5. Employee is entitled to Night Differential and Overtime
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "20:00",
                keys.END_TIME: "07:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (2 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 6 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_reg_nt_tsnr_without_ts_counter_per_hour(self, restd):
        """
        Test Flexi shift with Reg and NT
        1. Flexi schedule without core hours(20:00 to 07:00).
           Required Hours is 8
        2. Timesheet not required
        3. Night Time settings
           > Starts at 22:00 to 06:00
           > Counter per hour
        4. No timesheets
        5. Employee is entitled to Night Differential and Overtime
        """

        shift_id = "1"
        shift_date = "2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "20:00",
                keys.END_TIME: "07:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (2 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 6 * 60

    def test_rh_nt_tsnr_without_ts_carry_over(self):
        """
        Test Flexi shift with RHNT
        1. Flexi schedule without core hours(20:00 to 07:00).
           Required Hours is 8
        2. Timesheet not required
        3. Night Time settings
           > Starts at 22:00 to 06:00
           > Carry Over
        4. No timesheets
        5. Employee is entitled to Night Differential and Overtime
        6. Day is set as Regular Holiday
        """

        shift_id = "1"
        shift_date = "2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True,
            holiday_type=constants.ATT_RH)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "20:00",
                keys.END_TIME: "07:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_UH) == (8 * 60)

    def test_flexi_night_shift_tsr_nt_regular_overnight_no_nt(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 07:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "23:00",
                "total_hours": "14:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 07:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 13 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 1 * 60

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt_with_ot(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        6. Approved OT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 03:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 18:00",
                keys.END_DATETIME: "2020-02-25 03:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 4 * 60
        assert attendance.get("1").get(constants.ATT_NT_OT) == 5 * 60

    def test_flexi_night_shift_tsr_nt_regular_undertime(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Undertime
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-24 15:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_flexi_night_shift_tsr_nt_regular_ot_after_ts(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Undertime
        5. OT after last clock out
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-24 18:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 19:00",
                keys.END_DATETIME: "2020-02-24 23:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)
        assert not attendance.get("1").get(constants.ATT_OT)

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt_with_ot_beyond_clockout(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        6. Approved OT
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-24 22:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 18:00",
                keys.END_DATETIME: "2020-02-24 23:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 4 * 60
        assert not attendance.get("1").get(constants.ATT_NT_OT)

    def test_flexi_night_shift_tsr_nt_regular_overnight_with_nt_with_ot_before_clockout(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4202

        1. Flexi
        2. With no core hours
        3. Timesheet required
        4. Night shift schedule
        5. Overnight Clock-IN / Clock-OUT
        6. Approved OT (before Clock-OUT)
        """
        mock = utils.MockHero(
            date="2020-02-24",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.timesheet([
            ("2020-02-24 09:00:00", "clock_in"),
            ("2020-02-25 03:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2020-02-24 18:00",
                keys.END_DATETIME: "2020-02-24 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 2 * 60
        assert not attendance.get("1").get(constants.ATT_NT_OT)
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_ut_nt_reg_tsr(self):
        """
        Test Flexi shift with UT NT Regular
        1. Flexi schedule without core hours(17:00 to 05:00).
           Required Hours is 8
        2. Timesheet required
        3. Night Time settings
           > Starts at 22:00 to 06:00
           > Carry Over
        4. Time record:
           > Clock-in: 17:35:44 (1546335344)
           > Clock-out: 01:00:58 (1546362058) Next Day
        5. Employee is entitled to Night Differential and Overtime
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "17:00",
                keys.END_TIME: "05:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 17:35:00", "clock_in"),
            ("2019-01-02 01:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 3
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 35
        assert attendance.get(shift_id).get(constants.ATT_NT) == (3 * 60)
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == \
            (4 * 60) + 25

    def test_flexi_shift_tsr_no_core_hours_clock_in_is_earlier_than_start_of_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4886

        1. Flexi
        2. With core hours
        3. Timesheet required
        4. Clock IN is earlier than start of Shift
        """
        mock = utils.MockHero(
            date="2020-06-01",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "10:00",
                "end_time": "23:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.raw_timesheet([
            ("2020-06-01 10:00:00", "clock_in"),
            ("2020-06-01 19:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert calc.get_shift_raw_timesheet("1", "flexi") == [
            {
                "company_uid": 1,
                "employee_uid": 1,
                "state": True,
                "timestamp": 1590976800,
            },
            {
                "company_uid": 1,
                "employee_uid": 1,
                "state": False,
                "timestamp": 1591009200,
            }
        ]

    def test_current_day_midnight_nightshift_settings_cpr(self):
        """
        Flexi Shift 00:00 - 23:59
        Total Hours: 8:00
        Nightshift Settings: 22:00 - 06:00 Counter Per Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(date=shift_date)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FLEXI,
                keys.START_TIME: "03:00",
                keys.END_TIME: "02:59",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        # Felxi sched will consider ts span as
        # 3am-11am since 3am + 8 total hours
        mock.timesheet([
            ("2019-01-01 03:00:00", "clock_in"),
            ("2019-01-01 07:00:00", "clock_out"),
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out")
        ])

        mock.night_shift_settings(end="06:00")

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_NT) == 3 * 60
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 5 * 60

    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            hours_per_day=8,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_ABSENT) == 5 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day2(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1_on_holidays_false(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. Shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
    
    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1_on_holidays_false_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. Shift has on_holidays - False
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1_on_holidays_true(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. Shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day2(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_without_ts(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_without_ts(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Flexi
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
