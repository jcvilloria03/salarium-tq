# pylint: disable=W0212,C0301
"""Calculator unit tests"""
import unittest
from unittest import mock
import os
import time
from attendance import calculator, constants, keys
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams
os.environ["TZ"] = "Asia/Manila"
time.tzset()

mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"

class TestDefaultShift(unittest.TestCase):
    """Default Calculator tests"""

    def test_uh(self):
        """
        Test default shift with defined regular holiday
        - Default Shift
        - Unworked Holiday
        """
        data = utils.get_mock("scheduled_hours_default_shift_with_uh_sh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_UH) == 8 * 60

    def test_rh_tsr_with_time_records_with_approved_ot(self):
        """
        Test default shift with defined regular holiday
        - Default Shift
        - Regular Holiday
        - Timesheet Required
        - With Clock-in, Clock out matching approved OT
        - 8 hours approved O.T
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.default_shift()
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([
            {
                keys.SHIFT_ID: "ds_1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 08:00",
                keys.END_DATETIME: "2019-01-01 17:00"
            }
        ])
  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "RH" # Holiday Type
        )])

        # With time records

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_RH) == 8 * 60

    def test_sh_tsr_with_time_records_with_approved_ot(self):
        """
        Test default shift with defined regular holiday
        - Default Shift
        - Special Holiday
        - Timesheet Required
        - With Clock-in, Clock out matching approved OT
        - 8 hours approved O.T
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.default_shift()
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([
            {
                keys.SHIFT_ID: "ds_1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 08:00",
                keys.END_DATETIME: "2019-01-01 17:00"
            }
        ])

        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "RH" # Holiday Type
        )])

        # With time records

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_RH) == 8 * 60

    def test_sh_tsr_with_time_records_with_approved_ot_less_than_time_records(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Special Holiday
        3. 6 hours duration for Clock-In, Clock-Out
        4. 8 hours approved O.T
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        # mock.default_shift()
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            },
            {
                "break_hours": "1:00",
                "start": "09:00",
                "end": "10:00",
                "is_paid": False
            }
        ])
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([
            {
                keys.SHIFT_ID: "1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 08:00",
                keys.END_DATETIME: "2019-01-01 17:00"
            }
        ])

        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        # With time records

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 14:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_SH) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60

    def test_sh_tsr_with_time_records_with_approved_ot_more_than_time_records(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Special Holiday
        3. 6 hours duration for Clock-In, Clock-Out
        4. 8 hours approved O.T
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.default_shift()
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([
            {
                keys.SHIFT_ID: "ds_1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 08:00",
                keys.END_DATETIME: "2019-01-01 14:00"
            }
        ])

        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        # With time records

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("ds_1").get(constants.ATT_SH) == 5 * 60

    def test_sh_tsr_without_time_records_with_approved_ot(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Special Holiday
        3. No Clock-In, Clock-Out
        4. 8 hours approved O.T
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.default_shift()
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([
            {
                keys.SHIFT_ID: "ds_1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 08:00",
                keys.END_DATETIME: "2019-01-01 14:00"
            }
        ])
  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_sh_tsr_with_time_records_without_approved_ot(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Special Holiday
        3. With Clock-In, Clock-Out
        4. No approved O.T
        5. shift has on_holidays false
        """
        # Note: Now rendered hours during holidays are automatically
        # calculated without the need of approved O.T if the employee
        # has assigned a custom shift
        # Holiday attendance is not computed if custom shift is not assigned
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.default_shift()
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 14:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_sh_tsr_with_time_records_without_approved_ot_with_custom_shift_scenario_1(self):
        """
        Test default shift with defined regular holiday
        1. Custom Shift
        2. Special Holiday
        3. With Clock-In, Clock-Out
        4. No approved O.T
        5. shift has on_holidays false
        6. rendered hours should be calculated as REGULAR
        """
        # Note: Now rendered hours during holidays are automatically
        # calculated without the need of approved O.T if the employee
        # has assigned a custom shift
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 14:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 5 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
    
    def test_sh_tsr_with_time_records_without_approved_ot_with_custom_shift_scenario_2(self):
        """
        Test default shift with defined regular holiday
        1. Custom Shift
        2. Special Holiday
        3. With Clock-In, Clock-Out
        4. No approved O.T
        5. shift has on_holidays true
        6. rendered hours should be calculated as SH
        """
        # Note: Now rendered hours during holidays are automatically
        # calculated without the need of approved O.T if the employee
        # has assigned a custom shift
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays":True
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 14:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_SH) == 5 * 60

    def test_sh_tsr_with_time_records_with_approved_ot_day_shift_1(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Special Holiday
        3. 12 hours With Clock-In, Clock-Out
        4. 12 hours approved O.T
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        # Default Shift
        mock.default_shift()
        mock.night_shift_settings()
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.hours_worked([
            {
                keys.SHIFT_ID: "ds_1",
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 08:00",
                keys.END_DATETIME: "2019-01-01 20:00"
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_SH_OT) == 3 * 60
        assert attendance.get("ds_1").get(constants.ATT_SH) == 8 * 60

    def test_sh_night_shift_carry_over_tsr_with_time_records_with_approved_ot(self):
        """
        Test default shift with defined regular holiday
        1. Night Shift
        2. Special Holiday
        3. 12 hours With Clock-In, Clock-Out
        4. 12 hours approved O.T
        5. NT Carry Over
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "22:00",
                "end_time": "06:00"
            }
        ])  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.hours_worked([
            {
                keys.SHIFT_ID: 1,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 18:00",
                keys.END_DATETIME: "2019-01-02 05:00"
            }
        ])

        mock.timesheet([
            ("2019-01-01 18:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH_OT) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_SH_NT) == 7 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_sh_night_shift_counter_per_hour_tsr_with_time_records_with_approved_ot1(self, restd):
        """
        Test default shift with defined regular holiday
        1. Night Shift
        2. Special Holiday
        3. 12 hours With Clock-In, Clock-Out
        4. 12 hours approved O.T
        5. NT Carry Over
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "22:00",
                "end_time": "06:00"
            }
        ])  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01",
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.hours_worked([
            {
                keys.SHIFT_ID: 1,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 18:00",
                keys.END_DATETIME: "2019-01-02 05:00"
            }
        ])

        mock.timesheet([
            ("2019-01-01 18:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH_OT) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_SH_NT) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60

    def test_sh_night_shift_counter_per_hour_tsr_with_time_records_with_approved_ot_with_overtime(self):
        """
        Test default shift with defined regular holiday
        1. Night Shift
        2. Special Holiday
        3. 12 hours With Clock-In, Clock-Out
        4. 12 hours approved O.T
        5. NT_CARRY_OVER
        6. With Overtime
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER
            # time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "22:00",
                "end_time": "03:00"
            }
        ])  
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.hours_worked([
            {
                keys.SHIFT_ID: 1,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 22:00",
                keys.END_DATETIME: "2019-01-02 06:00"
            }
        ])

        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH_NT_OT) == 3 * 60
        assert attendance.get("1").get(constants.ATT_SH_NT) == 5 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_sh_night_shift_counter_per_hour_tsr_with_time_records_with_approved_ot_without_overtime(self, restd):
        """
        Test default shift with defined regular holiday
        1. Night Shift
        2. Special Holiday
        3. With Clock-In, Clock-Out
        4. Approved O.T same with clock in/out
        5. NT Counter Per Hour
        """
        mock = utils.MockHero(
            date="2018-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "18:00",
                "end_time": "05:00"
            }
        ])

        # Company settings holiday
        mock.set_shift_dates([(
            "2018-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.hours_worked([
            {
                keys.SHIFT_ID: 1,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2018-01-01 18:00",
                keys.END_DATETIME: "2018-01-02 05:00"
            }
        ])

        mock.timesheet([
            ("2018-01-01 18:00:00", "clock_in"),
            ("2018-01-02 05:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH_NT) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_SH) == 4 * 60

    def test_regular_night_shift_day_changed_to_holiday_with_approved_ot_outside_shift(self):
        """
        Test default shift with defined regular holiday
        1. Night Shift
        2. Special Holiday
        3. With Clock-In, Clock-Out
        4. Approved O.T outside shift
        """
        # Update: based on the feature #7838 rendered hours within core hours
        # does not need approved OT for attendance calculation.
        mock = utils.MockHero(
            date="2018-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "05:00"
            }
        ])

        # Company settings holiday
        mock.set_shift_dates([(
            "2018-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.hours_worked([
            {
                keys.SHIFT_ID: 1,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2018-01-02 05:00",
                keys.END_DATETIME: "2018-01-02 06:00"
            }
        ])

        mock.timesheet([
            ("2018-01-01 22:00:00", "clock_in"),
            ("2018-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        # updated result based on the ticket #7838
        assert attendance.get("1").get(constants.ATT_UH) == 1 * 60 # (21:00 - 22:00)
        assert attendance.get("1").get(constants.ATT_SH_NT_OT) == 1 * 60 # (05:00: 06:00)
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60 # (22:00: 05:00)

    def test_regular_day_changed_to_holiday_with_approved_ot_outside_shift(self):
        """
        Test default shift with defined regular holiday
        1. Default shift
        2. Special Holiday
        3. With Clock-In, Clock-Out
        4. Approved O.T outside shift
        """
        mock = utils.MockHero(
            date="2018-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.default_shift()

        # Company settings holiday
        mock.set_shift_dates([(
            "2018-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
        )])

        mock.hours_worked([
            {
                keys.SHIFT_ID: 1,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2018-01-01 17:00",
                keys.END_DATETIME: "2018-01-02 18:00"
            }
        ])

        mock.timesheet([
            ("2018-01-01 08:00:00", "clock_in"),
            ("2018-01-01 18:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_SH_OT) == 1 * 60
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_swh_night_shift(self):
        """
        Test specal working holiday
        1. Night Shift
        2. Special Working Holiday
        3. NT Carry Over
        4. scheduled overtime
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "20:30",
                "end_time": "05:30",
                "scheduled_overtime": [
                    {
                        "start": "05:30",
                        "end": "05:45"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            constants.ATT_SWH # Holiday Type
        )])

        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])
        mock.raw_timesheet([
            ("2018-01-01 22:00:00", "clock_in"),
            ("2018-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == int(0.5 * 60)
        assert attendance.get("1").get(constants.ATT_NT) == int(7.5 * 60)
        assert attendance.get("1").get(constants.ATT_NT_OT) == 15

    def test_swh_and_rh_night_shift(self):
        """
        Test specal working holiday + regular holiday
        1. Night Shift
        2. Special Working Holiday + RH
        3. NT Carry Over
        4. scheduled overtime
        REGULAR+RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "20:30",
                "end_time": "05:30",
                'on_holidays': True,
                "scheduled_overtime": [
                    {
                        "start": "05:30",
                        "end": "05:45"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            constants.ATT_SWH + constants.ATT_RH # Holiday Type
        )])

        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])
        mock.raw_timesheet([
            ("2018-01-01 22:00:00", "clock_in"),
            ("2018-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == int(0.5 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT) == int(7.5 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT_OT) == 15
    

    def test_rh_and_swh_night_shift(self):
        """
        Test specal working holiday + regular holiday
        1. Night Shift
        2. Special Working Holiday + RH
        3. NT Carry Over
        4. scheduled overtime
        RH+REGULAR
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "20:30",
                "end_time": "05:30",
                'on_holidays': True,
                "scheduled_overtime": [
                    {
                        "start": "05:30",
                        "end": "05:45"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([(
            "2019-01-01", 
            False, # Is Rest Day?
            constants.ATT_RH + constants.ATT_SWH # Holiday Type
        )])

        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])
        mock.raw_timesheet([
            ("2018-01-01 22:00:00", "clock_in"),
            ("2018-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == int(0.5 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT) == int(7.5 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT_OT) == 15

    @mock.patch(mock_is_restday, return_value=False)
    def test_holiday_nt_counter_per_hour_scenario_1(self, restd):
        """
        Issue: #8409
        1. Night Shift
        2. Special Working Holiday
        3. NT Counter Per Hour
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "23:30",
                "end_time": "08:30",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "03:00",
                keys.END: "04:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([
            (
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
            ),
            (
            "2019-01-02", 
            False, # Is Rest Day?
            None # Holiday Type
            )
        ])

        mock.timesheet([
            ("2019-01-01 23:19:00", "clock_in"),
            ("2019-01-02 08:30:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH_NT) == 30
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == int(2.5 * 60)

    @mock.patch(mock_is_restday, return_value=False)
    def test_holiday_nt_counter_per_hour_scenario_2(self, restd):
        """
        Issue: #8409
        1. Night Shift
        2. Special Working Holiday
        3. NT Counter Per Hour
        4. next day regular holiday
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "23:30",
                "end_time": "08:30",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "03:00",
                keys.END: "04:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([
            (
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
            ),
            (
            "2019-01-02", 
            False, # Is Rest Day?
            "RH" # Holiday Type
            )
        ])

        mock.timesheet([
            ("2019-01-01 23:19:00", "clock_in"),
            ("2019-01-02 08:30:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH_NT) == 30
        assert attendance.get("1").get(constants.ATT_RH_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_RH) == int(2.5 * 60)

    @mock.patch(mock_is_restday, return_value=False)
    def test_holiday_nt_counter_per_hour_scenario_3(self, restd):
        """
        Issue: #8409
        1. Night Shift
        2. Special Working Holiday
        3. NT Counter Per Hour
        4. next day regular holiday
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "23:30",
                "end_time": "08:30",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "03:00",
                keys.END: "04:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([
            (
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
            ),
            (
            "2019-01-02", 
            False, # Is Rest Day?
            None # Holiday Type
            )
        ])

        mock.timesheet([
            ("2019-01-02 00:00:00", "clock_in"),
            ("2019-01-02 08:30:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 30  # curr day undertime
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == int(2.5 * 60)

    @mock.patch(mock_is_restday, return_value=False)
    def test_holiday_nt_counter_per_hour_scenario_4(self, restd):
        """
        Issue: #8409
        1. Night Shift
        2. Special Working Holiday
        3. NT Counter Per Hour
        4. next day regular holiday
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "23:30",
                "end_time": "08:30",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "03:00",
                keys.END: "04:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([
            (
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
            ),
            (
            "2019-01-02", 
            False, # Is Rest Day?
            None # Holiday Type
            )
        ])

        mock.timesheet([
            ("2019-01-02 00:00:00", "clock_in"),
            ("2019-01-02 08:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 30  # curr day undertime
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == int(2 * 60)
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 30  # next day undertime

    @mock.patch(mock_is_restday, return_value=False)
    def test_holiday_nt_counter_per_hour_scenario_5(self, restd):
        """
        Issue: #8409
        1. Night Shift
        2. Special Working Holiday
        3. NT Counter Per Hour
        4. next day regular holiday
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "23:30",
                "end_time": "08:30",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "03:00",
                keys.END: "04:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([
            (
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
            ),
            (
            "2019-01-02", 
            False, # Is Rest Day?
            None # Holiday Type
            )
        ])

        mock.timesheet([
            ("2019-01-02 00:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 30  # curr day undertime
        assert attendance.get("1").get(constants.ATT_NT) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == int(3.5 * 60)  # next day undertime

    @mock.patch(mock_is_restday, return_value=False)
    def test_holiday_nt_counter_per_hour_scenario_6(self, restd):
        """
        Issue: #8409
        1. Night Shift
        2. Special Working Holiday
        3. NT Counter Per Hour
        4. next day regular holiday
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "23:30",
                "end_time": "08:30",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "03:00",
                keys.END: "04:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([
            (
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
            ),
            (
            "2019-01-02", 
            False, # Is Rest Day?
            "RH" # Holiday Type
            )
        ])

        mock.timesheet([
            ("2019-01-02 00:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 4 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT) == 4 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_holiday_nt_counter_per_hour_scenario_7(self, restd):
        """
        Issue: #8409
        1. Night Shift
        2. Special Working Holiday
        3. NT Counter Per Hour
        4. next day regular holiday
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "23:30",
                "end_time": "08:30",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "03:00",
                keys.END: "04:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        # Company settings holiday
        mock.set_shift_dates([
            (
            "2019-01-01", 
            False, # Is Rest Day?
            "SH" # Holiday Type
            ),
            (
            "2019-01-02", 
            False, # Is Rest Day?
            None # Holiday Type
            )
        ])

        mock.timesheet([
            ("2019-01-01 23:30:00", "clock_in"),
            ("2019-01-02 02:00:00", "clock_out"),
            ("2019-01-02 04:00:00", "clock_in"),
            ("2019-01-02 08:30:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH_NT) == 30
        assert attendance.get("1").get(constants.ATT_NT) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == int(2.5 * 60)
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60
