# pylint: disable=W0212
"""Calculator unit tests"""
import unittest
import os
import time
from attendance import calculator, constants, keys
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

# namespace for is_rest_day method from DefaultCalculator
mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"

os.environ["TZ"] = "Asia/Manila"
time.tzset()


class FlexiBreaksTests(unittest.TestCase):
    """
    Tests for shifts with flexi break
    """

    def test_rh_flexi_break_without_ts(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:00",
                "start": "10:00",
                "end": "15:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_rh_flexi_break_with_ts_without_taking_break(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. No break taken
        8. Break type unpaid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:00",
                "start": "10:00",
                "end": "15:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_rh_flexi_break_with_ts_with_full_break_taken(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Full 1hr break taken
        8. Break type unpaid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:00",
                "start": "10:00",
                "end": "15:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_rh_flexi_break_with_ts_with_over_break_taken(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Over break taken
        8. Break type unpaid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:00",
                "start": "10:00",
                "end": "15:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:30:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(7.5 * 60)
        assert attendance.get("1").get(constants.ATT_UH) == int(0.5 * 60)
    
    def test_rh_flexi_break_with_ts_with_under_break_taken_unpaid_break(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Under break taken
        8. Break type unpaid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:00",
                "start": "10:00",
                "end": "15:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 12:30:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(8 * 60)

    def test_rh_flexi_break_with_ts_with_under_break_taken_paid_break(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Under break taken
        8. Break type paid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:00",
                "start": "10:00",
                "end": "15:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 12:30:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(8.5 * 60)


class FloatingBreaksTests(unittest.TestCase):
    """
    Tests for shifts with floating break
    """

    def test_rh_floating_break_without_ts(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_rh_floating_break_with_ts_without_taking_break(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_rh_floating_break_with_ts_with_full_break_taken(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Full 1hr break taken
        8. Break type unpaid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_rh_floating_break_with_ts_with_over_break_taken(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Over break taken
        8. Break type unpaid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:30:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(7.5 * 60)
        assert attendance.get("1").get(constants.ATT_UH) == int(0.5 * 60)
    
    def test_rh_floating_break_with_ts_with_under_break_taken_unpaid_break(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Under break taken
        8. Break type unpaid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 12:30:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(8 * 60)
    
    def test_rh_floating_break_with_ts_with_under_break_taken_paid_break(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Under break taken
        8. Break type paid
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 08:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 12:30:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(8.5 * 60)


class FlexiBreaksNightShiftCarryOverTests(unittest.TestCase):

    """
    Tests for night shifts carry over with flexi break
    """

    def test_rh_nt_carry_over_shift_flexi_break_with_ts_with_full_break_taken(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Under break taken
        8. Break type unpaid
        9. Night shift
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "on_holidays": True
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:30",
                "start": "22:00",
                "end": "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 20:00:00", "clock_in"),
            ("2020-07-21 01:00:00", "clock_out"),
            ("2020-07-21 02:30:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(1 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT) == int(6.5 * 60)

    def test_rh_nt_carry_over_shift_flexi_break_with_ts_with_under_break_taken_non_paid(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        7. Under break taken
        8. Break type unpaid
        9. Night shift
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "on_holidays": True
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.breaks([
            {
                "type": keys.BR_FLEXI,
                "break_hours": "1:30",
                "start": "22:00",
                "end": "05:00"
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 20:00:00", "clock_in"),
            ("2020-07-21 01:00:00", "clock_out"),
            ("2020-07-21 02:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(1 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT) == int(6.5 * 60)

    def test_multiple_floating_breaks_with_over_break_scenario_1(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        # Ticket: https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8021
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 10:05:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 415
        assert attendance.get("1").get(constants.ATT_UH) == 65

    def test_multiple_floating_breaks_with_over_break_scenario_2(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        # Ticket: https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8021
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:05:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 475
        assert attendance.get("1").get(constants.ATT_UH) == 5

    def test_multiple_floating_breaks_with_over_break_scenario_3(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        # Ticket: https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8021
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 16:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 6 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 2 * 60

    def test_multiple_floating_breaks_with_over_break_scenario_4(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        # Ticket: https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8021
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:05:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 17:55:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 470
        assert attendance.get("1").get(constants.ATT_UH) == 10

    def test_multiple_floating_breaks_with_under_break_scenario_1(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:05:00", "clock_in"),
            ("2020-07-20 12:30:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_multiple_floating_breaks_with_under_break_scenario_2(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:05:00", "clock_in"),
            ("2020-07-20 12:30:00", "clock_out"),
            ("2020-07-20 13:00:00", "clock_in"),
            ("2020-07-20 13:10:00", "clock_out"),
            ("2020-07-20 13:20:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_under_floating_breaks_at_middle_of_shift(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 12:00:00", "clock_out"),
            ("2020-07-20 12:10:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_under_floating_breaks_at_end_of_shift_scenario_1(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 17:55:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_under_floating_breaks_at_end_of_shift_scenario_2(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 17:10:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_full_floating_breaks_at_end_of_shift_scenario_3(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_under_floating_breaks_at_end_of_shift_scenario_4(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:10:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_under_floating_breaks_at_end_of_shift_scenario_5(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 17:01:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_under_floating_breaks_at_start_of_shift_scenario_1(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 09:10:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_full_floating_breaks_at_start_of_shift(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "type": keys.BR_FLOATING,
                "break_hours": "1:00",
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2020-07-20 10:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
