#pylint: disable=C0111
"""Test for tasks"""
from unittest import TestCase, mock


from attendance.tasks import get_employee_info
from core import exceptions


class TestTasks(TestCase):

    @mock.patch('attendance.calls.get_employee_info', return_value={
        "id": '1',
        'employee_fullname': 'Test'
    })
    def test_get_employee_info(self, caller):
        att_params = {
            "employee_uid": 23,
            "date" : "2018-01-01",
        }
        info = get_employee_info(att_params)
        caller.assert_called_once()
        assert info.get("employee_uid") == 23
        assert info.get("date") == '2018-01-01'
        assert info.get('employee_fullname') == 'Test'

    # @mock.patch('attendance.calls.get_employee_info', side_effect=exceptions.TaskError())
    # @mock.patch('attendance.calls.add_job_task_error', return_value={})
    # def test_get_employee_info_error(self, adder, caller):
    #     att_params = {
    #         "employee_uid": 23,
    #         "date" : "2018-01-01",
    #     }
    #     with self.assertRaises(exceptions.TaskError):
    #         tasks.get_employee_info(att_params)
    #         caller.assert_called_once()
    #         adder.assert_called_once()
