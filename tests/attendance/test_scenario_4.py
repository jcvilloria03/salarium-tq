# pylint: disable=W0212
"""Calculator unit tests"""
import unittest
import os
import time
from attendance import calculator, constants
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()


class TestScenario4(unittest.TestCase):
    """Default Calculator tests

    Fixed Schedule with 1 Fixed Break and with one 1 Floating break (Day shift)
    """

    def test_regular(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 460

    def test_tardy(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and tardy"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_tardy")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 400
        assert attendance.get("1").get(constants.ATT_TARDY) == 60

    def test_overtime(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with OT"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_ot")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 460
        assert attendance.get("1").get(constants.ATT_OT) == 180

    def test_with_ot_but_no_entitlement(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with OT
        but no entitlement"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_no_ot_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 460
        assert not attendance.get("1").get(constants.ATT_OT, None)

    def test_with_holiday_entitlement_scenario_1(self):
        # Floating break paid: false
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with holiday entitlement"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_holiday_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 460

    def test_with_holiday_entitlement_scenario_2(self):
        # Floating break paid: true
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with holiday entitlement"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_holiday_entitlement")
        data['shifts'][0]['breaks'][0]['is_paid'] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 480

    def test_with_no_holiday_entitlement(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with no holiday
        entitlement"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_holiday_no_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 460

    def test_with_unworked_holiday_entitlement_shift_schedule_on_holidays_false(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break"""
        # has rendered hours
        # shift has on_holidays false
        # hours should be calculated as REGULAR
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_unworked_holiday_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 460
    
    def test_with_unworked_holiday_entitlement_with_on_holidays_true(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break"""
        # has rendered hours
        # shift has on_holidays true
        # hours should be calculated as RH
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_unworked_holiday_entitlement")
        data['shifts'][0]['on_holidays'] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 460

    def test_with_special_holiday_entitlement_unpaid_floating_break(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with special holiday entitlement"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_special_holiday_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 460

    def test_with_special_holiday_entitlement_paid_floating_break(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with special holiday entitlement"""
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_special_holiday_entitlement")
        data['shifts'][0]['breaks'][0]['is_paid'] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 480

    def test_with_special_and_regular_holiday_entitlement_scenario_unpaid_floating_break(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with regular holiday entitlement"""
        # Floating break paid: false
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_special_regular_holiday_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 460

    def test_with_special_and_regular_holiday_entitlement_scenario_paid_floating_break(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break and with regular holiday entitlement"""
        # Floating break paid: true
        data = utils.get_mock(
            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_special_regular_holiday_entitlement")
        data['shifts'][0]['breaks'][0]['is_paid'] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 480

    # Commented below as these are not yet coded.
#    def test_with_paid_leave(self):
#        """Test a fixed schedule with 1 fixed break and with 1 floating break and with paid leavfe"""
#        data = utils.get_mock(
#            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_with_paid_leave")
#        calc = calculator.get_calculator(data)
#        attendance = calc.attendance
#        assert attendance.get("1").get("PAID LEAVE") == 460


    # ================================================================
    # 4A
    # ================================================================
#    def test_with_approved_time_disputes(self):
#        """Test a fixed schedule with 1 fixed break and with 1 floating break and with time dispute request"""
#        data = utils.get_mock(
#            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_with_time_dispute")
#        calc = calculator.get_calculator(data)
#        attendance = calc.attendance
#        assert attendance.get("1").get("REGULAR") == 220
#        assert attendance.get("1").get("UNDERTIME") == 240


#    def test_with_approved_undertime(self):
#        """Test a fixed schedule with 1 fixed break and with 1 floating break and with approved undertime"""
#        data = utils.get_mock(
#            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_with_approved_undertime")
#        calc = calculator.get_calculator(data)
#        attendance = calc.attendance
#        assert attendance.get("1").get("REGULAR") == 240
#        assert attendance.get("1").get("UNDERTIME") == 300


#    def test_with_unpaid_leave(self):
#        """Test a fixed schedule with 1 fixed break and with 1 floating break and with unpaid leave"""
#        data = utils.get_mock(
#            "scenario_4_fixed_sched_1_fixed_break_with_1_floating_break_day_shift_with_unpaid_leave")
#        calc = calculator.get_calculator(data)
#        attendance = calc.attendance
#        assert attendance.get("1").get("UNPAID LEAVE") == 460



