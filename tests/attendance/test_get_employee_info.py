# pylint: disable=C0111
from unittest import TestCase, mock
from attendance.calls import get_employee_info
from attendance import tasks

class TestGetEmployeeAPI(TestCase):
    @mock.patch('attendance.calls._get_employee_info', return_value={
        "id": 2,
        "account_id": 1,
        "company_id": 1,
    })
    def test_simple(self, fetcher):
        get_employee_info(2)
        fetcher.assert_called_once_with(2)



class TestGetEmployeeTask(TestCase):
    @mock.patch('attendance.calls.get_employee_info', return_value={
        "id": 2,
        "account_id": 1,
        "company_id": 1
    })
    def test_simple_task(self, api_call):
        att_params = {
            "employee_uid": 23,
            "date" : "2018-01-01",
        }
        result = tasks.get_employee_info(att_params)
        api_call.assert_called_once_with(23)
        assert result
        assert result.get("employee_uid") == 23
        assert result.get("date") == '2018-01-01'
        assert result.get("company_id") == 1
