"""Utility functions for the tests
"""

import json

from datetime import datetime
from datetime import timedelta
import time

def get_mock(filename):
    """Get mock data"""

    with open(f'tests/attendance/mocks/{filename}.json', encoding="utf-8") as f:
        data = json.loads(f.read())
    return data

def datetime_to_secs(inp, _format="%Y-%m-%d %H:%M:%S"):
    """convert datetime to seconds"""
    date_time = datetime.strptime(inp, _format)
    date_ts = time.mktime(date_time.timetuple())
    return int(date_ts)


class MockHero(object):
    """A method that generates mock attendance
    test data
    """
    """
    _employee = {}

    _emp_id = 1
    _date = "2019-01-01"
    _company_id = 1
    """
    def __init__(self, date, employee_id=1,
                 company_id=1, is_rest_day=False,
                 holiday_type=None, **kwargs):
        """Constructor
        """
        self._emp_id = employee_id
        self._company_id = company_id
        self._date = date
        self._is_rest_day = is_rest_day
        self._holiday_type = holiday_type
        self._shifts = []
        self._breaks = []
        self._core_hours = []
        self._default_shift = {}
        self._timesheet = []
        self._tardiness_rule = {}
        self._night_shift_setting = {}
        self._hours_worked = []
        self._raw_timesheet = []

        # Array of tuples ex. ("2019-01-02", False, "RH")
        # Second item should be
        self._next_days = []
        self._employee = {
            "employee_uid": self._emp_id,
            "date": kwargs.get("date", self._date),
            "process_id": "9645abcb-c01a-4e9a-9efb-",
            "id": self._emp_id,
            "full_name": kwargs.get("full_name", "Mang Kanor"),
            "company_id": self._company_id,
            "employee_id": "EMP-000001",
            "rank_id": kwargs.get("rank_id", 1),
            "rank_name": kwargs.get("rank_name", "Team Lead"),
            "position_id": kwargs.get("position_id", 1),
            "position_name": kwargs.get(
                "position_name", "Vice President"),
            "department_id": kwargs.get("department_id", 1),
            "department_name": kwargs.get(
                "department_name", "Janitorial Services"),
            "location_id": kwargs.get("location_id", 1),
            "location_name": kwargs.get(
                "location_name", "Makati"),
            "hours_per_day": kwargs.get("hours_per_day", None),
            "active": kwargs.get("active", True),
            "primary_location_id": kwargs.get(
                "primary_location_id", 1),
            "primary_location_name": kwargs.get(
                "primary_location_name", "Boracay"),
            "secondary_location_id": kwargs.get(
                "secondary_location_id", None),
            "secondary_location_name": kwargs.get(
                "secondary_location_name", None),
            "timesheet_required": kwargs.get(
                "timesheet_required", True),
            "overtime": kwargs.get("overtime", True),
            "differential": kwargs.get("differential", True),
            "regular_holiday_pay": kwargs.get(
                "regular_holiday_pay", True),
            "special_holiday_pay": kwargs.get(
                "special_holiday_pay", True),
            "holiday_premium_pay": kwargs.get(
                "holiday_premium_pay", True),
            "rest_day_pay": kwargs.get("rest_day_pay", True),
        }

        self._user_defined_rest_days = []
        self._shift_dates = {
            self._date: {
                "rest_day": self._is_rest_day,
                "holiday_type": self._holiday_type
            }
        }

    def breaks(self, breaks):
        if not breaks:
            return
        for index, item in enumerate(breaks):
            self._breaks.append({
                "break_hours": item.get("break_hours", "01:00"),
                "end": item.get("end", "13:00"),
                "id": index + 1,
                "schedule_id": index + 1,
                "start": item.get("start", "12:00"),
                "type": item.get("type", "fixed"),
                "is_paid": item.get("is_paid", False)
            })

    def core_hours(self, core_hours):
        if not core_hours:
            return

        for index, item in enumerate(core_hours):
            self._core_hours.append({
                "id": index + 1,
                "schedule_id": index + 1,
                "start": item.get("start", "12:00"),
                "end": item.get("end", "13:00")
            })

    def hours_worked(self, hours_worked):
        if not hours_worked:
            return
        for index, item in enumerate(hours_worked):
            self._hours_worked.append({
                "id": 1,
                "employee_id": self._emp_id,
                "date": self._date,
                "type": item.get("type", "regular"),
                "type_name": item.get("type_name", "Regular"),
                "time": item.get("time", "08:00"),
                "value":"0.50",
                "shift_id": item.get("shift_id", 1),
                "start_datetime": item.get(
                    "start_datetime", None),
                "end_datetime": item.get("end_datetime", None)
            })

    def shifts(self, shifts):
        if not shifts:
            return
        for index, item in enumerate(shifts):
            _id = index + 1
            self._shifts.append({
                "shift_id":  _id,
                "name": item.get("name", "My Shift"),
                "type": item.get("type", "fixed"),
                "start_date": item.get(
                    "start_date", self._date),
                "start_time": item.get("start_time", "07:00"),
                "end_time": item.get("end_time", "22:00"),
                "total_hours": item.get("total_hours", "08:00"),
                "allowed_time_methods": item.get(
                    "allowed_time_methods", [
                        "bundy", "web_bundy"]),
                "affected_employees": [
                    {
                        "id": 255,
                        "schedule_id": 2,
                        "type": "employee",
                        "rel_id": None,
                        "created_at": "2018-10-11 05:18:50",
                        "updated_at": "2018-10-11 05:18:50"
                    }
                ],
                "breaks":  self._breaks,
                "core_times": self._core_hours,
                "on_holidays": item.get("on_holidays", False),
                "on_rest_day": item.get("on_rest_day", False),
                "scheduled_overtime": item.get("scheduled_overtime", [])
            })

    def default_shift(self, **kwargs):
        self._default_shift = {
            "id": 1,
            "company_id": self._company_id,
            "day_of_week": kwargs.get("day_of_week", 1),
            "day_type": kwargs.get("day_type", "regular"),
            "work_start": kwargs.get(
                "work_start", "08:00:00"),
            "work_break_start": kwargs.get(
                "work_break_start", "12:00:00"),
            "work_break_end": kwargs.get(
                "work_break_end", "13:00:00"),
            "work_end": kwargs.get("work_end", "17:00:00")
        }

    def night_shift_settings(self, **kwargs):
        self._night_shift_setting = {
            "id": 1,
            "company_id": self._company_id,
            "activated": True,
            "start": kwargs.get("start", "22:00"),
            "end": kwargs.get("end", "05:00"),
            "time_shift": kwargs.get(
                "time_shift", "Counter per hour")
        }

    def timesheet(self, timesheet):
        for time, state in timesheet:
            self._timesheet.append({
                "company_uid": self._company_id,
                "employee_uid": self._emp_id,
                "state": True if state is "clock_in" else False,
                "timestamp": datetime_to_secs(time)
            })

    def set_max_clock_out(self, index: int, max_clock_out: bool):
        """
        Args:
            index: index of the self._timesheet object
            max_clock_out: bool to set if this clockout
                is max_clock_out true or false
        """
        self._timesheet[index]['max_clock_out'] = max_clock_out

    def raw_timesheet(self, timesheet):
        for time, state in timesheet:
            self._raw_timesheet.append({
                "company_uid": self._company_id,
                "employee_uid": self._emp_id,
                "state": True if state is "clock_in" else False,
                "timestamp": datetime_to_secs(time)
            })

    def tardiness_rule(self, **kwargs):
        self._tardiness_rule = {
            "id": 98,
            "name": "Whatever",
            "company_id": self._company_id,
            "minutes_tardy": kwargs.get("minutes_tardy", 15),
            "minutes_to_deduct": kwargs.get("minutes_to_deduct", 0),
            "affected_employees": [
                {
                    "id": None,
                    "name": "All employees",
                    "type": "employee"
                }
            ],
            "updated_at": "2018-10-10 08:35"
        }

    def set_shift_dates(self, shift_dates):
        """List of tuples of date, rest day flag and holiday type

        Args:
            shift_dates: ([tuple]) -- List of tuples of date, rest day
                flag and holiday type.

                Format - (<date str>, <rest day flag True|False>, <holiday code>)

                Ex:
                [
                    ("2019-01-01", False, )
                ]
        """
        s_dates = {_date: {"rest_day": rd_flag, "holiday_type": code} for _date, rd_flag, code in shift_dates}
        self._shift_dates.update(s_dates)

    def user_defined_rest_days(self, **kwargs):
        self._user_defined_rest_days = [
            {
                "id": kwargs.get("id", 1),
                "company_id": self._company_id,
                "employee_id": self._emp_id,
                "start_date": kwargs.get("start_date", self._date),
                "end_date": kwargs.get("end_date", self._date),
                "employee": kwargs.get("employee", None),
                "dates": kwargs.get("dates", [self._date]),
                "repeat": {
                    "id": 1,
                    "repeat_every": kwargs.get("repeat_every", 1),
                    "rest_days": kwargs.get("rest_days", [
                        "monday"
                    ]),
                    "end_after": kwargs.get("end_after", None),
                    "end_never": kwargs.get("end_never", True)
                }
            }
        ]

    def generate(self):
        shift_dates = {}
        self._employee["shifts"] = self._shifts
        self._employee["default_shift"] = self._default_shift
        self._employee["night_shift_settings"] = self._night_shift_setting
        self._employee["hours_worked"] = self._hours_worked
        self._employee["tardiness_rule"] = self._tardiness_rule
        self._employee["timesheet"] = self._timesheet
        self._employee["raw_timesheet"] = self._raw_timesheet
        self._employee["shift_dates"] = self._shift_dates
        self._employee["rest_days"] = self._user_defined_rest_days

        return self._employee
