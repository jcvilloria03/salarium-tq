# pylint: disable=W0212
"""Calculator unit tests"""
import unittest
from unittest import mock
import os
import time
from attendance import calculator, constants, keys
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

# namespace for is_rest_day method from DefaultCalculator
mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"

os.environ["TZ"] = "Asia/Manila"
time.tzset()


class TestFixedShift(unittest.TestCase):
    """All Fixed Shift Calculation Tests
    """

    def test_tsr_with_half_day_leave_with_paid_break_scenario_1(self):
        """Test a fixed schedule with 1 fixed paid break
        - Fixed shift
        - Paid Leave    13:00-17:00
        - Paid Break    12:00-13:00
        - Time records  08:00-12:00

        """
        data = utils.get_mock("fixed_tsr_with_half_day_leave_with_paid_break_scenario_1")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_tsr_with_ot_multi_span(self):
        """Test a fixed schedule with multi span OT
        - Fixed shift
        """
        data = utils.get_mock("ticket-7546-mock")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
        assert attendance.get("1").get(constants.ATT_RH_OT) == 299

    def test_tsr_with_half_day_leave_with_paid_break_scenario_2(self):
        """Test a fixed schedule with 1 fixed paid break
        - Fixed shift
        - Paid Leave    13:00-17:00
        - Paid Break    12:00-13:00
        - No Time records

        """
        data = utils.get_mock("fixed_tsr_with_half_day_leave_with_paid_break_scenario_2")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 5 * 60

    def test_tsr_with_half_day_leave_with_paid_break_scenario_3(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break
        - Fixed shift
        - Paid Leave    13:00-17:00
        - Paid Break    12:00-13:00
        - Time records  09:00-11:00
        - Without Tardy

        """
        data = utils.get_mock("fixed_tsr_with_half_day_leave_with_paid_break_scenario_3")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 3 * 60

    def test_tsr_with_half_day_leave_with_paid_break_scenario_4(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break
        - Fixed shift
        - Paid Leave    13:00-17:00
        - Paid Break    12:00-13:00
        - Time records  09:00-11:00
        - With Tardy Settings
        """
        data = utils.get_mock("fixed_tsr_with_half_day_leave_with_paid_break_scenario_4")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60
        assert attendance.get("1").get(constants.ATT_TARDY) == 1 * 60

    def test_tsr_with_half_day_leave_with_paid_break_scenario_5(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break
        - Fixed shift
        - Paid Leave    08:00 AM - 12:00 PM
        - Paid Break    12:00-13:00
        - Time records  01:30 PM - 5:00 PM
        - Without Tardy

        """
        data = utils.get_mock("fixed_tsr_with_half_day_leave_with_paid_break_scenario_5")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 3.5 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1.5 * 60

    def test_tsr_with_half_day_leave_with_paid_break_scenario_6(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break
        - Fixed shift
        - Paid Leave    12:30 PM - 05:00 PM
        - Paid Break    12:00-13:00
        - Time records  08:00 AM - 12:00 PM
        - Without Tardy

        """
        data = utils.get_mock("fixed_tsr_with_half_day_leave_with_paid_break_scenario_6")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 3.5 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1.5 * 60

    def test_tsr_with_half_day_leave(self):
        """Test a fixed schedule with 1 fixed break and with 1 floating break"""
        data = utils.get_mock("fixed_tsr_with_half_day_leave")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60

    def test_tsr_with_paid_break(self):
        """
        1. Fixed schedule 8AM - 5PM
        2. TSR
        3. Has paid breaks
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60

    def test_tsr_halfday_with_paid_leave_with_paid_break(self):
        """
        1. Fixed schedule 8:00 - 13:00
        2. TSR
        3. Has paid breaks
        4. Has half day paid leave 13:00 - 17:00
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 13:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 13:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 5 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60

    def test_tsr_halfday_with_paid_leave_with_paid_break_scenario_8(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has half day paid leave 12:00 - 17:00
        5. Time records 8:00 AM - 12:00 PM

        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 12:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 12:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 5 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60

    def test_tsr_halfday_with_paid_leave_with_ut_with_paid_break(self):
        """
        1. Fixed schedule 8:00 - 13:00
        2. TSR
        3. Has paid breaks
        4. Has half day paid leave 13:00 - 17:00
        5. With 01:00 undertime
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 13:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_tsr_without_paid_break(self):
        """
        1. Fixed schedule 8AM - 5PM
        2. TSR
        3. No paid breaks
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_tsr_with_ut_with_paid_break(self):
        """
        1. Fixed schedule 8:00 - 14:00
        2. TSR
        3. Has paid breaks
        5. With 03:00 undertime
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "01:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 14:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 3 * 60

    def test_tsr_nt_with_paid_break(self):
        """
        1. Fixed schedule 20:00 - 05:00 (Night shift)
        2. TSR
        3. Has paid breaks
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "20:00",
                "end_time": "05:00"
            }
        ])
        mock.night_shift_settings()
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "00:00",
                "end": "01:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([(
            "2019-01-02",
            False,
            None
        )])
        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60

    def test_tsr_with_over_break(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. No paid breaks
        5. With 02:00 overbreak
        """
        mock = utils.MockHero(
            date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:30:00", "clock_out"),
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 14:30:00", "clock_out"),
            ("2019-01-01 15:30:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("1").get(constants.ATT_OVERBREAK) == 2 * 60

    def test_tsr_with_ut(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. No paid breaks
        5. With 02:00 overbreak
        """
        mock = utils.MockHero(
            date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:30:00", "clock_out"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 4 * 60

    def test_tsr_paid_break_with_pl_full_day(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has paid leave 8:00 - 17:00
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "09:00",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 9 * 60

    def test_tsr_paid_break_with_ul_full_day(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has paid leave 8:00 - 17:00
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "unpaid_leave",
                "type_name": "Unpaid Leave",
                "time": "09:00",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 9 * 60

    def test_tsr_paid_break_with_pl_half_day_at_the_start(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has paid leave 8:00 - 12:00
        5. Has time record 13:00 - 17:00
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 12:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_tsr_paid_break_with_ul_half_day_at_start(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has paid leave 8:00 - 12:00
        5. Has time record 13:00 - 17:00
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "unpaid_leave",
                "type_name": "Unaid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 12:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 5 * 60

    def test_tsr_paid_break_with_pl_half_day_at_start_with_ut(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has paid leave 8:00 - 12:00
        5. Has time record 13:00 - 17:00
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 12:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 3.5 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1.5 * 60

    def test_tsr_paid_break_with_ul_half_day_at_start_with_ut(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has paid leave 8:00 - 12:00
        5. Has time record 13:00 - 17:00
        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "unpaid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 12:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4.5 * 60
        assert attendance.get("1").get(constants.ATT_OVERBREAK) == 0.5 * 60

    def test_tsr_with_undertime_and_floating_paid_breaks(self):
        """
        Condition:
            1. Fixed schedule 1PM to 10PM
            2. Clock in at January 30, 2020 1:40:34 PM
            3. Clock out at January 30, 2020 10:01:08 PM
            4. No tardiness settings
        """

        mock_data = {
            'date': '2020-01-30',
            'night_shift_settings': {
                'id': 1,
                'company_id': 1,
                'activated': True,
                'start': '22:00',
                'end': '06:00',
                'time_shift': 'Carry over'
            },
            'tardiness_rule': [],
            'id': 1,
            'user_id': 1,
            'status': 'active',
            'employee_id': 'E1',
            'company_id': 1,
            'first_name': 'Test',
            'middle_name': '',
            'last_name': 'Tester',
            'full_name': 'Test Tester',
            'department_id': 1,
            'department_name': 'Department',
            'position_id': 1,
            'position_name': 'Position',
            'location_id': 1,
            'location_name': 'Location',
            'hours_per_day': None,
            'date_hired': '2020-01-13',
            'date_ended': '2020-01-31',
            'employment_type_id': 1,
            'employment_type_name': 'Employment Type',
            'timesheet_required': True,
            'overtime': True,
            'differential': True,
            'regular_holiday_pay': True,
            'special_holiday_pay': True,
            'holiday_premium_pay': True,
            'rest_day_pay': True,
            'employee_uid': 1,
            'rest_days': [],
            'shifts': [{
                'name': 'Cold Data- 1PM-10 PM',
                'type': 'fixed',
                'start_date': '2020-01-01',
                'start_time': '13:00',
                'end_time': '22:00',
                'total_hours': '09:00',
                'allowed_time_methods': ['Bundy App', 'Time Sheet', 'Web Bundy', 'Biometric'],
                'affected_employees': [
                    {
                        'id': 1,
                        'schedule_id': 1,
                        'type': 'employee',
                        'rel_id': None,
                        'created_at': '2020-01-22 18:20:01',
                        'updated_at': '2020-01-22 18:20:01'
                    }
                ],
                'breaks': [
                    {
                        'id': 1,
                        'schedule_id': 1,
                        'type': 'floating',
                        'start': None,
                        'end': None,
                        'break_hours': '00:15',
                        'is_paid': True
                    },
                    {
                        'id': 1,
                        'schedule_id': 1,
                        'type': 'floating',
                        'start': None,
                        'end': None,
                        'break_hours': '00:15',
                        'is_paid': True
                    },
                    {
                        'id': 1,
                        'schedule_id': 1,
                        'type': 'floating',
                        'start': None,
                        'end': None,
                        'break_hours': '01:00',
                        'is_paid': False
                    }
                ],
                'core_times': [],
                'shift_id': 1,
                'timesheet': [
                    {
                        'employee_uid': 1,
                        'company_uid': 1,
                        'tags': [],
                        'state': True,
                        'timestamp': 1580186912.0
                    },
                    {
                        'employee_uid': 1,
                        'company_uid': 1,
                        'tags': [],
                        'state': False,
                        'timestamp': 1580220099.0
                    }
                ],
                'raw_timesheet': [
                    {
                        'employee_uid': 1,
                        'company_uid': 1,
                        'tags': [],
                        'state': True,
                        'timestamp': 1580186912.0
                    },
                    {
                        'employee_uid': 1,
                        'company_uid': 1,
                        'tags': [],
                        'state': False,
                        'timestamp': 1580220099.0
                    }
                ]
            }],
            'default_shift': {},
            'hours_worked': [],
            'tardiness_rules': [],
            'shift_dates': {
                '2020-01-30': {
                'rest_day': False,
                'holiday_type': None
                }
            },
            'timesheet': [
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580362834.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580392868.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580447309.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580474912.0
                }
            ],
            'raw_timesheet': [
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580101235.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580108525.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580109475.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580115741.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580119303.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580133731.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580186912.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580220099.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580274009.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580306458.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580362834.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580392868.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': True,
                    'timestamp': 1580447309.0
                },
                {
                    'employee_uid': 1,
                    'company_uid': 1,
                    'tags': [],
                    'state': False,
                    'timestamp': 1580474912.0
                }
            ]
        }

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        # 9 Hours of Expected Working Hours
        # Less 1 Hour Unpaid Break
        # Less 10 minutes Undertime
        # Total = (9 * 60) - (1 * 60)
        # Total = 480 minutes or (8)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 480
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 10

    def test_tsr_paid_break_with_nt_counter_per_hour(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has paid breaks 17:00 to 18:00
        > Night Time starts at 22:00 to 06:00 Counter Per Hour
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "17:00",
                "end": "18:00",
                "is_paid": True
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 1 * 60

    def test_tsr_paid_break_with_nt_carry_over(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has paid breaks 17:00 to 18:00
        > Night Time starts at 22:00 to 06:00 Carry Over
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "17:00",
                "end": "18:00",
                "is_paid": True
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 1 * 60

    def test_tsr_unpaid_break_with_nt_counter_per_hour(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has unpaid breaks 17:00 to 18:00
        > Night Time starts at 22:00 to 06:00 Counter Per Hour
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "17:00",
                "end": "18:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 1 * 60

    def test_tsr_unpaid_break_with_nt_carry_over(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has unpaid breaks 17:00 to 18:00
        > Night Time starts at 22:00 to 06:00 Carry Over
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "17:00",
                "end": "18:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 1 * 60

    def test_tsr_paid_break_with_nt_counter_per_hour_last_hour_break(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has paid breaks 22:00 to 23:00
        > Night Time starts at 22:00 to 06:00 Counter Per Hour
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "22:00",
                "end": "23:00",
                "is_paid": True
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 1 * 60

    def test_tsr_paid_break_with_nt_carry_over_last_hour_break(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has paid breaks 22:00 to 23:00
        > Night Time starts at 22:00 to 06:00 Carry Over
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "22:00",
                "end": "23:00",
                "is_paid": True
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60)
        assert attendance.get(shift_id).get(constants.ATT_NT) == 1 * 60

    def test_tsr_unpaid_break_with_nt_counter_per_hour_last_hour_break(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has unpaid breaks 22:00 to 23:00
        > Night Time starts at 22:00 to 06:00 Counter Per Hour
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "22:00",
                "end": "23:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60)

    def test_tsr_unpaid_break_with_nt_carry_over_last_hour_break(self):
        """
        > Fixed schedule 14:00 to 23:00
        > TSR
        > Has unpaid breaks 22:00 to 23:00
        > Night Time starts at 22:00 to 06:00 Carry Over
        > Has time record 14:00 to 23:00
        """
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.set_shift_dates([(
            "2019-01-02",
            False, # Is Rest Day?
            None # Holiday Type
        )])

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "14:00",
                "end_time": "23:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "22:00",
                "end": "23:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60)

    def test_fixed_shift_tsr_regular(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:00:00", "clock_in"),
            ("2020-03-11 10:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60

    def test_fixed_shift_tsr_regular_no_timesheet(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert not attendance.get("1").get(constants.ATT_REGULAR)

    def test_fixed_shift_tsnr_regular(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet not required
        4. Regular
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:00:00", "clock_in"),
            ("2020-03-11 10:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60

    def test_fixed_shift_tsnr_regular_no_timesheet(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet not required
        4. Regular
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60

    def test_fixed_shift_tsr_regular_undertime_unpaid_break(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Undertime
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:00:00", "clock_in"),
            ("2020-03-11 16:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_fixed_shift_tsr_regular_tardiness_rule_unpaid_break_scenario1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=15
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:20:00", "clock_in"),
            ("2020-03-11 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == (7 * 60) + 55
        assert attendance.get("1").get(constants.ATT_TARDY) == 5

    def test_fixed_shift_tsr_regular_tardiness_rule_unpaid_break_scenario2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=15
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_TARDY) == 1

    def test_fixed_shift_tsr_regular_tardiness_rule_unpaid_break_scenario3a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_TARDY) == 16

    def test_fixed_shift_tsr_regular_tardiness_rule_unpaid_break_scenario3b(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:13:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert not attendance.get("1").get(constants.ATT_TARDY)

    def test_fixed_shift_tsr_regular_tardiness_rule_unpaid_break_scenario4(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=15
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_TARDY) == 1

    def test_fixed_shift_tsr_regular_tardiness_rule_unpaid_break_scenario5a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_TARDY) == 16

    def test_fixed_shift_tsr_regular_tardiness_rule_unpaid_break_scenario5b(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:10:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_TARDY) == 10

    def test_fixed_shift_tsr_regular_overbreak(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Fixed
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        7. Overbreak
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:00:00", "clock_in"),
            ("2020-03-11 12:00:00", "clock_out"),
            ("2020-03-11 13:30:00", "clock_in"),
            ("2020-03-11 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == (7 * 60) + 30
        assert attendance.get("1").get(constants.ATT_OVERBREAK) == 30

    def test_tsr_halfday_with_paid_leave_with_paid_break_scenario_9(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has paid breaks
        4. Has half day paid leave 12:00 - 17:00
        5. Time records 8:00 AM - 12:00 PM

        """
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 12:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 5 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60

    def test_fixed_break_with_ut_overbreak(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid breaks 12:00 - 13:00
        4. Time records
           > 08:30 - 12:00
           > 13:30 - 17:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.BREAK_HOURS: "1:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 3
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60)

    def test_fixed_break_with_tardy_ut_overbreak(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid breaks 12:00 - 13:00
        4. Time records
           > 08:30 - 12:00
           > 13:30 - 17:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.BREAK_HOURS: "1:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 4
        assert attendance.get(shift_id).get(constants.ATT_TARDY) == 30
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (6 * 60) + 30


    def test_flexi_break_with_ut(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        4. Time records
           > 08:30 - 12:00
           > 13:00 - 17:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60) + 30

    def test_flexi_break_with_ut_overbreak(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        4. Time records
           > 08:30 - 12:00
           > 13:30 - 17:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 3
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60)

    def test_flexi_break_with_ut_overbreak_tardy(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        4. Time records
           > 08:30 - 12:00
           > 13:30 - 16:30
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 4
        assert attendance.get(shift_id).get(constants.ATT_TARDY) == 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (6 * 60) + 30

    def test_flexi_break_tardy(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_TARDY) == 60
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 7 * 60

    def test_flexi_unpaid_break(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Unpaid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_out"),
            ("2019-01-01 10:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_unpaid_break_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Unpaid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_break_tardy_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_break_with_ut_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_break_with_ut_overbreak_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_break_with_ut_overbreak_tardy_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid flexi breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_paid_break(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_out"),
            ("2019-01-01 10:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_flexi_paid_break_tardy(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        Tardy Rule start deducting at 0 minutes, grace period 0
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get(shift_id).get(constants.ATT_TARDY) == 1 * 60

    def test_flexi_paid_break_ut(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 16:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 1 * 60

    def test_flexi_paid_break_overbreak(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60) + 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30

    def test_flexi_paid_break_tardy_undertime_overbreak(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        Tardy Rule start deducting at 0 minutes, grace period 0
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 4
        assert attendance.get(shift_id).get(constants.ATT_TARDY) == 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60) + 30

    def test_flexi_paid_break_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_out"),
            ("2019-01-01 10:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_flexi_paid_break_tardy_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        Tardy Rule start deducting at 0 minutes, grace period 0
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_flexi_paid_break_ut_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 16:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_flexi_paid_break_overbreak_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_flexi_paid_break_tardy_undertime_overbreak_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Flexi Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        Tardy Rule start deducting at 0 minutes, grace period 0
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "08:00",
                keys.END: "17:00",
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_flexi_break_starts_mid_shift_tsnr(self):
        """
        1. Fixed schedule 21:00 - 06:00
        2. TSNR
        3. Has unpaid flexi breaks 00:00 - 03:00 1 Hour
        4. NT starts 22:00 to 06:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False)

        mock.default_shift()
        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "21:00",
                keys.END_TIME: "06:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "00:00",
                keys.END: "03:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_NT) == (7 * 60)
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (1 * 60)

    def test_flexi_break_starts_mid_shift_tsr(self):
        """
        1. Fixed schedule 21:00 - 06:00
        2. TSR
        3. Has unpaid flexi breaks 00:00 - 03:00 1 Hour
        4. NT starts 22:00 to 06:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "21:00",
                keys.END_TIME: "06:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "00:00",
                keys.END: "03:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_NT) == (7 * 60)
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (1 * 60)

    def test_flexi_break_starts_mid_shift_partially_consumed_tsr(self):
        """
        1. Fixed schedule 21:00 - 06:00
        2. TSR
        3. Has unpaid flexi breaks 00:00 - 03:00 1 Hour
        4. NT starts 22:00 to 06:00
        5. Consumed Break 00:00 to 00:30
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "21:00",
                keys.END_TIME: "06:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "00:00",
                keys.END: "03:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 00:00:00", "clock_out"),
            ("2019-01-02 00:30:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_NT) == (7 * 60)
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (1 * 60)

    def test_flexi_break_starts_mid_shift_fully_consumed_tsr(self):
        """
        1. Fixed schedule 21:00 - 06:00
        2. TSR
        3. Has unpaid flexi breaks 00:00 - 03:00 1 Hour
        4. NT starts 22:00 to 06:00
        5. Consumed Break 00:00 to 01:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "21:00",
                keys.END_TIME: "06:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "00:00",
                keys.END: "03:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 00:00:00", "clock_out"),
            ("2019-01-02 01:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_NT) == (7 * 60)
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (1 * 60)

    def test_flexi_break_starts_mid_shift_fully_consumed_tsr(self):
        """
        1. Fixed schedule 21:00 - 06:00
        2. TSR
        3. Has unpaid flexi breaks 00:00 - 03:00 1 Hour
        4. NT starts 22:00 to 06:00
        5. Consumed Break 00:00 to 01:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "21:00",
                keys.END_TIME: "06:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "00:00",
                keys.END: "03:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 00:00:00", "clock_out"),
            ("2019-01-02 01:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_NT) == (7 * 60)
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (1 * 60)

    def test_flexi_break_starts_mid_shift_unpaid_break_with_overbreak_tsr(self):
        """
        1. Fixed schedule 21:00 - 06:00
        2. TSR
        3. Has unpaid flexi breaks 00:00 - 03:00 1 Hour
        4. NT starts 22:00 to 06:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "21:00",
                keys.END_TIME: "06:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "01:00",
                keys.START: "00:00",
                keys.END: "03:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out"),
            ("2019-01-02 04:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 3
        assert attendance.get(shift_id).get(constants.ATT_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (1 * 60)
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == (1 * 60)

    def test_4607_ot_nt_span_on_next_day_computation(self):
        """
            Shift: 3PM-11PM
            Tardiness: 15 gracec period 15 to deduct
            TSR
            OT Entitled
            Timesheets:
                IN: 2020-03-21 6:05 PM
                OUT: 2020-03-22 7:04 AM
            Hours Worked:
                OT
                    Start: 2020-03-21 11:00 PM
                    End: 2020-03-22 03:00 AM
        """

        mock_data = {
            'date': '2020-03-21',
            'night_shift_settings': {
                'id': 1,
                'company_id': 1,
                'activated': True,
                'start': '22:00',
                'end': '06:00',
                'time_shift': 'Carry over'
            },
            'tardiness_rule': {
                'id': 1,
                'name': '15minutes',
                'company_id': 1,
                'minutes_tardy': 15,
                'minutes_to_deduct': 15,
                'affected_employees': [{
                'id': None,
                'name': 'All employees',
                'type': 'employee'
                }],
                'updated_at': '2020-01-15 10:59'
            },
            'id': 1,
            'user_id': 1,
            'status': 'active',
            'employee_id': 'E_ID_4607',
            'company_id': 1,
            'first_name': 'First',
            'middle_name': 'Middle',
            'last_name': 'Last',
            'full_name': 'First Middle Last',
            'department_id': 1,
            'department_name': 'Department',
            'position_id': 1,
            'position_name': 'Position',
            'location_id': 1,
            'location_name': 'Location',
            'hours_per_day': None,
            'date_hired': '2019-07-01',
            'date_ended': None,
            'employment_type_id': 240,
            'employment_type_name': 'Probationary',
            'timesheet_required': True,
            'overtime': True,
            'differential': True,
            'regular_holiday_pay': True,
            'special_holiday_pay': True,
            'holiday_premium_pay': True,
            'rest_day_pay': True,
            'employee_uid': 1,
            'rest_days': [],
            'shifts': [{
                'name': '3PM-11PM (RD Monday)',
                'type': 'fixed',
                'start_date': '2020-01-01',
                'start_time': '15:00',
                'end_time': '23:00',
                'total_hours': '08:00',
                'allowed_time_methods': ['Bundy App', 'Time Sheet', 'Biometric'],
                'affected_employees': [{
                'id': 1,
                'schedule_id': 1,
                'type': 'employee',
                'rel_id': None,
                'created_at': '2020-03-03 15:40:06',
                'updated_at': '2020-03-03 15:40:06'
                }],
                'breaks': [],
                'core_times': [],
                'shift_id': 1,
                'timesheet': {},
                'raw_timesheet': [{
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584659816.0
                }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584713886.0
                }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584745778.0
                }],
                'computed_attendance': 57
            }],
            'default_shift': {},
            'hours_worked': [{
                'id': 1,
                'employee_id': 1,
                'date': '2020-03-21',
                'type': 'overtime',
                'type_name': 'Overtime',
                'time': '04:00',
                'value': '4.00',
                'shift_id': '1',
                'leave_type_id': None,
                'start_datetime': '2020-03-21 23:00',
                'end_datetime': '2020-03-22 03:00'
            }],
            'tardiness_rules': [],
            'shift_dates': {
                '2020-03-21': {
                'rest_day': False,
                'holiday_type': None
                }
            },
            'timesheet': [{
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584785100.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584831840.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584888180.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584918600.0
            }],
            'raw_timesheet': [{
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584572751.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584626978.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584659816.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584713886.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584745778.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584785115.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584831844.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584888221.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1584918624.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1584972062.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585004939.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585058563.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585091212.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585147733.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585177401.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': True,
                'timestamp': 1585231763.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1585263780.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': True,
                'timestamp': 1585319238.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1585350180.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': True,
                'timestamp': 1585392992.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1585436580.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': True,
                'timestamp': 1585492377.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1585523321.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585577847.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585610043.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585664185.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585695829.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585752823.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585784043.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585837814.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585869228.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585924508.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': False,
                'timestamp': 1585955605.0
            }, {
                'employee_uid': 1,
                'company_uid': '1',
                'state': True,
                'timestamp': 1585996123.0
            }]
        }

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_TARDY) == 170
        assert attendance.get("1").get(constants.ATT_REGULAR) == 250
        assert attendance.get("1").get(constants.ATT_NT) == 60
        assert attendance.get("1").get(constants.ATT_NT_OT) == 240

    def test_4775_tardiness_rule_minutes_to_deduct_set_to_none(self):
        """
            Tardiness settings minutes to deduct None
        """

        mock_data = {
            'date': '2020-05-20',
            'night_shift_settings': {
                'id': 5696,
                'company_id': 5711,
                'activated': True,
                'start': '22:00',
                'end': '06:00',
                'time_shift': 'Counter per hour'
            },
            'tardiness_rule': {
                'id': 1746,
                'name': '1',
                'company_id': 5711,
                'minutes_tardy': 0,
                'minutes_to_deduct': None,
                'affected_employees': [{
                    'id': None,
                    'name': 'All positions',
                    'type': 'position'
                }],
                'updated_at': '2020-04-16 16:12'
            },
            'id': 25976,
            'user_id': 39249,
            'status': 'active',
            'employee_id': 'tapr_jan2020-01',
            'company_id': 5711,
            'first_name': 'Gfriend',
            'middle_name': '',
            'last_name': 'Semi Regular',
            'full_name': 'Gfriend Semi Regular',
            'department_id': 12930,
            'department_name': 'Engineering',
            'position_id': 16000,
            'position_name': 'Engineering Supervisor',
            'location_id': 6344,
            'location_name': 'Main Office',
            'hours_per_day': None,
            'date_hired': '2019-05-15',
            'date_ended': None,
            'employment_type_id': 10415,
            'employment_type_name': 'Regular',
            'timesheet_required': True,
            'overtime': True,
            'differential': True,
            'regular_holiday_pay': True,
            'special_holiday_pay': True,
            'holiday_premium_pay': True,
            'rest_day_pay': True,
            'employee_uid': 25976,
            'rest_days': [],
            'shifts': [{
                'name': 'FIXED - NS - 22:00 to 08:00',
                'type': 'fixed',
                'start_date': '2020-01-01',
                'start_time': '20:00',
                'end_time': '05:00',
                'total_hours': '09:00',
                'allowed_time_methods': ['Bundy App', 'Time Sheet', 'Web Bundy', 'Biometric'],
                'affected_employees': [],
                'breaks': [{
                    'id': 19027,
                    'schedule_id': 18286,
                    'type': 'fixed',
                    'start': '01:00',
                    'end': '02:00',
                    'break_hours': '01:00',
                    'is_paid': True
                }],
                'core_times': [],
                'shift_id': 1
            }],
            'default_shift': {},
            'hours_worked': [],
            'tardiness_rules': [],
            'shift_dates': {
                '2020-05-20': {
                    'rest_day': False,
                    'holiday_type': None
                },
                '2020-05-21': {
                    'rest_day': False,
                    'holiday_type': None
                }
            },
            'timesheet': [{
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589983200.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1590019200.0
            }],
            'raw_timesheet': [{
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1588287600.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1588550400.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1588633200.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1588723200.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1588802400.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589180400.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1589212800.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589259600.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1589295600.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589374800.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589461200.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1589500800.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589810400.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1589846400.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589896800.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1589932800.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': True,
                'timestamp': 1589983200.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1590019200.0
            }, {
                'employee_uid': 25976.0,
                'company_uid': '5711',
                'tags': [],
                'state': False,
                'timestamp': 1590051600.0
            }],
            'previous_shift': {
                'name': 'FIXED - NS - 22:00 to 08:00',
                'type': 'fixed',
                'start_date': '2020-01-01',
                'start_time': '20:00',
                'end_time': '05:00',
                'total_hours': '09:00',
                'allowed_time_methods': ['Bundy App', 'Time Sheet', 'Web Bundy', 'Biometric'],
                'affected_employees': [],
                'breaks': [{
                    'id': 19027,
                    'schedule_id': 18286,
                    'type': 'fixed',
                    'start': '01:00',
                    'end': '02:00',
                    'break_hours': '01:00',
                    'is_paid': True
                }],
                'core_times': [],
                'shift_id': 8985,
                'start_timestamp': 1589889600,
                'end_timestamp': 1589922000
            }
        }

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_TARDY) == 120
        assert attendance.get("1").get(constants.ATT_NT) == 420
    def test_floating_break_with_ut(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        4. Time records
           > 08:30 - 12:00
           > 13:00 - 17:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60) + 30

    def test_floating_break_with_ut_overbreak(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        4. Time records
           > 08:30 - 12:00
           > 13:30 - 17:00
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 3
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60)

    def test_floating_break_with_ut_overbreak_tardy(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        4. Time records
           > 08:30 - 12:00
           > 13:30 - 16:30
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 4
        assert attendance.get(shift_id).get(constants.ATT_TARDY) == 30
        assert attendance.get(shift_id).get(constants.ATT_OVERBREAK) == 30
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (6 * 60) + 30

    def test_floating_break_tardy(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_TARDY) == 60
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 7 * 60

    def test_floating_unpaid_break(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Unpaid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_out"),
            ("2019-01-01 10:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_floating_unpaid_break_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Unpaid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_floating_break_tardy_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_floating_break_with_ut_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_floating_break_with_ut_overbreak_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_floating_break_with_ut_overbreak_tardy_tsnr(self):
        """
        1. Fixed schedule 8:00 - 17:00
        2. TSNR
        3. Has unpaid floating breaks 08:00 - 17:00 1 Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True)

        mock.default_shift()

        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "01:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 8 * 60

    def test_floating_paid_break(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_out"),
            ("2019-01-01 10:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_floating_paid_break_tardy(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        Tardy Rule start deducting at 0 minutes, grace period 0
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_floating_paid_break_ut(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 16:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_floating_paid_break_overbreak(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (8 * 60) + 30
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 30

    def test_floating_paid_break_tardy_undertime_overbreak(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSR
        Tardy Rule start deducting at 0 minutes, grace period 0
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_UNDERTIME) == 90
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == (7 * 60) + 30

    def test_floating_paid_break_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_out"),
            ("2019-01-01 10:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_floating_paid_break_tardy_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        Tardy Rule start deducting at 0 minutes, grace period 0
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_floating_paid_break_ut_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 16:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_floating_paid_break_overbreak_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_floating_paid_break_tardy_undertime_overbreak_tsnr(self):
        """
        Fixed Shift 08:00 to 17:00
        Floating Break: 08:00 to 17:00 (1 Hour, Paid)
        TSNR
        Tardy Rule start deducting at 0 minutes, grace period 0
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "08:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )

        mock.timesheet([
            ("2019-01-01 08:30:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:30:00", "clock_in"),
            ("2019-01-01 16:30:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 1
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 9 * 60

    def test_current_day_midnight_nightshift_settings_cpr(self):
        """
        Fixed Shift 3:00 - 12:00
        Fixed Break: 07:00 - 08:00
        Nightshift Settings: 22:00 - 06:00 Counter Per Hour
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(date=shift_date)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "03:00",
                keys.END_TIME: "12:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "07:00",
                keys.END: "08:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 03:00:00", "clock_in"),
            ("2019-01-01 07:00:00", "clock_out"),
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out")
        ])

        mock.night_shift_settings(end="06:00")

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_NT) == 3 * 60
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 5 * 60

    def test_current_day_midnight_nightshift_settings_carry_over(self):
        """
        Fixed Shift 3:00 - 12:00
        Fixed Break: 07:00 - 08:00
        Nightshift Settings: 22:00 - 06:00 Carry over
        """

        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(date=shift_date)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "03:00",
                keys.END_TIME: "12:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "07:00",
                keys.END: "08:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 03:00:00", "clock_in"),
            ("2019-01-01 07:00:00", "clock_out"),
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out")
        ])

        mock.night_shift_settings(end="06:00", time_shift="Carry over")

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_NT) == 3 * 60
        assert attendance.get(shift_id).get(constants.ATT_REGULAR) == 5 * 60

    def test_undertime_no_intersection_with_flexi_break(self):
        data = utils.get_mock("flexi_break_none_type_not_iterable")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("ds_72").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 4 * 60

    def test_absent_with_floating_break_deducted(self):
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(date=shift_date)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "07:00",
                keys.END_TIME: "16:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "1:00",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.night_shift_settings(end="06:00", time_shift="Carry over")

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_ABSENT) == 8 * 60

    def test_absent_with_flexi_break_deducted(self):
        shift_id = "1"
        shift_date="2019-01-01"

        mock = utils.MockHero(date=shift_date)

        mock.default_shift()
        mock.shifts([
            {
                keys.TYPE: keys.SH_FIXED,
                keys.START_TIME: "07:00",
                keys.END_TIME: "16:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FLEXI,
                keys.BREAK_HOURS: "1:00",
                keys.START: "12:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.night_shift_settings(end="06:00", time_shift="Carry over")

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get(shift_id).get(constants.ATT_ABSENT) == 8 * 60

    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_fixed_night_shift_tsnr_sh_uh_both_entitled_to_rh_sh_scenario1_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario1_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Carry over)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=None,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "8:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day1_scenario_1(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
    
    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day1_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day1_scenario_2(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT) == 2 * 60

    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario2_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day1(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_fixed_night_shift_tsnr_sh_uh_both_entitled_to_rh_sh_scenario3_day1(self, restd):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 5 * 60

    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario3_day2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-21",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "01:00",
                keys.END: "02:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-21 21:00:00", "clock_in"),
            ("2020-07-22 06:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60

    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shit has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shit has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shit has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_fixed_night_shift_tsr_sh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
    
    def test_fixed_night_shift_tsr_sh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_fixed_night_shift_tsr_sh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 7 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 1 * 60

    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_with_ts_scenario_2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario4_day1_without_ts(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_fixed_night_shift_tsnr_sh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_fixed_night_shift_tsnr_sh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_1_no_custom_shift(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays false
        """
        # Attendance should'nt be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )
        mock.default_shift()
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_fixed_night_shift_tsnr_sh_uh_both_entitled_to_rh_sh_scenario4_day1_scenario_2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_fixed_night_shift_tsr_rh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_fixed_night_shift_tsnr_rh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_fixed_night_shift_tsnr_sh_uh_both_entitled_to_rh_sh_scenario5_day1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/5813

        1. Fixed
        2. Timesheet not required
        3. Entitled to Unworked Holiday
        4. Night shift schedule (Counter per hour)
        5. Overnight
        6. Not entitled to Overtime
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "08:00"
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "13:00",
                keys.END: "14:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_tsr_with_floating_paid_break_last_clockout_before_shift_end(self):
        """Test specs:

        https://code.salarium.com/salarium/development/t-and-a-manila/issues/6194

        1. Fixed
        2. Timesheet required
        3. With 1h unpaid fixed break
        4. With 30m paid floating break
        5. Last clock out occurred 1hr before end of
        """
        mock = utils.MockHero(
            date="2020-09-01",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            overtime=False
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "06:00",
                "end_time": "15:30",
                "total_hours": "09:30"
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "10:00",
                keys.END: "11:00",
                keys.IS_PAID_BREAK: False
            },
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "0:30",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2020-09-01 06:00:00", "clock_in"),
            ("2020-09-01 10:00:00", "clock_out"),
            ("2020-09-01 11:00:00", "clock_in"),
            ("2020-09-01 14:30:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 0.5 * 60

    def test_tsr_with_floating_paid_break_regular(self):
        """Test specs:

        https://code.salarium.com/salarium/development/t-and-a-manila/issues/6194

        1. Fixed
        2. Timesheet required
        3. With 1h unpaid fixed break
        4. With 30m paid floating break
        5. Paid floating break hours goes to REGULAR
        """
        mock = utils.MockHero(
            date="2020-09-01",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            overtime=False
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "06:00",
                "end_time": "15:30",
                "total_hours": "09:30"
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "10:00",
                keys.END: "11:00",
                keys.IS_PAID_BREAK: False
            },
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "0:30",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2020-09-01 06:00:00", "clock_in"),
            ("2020-09-01 10:00:00", "clock_out"),
            ("2020-09-01 11:00:00", "clock_in"),
            ("2020-09-01 11:30:00", "clock_out"),
            ("2020-09-01 12:00:00", "clock_in"),
            ("2020-09-01 15:30:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8.5 * 60

    def test_tsr_with_floating_paid_break_uh(self):
        """Test specs:

        https://code.salarium.com/salarium/development/t-and-a-manila/issues/6194

        1. Fixed
        2. Timesheet required
        3. With 1h unpaid fixed break
        4. With 30m paid floating break
        5. Regular holiday
        """
        mock = utils.MockHero(
            date="2020-09-01",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            overtime=False
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "06:00",
                "end_time": "15:00",
                "total_hours": "09:00"
            }
        ])

        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "10:00",
                keys.END: "11:00",
                keys.IS_PAID_BREAK: False
            },
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "0:30",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.set_shift_dates([
            ("2020-09-01", False, constants.ATT_RH),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_tsr_with_floating_paid_break_regular_compensate_tardy(self):
        """Test specs:

        MAWSI
        """
        mock = utils.MockHero(
            date="2020-09-01",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            overtime=False
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            },
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "0:30",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2020-09-01 09:00:00", "clock_in"),
            ("2020-09-01 17:00:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 7.5 * 60
        assert attendance.get("1").get(constants.ATT_TARDY) == 0.5 * 60

    def test_tsr_with_floating_paid_break_regular_compensate_tardy_undertime(self):
        """Test specs:

        MAWSI
        """
        mock = utils.MockHero(
            date="2020-09-01",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            overtime=False
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "total_hours": "08:00"
            }
        ])

        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )

        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "1:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            },
            {
                keys.TYPE: keys.BR_FLOATING,
                keys.BREAK_HOURS: "0:30",
                keys.START: None,
                keys.END: None,
                keys.IS_PAID_BREAK: True
            }
        ])

        mock.timesheet([
            ("2020-09-01 08:40:00", "clock_in"),
            ("2020-09-01 16:40:00", "clock_out"),
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 7.5 * 60
        assert attendance.get("1").get(constants.ATT_TARDY) == 10
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 20
