# pylint: disable=C0111
"""Span tests
"""
import unittest
from attendance.spans import create_span, SS, MS, SpanConflictError


class TestSpan(unittest.TestCase):
    """Test for spans"""

    def test_create_simple_span(self):
        """Test create"""
        s = create_span((1, 2))
        assert isinstance(s, SS)
        assert s.i == 1
        assert s.j == 2

    def test_create_multi_span(self):
        """Test create multispan"""
        s = create_span((1, 2), (3, 4))
        assert isinstance(s, MS)

    def test_conflict(self):
        """Span conflict checks"""
        tests = [
            #name, s1, s2, isconflict
            ("(1,2) vs (3,4)", SS(1, 2), SS(3, 4), False),
            ("(1,2) vs (2,3)", SS(1, 2), SS(2, 3), False),
            ("(1,3) vs (2,4)", SS(1, 3), SS(2, 4), True),
            ("(2,4) vs (1,3)", SS(2, 4), SS(1, 3), True),
            # TODO: add tests for multispans
        ]

        for name, s1, s2, conflict in tests:
            assert s1.isconflict_with(s2) == conflict, name

    def test_int_value(self):
        """Test value property"""
        tests = [
            #name, s1, expected
            ("SS(1,2)", SS(1, 2), 1),
            ("MS( (1,2) (3,4) )", MS([SS(1, 2), SS(3, 4)]), 2),
        ]

        for name, s1, expected in tests:
            assert s1.value == expected, name

    def test_add_two_spans(self):
        """Span addition tests"""
        tests = [
            # (name, s1, s2, ok,  result) ok=False should be TypeError('conflict')
            ("SS(1,2)+SS(2,3)", SS(1, 2), SS(2, 3), True, SS(1, 3)),
            ("SS(2,3)+SS(1,2)", SS(2, 3), SS(1, 2), True, SS(1, 3)),
            ("SS(1,3)+SS(2,3)", SS(1, 3), SS(2, 3), False, None),
            ("SS(2,4)+SS(1,3)", SS(2, 4), SS(1, 3), False, None),
            ("SS(1,2)+SS(3,4)", SS(1, 2), SS(3, 4),
             True, MS([SS(1, 2), SS(3, 4)])),
            ("MS((1,2), (3,4)) + MS((5,6), (7,8))",
             create_span((1, 2), (3, 4)),
             create_span((5, 6), (7, 8)),
             True,
             create_span((1, 2), (3, 4), (5, 6), (7, 8))),
            ("MS((1,2), (2,3)) + MS((3,4), (4,5))",
             MS([SS(1, 2), SS(2, 3)]),
             MS([SS(3, 4), SS(4, 5)]),
             True,
             SS(1, 5)),
            ("MS((1,2), (3,4)) + MS((3,6), (7,8))",
             create_span((1, 2), (3, 4)),
             create_span((3, 6), (7, 8)),
             False,
             None),
        ]

        for name, s1, s2, ok, result in tests:
            if not ok:
                with self.assertRaises(SpanConflictError, msg=name):
                    res = s1 + s2
            else:
                res = s1 + s2
                if isinstance(res, SS):
                    assert res.i == result.i, name
                    assert res.j == result.j, name

                if isinstance(res, MS):
                    assert res == result, name

    def test_sub_two_spans(self):
        """Span subtraction tests"""
        tests = [
            ("S(2, 4) - S(1, 3)", SS(2, 4), SS(1, 3), SS(3, 4)),
            ("S(1, 4) - S(2, 5)", SS(1, 4), SS(2, 5), SS(1, 2)),
            ("S(4, 5) - S(4, 5)", SS(4, 5), SS(4, 5), None),
            ("S(4, 5) - S(1, 7)", SS(4, 5), SS(1, 7), None),
            ("S(1, 7) - S(4, 5)", SS(1, 7),
             SS(4, 5), MS([SS(1, 4), SS(5, 7)])),
            ("S(3, 6) - M((1,4), (5,7))", SS(3, 6),
             MS([SS(1, 4), SS(5, 7)]), SS(4, 5)),
            ("M((1,4), (5,8)) -  S(3,6)",
             MS([SS(1, 4), SS(5, 8)]), SS(3, 6), MS([SS(1, 3), SS(6, 8)])),
            ("M((1,4), (5,8)) - M((2,6), (7-9))",
             MS([SS(1, 4), SS(5, 8)]), MS([SS(2, 6), SS(7, 9)]), MS([SS(1, 2), SS(6, 7)])),
        ]

        for name, s1, s2, res in tests:
            assert s1 - s2 == res, name

    def test_intersection(self):
        """Span intersection tests
        """

        tests = [
            #name, S1, S2, result
            ("Disjoint", SS(1, 2), SS(2, 3), None),
            ("Simple test", SS(1, 3), SS(2, 4), SS(2, 3)),
            ("Full intersect", SS(1, 10), SS(1, 10), SS(1, 10)),
            ("Full intersect 2", SS(1, 10), SS(1, 11), SS(1, 10)),
            ("Single span  ∩  multiple span", SS(4, 7), MS(
                [SS(1, 5), SS(6, 8)]), MS([SS(4, 5), SS(6, 7)])),
            ("Single span  ∩  multiple span no intersect",
             SS(4, 7), MS([SS(1, 4), SS(7, 8)]), None),
            ("Multi vs multi -simple test",
             MS([SS(1, 4), SS(6, 7)]), MS([SS(3, 5), SS(7, 8)]), SS(3, 4)),

        ]

        for name, s1, s2, res in tests:
            assert s1.intersection(s2) == res, name

    def test_indexing(self):
        """Test indices"""
        tests = [
            #name, target, len, target[0], target.i, target.j
            ("Single", SS(1, 2), 1, SS(1, 2), 1, 2),
            ("Multi", MS([SS(4, 5), SS(7, 8)]), 2, SS(4, 5), 4, 8),
        ]

        for name, target, length, firstelem, i, j in tests:
            assert len(target) == length, name
            assert target[0] == firstelem, name
            assert target.i == i, name
            assert target.j == j, name

    def test_iteration(self):
        test_ms = MS([SS(4, 5), SS(7, 8)])
        print(enumerate(test_ms))
        for idx, sp in enumerate(test_ms):
            if idx == 0:
                assert sp.i == 4 and sp.j == 5
            if idx == 1:
                assert sp.i == 7 and sp.j == 8

        test_ss = SS(1, 2)
        for idx, sp in enumerate(test_ss):
            if idx == 0:
                assert sp.i == 1 and sp.j == 2

    def test_union(self):
        tests = [
            ("SS u SS", SS(1, 3), SS(2, 4), SS(1, 4)),
            ("SS u MS", SS(1, 3), MS([SS(2, 4), SS(5, 6)]), MS(
                [SS(1, 4), SS(5, 6)])),
            ("MS u MS", MS([SS(1, 2), SS(3, 4)]),
             MS([SS(2, 3), SS(4, 5)]), SS(1, 5)),
        ]
        for name, sp1, sp2, result in tests:
            print("SP1 ", str(sp1))
            print("SP2 ", str(sp2))
            print("Union ", str(sp1.union(sp2)))
            print("Union ", str(sp2.union(sp1)))
            print(type(sp1.union(sp2)))
            assert sp1.union(sp2) == result, name
            assert sp2.union(sp1) == result, name
