#pylint: disable=C0111
"""Test for filetasks"""
from unittest import TestCase

from datetime import datetime, timedelta
from attendance.filetasks import is_entitled_to_hour_code, get_matched_att_import_hrs_types, parse_file_contents, validate_employees_data, _replace_shift_with_default
import attendance.constants as constants
import attendance.keys as keys

class TestFileTasks(TestCase):
    """ Tests for filetasks"""


    def test_invalid_attendance_code(self):
        invalid_code = "SH+NT+HHHHH"
        csv_content = """Employee Id,Employee Name,Timesheet Date,Tags,Clock In,Clock Out,SH+NT, """+invalid_code+"""
                              EU1,EUONE EUONE EUONE,12/04/2019,xx,,,8:00,8:00
                              EU1,EUONE EUONE EUONE,12/05/2019,xx,,,8:00,8:00"""
        att_params = {
            'company_id': '1743',
            's3_bucket': 'v3-stg-uploads',
            's3_key': 'time_attendance_import:1743:5dedfa128e3c5',
            'job_id': '76e0c33c-e7ae-4f23-bc61-8060280d5aeb',
            'row_errors': [],
            'file_contents': csv_content
        }
        response = parse_file_contents(att_params)
        assert len(response['row_errors']) > 0

    def test_attendance_codes_ok(self):
        invalid_code = "SH+NT+HHHHH"
        csv_content = """Employee Id,Employee Name,Timesheet Date,Tags,Clock In,Clock Out,SH+NT,RD+OT
                              EU1,EUONE EUONE EUONE,12/04/2019,xx,,,8:00,8:00
                              EU1,EUONE EUONE EUONE,12/05/2019,xx,,,8:00,8:00"""
        att_params = {
            'company_id': '1743',
            's3_bucket': 'v3-stg-uploads',
            's3_key': 'time_attendance_import:1743:5dedfa128e3c5',
            'job_id': '76e0c33c-e7ae-4f23-bc61-8060280d5aeb',
            'row_errors': [],
            'file_contents': csv_content
        }
        response = parse_file_contents(att_params)
        assert len(response['row_errors']) == 0

    def test_invalid_attendance_codes_duration(self):
        invalid_time = "AAAAAA"
        csv_content = """Employee Id,Employee Name,Timesheet Date,Tags,Clock In,Clock Out,SH+NT,RD+OT
                              EU1,EUONE EUONE EUONE,12/04/2019,xx,,,8:00,"""+invalid_time+"""
                              EU1,EUONE EUONE EUONE,12/05/2019,xx,,,8:00,8:00"""
        att_params = {
            'company_id': '1743',
            's3_bucket': 'v3-stg-uploads',
            's3_key': 'time_attendance_import:1743:5dedfa128e3c5',
            'job_id': '76e0c33c-e7ae-4f23-bc61-8060280d5aeb',
            'row_errors': [],
            'file_contents': csv_content
        }
        response = parse_file_contents(att_params)
        assert len(response['row_errors']) > 0

    def test_invalid_duplicate_entries(self):
        csv_content = """Employee Id,Employee Name,Timesheet Date,Tags,Clock In,Clock Out,SH+NT,RD+OT
                              EU1,EUONE EUONE EUONE,12/05/2019,xx,,,8:00,8:00
                              EU1,EUONE EUONE EUONE,12/05/2019,xx,,,8:00,8:00"""
        att_params = {
            'company_id': '1743',
            's3_bucket': 'v3-stg-uploads',
            's3_key': 'time_attendance_import:1743:5dedfa128e3c5',
            'job_id': '76e0c33c-e7ae-4f23-bc61-8060280d5aeb',
            'row_errors': [],
            'file_contents': csv_content,
            'data': [
                {
                    'employee_id': 'EU1',
                    'employee_name': 'EUONE EUONE EUONE',
                    'timesheet_date': "2019-12-05"
                },
                {
                    'employee_id': 'EU1',
                    'employee_name': 'EUONE EUONE EUONE',
                    'timesheet_date': "2019-12-05"
                }
            ],
            'employees':
                {
                    'EU1': {
                        'id': 5519
                    }
                }
        }
        response = validate_employees_data(att_params)
        assert len(response['row_errors']) > 0

    def test_similar_time(self):
        csv_content = """Employee Id,Employee Name,Timesheet Date,Tags,Clock In,Clock Out
                              EU1,EUONE EUONE EUONE,12/04/2019,,8:00,8:00"""
        att_params = {
            'company_id': '1743',
            's3_bucket': 'v3-stg-uploads',
            's3_key': 'time_attendance_import:1743:5dedfa128e3c5',
            'job_id': '76e0c33c-e7ae-4f23-bc61-8060280d5aeb',
            'row_errors': [],
            'file_contents': csv_content
        }
        response = parse_file_contents(att_params)

        assert len(response['row_errors']) > 0

    def test_is_entitled_to_hour_code_ot_fail(self):
        att_code = constants.ATT_OT
        employee = {
            'overtime': False
        }

        assert is_entitled_to_hour_code(employee, att_code) == False

    def test_is_entitled_to_hour_code_ot_ok(self):
        att_code = constants.ATT_OT
        employee = {
            'overtime': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_nt_fail(self):
        att_code = constants.ATT_NT
        employee = {
            'differential': False
        }

        assert is_entitled_to_hour_code(employee, att_code) == False

    def test_is_entitled_to_hour_code_nt_ok(self):
        att_code = constants.ATT_NT
        employee = {
            'differential': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_rh_fail(self):
        att_code = constants.ATT_RH
        employee = {
            'regular_holiday_pay': False
        }

        assert is_entitled_to_hour_code(employee, att_code) == False

    def test_is_entitled_to_hour_code_rh_ok(self):
        att_code = constants.ATT_RH
        employee = {
            'regular_holiday_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_sh_fail(self):
        att_code = constants.ATT_SH
        employee = {
            'special_holiday_pay': False
        }

        assert is_entitled_to_hour_code(employee, att_code) == False

    def test_is_entitled_to_hour_code_sh_ok(self):
        att_code = constants.ATT_SH
        employee = {
            'special_holiday_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_rd_fail(self):
        att_code = constants.ATT_RD
        employee = {
            'rest_day_pay': False
        }

        assert is_entitled_to_hour_code(employee, att_code) == False

    def test_is_entitled_to_hour_code_rd_ok(self):
        att_code = constants.ATT_RD
        employee = {
            'rest_day_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_rh_ot_fail(self):
        att_code = constants.ATT_RH_NT_OT
        employee = {
            'regular_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': False
        }

        assert is_entitled_to_hour_code(employee, att_code) == False

    def test_is_entitled_to_hour_code_rh_ot_ok(self):
        att_code = constants.ATT_RH_NT_OT
        employee = {
            'regular_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_sh_ot_fail(self):
        att_code = constants.ATT_SH_NT_OT
        employee = {
            'special_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': False
        }

        assert is_entitled_to_hour_code(employee, att_code) == False

    def test_is_entitled_to_hour_code_sh_ot_ok(self):
        att_code = constants.ATT_SH_NT_OT
        employee = {
            'special_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_absent_skipped_ok(self):
        att_code = constants.ATT_ABSENT
        employee = {
            'special_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_regular_skipped_ok(self):
        att_code = constants.ATT_REGULAR
        employee = {
            'special_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_absent_skipped_ok(self):
        att_code = constants.ATT_ABSENT
        employee = {
            'special_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_is_entitled_to_hour_code_overbreak_skipped_ok(self):
        att_code = constants.ATT_OVERBREAK
        employee = {
            'special_holiday_pay': True,
            'differential': True,
            'overtime': True,
            'holiday_premium_pay': True
        }

        assert is_entitled_to_hour_code(employee, att_code) == True

    def test_get_matched_att_import_hrs_types_all_match(self):
        entry = {
            'hours_regular': '1:00',
            'hours_overtime': '1:00',
            'hours_night': '1:00',
            'hours_night_overtime': '1:00',
            'hours_rest_day': '1:00',
            'hours_special_holiday': '1:00',
            'hours_regular_holiday': '1:00',
            'hours_overbreak': '1:00',
            'hours_paid_leave': '1:00',
            'hours_unpaid_leave': '1:00',
            'hours_undertime': '1:00',
            'hours_unworked_holiday': '1:00',
            'hours_tardiness': '1:00',
            'hours_absent': '1:00',
            'hours_regular_holiday_night': '1:00',
            'hours_regular_holiday_overtime': '1:00',
            'hours_regular_holiday_night_overtime': '1:00',
            'hours_special_holiday_night': '1:00',
            'hours_special_holiday_overtime': '1:00',
            'hours_special_holiday_night_overtime': '1:00',
            'hours_regular_holiday_regular_holiday': '1:00',
            'hours_regular_holiday_regular_holiday_night': '1:00',
            'hours_regular_holiday_regular_holiday_overtime': '1:00',
            'hours_regular_holiday_regular_holiday_night_overtime': '1:00',
            'hours_rest_day_special_holiday': '1:00',
            'hours_rest_day_regular_holiday': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday': '1:00',
            'hours_rest_day_night': '1:00',
            'hours_rest_day_special_holiday_night': '1:00',
            'hours_rest_day_regular_holiday_night': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday_night': '1:00',
            'hours_rest_day_overtime': '1:00',
            'hours_rest_day_special_holiday_overtime': '1:00',
            'hours_rest_day_regular_holiday_overtime': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday_overtime': '1:00',
            'hours_rest_day_night_overtime': '1:00',
            'hours_rest_day_special_holiday_night_overtime': '1:00',
            'hours_rest_day_regular_holiday_night_overtime': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday_night_overtime': '1:00'
        }

        expected = [
            'hours_regular',
            'hours_overtime',
            'hours_night',
            'hours_night_overtime',
            'hours_rest_day',
            'hours_special_holiday',
            'hours_regular_holiday',
            'hours_overbreak',
            'hours_paid_leave',
            'hours_unpaid_leave',
            'hours_undertime',
            'hours_unworked_holiday',
            'hours_tardiness',
            'hours_absent',
            'hours_regular_holiday_night',
            'hours_regular_holiday_overtime',
            'hours_regular_holiday_night_overtime',
            'hours_special_holiday_night',
            'hours_special_holiday_overtime',
            'hours_special_holiday_night_overtime',
            'hours_regular_holiday_regular_holiday',
            'hours_regular_holiday_regular_holiday_night',
            'hours_regular_holiday_regular_holiday_overtime',
            'hours_regular_holiday_regular_holiday_night_overtime',
            'hours_rest_day_special_holiday',
            'hours_rest_day_regular_holiday',
            'hours_rest_day_regular_holiday_regular_holiday',
            'hours_rest_day_night',
            'hours_rest_day_special_holiday_night',
            'hours_rest_day_regular_holiday_night',
            'hours_rest_day_regular_holiday_regular_holiday_night',
            'hours_rest_day_overtime',
            'hours_rest_day_special_holiday_overtime',
            'hours_rest_day_regular_holiday_overtime',
            'hours_rest_day_regular_holiday_regular_holiday_overtime',
            'hours_rest_day_night_overtime',
            'hours_rest_day_special_holiday_night_overtime',
            'hours_rest_day_regular_holiday_night_overtime',
            'hours_rest_day_regular_holiday_regular_holiday_night_overtime'
        ]

        assert get_matched_att_import_hrs_types(entry) == expected

    def test_get_matched_att_import_hrs_types_has_missing(self):
        entry = {
            'hours_regular': '1:00',
            'hours_overtime': '1:00',
            'hours_night': '1:00',
            'hours_night_overtime': '1:00',
            'hours_rest_day': '1:00',
            'hours_special_holiday': '1:00',
            'hours_regular_holiday': '1:00',
            'hours_overbreak': '1:00',
            'hours_paid_leave': '1:00',
            'hours_unpaid_leave': '1:00',
            'hours_undertime': '1:00',
            'hours_unworked_holiday': '1:00',
            'hours_tardiness': '1:00',
            'hours_absent': '1:00',
            'hours_regular_holiday_night': '1:00',
            'hours_regular_holiday_overtime': '1:00',
            'hours_regular_holiday_night_overtime': '1:00',
            'hours_special_holiday_night': '1:00',
            'hours_special_holiday_overtime': '1:00',
            'hours_special_holiday_night_overtime': '1:00',
            'hours_regular_holiday_regular_holiday': '1:00',
            'hours_regular_holiday_regular_holiday_night': '1:00',
            'hours_regular_holiday_regular_holiday_overtime': '1:00',
            'hours_regular_holiday_regular_holiday_night_overtime': '1:00',
            'hours_rest_day_special_holiday': '1:00',
            'hours_rest_day_regular_holiday': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday': '1:00',
            'hours_rest_day_night': '1:00',
            'hours_rest_day_special_holiday_night': '1:00',
            'hours_rest_day_regular_holiday_night': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday_night': '1:00',
            'hours_rest_day_overtime': '1:00',
            'hours_rest_day_special_holiday_overtime': '1:00',
            'hours_rest_day_regular_holiday_overtime': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday_overtime': '1:00',
            'hours_rest_day_night_overtime': '1:00',
            'hours_rest_day_special_holiday_night_overtime': '1:00',
            'hours_rest_day_regular_holiday_night_overtime': '1:00',
            'hours_rest_day_regular_holiday_regular_holiday_night_overtime': '1:00',
            'hours_missing_from_list': '1:00'
        }

        expected = [
            'hours_regular',
            'hours_overtime',
            'hours_night',
            'hours_night_overtime',
            'hours_rest_day',
            'hours_special_holiday',
            'hours_regular_holiday',
            'hours_overbreak',
            'hours_paid_leave',
            'hours_unpaid_leave',
            'hours_undertime',
            'hours_unworked_holiday',
            'hours_tardiness',
            'hours_absent',
            'hours_regular_holiday_night',
            'hours_regular_holiday_overtime',
            'hours_regular_holiday_night_overtime',
            'hours_special_holiday_night',
            'hours_special_holiday_overtime',
            'hours_special_holiday_night_overtime',
            'hours_regular_holiday_regular_holiday',
            'hours_regular_holiday_regular_holiday_night',
            'hours_regular_holiday_regular_holiday_overtime',
            'hours_regular_holiday_regular_holiday_night_overtime',
            'hours_rest_day_special_holiday',
            'hours_rest_day_regular_holiday',
            'hours_rest_day_regular_holiday_regular_holiday',
            'hours_rest_day_night',
            'hours_rest_day_special_holiday_night',
            'hours_rest_day_regular_holiday_night',
            'hours_rest_day_regular_holiday_regular_holiday_night',
            'hours_rest_day_overtime',
            'hours_rest_day_special_holiday_overtime',
            'hours_rest_day_regular_holiday_overtime',
            'hours_rest_day_regular_holiday_regular_holiday_overtime',
            'hours_rest_day_night_overtime',
            'hours_rest_day_special_holiday_night_overtime',
            'hours_rest_day_regular_holiday_night_overtime',
            'hours_rest_day_regular_holiday_regular_holiday_night_overtime'
        ]

        assert get_matched_att_import_hrs_types(entry) == expected

    def test_no_shift_gets_replaced_with_default(self):
        default_shift = {
            "data": [{
            "id": 12090,
            "company_id": 1735,
            "day_of_week": 1,
            "day_type": "regular",
            "work_start": "08:00:00",
            "work_break_start": "12:00:00",
            "work_break_end": "13:00:00",
            "work_end": "17:00:00",
            "total_hours": "09:00"
            }]
        }

        target_date = "2020-01-13"
        shift_id = _replace_shift_with_default(default_shift, target_date)
        assert shift_id == default_shift[keys.DATA][0][keys.ID]

    def test_invalid_paid_leave_attendance_code(self):
        invalid_code = "Paid Leave"
        csv_content = """Employee Id,Employee Name,Timesheet Date,Tags,Clock In,Clock Out,"""+invalid_code+"""
                            EU1,EUONE EUONE EUONE,12/04/2019,xx,,,8:00
                            EU1,EUONE EUONE EUONE,12/05/2019,xx,,,8:00"""
        att_params = {
            'company_id': '1743',
            's3_bucket': 'v3-stg-uploads',
            's3_key': 'time_attendance_import:1743:5dedfa128e3c5',
            'job_id': '76e0c33c-e7ae-4f23-bc61-8060280d5aeb',
            'row_errors': [],
            'file_contents': csv_content
        }
        response = parse_file_contents(att_params)
        assert response['row_errors'][0].find(constants.PAID_LEAVE_ERROR) > 0
        assert response['row_errors'][1].find(constants.PAID_LEAVE_ERROR) > 0
