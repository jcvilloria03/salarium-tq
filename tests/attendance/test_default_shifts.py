# pylint: disable=W0212,C0301
"""Calculator unit tests"""
import unittest
import os
import time
from attendance import calculator, constants
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()

class TestDefaultShift(unittest.TestCase):
    """Default Calculator tests"""

    def test_uh(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Unworked Holiday
        """
        data = utils.get_mock("scheduled_hours_default_shift_with_uh_sh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_UH) == 8 * 60

    def test_tsr_uh(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Unworked Holiday
        3. TSR
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_with_uh_sh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_UH) == 8 * 60

    def test_tsnr_default_sched_with_ts_rd_without_ot(self):
        """
        Test default shift, rest day and with clockin/clockout
        1. Default Shift
        3. TSNR
        4. Default schedule Rest Day
        5. With clockin/clockout
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsnr_rd_with_ts")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_RD) == 9 * 0

    def test_tsnr_default_sched_rdrh(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Regular Holiday
        3. TSR
        4. Default schedule Rest Day
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsnr_with_rd_rh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        print("attendance")
        print(attendance)
        assert attendance.get("ds_72").get(constants.ATT_RD_RH) == 9 * 60

    def test_tsr_default_sched_tardy_without_deduction(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. TSR
        4. 15 minutes tardy
        5. 0 minutes to deduct
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_tardy_without_deduction")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("ds_72").get(constants.ATT_TARDY) == 30
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 7.5 * 60

    def test_tsr_default_sched_tardy_with_deduction(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. TSR
        4. 15 minutes tardy
        5. 15 minutes to deduct
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_tardy_with_deduction")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("ds_72").get(constants.ATT_TARDY) == 15
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 7.75 * 60

    def test_tsr_default_sched_tardy_not_included_in_tardy_settings(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. TSR
        4. 15 minutes tardy
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_tardy_not_included_in_settings")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_UNDERTIME) == 30
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 7.5 * 60

    def test_tsnr_default_sched_rdrh_with_ot(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Regular Holiday
        3. TSR
        4. Entitled to O.T. - 30 minutes
        5. Default schedule Rest Day
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsnr_with_rd_rh_with_ot")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_RD_RH) == 9 * 60
        assert attendance.get("ds_72").get(constants.ATT_RD_OT) == 0.5 * 60

    def test_tsnr_default_sched_rdsh(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Special Holiday
        3. TSNR
        4. User defined rest Day
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsnr_with_rd_sh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_RD_SH) == 9 * 60

    def test_rd_date_hired(self):
        """
        Test default shift with defined regular holiday
        1. Employee date hired is less than the attendance date
        """
        data = utils.get_mock("scheduled_hours_default_shift_rd_date_hired")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance == {}

    def test_rd_date_ended(self):
        """
        Test default shift with defined regular holiday
        1. Employee date hired is less than the attendance date
        """
        data = utils.get_mock("scheduled_hours_default_shift_rd_date_ended")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance == {}

    def test_tsr_with_half_day_leave(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Unworked Holiday
        3. TSR
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_with_half_day_leave")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_72").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 4 * 60

    def test_half_day_leave_no_ts(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Approved half day leave
        3. TSR
        4. NO rendered hours
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)
        mock.hours_worked([
            {
                "shift_id": "ds_1",
                "type": "paid_leave",
                "type_name": "Paid Leave",
                "time": "04:00",
                "start_datetime": "2019-01-01 13:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("ds_1").get(constants.ATT_UNDERTIME) == 4 * 60

    def test_half_overbreak(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. Approved half day leave
        3. TSR
        4. NO rendered hours
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True)

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 12:30:00", "clock_out"),
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 14:30:00", "clock_out"),
            ("2019-01-01 15:30:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out"),
        ])

        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 6 * 60
        assert attendance.get("ds_1").get(constants.ATT_OVERBREAK) == 2 * 60

    def test_ordered_shift_error(self):
        """
            Handle ordered shits initialization on default schedule
            1. Default schedule 8-5pm
            2. Tardy rule of 30 min grace period w/ 1 min deduction
            3. Has clock pair at:
                - in 2020-01-01 08:47:48 AM to
                - out 2020-01-01 12:39:26 AM

            https://code.salarium.com/salarium/production-operations/issues/360
            https://code.salarium.com/salarium/development/t-and-a-manila/issues/4002
        """

        mock_data = {
            'employee_uid': 1,
            'date': '2020-01-30',
            'job_id': '747b7395-e216-4102-9861-aeb64da5e6de',
            'id': 1,
            'full_name': 'EMPLOYEE TEST',
            'company_id': 1,
            'employee_id': 'HMC-0004',
            'rank_id': 1,
            'rank_name': 'MANAGER',
            'position_id': 1,
            'position_name': 'POSITION',
            'department_id': 1,
            'department_name': 'FINANCE',
            'location_id': 1,
            'location_name': 'BAHAY',
            'hours_per_day': None,
            'active': 'active',
            'date_hired': '2019-07-11',
            'date_ended': None,
            'time_attendance_id': 1,
            'team_id': 1,
            'team_name': 'TEAM',
            'team_role': 'Leader',
            'primary_location_id': 1,
            'primary_location_name': 'BAHAY',
            'secondary_location_id': 1,
            'secondary_location_name': 'WORK',
            'timesheet_required': False,
            'overtime': False,
            'differential': False,
            'regular_holiday_pay': True,
            'special_holiday_pay': True,
            'holiday_premium_pay': False,
            'rest_day_pay': False,
            'default_shifts': {
                'data': [
                    {
                        'id': 1,
                        'company_id': 1,
                        'day_of_week': 1,
                        'day_type': 'regular',
                        'work_start': '08:00:00',
                        'work_break_start': '12:00:00',
                        'work_break_end': '13:00:00',
                        'work_end': '17:00:00',
                        'total_hours': '09:00'
                    },
                    {
                        'id': 2,
                        'company_id': 1,
                        'day_of_week': 2,
                        'day_type': 'regular',
                        'work_start': '08:00:00',
                        'work_break_start': '12:00:00',
                        'work_break_end': '13:00:00',
                        'work_end': '17:00:00',
                        'total_hours': '09:00'
                    },
                    {
                        'id': 3,
                        'company_id': 1,
                        'day_of_week': 3,
                        'day_type': 'regular',
                        'work_start': '08:00:00',
                        'work_break_start': '12:00:00',
                        'work_break_end': '13:00:00',
                        'work_end': '17:00:00',
                        'total_hours': '09:00'
                    },
                    {
                        'id': 4,
                        'company_id': 1,
                        'day_of_week': 4,
                        'day_type': 'regular',
                        'work_start': '08:00:00',
                        'work_break_start': '12:00:00',
                        'work_break_end': '13:00:00',
                        'work_end': '17:00:00',
                        'total_hours': '09:00'
                    },
                    {
                        'id': 5,
                        'company_id': 1,
                        'day_of_week': 5,
                        'day_type': 'regular',
                        'work_start': '08:00:00',
                        'work_break_start': '12:00:00',
                        'work_break_end': '13:00:00',
                        'work_end': '17:00:00',
                        'total_hours': '09:00'
                    },
                    {
                        'id': 6,
                        'company_id': 1,
                        'day_of_week': 6,
                        'day_type': 'rest_day',
                        'work_start': '08:00:00',
                        'work_break_start': '12:00:00',
                        'work_break_end': '13:00:00',
                        'work_end': '17:00:00',
                        'total_hours': '09:00'
                    },
                    {
                        'id': 7,
                        'company_id': 1,
                        'day_of_week': 7,
                        'day_type': 'rest_day',
                        'work_start': '08:00:00',
                        'work_break_start': '12:00:00',
                        'work_break_end': '13:00:00',
                        'work_end': '17:00:00',
                        'total_hours': '09:00'
                    }
                ]
            },
            'shifts': [],
            'default_shift': {
                'id': 4,
                'company_id': 1,
                'day_of_week': 4,
                'day_type': 'regular',
                'work_start': '08:00:00',
                'work_break_start': '12:00:00',
                'work_break_end': '13:00:00',
                'work_end': '17:00:00',
                'total_hours': '09:00'
            },
            'night_shift_settings': {
                'id': 1,
                'company_id': 1,
                'activated': True,
                'start': '22:00',
                'end': '06:00',
                'time_shift': 'Carry over'
            },
            'hours_worked': [],
            'raw_timesheet': [
                {
                    'employee_uid': 1.0,
                    'company_uid': '1',
                    'tags': [],
                    'state': True,
                    'timestamp': 1580345268.0
                },
                {
                    'employee_uid': 1.0,
                    'company_uid': '1',
                    'tags': [],
                    'state': False,
                    'timestamp': 1580359166.0
                }
            ],
            'timesheet': [
                {
                    'employee_uid': 1.0,
                    'company_uid': '1',
                    'tags': [],
                    'state': True,
                    'timestamp': 1580345268.0
                },
                {
                    'employee_uid': 1.0,
                    'company_uid': '1',
                    'tags': [],
                    'state': False,
                    'timestamp': 1580359166.0
                }
            ],
            'tardiness_rule': {
                'id': 1,
                'name': '30-Minute Grace Period',
                'company_id': 1,
                'minutes_tardy': 30,
                'minutes_to_deduct': 1,
                'affected_employees': [
                    {
                        'id': 2,
                        'name': 'EMP TEST - TEST-001',
                        'type': 'employee'
                    },
                    {
                        'id': 1,
                        'name': 'EMP TEST - TEST-000',
                        'type': 'employee'
                    }
                ],
                'updated_at': '2020-01-07 17:56'
            },
            'shift_dates': {
                '2020-01-30': {
                    'rest_day': False,
                    'holiday_type': None
                }
            }
        }

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_4").get(constants.ATT_REGULAR) == 8 * 60

    def test_tsr_default_sched_tardy_with_deduction_scenario_4(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. TSR
        4. 30 minutes tardy
        5. 30 minutes to deduct
        6. 8.31a clock in
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_tardy_with_extended_deduction")
        data['tardiness_rule']['minutes_tardy'] = 30
        data['tardiness_rule']['minutes_to_deduct'] = 30
        data['timesheet'][0]['timestamp'] = 1540773060

        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("ds_72").get(constants.ATT_TARDY) == 1
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 479

    def test_tsr_default_sched_tardy_with_deduction_scenario_3(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. TSR
        4. 15 minutes tardy
        5. 15 minutes to deduct
        6. 8.31a clock in
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_tardy_with_extended_deduction")
        data['tardiness_rule']['minutes_tardy'] = 15
        data['tardiness_rule']['minutes_to_deduct'] = 15
        data['timesheet'][0]['timestamp'] = 1540773060
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("ds_72").get(constants.ATT_TARDY) == 16
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 464

    def test_tsr_default_sched_tardy_with_deduction_scenario_2(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. TSR
        4. 15 minutes tardy
        5. 15 minutes to deduct
        6. 8.31a clock in
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_tardy_with_extended_deduction")
        data['tardiness_rule']['minutes_tardy'] = 15
        data['tardiness_rule']['minutes_to_deduct'] = 15
        data['timesheet'][0]['timestamp'] = 1540773060

        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("ds_72").get(constants.ATT_TARDY) == 16
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 464

    def test_tsr_default_sched_tardy_with_deduction_scenario_1(self):
        """
        Test default shift with defined regular holiday
        1. Default Shift
        2. TSR
        4. 60 minutes tardy
        5. 60 minutes to deduct
        6. 9.31a clock in
        """
        data = utils.get_mock("scheduled_hours_default_shift_tsr_tardy_with_extended_deduction")
        data['tardiness_rule']['minutes_tardy'] = 60
        data['tardiness_rule']['minutes_to_deduct'] = 60
        data['timesheet'][0]['timestamp'] = 1540776660

        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("ds_72").get(constants.ATT_TARDY) == 31
        assert attendance.get("ds_72").get(constants.ATT_REGULAR) == 449

    def test_ds_with_ot_until_next_day(self):
        mock_data = {
            'date': '2020-02-13',
            'night_shift_settings': {
                'id': 1,
                'company_id': 1,
                'activated': True,
                'start': '22:00',
                'end': '06:00',
                'time_shift': 'Carry over'
            },
            'tardiness_rule': {
                'id': 1,
                'name': 'Grace Period',
                'company_id': 1,
                'minutes_tardy': 15,
                'minutes_to_deduct': 15,
                'affected_employees': [
                    {
                        'id': 1,
                        'name': 'Employee - 1',
                        'type': 'employee'
                    },
                    {
                        'id': 1,
                        'name': 'Makati',
                        'type': 'location'
                    }
                ],
                'updated_at': '2020-02-28 12:43'
            },
            'id': 1,
            'user_id': 1,
            'status': 'active',
            'employee_id': '1',
            'company_id': 1,
            'first_name': 'EMPLOYEE',
            'middle_name': 'OT',
            'last_name': 'TEST',
            'full_name': 'EMPLOYEE OT TEST',
            'department_id': 1,
            'department_name': 'Operations',
            'position_id': 1,
            'position_name': 'Service Engineer',
            'location_id': 1,
            'location_name': 'Makati',
            'hours_per_day': None,
            'date_hired': '2018-06-01',
            'date_ended': None,
            'employment_type_id': 1,
            'employment_type_name': 'Regular',
            'timesheet_required': True,
            'overtime': True,
            'differential': True,
            'regular_holiday_pay': True,
            'special_holiday_pay': True,
            'holiday_premium_pay': True,
            'rest_day_pay': True,
            'employee_uid': 1,
            'rest_days': [],
            'shifts': [],
            'default_shift': {
                'id': 1,
                'company_id': 1,
                'day_of_week': 4,
                'day_type': 'regular',
                'work_start': '08:00:00',
                'work_break_start': '12:00:00',
                'work_break_end': '13:00:00',
                'work_end': '17:00:00',
                'total_hours': '09:00'
            },
            'hours_worked': [{
                'id': 1,
                'employee_id': 1,
                'date': '2020-02-13',
                'type': 'overtime',
                'type_name': 'Overtime',
                'time': '19:00',
                'value': '19.00',
                'shift_id': 'ds_1',
                'leave_type_id': None,
                'start_datetime': '2020-02-13 17:00',
                'end_datetime': '2020-02-14 12:00'
            }],
            'tardiness_rules': [],
            'shift_dates': {
                '2020-02-13': {
                    'rest_day': False,
                    'holiday_type': None
                }
            },
            'timesheet': [{
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1581551340.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1581599520.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'tags': [],
                'state': True,
                'timestamp': 1581638400.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1581677760.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'tags': [],
                'state': True,
                'timestamp': 1581724800.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1581762420.0
            }],
            'raw_timesheet': [{
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': True,
                'timestamp': 1581551340.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'state': False,
                'timestamp': 1581599520.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'tags': [],
                'state': True,
                'timestamp': 1581638400.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1581677760.0
            }, {
                'employee_uid': 5055.0,
                'company_uid': '76',
                'tags': [],
                'state': True,
                'timestamp': 1581724800.0
            }, {
                'employee_uid': 1.0,
                'company_uid': '1',
                'tags': [],
                'state': False,
                'timestamp': 1581762420.0
            }]
        }

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 480
        assert attendance.get("ds_1").get(constants.ATT_OT) == 492

    def test_default_shift_tsr_regular(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.timesheet([
            ("2020-03-11 08:00:00", "clock_in"),
            ("2020-03-11 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_default_shift_tsr_regular_no_timesheet(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. No Timesheet
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert not attendance.get("ds_1").get(constants.ATT_REGULAR)

    def test_default_shift_tsnr_regular(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet not required
        4. Regular
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=False
        )
        mock.default_shift()
        mock.timesheet([
            ("2020-03-11 08:00:00", "clock_in"),
            ("2020-03-11 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_default_shift_tsnr_regular_no_timesheet(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet not required
        4. Regular
        5. No Timesheet
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=False
        )
        mock.default_shift()
        mock.timesheet([])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_default_shift_tsr_regular_tardiness_rule(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=15
        )
        mock.timesheet([
            ("2020-03-11 08:15:00", "clock_in"),
            ("2020-03-11 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_default_shift_tsr_regular_tardiness_rule_scenario1(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=15
        )
        mock.timesheet([
            ("2020-03-11 08:20:00", "clock_in"),
            ("2020-03-11 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == (7 * 60) + 55
        assert attendance.get("ds_1").get(constants.ATT_TARDY) == 5

    def test_default_shift_tsr_regular_tardiness_rule_unpaid_break_scenario2(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Schedule
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=15
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_TARDY) == 1

    def test_default_shift_tsr_regular_tardiness_rule_unpaid_break_scenario3a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_TARDY) == 16

    def test_default_shift_tsr_regular_tardiness_rule_unpaid_break_scenario3b(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:13:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert not attendance.get("ds_1").get(constants.ATT_TARDY)

    def test_default_shift_tsr_regular_tardiness_rule_unpaid_break_scenario4(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=15
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_TARDY) == 1

    def test_default_shift_tsr_regular_tardiness_rule_unpaid_break_scenario5a(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:16:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_TARDY) == 16

    def test_default_shift_tsr_regular_tardiness_rule_unpaid_break_scenario5b(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Unpaid Break
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.tardiness_rule(
            minutes_tardy=0,
            minutes_to_deduct=0
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2020-03-11 08:10:00", "clock_in"),
            ("2020-03-11 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_TARDY) == 10

    def test_default_shift_tsr_regular_overbreak(self):
        """Test specs:

        Issue: https://code.salarium.com/salarium/development/t-and-a-manila/issues/4227

        1. Default Sched
        2. With no core hours
        3. Timesheet required
        4. Regular
        5. Tardiness Rule
        6. Overbreak
        """
        mock = utils.MockHero(
            date="2020-03-11",
            timesheet_required=True
        )
        mock.default_shift()
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.tardiness_rule(
            minutes_tardy=15,
            minutes_to_deduct=15
        )
        mock.timesheet([
            ("2020-03-11 08:00:00", "clock_in"),
            ("2020-03-11 12:00:00", "clock_out"),
            ("2020-03-11 13:30:00", "clock_in"),
            ("2020-03-11 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == (7 * 60) + 30
        assert attendance.get("ds_1").get(constants.ATT_OVERBREAK) == 30
