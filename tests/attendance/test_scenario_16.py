# pylint: disable=W0212
"""Calculator unit tests"""
import unittest
import os
import time
from attendance import calculator, constants
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()


class TestScenario16(unittest.TestCase):
    """Default Calculator tests

    Fixed Schedule with 1 Fixed Break and with one 1 Floating break (Day shift)
    """
    def test_regular(self):
        """Test a flexi schedule without core hours"""
        data = utils.get_mock("scenario_16d_flexi_without_core_hour_day_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60


    def test_regular_has_approved_undertime(self):
        """Test a flexi schedule without core hours has approved undertime

        Note: Should not count undertime
        """
        data = utils.get_mock("scenario_16d_flexi_without_core_hour_day_shift_has_approved_undertime")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2.5 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 5.5 * 60


    def test_regular_has_ot(self):
        """Test a flexi schedule without core hours has OT
        """
        data = utils.get_mock("scenario_16d_flexi_without_core_hour_day_shift_has_ot")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 1.5 * 60


    def test_regular_has_paid_leave(self):
        """Test a flexi schedule without core hours has paid leave
        """
        data = utils.get_mock("scenario_16d_flexi_without_core_hour_day_shift_has_paid_leave")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 14 * 60


    def test_regular_has_unpaid_leave(self):
        """Test a flexi schedule without core hours has paid leave
        """
        data = utils.get_mock("scenario_16d_flexi_without_core_hour_day_shift_has_unpaid_leave")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 14 * 60


    # 16C
    def test_regular_has_ot_no_entitlement(self):
        """Test a flexi schedule without core hours has OT
        """
        data = utils.get_mock("scenario_16d_flexi_without_core_hour_day_shift_has_no_ot_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 13 * 60


    def test_has_unworked_holiday(self):
        """Test a flexi schedule without core hours has unworked holiday
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_unworked_holiday")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 14 * 60


    def test_has_unworked_holiday_wtih_ts_scenario_1_schedule_on_holidays_false(self):
        """Test a flexi schedule without core hours has unworked holiday
            and shift has on_holidays false
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_unworked_holiday_with_ts")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 6 * 60
    
    def test_has_unworked_holiday_wtih_ts_scenario_1_schedule_on_holidays_true(self):
        """Test a flexi schedule without core hours has unworked holiday
            and shift has on_holidays true
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_unworked_holiday_with_ts")
        data['shifts'][0]['on_holidays'] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
        assert attendance.get("1").get(constants.ATT_UH) == 6 * 60
    
    def test_has_unworked_holiday_wtih_ts_scenario_2(self):
        """Test a flexi schedule without core hours has unworked holiday
            and has on_holidays true
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_unworked_holiday_with_ts")
        data['shifts'][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 6 * 60
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_has_unworked_holiday_no_entitlement(self):
        """Test a flexi schedule without core hours has unworked holiday
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_unworked_holiday_no_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 14 * 60

    def test_has_premium_holiday_rh_scenario_1_schedule_on_holidays_false(self):
        """Test a flexi schedule without core hours has premium holiday RH
        shift has on_holidays false
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 14 * 60
    
    def test_has_premium_holiday_rh_scenario_2(self):
        """Test a flexi schedule without core hours has premium holiday RH
        shift has on_holidays true
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday")
        data["shifts"][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 14 * 60

    def test_has_premium_holiday_sh_with_ts_scenario_1_schedule_on_holidays_false(self):
        """Test a flexi schedule without core hours has premium holiday SH
            and shift has on_holidays false
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_sh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 14 * 60
    
    def test_has_premium_holiday_sh_with_ts_scenario_1_schedule_on_holidays_true(self):
        """Test a flexi schedule without core hours has premium holiday SH
            and shift has on_holidays true
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_sh")
        data['shifts'][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 14 * 60
    
    def test_has_premium_holiday_sh_with_ts_scenario_2(self):
        """Test a flexi schedule without core hours has premium holiday SH
            and shift has on_holidays false
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_sh")
        data["shifts"][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 14 * 60
    
    def test_has_premium_holiday_sh_without_ts(self):
        """Test a flexi schedule without core hours has premium holiday SH
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_sh")
        data['timesheet'] = []
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 14 * 60

    def test_has_premium_holiday_sh_has_approved_ot_scenario_1_schedule_on_holidays_false(self):
        """Test a flexi schedule without core hours has premium holiday SH
         and shift has on_holidays false
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_sh_with_approved_ot")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 324
        assert attendance.get("1").get(constants.ATT_UH) == 156
    
    def test_has_premium_holiday_sh_has_approved_ot_scenario_2(self):
        """Test a flexi schedule without core hours has premium holiday SH
        and shift has on_holidays true
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_sh_with_approved_ot")
        data["shifts"][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 324
        assert attendance.get("1").get(constants.ATT_UH) == 156

    def test_has_premium_holiday_rh_sh_with_ts_scenario_1_schedule_on_holidays_false(self):
        """Test a flexi schedule without core hours has premium holiday SH
        and shift has on_holidays false
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_rh_sh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 14 * 60
    
    def test_has_premium_holiday_rh_sh_with_ts_scenario_2(self):
        """Test a flexi schedule without core hours has premium holiday SH
        and shift has on_holidays true
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_rh_sh")
        data["shifts"][0]["on_holidays"] = True
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 14 * 60
    
    def test_has_premium_holiday_rh_sh_without_ts(self):
        """Test a flexi schedule without core hours has premium holiday SH
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_rh_sh")
        data['timesheet'] = []
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 14 * 60

    def test_has_premium_holiday_no_entitlement(self):
        """Test a flexi schedule without core hours has no premium holiday entitlement
        """
        data = utils.get_mock("scenario_16c_flexi_without_core_hour_day_shift_has_premium_holiday_no_entitlement")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 14 * 60


    ### Unit tests for 16d - Flexi with core hours
    def test_regular_flexi_ot_with_core_hours(self):
        """Flexi specifications
        1. Flexi hours day shift
        2. With core hours
        3. Wtih timesheet and timesheet required
        4. With overtime
        """
        data = utils.get_mock("scenario_16d_flexi_with_core_hours_day_shift_has_ot")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_OT) == 2 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_regular_flexi_ot_with_core_hours_rest_day(self):
        """Flexi specifications
        1. Flexi hours rest day
        2. With core hours
        3. Wtih timesheet and timesheet required
        4. With overtime
        """
        data = utils.get_mock("scenario_16d_flexi_with_core_hours_day_shift_has_ot_rest_day")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_77").get(constants.ATT_RD) == 2 * 60

    def test_regular_flexi_ot_with_paid_leave(self):
        """Flexi specifications
        1. Flexi hours day shift
        2. With core hours
        3. Wtih timesheet and timesheet required
        4. With paid leave
        """
        data = utils.get_mock("scenario_16d_flexi_with_core_hours_day_shift_has_paid_leave")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60

    def test_regular_flexi_undertime_paid_leave_no_timesheet(self):
        """Flexi specifications
        1. Flexi hours day shift
        2. With core hours
        3. Wtih no timesheet but timesheet required
        4. With paid leave
        """
        data = utils.get_mock("scenario_16d_flexi_with_core_hours_day_shift_has_paid_leave_no_timesheet")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 4 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60

    def test_regular_flexi_undertime_unpaid_leave_no_timesheet(self):
        """Flexi specifications
        1. Flexi hours day shift
        2. With core hours
        3. Wtih no timesheet but timesheet required
        4. With unpaid leave
        """
        data = utils.get_mock("scenario_16d_flexi_without_core_hour_day_shift_has_unpaid_leave_no_timesheet")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 4 * 60

    def test_regular_flexi_ot_with_core_hours_rest_day_no_timesheet_and_timehsheet_not_required(self):
        """Flexi specifications
        1. Flexi hours rest day
        2. With core hours
        3. With no timesheet and timesheet not required
        4. With overtime
        """
        data = utils.get_mock("scenario_16d_flexi_with_core_hours_day_shift_has_ot_rest_day_timesheet_not_required")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_3108").get(constants.ATT_RD) == 2 * 60

    def test_regular_flexi_with_paid_leave_equal_to_schedule_hours(self):
        """Flexi specifications
        1. Flexi hours day shift
        2. With core hours
        3. Wtih no timesheet and not timesheet required
        4. With paid leave
        5. Filed leave is equal to scheduled hours which is 14 but with required 8 hours
        """
        data = utils.get_mock("scenario_16d_flexi_with_core_hours_day_shift_has_paid_leave_equal_to_schedule")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 8 * 60

    def test_flexi_with_core_hours_nt_ot(self):
        """Flexi specifications
        1. Flexi hours day shift
        2. With core hours
        3. Wtih timesheet and timesheet required
        4. With overtime
        """
        data = utils.get_mock("flexi_core_hours_nt_ot_carry_over_timesheet_required")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 6 * 60
        assert attendance.get("1").get(constants.ATT_NT_OT) == 2 * 60
