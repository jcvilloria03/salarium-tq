# pylint: disable=W0212,C0301
"""Calculator unit tests"""
import unittest
from unittest import mock
import os
import time
from attendance import calculator, constants
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()

# namespace for is_rest_day method from DefaultCalculator
mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"

class TestCalculatorSpecialTests(unittest.TestCase):
    """Default Calculator tests"""

    def test_8215_should_be_absent(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_8215_should_be_absent")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("619")
        assert attendance.get("619").get(constants.ATT_ABSENT)
        assert attendance.get("619").get(constants.ATT_ABSENT) == 9 * 60

    def test_issue_172(self):
        """ISsue 172 test"""
        data = utils.get_mock("scenario_issue_172")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("739")
        assert calc.get_shift_timesheet("739")
        assert len(calc.get_shift_timesheet("739")) == 2

    def test_issue_175(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_issue_175")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("864")
        assert attendance.get("864").get(constants.ATT_UH)
        assert attendance.get("864").get(constants.ATT_UH) == 8 * 60

    def test_issue_176(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_issue_176")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("865")
        assert attendance.get("865").get(constants.ATT_ABSENT)
        assert attendance.get("865").get(constants.ATT_ABSENT) == 8 * 60

    def test_issue_177(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_issue_177")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT) == 4 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 3 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 2 * 60

    def test_issue_178(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_issue_178")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("739")
        assert attendance.get("739").get(constants.ATT_REGULAR)
        assert attendance.get("739").get(constants.ATT_REGULAR) == 8 * 60

    def test_issue_184(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_issue_184")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        #assert attendance.get("1").get(constants.ATT_PAID_LEAVE)
        #assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 9 * 60
        # data indicates UH situation
        assert len(attendance.get("1")) == 1

    @mock.patch(mock_is_restday, return_value=False)
    def test_scenario_glads_reg_hours_after_nt(self, restd):
        """Test shift info only"""
        data = utils.get_mock("scenario_glads_reg_hours_after_nt")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1051").get(constants.ATT_RH_NT) == 2 * 60
        assert attendance.get("1051").get(constants.ATT_NT) == 6 * 60
        assert attendance.get("1051").get(constants.ATT_RH) == 1 * 60

    def test_scenario_issue_237(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_issue_237")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1240")
        assert attendance.get("1240").get(constants.ATT_REGULAR) == 480

    def test_scenario_issue_251(self):
        """Test issue 251"""
        data = utils.get_mock("scenario_issue_251")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1137")
        assert attendance.get("1137").get(constants.ATT_REGULAR) == 480

    def test_scenario_issue_252(self):
        """Test issue 252"""
        data = utils.get_mock("scenario_issue_252")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1406")
        assert attendance.get("1406").get(constants.ATT_REGULAR) == 60
        assert attendance.get("1406").get(constants.ATT_NT) == 420

    def test_scenario_issue_254(self):
        """Test issue 254"""
        data = utils.get_mock("scenario_issue_254")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1315")
        assert attendance.get("1315").get(constants.ATT_REGULAR) == 360
        assert attendance.get("1315").get(constants.ATT_NT) == 120

    def test_scenario_issue_272(self):
        """Test issue 272"""
        data = utils.get_mock("scenario_issue_272")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1427")
        assert attendance.get("1427").get("RH+NT") == 420
        assert attendance.get("1427").get("RH") == 60

    def test_scenario_night_time_default_shift(self):
        """Test issue 272"""
        data = utils.get_mock("scenario_night_time_default_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_865")
        assert attendance.get("ds_865").get("ABSENT") == 480

    def test_scenario_hours_worked_override_items(self):
        data = utils.get_mock("scenario_hours_worked_override_items")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1390").get("REGULAR") == 240
        assert attendance.get("1390").get("NT") == 240
        assert len(attendance.get("1390")) == 2

    def test_scenario_issue_449(self):
        data = utils.get_mock("scenario_issue_449")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_45").get("PAID LEAVE") == 480

    def test_scenario_issue_471(self):
        data = utils.get_mock("scenario_issue_471")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("28").get("UNPAID LEAVE") == 240
        assert attendance.get("28").get("UNDERTIME") == 240

    def test_scenario_issue_450(self):
        data = utils.get_mock("scenario_issue_450")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("55").get("NT") == 420
        assert attendance.get("55").get(constants.ATT_UNDERTIME) == 60

    def test_scenario_bug_nonstring_dkeys(self):
        data = utils.get_mock("scenario_bug_nonstring_dkeys")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1844").get("REGULAR") == 420

    @mock.patch(mock_is_restday, return_value=False)
    def test_scenario_issue_614(self, restd):
        """
        Conditions:
        > Timesheet not Required
        > No clockin/clockout
        > Flexi Shift without core hours
        > Shift 22:00 to 07:00
        > Employee Entitled to Night Diff
        > Night Settings 22:00 to 06:00 Counter per hour
        > Approved Overtime Next Day 07:00 to 09:00 2 hours
        """
        data = utils.get_mock("scenario_issue_614")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert len(attendance.get("1922")) == 2
        # assert attendance.get("1922").get(constants.ATT_SH_NT) == 360
        assert attendance.get("1922").get(constants.ATT_NT) == 8 * 60
        assert attendance.get("1922").get(constants.ATT_OT) == 2 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_scenario_issue_615(self, restd):
        """
        Conditions:
        > Timesheet not Required
        > No clockin/clockout
        > Flexi Shift without core hours
        > Shift 22:00 to 07:00
        > Employee Entitled to Night Diff
        > Night Settings 22:00 to 06:00 Counter per hour
        > Approved Undertime 06:00 to 07:00 1 hour
        """
        data = utils.get_mock("scenario_issue_615")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1922").get(constants.ATT_NT) == 420
        assert attendance.get("1922").get(constants.ATT_UNDERTIME) == 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_scenario_issue_617(self, restd):
        """
        Conditions:
        > Timesheet not Required
        > No clockin/clockout
        > Flexi Shift without core hours
        > Shift 22:00 to 07:00
        > Employee Entitled to Night Diff
        > Night Settings 22:00 to 06:00 Counter per hour
        > Approved Paid Leave 22:00 to 02:00 4 hours
        """
        data = utils.get_mock("scenario_issue_617")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1922").get(constants.ATT_NT) == 240
        assert attendance.get("1922").get(constants.ATT_PAID_LEAVE) == 240
