# pylint: disable=W0212,C0301
"""Calculator unit tests"""
import unittest
import os
import time
from attendance import calculator, constants, keys
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()

class TestDefaultCalculator(unittest.TestCase):
    """Flexi night shift raw timesheets"""
    def test_night_shift_flexi_raw_timesheet(self):
        """Test raw timesheet on night shift"""
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.night_shift_settings()
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "22:00",
                "end_time": "06:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([(
            "2019-01-02",
            False,
            None
        )])

        timesheets = [
            ("2019-01-01 22:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        raw_timesheet = calc.get_shift_raw_timesheet("1", "flexi")
        assert len(raw_timesheet) == 2

    """Fixed day shift raw timesheets"""
    def test_day_shift_flexi_raw_timesheet_next_day_timestamps(self):
        """Test raw timesheet on night shift"""
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.night_shift_settings()
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "03:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([(
            "2019-01-01",
            False,
            None
        )])

        timesheets = [
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
            ("2019-01-02 1:00:00", "clock_in"),
            ("2019-01-02 3:00:00", "clock_out"),
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        raw_timesheet = calc.get_shift_raw_timesheet("1", "flexi")
        assert len(raw_timesheet) == 4


    """Flexi day shift raw timesheets"""
    def test_day_shift_flexi_raw_timesheet_next_day_timestamps_odd_timestamps(self):
        """Test raw timesheet on night shift"""
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.night_shift_settings()
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "03:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([(
            "2019-01-01",
            False,
            None
        )])

        timesheets = [
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
            ("2019-01-02 1:00:00", "clock_in")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        raw_timesheet = calc.get_shift_raw_timesheet("1", "flexi")
        assert len(raw_timesheet) == 3

    """Fixed night shift raw timesheets"""
    def test_night_shift_fixed_raw_timesheet(self):
        """Test raw timesheet on night shift"""
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.night_shift_settings()
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "22:00",
                "end_time": "06:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([(
            "2019-01-02",
            False,
            None
        )])

        timesheets = [
            ("2019-01-01 22:00:00", "clock_in"),
            ("2019-01-02 06:00:00", "clock_out")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        raw_timesheet = calc.get_shift_raw_timesheet("1", "fixed")
        assert len(raw_timesheet) == 2

    """Fixed day shift raw timesheets"""
    def test_day_shift_fixed_raw_timesheet_next_day_timestamps(self):
        """Test raw timesheet on night shift"""
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.night_shift_settings()
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([(
            "2019-01-01",
            False,
            None
        )])

        timesheets = [
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
            ("2019-01-02 1:00:00", "clock_in"),
            ("2019-01-02 3:00:00", "clock_out"),
            ("2019-01-02 08:00:00", "clock_in"),
            ("2019-01-02 17:00:00", "clock_out"),
            ("2019-01-03 08:00:00", "clock_in"),
            ("2019-01-03 17:00:00", "clock_out"),
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        raw_timesheet = calc.get_shift_raw_timesheet("1", "fixed")
        assert len(raw_timesheet) == 4

    """Fixed day shift raw timesheets"""
    def test_day_shift_fixed_raw_timesheet_next_day_timestamps_odd_timestamps(self):
        """Test raw timesheet on night shift"""
        mock = utils.MockHero(date="2019-01-01", timesheet_required=True)
        mock.night_shift_settings()
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": True
            }
        ])
        mock.set_shift_dates([(
            "2019-01-01",
            False,
            None
        )])

        timesheets = [
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out"),
            ("2019-01-02 01:00:00", "clock_in"),
            ("2019-01-02 07:00:00", "clock_out")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        raw_timesheet = calc.get_shift_raw_timesheet("1", "fixed")
        assert len(raw_timesheet) == 3

    def test_flexi_clockin_early_than_schedule(self):
        """
        Conditions:
          > Shift: 08:00 to 20:00
          > Clock-in: 07:00
          > Clock-out: 17:00
        """
        shift_id = "1"
        shift_date="2019-01-01"
        shift_type = keys.SH_FLEXI

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings()

        mock.shifts([
            {
                keys.TYPE: shift_type,
                keys.START_TIME: "08:00",
                keys.END_TIME: "20:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        timesheets = [
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet(shift_id, shift_type)
        assert len(raw_timesheet) == 2

    def test_flexi_clockout_beyond_schedule(self):
        """
        Conditions:
          > Shift: 08:00 to 20:00
          > Clock-in: 08:00
          > Clock-out: 21:00
        """
        shift_id = "1"
        shift_date="2019-01-01"
        shift_type = keys.SH_FLEXI

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings()

        mock.shifts([
            {
                keys.TYPE: shift_type,
                keys.START_TIME: "08:00",
                keys.END_TIME: "20:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        timesheets = [
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 21:00:00", "clock_out")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet(shift_id, shift_type)
        assert len(raw_timesheet) == 1

    def test_flexi_time_records_outside_schedule(self):
        """
        Conditions:
          > Shift: 08:00 to 20:00
          > Clock-in: 07:00
          > Clock-out: 21:00
        """
        shift_id = "1"
        shift_date="2019-01-01"
        shift_type = keys.SH_FLEXI

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings()

        mock.shifts([
            {
                keys.TYPE: shift_type,
                keys.START_TIME: "08:00",
                keys.END_TIME: "20:00",
                keys.TOTAL_HOURS: "08:00",
            }
        ])

        timesheets = [
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 21:00:00", "clock_out")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet(shift_id, shift_type)
        assert len(raw_timesheet) == 1

    def test_fixed_multi_shifts(self):
        """
        Conditions:
          > 1st Shift: 08:00 to 12:00
          > 2nd Shift: 13:00 to 17:00
          > Clock-in: 07:00
          > Clock-out: 21:00
        """
        shift_id = "1"
        shift_date="2019-01-01"
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True)

        mock.default_shift()
        mock.night_shift_settings()

        mock.shifts([
            {
                keys.TYPE: shift_type,
                keys.START_TIME: "08:00",
                keys.END_TIME: "12:00",
                keys.TOTAL_HOURS: "04:00",
            },
            {
                keys.TYPE: shift_type,
                keys.START_TIME: "13:00",
                keys.END_TIME: "17:00",
                keys.TOTAL_HOURS: "04:00",
            }
        ])

        timesheets = [
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 10:00:00", "clock_out"),
            ("2019-01-01 10:05:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 12:30:00", "clock_in"),
            ("2019-01-01 15:00:00", "clock_out"),
            ("2019-01-01 15:30:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")
        ]
        mock.raw_timesheet(timesheets)

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)

        raw_timesheet_1 = calc.get_shift_raw_timesheet("1", shift_type)
        raw_timesheet_2 = calc.get_shift_raw_timesheet("2", shift_type)

        assert len(raw_timesheet_1) == 5
        assert len(raw_timesheet_2) == 3

    def test_user_defined_rd_no_clock_out(self):
        """
        Conditions:
           User defined rest day
           No Approved OT
           Clock-in: 08:00, 09:00 
           Clock-out: None
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift()
        mock.user_defined_rest_days()

        mock.raw_timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_in")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 2

    def test_user_defined_rd_no_clock_in(self):
        """
        Conditions:
           User defined rest day
           No Approved OT
           Clock-in: None
           Clock-out: 17:00, 18:00
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift()
        mock.user_defined_rest_days()

        mock.raw_timesheet([
            ("2019-01-01 17:00:00", "clock_out"),
            ("2019-01-01 18:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 2

    def test_user_defined_rd_no_ts(self):
        """
        Conditions:
           User defined rest day
           No Approved OT
           Clock-in: None
           Clock-out: None
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift()
        mock.user_defined_rest_days()

        mock.raw_timesheet([])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 0

    def test_user_defined_rd_clock_out_next_day(self):
        """
        Conditions:
           User defined rest day
           No Approved OT
           Clock-in: 23:00
           Clock-out: 07:00, 10:00
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift()
        mock.user_defined_rest_days()

        mock.raw_timesheet([
            ("2019-01-01 23:00:00", "clock_in"),
            ("2019-01-02 07:00:00", "clock_out"),
            ("2019-01-02 10:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 2

    def test_user_defined_rd_next_day_ts_is_clock_in(self):
        """
        Conditions:
           User defined rest day
           No Approved OT
           Clock-in: 23:00, 05:00(Next day)
           Clock-out: 07:00 (Next day)
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift()
        mock.user_defined_rest_days()

        mock.raw_timesheet([
            ("2019-01-01 23:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_in"),
            ("2019-01-02 07:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 1

    def test_default_rd_no_clock_out(self):
        """
        Conditions:
           Default rest day
           No Approved OT
           Clock-in: 08:00, 09:00 
           Clock-out: None
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift(day_type="rest_day")

        mock.raw_timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 09:00:00", "clock_in")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 2

    def test_default_rd_no_clock_in(self):
        """
        Conditions:
           Default rest day
           No Approved OT
           Clock-in: None
           Clock-out: 17:00, 18:00
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift(day_type="rest_day")
        
        mock.raw_timesheet([
            ("2019-01-01 17:00:00", "clock_out"),
            ("2019-01-01 18:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 2

    def test_default_rd_no_ts(self):
        """
        Conditions:
           Default rest day
           No Approved OT
           Clock-in: None
           Clock-out: None
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift(day_type="rest_day")
        

        mock.raw_timesheet([])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 0

    def test_default_rd_clock_out_next_day(self):
        """
        Conditions:
           Default rest day
           No Approved OT
           Clock-in: 23:00
           Clock-out: 07:00, 10:00
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift(day_type="rest_day")

        mock.raw_timesheet([
            ("2019-01-01 23:00:00", "clock_in"),
            ("2019-01-02 07:00:00", "clock_out"),
            ("2019-01-02 10:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 2

    def test_default_rd_next_day_ts_is_clock_in(self):
        """
        Conditions:
           Default rest day
           No Approved OT
           Clock-in: 23:00, 05:00(Next day)
           Clock-out: 07:00 (Next day)
        """
        shift_type = keys.SH_FIXED

        mock = utils.MockHero(
            date="2019-01-01",
            is_rest_day=True)

        mock.default_shift(day_type="rest_day")

        mock.raw_timesheet([
            ("2019-01-01 23:00:00", "clock_in"),
            ("2019-01-02 05:00:00", "clock_in"),
            ("2019-01-02 07:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)

        raw_timesheet = calc.get_shift_raw_timesheet("1", shift_type)

        assert len(raw_timesheet) == 1