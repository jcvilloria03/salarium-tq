# pylint: disable=E1102
"""
Basic fixed shift tests
"""
#import contextlib
#from unittest import TestCase, main, mock

# from tasks.attendance.ph import calculator
# from tests.data import create_fixedshift_withbreaks, create_mock_fixed_shift


# @contextlib.contextmanager
# def multiple_targets(mock_patches):
#     """
#     `mock_patches` is a list (or iterable) of mock.patch objects
#     Example usage:
#         with mock.patch.multiple_targets([
#                 mock.patch('os.path.exists', side_effect=mock_path_exists),
#                 mock.patch('subprocess.Popen'),
#                 mock.patch('os.mkdir'),
#                 mock.patch('os.symlink')]):
#             do_stuff()
#     Or if you need to do stuff with the mock objects:
#         with mock.patch.multiple_targets([
#                 mock.patch('os.path.exists', side_effect=mock_path_exists),
#                 mock.patch('subprocess.Popen'),
#                 mock.patch('os.mkdir'),
#                 mock.patch('os.symlink')]) as (mpe, msp, mom, mos):
#     """

#     mocks = []

#     for mock_patch in mock_patches:
#         _mock = mock_patch.start()
#         mocks.append(_mock)

#     yield mocks

#     for mock_patch in mock_patches:
#         mock_patch.stop()


# mock.patch.multiple_targets = multiple_targets


# class TestBasicCalculation(TestCase):
#     """Basic Attendance (PH) calculation testsv -No timeclock"""

#     def _run_test_table(self, test_cases):
#         for shift, clocks, result in test_cases:
#             with mock.patch.multiple_targets([
#                     mock.patch.object(calculator,
#                                       'get_shift',
#                                       return_value=shift),
#                     mock.patch.object(calculator,
#                                       'get_timeclocks',
#                                       return_Value=clocks)
#             ]):
#                 calc = calculator()
#                 res = calc.calculate('101', '2018-09-11')
#                 assert res == result

#     def test_ph_calculator_fixed_shift_basic(self):
#         """Basic No breaks, no nightshift simple tests"""

#         test_cases = [
#             # data_from_shift, data_from time clock, expected result
#             (create_mock_fixed_shift('07:00', '19:00'), [], dict(Regular=720)),
#             (create_mock_fixed_shift('08:00', '17:00'), [], dict(Regular=540)),
#             (create_mock_fixed_shift('09:00', '12:00'), [], dict(Regular=180))
#         ]

#         self._run_test_table(test_cases)

#     def test_ph_calculator_fixed_shift_with_breaks(self):
#         """Basic fixed shifts with breaks"""

#         test_cases = [
#             (create_fixedshift_withbreaks('08:00', '17:00').
#              with_break('fixed', '10:00', '10:15', '0:15').
#              with_break('fixed', '12:00', '13:00', '1:00').
#              with_break('flexi', '15:00', '16:00', '0:15').
#              with_break('floating', 'NA', 'NA', '0:20'), [], dict(Regular=430))
#         ]

#         self._run_test_table(test_cases)


# if __name__ == '__main__':
#     main()
