# pylint: disable=C0111
from unittest import TestCase, mock
from attendance.calls import get_tardiness_rule, _get_tardy_rules
from tests.attendance.utils import get_mock

class GetTardyRule(TestCase):
    @mock.patch('attendance.calls._get_tardy_rules',
                return_value=get_mock('tardy_rules_1'))
    def test_simple_1(self, getter):
        """Test for all employees"""
        rule = get_tardiness_rule(1, 2)
        getter.assert_called_once()
        assert rule
        assert rule["id"] == 98
        assert rule["company_id"] == 1

    @mock.patch('attendance.calls._get_tardy_rules',
                return_value=get_mock('tardy_rules_2'))
    def test_simple_employee_specific(self, getter):
        """Test for all employees"""
        rule = get_tardiness_rule(1, 2)
        getter.assert_called_once()
        assert rule
        assert rule["id"] == 98
        assert rule["company_id"] == 1

    @mock.patch('attendance.calls._get_tardy_rules',
                return_value=get_mock('tardy_rules_2'))
    def test_simple_employee_specific_no_match(self, getter):
        """Test for all employees"""
        rule = get_tardiness_rule(1, 100)
        getter.assert_called_once()
        assert rule is None

    @mock.patch("attendance.calls._get_resource",
                return_value={"data": [{"id": 123}]})
    def test__get_tardy_rules(self, fetcher):
        rules = _get_tardy_rules(1)
        fetcher.assert_called_once()
        assert isinstance(rules, list)
        assert len(rules) == 1
