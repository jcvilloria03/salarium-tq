# pylint: disable=E1102,C0111
from unittest import TestCase, mock

from attendance.models import HoursWorkedModel
from tests.attendance.utils import get_mock

SERVICE_NAMESPACE = "attendance.calls.get_hours_worked"

BROKEN_DATA = [
    {
        "id": 94,
        "employee_id": 5160,
        "date": "2018-12-22",
        "type": "regular",
        "type_name": "Regular",
        "time": None,
        "value": None,
        "shift_id": 341,
        "start_datetime": None,
        "end_datetime": None
    }
]


class HoursWorkedModelTest(TestCase):

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_overtime(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")

        mockeroo.assert_called_once()
        for hw in hw():
            if hw.hours_worked_id == 19:
                assert hw.hours_worked_type == "overtime"
                assert hw.start_date_time == "2018-09-14 18:00"
                assert hw.end_date_time == "2018-09-14 21:00"
                assert hw.code == "OT"
                assert hw.span
                assert hw.span.value == 180

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_undertime(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            if hw.hours_worked_id == 24:
                assert hw.hours_worked_type == "undertime"
                assert hw.start_date_time == "2018-09-18 07:00"
                assert hw.end_date_time == "2018-09-18 08:00"
                assert hw.code == "UH"
                assert hw.span
                assert hw.span.value == 60

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_paid_leave(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            if hw.hours_worked_id == 28:
                assert hw.hours_worked_type == "paid_leave"
                assert hw.start_date_time == "2018-09-19 09:00"
                assert hw.end_date_time == "2018-09-19 15:00"
                assert hw.code == "PAID LEAVE"
                assert hw.span
                assert hw.span.value == 360

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_otnt(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")

        mockeroo.assert_called_once()
        for hw in hw():
            if hw.hours_worked_id == 75:
                assert hw.hours_worked_type == "overtime_night_shift"
                assert hw.start_date_time is None
                assert hw.end_date_time is None
                assert hw.code == "NT+OT"
                assert hw.span
                assert hw.span.value == 1380

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_unpaid_leave(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            if hw.hours_worked_id == 90:
                assert hw.hours_worked_type == "unpaid_leave"
                assert hw.start_date_time == "2018-11-19 07:00"
                assert hw.end_date_time == "2018-11-19 15:00"
                assert hw.code == "UNPAID LEAVE"
                assert hw.span
                assert hw.span.value == 480

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_nt(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            if hw.hours_worked_id == 91:
                assert hw.hours_worked_type == "night_shift"
                assert hw.start_date_time == "2018-12-19 22:00"
                assert hw.end_date_time == "2018-12-20 03:00"
                assert hw.code == "NT"
                assert hw.span
                assert hw.span.value == 300

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_regular(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            if hw.hours_worked_id == 92:
                assert hw.hours_worked_type == "regular"
                assert hw.start_date_time == "2018-12-22 08:00"
                assert hw.end_date_time == "2018-12-22 17:00"
                assert hw.code == "REGULAR"
                assert hw.span
                assert hw.span.value == 540

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("hours_worked"))
    def test_hours_worked_all(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            assert hw.hours_worked_type
            assert hw.start_date_time is None or hw.start_date_time
            assert hw.end_date_time is None or hw.end_date_time
            assert hw.code
            assert hw.span
            assert hw.hours_worked_id

    @mock.patch(SERVICE_NAMESPACE, return_value=BROKEN_DATA)
    def test_hours_worked_broken_data(self, mockeroo):
        hw = HoursWorkedModel(1199, employee_ids=[5160],
                              start_date="2018-01-01", end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            assert hw.hours_worked_type == "regular"
            assert hw.start_date_time is None
            assert hw.end_date_time is None
            assert hw.code == "REGULAR"
            assert hw.span is None
            

    @mock.patch(SERVICE_NAMESPACE,
                return_value=get_mock("hours_worked_multi_emps"))
    def test_hours_worked_multi_emp_ids(self, mockeroo):
        ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        hw = HoursWorkedModel(1199, employee_ids=ids, start_date="2018-01-01",
                              end_date="2018-12-03")
        mockeroo.assert_called_once()
        for hw in hw():
            assert hw.employee_uid in ids
