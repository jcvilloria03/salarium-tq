# pylint: disable=E1102,C0111
from unittest import TestCase, mock

from attendance.tasks import get_company_timesheet
from tests.attendance import utils
from attendance import keys
from attendance import constants
from attendance import calculator

MOCK_GET_EMPLOYEE_TIMESHEET = 'core.timeclockutils.TimeClockModel.get_employee_timesheet'

# the clockout difference is exceeding 8hrs
GET_EMPLOYEE_TIMESHEET_RESULT_EXCEEDED_8_HRS = [
    {
        "employee_uid": 34388.0,
        "company_uid": "12074",
        "tags": [],
        "state": True,
        "timestamp": 1665446400.0
    },
    {
        "employee_uid": 34388.0,
        "company_uid": "12074",
        "timestamp": 1665486000.0,
        "tags": [],
        "state": False
    }
]

# the clockout difference is under 8hrs
GET_EMPLOYEE_TIMESHEET_RESULT_UNDER_8_HRS = [
    {
        "employee_uid": 34388.0,
        "company_uid": "12074",
        "tags": [],
        "state": True,
        "timestamp": 1665446400.0
    },
    {
        "employee_uid": 34388.0,
        "company_uid": "12074",
        "timestamp": 1665471600.0,
        "tags": [],
        "state": False
    }
]

MOCK_UPDATE_TIMERECORD = 'core.timeclockutils.TimeClockModel.update_timerecord'
MOCK_TimeClockModel_CONSTRUCTOR = 'core.timeclockutils.TimeClockModel.__init__'

def get_att_param():
    '''
    return att_param for 'get_company_timesheet' function
    '''
    att_param = {
        "dates": ["2022-10-11"],
        "company_id": 12074,
        "job_id": "86b330ca-d97a-424d-afc7-9fc2b2d2a6a7",
        "attendances": [
            {
                "date": "2022-10-11",
                "job_id": "86b330ca-d97a-424d-afc7-9fc2b2d2a6a7",
                "id": 34388,
                "user_id": 53351,
                "employee_id": "APPROVER-EMPLOYEE",
                "company_id": 12074,
                "max_clockout": {
                    "name": "test max clock out",
                    "hours": "8.00",
                    "type": "employee",
                },
                "employee_uid": 34388,
                "rest_days": [],
                "shifts": [],
                "timesheet": [],
                "raw_timesheet": [],
            }
        ],
    }
    return att_param


class TestMaxClockOutSetting(TestCase):

    @mock.patch(MOCK_TimeClockModel_CONSTRUCTOR, return_value=None)
    @mock.patch(MOCK_GET_EMPLOYEE_TIMESHEET, return_value=GET_EMPLOYEE_TIMESHEET_RESULT_EXCEEDED_8_HRS)
    @mock.patch(MOCK_UPDATE_TIMERECORD, return_value=True)
    def test_exceeded_timesheet_set_max_clockout_node_true(self, mock_constructor, mock_get_timesheet, mock_update_timerecord):
        '''
        max clockout rule = 8 hrs
        employee attendance exceeding 8 hrs
        '''
        res = get_company_timesheet(get_att_param())
        mock_get_timesheet.assert_called_once()
        mock_update_timerecord.assert_called_once()
        assert res['attendances'][0]['timesheet'][1]['max_clock_out'] == True
        assert res['attendances'][0]['raw_timesheet'][1]['max_clock_out'] == True
    
    @mock.patch(MOCK_TimeClockModel_CONSTRUCTOR, return_value=None)
    @mock.patch(MOCK_GET_EMPLOYEE_TIMESHEET, return_value=GET_EMPLOYEE_TIMESHEET_RESULT_UNDER_8_HRS)
    @mock.patch(MOCK_UPDATE_TIMERECORD, return_value=True)
    def test_valid_timesheet_set_max_clockout_node_true(self, mock_constructor, mock_get_timesheet, mock_update_timerecord):
        '''
        max clockout rule = 8 hrs
        employee attendance under 8 hrs
        '''
        res = get_company_timesheet(get_att_param())
        mock_get_timesheet.assert_called_once()
        mock_update_timerecord.assert_called_once()
        assert res['attendances'][0]['timesheet'][1]['max_clock_out'] == False
        assert res['attendances'][0]['raw_timesheet'][1]['max_clock_out'] == False


class TestMaxClockOutComputation(TestCase):
    """
    Test cases for timesheet required and not required
    """

    def test_tsr_invalid_attendance_scenario_1(self):
        """
        Has max_clock_out = True
        Should not compute the hours worked of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 60 * 8

    def test_tsr_invalid_attendance_scenario_2(self):
        """
        Has max_clock_out = True
        Should not compute the hours worked of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 18:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 60 * 8

    def test_tsr_invalid_attendance_scenario_3(self):
        """
        Has max_clock_out = True
        Should not compute the hours worked of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_in"),  # max_clock_out=True
        ])
        mock.set_max_clock_out(3, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 3
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 60 * 5

    def test_tsr_invalid_attendance_scenario_4(self):
        """
        Has max_clock_out = True
        Should not compute the hours worked of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),  # max_clock_out=True
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_in"),
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 60 * 3
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 5

    def test_tsr_valid_attendance_scenario_1(self):
        """
        Has max_clock_out = False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 18:00:00", "clock_in"),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 8

    # ==========================================================================
    # ======================== Timesheet note required =========================
    # ==========================================================================
    def test_tsnr_invalid_attendance(self):
        """
        Has max_clock_out = True
        Timesheet not required
        Should still compute the invalid pair
        of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 8


class TestMaxClockOutScheduledOvertime(TestCase):
    """
    Test cases for scheduled overtime
    """

    def test_tsr_invalid_attendance_scenario_1(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 60 * 8
        assert attendance.get("1").get(constants.ATT_OT) is None
    
    def test_tsr_invalid_attendance_scenario_2(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(3, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 3
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 60 * 5
        assert attendance.get("1").get(constants.ATT_OT) is None
    
    def test_tsr_invalid_attendance_scenario_3(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),  # max_clock_out=True
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 5
        assert attendance.get("1").get(constants.ATT_OT) == 60 * 2
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 60 * 3
    
    def test_tsr_invalid_attendance_scenario_4(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 18:00:00", "clock_out"),
            ("2019-01-01 18:10:00", "clock_in"),
            ("2019-01-01 22:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(5, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 8
        assert attendance.get("1").get(constants.ATT_OT) is None

    def test_tsr_valid_attendance(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")  # max_clock_out=False
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 8
        assert attendance.get("1").get(constants.ATT_OT) == 60 * 2

    # ==========================================================================
    # ======================== Timesheet note required =========================
    # ==========================================================================
    def test_tsnr_invalid_attendance_scenario_1(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 18:00:00", "clock_out"),
            ("2019-01-01 18:10:00", "clock_in"),
            ("2019-01-01 22:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(5, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 8
        assert attendance.get("1").get(constants.ATT_OT) is None
    
    def test_tsnr_invalid_attendance_scenario_2(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 22:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 8
        assert attendance.get("1").get(constants.ATT_OT) is None
    
    def test_tsnr_invalid_attendance_scenario_3(self):
        """
        Has max_clock_out = True
        Should not compute the scheduled overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            differential=True,
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])

        mock.timesheet([
            ("2019-01-01 18:00:00", "clock_in"),
            ("2019-01-01 22:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 60 * 8
        assert attendance.get("1").get(constants.ATT_OT) is None


class TestMaxClockOutOvertimeRequest(TestCase):

    def test_tsr_invalid_attendance_scenario_1(self):
        """
        Has max_clock_out = True
        Should not compute the approved overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-01 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) is None

    def test_tsr_invalid_attendance_scenario_2(self):
        """
        Has max_clock_out = True
        Should not compute the approved overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(3, True)
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-01 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 3 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 5 * 60
        assert attendance.get("1").get(constants.ATT_OT) is None
    
    def test_tsr_invalid_attendance_scenario_3(self):
        """
        Has max_clock_out = True
        Should not compute the approved overtime of
        invalid pair of clock-in and clock-out
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 12:00:00", "clock_out"),
            ("2019-01-01 13:00:00", "clock_in"),
            ("2019-01-01 18:00:00", "clock_out"),
            ("2019-01-01 19:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(5, True)
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 19:00",
                "end_datetime": "2019-01-01 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) is None

    # ==========================================================================
    # ======================== Timesheet note required =========================
    # ==========================================================================
    def test_tsnr_invalid_attendance_scenario_1(self):
        """
        Has max_clock_out = True
        Should still compute the approved overtime when
        timesheet not required and clockout is invalid
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
            }
        ])
        mock.breaks([
            {
                keys.TYPE: keys.BR_FIXED,
                keys.BREAK_HOURS: "01:00",
                keys.START: "12:00",
                keys.END: "13:00",
                keys.IS_PAID_BREAK: False
            }
        ])
        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")  # max_clock_out=True
        ])
        mock.set_max_clock_out(1, True)
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-01 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_OT) == 2 * 60
