# pylint: disable=E1102,C0111
"""
Basic fixed shift tests
"""
from unittest import TestCase, mock

from attendance.models import RestDayModel
from tests.attendance.utils import get_mock
from tests.attendance import utils
from attendance import calculator
from attendance import constants, keys

SERVICE_NAMESPACE = "attendance.calls.get_rest_days"

# namespace for is_rest_day method from DefaultCalculator
mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"
class RestDayModelTest(TestCase):

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("day_type"))
    def test_is_rest_day(self, mockeroo):
        dt = RestDayModel(172, "2018-09-08")
        mockeroo.assert_called_once()
        assert dt.is_rest_day()

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("day_type"))
    def test_repeating_restday_valid(self, mockeroo):
        dt = RestDayModel(172, "2019-09-07")
        mockeroo.assert_called_once()
        assert dt.is_rest_day()

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("day_type"))
    def test_repeating_restday_invalid(self, mockeroo):
        dt = RestDayModel(172, "2019-02-13")
        mockeroo.assert_called_once()
        assert not dt.is_rest_day()

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("day_type"))
    def test_repeating_restday_invalid2(self, mockeroo):
        dt = RestDayModel(172, "2019-02-13")
        mockeroo.assert_called_once()
        data = dt()

        assert data

        for i in data:
            assert i.start_date
            assert i.end_date
            assert i.id
            assert i.company_id

    def test_default_sched_rd_with_ts_with_no_ot(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. Default Schedule
        7. Rest Day
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0 * 60

    def test_default_sched_rd_without_ts(self):
        """
        1. Timesheet required - True
        5. Has rendered hours - False
        6. Default Schedule
        7. Rest Day
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0 * 60
        assert calc.get_scheduled_hours() == 0

    def test_user_defined_rd_with_ts(self):
        """
        1. Timesheet required - True
        5. Has rendered hours - True
        6. User defined rest day
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            is_rest_day=True)

        mock.user_defined_rest_days()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "08:00",
          "value": "8.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 17:00"
        }])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 9 * 60

    def test_user_defined_rd_without_ts(self):
        """
        1. Timesheet required - True
        5. Has rendered hours - False
        6. User defined rest day
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            is_rest_day=True)

        mock.user_defined_rest_days()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0 * 60
        assert calc.get_scheduled_hours() == 0

    def test_default_sched_rd_ts_required_with_ts_with_approved_ot(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Approved overtime 30 mins 
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            is_rest_day=True)
    
        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "08:00",
          "value": "8.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 17:00"
        }])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 9 * 60

    def test_default_sched_rd_ts_required_with_ts_time_records_and_approved_ot_exceeds_hours_per_day(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="12.00",
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 20:30:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "12:30",
          "value": "12.30",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 20:30"
        }])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 12 * 60
        assert attendance.get("ds_1").get(constants.ATT_RD_OT) == 0.5 * 60

    def test_default_sched_rd_ts_not_required_with_approved_ot(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            hours_per_day="12.00",
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "12:30",
          "value": "12.30",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 20:30"
        }])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 12 * 60
        assert attendance.get("ds_1").get(constants.ATT_RD_OT) == 0.5 * 60

    def test_default_sched_rd_ts_not_required_without_approved_ot(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            hours_per_day="12.00",
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0

    def test_default_sched_rd_ts_required_without_time_records_with_approved_ot(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="12.00",
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "12:30",
          "value": "12.30",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 20:30"
        }])
        
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0

    def test_default_sched_rd_ts_required_time_records_with_approved_ot_less_than_hours_per_day(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="8.00",
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 13:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "05:00",
          "value": "5.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 13:00"
        }])
        
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 300

    def test_default_sched_rd_ts_required_time_records_with_approved_ot_entitlement_rd_off_ot_on(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="8.00",
            rest_day_pay=False,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 13:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "05:00",
          "value": "5.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 13:00"
        }])
        
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0

    def test_default_sched_rd_ts_required_time_records_with_approved_ot_entitlement_rd_on_ot_off(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="8.00",
            rest_day_pay=True,
            overtime=False,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 13:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "05:00",
          "value": "5.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 13:00"
        }])
        
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0

    def test_default_sched_rd_ts_required_time_records_with_approved_ot_entitlement_rd_on_ot_on(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="8.00",
            rest_day_pay=True,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 13:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "05:00",
          "value": "5.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 13:00"
        }])
        
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 300

    def test_default_sched_rd_ts_required_time_records_with_approved_ot_entitlement_rd_on_ot_exceed_hrs_per_day(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="8.00",
            rest_day_pay=True,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 18:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "10:00",
          "value": "10.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 18:00"
        }])
        
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 8 * 60
        assert attendance.get("ds_1").get(constants.ATT_RD_OT) == 2 * 60


    def test_default_sched_rd_ts_required_time_records_with_approved_ot_entitlement_rd_on_ot_within_hrs_per_day(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Overtime filed = 2 hrs only
        - Approved overtime 30 mins
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="9.00",
            rest_day_pay=True,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "9:00",
          "value": "9.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 08:00",
          "end_datetime": "2019-01-01 17:00"
        }])
        
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 9 * 60
        assert attendance.get("ds_1").get(constants.ATT_RD) == 9 * 60

    def test_default_sched_rd_tsnr_with_time_records_without_approved_overtime_entitlements_rd_off_ot_on(self):
        """
        - Timesheet required - False
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            hours_per_day="9.00",
            rest_day_pay=False,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0

    def test_default_sched_rd_ts_required_time_records_with_approved_ot_entitlement_rd_on_ot_not_within_time_records(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Approved Overtime filed = 2 hrs
        - Not overlapping with time records
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="9.00",
            rest_day_pay=True,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "9:00",
          "value": "9.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 17:00",
          "end_datetime": "2019-01-01 20:00"
        }])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0

    def test_default_sched_rd_ts_required_time_records_with_approved_ot_entitlement_rd_on_ot_within_time_records(self):
        """
        - Timesheet required - True
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Approved Overtime filed = 2 hours duration, only 30 mins overlap with time records
        - Results in RD hours value from overlap with approved OT and time records
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            hours_per_day="9.00",
            rest_day_pay=True,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "9:00",
          "value": "9.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 17:00",
          "end_datetime": "2019-01-01 20:00"
        }])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 0.5 * 60


    def test_default_sched_rd_ts_not_required_time_records_with_approved_ot_entitlement_rd_on_ot_within_time_records(self):
        """
        - Timesheet required - False
        - Has rendered hours - True
        - Default Schedule
        - Rest Day
        - Set hours per day = 12
        - Approved Overtime filed = 2 hrs only
        - Should result to full duration of approved OT. Regardless of time records
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            hours_per_day="8.00",
            rest_day_pay=True,
            overtime=True,
            is_rest_day=True)

        mock.default_shift(
            day_type="rest_day")

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])

        mock.hours_worked([{
          "id": 19,
          "employee_id": 93,
          "date": "2019-01-01",
          "type": "overtime",
          "type_name": "Overtime",
          "time": "9:00",
          "value": "9.00",
          "shift_id": 1,
          "start_datetime": "2019-01-01 17:00",
          "end_datetime": "2019-01-01 23:00"
        }])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RD) == 6 * 60

    def test_tsr_rd_nt_carry_over(self):
        """
        - Timesheet required - True
        - Rest Day
        - Approved overtime 6 hours
        - Carry Over
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 22:00:00", "clock_in"),
            ("2019-01-02 04:00:00", "clock_out")
        ])

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 22:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == 0

    def test_tsr_rd_nt_cph(self):
        """
        - Timesheet required - True
        - Rest Day
        - Approved overtime 6 hours
        - Counter per hour
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.timesheet([
            ("2019-01-01 22:00:00", "clock_in"),
            ("2019-01-02 04:00:00", "clock_out")
        ])

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 22:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == 0
        
    def test_tsnr_rd_nt_carry_over(self):
        """
        - Timesheet required - False
        - Rest Day
        - Approved overtime 6 hours
        - Carry Over
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 22:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == 0

    def test_tsnr_rd_nt_cph(self):
        """
        - Timesheet required - False
        - Rest Day
        - Approved overtime 6 hours
        - Counter per hour
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 22:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == 0
 
    def test_tsr_rd_rd_plus_nt_carry_over(self):
        """
        - Timesheet required - True
        - Rest Day
        - Approved overtime 8 hours
        - Carry Over
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 04:00:00", "clock_out")
        ])

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 20:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == (2 * 60)

    def test_tsr_rd_rd_plus_nt_cph(self):
        """
        - Timesheet required - True
        - Rest Day
        - Approved overtime 8 hours
        - Counter per hour
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-02 04:00:00", "clock_out")
        ])

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 20:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == (2 * 60)

    def test_tsnr_rd_rd_plus_nt_carry_over(self):
        """
        - Timesheet required - False
        - Rest Day
        - Approved overtime 8 hours
        - Carry Over
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_CARRY_OVER)

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 20:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == (2 * 60)

    def test_tsnr_rd_rd_plus_nt_cph(self):
        """
        - Timesheet required - False
        - Rest Day
        - Approved overtime 8 hours
        - Counter per hour
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=False,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.default_shift(
            day_type=keys.REST_DAY)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 20:00",
                keys.END_DATETIME: "2019-01-02 04:00"
            }
        ])
        
        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (6 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == (2 * 60)

    def test_user_defined_tsr_rd_rd_plus_nt(self):
        """
        - Timesheet required - True
        - User Defined Rest Day
        - Approved overtime 3 hours
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.default_shift(
            day_type=keys.HR_REGULAR)

        mock.user_defined_rest_days(id=1000)

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 20:00",
                keys.END_DATETIME: "2019-01-01 23:00"
            }
        ])
        
        mock.timesheet([
            ("2019-01-01 20:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        
        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (1 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == (2 * 60)

    def test_user_defined_ot_longer_than_ts(self):
        """
        - Timesheet required - True
        - User Defined Rest Day
        - Approved overtime 10 hours
        """

        shift_id = "ds_1"
        shift_date="2019-01-01"

        mock = utils.MockHero(
            date=shift_date,
            timesheet_required=True,
            overtime=True,
            differential=True,
            is_rest_day=True)

        mock.night_shift_settings(
            activated=True,
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR)

        mock.default_shift(
            day_type=keys.HR_REGULAR)

        mock.user_defined_rest_days()

        mock.hours_worked([
            {
                keys.SHIFT_ID: shift_id,
                keys.TYPE: keys.HR_OVERTIME,
                keys.START_DATETIME: "2019-01-01 14:00",
                keys.END_DATETIME: "2019-01-02 00:00"
            }
        ])
        
        mock.timesheet([
            ("2019-01-01 14:00:00", "clock_in"),
            ("2019-01-01 23:00:00", "clock_out")
        ])

        mock_data = mock.generate()

        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert len(attendance.get(shift_id)) == 2
        assert attendance.get(shift_id).get(constants.ATT_RD_NT) == (1 * 60)
        assert attendance.get(shift_id).get(constants.ATT_RD) == (8 * 60)

    @mock.patch(mock_is_restday, return_value=True)
    def test_fixed_shift_continous_restday_night_shift_scenario_1(self, restd):
        """
        Scenario:-
        - Fixed night shift assigned
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day user defined rest day
        - Holidays settings:-
            - current day is regular holiday
            - next day is not holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        # Default Shift
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00"
            }
        ])
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([
            ( "2019-01-01",  True, constants.ATT_RH),
            ( "2019-01-02",  True, None)
            ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_RD_NT) == 2 * 60  # 00:00 to 00:03 (deducted break time)
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=True)
    def test_fixed_shift_continous_restday_night_shift_scenario_2(self, restd):
        """
        Scenario:-
        - Fixed night shift assigned
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day user defined rest day
        - Holidays settings:-
            - current day is regular holiday
            - next day is regular holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        # Default Shift
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00"
            }
        ])
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([
            ( "2019-01-01",  True, constants.ATT_RH),
            ( "2019-01-02",  True, constants.ATT_RH)
            ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 4 * 60  # 22:00 to 00:03 (deducted break time)
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=False)
    def test_fixed_shift_continous_restday_night_shift_scenario_3(self, restd):
        """
        Scenario:-
        - Fixed night shift assigned
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day is regular day
        - Holidays settings:-
            - current day is regular holiday
            - next day is not holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        # Default Shift
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00"
            }
        ])
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([
            ( "2019-01-01",  True, constants.ATT_RH),
            ( "2019-01-02",  False, None)
            ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60  # 00:00 to 00:03 (deducted break time)
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=False)
    def test_fixed_shift_continous_restday_night_shift_scenario_3(self, restd):
        """
        Scenario:-
        - Fixed night shift assigned
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day is regular day
        - Holidays settings:-
            - current day is regular holiday
            - next day is regular holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        # Default Shift
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00"
            }
        ])
        # With clock in, clock out
        mock.hours_worked([])
  
        # Company settings holiday
        mock.set_shift_dates([
            ( "2019-01-01",  True, constants.ATT_RH),
            ( "2019-01-02",  False, constants.ATT_RH)
            ])

        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_RH_NT) == 2 * 60  # 00:00 to 00:03 (deducted break time)
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")


    @mock.patch(mock_is_restday, return_value=True)
    def test_flexi_shift_no_core_hours_continous_restday_night_shift_scenario_1(self, restd):
        """
        Scenario:-
        - flexi night shift with no core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day user defined rest day
        - Holidays settings:-
            - current day is regular holiday
            - next day is not holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", True, None),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_RD_NT) == 3 * 60  # 00:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=True)
    def test_flexi_shift_no_core_hours_continous_restday_night_shift_scenario_2(self, restd):
        """
        Scenario:-
        - flexi night shift with no core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day user defined rest day
        - Holidays settings:-
            - current day is regular holiday
            - next day regular holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", True, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 5 * 60  # 22:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_shift_no_core_hours_continous_restday_night_shift_scenario_3(self, restd):
        """
        Scenario:-
        - flexi night shift with no core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day is regular day
        - Holidays settings:-
            - current day is regular holiday
            - next day is not holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", False, None),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_NT) == 3 * 60  # 00:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_shift_no_core_hours_continous_restday_night_shift_scenario_4(self, restd):
        """
        Scenario:-
        - flexi night shift with no core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day is regular day
        - Holidays settings:-
            - current day is regular holiday
            - next day is regular holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_RH_NT) == 3 * 60  # 00:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    

    @mock.patch(mock_is_restday, return_value=True)
    def test_flexi_shift_with_core_hours_continous_restday_night_shift_scenario_1(self, restd):
        """
        Scenario:-
        - flexi night shift with core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day user defined rest day
        - Holidays settings:-
            - current day is regular holiday
            - next day is not holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])

        mock.core_hours([
            {
                keys.START: "00:00",
                keys.END: "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", True, None),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_RD_NT) == 3 * 60  # 00:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=True)
    def test_flexi_shift_with_core_hours_continous_restday_night_shift_scenario_2(self, restd):
        """
        Scenario:-
        - flexi night shift with core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day user defined rest day
        - Holidays settings:-
            - current day is regular holiday
            - next day regular holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])

        mock.core_hours([
            {
                keys.START: "00:00",
                keys.END: "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", True, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 5 * 60  # 22:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_shift_with_core_hours_continous_restday_night_shift_scenario_3(self, restd):
        """
        Scenario:-
        - flexi night shift with core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day is regular day
        - Holidays settings:-
            - current day is regular holiday
            - next day is not holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])

        mock.core_hours([
            {
                keys.START: "00:00",
                keys.END: "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", False, None),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_NT) == 3 * 60  # 00:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
    
    @mock.patch(mock_is_restday, return_value=False)
    def test_flexi_shift_with_core_hours_continous_restday_night_shift_scenario_4(self, restd):
        """
        Scenario:-
        - flexi night shift with core hours
        - Night shift counter per hour
        - Rest days settings:-
            - current day user defined rest day
            - next day is regular day
        - Holidays settings:-
            - current day is regular holiday
            - next day is regular holiday
        - Night shift counter per hour
        - Shift has on_rest_day and on_holidays True

        - undertime or UH should not be calculated
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            overtime=True,
            differential=True,
            holiday_premium_pay=True,
            regular_holiday_pay=True,
            special_holiday_pay=True
        )

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift=keys.NT_COUNTER_PER_HOUR
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_rest_day": True,
                "on_holidays": True
            }
        ])

        mock.core_hours([
            {
                keys.START: "00:00",
                keys.END: "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.set_shift_dates([
            ("2019-01-01", True, constants.ATT_RH),
            ("2019-01-02", False, constants.ATT_RH),
        ])
        mock.timesheet([
            ("2019-01-01 21:00:00", "clock_in"),
            ("2019-01-02 03:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 1 * 60  # 21:00 to 22:00
        assert attendance.get("1").get(constants.ATT_RD_RH_NT) == 2 * 60  # 22:00 to 00:00
        assert attendance.get("1").get(constants.ATT_RH_NT) == 3 * 60  # 00:00 to 00:03
        assert constants.ATT_UH not in attendance.get("1")
        assert constants.ATT_UNDERTIME not in attendance.get("1")
        assert constants.ATT_ABSENT not in attendance.get("1")
