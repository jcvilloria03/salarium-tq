
# pylint: disable=E1102,C0111
"""
Basic fixed shift tests
"""
from unittest import TestCase, mock

from attendance.models import EmployeeShiftModel
from attendance.calls import get_employee_shift
from tests.attendance.utils import get_mock

SERVICE_NAMESPACE = "attendance.calls.get_employee_shift"


class TestGetEmployeeShift(TestCase):
    @mock.patch("attendance.calls._get_resource", return_value=get_mock('employee_shift'))
    def test_basic_get(self, mock_get):
        shifts = get_employee_shift(2, start_date='2018-10-10', end_date='2018-10-10')
        mock_get.assert_called_once()
        #assert shifts.get("data")
        assert len(shifts) == 2

class TestEmployeeShiftModel(TestCase):

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock('employee_shift_pruned'))
    def test_basic_fetch(self, mock_get):
        possible_time_methods = ["Bundy App", "Web Bundy"]
        emp = EmployeeShiftModel(
            172, start_date="2018-10-10", end_date="2018-10-10")
        mock_get.assert_called_once()
        for i in emp():
            assert i() == "fixed"
            assert i.timein in [1320, 420]
            assert i.timeout in [480, 1140]
            assert i.total_shift_hours in [600, 720]
            #assert i.start == "2018-10-10"
            #assert i.end == "2018-10-10"
            assert isinstance(i.time_methods, list)

            if i.time_methods:
                assert i.is_required_clocking is True
                for t in i.time_methods:
                    assert t in possible_time_methods
            else:
                assert i.is_required_clocking is False

        assert emp.scheduled_hours == 1320

    @mock.patch(SERVICE_NAMESPACE, return_value=None)
    def test_error_fetch(self, mock_get):
        emp = EmployeeShiftModel(
            1, start_date="2018-10-10", end_date="2018-10-10")
        mock_get.assert_called_once()
        assert not emp()

    @mock.patch(SERVICE_NAMESPACE, return_value=[])
    def test_blank_fetch(self, mock_get):
        emp = EmployeeShiftModel(
            1, start_date="2018-10-10", end_date="2018-10-10")
        mock_get.assert_called_once()
        assert not emp()

    @mock.patch(SERVICE_NAMESPACE, return_value=[get_mock('employee_shift_pruned')[0]])
    def test_breaks(self, mock_get):
        emp = EmployeeShiftModel(
            5, start_date="2018-10-10", end_date="2018-10-10")
        mock_get.assert_called_once()
        for i in emp():
            for b in i.shift_breaks:
                assert b.duration in [15, 60, 15, 20]
                assert b.id in [51, 52, 53, 54]
                assert b.schedule_id == 94
                assert b.type in ['fixed', 'flexi', 'floating']
