# pylint: disable=W0212,C0301
"""Calculator unit tests"""
import unittest
import os
import time
from attendance import calculator, constants
from tests.attendance import utils
#from tests.data import MockAttParams, MockShiftParams

os.environ["TZ"] = "Asia/Manila"
time.tzset()


class TestDefaultCalculator(unittest.TestCase):
    """Default Calculator tests"""

    def test_scenario_1_a(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_1_simple_regular_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 12 * 60

    def test_scenario_1_b(self):
        """Test night shift only"""
        data = utils.get_mock("scenario_1_simple_night_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 9 * 60

    def test_scenario_1_regular_nt_overlap(self):
        """Test regular hours with two hours NT overlap"""
        data = utils.get_mock("scenario_1_regular_nt_overlap")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60

    def test_scenario_1_regular_nt_overlap_not_entitled(self):
        """Test regular hours with two hours NT overlap"""
        data = utils.get_mock("scenario_1_regular_nt_overlap_not_entitled")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60
        assert len(attendance) == 1

    def test_scenario_1_multiple_regular_shifts(self):
        """Test regular hours from two regular shifts"""
        data = utils.get_mock("scenario_1_multiple_regular_shifts")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("2")
        assert attendance.get("2").get(constants.ATT_REGULAR)
        assert attendance.get("2").get(constants.ATT_REGULAR) == 4 * 60

    def test_scenario_1_nt_counter_tomorrow_holiday(self):
        """Test a nightshift counter per hour, next day is an RH"""
        data = utils.get_mock("scenario_1_nightshift_counter_tomorrow_holiday")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60
        assert attendance.get("1").get("RH+NT")
        assert attendance.get("1").get("RH+NT") == 7 * 60

    def test_scenario_2_fixed_sched_with_no_timesheet(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock("scenario_2_fixed_sched_with_no_timesheet")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert calc._ts_span is None
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT)
        assert attendance.get("1").get(constants.ATT_ABSENT) == 9 * 60

    def test_scenario_3_fixed_sched_with_fixed_break_no_timesheet(self):
        """Scenario 3 test"""
        data = utils.get_mock(
            "scenario_3_fixed_sched_fixed_break_no_timesheet")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_scenario_4_fixed_sched_with_timesheet_with_undertime(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock(
            "scenario_4_fixed_sched_with_timesheet_with_undertime")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert calc._ts_span
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME)
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 60

    def test_scenario_4_fixed_sched_with_timesheet_simple(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock("scenario_4_fixed_sched_with_timesheet")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60

    def test_scenario_4_fixed_sched_with_timesheet_with_tardy(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock("scenario_4_fixed_sched_with_tardy")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert calc._ts_span
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
        assert attendance.get("1").get(constants.ATT_TARDY)
        assert attendance.get("1").get(constants.ATT_TARDY) == 60

    def test_scenario_4_fixed_sched_with_timesheet_with_almost_tardy(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock("scenario_4_fixed_sched_with_almost_tardy")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert calc._ts_span
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60

    def test_scenario_5_fixed_sched_fixed_break_no_ts_approved_leave_1(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock(
            "scenario_5_fixed_sched_fixed_break_no_ts_approved_leave_1")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60

    def test_scenario_5_fixed_sched_fixed_break_no_ts_approved_leave_2(self):
        """Test a normal shift with simple timesheet - full shift leave"""
        data = utils.get_mock(
            "scenario_5_fixed_sched_fixed_break_no_ts_approved_leave_2")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_PAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 8 * 60

    def test_scenario_5_fixed_sched_fixed_break_no_ts_unpaid_leave_1(self):
        """Test a normal shift with simple timesheet - full shift leave"""
        data = utils.get_mock(
            "scenario_5_fixed_sched_fixed_break_no_ts_unpaid_leave_1")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 8 * 60

    def test_scenario_5_fixed_sched_fixed_break_with_ts_paid_leave_1(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock(
            "scenario_5_fixed_sched_fixed_break_with_ts_paid_leave_1")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert calc._ts_span
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 4 * 60

    def test_scenario_6_fixed_sched_no_ts_with_floating_break(self):
        """Test a normal shift with simple timesheet."""
        data = utils.get_mock(
            "scenario_6_fixed_sched_no_ts_with_floating_break")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60 - 20

    def test_scenario_6_fixed_sched_with_ts_with_floating_break(self):
        """Test a normal shift with simple timesheet with 20 minute floating break
           timesheet indicates gap from 9:00-9:20 (the supposed ft break)
        """
        data = utils.get_mock(
            "scenario_6_fixed_sched_with_ts_with_floating_break")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60 - 20
        assert len(attendance) == 1

    def test_scenario_6_fixed_sched_with_ts_with_ftb_undertime_end(self):
        """Test a normal shift with simple timesheet with floating break,
           Employee did not take floating break but has a 20 minute undertime
           at the end of shift.
        """
        data = utils.get_mock(
            "scenario_6_fixed_sched_with_ts_with_ftb_undertime_end")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * \
            60 - 20 - 20  # for the undertime amd for the unused break
        assert attendance.get("1").get(constants.ATT_UNDERTIME)
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 20

    def test_scenario_6_fixed_sched_with_ts_overtime(self):
        """Straight shift with overtime.
        """
        data = utils.get_mock(
            "scenario_6_fixed_sched_with_ts_overtime")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 9 * 60
        assert attendance.get("1").get(constants.ATT_OT)
        assert attendance.get("1").get(constants.ATT_OT) == 120

    def test_scenario_7_fixed_sched_with_ts_with_flexi(self):
        """Shift with flexi break from 11am to 1pm for 1 hour only"""
        data = utils.get_mock(
            "scenario_7_fixed_sched_with_ts_with_flexi")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_scenario_8_flexi_sched_with_ts_no_core_hours(self):
        """Flexi shift with no core hours basic"""
        data = utils.get_mock(
            "scenario_8_flexi_sched_with_ts_no_core_hours"
        )
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_scenario_8_flexi_sched_with_ts_with_core_hours(self):
        """Flexi shift with no core hours basic"""
        data = utils.get_mock(
            "scenario_8_flexi_sched_with_ts_with_core_hours"
        )
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_scenario_8_flexi_sched_with_ts_with_core_hours_undertime(self):
        """Flexi sched with core hours, with core hour undertime"""
        data = utils.get_mock(
            "scenario_8_flexi_sched_with_ts_with_core_hours_undertime")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 7 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME)
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 1 * 60

    def test_scenario_9_fixed_sched_with_timesheet_unworked(self):
        """Fixed sched on a special holiday"""
        data = utils.get_mock("scenario_9_fixed_sched_with_timesheet_unworked")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH)
        assert attendance.get("1").get(constants.ATT_UH) == 9 * 60

    def test_scenario_time_dispute_replaces_timesheet(self):
        """Flexi sched with core hours, with core hour undertime"""
        data = utils.get_mock(
            "scenario_time_dispute_replaces_timesheet")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR)
        assert attendance.get("1").get(constants.ATT_REGULAR) == 4 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME)
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 5 * 60

    def test_scenario_2c_fixed_sched_with_ts_night_shift_holiday_counter_per_hour(self):
        """Fixed Sched Night Shift with Counter Per Hour settings, Time Records,
        Entitlement checking
        """
        data = utils.get_mock(
            "scenario_2c_fixed_sched_with_ts_night_shift_holiday_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60
        assert attendance.get("1").get("RH+NT")
        assert attendance.get("1").get("RH+NT") == 7 * 60

    def test_scenario_2c_fixed_sched_with_ts_night_shift_non_holiday_counter_per_hour(self):
        """Fixed Sched Night Shift with Counter Per Hour settings, Time Records,
        Entitlement checking
        """
        data = utils.get_mock(
            "scenario_2c_fixed_sched_with_ts_night_shift_non_holiday_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 9 * 60

    def test_scenario_2b_fixed_sched_with_ts_night_shift_carry_over(self):
        """Fixed Sched Night Shift with Carry Over settings, Time Records, Entitlement checking"""
        data = utils.get_mock(
            "scenario_2b_fixed_sched_with_ts_night_shift_carry_over")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 9 * 60

    def test_scenario_8_fixed_sched_with_ts_fixed_break_night_shift_counter_per_hour(self):
        """Fixed Sched Night Shift with Fixed Break, Counter Per Hour settings, Time Records,
           Entitlement checking
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_fixed_break_night_shift_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60
        assert attendance.get("1").get("RH+NT")
        assert attendance.get("1").get("RH+NT") == 6 * 60

    def test_scenario_8_fixed_sched_with_ts_fixed_break_night_shift_carry_over(self):
        """Fixed Sched Night Shift with Fixed Break, Counter Per Hour settings, Time Records,
           Entitlement checking
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_fixed_break_night_shift_carry_over")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 8 * 60

    def test_scenario_8_fixed_sched_with_ts_tardy_fixed_break_night_shift_carry_over(self):
        """Fixed Sched Night Shift with Tardy, Fixed Break, Counter Per Hour settings, Time Records,
           Entitlement checking
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_tardy_fixed_break_night_shift_carry_over")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 7.25 * 60  # 7h 15m
        assert attendance.get("1").get(constants.ATT_TARDY)
        assert attendance.get("1").get(constants.ATT_TARDY) == 0.75 * 60  # 45m

    def test_scenario_8_fixed_sched_with_ts_tardy_fixed_break_night_shift_counter_per_hour(self):
        """Fixed Sched Night Shift with Tardy, Fixed Break, Counter Per Hour settings,
           Time Records, Entitlement checking
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_tardy_fixed_break_night_shift_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 1.25 * 60  # 1h 15m
        assert attendance.get("1").get(constants.ATT_TARDY)
        assert attendance.get("1").get(constants.ATT_TARDY) == 0.75 * 60  # 45m
        assert attendance.get("1").get("RH+NT")
        assert attendance.get("1").get("RH+NT") == 6 * 60

    def test_scenario_8_fixed_sched_with_ts_ot_tardy_fixed_break_night_shift_counter_per_hour(self):
        """Fixed Sched Night Shift with Overtime, Tardy, Fixed Break, Counter Per Hour settings,
           Time Records, Entitlement checking
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_ot_tardy_fixed_break_night_shift_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 1.25 * 60  # 1h 15m
        assert attendance.get("1").get(constants.ATT_TARDY)
        assert attendance.get("1").get(constants.ATT_TARDY) == 0.75 * 60  # 45m
        assert attendance.get("1").get("RH+NT")
        assert attendance.get("1").get("RH+NT") == 6 * 60

    def test_scenario_8_fixed_sched_with_ts_not_entitled_ot_fixed_break_night_shift(self):
        """Fixed Sched Night Shift with Overtime, Tardy, Fixed Break, Counter Per Hour settings,
           Time Records, Entitlement checking
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_not_entitled_ot_fixed_break_night_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 8 * 60

    def test_scenario_8_fixed_sched_no_ts_not_entitled_uh_fixed_break_night_shift(self):
        """Fixed Sched Night Shift, Not Entitled to any Holiday, ABSENT, Timesheet Required
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_no_ts_not_entitled_uh_fixed_break_night_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT)
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_scenario_8_fixed_sched_with_ts_not_entitled_uh_fixed_break_night_shift(self):
        """Fixed Sched Night Shift, Not Entitled to any Holiday, with Timesheet, Timesheet Required
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_not_entitled_uh_fixed_break_night_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 8 * 60

    def test_scenario_8_fixed_sched_with_ts_entitled_premium_uh_fixed_break_night_shift(self):
        """Fixed Sched Night Shift, Entitled to any Holiday, with TS, Timesheet Required
        """
        data = utils.get_mock(
            "scenario_8_fixed_sched_with_ts_entitled_premium_uh_fixed_break_night_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 2 * 60
        assert attendance.get("1").get("RH+NT")
        assert attendance.get("1").get("RH+NT") == 6 * 60

    def test_scenario_8a_fixed_sched_with_ts_ut_fixed_break_night_shift_counter_per_hour(self):
        """Fixed Sched Night Shift with Tardy, Undertime, Fixed Break, Counter Per Hour settings,
           Time Records
        """
        data = utils.get_mock(
            "scenario_8a_fixed_sched_with_ts_ut_fixed_break_night_shift_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 1.25 * 60  # 1h 15m
        assert attendance.get("1").get(constants.ATT_TARDY)
        assert attendance.get("1").get(constants.ATT_TARDY) == 0.75 * 60  # 45m
        assert attendance.get("1").get("RH+NT")
        assert attendance.get("1").get("RH+NT") == 4.75 * 60  # 4h 45m
        assert attendance.get("1").get(constants.ATT_UNDERTIME)
        assert attendance.get("1").get(
            constants.ATT_UNDERTIME) == 1.25 * 60  # 1h 15m

    def test_scenario_8a_fixed_sched_with_ts_ut_fixed_break_night_shift_carry_over(self):
        """Fixed Sched Night Shift with Tardy, Undertime, Fixed Break, Carry Over settings,
           Time Records
        """
        data = utils.get_mock(
            "scenario_8a_fixed_sched_with_ts_ut_fixed_break_night_shift_carry_over")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_NT)
        assert attendance.get("1").get(constants.ATT_NT) == 6 * 60
        assert attendance.get("1").get(constants.ATT_TARDY)
        assert attendance.get("1").get(constants.ATT_TARDY) == 0.75 * 60  # 45m
        assert attendance.get("1").get(constants.ATT_UNDERTIME)
        assert attendance.get("1").get(
            constants.ATT_UNDERTIME) == 1.25 * 60  # 1h 15m

    def test_scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_approved_leave_carry_over(self):
        """Fixed Sched Night Shift with Paid Leave, Carry Over settings"""
        data = utils.get_mock(
            "scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_approved_leave_carry_over")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_approved_leave_counter_per_hour(self):
        """Fixed Sched Night Shift with Paid Leave, Counter Per Hour settings"""
        data = utils.get_mock(
            "scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_approved_leave_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_PAID_LEAVE) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_unpaid_leave_counter_per_hour(self):
        """Fixed Sched Night Shift with Paid Leave, Counter Per Hour settings"""
        data = utils.get_mock(
            "scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_unpaid_leave_counter_per_hour")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_unpaid_leave_carry_over(self):
        """Fixed Sched Night Shift with Paid Leave, Carry Over settings"""
        data = utils.get_mock(
            "scenario_8a_fixed_sched_fixed_break_no_ts_with_fixed_break_night_shift_unpaid_leave_carry_over")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE)
        assert attendance.get("1").get(constants.ATT_UNPAID_LEAVE) == 8 * 60
        assert not attendance.get("1").get(constants.ATT_NT)

    def test_scenario_default_shift_is_used_when_no_shift(self):
        """Test for default schedule kicking in if no shift data"""
        data = utils.get_mock("scenario_default_shift_is_used_when_no_shift")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_664")
        assert attendance.get("ds_664").get(constants.ATT_REGULAR)
        assert attendance.get("ds_664").get(constants.ATT_REGULAR) == 8 * 60

    def test_scenario_regular_shift_date_hired(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_1_simple_night_shift_date_hired")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT)
        assert attendance.get("1").get(constants.ATT_ABSENT) == 12 * 60

    def test_scenario_regular_shift_date_ended(self):
        """Test shift info only"""
        data = utils.get_mock("scenario_1_simple_night_shift_date_ended")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT)
        assert attendance.get("1").get(constants.ATT_ABSENT) == 12 * 60
