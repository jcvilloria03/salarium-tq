# pylint: disable=E1102,C0111
"""
Basic fixed shift tests
"""
from re import T
from unittest import TestCase, mock
import os
import time
from attendance.models import HolidayModel
from tests.attendance.utils import get_mock
from attendance import calculator, constants
from tests.attendance import utils

os.environ["TZ"] = "Asia/Manila"
time.tzset()


SERVICE_NAMESPACE = "attendance.calls.get_holidays"
# namespace for is_rest_day method from DefaultCalculator
mock_is_restday = "attendance.calculator.DefaultCalculator._is_date_restday"

class HolidayModelTest(TestCase):

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("holidays"))
    def test_holiday_SH(self, mockeroo):
        h = HolidayModel(1, date="2018-09-05", department_id=86)
        mockeroo.assert_called_once()
        assert h() == 'RH+SH'

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("holidays"))
    def test_holiday_RH(self, mockeroo):
        h = HolidayModel(1, date="2018-09-05", location_id=21)
        mockeroo.assert_called_once()
        assert h() == '2RH'

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("holidays"))
    def test_combination_2SHRH(self, mockeroo):
        h = HolidayModel(1, date="2018-09-05", location_id=21,
                         position_id=86, employee_id=96)
        mockeroo.assert_called_once()
        assert h() == "2RH+SH"

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("holidays"))
    def test_combination_2SHRH_2(self, mockeroo):
        h = HolidayModel(1, date="2018-09-05", location_id=21,
                         position_id=86, employee_id=96)
        mockeroo.assert_called_once()
        assert h() == "2RH+SH"

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("holidays"))
    def test_all_employees(self, mockeroo):
        h = HolidayModel(1, date="2018-09-04", employee_id=96)
        mockeroo.assert_called_once()
        assert h() == constants.ATT_RH

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("holidays"))
    def test_all_2SH(self, mockeroo):
        h = HolidayModel(1, date="2018-09-05", employee_id=96,
                         department_id=86)
        mockeroo.assert_called_once()
        assert h() == "RH+2SH"

    @mock.patch(SERVICE_NAMESPACE, return_value=None)
    def test_None(self, mockeroo):
        h = HolidayModel(5, date="2018-09-05", employee_id=96,
                         department_id=86)
        mockeroo.assert_called_once()
        assert h() is None

    @mock.patch(SERVICE_NAMESPACE, return_value=get_mock("holidays"))
    def test_no_matching_date(self, mockeroo):
        h = HolidayModel(1, date="2018-09-20", employee_id=96,
                         department_id=86)
        mockeroo.assert_called_once()
        assert h() is None

    def test_scenario_tsnr_default_sched_day_shift_uh(self):
        """Test TSNR default schedule RH
        1. TSNR
        2. Default schedule
        3. RH
        4. Day shift
        5. regular holiday pay - true
        6. special holiday pay - true
        7. holiday premium pay - true
        """
        data = utils.get_mock("holidays_tsnr_default_sched_day_shift_uh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_664")
        assert attendance.get("ds_664").get(constants.ATT_UH) == 8 * 60

    def test_scenario_tsr_default_sched_day_shift_uh(self):
        """Test TSNR default schedule RH
        1. TSR
        2. Default schedule
        3. RH
        4. Day shift
        5. regular holiday pay - true
        6. special holiday pay - true
        7. holiday premium pay - true
        """
        data = utils.get_mock("holidays_tsr_default_sched_day_shift_uh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_664")
        assert attendance.get("ds_664").get(constants.ATT_UH) == 8 * 60

    def test_scenario_tsr_default_sched_day_shift_with_ts_rh(self):
        """Test TSNR default schedule RH
        1. TSR
        2. Default schedule
        3. RH
        4. Day shift
        5. regular holiday pay - true
        6. special holiday pay - true
        7. holiday premium pay - true
        8. With timesheet
        """
        data = utils.get_mock("holidays_tsr_default_sched_day_shift_with_ts_rh")
        calc = calculator.get_calculator(data)
        attendance = calc.attendance
        assert attendance.get("ds_664")
        assert attendance.get("ds_664").get(constants.ATT_RH) == 8 * 60

    def test_holidays_sc_default_1_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RH) == 8 * 60

    def test_holidays_sc_default_1_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_SH) == 8 * 60

    def test_holidays_sc_default_1_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RH_SH) == 8 * 60

    def test_holidays_sc_default_2_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_2_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_2_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_3_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_3_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_3_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_4_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_4_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_4_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_5_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RH) == 8 * 60

    def test_holidays_sc_default_5_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_SH) == 8 * 60

    def test_holidays_sc_default_5_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RH_SH) == 8 * 60

    def test_holidays_sc_default_6_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_default_6_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_default_6_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_default_7_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_7_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_7_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_8_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_default_8_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_default_8_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_default_9_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RH) == 8 * 60

    def test_holidays_sc_default_9_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_SH) == 8 * 60

    def test_holidays_sc_default_9_rhsh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_RH_SH) == 8 * 60

    def test_holidays_sc_default_10_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_10_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_10_rhsh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_11_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_11_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_11_rhsh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_default_12_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_12_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_default_12_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1")
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_1_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_holidays_sc_fixed_1_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_holidays_sc_fixed_1_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60

    def test_holidays_sc_fixed_2_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_2_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_2_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_2_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_3_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_3_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_3_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_4_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_4_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_4_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_5_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_holidays_sc_fixed_5_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_holidays_sc_fixed_5_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60

    def test_holidays_sc_fixed_6_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_fixed_6_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_fixed_6_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_fixed_7_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_7_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_7_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_8_rh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_fixed_8_sh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_fixed_8_rhsh(self):
        """
        1. Timesheet required - True
        2. Unworked regular holiday - False
        3. Unworked special holiday - False
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_holidays_sc_fixed_9_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_holidays_sc_fixed_9_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_holidays_sc_fixed_9_rhsh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60

    def test_holidays_sc_fixed_10_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_10_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_10_rhsh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_11_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_11_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_11_rhsh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - True
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 08:00",
                "end_datetime": "2019-01-01 17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_holidays_sc_fixed_12_rh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_12_sh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_holidays_sc_fixed_12_rhsh(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - False
        5. Has rendered hours - False
        6. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00"
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_1_rh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_1_rh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays - false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_1_rh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_flexi_holiday_with_core_hours_1_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_1_sh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays - false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_1_sh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_flexi_holiday_with_core_hours_1_rhsh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_1_rhsh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_1_rhsh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60

    def test_flexi_holiday_with_core_hours_2_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_2_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_2_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_3_rh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_3_rh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_3_rh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_3_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_3_sh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays - false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_3_sh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_3_rhsh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_3_rhsh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays - false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_3_rhsh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_4_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_4_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_4_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_4_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_4_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_4_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_5_rh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_5_rh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_5_rh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_flexi_holiday_with_core_hours_5_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_5_sh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_5_sh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_flexi_holiday_with_core_hours_5_rhsh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_5_rhsh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_5_rhsh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60

    def test_flexi_holiday_with_core_hours_6_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_with_core_hours_6_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_with_core_hours_6_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_with_core_hours_7_rh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_7_rh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_7_rh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_7_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_7_sh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_7_sh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_7_rhsh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_7_rhsh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_7_rhsh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_8_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_with_core_hours_8_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_with_core_hours_8_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_with_core_hours_8_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_with_core_hours_9_rh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_9_rh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_9_rh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60

    def test_flexi_holiday_with_core_hours_9_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_9_sh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_9_sh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_flexi_holiday_with_core_hours_9_rhsh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - False
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_9_rhsh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays - False
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_9_rhsh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60

    def test_flexi_holiday_with_core_hours_10_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_10_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_10_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_11_rh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_11_rh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_11_rh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_11_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_11_sh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_11_sh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_11_rhsh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_11_rhsh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_with_core_hours_11_rhsh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_with_core_hours_12_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_12_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_with_core_hours_12_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_1_rh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_rh_with_ts_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_rh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_rh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_1_sh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_sh_with_ts_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_sh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_sh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_1_rhsh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_rhsh_with_ts_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_rhsh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_1_rhsh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60


    def test_flexi_holiday_without_core_hours_2_rh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_2_sh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_2_rhsh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_3_rh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_3_rh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_3_rh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_without_core_hours_3_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_3_without_ts_sh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_3_sh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_without_core_hours_3_rhsh_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_3_rhsh_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_3_rhsh_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.core_hours([{
            "start": "10:00",
            "end": "16:00"
        }])
        mock.timesheet([
            ("2019-01-01 07:00:00", "clock_in"),
            ("2019-01-01 17:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_flexi_holiday_without_core_hours_4_rh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_4_sh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_4_rhsh(self):
        """Test specs:
        1. Flexi
        2. With core hours
        3. timesheet required - True
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_5_rh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_rh_without_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_rh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_rh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_5_sh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_sh_without_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_sh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_sh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_5_rhsh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_rhsh_without_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_rhsh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays True
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_5_rhsh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_6_rh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_6_sh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_6_rhsh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_7_rh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_rh_without_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_rh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_rh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_7_sh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        """
        #  Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_sh_withut_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_sh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_sh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_7_rhsh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_rhsh_without_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_rhsh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_7_rhsh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_8_rh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_8_sh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_8_sh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_8_rhsh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - True
        4. unworked regular holiday - False
        5. unworked special holiday - False
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=False,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_flexi_holiday_without_core_hours_9_rh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_rh_without_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_rh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_rh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_9_sh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        10. attendance should be calculated as REGULAR
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_sh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_sh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_9_rhsh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
            attendance should be calculated as REGUALR
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_rhsh_without_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_rhsh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RH_SH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_9_rhsh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - True
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_10_rh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_10_sh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_10_rhsh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - True
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_11_rh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shif has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_rh_with_ts_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        9. shif has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_rh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        9. shif has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_rh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60


    def test_flexi_holiday_without_core_hours_11_sh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_sh_with_ts_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_sh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_sh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_11_rhsh_with_ts_scenario_1(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_rhsh_with_ts_scenario_1_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        9. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_rhsh_with_ts_scenario_2(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        9. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 17:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_flexi_holiday_without_core_hours_11_rhsh_without_ts(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - True
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_12_rh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_12_sh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_flexi_holiday_without_core_hours_12_rhsh(self):
        """Test specs:
        1. Flexi
        2. Without core hours
        3. timesheet required - False
        4. unworked regular holiday - True
        5. unworked special holiday - True
        6. premium holiday - False
        7. has rendered hours - False
        8. RH+SH
        """
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH_SH)
        mock.shifts([
            {
                "type": "flexi",
                "start_time": "08:00",
                "end_time": "22:00",
                "total_hours": "08:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_fixed_rh_day_rh_entitled_uh(self):
        """Test specs:
        1. Fixed 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_fixed_rh_day_sh_entitled_uh(self):
        """Test specs:
        1. Fixed 9pm-6am
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_fixed_sh_day_sh_entitled_uh(self):
        """Test specs:
        1. Fixed 9pm-6am
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_fixed_sh_day_rh_entitled_uh(self):
        """Test specs:
        1. Fixed 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_fixed_rhsh_day_rh_entitled_uh(self):
        """Test specs:
        1. Fixed 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_fixed_rhsh_day_sh_entitled_uh(self):
        """Test specs:
        1. Fixed 9pm-6am
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_fixed_rhsh_day_rhsh_entitled_uh(self):
        """Test specs:
        1. Fixed 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """

        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH_SH
        )

        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_rh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_rh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_flexi_sh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_flexi_sh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_core_hrs_rh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_core_hrs_rh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_flexi_core_hrs_sh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60

    def test_6079_flexi_core_hrs_sh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_tsnr_rh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_tsnr_rh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_6079_flexi_tsnr_rh_day_sh_entitled_with_timesheet_uh_on_holidays_false(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - false
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
    
    def test_6079_flexi_tsnr_rh_day_sh_entitled_with_timesheet_uh_on_holidays_true(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT) == 7 * 60

    def test_6079_flexi_tsnr_sh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_6079_flexi_tsnr_sh_day_rh_entitled_with_timesheet_uh_on_holidays_false(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
    
    def test_6079_flexi_tsnr_sh_day_rh_entitled_with_timesheet_uh_on_holidays_false_without_ts(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_6079_flexi_tsnr_sh_day_rh_entitled_with_timesheet_uh_on_holidays_true(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_SH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_SH_NT) == 7 * 60

    def test_6079_flexi_tsnr_sh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_tsnr_core_hrs_rh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_flexi_tsnr_core_hrs_rh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_6079_flexi_tsnr_core_hrs_rh_day_sh_entitled_with_timesheet_uh_scenario_1(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - False
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
    
    def test_6079_flexi_tsnr_core_hrs_rh_day_sh_entitled_with_timesheet_uh_scenario_1_no_custom_shift(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - False
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # Attendance should not be calculated if no custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.default_shift()

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60
    

    def test_6079_flexi_tsnr_core_hrs_rh_day_sh_entitled_with_timesheet_uh_scenario_2_with_custom_shift(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
    
    def test_6079_flexi_tsnr_core_hrs_rh_day_sh_entitled_with_timesheet_uh_scenario_2_without_custom_shift(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.default_shift()
        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_6079_flexi_tsnr_core_hrs_rh_day_sh_entitled_with_timesheet_uh_scenario_2_without_custom_shift_rh_pay_false(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - False
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # Attendance should be calculated if custom shift is assigned
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.default_shift()
        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_6079_flexi_tsnr_core_hrs_rh_day_sh_entitled_with_timesheet_uh_scenario_3(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_RH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT) == 7 * 60

    def test_6079_flexi_tsnr_core_hrs_sh_day_rh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60

    def test_6079_flexi_tsnr_core_hrs_sh_day_rh_entitled_uh_scenario_1(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # If customs shift is assigned the attendance should be calculated
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 1 * 60
        assert attendance.get("1").get(constants.ATT_NT) == 7 * 60
    
    def test_6079_flexi_tsnr_core_hrs_sh_day_rh_entitled_uh_scenario_1_no_custom_shift_sh_pay_false(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # If customs shift not assigned then attendance should not be calculated
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        # mock.shifts([
        #     {
        #         "type": "flexi",
        #         "start_time": "21:00",
        #         "end_time": "06:00",
        #         "total_hours": "08:00"
        #     }
        # ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_6079_flexi_tsnr_core_hrs_sh_day_rh_entitled_uh_scenario_1_no_custom_shift_sh_pay_true(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - False
        """
        # If customs shift not assigned then attendance should not be calculated
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])
        mock.default_shift()
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("ds_1").get(constants.ATT_UH) == 8 * 60
    
    def test_6079_flexi_tsnr_core_hrs_sh_day_rh_entitled_uh_scenario_2(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. has rendered hours - False
        7. shift has on_holidays - True
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00",
                "on_holidays": True
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.timesheet([
            ("2020-07-20 21:00:00", "clock_in"),
            ("2020-07-21 06:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_SH) == 1 * 60
        assert attendance.get("1").get(constants.ATT_SH_NT) == 7 * 60

    def test_6079_flexi_tsnr_core_hrs_sh_day_sh_entitled_uh(self):
        """Test specs:
        1. Flexi 9pm-6am
        2. TSNR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. has rendered hours - False
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=False,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "type": "flexi",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "08:00"
            }
        ])

        mock.core_hours([
            {
                "start": "01:00",
                "end": "02:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    def test_6079_sc4_2_fixed_tsr_rh_no_ot_scenario_1(self):
        """Test specs:
        1. Fixed 9-6
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. night differential - True
        7. Overtime pay - False
        8. has rendered hours - True
        9. Day is SH
        10. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "id": 1,
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])


        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "13:00",
                "end": "14:00"
            }
        ])

        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_6079_sc4_2_fixed_tsr_rh_no_ot_scenario_1_without_ts(self):
        """Test specs:
        1. Fixed 9-6
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. night differential - True
        7. Overtime pay - False
        8. has rendered hours - False
        9. Day is SH
        10. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=False,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "id": 1,
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])


        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "13:00",
                "end": "14:00"
            }
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60
    
    def test_6079_sc4_2_fixed_tsr_rh_no_ot_scenario_2(self):
        """Test specs:
        1. Fixed 9-6
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. night differential - True
        7. Overtime pay - False
        8. has rendered hours - True
        9. Day is SH
        10. shift has on_holidays True
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "id": 1,
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])


        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "13:00",
                "end": "14:00"
            }
        ])

        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    def test_6079_sc4_4_fixed_tsr_sh_no_ot_scenario_1(self):
        """Test specs:
        1. Fixed 9-6
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. night differential - True
        7. Overtime pay - False
        8. has rendered hours - True
        9. Day is RH
        10. shift has on_holidays false
        """
        # Attendance should be calculated as REGULAR
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "id": 1,
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])


        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "13:00",
                "end": "14:00"
            }
        ])

        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_REGULAR) == 8 * 60
    
    def test_6079_sc4_4_fixed_tsr_sh_no_ot_scenario_1_without_ts(self):
        """Test specs:
        1. Fixed 9-6
        2. TSR
        3. unworked regular holiday - False
        4. unworked special holiday - True
        5. premium holiday - True
        6. night differential - True
        7. Overtime pay - False
        8. has rendered hours - False
        9. Day is RH
        10. shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "id": 1,
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00"
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])


        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "13:00",
                "end": "14:00"
            }
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_ABSENT) == 8 * 60
    
    def test_6079_sc4_4_fixed_tsr_sh_no_ot_scenario_2(self):
        """Test specs:
        1. Fixed 9-6
        2. TSR
        3. unworked regular holiday - True
        4. unworked special holiday - False
        5. premium holiday - True
        6. night differential - True
        7. Overtime pay - False
        8. has rendered hours - True
        9. Day is RH
        10. shift has on_holidays true
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH,
            overtime=False
        )

        mock.shifts([
            {
                "id": 1,
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_SH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])


        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "13:00",
                "end": "14:00"
            }
        ])

        mock.timesheet([
            ("2020-07-20 09:00:00", "clock_in"),
            ("2020-07-20 18:00:00", "clock_out")
        ])

        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance

        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_nt_no_timesheet_with_continous_holidays(self, restd):
        """
        Test scenario:
        - Current day is holiday
        - No time records
        - Unworked regular holiday true
        - Next day holiday
        - Counter per hour
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, constants.ATT_RH),
            ("2020-07-22", False, None),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 8 * 60

    @mock.patch(mock_is_restday, return_value=False)
    def test_nt_no_timesheet_with_no_continous_holidays(self, restd):
        """
        Test scenario:
        - Current day is holiday
        - No time records
        - Unworked regular holiday true
        - Next day not holiday
        - Counter per hour
        """
        mock = utils.MockHero(
            date="2020-07-20",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "21:00",
                "end_time": "06:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2020-07-20", False, constants.ATT_RH),
            ("2020-07-21", False, None),
            ("2020-07-22", False, None),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "01:00",
                "end": "02:00",
                "is_paid": False
            }
        ])

        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == 3 * 60
        assert attendance.get("1").get(constants.ATT_UNDERTIME) == 5 * 60
    
    def test_custom_rd_plus_holiday_scenario_1(self):
        """
        Test scenario:
        - Current day is holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_2_schedule_on_rd_false(self):
        """
        Test scenario:
        - Current day is holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays true
        - shift has on_rest_day false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_2_schedule_on_rd_true(self):
        """
        Test scenario:
        - Current day is holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays true
        - shift has on_rest_day True
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 8 * 60
    
    def test_custom_rd_plus_holiday_scenario_3_on_holidays_false(self):
        """
        Test scenario:
        - Current day is holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_3_on_holidays_true_on_rd_false(self):
        """
        Test scenario:
        - Current day is holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays true
        - shift has on_rest_day false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_3_on_holidays_true_on_rd_true(self):
        """
        Test scenario:
        - Current day is holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays true
        - shift has on_rest_day True
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 8 * 60
    
    def test_custom_rd_plus_holiday_scenario_4_schedule_on_rd_false(self):
        """
        Test scenario:
        - Current day is special holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked special holiday false
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays false
        - shift has on_rest_day false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_SH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0

    
    def test_custom_rd_plus_holiday_scenario_5_on_rd_false(self):
        """
        Test scenario:
        - Current day is special holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked special holiday false
        - Holiday premium pay true
        - rest day pay true
        - shift has on_holidays true
        - shift has on_rest_day false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_SH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_6(self):
        """
        Test scenario:
        - Current day is special holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay false
        - rest day pay true
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_SH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 8 * 60
    
    def test_custom_rd_plus_holiday_scenario_7(self):
        """
        Test scenario:
        - Current day is special holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked special holiday false
        - Holiday premium pay true
        - rest day pay false
        - shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=False,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_SH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_8_on_rd_false(self):
        """
        Test scenario:
        - Current day is special holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked special holiday false
        - Holiday premium pay true
        - rest day pay false
        - shift has on_holidays true
        - shift has on_restday false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=False,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": False
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_SH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_8_on_rd_true_rd_pay_false(self):
        """
        Test scenario:
        - Current day is special holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked special holiday false
        - Holiday premium pay true
        - rest day pay false
        - shift has on_holidays true
        - shift has on_rest_day true
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=False,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_SH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_8_on_rd_true_rd_pay_true(self):
        """
        Test scenario:
        - Current day is special holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked special holiday false
        - Holiday premium pay true
        - rest day pay True
        - shift has on_holidays true
        - shift has on_rest_day true
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_SH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_SH) == 8 * 60
    
    def test_custom_rd_plus_holiday_scenario_9(self):
        """
        Test scenario:
        - Current day is regular holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay false
        - rest day pay false
        - shift has on_holidays false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH,
            rest_day_pay=False,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_10_schedule_on_rd_false(self):
        """
        Test scenario:
        - Current day is regular holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay false
        - rest day pay false
        - shift has on_holidays true
        - shift has on_rest_day false
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH,
            rest_day_pay=False,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_10_schedule_on_rd_true_rd_pay_false(self):
        """
        Test scenario:
        - Current day is regular holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay false
        - rest day pay false
        - shift has on_holidays true
        - shift has on_rest_day True
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH,
            rest_day_pay=False,
            overtime=False
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 0
    
    def test_custom_rd_plus_holiday_scenario_10_schedule_on_rd_true_rd_pay_true(self):
        """
        Test scenario:
        - Current day is regular holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday true
        - Holiday premium pay false
        - rest day pay True
        - shift has on_holidays true
        - shift has on_rest_day True
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=False,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD) == 8 * 60
    
    def test_custom_rd_plus_holiday_scenario_10_schedule_on_rd_true_rd_pay_true_holiday_permium_pay_true(self):
        """
        Test scenario:
        - Current day is regular holiday
        - Current day is rest day
        - Emplyee has time record
        - Unworked regular holiday True
        - Holiday premium pay True
        - rest day pay True
        - shift has on_holidays true
        - shift has on_rest_day True
        """
        mock = utils.MockHero(
            date="2022-06-06",
            timesheet_required=True,
            differential=True,
            regular_holiday_pay=True,
            special_holiday_pay=False,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH,
            rest_day_pay=True,
            overtime=True
        )
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "total_hours": "09:00",
                "on_holidays": True,
                "on_rest_day": True
            }
        ])

        mock.set_shift_dates([
            ("2022-06-06", True, constants.ATT_RH),
        ])

        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00",
                "is_paid": False
            }
        ])
        mock.timesheet([
            ("2022-06-06 09:00:00", "clock_in"),
            ("2022-06-06 12:00:00", "clock_out"),
            ("2022-06-06 13:00:00", "clock_out"),
            ("2022-06-06 18:00:00", "clock_out"),
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_RD_RH) == 8 * 60

    def test_holidays_sc_fixed_9_sh_with_approved_ot_outside_core_hours_scenario_1(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        # test scenario from ticket (SCENARIO: 1)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7945
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "08:00",
                "end_time": "17:00",
                "on_holidays": True,
            }
        ])
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-01 20:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 08:00:00", "clock_in"),
            ("2019-01-01 22:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_SH) == 8 * 60
        assert attendance.get("1").get(constants.ATT_SH_OT) == 2 * 60

    @mock.patch(mock_is_restday, return_value=False)    
    def test_holidays_sc_fixed_9_sh_with_approved_ot_outside_core_hours_scenario_2(self, restd):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        # test scenario from ticket (SCENARIO: 2)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7945
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=False,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "12:00",
                "end_time": "21:00",
                "on_holidays": True,
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "16:00",
                "end": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 21:00",
                "end_datetime": "2019-01-02 01:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 11:00:00", "clock_in"),
            ("2019-01-02 01:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == 8 * 60
        assert attendance.get("1").get(constants.ATT_RH_OT) == 1 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT_OT) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT_OT) == 1 * 60
    
    @mock.patch(mock_is_restday, return_value=False)
    def test_holidays_sc_fixed_9_sh_with_approved_ot_outside_core_hours_scenario_3(self, restd):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        # test scenario from ticket (SCENARIO: 3)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7945
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "15:00",
                "end_time": "00:00",
                "on_holidays": True,
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "19:00",
                "end": "20:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-02 00:00",
                "end_datetime": "2019-01-02 00:30"
            }
        ])
        mock.timesheet([
            ("2019-01-01 15:30:00", "clock_in"),
            ("2019-01-02 00:30:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(5.5 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT) == 2 * 60
        assert attendance.get("1").get(constants.ATT_NT_OT) == int(0.5 * 60)
        assert attendance.get("1").get(constants.ATT_UH) == int(0.5 * 60)
    
    def test_holidays_sc_fixed_9_sh_with_approved_ot_outside_core_hours_scenario_4(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        # test scenario from ticket (SCENARIO: 4)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7945
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True,
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-01 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == int(8 * 60)

    def test_holidays_sc_fixed_9_sh_with_approved_ot_outside_core_hours_scenario_5(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        # test scenario from ticket (SCENARIO: 5)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7945
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "12:00",
                "end_time": "21:00",
                "on_holidays": True,
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "16:00",
                "end": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 21:00",
                "end_datetime": "2019-01-02 01:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 11:00:00", "clock_in"),
            ("2019-01-02 01:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(8 * 60)
        assert attendance.get("1").get(constants.ATT_RH_OT) == 1 * 60
        assert attendance.get("1").get(constants.ATT_RH_NT_OT) == int(3 * 60)

    def test_holidays_sc_fixed_9_sh_with_approved_ot_outside_core_hours_scenario_6(self):
        """
        1. Timesheet required - False
        2. Unworked regular holiday - True
        3. Unworked special holiday - True
        4. Premium holiday pay - True
        5. Has rendered hours - True
        6. SH
        """
        # test scenario from ticket (SCENARIO: 7)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7945
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_SH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True,
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "16:00",
                "end": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-02 20:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 20:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_SH) == int(8 * 60)
        assert attendance.get("1").get(constants.ATT_SH_OT) == 2 * 60

    def test_holiday_with_scheduled_ot_scenario_1(self):
        # test scenario from ticket (SCENARIO: 1)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8017
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True,
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-02 20:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 09:00:00", "clock_in"),
            ("2019-01-01 18:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(8 * 60)
    
    def test_holiday_with_scheduled_ot_scenario_2(self):
        # test scenario from ticket (SCENARIO: 2)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8017
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "12:00",
                "end_time": "21:00",
                "on_holidays": True,
                "scheduled_overtime": [
                    {
                        "start": "21:00",
                        "end": "04:00"
                    }
                ],
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Carry over"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "16:00",
                "end": "17:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 21:00",
                "end_datetime": "2019-01-02 04:00"
            }
        ])
        mock.timesheet([
            ("2019-01-01 11:00:00", "clock_in"),
            ("2019-01-01 21:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(8 * 60)
    
    def test_holiday_with_scheduled_ot_scenario_3(self):
        # test scenario from ticket (SCENARIO: 3)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8017
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "15:00",
                "end_time": "00:00",
                "on_holidays": True,
                "scheduled_overtime": [
                    {
                        "start": "00:00",
                        "end": "00:30"
                    }
                ],
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "19:00",
                "end": "20:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 00:00",
                "end_datetime": "2019-01-02 00:30"
            }
        ])
        mock.timesheet([
            ("2019-01-01 15:30:00", "clock_in"),
            ("2019-01-02 00:00:00", "clock_out")
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1")
        assert attendance.get("1").get(constants.ATT_RH) == int(5.5 * 60)
        assert attendance.get("1").get(constants.ATT_RH_NT) == int(2 * 60)
        assert attendance.get("1").get(constants.ATT_UH) == int(0.5 * 60)
    
    def test_holiday_with_scheduled_ot_scenario_4(self):
        # test scenario from ticket (SCENARIO: 4)
        # https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/8017
        mock = utils.MockHero(
            date="2019-01-01",
            timesheet_required=True,
            regular_holiday_pay=True,
            special_holiday_pay=True,
            holiday_premium_pay=True,
            holiday_type=constants.ATT_RH)
        mock.shifts([
            {
                "type": "fixed",
                "start_time": "09:00",
                "end_time": "18:00",
                "on_holidays": True,
                "scheduled_overtime": [
                    {
                        "start": "18:00",
                        "end": "20:00"
                    }
                ],
            }
        ])
        mock.night_shift_settings(
            start="22:00",
            end="06:00",
            time_shift="Counter per hour"
        )
        mock.breaks([
            {
                "break_hours": "1:00",
                "start": "12:00",
                "end": "13:00"
            }
        ])
        mock.hours_worked([
            {
                "shift_id": "1",
                "type": "overtime",
                "start_datetime": "2019-01-01 18:00",
                "end_datetime": "2019-01-01 20:00"
            }
        ])
        mock_data = mock.generate()
        calc = calculator.get_calculator(mock_data)
        attendance = calc.attendance
        assert attendance.get("1").get(constants.ATT_UH) == int(8 * 60)
