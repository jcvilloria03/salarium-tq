# -*- coding: utf-8 -*-

from collections import namedtuple
from datetime import date

from .constants import PayFrequency, SSSEmployeeType

MAX_AMOUNT = 4294967295.0

_PayrunInAYear = {
    PayFrequency.ANNUALLY: 1,
    PayFrequency.MONTHLY: 12,
    PayFrequency.SEMI_MONTHLY: 24,
    PayFrequency.FORTNIGHTLY: 26,
    PayFrequency.WEEKLY: 52,
    # there is no DAILY since it will
    # depend on the day_factor
}

_GovtLoanNames = ["SSS", "Pag-ibig", "Gap"]

_MinMaxPaynum = {
    PayFrequency.MONTHLY: (1, 1),
    PayFrequency.SEMI_MONTHLY: (2, 2),
    PayFrequency.FORTNIGHTLY: (2, 3),
    PayFrequency.WEEKLY: (4, 5),
}

"""
NOTE: Format is as follows:
    - <X>GroupHourTypes - this will contain values that <X> is existing, will be used
                            only for grouping purposes NOT payslip output EXCEPT for OT
    - <X>HourTypes      - this will contain values that are considered PURE <X>
                            work (ie NT work hour that is not OT), will be used for payslip
                            output as some needs a -+ entries (ie pure HOLIDAY hour types
                            since employee worked on a pure holiday (ie should have been
                            a regular work day) we need to subtract regular daily rate then
                            add the daily rate with multiplier)
PS:
    - only OT does not have a PURE list since all OT are just additional to the payslip

GROUPING LOGIC:
    OT > NT > RD > HOLIDAY

SOURCE LIST:
    REGULAR
    PAID_LEAVE
    UNPAID_LEAVE
    UNDERTIME
    ABSENT
    UH
    RD
    SH
    RD+SH
    RH
    RD+RH
    RH+SH
    RD+RH+SH
    2RH
    RD+2RH
    OT
    RD+OT
    SH+OT
    RD+SH+OT
    RH+OT
    RD+RH+OT
    RH+SH+OT
    RD+RH+SH+OT
    2RH+OT
    RD+2RH+OT
    NT
    RD+NT
    SH+NT
    RD+SH+NT
    RH+NT
    RD+RH+NT
    RH+SH+NT
    RD+RH+SH+NT
    2RH+NT
    RD+2RH+NT
    NT+OT
    RD+NT+OT
    SH+NT+OT
    RD+SH+NT+OT
    RH+NT+OT
    RD+RH+NT+OT
    RH+SH+NT+OT
    RD+RH+SH+NT+OT
    2RH+NT+OT
    RD+2RH+NT+OT

PAYSLIP OPERATIONS:
    OT          => +
    NT          => +
    RD          => +
    UH          => -+
    HOLIDAYS    => -+
"""
# overtime here means ALL rendered extra work (payslip: +), working during holiday counts
_OvertimeGroupHourTypes = [
    "OT",
    "RD+OT",
    "SH+OT",
    "RD+SH+OT",
    "RH+OT",
    "RD+RH+OT",
    "RH+SH+OT",
    "RD+RH+SH+OT",
    "2RH+OT",
    "RD+2RH+OT",
    "NT+OT",
    "RD+NT+OT",
    "SH+NT+OT",
    "RD+SH+NT+OT",
    "RH+NT+OT",
    "RD+RH+NT+OT",
    "RH+SH+NT+OT",
    "RD+RH+SH+NT+OT",
    "2RH+NT+OT",
    "RD+2RH+NT+OT",
]

_NightDiffGroupHourTypes = [
    "NT",
    "RD+NT",
    "SH+NT",
    "RD+SH+NT",
    "RH+NT",
    "RD+RH+NT",
    "RH+SH+NT",
    "RD+RH+SH+NT",
    "2RH+NT",
    "RD+2RH+NT",
    "NT+OT",
    "RD+NT+OT",
    "SH+NT+OT",
    "RD+SH+NT+OT",
    "RH+NT+OT",
    "RD+RH+NT+OT",
    "RH+SH+NT+OT",
    "RD+RH+SH+NT+OT",
    "2RH+NT+OT",
    "RD+2RH+NT+OT",
]
# night diff here means rendered work that IS NOT OT
# and fell within the NT range (payslip: +)
_NightDiffHourTypes = [
    "NT",
    "RD+NT",
    "SH+NT",
    "RD+SH+NT",
    "RH+NT",
    "RD+RH+NT",
    "RH+SH+NT",
    "RD+RH+SH+NT",
    "2RH+NT",
    "RD+2RH+NT",
]

_RestDayGroupHourTypes = [
    "RD",
    "RD+SH",
    "RD+RH",
    "RD+RH+SH",
    "RD+2RH",
    "RD+OT",
    "RD+SH+OT",
    "RD+RH+OT",
    "RD+RH+SH+OT",
    "RD+2RH+OT",
    "RD+NT",
    "RD+SH+NT",
    "RD+RH+NT",
    "RD+RH+SH+NT",
    "RD+2RH+NT",
    "RD+NT+OT",
    "RD+SH+NT+OT",
    "RD+RH+NT+OT",
    "RD+RH+SH+NT+OT",
    "RD+2RH+NT+OT",
]
# rest day means rendered work during RD that IS NOT OT or NT (payslip: +)
_RestDayHourTypes = ["RD", "RD+SH", "RD+RH", "RD+RH+SH", "RD+2RH"]

# worked holiday
_HolidayGroupHourTypes = [
    "UH",
    "SH",
    "RD+SH",
    "RH",
    "RD+RH",
    "RH+SH",
    "RD+RH+SH",
    "2RH",
    "RD+2RH",
    "SH+OT",
    "RD+SH+OT",
    "RH+OT",
    "RD+RH+OT",
    "RH+SH+OT",
    "RD+RH+SH+OT",
    "2RH+OT",
    "RD+2RH+OT",
    "SH+NT",
    "RD+SH+NT",
    "RH+NT",
    "RD+RH+NT",
    "RH+SH+NT",
    "RD+RH+SH+NT",
    "2RH+NT",
    "RD+2RH+NT",
    "SH+NT+OT",
    "RD+SH+NT+OT",
    "RH+NT+OT",
    "RD+RH+NT+OT",
    "RH+SH+NT+OT",
    "RD+RH+SH+NT+OT",
    "2RH+NT+OT",
    "RD+2RH+NT+OT",
]
_HolidayHourTypes = ["SH", "RH", "RH+SH", "2RH"]

_AllowanceHolidayRegularHourTypes = ["SH", "RH", "RH+SH", "2RH"]

__default_hour_type_multiplier = {
    "REGULAR": 1,
    "PAID_LEAVE": 1,
    "UNPAID_LEAVE": 1,
    "UNDERTIME": 1,
    "ABSENT": 1,
    "UH": 1,
    "RD": 1.30,
    "SH": 1.30,
    "RD+SH": 1.50,
    "RH": 2.0,
    "RD+RH": 2.60,
    "RH+SH": 2.60,
    "RD+RH+SH": 3.0,
    "2RH": 3.0,
    "RD+2RH": 3.90,
    "OT": 1.25,
    "RD+OT": 1.69,
    "SH+OT": 1.69,
    "RD+SH+OT": 1.95,
    "RH+OT": 2.60,
    "RD+RH+OT": 3.38,
    "RH+SH+OT": 3.38,
    "RD+RH+SH+OT": 3.90,
    "2RH+OT": 3.90,
    "RD+2RH+OT": 5.070,
    "NT": 1.10,
    "RD+NT": 1.43,
    "SH+NT": 1.43,
    "RD+SH+NT": 1.65,
    "RH+NT": 2.20,
    "RD+RH+NT": 2.86,
    "RH+SH+NT": 2.86,
    "RD+RH+SH+NT": 3.30,
    "2RH+NT": 3.30,
    "RD+2RH+NT": 4.29,
    "NT+OT": 1.375,
    "RD+NT+OT": 1.859,
    "SH+NT+OT": 1.859,
    "RD+SH+NT+OT": 2.145,
    "RH+NT+OT": 2.86,
    "RD+RH+NT+OT": 3.718,
    "RH+SH+NT+OT": 3.718,
    "RD+RH+SH+NT+OT": 4.29,
    "2RH+NT+OT": 4.29,
    "RD+2RH+NT+OT": 5.577,
}


_HDMFTableEntry = namedtuple(
    "_HDMFTableEntry",
    [
        "effectivity_start_date",
        "effectivity_end_date",
        "min_amount",
        "max_amount",
        "employee_share_multiplier",
        "employer_share_multiplier",
    ],
)

_TaxTableEntry = namedtuple(
    "_TaxTableEntry",
    [
        "pay_frequency",
        "effectivity_start_date",
        "effectivity_end_date",
        "min_amount",
        "max_amount",
        "fixed_amount_deducted",
        "tax_rate",
        "compensation_level_amount",
    ],
)

_SSSTableEntry = namedtuple(
    "_SSSTableEntry",
    [
        "employee_type",
        "effectivity_start_date",
        "effectivity_end_date",
        "min_amount",
        "max_amount",
        "monthly_salary_credit_amount",
        "employee_share_amount",
        "employer_share_amount",
        "employee_compensation_amount",
        "employee_mandatory_provident_fund_amount",
        "employer_mandatory_provident_fund_amount",
    ],
)

_PhilHealthTableEntry = namedtuple(
    "_PhilHealthTableEntry",
    [
        "effectivity_start_date",
        "effectivity_end_date",
        "premium_rate_multiplier",
        "min_employee_share_amount",
        "max_employee_share_amount",
        "min_employer_share_amount",
        "max_employer_share_amount",
    ],
)

# used for HDMF
__d_20000101 = date(year=2000, month=1, day=1)
# used for Tax
__d_20180101 = date(year=2018, month=1, day=1)
# used for SSS/Tax
__d_20140101 = date(year=2014, month=1, day=1)
__d_20190331 = date(year=2019, month=3, day=31)
__d_20190401 = date(year=2019, month=4, day=1)
__d_20201231 = date(year=2020, month=12, day=31)
__d_20210101 = date(year=2021, month=1, day=1)
__d_20221231 = date(year=2022, month=12, day=31)
# used for PhilHealth
__d_20180101 = date(year=2018, month=1, day=1)
__d_20191207 = date(year=2019, month=12, day=7)
__d_20200101 = date(year=2020, month=1, day=1)
__d_20210101 = date(year=2021, month=1, day=1)
__d_20211231 = date(year=2021, month=12, day=31)
__d_20220101 = date(year=2022, month=1, day=1)
__d_20230101 = date(year=2023, month=1, day=1)
__d_20240101 = date(year=2024, month=1, day=1)
__d_20250101 = date(year=2025, month=1, day=1)
__d_20260101 = date(year=2026, month=1, day=1)
# fmt: off

# effectivity_start_date, effectivity_end_date, min_employee_share_amount, max_employee_share_amount, min_employer_share_amount, max_employer_share_amount
__philhealth_table = [
    _PhilHealthTableEntry(__d_20180101, __d_20191207, 0.0275, 137.50, 550, 137.50, 550),
    _PhilHealthTableEntry(__d_20191207, __d_20200101, 0.0275, 137.50, 687.5, 137.50, 687.5),
    _PhilHealthTableEntry(__d_20200101, __d_20211231, 0.03, 150, 900, 150, 900),
    # https://code.salarium.com/salarium/development/t-and-a-manila/issues/6604
    # 2021 rate was put on hold by the govt due to the pandemic
    # _PhilHealthTableEntry(__d_20210101, __d_20220101, 0.035, 175, 1225, 175, 1225),
    _PhilHealthTableEntry(__d_20220101, __d_20240101, 0.04, 200, 1600, 200, 1600),
    _PhilHealthTableEntry(__d_20240101, __d_20250101, 0.045, 225, 2025, 225, 2025),
    _PhilHealthTableEntry(__d_20250101, __d_20260101, 0.05, 250, 2500, 250, 2500),
    _PhilHealthTableEntry(__d_20260101, None, 0.05, 250, 2500, 250, 2500),
]

# min_amount, max_amount, employee_share_multiplier, employer_share_multiplier, effectivity_start_date, effectivity_end_date
__hdmf_table = [
    _HDMFTableEntry(__d_20000101, None, 0.0, 1500.01, 0.01, 0.02,),
    _HDMFTableEntry(__d_20000101, None, 1500.0, MAX_AMOUNT, 0.02, 0.02,)
]

# min_amount, max_amount, pay_frequency, tax_rate, fixed_amount, compensation_level_amount, effectivity_start_date, effectivity_end_date
__regular_tax_table = [
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20180101, __d_20221231, 0.0, 250000.01, 0.0, 0.0, 0.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20180101, __d_20221231, 250000.01, 400000.01, 0.0, 0.2, 250000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20180101, __d_20221231, 400000.01, 800000.01, 30000.0, 0.25, 400000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20180101, __d_20221231, 800000.01, 2000000.01, 130000.0, 0.3, 800000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20180101, __d_20221231, 2000000.01, 8000000.01, 490000.0, 0.32, 2000000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20180101, __d_20221231, 8000000.01, MAX_AMOUNT, 2410000.0, 0.35, 8000000.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20180101, None, 0.0, 20833.01, 0.0, 0.0, 0.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20180101, __d_20221231, 20833.01, 33333.01, 0.0, 0.2, 20833.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20180101, __d_20221231, 33333.01, 66667.01, 2500.0, 0.25, 33333.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20180101, __d_20221231, 66667.01, 166667.01, 10833.33, 0.3, 66667.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20180101, __d_20221231, 166667.01, 666667.01, 40833.33, 0.32, 166667.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20180101, __d_20221231, 666667.01, MAX_AMOUNT, 200833.33, 0.35, 666667.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20180101, None, 0.0, 10417.01, 0.0, 0.0, 0.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20180101, __d_20221231, 10417.01, 16667.01, 0.0, 0.2, 10417.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20180101, __d_20221231, 16667.01, 33333.01, 1250.0, 0.25, 16667.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20180101, __d_20221231, 33333.01, 83333.01, 5416.67, 0.3, 33333.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20180101, __d_20221231, 83333.01, 333333.01, 20416.67, 0.32, 83333.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20180101, __d_20221231, 333333.01, MAX_AMOUNT, 100416.67, 0.35, 333333.0,),
    _TaxTableEntry(PayFrequency.FORTNIGHTLY, __d_20180101, None, 0.0, 10417.01, 0.0, 0.0, 0.0,),
    _TaxTableEntry(PayFrequency.FORTNIGHTLY, __d_20180101, None, 10417.01, 16667.01, 0.0, 0.2, 10417.0,),
    _TaxTableEntry(PayFrequency.FORTNIGHTLY, __d_20180101, None, 16667.01, 33333.01, 1250.0, 0.25, 16667.0,),
    _TaxTableEntry(PayFrequency.FORTNIGHTLY, __d_20180101, None, 33333.01, 83333.01, 5416.67, 0.3, 33333.0,),
    _TaxTableEntry(PayFrequency.FORTNIGHTLY, __d_20180101, None, 83333.01, 333333.01, 20416.67, 0.32, 83333.0,),
    _TaxTableEntry(PayFrequency.FORTNIGHTLY, __d_20180101, None, 333333.01, MAX_AMOUNT, 100416.67, 0.35, 333333.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20180101, None, 0.0, 4808.01, 0.0, 0.0, 0.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20180101, __d_20221231, 4808.01, 7692.01, 0.0, 0.2, 4808.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20180101, __d_20221231, 7692.01, 15385.01, 576.92, 0.25, 7692.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20180101, __d_20221231, 15385.01, 38462.01, 2500.0, 0.3, 15385.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20180101, __d_20221231, 38462.01, 153846.01, 9423.08, 0.32, 38462.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20180101, __d_20221231, 153846.01, MAX_AMOUNT, 46346.15, 0.35, 153846.0,),
    _TaxTableEntry(PayFrequency.DAILY, __d_20180101, None, 0.0, 685.01, 0.0, 0.0, 0.0,),
    _TaxTableEntry(PayFrequency.DAILY, __d_20180101, None, 685.01, 1096.01, 0.0, 0.2, 685.0,),
    _TaxTableEntry(PayFrequency.DAILY, __d_20180101, None, 1096.01, 2192.01, 82.19, 0.25, 1096.0,),
    _TaxTableEntry(PayFrequency.DAILY, __d_20180101, None, 2192.01, 5479.01, 356.16, 0.3, 2192.0,),
    _TaxTableEntry(PayFrequency.DAILY, __d_20180101, None, 5479.01, 21918.01, 1342.47, 0.32, 5479.0,),
    _TaxTableEntry(PayFrequency.DAILY, __d_20180101, None, 21918.01, MAX_AMOUNT, 6602.74, 0.35, 21918.0,),
    # January 1, 2023 (Updated)
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20230101, None, 4808.01, 7691.99, 0.0, 0.15, 4808.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20230101, None, 7692.00, 15384.99, 432.60, 0.2, 7692.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20230101, None, 15385.00, 38461.99, 1971.20, 0.25, 15385.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20230101, None, 38462.00, 153845.99, 7740.45, 0.3, 38462.0,),
    _TaxTableEntry(PayFrequency.WEEKLY, __d_20230101, None, 153846.00, MAX_AMOUNT, 42355.65, 0.35, 153846.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20230101, None, 10417.01, 16666.99, 0.0, 0.15, 10417.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20230101, None, 16667.00, 33332.99, 937.50, 0.2, 16667.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20230101, None, 33333.00, 83332.99, 4270.70, 0.25, 33333.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20230101, None, 83333.00, 333332.99, 16770.70, 0.3, 83333.0,),
    _TaxTableEntry(PayFrequency.SEMI_MONTHLY, __d_20230101, None, 333333.00, MAX_AMOUNT, 91770.70, 0.35, 333333.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20230101, None, 20833.00, 33332.99, 0.0, 0.15, 20833.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20230101, None, 33333.00, 66666.99, 1875.00, 0.2, 33333.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20230101, None, 66667.00, 166666.99, 8541.80, 0.25, 66667.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20230101, None, 166667.00, 666666.99, 33541.80, 0.3, 166667.0,),
    _TaxTableEntry(PayFrequency.MONTHLY, __d_20230101, None, 666667.00, MAX_AMOUNT, 183541.80, 0.35, 666667.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20230101, None, 0.0, 250000.00, 0.0, 0.0, 0.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20230101, None, 250000.01, 400000.0, 0.0, 0.15, 250000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20230101, None, 400000.01, 800000.0, 22500.0, 0.2, 400000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20230101, None, 800000.01, 2000000.0, 102500.0, 0.25, 800000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20230101, None, 2000000.01, 8000000.0, 402500.0, 0.3, 2000000.0,),
    _TaxTableEntry(PayFrequency.ANNUALLY, __d_20230101, None, 8000000.01, MAX_AMOUNT, 2202500.0, 0.35, 8000000.0,),

]

# employee_type, min_amount, max_amount, monthly_salary_credit_amount, employee_share_amount, employer_share_amount, employee_compensation_amount, effectivity_start_date, effectivity_end_date
__sss_table = [
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 0, 1000.00, 0, 0, 0, 0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 1000.00, 1250.00, 1000.00, 36.30, 73.70, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 1250.00, 1750.00, 1500.00, 54.50, 110.50, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 1750.00, 2250.00, 2000.00, 72.70, 147.30, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 2250.00, 2750.00, 2500.00, 90.80, 184.20, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 2750.00, 3250.00, 3000.00, 109.00, 221.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 3250.00, 3750.00, 3500.00, 127.20, 257.80, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 3750.00, 4250.00, 4000.00, 145.30, 294.70, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 4250.00, 4750.00, 4500.00, 163.50, 331.50, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 4750.00, 5250.00, 5000.00, 181.70, 368.30, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 5250.00, 5750.00, 5500.00, 199.80, 405.20, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 5750.00, 6250.00, 6000.00, 218.00, 442.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 6250.00, 6750.00, 6500.00, 236.20, 478.80, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 6750.00, 7250.00, 7000.00, 254.30, 515.70, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 7250.00, 7750.00, 7500.00, 272.50, 552.50, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 7750.00, 8250.00, 8000.00, 290.70, 589.30, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 8250.00, 8750.00, 8500.00, 308.80, 626.20, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 8750.00, 9250.00, 9000.00, 327.00, 663.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 9250.00, 9750.00, 9500.00, 345.20, 699.80, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 9750.00, 10250.00, 10000.00, 363.30, 736.70, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 10250.00, 10750.00, 10500.00, 381.50, 773.50, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 10750.00, 11250.00, 11000.00, 399.70, 810.30, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 11250.00, 11750.00, 11500.00, 417.80, 847.20, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 11750.00, 12250.00, 12000.00, 436.00, 884.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 12250.00, 12750.00, 12500.00, 454.20, 920.80, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 12750.00, 13250.00, 13000.00, 472.30, 957.70, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 13250.00, 13750.00, 13500.00, 490.50, 994.50, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 13750.00, 14250.00, 14000.00, 508.70, 1031.30, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 14250.00, 14750.00, 14500.00, 526.80, 1068.20, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 14750.00, 15250.00, 15000.00, 545.00, 1105.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 15250.00, 15750.00, 15500.00, 563.20, 1141.80, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20140101, __d_20190331, 15750.00, MAX_AMOUNT, 16000.00, 581.30, 1178.70, 30.00, 0, 0,),
    # April 1, 2019
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 0.00, 2250.00, 2000.00, 80.00, 160.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 2250.00, 2750.00, 2500.00, 100.00, 200.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 2750.00, 3250.00, 3000.00, 120.00, 240.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 3250.00, 3750.00, 3500.00, 140.00, 280.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 3750.00, 4250.00, 4000.00, 160.00, 320.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 4250.00, 4750.00, 4500.00, 180.00, 360.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 4750.00, 5250.00, 5000.00, 200.00, 400.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 5250.00, 5750.00, 5500.00, 220.00, 440.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 5750.00, 6250.00, 6000.00, 240.00, 480.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 6250.00, 6750.00, 6500.00, 260.00, 520.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 6750.00, 7250.00, 7000.00, 280.00, 560.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 7250.00, 7750.00, 7500.00, 300.00, 600.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 7750.00, 8250.00, 8000.00, 320.00, 640.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 8250.00, 8750.00, 8500.00, 340.00, 680.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 8750.00, 9250.00, 9000.00, 360.00, 720.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 9250.00, 9750.00, 9500.00, 380.00, 760.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 9750.00, 10250.00, 10000.00, 400.00, 800.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 10250.00, 10750.00, 10500.00, 420.00, 840.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 10750.00, 11250.00, 11000.00, 440.00, 880.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 11250.00, 11750.00, 11500.00, 460.00, 920.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 11750.00, 12250.00, 12000.00, 480.00, 960.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 12250.00, 12750.00, 12500.00, 500.00, 1000.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 12750.00, 13250.00, 13000.00, 520.00, 1040.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 13250.00, 13750.00, 13500.00, 540.00, 1080.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 13750.00, 14250.00, 14000.00, 560.00, 1120.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 14250.00, 14750.00, 14500.00, 580.00, 1160.00, 10.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 14750.00, 15250.00, 15000.00, 600.00, 1200.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 15250.00, 15750.00, 15500.00, 620.00, 1240.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 15750.00, 16250.00, 16000.00, 640.00, 1280.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 16250.00, 16750.00, 16500.00, 660.00, 1320.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 16750.00, 17250.00, 17000.00, 680.00, 1360.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 17250.00, 17750.00, 17500.00, 700.00, 1400.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 17750.00, 18250.00, 18000.00, 720.00, 1440.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 18250.00, 18750.00, 18500.00, 740.00, 1480.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 18750.00, 19250.00, 19000.00, 760.00, 1520.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 19250.00, 19750.00, 19500.00, 780.00, 1560.00, 30.00, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20190401, __d_20201231, 19750.00, MAX_AMOUNT, 20000.00, 800.00, 1600.00, 30.00, 0, 0,),
    # January 1, 2021
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 0.00, 1000.0, 0, 0, 0, 0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 1000.0, 3250.0, 3000.0, 135.0, 255.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 3250.0, 3750.0, 3500.0, 157.5, 297.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 3750.0, 4250.0, 4000.0, 180.0, 340.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 4250.0, 4750.0, 4500.0, 202.5, 382.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 4750.0, 5250.0, 5000.0, 225.0, 425.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 5250.0, 5750.0, 5500.0, 247.5, 467.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 5750.0, 6250.0, 6000.0, 270.0, 510.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 6250.0, 6750.0, 6500.0, 292.5, 552.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 6750.0, 7250.0, 7000.0, 315.0, 595.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 7250.0, 7750.0, 7500.0, 337.5, 637.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 7750.0, 8250.0, 8000.0, 360.0, 680.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 8250.0, 8750.0, 8500.0, 382.5, 722.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 8750.0, 9250.0, 9000.0, 405.0, 765.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 9250.0, 9750.0, 9500.0, 427.5, 807.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 9750.0, 10250.0, 10000.0, 450.0, 850.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 10250.0, 10750.0, 10500.0, 472.5, 892.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 10750.0, 11250.0, 11000.0, 495.0, 935.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 11250.0, 11750.0, 11500.0, 517.5, 977.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 11750.0, 12250.0, 12000.0, 540.0, 1020.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 12250.0, 12750.0, 12500.0, 562.5, 1062.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 12750.0, 13250.0, 13000.0, 585.0, 1105.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 13250.0, 13750.0, 13500.0, 607.5, 1147.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 13750.0, 14250.0, 14000.0, 630.0, 1190.0, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 14250.0, 14750.0, 14500.0, 652.5, 1232.5, 10.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 14750.0, 15250.0, 15000.0, 675.0, 1275.0, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 15250.0, 15750.0, 15500.0, 697.5, 1317.5, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 15750.0, 16250.0, 16000.0, 720.0, 1360.0, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 16250.0, 16750.0, 16500.0, 742.5, 1402.5, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 16750.0, 17250.0, 17000.0, 765.0, 1445.0, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 17250.0, 17750.0, 17500.0, 787.5, 1487.5, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 17750.0, 18250.0, 18000.0, 810.0, 1530.0, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 18250.0, 18750.0, 18500.0, 832.5, 1572.5, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 18750.0, 19250.0, 19000.0, 855.0, 1615.0, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 19250.0, 19750.0, 19500.0, 877.5, 1657.5, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 19750.0, 20250.0, 20000.0, 900.0, 1700.0, 30.0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 20250.0, 20750.0, 20000.0, 900.0, 1700.0, 30.0, 22.5, 42.5,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 20750.0, 21250.0, 20000.0, 900.0, 1700.0, 30.0, 45.0, 85.0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 21250.0, 21750.0, 20000.0, 900.0, 1700.0, 30.0, 67.5, 127.5,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 21750.0, 22250.0, 20000.0, 900.0, 1700.0, 30.0, 90.0, 170.0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 22250.0, 22750.0, 20000.0, 900.0, 1700.0, 30.0, 112.5, 212.5,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 22750.0, 23250.0, 20000.0, 900.0, 1700.0, 30.0, 135.0, 255.0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 23250.0, 23750.0, 20000.0, 900.0, 1700.0, 30.0, 157.5, 297.5,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 23750.0, 24250.0, 20000.0, 900.0, 1700.0, 30.0, 180.0, 340.0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 24250.0, 24750.0, 20000.0, 900.0, 1700.0, 30.0, 202.5, 382.5,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20210101, __d_20221231, 24750.0, MAX_AMOUNT, 20000.0, 900.0, 1700.0, 30.0, 225.0, 425.0,),
    # January 1, 2023
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 0.00, 0.0, 0, 0, 0, 0, 0, 0,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 0.00, 4249.99, 4000.00, 180.00, 380.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 4250.00, 4749.99, 4500.00, 202.50, 427.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 4750.00, 5249.99, 5000.00, 225.00, 475.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 5250.00, 5749.99, 5500.00, 247.50, 522.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 5750.00, 6249.99, 6000.00, 270.00, 570.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 6250.00, 6749.99, 6500.00, 292.50, 617.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 6750.00, 7249.99, 7000.00, 315.00, 665.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 7250.00, 7749.99, 7500.00, 337.50, 712.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 7750.00, 8249.99, 8000.00, 360.00, 760.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 8250.00, 8749.99, 8500.00, 382.50, 807.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 8750.00, 9249.99, 9000.00, 405.00, 855.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 9250.00, 9749.99, 9500.00, 427.50, 902.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 9750.00, 10249.99, 10000.00, 450.00, 950.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 10250.00, 10749.99, 10500.00, 472.50, 997.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 10750.00, 11249.99, 11000.00, 495.00, 1045.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 11250.00, 11749.99, 11500.00, 517.50, 1092.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 11750.00, 12249.99, 12000.00, 540.00, 1140.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 12250.00, 12749.99, 12500.00, 562.50, 1187.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 12750.00, 13249.99, 13000.00, 585.00, 1235.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 13250.00, 13749.99, 13500.00, 607.50, 1282.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 13750.00, 14249.99, 14000.00, 630.00, 1330.00, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 14250.00, 14749.99, 14500.00, 652.50, 1377.50, 10.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 14750.00, 15249.99, 15000.00, 675.00, 1425.00, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 15250.00, 15749.99, 15500.00, 697.50, 1472.50, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 15750.00, 16249.99, 16000.00, 720.00, 1520.00, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 16250.00, 16749.99, 16500.00, 742.50, 1567.50, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 16750.00, 17249.99, 17000.00, 765.00, 1615.00, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 17250.00, 17749.99, 17500.00, 787.50, 1662.50, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 17750.00, 18249.99, 18000.00, 810.00, 1710.00, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 18250.00, 18749.99, 18500.00, 832.50, 1757.50, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 18750.00, 19249.99, 19000.00, 855.00, 1805.00, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 19250.00, 19749.99, 19500.00, 877.50, 1852.50, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 19750.00, 20249.99, 20000.00, 900.00, 1900.00, 30.00, 0.00, 0.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 20250.00, 20749.99, 20500.00, 900.00, 1900.00, 30.00, 22.50, 47.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 20750.00, 21249.99, 21000.00, 900.00, 1900.00, 30.00, 45.00, 95.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 21250.00, 21749.99, 21500.00, 900.00, 1900.00, 30.00, 67.50, 142.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 21750.00, 22249.99, 22000.00, 900.00, 1900.00, 30.00, 90.00, 190.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 22250.00, 22749.99, 22500.00, 900.00, 1900.00, 30.00, 112.50, 237.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 22750.00, 23249.99, 23000.00, 900.00, 1900.00, 30.00, 135.00, 285.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 23250.00, 23749.99, 23500.00, 900.00, 1900.00, 30.00, 157.50, 332.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 23750.00, 24249.99, 24000.00, 900.00, 1900.00, 30.00, 180.00, 380.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 24250.00, 24749.99, 24500.00, 900.00, 1900.00, 30.00, 202.50, 427.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 24750.00, 25249.99, 25000.00, 900.00, 1900.00, 30.00, 225.00, 475.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 25250.00, 25749.99, 25500.00, 900.00, 1900.00, 30.00, 247.50, 522.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 25750.00, 26249.99, 26000.00, 900.00, 1900.00, 30.00, 270.00, 570.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 26250.00, 26749.99, 26500.00, 900.00, 1900.00, 30.00, 292.50, 617.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 26750.00, 27249.99, 27000.00, 900.00, 1900.00, 30.00, 315.00, 665.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 27250.00, 27749.99, 27500.00, 900.00, 1900.00, 30.00, 337.50, 712.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 27750.00, 28249.99, 28000.00, 900.00, 1900.00, 30.00, 360.00, 760.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 28250.00, 28749.99, 28500.00, 900.00, 1900.00, 30.00, 382.50, 807.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 28750.00, 29249.99, 29000.00, 900.00, 1900.00, 30.00, 405.00, 855.00,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 29250.00, 29749.99, 29500.00, 900.00, 1900.00, 30.00, 427.50, 902.50,),
    _SSSTableEntry(SSSEmployeeType.REGULAR, __d_20230101, None, 29750.00, MAX_AMOUNT, 30000.00, 900.00, 1900.00, 30.00, 450.00, 950.00,),
]
# fmt: on
