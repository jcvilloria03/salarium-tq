# -*- coding: utf-8 -*-

from enum import Enum

__all__ = [
    "ContributionEnum",
    "ContributionIncomeType",
    "ContributionSchedule",
    "TaxType",
    "PayFrequency",
    "PayRate",
    "SSSEmployeeType",
]


class BaseEnum(Enum):
    def __repr__(self):
        return self.value


class AdjustmentType(BaseEnum):
    TAX_ADJUSTMENT = "TAX_ADJUSTMENT"
    TAXABLE_INCOME = "TAXABLE_INCOME"
    NON_TAXABLE_INCOME = "NON_TAXABLE_INCOME"
    NON_INCOME = "NON_INCOME"
    PHILHEALTH_EMPLOYEE_CONTRIBUTION = "PHILHEALTH_EMPLOYEE_CONTRIBUTION"
    PHILHEALTH_EMPLOYER_CONTRIBUTION = "PHILHEALTH_EMPLOYER_CONTRIBUTION"


class DeminimisType(BaseEnum):
    REDUCE_TAXABLE = "REDUCE_TAXABLE"
    NON_TAXABLE = "NON_TAXABLE"


class ContributionEnum(BaseEnum):
    SSS = "SSS"
    HDMF = "HDMF"
    PHILHEALTH = "PHILHEALTH"


class ContributionIncomeType(BaseEnum):
    BASIC = "BASIC"
    GROSS = "GROSS"
    FIXED = "FIXED"
    NO_CONTRIBUTION = "NO_CONTRIBUTION"
    GROSS_TAXABLE = "GROSS_TAXABLE"
    NET_BASIC_PAY = "NET_BASIC_PAY"

class ContributionSchedule(BaseEnum):
    FIRST_PAY = "FIRST_PAY"
    LAST_PAY = "LAST_PAY"
    EVERY_PAY = "EVERY_PAY"
    SECOND_PAY = "SECOND_PAY"
    THIRD_PAY = "THIRD_PAY"
    FOURTH_PAY = "FOURTH_PAY"
    FIFTH_PAY = "FIFTH_PAY"


class TaxType(BaseEnum):
    NONE = "NONE"
    REGULAR = "REGULAR"
    CONSULTANT = "CONSULTANT"
    MINIMUM_WAGE = "MINIMUM_WAGE"


class PayFrequency(BaseEnum):
    ANNUALLY = "ANNUALLY"
    MONTHLY = "MONTHLY"
    SEMI_MONTHLY = "SEMI_MONTHLY"
    FORTNIGHTLY = "FORTNIGHTLY"
    WEEKLY = "WEEKLY"
    DAILY = "DAILY"

    # @classmethod
    # def is_valid(cls, value):
    #     return any(value == item.value for item in cls)


class PayRate(BaseEnum):
    YEARLY = "YEARLY"
    MONTHLY = "MONTHLY"
    DAILY = "DAILY"
    HOURLY = "HOURLY"


class SSSEmployeeType(BaseEnum):
    REGULAR = "REGULAR"
    SPECIAL = "SPECIAL"

class FinalPayItemType(BaseEnum):
    ADJUSTMENT = "ADJUSTMENT"
    ALLOWANCE = "ALLOWANCE"
    BONUS = "BONUS"
    COMMISSION = "COMMISSION"
    DEDUCTION = "DEDUCTION"
    CONTRIBUTION = "CONTRIBUTION"
    LOAN = "LOAN"

class SelectedReleasesType(BaseEnum):
    BONUS = "BONUS"

class TransactionType(BaseEnum):
    NONE = "NONE"
    FINAL_NETPAY = "FINAL_NETPAY"
    NETPAY = "NETPAY"