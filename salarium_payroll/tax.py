# -*- coding: utf-8 -*-


from dataclasses import dataclass, field
from datetime import date
from typing import ClassVar, Dict, List

from ._internal_data import __regular_tax_table as _InternalRegularTaxTable
from ._internal_data import _TaxTableEntry
from .constants import PayFrequency, TaxType

__all__ = ["NoMatchingTaxTableEntry", "Tax"]


class NoMatchingTaxTableEntry(RuntimeError):
    pass


@dataclass
class Tax:
    pay_frequency: PayFrequency
    effective_date: date = date.today()
    type: TaxType = TaxType.REGULAR

    __reference_table_cols: ClassVar[List[str]] = [
        "min_amount",
        "max_amount",
        "fixed_amount_deducted",
        "tax_rate",
        "compensation_level_amount",
        # "effectivity_start_date",
    ]
    __ref_tbl: Dict = field(default_factory=dict)

    @property
    def reference_table(self) -> Dict:
        if not self.__ref_tbl:
            filtered = filter(self._filter_table, _InternalRegularTaxTable)

            # https://stackoverflow.com/a/13254290
            tax_table = list(
                {k: getattr(d, k) for k in d._fields if k in Tax.__reference_table_cols}
                for d in filtered
            )
            # NOTE: do we really need to extract effective start and end dates from entries?
            self.__ref_tbl = {
                "pay_frequency": self.pay_frequency.value,
                "tax_type": self.type.value,
                "table": tax_table,
                # "version": self.effective_date.strftime("%Y-%m-%d"),
            }

        return self.__ref_tbl

    def compute_tax_deduction_amount(
        self, taxable_income: float, tax_rate: float = 0.1
    ) -> float:
        if self.type in [TaxType.MINIMUM_WAGE, TaxType.NONE] or taxable_income < 0:
            return 0
        elif self.type is TaxType.CONSULTANT:
            return taxable_income * tax_rate
        else:
            entry = self.get_reference_table_entry(amount=taxable_income)

            amt_to_be_rated = taxable_income - entry["compensation_level_amount"]

            return entry["fixed_amount_deducted"] + (
                amt_to_be_rated * entry["tax_rate"]
            )

    def get_reference_table_entry(self, amount: float) -> _TaxTableEntry:
        for row in self.reference_table["table"]:
            if row["min_amount"] <= amount < row["max_amount"]:
                return row

        raise NoMatchingTaxTableEntry(
            f"Can't find match for amount({amount})"
            f" within effective_date({self.effective_date})"
            f" using table -- {self.reference_table}"
        )

    def _filter_table(self, x: _TaxTableEntry) -> bool:
        # filter pay_frequency and dates between
        return x.pay_frequency is self.pay_frequency and (
            (
                x.effectivity_start_date <= self.effective_date
                and x.effectivity_end_date is None
            )
            or (
                x.effectivity_start_date
                <= self.effective_date
                <= x.effectivity_end_date
            )
        )
