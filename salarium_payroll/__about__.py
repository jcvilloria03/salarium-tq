# -*- coding: utf-8 -*-

__all__ = [
    "__title__",
    "__summary__",
    "__uri__",
    "__version__",
    "__author__",
    "__email__",
    "__license__",
    "__copyright__",
]

__title__ = "salarium-payroll"
__version__ = "1"

__summary__ = (
    "{} is a package which provides payroll related logic"
    " code with no presentation dependencies".format(__title__)
)
__uri__ = "https://code.salarium.com/salarium/code/payroll-domain"

__author__ = "The Salarium developers"
__email__ = "dev@salarium.com"

__license__ = "Salarium License"
__copyright__ = "Copyright 2019-2020 {}".format(__author__)
