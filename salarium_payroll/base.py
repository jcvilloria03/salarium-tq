# -*- coding: utf-8 -*-

from dataclasses import dataclass, field
from datetime import date
from decimal import ROUND_HALF_DOWN, ROUND_HALF_UP, Decimal
from typing import Dict, List, Optional, Tuple

from ._internal_data import _PayrunInAYear
from .constants import (
    ContributionEnum,
    ContributionIncomeType,
    ContributionSchedule,
    PayFrequency,
    PayRate,
    TaxType,
)
from .period import (
    generate_forthnightly,
    generate_monthly,
    generate_semi_monthly,
    generate_weekly,
)

__all__ = [
    "money_round",
    "divide_by_2_unequal",
    "EmployeeContributionBasisInfo",
    "EmployeeDetails",
    "PayrollGroup",
    "RateCalculator",
]


def money_round(x, rounding=ROUND_HALF_UP) -> float:
    return float(Decimal(str(x)).quantize(Decimal(".00"), rounding=rounding))


def divide_unequal(num: float, divisor: int) -> Tuple:
    # all excess cents should be handled on the last payment
    a = money_round(num / divisor, rounding=ROUND_HALF_DOWN)

    if num % divisor == 0:
        return tuple(a for x in range(divisor))
    else:
        t1 = [a for x in range(divisor - 1)]
        last_num = money_round(num - sum(t1))
        test_weird_python_precision = last_num - (divisor / 100)
        # test if python had a weird precision issue
        # ie 372.4 / 5 = 74.47999999999999 which should be just an exact 74.48
        #
        # cannot be solved by this:
        # FIXED 101; max_pay_sched: 5
        #
        if test_weird_python_precision == a:
            a += 0.01
            t1 = [a] * divisor
        else:
            t1.append(last_num)

        return tuple(sorted(t1))


def divide_by_2_unequal(num: float) -> Tuple:
    return divide_unequal(num, 2)


@dataclass(frozen=True)
class EmployeeContributionBasisInfo:
    type: ContributionIncomeType
    amount: float = 0


@dataclass(frozen=True)
class EmployeeDetails:
    contribution_basis: Dict[ContributionEnum, EmployeeContributionBasisInfo]
    date_hired: date
    date_ended: date
    basic_info: Dict[str, str]
    tax_status: str
    tax_type: TaxType = TaxType.REGULAR
    tax_rate: float = 0

    # TODO: add the basic info, for payslip printing
    hrs_per_day: float = 8
    day_factor: float = 261

@dataclass(frozen=True)
class PayrollGroup:
    pay_frequency: PayFrequency
    pay_start_date: date
    pay_end_date: date
    pay_run: date

    attendance_start_date: date
    attendance_end_date: date

    # original_attendance_start_date is the original attendance_start_date returned from the payroll details and is not overwritten
    original_attendance_start_date: date

    hour_type_multiplier: Dict

    cutoff: date
    cutoff_is_eom: bool

    posting_date1: date
    posting_date1_is_eom: bool

    posting_date: date
    contribution_schedules: Dict[ContributionEnum, ContributionSchedule]

    cutoff2: Optional[date] = None
    cutoff2_is_eom: bool = False

    posting_date2: Optional[date] = None
    posting_date2_is_eom: bool = False

    hrs_per_day: float = 261
    day_factor: float = 8
    gap_loan_enforced: bool = False
    gap_loan_min_take_home_pay: float = 0

    compute_overbreak: bool = False
    compute_tardiness: bool = True
    compute_undertime: bool = True
    force_compute_daily: bool = False

    _pay_year_calendar: List[Tuple] = field(init=False, repr=False, compare=False)

    def __post_init__(self):
        # https://docs.python.org/3/library/dataclasses.html#frozen-instances
        self.__init_pay_calendar()
        # run this just to make sure all data passed, matches
        self.pay_schedule

    @property
    def max_pay_schedule(self) -> int:
        max = 0
        for (
            pay_period_start,
            pay_period_end,
            pay_period_posting,
            pay_schedule,
        ) in self._pay_year_calendar:
            if (
                self.posting_date.month == pay_period_posting.month
                and pay_schedule > max
            ):
                max = pay_schedule
        return max

    @property
    def pay_schedule(self) -> int:
        for (
            pay_period_start,
            pay_period_end,
            pay_period_posting,
            pay_schedule,
        ) in self._pay_year_calendar:
            if self.posting_date == pay_period_posting:
                return pay_schedule

        raise RuntimeError(
            f"no matching range for posting_date({self.posting_date}) with config "
            f"pay_end_date ({self.pay_end_date}) and cutoff settings "
            f"{(self.cutoff, self.cutoff2)} definitions based on calendar "
            f"{self._pay_year_calendar}"
        )

    def is_within_pay_period(self, date) -> bool:
        return self.pay_start_date <= date <= self.pay_end_date

    def is_within_attendance_period(self, date) -> bool:
        return self.attendance_start_date <= date <= self.attendance_end_date

    def is_within_original_attendance_period(self, date) -> bool:
        return self.original_attendance_start_date <= date <= self.attendance_end_date

    def __init_pay_calendar(self) -> None:
        calendar: List = []
        if self.pay_frequency is PayFrequency.SEMI_MONTHLY:
            calendar = generate_semi_monthly(
                cutoff1=self.cutoff,
                cutoff1_is_eom=self.cutoff_is_eom,
                cutoff2=self.cutoff2,
                cutoff2_is_eom=self.cutoff2_is_eom,
                posting1=self.posting_date1,
                posting1_is_eom=self.posting_date1_is_eom,
                posting2=self.posting_date2,
                posting2_is_eom=self.posting_date2_is_eom,
                basis_date=self.posting_date,
                get_year_grp=True,
            )
        elif self.pay_frequency is PayFrequency.FORTNIGHTLY:
            calendar = generate_forthnightly(
                cutoff=self.cutoff,
                posting=self.posting_date1,
                basis_date=self.posting_date,
                get_year_grp=True,
            )
        elif self.pay_frequency is PayFrequency.WEEKLY:
            calendar = generate_weekly(
                cutoff=self.cutoff,
                posting=self.posting_date1,
                basis_date=self.posting_date,
                get_year_grp=True,
            )
        elif self.pay_frequency is PayFrequency.MONTHLY:
            calendar = generate_monthly(
                cutoff=self.cutoff,
                cutoff_is_eom=self.cutoff_is_eom,
                posting=self.posting_date1,
                posting_is_eom=self.posting_date1_is_eom,
                basis_date=self.posting_date,
                get_year_grp=True,
            )
        else:
            raise RuntimeError(
                f"PayFrequency {self.pay_frequency} pay_calendar not supported."
            )

        # https://stackoverflow.com/a/54119384
        object.__setattr__(self, "_pay_year_calendar", calendar)

@dataclass(frozen=True)
class FinalPayrollGroup:
    pay_frequency: PayFrequency
    pay_start_date: date
    pay_end_date: date
    pay_run: date

    attendance_start_date: date
    attendance_end_date: date

    hour_type_multiplier: Dict

    cutoff: date
    cutoff_is_eom: bool

    posting_date1: date
    posting_date1_is_eom: bool

    cutoff2: Optional[date] = None
    cutoff2_is_eom: bool = False

    posting_date2: Optional[date] = None
    posting_date2_is_eom: bool = False

    hrs_per_day: float = 261
    day_factor: float = 8
    gap_loan_enforced: bool = False
    gap_loan_min_take_home_pay: float = 0

    posting_date: Optional[date] = None
    contribution_schedules: Dict[ContributionEnum, ContributionSchedule] = None

    compute_overbreak: bool = False
    compute_undertime: bool = True
    compute_tardiness: bool = True

    _pay_year_calendar: List[Tuple] = field(init=False, repr=False, compare=False)

    def __post_init__(self):
        # https://docs.python.org/3/library/dataclasses.html#frozen-instances
        self.__init_pay_calendar()
        # run this just to make sure all data passed, matches

    def get_calendar_by_date_range(self, start, end):
        calendar = []
        for (
            pay_period_start,
            pay_period_end,
            pay_period_posting,
            pay_schedule,
        ) in self._pay_year_calendar:
            if pay_period_start >= start and pay_period_end <= end:
                calendar.append((pay_period_start, pay_period_end))
        return calendar

    @property
    def max_pay_schedule(self) -> int:
        max = 0
        for (
            pay_period_start,
            pay_period_end,
            pay_period_posting,
            pay_schedule,
        ) in self._pay_year_calendar:
            if (pay_schedule > max):
                max = pay_schedule
        return max

    @property
    def pay_schedule(self) -> int:
        for (
            pay_period_start,
            pay_period_end,
            pay_period_posting,
            pay_schedule,
        ) in self._pay_year_calendar:
            if self.posting_date == pay_period_posting:
                return pay_schedule

        raise RuntimeError(
            f"no matching range for posting_date({self.posting_date}) with config "
            f"pay_end_date ({self.pay_end_date}) and "
            f"cutoff {(self.cutoff, self.cutoff2)} definitions"
        )

    def is_within_pay_period(self, date) -> bool:
        return self.pay_start_date <= date <= self.pay_end_date

    def is_within_attendance_period(self, date) -> bool:
        return self.attendance_start_date <= date <= self.attendance_end_date

    def __init_pay_calendar(self) -> None:
        calendar: List = []
        if self.pay_frequency is PayFrequency.SEMI_MONTHLY:
            calendar = generate_semi_monthly(
                cutoff1=self.cutoff,
                cutoff1_is_eom=self.cutoff_is_eom,
                cutoff2=self.cutoff2,
                cutoff2_is_eom=self.cutoff2_is_eom,
                posting1=self.posting_date1,
                posting1_is_eom=self.posting_date1_is_eom,
                posting2=self.posting_date2,
                posting2_is_eom=self.posting_date2_is_eom,
                basis_date=self.attendance_start_date,
                get_year_grp=False,
            )
        elif self.pay_frequency is PayFrequency.FORTNIGHTLY:
            calendar = generate_forthnightly(
                cutoff=self.cutoff,
                posting=self.posting_date1,
                basis_date=self.attendance_start_date,
                get_year_grp=True,
            )
        elif self.pay_frequency is PayFrequency.WEEKLY:
            calendar = generate_weekly(
                cutoff=self.cutoff,
                posting=self.posting_date1,
                basis_date=self.attendance_start_date,
                get_year_grp=True,
            )
        elif self.pay_frequency is PayFrequency.MONTHLY:
            calendar = generate_monthly(
                cutoff=self.cutoff,
                cutoff_is_eom=self.cutoff_is_eom,
                posting=self.posting_date1,
                posting_is_eom=self.posting_date1_is_eom,
                basis_date=self.attendance_start_date,
                get_year_grp=True,
            )
        else:
            raise RuntimeError(
                f"PayFrequency {self.pay_frequency} pay_calendar not supported."
            )

        # https://stackoverflow.com/a/54119384
        object.__setattr__(self, "_pay_year_calendar", calendar)

# Calculator (no longer a converter because it needs to calculate by pay frequency)
# is the ONLY pay object to hold the df and hpd since it can change case to case
@dataclass
class RateCalculator:
    # number of "work" days in a year 260 non leap year
    day_factor: float = 261
    hrs_per_day: float = 8

    def calc_amount_by_pay_frequency(
        self, pay_amount: float, pay_rate: PayRate, pay_frequency: PayFrequency
    ) -> float:
        # amount is dependent on the pay_frequency so to compute
        # we just convert the base pay to its YEARLY rate then
        # convert down from there
        payrun_ina_year = _PayrunInAYear.get(pay_frequency, self.day_factor)

        return (
            self.__convert_to_yearly(pay_amount=pay_amount, pay_rate=pay_rate)
            / payrun_ina_year
        )

    def get_full_conversion_pay_frequency(
        self, pay_amount: float, pay_rate: PayRate
    ) -> Dict:
        # = for debug for now
        d = {}
        for pf in PayFrequency:
            key = "pf_" + pf.value.lower()
            value = self.calc_amount_by_pay_frequency(
                pay_amount=pay_amount, pay_rate=pay_rate, pay_frequency=pf
            )
            d[key] = value

        return d

    def convert_rate_to(
        self, pay_amount: float, pay_rate: PayRate, convert_rate: PayRate
    ) -> float:
        # if same just return ¯\_(ツ)_/¯
        if pay_rate is convert_rate:
            return pay_amount

        yearly_rate_amount = self.__convert_to_yearly(pay_amount, pay_rate)

        # convert to yearly first then down-size from there
        if convert_rate is PayRate.YEARLY:
            return yearly_rate_amount
        elif convert_rate is PayRate.MONTHLY:
            return yearly_rate_amount / 12
        elif convert_rate is PayRate.DAILY:
            return yearly_rate_amount / self.day_factor
        else:
            return (yearly_rate_amount / self.day_factor) / self.hrs_per_day

    def get_full_conversion_rate(self, pay_amount: float, pay_rate: PayRate) -> Dict:
        # convert to yearly first then down-size from there
        yearly_rate_amount = self.__convert_to_yearly(pay_amount, pay_rate)
        monthly_rate_amount = yearly_rate_amount / 12
        daily_rate_amount = yearly_rate_amount / self.day_factor
        hourly_rate_amount = daily_rate_amount / self.hrs_per_day

        return {
            "yearly_rate_amount": yearly_rate_amount,
            "monthly_rate_amount": monthly_rate_amount,
            "daily_rate_amount": daily_rate_amount,
            "hourly_rate_amount": hourly_rate_amount,
        }

    def __convert_to_yearly(self, pay_amount: float, pay_rate: PayRate) -> float:
        if pay_rate is PayRate.YEARLY:
            # ¯\_(ツ)_/¯
            return pay_amount
        elif pay_rate is PayRate.MONTHLY:
            # 12 months in a year *undeniable*
            return pay_amount * 12
        elif pay_rate is PayRate.DAILY:
            # since day_factor is number of "work" days in a year we just multiply
            return pay_amount * self.day_factor
        else:
            # we have to convert to day using hrs_per_day
            # then convert to year using day_factor
            return (pay_amount * self.hrs_per_day) * self.day_factor


@dataclass(frozen=True)
class BasePay:
    amount: float
    pay_rate: PayRate
    effective_date: date
    rate_calculator: RateCalculator = field(repr=False, compare=False)

    @property
    def yearly_rate_amount(self) -> float:
        return self.__get_amount_by_rate(pay_rate_to=PayRate.YEARLY)

    @property
    def monthly_rate_amount(self) -> float:
        return self.__get_amount_by_rate(pay_rate_to=PayRate.MONTHLY)

    @property
    def daily_rate_amount(self) -> float:
        return self.__get_amount_by_rate(pay_rate_to=PayRate.DAILY)

    @property
    def hourly_rate_amount(self) -> float:
        return self.__get_amount_by_rate(pay_rate_to=PayRate.HOURLY)

    @property
    def full_conversion_rate_table(self) -> Dict:
        return self.rate_calculator.get_full_conversion_rate(
            pay_amount=self.amount, pay_rate=self.pay_rate
        )

    @property
    def full_conversion_pay_frequency_table(self) -> Dict:
        # NOTE: for debug for now
        return self.rate_calculator.get_full_conversion_pay_frequency(
            pay_amount=self.amount, pay_rate=self.pay_rate
        )

    def convert_amount_by_pay_frequency(self, pay_frequency=PayFrequency) -> float:
        return self.rate_calculator.calc_amount_by_pay_frequency(
            pay_amount=self.amount, pay_rate=self.pay_rate, pay_frequency=pay_frequency
        )

    def __get_amount_by_rate(self, pay_rate_to: PayRate) -> float:
        # NOTE: this will only be troublesome if the rates are not the same
        return self.rate_calculator.convert_rate_to(
            pay_amount=self.amount, pay_rate=self.pay_rate, convert_rate=pay_rate_to
        )


class NoMatchingHistoricalPay(RuntimeError):
    pass


class NoMatchingHistoricalBasePay(RuntimeError):
    pass


@dataclass
class Pay:
    amount: float
    effective_start_date: date
    effective_end_date: date

    meta: str = ""


@dataclass
class PayHistory:
    records: List[Pay]
    history_start_date: Optional[date] = None
    history_end_date: Optional[date] = None

    date_hired: Optional[date] = None
    date_ended: Optional[date] = None

    autorun_optimize: bool = True

    def __post_init__(self):
        if not self.date_hired:
            self.date_hired = self.history_start_date

        if not self.date_ended:
            self.date_ended = self.history_end_date

        if self.autorun_optimize:
            self.__optimize_records()

    @property
    def has_only_one_record(self) -> bool:
        return len(self.records) == 1

    @property
    def oldest_pay(self) -> Optional[Pay]:
        oldest_pay: Optional[Pay] = None
        for pay in self.records:
            if (
                not oldest_pay
                or pay.effective_start_date < oldest_pay.effective_start_date
            ):
                oldest_pay = pay
        return oldest_pay

    @property
    def latest_pay(self) -> Optional[Pay]:
        latest_pay: Optional[Pay] = None
        for pay in self.records:
            if not latest_pay or pay.effective_end_date > latest_pay.effective_end_date:
                latest_pay = pay
        return latest_pay

    def get_pay_for_the_day(self, day: date) -> Pay:
        return self.__get_nearest_pay(reference_date=day)

    def clone(
        self,
        history_start_date: Optional[date],
        history_end_date: Optional[date],
        date_hired: Optional[date] = None,
        date_ended: Optional[date] = None,
    ) -> "PayHistory":
        return PayHistory(
            records=self.records,
            history_start_date=history_start_date,
            history_end_date=history_end_date,
            date_hired=date_hired,
            date_ended=date_ended,
        )

    def __optimize_records(self) -> None:
        # remove all pays not needed for the history start and end date
        filtered = None
        # take all pay that has an effective_end_date less than equal
        # to the history_end_date or at least contains the history_end_date
        # between its date ranges
        if self.history_end_date:
            filtered = filter(
                lambda pay: pay.effective_end_date <= self.history_end_date
                or pay.effective_start_date
                <= self.history_end_date
                <= pay.effective_end_date,  # noqa: T400
                self.records,
            )

        # take all pay that has a greater or equal effective_start_date
        # to the history_start_date or at least contains the history_start_date
        # between its date ranges
        # basically sandwiching related pay with the history start and end date
        if self.history_start_date:
            to_filter = filtered if filtered else self.records
            filtered = filter(
                lambda pay: pay.effective_start_date >= self.history_start_date
                or pay.effective_start_date
                <= self.history_start_date
                <= pay.effective_end_date,
                to_filter,
            )

        if filtered:
            self.records = list(filtered)

    def __get_nearest_pay(self, reference_date: date) -> Pay:
        # get the closest date from either start or end of a pay and match
        # it to the reference_date; which ever is the first nearest will be chosen
        curMinDeltaDate = None
        nearest_pay = None
        for pay in self.records:
            diffWithStart = abs(reference_date - pay.effective_start_date)
            diffWithEnd = abs(reference_date - pay.effective_end_date)
            minDeltaDate = min(diffWithStart, diffWithEnd)

            if curMinDeltaDate is None or minDeltaDate < curMinDeltaDate:
                curMinDeltaDate = minDeltaDate
                nearest_pay = pay

        if not nearest_pay:
            # should only happen if self.records is empty
            raise NoMatchingHistoricalPay(
                f"There's no matching pay for the nearest reference_date ({reference_date})."
                f"History Pays: {self.records}"
            )

        return nearest_pay

    def __iter__(self):
        for bp in self.records:
            yield bp

    def __len__(self):
        return len(self.records)


@dataclass
class BasePayHistory:
    records: List[BasePay]
    history_start_date: Optional[date] = None
    history_end_date: Optional[date] = None

    date_hired: Optional[date] = None
    date_ended: Optional[date] = None

    autorun_optimize: bool = True

    def __post_init__(self):
        if not self.date_hired:
            self.date_hired = self.history_start_date

        if not self.date_ended:
            self.date_ended = self.history_end_date

        if self.autorun_optimize:
            self.optimize_records()

    @property
    def has_pay_adjustments(self) -> bool:
        return not self.has_only_one_record

    @property
    def has_only_one_record(self) -> bool:
        return len(self.records) == 1

    @property
    def oldest_base_pay(self) -> Optional[BasePay]:
        oldest_base_pay = None
        for bp in self.records:
            if (
                not oldest_base_pay
                or oldest_base_pay.effective_date > bp.effective_date
            ):
                oldest_base_pay = bp
        return oldest_base_pay

    @property
    def latest_base_pay(self) -> Optional[BasePay]:
        latest_base_pay = None
        for bp in self.records:
            if (
                not latest_base_pay
                or bp.effective_date > latest_base_pay.effective_date
            ):
                latest_base_pay = bp
        return latest_base_pay

    def get_pay_for_the_day(self, day: date) -> BasePay:
        return self.__get_nearest_pay(effective_date=day)

    def clone(
        self, start_date: Optional[date], end_date: Optional[date]
    ) -> "BasePayHistory":
        return BasePayHistory(
            records=self.records,
            history_start_date=start_date,
            history_end_date=end_date,
        )

    def optimize_records(self) -> None:
        # remove all base pays not needed for the payroll group start and end date
        filtered = None
        # take all base pay that has an effective_date less than equal
        # to the payroll end_date
        if self.history_end_date:
            filtered = filter(
                lambda bp: bp.effective_date <= self.history_end_date,  # noqa: T400
                self.records,
            )

        # take all base pay that has and effective_date greater than or equal
        # to the nearest base pay to the payroll start_date,
        # basically sandwiching related base pay with the payroll group start and end date
        if self.history_start_date:
            oldest_bp = self.__get_nearest_pay(self.history_start_date)
            to_filter = filtered if filtered else self.records
            filtered = filter(
                lambda bp: bp.effective_date >= oldest_bp.effective_date, to_filter
            )

        if filtered:
            self.records = list(filtered)

    def __get_nearest_pay(self, effective_date: date) -> BasePay:
        # get the base pay with the nearest (less than or equal) to a certain date
        nearest_pay = None
        nearest_dt = None
        for bp in self.records:
            if bp.effective_date <= effective_date and (
                # if there is no candidate yet, and the bp fits (prev check)
                not nearest_pay
                # or there is a much closer adjustment
                or nearest_dt < bp.effective_date
            ):
                nearest_dt = bp.effective_date
                nearest_pay = bp

        if not nearest_pay:
            if effective_date < self.date_hired:  # noqa: T400,T484
                return self.__get_nearest_pay(self.date_hired)  # noqa: T484

            if effective_date > self.date_ended:  # noqa: T400,T484
                return self.__get_nearest_pay(self.date_ended)  # noqa: T484

            raise NoMatchingHistoricalBasePay(
                f"There's no matching salary for the effective_date ({effective_date})."
                f"History Pays: {self.records}"
            )

        return nearest_pay

    def __iter__(self):
        for bp in self.records:
            yield bp

    def __len__(self):
        return len(self.records)
