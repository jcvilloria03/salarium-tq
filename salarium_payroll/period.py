# -*- coding: utf-8 -*-

from datetime import date
from typing import List, Optional, Tuple

from dateutil.relativedelta import relativedelta

one_month, one_day = relativedelta(months=1), relativedelta(days=1)
# https://gist.github.com/waynemoore/1109153
add_to_eom = relativedelta(day=1, months=+1, days=-1)


def combine_dates(month_year_date: date, day_date: date):
    try:
        resulting_date = date(month_year_date.year, month_year_date.month, day_date.day)
    except ValueError:
        resulting_date = (
            date(month_year_date.year, month_year_date.month + 1, 1) - one_day
        )
    return resulting_date


def generate_semi_monthly(
    cutoff1: date,
    cutoff1_is_eom: bool,
    cutoff2: date,
    cutoff2_is_eom: bool,
    posting1: date,
    posting1_is_eom: bool,
    posting2: date,
    posting2_is_eom: bool,
    basis_date: Optional[date] = None,
    get_year_grp: bool = False,
    back_limit: int = 3,
    forward_limit: int = 6,
) -> List:
    global one_month, one_day, add_to_eom

    # print(f"cutoff1         = {cutoff1}")
    # print(f"cutoff1_is_eom  = {cutoff1_is_eom}")
    # print(f"cutoff2         = {cutoff2}")
    # print(f"cutoff2_is_eom  = {cutoff2_is_eom}")
    # print(f"posting1        = {posting1}")
    # print(f"posting1_is_eom = {posting1_is_eom}")
    # print(f"posting2        = {posting2}")
    # print(f"posting2_is_eom = {posting2_is_eom}")
    # print(f"basis_date      = {basis_date}")
    # print(f"get_year_grp    = {get_year_grp}")
    # print(f"back_limit      = {back_limit}")
    # print(f"forward_limit   = {forward_limit}")

    # we are working with the idea that cutoff1 < cutoff2
    if cutoff1 > cutoff2:
        cutoff1, cutoff2 = cutoff2, cutoff1
        cutoff1_is_eom, cutoff2_is_eom = cutoff2_is_eom, cutoff1_is_eom
        posting1, posting2 = posting2, posting1
        posting1_is_eom, posting2_is_eom = posting2_is_eom, posting1_is_eom

    basis_date = basis_date if basis_date else cutoff1

    cut1, cut2, post1, post2 = cutoff1, cutoff2, posting1, posting2
    cut1_post1_gap, cut2_post2_gap = (
        relativedelta(post1, cut1),
        relativedelta(post2, cut2),
    )
    cut1_post1_same_month, cut2_post2_same_month = (
        cut1.month == post1.month,
        cut2.month == post2.month,
    )

    move_months = 0
    basis_cut1_diff = relativedelta(basis_date, cut1)
    if basis_cut1_diff.years != 0 or basis_cut1_diff.months != 0:
        move_months = (12 * basis_cut1_diff.years) + basis_cut1_diff.months

    # move to the basis date then we start from there
    (
        (basis_start1, basis_cut1, basis_post1),
        (basis_start2, basis_cut2, basis_post2),
    ) = __move_semimonthly_dates(
        no_of_months=move_months,
        cut1=cut1,
        cut1_is_eom=cutoff1_is_eom,
        cut2=cut2,
        cut2_is_eom=cutoff2_is_eom,
        post1=post1,
        post1_is_eom=posting1_is_eom,
        post2=post2,
        post2_is_eom=posting2_is_eom,
        cut1_post1_gap=cut1_post1_gap,
        cut2_post2_gap=cut2_post2_gap,
        cut1_post1_same_month=cut1_post1_same_month,
        cut2_post2_same_month=cut2_post2_same_month,
    )
    # print(f"{basis_start1} => {basis_cut1} ({basis_post1})")
    # print(f"{basis_start2} => {basis_cut2} ({basis_post2})")
    basis_post1 = combine_dates(basis_post1, posting1)
    basis_post2 = combine_dates(basis_post2, posting2)

    periods = [
        (basis_start1, basis_cut1, basis_post1),
        (basis_start2, basis_cut2, basis_post2),
    ]

    for x in range(12):
        no_of_months = x + 1
        (
            (new_start1_backward, new_cut1_backward, new_post1_backward),
            (new_start2_backward, new_cut2_backward, new_post2_backward),
        ) = __move_semimonthly_dates(
            no_of_months=no_of_months * -1,
            cut1=basis_cut1,
            cut1_is_eom=cutoff1_is_eom,
            cut2=basis_cut2,
            cut2_is_eom=cutoff2_is_eom,
            post1=basis_post1,
            post1_is_eom=posting1_is_eom,
            post2=basis_post2,
            post2_is_eom=posting2_is_eom,
            cut1_post1_gap=cut1_post1_gap,
            cut2_post2_gap=cut2_post2_gap,
            cut1_post1_same_month=cut1_post1_same_month,
            cut2_post2_same_month=cut2_post2_same_month,
        )

        new_post1_backward = combine_dates(new_post1_backward, posting1)
        new_post2_backward = combine_dates(new_post2_backward, posting2)

        # order is important here
        periods.insert(0, (new_start2_backward, new_cut2_backward, new_post2_backward))
        periods.insert(0, (new_start1_backward, new_cut1_backward, new_post1_backward))
        # print(f"{new_start1_backward} => {new_cut1_backward} ({new_post1_backward})")
        # print(f"{new_start2_backward} => {new_cut2_backward} ({new_post2_backward})")
        (
            (new_start1_forward, new_cut1_forward, new_post1_forward),
            (new_start2_forward, new_cut2_forward, new_post2_forward),
        ) = __move_semimonthly_dates(
            no_of_months=no_of_months,
            cut1=basis_cut1,
            cut1_is_eom=cutoff1_is_eom,
            cut2=basis_cut2,
            cut2_is_eom=cutoff2_is_eom,
            post1=basis_post1,
            post1_is_eom=posting1_is_eom,
            post2=basis_post2,
            post2_is_eom=posting2_is_eom,
            cut1_post1_gap=cut1_post1_gap,
            cut2_post2_gap=cut2_post2_gap,
            cut1_post1_same_month=cut1_post1_same_month,
            cut2_post2_same_month=cut2_post2_same_month,
        )

        new_post1_forward = combine_dates(new_post1_forward, posting1)
        new_post2_forward = combine_dates(new_post2_forward, posting2)

        periods.append((new_start1_forward, new_cut1_forward, new_post1_forward))
        periods.append((new_start2_forward, new_cut2_forward, new_post2_forward))
        # print(f"{new_start1_forward} => {new_cut1_forward} ({new_post1_forward})")
        # print(f"{new_start2_forward} => {new_cut2_forward} ({new_post2_forward})")


    updated_periods = []

    month, pay_ctr = 1, 1
    for start, end, post in periods:
        # reset the pay counter when end_date month is no longer the same
        if month != post.month:
            month, pay_ctr = post.month, 1
        updated_periods.append((start, end, post, pay_ctr))
        pay_ctr += 1
    return updated_periods


def __move_semimonthly_dates(
    no_of_months: int,
    cut1: date,
    cut1_is_eom: bool,
    cut2: date,
    cut2_is_eom: bool,
    post1: date,
    post1_is_eom: bool,
    post2: date,
    post2_is_eom: bool,
    cut1_post1_gap: relativedelta,
    cut2_post2_gap: relativedelta,
    cut1_post1_same_month: bool,
    cut2_post2_same_month: bool,
) -> Tuple:
    """
    this function move all the cutoffs together month by month (taking to
    account end of month settings) after which will base the paired start
    and posting dates for each
    """
    global one_month, one_day, add_to_eom

    interval = one_month * no_of_months
    # print()
    # print(f"  ORIG: {cut1, cut2}")
    # print(f"  {interval}")

    new_cut1, new_cut2 = cut1 + interval, cut2 + interval
    # print(f"  NEW: {new_cut1, new_cut2}")
    if cut1_is_eom:
        new_cut1 += add_to_eom
    if cut2_is_eom:
        new_cut2 += add_to_eom

    new_start1 = new_cut2 + one_day - one_month
    new_start2 = new_cut1 + one_day

    new_post1 = __compute_posting_date(
        cutoff=new_cut1,
        cut_post_gap=cut1_post1_gap,
        posting_is_eom=post1_is_eom,
        cut_post_same_month=cut1_post1_same_month,
    )
    new_post2 = __compute_posting_date(
        cutoff=new_cut2,
        cut_post_gap=cut2_post2_gap,
        posting_is_eom=post2_is_eom,
        cut_post_same_month=cut2_post2_same_month,
    )

    # print(f"  {(new_start1, new_cut1, new_post1)}")
    # print(f"  {(new_start2, new_cut2, new_post2)}")
    return (new_start1, new_cut1, new_post1), (new_start2, new_cut2, new_post2)


def generate_monthly(
    cutoff: date,
    cutoff_is_eom: bool,
    posting: date,
    posting_is_eom: bool,
    basis_date: date = None,
    get_year_grp: bool = False,
    back_limit: int = 6,
    forward_limit: int = 12,
) -> List:
    return __gen_period_for_one_cutoff(
        gap=relativedelta(months=1),
        cutoff=cutoff,
        cutoff_is_eom=cutoff_is_eom,
        posting=posting,
        posting_is_eom=posting_is_eom,
        basis_date=basis_date,
        get_year_grp=get_year_grp,
        back_limit=back_limit,
        forward_limit=forward_limit,
    )


def generate_weekly(
    cutoff: date,
    posting: date,
    basis_date: date = None,
    get_year_grp: bool = False,
    back_limit: int = 6,
    forward_limit: int = 12,
) -> List:
    return __gen_period_for_one_cutoff(
        gap=relativedelta(days=7),
        cutoff=cutoff,
        cutoff_is_eom=False,
        posting=posting,
        posting_is_eom=False,
        basis_date=basis_date,
        get_year_grp=get_year_grp,
        back_limit=back_limit,
        forward_limit=forward_limit,
    )


def generate_biweekly(
    cutoff: date,
    posting: date,
    basis_date: date = None,
    get_year_grp: bool = False,
    back_limit: int = 6,
    forward_limit: int = 12,
) -> List:
    return generate_forthnightly(
        cutoff=cutoff,
        posting=posting,
        basis_date=basis_date,
        get_year_grp=get_year_grp,
        back_limit=back_limit,
        forward_limit=forward_limit,
    )


def generate_forthnightly(
    cutoff: date,
    posting: date,
    basis_date: date = None,
    get_year_grp: bool = False,
    back_limit: int = 6,
    forward_limit: int = 12,
) -> List:
    return __gen_period_for_one_cutoff(
        gap=relativedelta(days=14),
        cutoff=cutoff,
        cutoff_is_eom=False,
        posting=posting,
        posting_is_eom=False,
        basis_date=basis_date,
        get_year_grp=get_year_grp,
        back_limit=back_limit,
        forward_limit=forward_limit,
    )


# NOTE: if the process of slowly moving to match the basis_date is too slow we might need
#   to find a fancy formula, but for now this is easier to track and replicate manually
def __gen_period_for_one_cutoff(
    gap,
    cutoff: date,
    cutoff_is_eom: bool,
    posting: date,
    posting_is_eom: bool,
    basis_date: date = None,
    get_year_grp: bool = False,
    back_limit: int = 6,
    forward_limit: int = 12,
) -> List:
    global add_to_eom

    ref_date = basis_date if basis_date else cutoff

    the_year = ref_date.year
    c1_start, c1_end = cutoff - gap + relativedelta(days=1), cutoff

    cutoff_and_posting_gap = relativedelta(posting, cutoff)

    post_month_same_as_cut = cutoff.month == posting.month

    # slowly move to the nearest cutoff equiv of the ref_date
    new_c1_start, new_c1_end = __get_new_cutoff(ref_date, c1_start, c1_end, gap)

    # NOTE: this just loops the cutoffs on a supplied parameter `gap` interval and adds
    #   to the return array depending on the mode chosen (get_year_grp). Either all
    #   periods within a year OR certain number of periods back and forth wise
    periods = [(new_c1_start, new_c1_end, new_c1_end + cutoff_and_posting_gap)]
    for i in range(max(53, back_limit, forward_limit)):
        interval = gap * (i + 1)
        new_c1_start_forward, new_c1_end_forward = (
            new_c1_start + interval,
            new_c1_end + interval,
        )
        new_c1_start_backward, new_c1_end_backward = (
            new_c1_start - interval,
            new_c1_end - interval,
        )
        # apply end of month boolean for cutoff
        if cutoff_is_eom:
            new_c1_end_forward += add_to_eom
            new_c1_end_backward += add_to_eom

        new_c1_post_forward = __compute_posting_date(
            cutoff=new_c1_end_forward,
            cut_post_gap=cutoff_and_posting_gap,
            posting_is_eom=posting_is_eom,
            cut_post_same_month=post_month_same_as_cut,
        )
        new_c1_post_backward = __compute_posting_date(
            cutoff=new_c1_end_backward,
            cut_post_gap=cutoff_and_posting_gap,
            posting_is_eom=posting_is_eom,
            cut_post_same_month=post_month_same_as_cut,
        )

        if get_year_grp:
            if new_c1_post_backward.year == the_year:
                periods.insert(
                    0,
                    (new_c1_start_backward, new_c1_end_backward, new_c1_post_backward),
                )

            if new_c1_post_forward.year == the_year:
                periods.append(
                    (new_c1_start_forward, new_c1_end_forward, new_c1_post_forward)
                )
        else:
            if i < back_limit:
                periods.insert(
                    0,
                    (new_c1_start_backward, new_c1_end_backward, new_c1_post_backward),
                )

            if i < forward_limit:
                periods.append(
                    (new_c1_start_forward, new_c1_end_forward, new_c1_post_forward)
                )

    if get_year_grp:
        ret_arr = []
        month, pay_ctr = 1, 1
        for start, end, post in periods:
            # reset the pay counter when end_date month is no longer the same
            if month != post.month:
                month, pay_ctr = post.month, 1

            ret_arr.append((start, end, post, pay_ctr))
            pay_ctr += 1
        return ret_arr

    return periods


def __get_new_cutoff(basis_date: date, orig_start, orig_end, gap: int):
    new_start, new_end = orig_start, orig_end
    # print(f"ORIG {orig_start} - {orig_end}")
    # print(f"NEW {new_start} - {new_end}")
    return_start, return_end = new_start, new_end
    i = 0
    while not (new_start <= basis_date <= new_end):
        i += 1
        interval = gap * i
        if orig_start <= basis_date:
            new_start, new_end = orig_start + interval, orig_end + interval
        else:
            new_start, new_end = orig_start - interval, orig_end - interval

        return_start, return_end = new_start, new_end

    return return_start, return_end


def __compute_posting_date(
    cutoff: date,
    cut_post_gap: relativedelta,
    posting_is_eom: bool,
    cut_post_same_month: bool,
) -> date:
    global add_to_eom

    new_post = cutoff + cut_post_gap

    if posting_is_eom:
        if not add_to_eom:
            add_to_eom = relativedelta(day=1, months=+1, days=-1)
        # since posting is computed using the gap between cut and post
        # we need to check if we need to make sure they (cut&post) have
        # the same month
        if cut_post_same_month:
            # since we are already going to eom it's safe to start at day 1
            new_post = date(new_post.year, cutoff.month, 1) + add_to_eom
        else:
            new_post += add_to_eom

    return new_post
