# -*- coding: utf-8 -*-

from dataclasses import dataclass
from datetime import date

from ._internal_data import _GovtLoanNames


@dataclass(frozen=True)
class Loan:
    name: str
    amount: float
    due_date: date

    meta: str = ""

    @property
    def is_government_loan(self) -> bool:
        return self.name in _GovtLoanNames


@dataclass(frozen=True)
class CreditedLoan:
    name: str
    amount: float
    date: date

    meta: str = ""

    @property
    def is_government_loan(self) -> bool:
        return self.name in _GovtLoanNames
