# -*- coding: utf-8 -*-

import json
from datetime import date, datetime, timedelta
from pprint import pprint
from tokenize import String
from typing import Dict, List
from collections import defaultdict

from marshmallow import UnmarshalResult

from ._internal_data import _HolidayHourTypes
from .attendance import Attendance, AttendanceDayRecord, AttendanceHourRecord
from .base import (
    BasePay,
    BasePayHistory,
    EmployeeContributionBasisInfo,
    EmployeeDetails,
    Pay,
    PayHistory,
    PayrollGroup,
    FinalPayrollGroup,
    RateCalculator,
)
from .constants import (
    AdjustmentType,
    ContributionEnum,
    ContributionIncomeType,
    ContributionSchedule,
    DeminimisType,
    PayFrequency,
    PayRate,
    TaxType,
    FinalPayItemType,
    SelectedReleasesType,
)
from .contributions import ContributionBasisRecord, ContributionRecord
from .income import (
    AABCDGenerator,
    Allowance,
    Bonus,
    Commission,
    Deduction,
    DeMinimis,
    DeMinimisRecord,
    OtherIncomeRecord,
    SpecialAABCDGenerator,
    FinalAABCDGenerator,
)
from .loan import CreditedLoan, Loan
from .pay import BasicPay, FinalBasicPay, NetPay, SpecialNetPay, FinalNetPay
from .schemas import (
    AdjustmentSettingsSchema,
    AllowanceSettingsSchema,
    BonusSettingsSchema,
    CommissionSettingsSchema,
    DeductionSettingsSchema,
)


class PayrollCalculator:
    @classmethod
    def calculate(
        cls,
        payroll_group: Dict,
        employee_details: Dict,
        base_pay_history: Dict,
        attendance_record: List,
        prev_contributions: Dict,
        aabcd: Dict,
        uploaded_other_incomes: List,
        uploaded_deductions: List,
        loans_to_credit: List,
        loan_amortizations: List,
        deminimis: Dict = None,
        annualize: bool = False,
        annual_earnings: Dict = {},
        leave_credits: Dict = {},
    ):
        # Joe
        deminimis = (
            {"reduceToTaxableIncome": [], "addAsNontaxableIncome": []}
            if deminimis is None
            else deminimis
        )

        annual_withholding_tax = annual_earnings.get("withholdingTax", 0)
        annual_taxable_income = annual_earnings.get("taxableIncome", 0)
        annual_breakdown = annual_earnings.get("breakdown", {})
        annual_breakdown_ytd = annual_breakdown.get("ytd", {})
        annual_breakdown_ytd_contrib = annual_breakdown_ytd.get("contributions", 0)
        annual_breakdown_current = annual_breakdown.get("currentEmployer", {})
        annual_breakdown_current_contrib = annual_breakdown_current.get(
            "contributions", 0
        )
        annual_contributions = (
            annual_breakdown_ytd_contrib + annual_breakdown_current_contrib
        )

        pg = PayrollCalculator._load_payroll_group(payroll_group, employee_details.get('basic_info', {}).get('dateHired', None))
        emp = PayrollCalculator._load_employee_details(employee_details, pg)
        rate_calcu = RateCalculator(
            day_factor=emp.day_factor, hrs_per_day=emp.hrs_per_day
        )

        att = PayrollCalculator._load_attendance_record(attendance_record, pg)
        bph = PayrollCalculator._load_base_pay_history(
            base_pay_history, rate_calcu, pg, emp
        )
        aabcd_param = {} if not aabcd else aabcd
        basic_pay = BasicPay(
            payroll_group=pg,
            attendance=att,
            base_pay_history=bph,
            leave_credits=leave_credits,
        )


        aabcd_generator = PayrollCalculator._load_aabcd(
            aabcd_param, emp, rate_calcu, pg, att, basic_pay
        )

        filtered_deminimies = aabcd_generator.filter_deminimies_from_allowances()
        att_ratios = aabcd_generator._compute_attendance_ratio()
        for demini in filtered_deminimies:
            if demini.get('tax_option') == "reduce_to_taxable_income":
                deminimis['reduceToTaxableIncome'].append(
                    {
                        "name": demini.get('name'),
                        "amount": aabcd_generator._get_allowance_amount(demini, att_ratios)[1],
                        "type": "REDUCE_TAXABLE"
                    }
                )
            elif demini.get('tax_option') == "fully_non_taxable":
                deminimis['addAsNontaxableIncome'].append(
                    {
                        "name": demini.get('name'),
                        "amount": aabcd_generator._get_allowance_amount(demini, att_ratios)[1],
                        "type": "NON_TAXABLE"
                    }
                )

        pcb = PayrollCalculator._load_previous_contributions(prev_contributions)
        loa = PayrollCalculator._load_loan_amortizations(loan_amortizations)
        loc = PayrollCalculator._load_loans_to_credit(loans_to_credit)
        dms = PayrollCalculator._load_deminimis(deminimis)

        upl_oi = PayrollCalculator._load_other_incomes(uploaded_other_incomes)
        upl_ded = PayrollCalculator._load_deductions(uploaded_deductions)

        oi_list = (
            upl_oi
            + aabcd_generator.generate_allowances()
            + aabcd_generator.generate_bonuses()
            + aabcd_generator.generate_commissions()
        )
        ded_list = upl_ded + aabcd_generator.generate_deductions()
        net_pay = NetPay(
            basic_pay=basic_pay,
            employee_details=emp,
            prev_contrib=pcb,
            other_incomes=OtherIncomeRecord(records=oi_list),
            adjustments=aabcd_generator.generate_adjustments(),
            deductions=ded_list,
            loan_amortizations=loa,
            loans_to_credit=loc,
            deminimis=dms,
            annualize=annualize,
            annual_withholding_tax=annual_withholding_tax,
            annual_taxable_income=annual_taxable_income,
            annual_contributions=annual_contributions,
        )

        payroll_breakdown = net_pay.breakdown
        payroll_breakdown[
            "payroll_items"
        ] = PayrollCalculator._create_summary_from_breakdown(net_pay)

        return {
            "input": {
                "payroll_group": payroll_group,
                "employee_details": employee_details,
                "base_pay_history": base_pay_history,
                "attendance_record": attendance_record,
                "prev_contributions": prev_contributions,
                "aabcd": aabcd,
                "uploaded_other_incomes": uploaded_other_incomes,
                "uploaded_deductions": uploaded_deductions,
                "loans_to_credit": loans_to_credit,
                "loan_amortizations": loan_amortizations,
                "deminimis": deminimis,
                "annualize": annualize,
                "annual_earnings": annual_earnings,
            },
            "output": PayrollCalculator._breakdown_to_basic_data(payroll_breakdown),
        }

    @classmethod
    def _create_summary_from_breakdown(cls, net_pay: NetPay) -> Dict:
        breakdown = net_pay.breakdown

        att_keys = [
            "absent",
            "holiday_work",
            "night_differential",
            "overtime",
            "rest_day_work",
            "undertime",
            "tardy",
            "overbreak",
            "unworked_holiday",
        ]
        att_summary: Dict = {}
        leave_keys = ["paid_leave", "unpaid_leave"]
        leave_summary: Dict = {}
        for key, bp_breakdown in breakdown["basic_pay"].items():
            if key in att_keys:
                att_summary = {**att_summary, **bp_breakdown["summary"]}
            elif key in leave_keys:
                leave_summary = {**leave_summary, **bp_breakdown["summary"]}

        total_worked_hours = net_pay.basic_pay.attendance.total_scheduled_hours

        deduction_key = [
            "UNDERTIME",
            "ABSENT",
            "UNPAID_LEAVE",
            "UH",
            "TARDY",
            "OVERBREAK",
        ]
        skip_key = [
            "PAID_LEAVE",
            "NT",
            "SH+NT",
            "RH+NT",
            "RH+SH+NT",
            "2RH+NT",
        ] + _HolidayHourTypes
        full_summary = {**att_summary, **leave_summary}
        for hr_key, hr_summary in full_summary.items():
            if hr_key in deduction_key:
                total_worked_hours -= hr_summary["total_hours"]
            elif hr_key in skip_key:
                # ignore since pure NT is already supposed to be rendered work together
                # with the scheduled hours
                continue
            else:
                total_worked_hours += hr_summary["total_hours"]

        lbp = breakdown["basic_pay"]["full_pay"]["latest_base_pay"]
        contrib = net_pay.breakdown["contributions"]

        return {
            "attendance": att_summary,
            "adjustments": breakdown["adjustments"],
            "leaves": leave_summary,
            "deductions": breakdown["deductions"],
            "other_incomes": breakdown["other_incomes"],
            "loan_amortizations": breakdown["loan_amortizations"],
            "loans_to_credit": breakdown["loans_to_credit"],
            "deminimis": breakdown["deminimis"],
            "base_pay": lbp.amount,
            "base_pay_unit": lbp.pay_rate.value,
            "monthly_income": lbp.full_conversion_rate_table["monthly_rate_amount"],
            "daily_rate": lbp.full_conversion_rate_table["daily_rate_amount"],
            "hourly_rate": lbp.full_conversion_rate_table["hourly_rate_amount"],
            "full_pay": net_pay.basic_pay.full_pay_amount,
            "gross_basic": net_pay.basic_pay.gross_basic_amount,
            "basic_pay": net_pay.basic_pay.basic_pay_amount,
            "taxable_income": net_pay.taxable_income_amount,
            "total_reduce_taxable_deminimis_amount": net_pay.total_reduce_taxable_deminimis_amount,
            "total_non_taxable_deminimis_amount": net_pay.total_non_taxable_deminimis_amount,
            "gross": net_pay.gross_income_amount,
            "withholding_tax": net_pay.tax_amount,
            "tax_amount_due": net_pay.tax_amount_due,
            "total_deduction": net_pay.total_gross_income_deduction_amount,
            "net_pay": net_pay.amount,
            "tax_status": net_pay.employee_details.tax_status,
            "tax_type": net_pay.employee_details.tax_type.value,
            "tax_rate": net_pay.employee_details.tax_rate,
            "sss_employee": contrib[ContributionEnum.SSS]["ee_share"],
            "sss_employee_provident_fund": contrib[ContributionEnum.SSS][
                "ee_provident_fund"
            ],
            "hdmf_employee": contrib[ContributionEnum.HDMF]["emp_income_deduction"],
            "philhealth_employee": contrib[ContributionEnum.PHILHEALTH][
                "emp_income_deduction"
            ],
            "total_philhealth_employee_contribution_adjustment": net_pay.total_philhealth_employee_contribution_adjustments_amount,
            "total_mandatory_employee": net_pay.total_contributions_income_deduction_amount,
            "sss_employer": contrib[ContributionEnum.SSS]["er_share"],
            "sss_ec_employer": contrib[ContributionEnum.SSS]["ec_share"],
            "sss_employer_provident_fund": contrib[ContributionEnum.SSS][
                "er_provident_fund"
            ],
            "hdmf_employer": contrib[ContributionEnum.HDMF]["er_share"],
            "philhealth_employer": contrib[ContributionEnum.PHILHEALTH]["er_share"],
            "total_philhealth_employer_contribution_adjustment": net_pay.total_philhealth_employer_contribution_adjustments_amount,
            "total_mandatory_employer": net_pay.total_contributions_of_employer_amount,
            "total_scheduled_hours": net_pay.basic_pay.attendance.total_scheduled_hours,
            "total_worked_hours": total_worked_hours,
            "gross_taxable": net_pay.gross_income_taxable_amount,
        }

    @classmethod
    def _load_aabcd(
        cls,
        aabcd_dict,
        employee_details: EmployeeDetails,
        rate_calcu: RateCalculator,
        payroll_group: PayrollGroup,
        attendance: Attendance,
        basic_pay: BasicPay,
    ) -> AABCDGenerator:
        aabcd_schemas = {
            "adjustment": AdjustmentSettingsSchema(),
            "allowance": AllowanceSettingsSchema(),
            "bonus": BonusSettingsSchema(),
            "commission": CommissionSettingsSchema(),
            "deduction": DeductionSettingsSchema(),
        }

        base_pay_history: BasePayHistory = BasePayHistory(records=[])
        pay_history = {
            "grossIncome": PayHistory(records=[]),
            "grossBasic": PayHistory(records=[]),
            "basicPay": PayHistory(records=[]),
        }

        aabcd_arr = {}
        for key in aabcd_dict:
            if "salaryBasis" == key:
                for basis in aabcd_dict["salaryBasis"].keys():
                    if "basePay" == basis:
                        records: List[BasePay] = [
                            BasePay(
                                amount=float(pay["attributes"]["amount"]),
                                pay_rate=PayRate(pay["attributes"]["unit"]),
                                effective_date=datetime.strptime(
                                    pay["attributes"]["effectiveDate"], "%Y-%m-%d"
                                ).date(),
                                rate_calculator=rate_calcu,
                            )
                            for pay in aabcd_dict["salaryBasis"]["basePay"]
                        ]
                        base_pay_history = BasePayHistory(records=records)
                    else:
                        pay_history_records = [
                            Pay(
                                amount=float(pay["amount"]),
                                effective_start_date=datetime.strptime(
                                    pay["startDate"], "%Y-%m-%d"
                                ).date(),
                                effective_end_date=datetime.strptime(
                                    pay["endDate"], "%Y-%m-%d"
                                ).date(),
                                meta=pay["meta"],
                            )
                            for pay in aabcd_dict["salaryBasis"][basis]
                        ]
                        # just add the current payroll grossBasic and basicPay
                        # then let the coverage filter in aabcd calc
                        if basis in ["grossBasic", "basicPay"]:
                            amount_from_bp = (
                                basic_pay.gross_basic_amount
                                if basis == "grossBasic"
                                else basic_pay.basic_pay_amount
                            )
                            pay_history_records.append(
                                Pay(
                                    amount=amount_from_bp,
                                    effective_start_date=payroll_group.pay_start_date,
                                    effective_end_date=payroll_group.pay_end_date,
                                    meta={"payroll_id": "current"},
                                )
                            )

                        pay_history[basis] = PayHistory(records=pay_history_records)
            else:
                aabcd_arr[key] = list(
                    map(
                        lambda x: PayrollCalculator._load_via_schema(
                            aabcd_schemas[key], x
                        ),
                        aabcd_dict[key],
                    )
                )

        return AABCDGenerator(
            payroll_group=payroll_group,
            attendance=attendance,
            employee_details=employee_details,
            base_pay_history=base_pay_history,
            basic_pay_history=pay_history["basicPay"],
            gross_basic_history=pay_history["grossBasic"],
            gross_income_history=pay_history["grossIncome"],
            adjustment_settings=aabcd_arr.get("adjustment", []),
            allowance_settings=aabcd_arr.get("allowance", []),
            bonus_settings=aabcd_arr.get("bonus", []),
            commission_settings=aabcd_arr.get("commission", []),
            deduction_settings=aabcd_arr.get("deduction", []),
        )

    @classmethod
    def _load_via_schema(cls, schema, data_to_load):
        loaded_data = schema.load(data_to_load)
        # for marshmallow version 2
        if isinstance(loaded_data, UnmarshalResult):
            return loaded_data.data
        return loaded_data

    @classmethod
    def _load_deminimis(cls, dms_dict) -> DeMinimisRecord:
        dms_list = []
        for dms in dms_dict["reduceToTaxableIncome"]:
            dms_list.append(
                DeMinimis(
                    name=dms["name"],
                    amount=float(dms["amount"]),
                    type=DeminimisType(dms["type"]),
                )
            )

        for dms in dms_dict["addAsNontaxableIncome"]:
            dms_list.append(
                DeMinimis(
                    name=dms["name"],
                    amount=float(dms["amount"]),
                    type=DeminimisType(dms["type"]),
                )
            )

        return DeMinimisRecord(records=dms_list)

    @classmethod
    def _load_loans_to_credit(cls, lon_list):
        ret_arr = []
        for lon in lon_list:
            meta = json.dumps(
                {k: lon[k] for k in ["id", "typeID", "subtype", "paymentScheme"]}
            )
            ret_arr.append(
                CreditedLoan(
                    name=lon["loanName"],
                    amount=float(lon["totalAmount"]),
                    date=cls.___str_to_date(lon["createdDate"]),
                    meta=meta,
                )
            )

        return ret_arr

    @classmethod
    def _load_loan_amortizations(cls, lon_list):
        ret_arr = []
        for lon in lon_list:
            meta = json.dumps(
                {
                    k: lon[k]
                    for k in [
                        "id",
                        "loanID",
                        "subtype",
                        "employerRemarks",
                        "principalAmount",
                        "referenceNumber",
                    ]
                }
            )
            ret_arr.append(
                Loan(
                    name=lon["loanName"],
                    amount=float(lon["amountDue"]),
                    due_date=cls.___str_to_date(lon["dueDate"]),
                    meta=meta,
                )
            )

        return ret_arr

    @classmethod
    def _load_deductions(cls, ded_list):
        """ Load Uploaded Deductions """
        return [
            Deduction(
                type=ded["type"], amount=ded["amount"], meta={"is_uploaded": True}
            )
            for ded in ded_list
        ]

    @classmethod
    def _load_other_incomes(cls, oi_list):
        """ Load Uploaded Allowances, Bonuses, and Commissions """
        records: List = []
        vtable = {"ALLOWANCES": Allowance, "BONUSES": Bonus, "COMMISSIONS": Commission}
        for n in vtable:
            for oi in oi_list[n]:
                obj = vtable[n](
                    type=oi["type"],
                    amount=oi["amount"],
                    is_taxable=oi["taxable"],
                    meta={"is_uploaded": True},
                )
                records.append(obj)
        return records

    @classmethod
    def _breakdown_to_basic_data(cls, breakdown):
        # convert dates
        bp_breakdown = breakdown["basic_pay"]
        for x in [
            "absent",
            "holiday_work",
            "night_differential",
            "overtime",
            "paid_leave",
            "rest_day_work",
            "undertime",
            "tardy",
            "overbreak",
            "unpaid_leave",
            "unworked_holiday",
        ]:
            for hr_att in bp_breakdown[x]["records"]:
                hr_att["date"] = hr_att["date"].strftime("%Y-%m-%d")

        # convert base pay
        new_rec: List = [
            {
                "amount": x.amount,
                "pay_rate": x.pay_rate.value,
                "effective_date": x.effective_date.strftime("%Y-%m-%d"),
                "conversion_rates": x.full_conversion_rate_table,
            }
            for x in bp_breakdown["full_pay"]["records"]
        ]
        bp_breakdown["full_pay"]["records"] = new_rec
        lbp = bp_breakdown["full_pay"]["latest_base_pay"]
        bp_breakdown["full_pay"]["latest_base_pay"] = {
            "amount": lbp.amount,
            "pay_rate": lbp.pay_rate.value,
            "effective_date": lbp.effective_date.strftime("%Y-%m-%d"),
            "conversion_rates": lbp.full_conversion_rate_table,
        }

        # convert contribution records
        contrib_breakdown = breakdown["contributions"]
        for x in ContributionEnum:
            new_rec = []
            for rec in contrib_breakdown[x]["records"].basis_records:
                new_rec.append(
                    {
                        "basis_amount": rec.basis_amount,
                        "basis_type": rec.basis_type.value,
                        "deduction_schedule": rec.deduction_schedule.value,
                        "employee_income_deduction": rec.employee_income_deduction,
                        "employee_share_amount": rec.employee_share_amount,
                        "employer_share_amount": rec.employer_share_amount,
                        "employee_provident_fund_amount": rec.employee_provident_fund_amount,
                        "employer_provident_fund_amount": rec.employer_provident_fund_amount,
                        "max_pay_schedule": rec.max_pay_schedule,
                        "pay_schedule": rec.pay_schedule,
                        "current_gross_income": rec.current_gross_income,
                        "employee_compensation_amount": rec.employee_compensation_amount,
                    }
                )
            contrib_breakdown[x]["records"] = new_rec
            # convert to string the contrib keys
            contrib_breakdown[x.value] = contrib_breakdown[x]
            contrib_breakdown.pop(x)

        # convert tax
        tax_breakdown = breakdown["tax"]
        tax_breakdown["tax_type"] = tax_breakdown["tax_type"].value

        # convert other_incomes
        oi_breakdown = breakdown["other_incomes"]
        for l in ["ALLOWANCE", "BONUS", "COMMISSION"]:
            for t in ["taxable", "non_taxable"]:
                new_rec: List = []
                for oi in oi_breakdown[l][t]:
                    new_rec.append(
                        {"type": oi.type, "amount": oi.amount, "meta": oi.meta}
                    )

                oi_breakdown[l][t] = new_rec

        # convert adjustments
        new_rec: List = []
        for adj_type, adj_sub_list in breakdown["adjustments"].items():
            new_rec: List = []
            for adj in adj_sub_list:
                new_rec.append(
                    {
                        "type": adj.type.value,
                        "amount": adj.amount,
                        "reason": adj.reason,
                        "meta": adj.meta,
                    }
                )
            breakdown["adjustments"][adj_type] = new_rec

        # convert deductions
        new_rec: List = [
            {"type": ded.type, "amount": ded.amount, "meta": ded.meta}
            for ded in breakdown["deductions"]
        ]
        # essentially the same records
        breakdown["deductions"] = new_rec
        breakdown["payroll_items"]["deductions"] = new_rec

        # convert loans_to_credit
        new_rec: List = [
            {
                "type": lon.name,
                "amount": lon.amount,
                "date": lon.date.strftime("%Y-%m-%d"),
                "meta": lon.meta,
            }
            for lon in breakdown["loans_to_credit"]
        ]
        # essentially the same records
        breakdown["loans_to_credit"] = new_rec
        breakdown["payroll_items"]["loans_to_credit"] = new_rec

        # convert loan_amortizations
        for lon_type, lon_sub_list in breakdown["loan_amortizations"].items():
            new_rec: List = []
            for lon in lon_sub_list:
                new_rec.append(
                    {
                        "type": lon.name,
                        "amount": lon.amount,
                        "due_date": lon.due_date.strftime("%Y-%m-%d"),
                        "meta": lon.meta,
                    }
                )
            breakdown["loan_amortizations"][lon_type] = new_rec

        # convert deminimis
        for dms_type, dms_sub_list in breakdown["deminimis"].items():
            new_rec: List = []
            for dms in dms_sub_list:
                new_rec.append(
                    {
                        "name": dms.name,
                        "amount": dms.amount,
                        "type": dms.type.value,
                        "meta": dms.meta,
                    }
                )
            breakdown["deminimis"][dms_type] = new_rec

        # convert adjustments
        adj_breakdown = breakdown["adjustments"]
        for adj in AdjustmentType:
            adj_breakdown[adj.value] = adj_breakdown[adj]
            adj_breakdown.pop(adj)

        # convert employee_details
        emp_details = breakdown["employee_details"]
        emp_cbasis = {}
        for x in ContributionEnum:
            emp_cbasis[x.value] = {
                "type": emp_details.contribution_basis[x].type.value,
                "amount": emp_details.contribution_basis[x].amount,
            }
        emp = {
            "basic_info": emp_details.basic_info,
            "tax_status": emp_details.tax_status,
            "tax_type": emp_details.tax_type.value,
            "hrs_per_day": emp_details.hrs_per_day,
            "day_factor": emp_details.day_factor,
            "contribution_basis": emp_cbasis,
        }
        breakdown["employee_details"] = emp

        return breakdown

    """ PreviousContributionRecord = {
        "SSS": [
            {
                basis_amount: float,
                basis_type: "BASIC"|"GROSS"|"FIXED",
                deduction_schedule: "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
                employee_income_deduction: float,
                employee_share_amount: float,
                employer_share_amount: float,
                max_pay_schedule: int,
                pay_schedule: int,
                employee_compensation_amount: float,
            }
        ],
        "HDMF": [
            {
                basis_amount: float,
                basis_type: "BASIC"|"GROSS"|"FIXED",
                deduction_schedule: "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
                employee_income_deduction: float,
                employee_share_amount: float,
                employer_share_amount: float,
                max_pay_schedule: int,
                pay_schedule: int,
                employee_compensation_amount: float,
            }
        ],
        "PHILHEALTH": [
            {
                basis_amount: float,
                basis_type: "BASIC"|"GROSS"|"FIXED",
                deduction_schedule: "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
                employee_income_deduction: float,
                employee_share_amount: float,
                employer_share_amount: float,
                max_pay_schedule: int,
                pay_schedule: int,
                employee_compensation_amount: float,
            }
        ],
    }
    """

    @classmethod
    def _load_previous_contributions(cls, parr: Dict) -> Dict[str, ContributionRecord]:
        record = {}
        for c in ContributionEnum:
            rec_arr = []
            for c_rec in parr[c.value]:
                rec_arr.append(
                    ContributionBasisRecord(
                        basis_amount=float(c_rec["basis_amount"]),
                        basis_type=ContributionIncomeType(c_rec["basis_type"]),
                        deduction_schedule=ContributionSchedule(
                            c_rec["deduction_schedule"]
                        ),
                        employee_income_deduction=float(
                            c_rec["employee_income_deduction"]
                        ),
                        employee_share_amount=float(c_rec["employee_share_amount"]),
                        employer_share_amount=float(c_rec["employer_share_amount"]),
                        employer_provident_fund_amount=float(c_rec.get("employer_provident_fund_amount", 0)),
                        employee_provident_fund_amount=float(c_rec.get("employee_provident_fund_amount", 0)),
                        max_pay_schedule=c_rec["max_pay_schedule"],
                        pay_schedule=c_rec["pay_schedule"],
                        current_gross_income=c_rec["current_gross_income"],
                        employee_compensation_amount=float(
                            c_rec["employee_compensation_amount"]
                        ),
                    )
                )
            record[c] = ContributionRecord(basis_records=rec_arr)

        return record

    """ EmployeeDetails = {
        "contribution_basis": [
            "SSS": {
                "type": "BASIC"|"GROSS"|"FIXED",
                "amount": <float:0>,
            },
            "HDMF": {
                "type": "BASIC"|"GROSS"|"FIXED",
                "amount": <float:0>,
            },
            "PHILHEALTH": {
                "type": "BASIC"|"GROSS"|"FIXED",
                "amount": <float:0>,
            },
        ],
        "tax_type": "REGULAR"|"CONSULTANT"|"MINIMUM"
        "tax_status": str,
        "tax_rate": float,
        "hrs_per_day": float,
        "basic_info": {
            str: str,
        },
    }"""

    @classmethod
    def _load_employee_details(
        cls, parr: Dict, payroll_group: PayrollGroup
    ) -> EmployeeDetails:
        return EmployeeDetails(
            contribution_basis=cls.__load_emp_contrib_basis(parr["contribution_basis"]),
            date_hired=cls.___str_to_date(parr["basic_info"]["dateHired"]),
            date_ended=cls.___str_to_date(parr["basic_info"]["dateEnded"])
            if parr["basic_info"]["dateEnded"]
            else parr["basic_info"]["dateEnded"],
            basic_info=parr["basic_info"],
            tax_status=parr["tax_status"],
            tax_type=TaxType(parr["tax_type"]),
            tax_rate=float(parr["tax_rate"]),
            hrs_per_day=float(parr["hrs_per_day"])
            if float(parr["hrs_per_day"])
            else payroll_group.hrs_per_day,
            day_factor=payroll_group.day_factor,
        )

    @classmethod
    def __load_emp_contrib_basis(cls, parr: Dict) -> Dict:
        new_basis = {}

        for x in ContributionEnum:
            new_basis[x] = EmployeeContributionBasisInfo(
                type=ContributionIncomeType(
                    parr[x.value]["type"].upper().replace(" ", "_")
                ),
                amount=float(parr[x.value]["amount"]),
            )

        return new_basis

    """ BasePayHistory = {
        "records": [
            {
                "amount": float,
                "pay_rate": "YEARLY"|"MONTHLY"|"DAILY"|"HOURLY",
                "effective_date": YYYY-MM-DD,
            }
        ]
    }"""

    @classmethod
    def _load_base_pay_history(
        cls,
        parr: Dict,
        rate_calc: RateCalculator,
        payroll_group: PayrollGroup,
        employee_details: EmployeeDetails,
    ) -> BasePayHistory:
        new_bp_rec = []
        for x in parr["records"]:
            new_bp_rec.append(
                BasePay(
                    amount=float(x["amount"]),
                    pay_rate=PayRate(x["pay_rate"]),
                    effective_date=cls.___str_to_date(x["effective_date"]),
                    rate_calculator=rate_calc,
                )
            )

        return BasePayHistory(
            records=new_bp_rec,
            history_start_date=payroll_group.attendance_start_date,
            history_end_date=payroll_group.attendance_end_date,
            date_hired=employee_details.date_hired,
            date_ended=employee_details.date_ended,
        )

    """ AttendanceRecord = [
        {
            "date": YYYY-MM-DD,
            "scheduled_hours": 8,
            "hr_types": [
                {"type": "RH", "hours": 5},
                {"type": "UH", "hours": 3},
                {"type": "RH+NT+OT", "hours": 3},
            ],
        },
        {
            "date": YYYY-MM-DD,
            # "scheduled_hours": 0, because RD
            "hr_types": [
                {"type": "RD+SH", "hours": 7}
            ],
        },
    ]"""

    @classmethod
    def _load_attendance_record(
        cls, parr: List, payroll_group: PayrollGroup
    ) -> Attendance:
        day_records = []
        for ta in parr:
            hr_arr = []
            for hr in ta["hr_types"]:
                hr_arr.append(
                    AttendanceHourRecord(hour_type=hr["type"], hours=hr["hours"])
                )
            day = AttendanceDayRecord(
                date=cls.___str_to_date(ta["date"]),
                hour_records=hr_arr,
                scheduled_hours=(
                    ta["scheduled_hours"] if "scheduled_hours" in ta else 0
                ),
            )
            day_records.append(day)

        return Attendance(day_records=day_records, payroll_group=payroll_group)

    """ PayrollGroup = {
        "pay_start_date": YYYY-MM-DD,
        "pay_end_date": YYYY-MM-DD,
        "attendance_start_date": YYYY-MM-DD,
        "attendance_end_date": YYYY-MM-DD,
        "posting_date": YYYY-MM-DD,
        "pay_run": YYYY-MM-DD,
        "cutoff": YYYY-MM-DD,
        "cutoff2": YYYY-MM-DD,

        "hrs_per_day": float,
        "day_factor": float,

        "pay_frequency": "MONTHLY"|"SEMI_MONTHLY"|"FORTNIGHTLY"|"WEEKLY",

        "contribution_schedules": {
            "SSS": "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
            "HDMF": "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
            "PHILHEALTH": "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
        },
        "hour_type_multiplier":{
            "REGULAR": 1,
            "PAID_LEAVE": 1,
            "UNPAID_LEAVE": 1,
            "UNDERTIME": 1,
            "ABSENT": 1,
            "UH": 1,
            "RD": 1.30,
            "SH": 1.30,
            "RD+SH": 1.50,
            "RH": 2.0,
            "RD+RH": 2.60,
            "RH+SH": 2.60,
            "RD+RH+SH": 3.0,
            "2RH": 3.0,
            "RD+2RH": 3.90,
            "OT": 1.25,
            "RD+OT": 1.69,
            "SH+OT": 1.69,
            "RD+SH+OT": 1.95,
            "RH+OT": 2.60,
            "RD+RH+OT": 3.38,
            "RH+SH+OT": 3.38,
            "RD+RH+SH+OT": 3.90,
            "2RH+OT": 3.90,
            "RD+2RH+OT": 5.070,
            "NT": 1.10,
            "RD+NT": 1.43,
            "SH+NT": 1.43,
            "RD+SH+NT": 1.65,
            "RH+NT": 2.20,
            "RD+RH+NT": 2.86,
            "RH+SH+NT": 2.86,
            "RD+RH+SH+NT": 3.30,
            "2RH+NT": 3.30,
            "RD+2RH+NT": 4.29,
            "NT+OT": 1.375,
            "RD+NT+OT": 1.859,
            "SH+NT+OT": 1.859,
            "RD+SH+NT+OT": 2.145,
            "RH+NT+OT": 2.86,
            "RD+RH+NT+OT": 3.718,
            "RH+SH+NT+OT": 3.718,
            "RD+RH+SH+NT+OT": 4.29,
            "2RH+NT+OT": 4.29,
            "RD+2RH+NT+OT": 5.577,
        },
    }"""

    @classmethod
    def _load_payroll_group(cls, parr: Dict, date_hired: String = None) -> PayrollGroup:
        cutoff2 = parr.get("cutoff2", None)
        cutoff2 = cls.___str_to_date(cutoff2) if cutoff2 else cutoff2

        posting_date2 = parr.get("posting_date2", None)
        posting_date2 = (
            cls.___str_to_date(posting_date2) if posting_date2 else posting_date2
        )

        # differentiate here if attend_period_switch is present in and checked yes in payroll group
        attendance_start_date = cls.___str_to_date(parr.get("attendance_start_date", parr["pay_start_date"]))
        original_attendance_start_date = attendance_start_date  # original attendance start date from the payroll details
        attendance_end_date = cls.___str_to_date(parr.get("attendance_end_date", parr["pay_end_date"]))
        pay_start_date = cls.___str_to_date(parr["pay_start_date"])
        pay_end_date = cls.___str_to_date(parr["pay_end_date"])
        force_compute_daily = False
        if parr.get("attend_period_switch", False) is True and date_hired:
            date_hired = cls.___str_to_date(date_hired)
            if attendance_start_date < date_hired <= attendance_end_date:
                force_compute_daily = True
                attendance_start_date = date_hired
            elif attendance_start_date <= attendance_end_date < date_hired:
                force_compute_daily = True

        return PayrollGroup(
            pay_start_date=pay_start_date,
            pay_end_date=pay_end_date,
            attendance_start_date=attendance_start_date,
            attendance_end_date=attendance_end_date,
            original_attendance_start_date=original_attendance_start_date,
            posting_date=cls.___str_to_date(parr["posting_date"]),
            pay_run=cls.___str_to_date(parr["pay_run"]),
            pay_frequency=PayFrequency(parr["pay_frequency"]),
            contribution_schedules={
                x: ContributionSchedule(parr["contribution_schedules"][x.value])
                for x in ContributionEnum
            },
            hrs_per_day=float(parr["hrs_per_day"]),
            day_factor=float(parr["day_factor"]),
            cutoff=cls.___str_to_date(parr["cutoff"]),
            cutoff_is_eom=bool(parr.get("cutoff_is_eom", False)),
            posting_date1=cls.___str_to_date(parr["posting_date1"]),
            posting_date1_is_eom=bool(parr.get("posting_date1_is_eom", False)),
            cutoff2=cutoff2,
            cutoff2_is_eom=bool(parr.get("cutoff2_is_eom", False)),
            posting_date2=posting_date2,
            posting_date2_is_eom=bool(parr.get("posting_date2_is_eom", False)),
            hour_type_multiplier=parr["hour_type_multiplier"],
            compute_overbreak=parr["compute_overbreak"],
            compute_tardiness=parr["compute_tardiness"],
            compute_undertime=parr["compute_undertime"],
            force_compute_daily=force_compute_daily,
        )

    @classmethod
    def ___str_to_date(cls, string_date: str) -> date:
        return datetime.strptime(string_date, "%Y-%m-%d").date()


class SpecialPayrollCalculator:
    @classmethod
    def calculate(cls, payroll_details: Dict, employee_details: Dict):
        # Joe
        aabcd = employee_details["attributes"]["aabcd"]
        print('SpecialPayrollCalculator calc aabcddddddddd')
        print(aabcd)
        selected_releases = employee_details["attributes"].get("selectedReleases", [])

        selected_bonus_release_ids = []
        for item in selected_releases:
            if SelectedReleasesType(item["type"]) == SelectedReleasesType.BONUS:
                selected_bonus_release_ids.append(item.get("release_id", 0))

        emp = SpecialPayrollCalculator._load_employee_details(
            employee_details["attributes"]
        )
        print('SpecialPayrollCalculator calc empppppppppppppppppp')
        print(emp)
        rate_calcu = RateCalculator(
            day_factor=emp.day_factor, hrs_per_day=emp.hrs_per_day
        )

        aabcd_param = {} if not aabcd else aabcd

        aabcd_generator = SpecialPayrollCalculator._load_aabcd(
            aabcd_param, emp, rate_calcu, selected_bonus_release_ids
        )
        deminimis = {"reduceToTaxableIncome": [], "addAsNontaxableIncome": []}
        filtered_deminimies = aabcd_generator.filter_deminimies_from_allowances()
        for demini in filtered_deminimies:
            if demini.get('tax_option') == "reduce_to_taxable_income":
                deminimis['reduceToTaxableIncome'].append(
                    {
                        "name": demini.get('name'),
                        "amount": aabcd_generator._get_allowance_amount(demini)[1],
                        "type": "REDUCE_TAXABLE"
                    }
                )
            elif demini.get('tax_option') == "fully_non_taxable":
                deminimis['addAsNontaxableIncome'].append(
                    {
                        "name": demini.get('name'),
                        "amount": aabcd_generator._get_allowance_amount(demini)[1],
                        "type": "NON_TAXABLE"
                    }
                )
        dms = SpecialPayrollCalculator._load_deminimis(deminimis)

        oi_list = (
            aabcd_generator.generate_allowances()
            + aabcd_generator.generate_bonuses()
            + aabcd_generator.generate_commissions()
        )
        print('SpecialPayrollCalculator calc oi_list')
        print(aabcd_generator.generate_bonuses())

        ded_list = aabcd_generator.generate_deductions()

        print('SpecialPayrollCalculator calc annualEarningssssssssssssssss')
        print(emp.basic_info.get("annualEarnings", {}))
        annual_earnings = emp.basic_info.get("annualEarnings", {})
        # annual_withholding_tax = annual_earnings.get("withholdingTax", 0)
        annual_taxable_income = annual_earnings.get("taxableIncome", 0)
        # annual_contributions = annual_earnings.get("contributions", 0)
        print('payroll_detailsssssssssssssssssssssssssssssssssssssssssssss')
        print(payroll_details["data"])
        print('OtherIncomeRecord')
        print(OtherIncomeRecord(records=oi_list))
        net_pay = SpecialNetPay(
            employee_details=emp,
            pay_frequency=PayFrequency(
                employee_details["attributes"]["payrollFrequency"]
            ),
            payroll_details=payroll_details["data"],
            other_incomes=OtherIncomeRecord(records=oi_list),
            adjustments=aabcd_generator.generate_adjustments(),
            deductions=ded_list,
            deminimis=dms,
            # annual_taxable_income=annual_taxable_income,
        )
        print('SpecialPayrollCalculator calculate net_payyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy')
        print(net_pay)
        payroll_breakdown = net_pay.breakdown
        payroll_breakdown[
            "payroll_items"
        ] = SpecialPayrollCalculator._create_summary_from_breakdown(net_pay)

        return {
            "input": {
                "payroll_details": payroll_details,
                "employee_details": employee_details,
            },
            "output": SpecialPayrollCalculator._breakdown_to_basic_data(
                payroll_breakdown
            ),
        }

    @classmethod
    def _create_summary_from_breakdown(cls, net_pay: SpecialNetPay) -> Dict:
        breakdown = net_pay.breakdown

        return {
            "adjustments": breakdown["adjustments"],
            "deductions": breakdown["deductions"],
            "other_incomes": breakdown["other_incomes"],
            "taxable_income": net_pay.taxable_income_amount,
            "gross": net_pay.gross_income_amount,
            "withholding_tax": net_pay.tax_amount,
            "tax_amount_due": net_pay.tax_amount_due,
            "total_deduction": net_pay.total_gross_income_deduction_amount,
            "net_pay": net_pay.amount,
            "tax_status": net_pay.employee_details.tax_status,
            "tax_type": net_pay.employee_details.tax_type.value,
            "tax_rate": net_pay.employee_details.tax_rate,
            'total_philhealth_employee_contribution_adjustment': net_pay.total_philhealth_employee_contribution_adjustments_amount,
            'total_philhealth_employer_contribution_adjustment': net_pay.total_philhealth_employer_contribution_adjustments_amount,
            "deminimis": breakdown["deminimis"],
        }

    @classmethod
    def _breakdown_to_basic_data(cls, breakdown):
        # convert tax
        tax_breakdown = breakdown["tax"]
        tax_breakdown["tax_type"] = tax_breakdown["tax_type"].value

        # convert other_incomes
        oi_breakdown = breakdown["other_incomes"]
        for l in ["ALLOWANCE", "BONUS", "COMMISSION"]:
            for t in ["taxable", "non_taxable"]:
                new_rec: List = []
                for oi in oi_breakdown[l][t]:
                    new_rec.append(
                        {"type": oi.type, "amount": oi.amount, "meta": oi.meta}
                    )

                oi_breakdown[l][t] = new_rec

        # convert adjustments
        new_rec: List = []
        for adj_type, adj_sub_list in breakdown["adjustments"].items():
            new_rec: List = []
            for adj in adj_sub_list:
                new_rec.append(
                    {
                        "type": adj.type.value,
                        "amount": adj.amount,
                        "reason": adj.reason,
                        "meta": adj.meta,
                    }
                )
            breakdown["adjustments"][adj_type] = new_rec

        # convert deductions
        new_rec: List = [
            {"type": ded.type, "amount": ded.amount, "meta": ded.meta}
            for ded in breakdown["deductions"]
        ]
        # essentially the same records
        breakdown["deductions"] = new_rec
        breakdown["payroll_items"]["deductions"] = new_rec

        # convert adjustments
        adj_breakdown = breakdown["adjustments"]
        for adj in AdjustmentType:
            adj_breakdown[adj.value] = adj_breakdown[adj]
            adj_breakdown.pop(adj)

        # convert deminimis
        for dms_type, dms_sub_list in breakdown["deminimis"].items():
            new_rec: List = []
            for dms in dms_sub_list:
                new_rec.append(
                    {
                        "name": dms.name,
                        "amount": dms.amount,
                        "type": dms.type.value,
                        "meta": dms.meta,
                    }
                )
            breakdown["deminimis"][dms_type] = new_rec

        # convert employee_details
        emp_details = breakdown["employee_details"]

        emp = {
            "basic_info": emp_details.basic_info,
            "tax_status": emp_details.tax_status,
            "tax_type": emp_details.tax_type.value,
            "hrs_per_day": emp_details.hrs_per_day,
            "day_factor": emp_details.day_factor,
        }

        breakdown["employee_details"] = emp

        return breakdown

    @classmethod
    def _load_employee_details(cls, parr: Dict) -> EmployeeDetails:
        payroll_calculator = PayrollCalculator()
        return EmployeeDetails(
            contribution_basis=None,
            date_hired=payroll_calculator._PayrollCalculator___str_to_date(
                parr["dateHired"]
            ),
            date_ended=payroll_calculator._PayrollCalculator___str_to_date(
                parr["dateEnded"]
            )
            if parr["dateEnded"]
            else parr["dateEnded"],
            basic_info=parr,
            tax_status=parr["taxStatus"],
            tax_type=TaxType(parr["taxType"].replace(" ", "_").upper()),
            tax_rate=float(parr["taxRate"]) if parr["taxRate"] else None,
            hrs_per_day=float(parr["hoursPerDay"]),
            day_factor=float(parr["dayFactor"]),
        )

    @classmethod
    def _load_aabcd(
        cls,
        aabcd_dict,
        employee_details: EmployeeDetails,
        rate_calcu: RateCalculator,
        selected_bonus_release_ids: list,
    ) -> SpecialAABCDGenerator:
        aabcd_schemas = {
            "adjustment": AdjustmentSettingsSchema(),
            "allowance": AllowanceSettingsSchema(),
            "bonus": BonusSettingsSchema(),
            "commission": CommissionSettingsSchema(),
            "deduction": DeductionSettingsSchema(),
        }

        base_pay_history: BasePayHistory = BasePayHistory(records=[])
        pay_history = {
            "grossIncome": PayHistory(records=[]),
            "grossBasic": PayHistory(records=[]),
            "basicPay": PayHistory(records=[]),
        }

        aabcd_arr = {}
        for key in aabcd_dict:
            if "salaryBasis" == key:
                for basis in aabcd_dict["salaryBasis"].keys():
                    if "basePay" == basis:
                        records: List[BasePay] = [
                            BasePay(
                                amount=float(pay["attributes"]["amount"]),
                                pay_rate=PayRate(pay["attributes"]["unit"]),
                                effective_date=datetime.strptime(
                                    pay["attributes"]["effectiveDate"], "%Y-%m-%d"
                                ).date(),
                                rate_calculator=rate_calcu,
                            )
                            for pay in aabcd_dict["salaryBasis"]["basePay"]
                        ]
                        base_pay_history = BasePayHistory(records=records)
                    else:
                        pay_history_records = [
                            Pay(
                                amount=float(pay["amount"]),
                                effective_start_date=datetime.strptime(
                                    pay["startDate"], "%Y-%m-%d"
                                ).date(),
                                effective_end_date=datetime.strptime(
                                    pay["endDate"], "%Y-%m-%d"
                                ).date(),
                                meta=pay["meta"],
                            )
                            for pay in aabcd_dict["salaryBasis"][basis]
                        ]
                        pay_history[basis] = PayHistory(records=pay_history_records)
            else:
                aabcd_arr[key] = list(
                    map(
                        lambda x: PayrollCalculator._load_via_schema(
                            aabcd_schemas[key], x
                        ),
                        aabcd_dict[key],
                    )
                )

        return SpecialAABCDGenerator(
            employee_details=employee_details,
            base_pay_history=base_pay_history,
            basic_pay_history=pay_history["basicPay"],
            gross_basic_history=pay_history["grossBasic"],
            gross_income_history=pay_history["grossIncome"],
            adjustment_settings=aabcd_arr.get("adjustment", []),
            allowance_settings=aabcd_arr.get("allowance", []),
            bonus_settings=aabcd_arr.get("bonus", []),
            commission_settings=aabcd_arr.get("commission", []),
            deduction_settings=aabcd_arr.get("deduction", []),
            selected_bonus_release_ids=selected_bonus_release_ids,
        )

    @classmethod
    def _load_deminimis(cls, dms_dict) -> DeMinimisRecord:
        dms_list = []
        for dms in dms_dict["reduceToTaxableIncome"]:
            dms_list.append(
                DeMinimis(
                    name=dms["name"],
                    amount=float(dms["amount"]),
                    type=DeminimisType(dms["type"]),
                )
            )

        for dms in dms_dict["addAsNontaxableIncome"]:
            dms_list.append(
                DeMinimis(
                    name=dms["name"],
                    amount=float(dms["amount"]),
                    type=DeminimisType(dms["type"]),
                )
            )

        return DeMinimisRecord(records=dms_list)


class FinalPayrollCalculator:
    @classmethod
    def calculate(cls, payroll_details: Dict, employee_details: Dict):
        # Joe
        payroll = payroll_details.get("attributes", {})
        pay_run_date = payroll.get("payRunDate", "")
        employee = employee_details.get("attributes", {})
        prev_contributions = employee.get("previousContributions", {})
        loan_amortizations = employee.get("amortizations", {})
        aabcd = employee.get("aabcd", {})
        final_pay_items = employee.get("finalPayItems", [])

        selected_bonus_release_ids = []
        for item in final_pay_items:
            if FinalPayItemType(item["type"]) == FinalPayItemType.BONUS:
                selected_bonus_release_ids.append(item.get("item_id", 0))

        # Filter the amortizations to be paid
        amortizations = defaultdict(list)
        for amort in loan_amortizations:
            amortizations[amort["loanId"]].append(amort)

        filtered_amortizations = []
        for item in final_pay_items:
            if FinalPayItemType(item["type"]) == FinalPayItemType.LOAN:
                meta = item.get("meta", {})
                is_full = meta.get("is_full", False)
                for loan_id, amorts in amortizations.items():
                    if loan_id == item.get("item_id", 0):
                        for amort in amorts:
                            # Collate all amortizations here
                            filtered_amortizations.append(amort)

        sss_refund = False
        hdmf_refund = False
        philhealth_refund = False
        for item in final_pay_items:
            if FinalPayItemType(item["type"]) == FinalPayItemType.CONTRIBUTION:
                if ContributionEnum(item["meta"]["type"]) == ContributionEnum.SSS:
                    sss_refund = item["meta"]["refund"]
                if ContributionEnum(item["meta"]["type"]) == ContributionEnum.HDMF:
                    hdmf_refund = item["meta"]["refund"]
                if (
                    ContributionEnum(item["meta"]["type"])
                    == ContributionEnum.PHILHEALTH
                ):
                    philhealth_refund = item["meta"]["refund"]

        annual_earnings = employee.get("annualEarnings", {})
        annual_withholding_tax = annual_earnings.get("withholdingTax", 0)
        annual_taxable_income = annual_earnings.get("taxableIncome", 0)
        annual_contributions = annual_earnings.get("contributions", 0)

        base_pay_history = []
        basepay_history = employee.get("basePay")

        transformed_bh = []
        for item in basepay_history:
            new_item = {}
            attributes = item.get("attributes", {})
            if attributes:
                new_item["amount"] = float(attributes.get("amount", 0))
                new_item["pay_rate"] = attributes.get("unit")
                new_item["effective_date"] = attributes.get("effectiveDate")
                transformed_bh.append(new_item)

        base_pay_history = {"records": transformed_bh}

        attendance_data = dict(employee.get("attendanceData", {}))
        attendance_record = []
        for dt, value in attendance_data.items():
            value = dict(value)
            new_item = {}
            new_item["date"] = dt
            sh = value.get("SCHEDULED_HOURS", None)
            if sh is not None:
                new_item["scheduled_hours"] = float(sh)
                del value["SCHEDULED_HOURS"]
            new_item["hr_types"] = []
            for htype, hvalue in value.items():
                new_item["hr_types"].append(
                    {"type": str(htype).upper(), "hours": float(hvalue)}
                )
            attendance_record.append(new_item)

        # deminimis = (
        #     {"reduceToTaxableIncome": [], "addAsNontaxableIncome": []}
        #     if deminimis is None
        #     else deminimis
        # )
        deminimis = {"reduceToTaxableIncome": [], "addAsNontaxableIncome": []}

        pg = FinalPayrollCalculator._load_payroll_group(payroll, employee)
        emp = FinalPayrollCalculator._load_employee_details(employee, pg)

        rate_calcu = RateCalculator(
            day_factor=emp.day_factor, hrs_per_day=emp.hrs_per_day
        )

        att = FinalPayrollCalculator._load_attendance_record(attendance_record, pg)

        bph = FinalPayrollCalculator._load_base_pay_history(
            base_pay_history, rate_calcu, pg, emp
        )

        pcb = FinalPayrollCalculator._load_previous_contributions(prev_contributions)

        loa = FinalPayrollCalculator._load_loan_amortizations(filtered_amortizations)

        # dms = FinalPayrollCalculator._load_deminimis(deminimis)
        # upl_oi = FinalPayrollCalculator._load_other_incomes(uploaded_other_incomes)
        # upl_ded = FinalPayrollCalculator._load_deductions(uploaded_deductions)

        basic_pay = FinalBasicPay(
            payroll_group=pg, attendance=att, base_pay_history=bph
        )

        aabcd_param = {} if not aabcd else aabcd

        aabcd_generator = FinalPayrollCalculator._load_aabcd(
            aabcd_param, emp, rate_calcu, pg, att, basic_pay, selected_bonus_release_ids
        )
        filtered_deminimies = aabcd_generator.filter_deminimies_from_allowances()
        att_ratios = aabcd_generator._compute_attendance_ratio()
        for demini in filtered_deminimies:
            if demini.get('tax_option') == "reduce_to_taxable_income":
                deminimis['reduceToTaxableIncome'].append(
                    {
                        "name": demini.get('name'),
                        "amount": aabcd_generator._get_allowance_amount(demini, att_ratios)[1],
                        "type": "REDUCE_TAXABLE"
                    }
                )
            elif demini.get('tax_option') == "fully_non_taxable":
                deminimis['addAsNontaxableIncome'].append(
                    {
                        "name": demini.get('name'),
                        "amount": aabcd_generator._get_allowance_amount(demini, att_ratios)[1],
                        "type": "NON_TAXABLE"
                    }
                )
        dms = FinalPayrollCalculator._load_deminimis(deminimis)
        oi_list = (
            # upl_oi
            # + aabcd_generator.generate_allowances()
            aabcd_generator.generate_allowances()
            + aabcd_generator.generate_bonuses()
            + aabcd_generator.generate_commissions()
        )

        # ded_list = upl_ded + aabcd_generator.generate_deductions()
        ded_list = aabcd_generator.generate_deductions()
        # --

        # loc = FinalPayrollCalculator._load_loans_to_credit(loans_to_credit)

        net_pay = FinalNetPay(
            basic_pay=basic_pay,
            employee_details=emp,
            prev_contrib=pcb,
            other_incomes=OtherIncomeRecord(records=oi_list),
            adjustments=aabcd_generator.generate_adjustments(),
            deductions=ded_list,
            loan_amortizations=loa,
            sss_refund=sss_refund,
            hdmf_refund=hdmf_refund,
            philhealth_refund=philhealth_refund,
            # loans_to_credit=loc,
            deminimis=dms,
            annual_withholding_tax=annual_withholding_tax,
            annual_taxable_income=annual_taxable_income,
            annual_contributions=annual_contributions,
        )

        payroll_breakdown = net_pay.breakdown
        payroll_breakdown[
            "payroll_items"
        ] = FinalPayrollCalculator._create_summary_from_breakdown(net_pay)

        return {
            "input": {
                "payroll_details": payroll_details,
                "employee_details": employee_details,
            },
            "output": FinalPayrollCalculator._breakdown_to_basic_data(
                payroll_breakdown
            ),
        }

    @classmethod
    def _create_summary_from_breakdown(cls, net_pay: NetPay) -> Dict:
        breakdown = net_pay.breakdown

        att_keys = [
            "absent",
            "holiday_work",
            "night_differential",
            "overtime",
            "rest_day_work",
            "undertime",
            "tardy",
            "overbreak",
            "unworked_holiday",
        ]
        att_summary: Dict = {}
        leave_keys = ["paid_leave", "unpaid_leave"]
        leave_summary: Dict = {}
        for key, bp_breakdown in breakdown["basic_pay"].items():
            if key in att_keys:
                att_summary = {**att_summary, **bp_breakdown["summary"]}
            elif key in leave_keys:
                leave_summary = {**leave_summary, **bp_breakdown["summary"]}

        total_worked_hours = net_pay.basic_pay.attendance.total_scheduled_hours

        deduction_key = [
            "UNDERTIME",
            "ABSENT",
            "UNPAID_LEAVE",
            "UH",
            "TARDY",
            "OVERBREAK",
        ]
        skip_key = [
            "PAID_LEAVE",
            "NT",
            "SH+NT",
            "RH+NT",
            "RH+SH+NT",
            "2RH+NT",
        ] + _HolidayHourTypes
        full_summary = {**att_summary, **leave_summary}
        for hr_key, hr_summary in full_summary.items():
            if hr_key in deduction_key:
                total_worked_hours -= hr_summary["total_hours"]
            elif hr_key in skip_key:
                # ignore since pure NT is already supposed to be rendered work together
                # with the scheduled hours
                continue
            else:
                total_worked_hours += hr_summary["total_hours"]

        lbp = breakdown["basic_pay"]["full_pay"]["latest_base_pay"]
        contrib = net_pay.breakdown["contributions"]
        refund_contrib = net_pay.breakdown["refunded_contributions"]

        return {
            "attendance": att_summary,
            "adjustments": breakdown["adjustments"],
            "leaves": leave_summary,
            "deductions": breakdown["deductions"],
            "other_incomes": breakdown["other_incomes"],
            "loan_amortizations": breakdown["loan_amortizations"],
            # "loans_to_credit": breakdown["loans_to_credit"],
            "deminimis": breakdown["deminimis"],
            "base_pay": lbp.amount,
            "base_pay_unit": lbp.pay_rate.value,
            "monthly_income": lbp.full_conversion_rate_table["monthly_rate_amount"],
            "daily_rate": lbp.full_conversion_rate_table["daily_rate_amount"],
            "hourly_rate": lbp.full_conversion_rate_table["hourly_rate_amount"],
            "full_pay": net_pay.basic_pay.full_pay_amount,
            "gross_basic": net_pay.basic_pay.gross_basic_amount,
            "basic_pay": net_pay.basic_pay.basic_pay_amount,
            "taxable_income": net_pay.taxable_income_amount,
            "non_taxable_income": net_pay.non_taxable_income_amount,
            "taxable_incomes": net_pay.taxable_incomes_list,
            "non_taxable_incomes": net_pay.non_taxable_incomes_list,
            "deduction_items": net_pay.deductions_list,
            "total_reduce_taxable_deminimis_amount": net_pay.total_reduce_taxable_deminimis_amount,
            "total_non_taxable_deminimis_amount": net_pay.total_non_taxable_deminimis_amount,
            "gross": net_pay.gross_income_amount,
            "withholding_tax": net_pay.tax_amount,
            "tax_amount_due": net_pay.tax_amount_due,
            "total_deduction": net_pay.total_gross_income_deduction_amount,
            "net_pay": net_pay.amount,
            "tax_status": net_pay.employee_details.tax_status,
            "tax_type": net_pay.employee_details.tax_type.value,
            "tax_rate": net_pay.employee_details.tax_rate,
            "sss_employee": contrib.get(ContributionEnum.SSS, {}).get(
                "ee_share", 0
            ),
            "sss_employee_provident_fund": contrib.get(ContributionEnum.SSS, {}).get(
                "ee_provident_fund", 0
            ),
            "hdmf_employee": contrib.get(ContributionEnum.HDMF, {}).get(
                "emp_income_deduction", 0
            ),
            "philhealth_employee": contrib.get(ContributionEnum.PHILHEALTH, {}).get(
                "emp_income_deduction", 0
            ),
            "total_mandatory_employee": net_pay.total_contributions_income_deduction_amount,
            "refund_sss_employee": refund_contrib.get(ContributionEnum.SSS, {}).get(
                "emp_income_deduction", 0
            ),
            "refund_hdmf_employee": refund_contrib.get(ContributionEnum.HDMF, {}).get(
                "emp_income_deduction", 0
            ),
            "refund_philhealth_employee": refund_contrib.get(
                ContributionEnum.PHILHEALTH, {}
            ).get("emp_income_deduction", 0),
            "total_mandatory_employee_refund": net_pay.contribution_refund_amount,
            "sss_employer": contrib.get(ContributionEnum.SSS, {}).get("er_share", 0),
            "sss_ec_employer": contrib.get(ContributionEnum.SSS, {}).get("ec_share", 0),
            "sss_employer_provident_fund": contrib.get(ContributionEnum.SSS, {}).get(
                "er_provident_fund", 0
            ),
            "hdmf_employer": contrib.get(ContributionEnum.HDMF, {}).get("emp_income_deduction", 0),
            "philhealth_employer": contrib.get(ContributionEnum.PHILHEALTH, {}).get(
                "er_share", 0
            ),
            "total_mandatory_employer": net_pay.total_contributions_of_employer_amount,
            "total_scheduled_hours": net_pay.basic_pay.attendance.total_scheduled_hours,
            "total_worked_hours": total_worked_hours,
            "gross_taxable": net_pay.gross_income_taxable_amount,
        }

    @classmethod
    def _load_aabcd(
        cls,
        aabcd_dict,
        employee_details: EmployeeDetails,
        rate_calcu: RateCalculator,
        payroll_group: FinalPayrollGroup,
        attendance: Attendance,
        basic_pay: BasicPay,
        selected_bonus_release_ids: list,
    ) -> FinalAABCDGenerator:
        aabcd_schemas = {
            "adjustment": AdjustmentSettingsSchema(),
            "allowance": AllowanceSettingsSchema(),
            "bonus": BonusSettingsSchema(),
            "commission": CommissionSettingsSchema(),
            "deduction": DeductionSettingsSchema(),
        }

        base_pay_history: BasePayHistory = BasePayHistory(records=[])
        pay_history = {
            "grossIncome": PayHistory(records=[]),
            "grossBasic": PayHistory(records=[]),
            "basicPay": PayHistory(records=[]),
        }

        aabcd_arr = {}
        for key in aabcd_dict:
            if "salaryBasis" == key:
                for basis in aabcd_dict["salaryBasis"].keys():
                    if "basePay" == basis:
                        records: List[BasePay] = [
                            BasePay(
                                amount=float(pay["attributes"]["amount"]),
                                pay_rate=PayRate(pay["attributes"]["unit"]),
                                effective_date=datetime.strptime(
                                    pay["attributes"]["effectiveDate"], "%Y-%m-%d"
                                ).date(),
                                rate_calculator=rate_calcu,
                            )
                            for pay in aabcd_dict["salaryBasis"]["basePay"]
                        ]
                        base_pay_history = BasePayHistory(records=records)
                    else:
                        pay_history_records = [
                            Pay(
                                amount=float(pay["amount"]),
                                effective_start_date=datetime.strptime(
                                    pay["startDate"], "%Y-%m-%d"
                                ).date(),
                                effective_end_date=datetime.strptime(
                                    pay["endDate"], "%Y-%m-%d"
                                ).date(),
                                meta=pay["meta"],
                            )
                            for pay in aabcd_dict["salaryBasis"][basis]
                        ]
                        # just add the current payroll grossBasic and basicPay
                        # then let the coverage filter in aabcd calc
                        if basis in ["grossBasic", "basicPay"]:
                            amount_from_bp = (
                                basic_pay.gross_basic_amount
                                if basis == "grossBasic"
                                else basic_pay.basic_pay_amount
                            )
                            pay_history_records.append(
                                Pay(
                                    amount=amount_from_bp,
                                    effective_start_date=payroll_group.pay_start_date,
                                    effective_end_date=payroll_group.pay_end_date,
                                    meta={"payroll_id": "current"},
                                )
                            )

                        pay_history[basis] = PayHistory(records=pay_history_records)
            else:
                aabcd_arr[key] = list(
                    map(
                        lambda x: FinalPayrollCalculator._load_via_schema(
                            aabcd_schemas[key], x
                        ),
                        aabcd_dict[key],
                    )
                )

        return FinalAABCDGenerator(
            payroll_group=payroll_group,
            attendance=attendance,
            employee_details=employee_details,
            base_pay_history=base_pay_history,
            basic_pay_history=pay_history["basicPay"],
            gross_basic_history=pay_history["grossBasic"],
            gross_income_history=pay_history["grossIncome"],
            adjustment_settings=aabcd_arr.get("adjustment", []),
            allowance_settings=aabcd_arr.get("allowance", []),
            bonus_settings=aabcd_arr.get("bonus", []),
            commission_settings=aabcd_arr.get("commission", []),
            deduction_settings=aabcd_arr.get("deduction", []),
            selected_bonus_release_ids=selected_bonus_release_ids,
        )

    @classmethod
    def _load_via_schema(cls, schema, data_to_load):
        loaded_data = schema.load(data_to_load)
        # for marshmallow version 2
        if isinstance(loaded_data, UnmarshalResult):
            return loaded_data.data
        return loaded_data

    @classmethod
    def _load_deminimis(cls, dms_dict) -> DeMinimisRecord:
        dms_list = []
        for dms in dms_dict["reduceToTaxableIncome"]:
            dms_list.append(
                DeMinimis(
                    name=dms["name"],
                    amount=float(dms["amount"]),
                    type=DeminimisType(dms["type"]),
                )
            )

        for dms in dms_dict["addAsNontaxableIncome"]:
            dms_list.append(
                DeMinimis(
                    name=dms["name"],
                    amount=float(dms["amount"]),
                    type=DeminimisType(dms["type"]),
                )
            )

        return DeMinimisRecord(records=dms_list)

    @classmethod
    def _load_loans_to_credit(cls, lon_list):
        ret_arr = []
        for lon in lon_list:
            meta = json.dumps(
                {k: lon[k] for k in ["id", "typeID", "subtype", "paymentScheme"]}
            )
            ret_arr.append(
                CreditedLoan(
                    name=lon["loanName"],
                    amount=float(lon["totalAmount"]),
                    date=cls.___str_to_date(lon["createdDate"]),
                    meta=meta,
                )
            )

        return ret_arr

    @classmethod
    def _load_loan_amortizations(cls, lon_list):
        ret_arr = []
        for lon in lon_list:
            meta = json.dumps(
                {
                    k: lon[k]
                    for k in [
                        "id",
                        "loanId",
                        "subtype",
                        "employerRemarks",
                        "principalAmount",
                        "referenceNumber",
                    ]
                }
            )
            ret_arr.append(
                Loan(
                    name=lon["loanName"],
                    amount=float(lon["amountDue"]),
                    due_date=cls.___str_to_date(lon["dueDate"]),
                    meta=meta,
                )
            )

        return ret_arr

    @classmethod
    def _load_deductions(cls, ded_list):
        return [Deduction(type=ded["type"], amount=ded["amount"]) for ded in ded_list]

    @classmethod
    def _load_other_incomes(cls, oi_list):
        records: List = []
        vtable = {"ALLOWANCES": Allowance, "BONUSES": Bonus, "COMMISSIONS": Commission}
        for n in vtable:
            for oi in oi_list[n]:
                obj = vtable[n](oi["type"], oi["amount"], oi["taxable"])
                records.append(obj)
        return records

    @classmethod
    def _breakdown_to_basic_data(cls, breakdown):
        # convert dates
        bp_breakdown = breakdown["basic_pay"]
        for x in [
            "absent",
            "holiday_work",
            "night_differential",
            "overtime",
            "paid_leave",
            "rest_day_work",
            "undertime",
            "tardy",
            "overbreak",
            "unpaid_leave",
            "unworked_holiday",
        ]:
            for hr_att in bp_breakdown[x]["records"]:
                hr_att["date"] = hr_att["date"].strftime("%Y-%m-%d")

        # convert base pay
        new_rec: List = [
            {
                "amount": x.amount,
                "pay_rate": x.pay_rate.value,
                "effective_date": x.effective_date.strftime("%Y-%m-%d"),
                "conversion_rates": x.full_conversion_rate_table,
            }
            for x in bp_breakdown["full_pay"]["records"]
        ]
        bp_breakdown["full_pay"]["records"] = new_rec
        lbp = bp_breakdown["full_pay"]["latest_base_pay"]
        bp_breakdown["full_pay"]["latest_base_pay"] = {
            "amount": lbp.amount,
            "pay_rate": lbp.pay_rate.value,
            "effective_date": lbp.effective_date.strftime("%Y-%m-%d"),
            "conversion_rates": lbp.full_conversion_rate_table,
        }

        # convert contribution records
        contrib_breakdown = breakdown["contributions"]
        if contrib_breakdown:
            for x in ContributionEnum:
                new_rec = []
                if contrib_breakdown.get(x, None):
                    for rec in contrib_breakdown[x]["records"].basis_records:
                        new_rec.append(
                            {
                                "basis_amount": rec.basis_amount,
                                "basis_type": rec.basis_type.value,
                                "deduction_schedule": rec.deduction_schedule.value,
                                "employee_income_deduction": rec.employee_income_deduction,
                                "employee_share_amount": rec.employee_share_amount,
                                "employer_share_amount": rec.employer_share_amount,
                                "max_pay_schedule": rec.max_pay_schedule,
                                "pay_schedule": rec.pay_schedule,
                                "current_gross_income": rec.current_gross_income,
                                "employee_compensation_amount": rec.employee_compensation_amount,
                            }
                        )
                    contrib_breakdown[x]["records"] = new_rec
                    # convert to string the contrib keys
                    contrib_breakdown[x.value] = contrib_breakdown[x]
                    contrib_breakdown.pop(x)

        refunded_contrib_breakdown = breakdown["refunded_contributions"]
        if refunded_contrib_breakdown:
            for x in ContributionEnum:
                new_rec = []
                if refunded_contrib_breakdown.get(x, None):
                    for rec in refunded_contrib_breakdown[x]["records"].basis_records:
                        new_rec.append(
                            {
                                "basis_amount": rec.basis_amount,
                                "basis_type": rec.basis_type.value,
                                "deduction_schedule": rec.deduction_schedule.value,
                                "employee_income_deduction": rec.employee_income_deduction,
                                "employee_share_amount": rec.employee_share_amount,
                                "employer_share_amount": rec.employer_share_amount,
                                "max_pay_schedule": rec.max_pay_schedule,
                                "pay_schedule": rec.pay_schedule,
                                "current_gross_income": rec.current_gross_income,
                                "employee_compensation_amount": rec.employee_compensation_amount,
                            }
                        )
                    refunded_contrib_breakdown[x]["records"] = new_rec
                    # convert to string the contrib keys
                    refunded_contrib_breakdown[x.value] = refunded_contrib_breakdown[x]
                    refunded_contrib_breakdown.pop(x)

        # convert tax
        tax_breakdown = breakdown["tax"]
        tax_breakdown["tax_type"] = tax_breakdown["tax_type"].value

        # convert other_incomes
        oi_breakdown = breakdown["other_incomes"]
        for l in ["ALLOWANCE", "BONUS", "COMMISSION"]:
            for t in ["taxable", "non_taxable"]:
                new_rec: List = []
                for oi in oi_breakdown[l][t]:
                    new_rec.append(
                        {"type": oi.type, "amount": oi.amount, "meta": oi.meta}
                    )

                oi_breakdown[l][t] = new_rec

        # convert adjustments
        new_rec: List = []
        for adj_type, adj_sub_list in breakdown["adjustments"].items():
            new_rec: List = []
            for adj in adj_sub_list:
                new_rec.append(
                    {
                        "type": adj.type.value,
                        "amount": adj.amount,
                        "reason": adj.reason,
                        "meta": adj.meta,
                    }
                )
            breakdown["adjustments"][adj_type] = new_rec

        # convert deductions
        new_rec: List = [
            {"type": ded.type, "amount": ded.amount, "meta": ded.meta}
            for ded in breakdown["deductions"]
        ]
        # essentially the same records
        breakdown["deductions"] = new_rec
        breakdown["payroll_items"]["deductions"] = new_rec

        # convert loans_to_credit
        # new_rec: List = [
        #     {
        #         "type": lon.name,
        #         "amount": lon.amount,
        #         "date": lon.date.strftime("%Y-%m-%d"),
        #         "meta": lon.meta,
        #     }
        #     for lon in breakdown["loans_to_credit"]
        # ]
        # essentially the same records
        # breakdown["loans_to_credit"] = new_rec
        # breakdown["payroll_items"]["loans_to_credit"] = new_rec

        # convert loan_amortizations
        for lon_type, lon_sub_list in breakdown["loan_amortizations"].items():
            new_rec: List = []
            for lon in lon_sub_list:
                new_rec.append(
                    {
                        "type": lon.name,
                        "amount": lon.amount,
                        "due_date": lon.due_date.strftime("%Y-%m-%d"),
                        "meta": lon.meta,
                    }
                )
            breakdown["loan_amortizations"][lon_type] = new_rec

        # convert deminimis
        for dms_type, dms_sub_list in breakdown["deminimis"].items():
            new_rec: List = []
            for dms in dms_sub_list:
                new_rec.append(
                    {
                        "name": dms.name,
                        "amount": dms.amount,
                        "type": dms.type.value,
                        "meta": dms.meta,
                    }
                )
            breakdown["deminimis"][dms_type] = new_rec

        # convert adjustments
        adj_breakdown = breakdown["adjustments"]
        for adj in AdjustmentType:
            adj_breakdown[adj.value] = adj_breakdown[adj]
            adj_breakdown.pop(adj)

        # convert employee_details
        emp_details = breakdown["employee_details"]
        emp_cbasis = {}
        for x in ContributionEnum:
            emp_cbasis[x.value] = {
                "type": emp_details.contribution_basis[x].type.value,
                "amount": emp_details.contribution_basis[x].amount,
            }
        emp = {
            "basic_info": emp_details.basic_info,
            "tax_status": emp_details.tax_status,
            "tax_type": emp_details.tax_type.value,
            "hrs_per_day": emp_details.hrs_per_day,
            "day_factor": emp_details.day_factor,
            "contribution_basis": emp_cbasis,
        }
        breakdown["employee_details"] = emp

        return breakdown

    """ PreviousContributionRecord = {
        "SSS": [
            {
                basis_amount: float,
                basis_type: "BASIC"|"GROSS"|"FIXED",
                deduction_schedule: "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
                employee_income_deduction: float,
                employee_share_amount: float,
                employer_share_amount: float,
                max_pay_schedule: int,
                pay_schedule: int,
                employee_compensation_amount: float,
            }
        ],
        "HDMF": [
            {
                basis_amount: float,
                basis_type: "BASIC"|"GROSS"|"FIXED",
                deduction_schedule: "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
                employee_income_deduction: float,
                employee_share_amount: float,
                employer_share_amount: float,
                max_pay_schedule: int,
                pay_schedule: int,
                employee_compensation_amount: float,
            }
        ],
        "PHILHEALTH": [
            {
                basis_amount: float,
                basis_type: "BASIC"|"GROSS"|"FIXED",
                deduction_schedule: "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
                employee_income_deduction: float,
                employee_share_amount: float,
                employer_share_amount: float,
                max_pay_schedule: int,
                pay_schedule: int,
                employee_compensation_amount: float,
            }
        ],
    }
    """

    @classmethod
    def _load_previous_contributions(cls, parr: Dict) -> Dict[str, ContributionRecord]:
        record = {}
        for c in ContributionEnum:
            rec_arr = []
            for c_rec in parr[c.value]:
                rec_arr.append(
                    ContributionBasisRecord(
                        basis_amount=float(c_rec["basis_amount"]),
                        basis_type=ContributionIncomeType(c_rec["basis_type"]),
                        deduction_schedule=ContributionSchedule(
                            c_rec["deduction_schedule"]
                        ),
                        employee_income_deduction=float(
                            c_rec["employee_income_deduction"]
                        ),
                        employee_share_amount=float(c_rec["employee_share_amount"]),
                        employer_share_amount=float(c_rec["employer_share_amount"]),
                        max_pay_schedule=c_rec["max_pay_schedule"],
                        pay_schedule=c_rec["pay_schedule"],
                        current_gross_income=c_rec["current_gross_income"],
                        employee_compensation_amount=float(
                            c_rec["employee_compensation_amount"]
                        ),
                    )
                )
            record[c] = ContributionRecord(basis_records=rec_arr)

        return record

    """ EmployeeDetails = {
        "contribution_basis": [
            "SSS": {
                "type": "BASIC"|"GROSS"|"FIXED",
                "amount": <float:0>,
            },
            "HDMF": {
                "type": "BASIC"|"GROSS"|"FIXED",
                "amount": <float:0>,
            },
            "PHILHEALTH": {
                "type": "BASIC"|"GROSS"|"FIXED",
                "amount": <float:0>,
            },
        ],
        "tax_type": "REGULAR"|"CONSULTANT"|"MINIMUM"
        "tax_status": str,
        "tax_rate": float,
        "hrs_per_day": float,
        "basic_info": {
            str: str,
        },
    }"""

    @classmethod
    def _load_employee_details(
        cls, parr: Dict, payroll_group: FinalPayrollGroup
    ) -> EmployeeDetails:
        return EmployeeDetails(
            contribution_basis=cls.__load_emp_contrib_basis(parr["contributionBasis"]),
            date_hired=cls.___str_to_date(parr["dateHired"]),
            date_ended=cls.___str_to_date(parr["dateEnded"]),
            basic_info=parr,
            tax_status=parr["taxStatus"],
            tax_type=TaxType(parr["taxType"].replace(" ", "_").upper()),
            tax_rate=float(parr["taxRate"]) if parr["taxRate"] else None,
            hrs_per_day=float(parr["hoursPerDay"])
            if parr["hoursPerDay"]
            else payroll_group.hrs_per_day,
            day_factor=payroll_group.day_factor,
        )

    @classmethod
    def __load_emp_contrib_basis(cls, parr: Dict) -> Dict:
        new_basis = {}

        for x in ContributionEnum:
            new_basis[x] = EmployeeContributionBasisInfo(
                type=ContributionIncomeType(
                    parr[x.value]["type"].upper().replace(" ", "_")
                ),
                amount=float(parr[x.value]["amount"]),
            )

        return new_basis

    """ BasePayHistory = {
        "records": [
            {
                "amount": float,
                "pay_rate": "YEARLY"|"MONTHLY"|"DAILY"|"HOURLY",
                "effective_date": YYYY-MM-DD,
            }
        ]
    }"""

    @classmethod
    def _load_base_pay_history(
        cls,
        parr: Dict,
        rate_calc: RateCalculator,
        payroll_group: FinalPayrollGroup,
        employee_details: EmployeeDetails,
    ) -> BasePayHistory:
        new_bp_rec = []
        for x in parr["records"]:
            new_bp_rec.append(
                BasePay(
                    amount=float(x["amount"]),
                    pay_rate=PayRate(x["pay_rate"]),
                    effective_date=cls.___str_to_date(x["effective_date"]),
                    rate_calculator=rate_calc,
                )
            )

        return BasePayHistory(
            records=new_bp_rec,
            history_start_date=payroll_group.attendance_start_date,
            history_end_date=payroll_group.attendance_end_date,
            date_hired=employee_details.date_hired,
            date_ended=employee_details.date_ended,
        )

    """ AttendanceRecord = [
        {
            "date": YYYY-MM-DD,
            "scheduled_hours": 8,
            "hr_types": [
                {"type": "RH", "hours": 5},
                {"type": "UH", "hours": 3},
                {"type": "RH+NT+OT", "hours": 3},
            ],
        },
        {
            "date": YYYY-MM-DD,
            # "scheduled_hours": 0, because RD
            "hr_types": [
                {"type": "RD+SH", "hours": 7}
            ],
        },
    ]"""

    @classmethod
    def _load_attendance_record(
        cls, parr: List, payroll_group: FinalPayrollGroup
    ) -> Attendance:
        day_records = []
        for ta in parr:
            hr_arr = []
            for hr in ta["hr_types"]:
                hr_arr.append(
                    AttendanceHourRecord(hour_type=hr["type"], hours=hr["hours"])
                )
            day = AttendanceDayRecord(
                date=cls.___str_to_date(ta["date"]),
                hour_records=hr_arr,
                scheduled_hours=(
                    ta["scheduled_hours"] if "scheduled_hours" in ta else 0
                ),
            )
            day_records.append(day)

        return Attendance(day_records=day_records, payroll_group=payroll_group)

    """ PayrollGroup = {
        "pay_start_date": YYYY-MM-DD,
        "pay_end_date": YYYY-MM-DD,
        "attendance_start_date": YYYY-MM-DD,
        "attendance_end_date": YYYY-MM-DD,
        "posting_date": YYYY-MM-DD,
        "pay_run": YYYY-MM-DD,
        "cutoff": YYYY-MM-DD,
        "cutoff2": YYYY-MM-DD,

        "hrs_per_day": float,
        "day_factor": float,

        "pay_frequency": "MONTHLY"|"SEMI_MONTHLY"|"FORTNIGHTLY"|"WEEKLY",

        "contribution_schedules": {
            "SSS": "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
            "HDMF": "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
            "PHILHEALTH": "EVERY_PAY"|"FIRST_PAY"|"SECOND_PAY",
        },
        "hour_type_multiplier":{
            "REGULAR": 1,
            "PAID_LEAVE": 1,
            "UNPAID_LEAVE": 1,
            "UNDERTIME": 1,
            "ABSENT": 1,
            "UH": 1,
            "RD": 1.30,
            "SH": 1.30,
            "RD+SH": 1.50,
            "RH": 2.0,
            "RD+RH": 2.60,
            "RH+SH": 2.60,
            "RD+RH+SH": 3.0,
            "2RH": 3.0,
            "RD+2RH": 3.90,
            "OT": 1.25,
            "RD+OT": 1.69,
            "SH+OT": 1.69,
            "RD+SH+OT": 1.95,
            "RH+OT": 2.60,
            "RD+RH+OT": 3.38,
            "RH+SH+OT": 3.38,
            "RD+RH+SH+OT": 3.90,
            "2RH+OT": 3.90,
            "RD+2RH+OT": 5.070,
            "NT": 1.10,
            "RD+NT": 1.43,
            "SH+NT": 1.43,
            "RD+SH+NT": 1.65,
            "RH+NT": 2.20,
            "RD+RH+NT": 2.86,
            "RH+SH+NT": 2.86,
            "RD+RH+SH+NT": 3.30,
            "2RH+NT": 3.30,
            "RD+2RH+NT": 4.29,
            "NT+OT": 1.375,
            "RD+NT+OT": 1.859,
            "SH+NT+OT": 1.859,
            "RD+SH+NT+OT": 2.145,
            "RH+NT+OT": 2.86,
            "RD+RH+NT+OT": 3.718,
            "RH+SH+NT+OT": 3.718,
            "RD+RH+SH+NT+OT": 4.29,
            "2RH+NT+OT": 4.29,
            "RD+2RH+NT+OT": 5.577,
        },
    }"""

    @classmethod
    def _load_payroll_group(cls, payroll: Dict, employee: Dict) -> FinalPayrollGroup:
        payroll_group = employee.get("payrollGroup", {})
        termination_information = employee.get("terminationInformation", {}).get(
            "attributes", {}
        )
        contribution_schedules = payroll_group.get("contributionSchedules", {})

        pay_start_date = termination_information["start_date_basis"]
        pay_end_date = termination_information["last_date"]
        attendance_start_date = termination_information["start_date_basis"]
        attendance_end_date = termination_information["last_date"]
        cutoff = payroll_group["cut_off_date"].split(" ")[0]
        cutoff_is_eom = payroll_group["fcd_end_of_month"]
        posting_date1 = payroll_group["pay_run_posting"].split(" ")[0]
        posting_date1_is_eom = payroll_group["fpp_end_of_month"]
        cutoff2 = payroll_group["cut_off_date_2"].split(" ")[0]\
                    if payroll_group.get("cut_off_date_2", None) is not None else ''
        cutoff2_is_eom = payroll_group["scd_end_of_month"]
        posting_date2 = payroll_group["pay_run_posting_2"].split(" ")[0]\
                    if payroll_group.get("pay_run_posting_2", None) is not None else ''
        posting_date2_is_eom = payroll_group["spp_end_of_month"]
        compute_overbreak = payroll_group.get("compute_overbreak", False)
        compute_undertime = payroll_group.get("compute_undertime", True)
        compute_tardiness = payroll_group.get("compute_tardiness", True)

        return FinalPayrollGroup(
            pay_start_date=cls.___str_to_date(pay_start_date),
            pay_end_date=cls.___str_to_date(pay_end_date),
            attendance_start_date=cls.___str_to_date(attendance_start_date),
            attendance_end_date=cls.___str_to_date(attendance_end_date),
            # posting_date=cls.___str_to_date(parr["posting_date"]),
            pay_run=cls.___str_to_date(payroll["payRunDate"]),
            pay_frequency=PayFrequency(payroll_group["payroll_frequency"]),
            contribution_schedules={
                x: ContributionSchedule(contribution_schedules[x.value])
                for x in ContributionEnum
            },
            hrs_per_day=float(payroll_group["hours_per_day"]),
            day_factor=float(payroll_group["day_factor"]),
            cutoff=cls.___str_to_date(cutoff),
            cutoff_is_eom=bool(cutoff_is_eom),
            posting_date1=cls.___str_to_date(posting_date1),
            posting_date1_is_eom=bool(posting_date1_is_eom),
            cutoff2=cls.___str_to_date(cutoff2),
            cutoff2_is_eom=bool(cutoff2_is_eom),
            posting_date2=cls.___str_to_date(posting_date2),
            posting_date2_is_eom=bool(posting_date2_is_eom),
            hour_type_multiplier=payroll["dayHourRates"],
            compute_overbreak=compute_overbreak,
            compute_undertime=compute_undertime,
            compute_tardiness=compute_tardiness,
        )

    @classmethod
    def ___str_to_date(cls, string_date: str) -> date:
        try:
            return datetime.strptime(string_date, "%Y-%m-%d").date()
        except ValueError:
            return None