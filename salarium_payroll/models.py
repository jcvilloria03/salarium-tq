# -*- coding: utf-8 -*-

from sqlalchemy import Column, Date, Enum, TIMESTAMP, text
from sqlalchemy.dialects.mysql import INTEGER, FLOAT
from sqlalchemy.ext.declarative import declarative_base

from .constants import PayFrequency, SSSEmployeeType


class BaseModel(object):
    pass
    # @declared_attr
    # def __tablename__(cls):
    #     return cls.__name__.lower()

    # __table_args__ = {'mysql_engine': 'InnoDB'}

    # id =  Column(Integer, primary_key=True)


Base = declarative_base(cls=BaseModel)


class TaxWitholdingLookup(Base):
    __tablename__ = "tax_witholding_lookup"

    # don't use Numeric it will be converted to DECIMAL
    # FLOAT(53) not decimal because you need a custom json decoder in python for decimals to be transcoded
    id = Column(INTEGER(unsigned=True), primary_key=True)
    pay_frequency = Column(Enum(PayFrequency), nullable=False)
    min = Column(FLOAT(53, unsigned=True), nullable=False)
    max = Column(FLOAT(53, unsigned=True), nullable=False)
    fixed_amount = Column(FLOAT(53, unsigned=True), nullable=False)
    tax_rate = Column(FLOAT(unsigned=True), nullable=False)
    compensation_level_amount = Column(FLOAT(53, unsigned=True), nullable=False)
    effectivity_start_date = Column(Date, nullable=False)
    effectivity_end_date = Column(Date)
    created_at = Column(TIMESTAMP, server_default=text("CURRENT_TIMESTAMP"))
    updated_at = Column(
        TIMESTAMP, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    )
    deleted_at = Column(TIMESTAMP, default=None)


class SSSLookup(Base):
    __tablename__ = "sss_lookup"

    id = Column(INTEGER(unsigned=True), primary_key=True)
    employee_type = Column(Enum(SSSEmployeeType), nullable=False)
    min = Column(FLOAT(53, unsigned=True), nullable=False)
    max = Column(FLOAT(53, unsigned=True), nullable=False)
    monthly_salary_credit_amount = Column(FLOAT(53, unsigned=True), nullable=False)
    employee_share_amount = Column(FLOAT(53, unsigned=True), nullable=False)
    employer_share_amount = Column(
        FLOAT(53, unsigned=True), nullable=False, server_default=text("0")
    )
    employee_compensation_amount = Column(FLOAT(53, unsigned=True), nullable=False)
    effectivity_start_date = Column(Date, nullable=False)
    effectivity_end_date = Column(Date)
    created_at = Column(TIMESTAMP, server_default=text("CURRENT_TIMESTAMP"))
    updated_at = Column(
        TIMESTAMP, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    )
    deleted_at = Column(TIMESTAMP, default=None)


class HDMFLookup(Base):
    __tablename__ = "hdmf_lookup"

    id = Column(INTEGER(unsigned=True), primary_key=True)
    min = Column(FLOAT(53, unsigned=True), nullable=False)
    max = Column(FLOAT(53, unsigned=True), nullable=False)
    employee_share_multiplier = Column(FLOAT(unsigned=True), nullable=False)
    employer_share_multiplier = Column(FLOAT(unsigned=True), nullable=False)
    effectivity_start_date = Column(Date, nullable=False)
    effectivity_end_date = Column(Date)
    created_at = Column(TIMESTAMP, server_default=text("CURRENT_TIMESTAMP"))
    updated_at = Column(
        TIMESTAMP, server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    )
    deleted_at = Column(TIMESTAMP, default=None)
