# -*- coding: utf-8 -*-

from marshmallow import Schema, fields, post_load


class AdjustmentSettingsAmountSchema(Schema):
    type = fields.Str()
    amount = fields.Float()


class AdjustmentSettingsReleaseDetailsSchema(Schema):
    id = fields.Integer()
    date = fields.Date()
    disburse_through_special_pay_run = fields.Boolean()
    status = fields.Boolean()


class AdjustmentSettingsSchema(Schema):
    type = fields.Str()
    name = fields.Str()
    reason = fields.Str()
    category = fields.Str()
    fully_taxable = fields.Boolean()
    max_non_taxable = fields.Float(allow_none=True)

    frequency_type = fields.Str()
    frequency_schedule = fields.Str(allow_none=True)

    amount_details = fields.Nested(AdjustmentSettingsAmountSchema)

    release_details = fields.List(fields.Nested(AdjustmentSettingsReleaseDetailsSchema))

    meta = fields.Dict(keys=fields.Str(), values=fields.Raw())


class CommissionSettingsAmountSchema(Schema):
    type = fields.Str()
    amount = fields.Float()


class CommissionSettingsReleaseDetailsSchema(Schema):
    id = fields.Integer()
    date = fields.Date()
    disburse_through_special_pay_run = fields.Boolean()
    status = fields.Boolean()


class CommissionSettingsSchema(Schema):
    type = fields.Str()
    name = fields.Str()
    fully_taxable = fields.Boolean()
    max_non_taxable = fields.Float(allow_none=True)

    frequency_type = fields.Str()
    frequency_schedule = fields.Str(allow_none=True)

    amount_details = fields.Nested(CommissionSettingsAmountSchema)
    # {
    #     type = fields.Str()
    #     amount = fields.Float()
    # }
    release_details = fields.List(fields.Nested(CommissionSettingsReleaseDetailsSchema))
    # [
    #     {
    #         id = 225
    #         date = "2019-07-16"
    #         disburse_through_special_pay_run = false
    #         status = false
    #     }
    # ]
    meta = fields.Dict(keys=fields.Str(), values=fields.Raw())
    # {
    #     id = 187
    #     subtype_id = 22
    #     subtype_name = fields.Str()
    #     other_income_type_id = 2970
    #     other_income_type_subtype_type_id = 722
    # }


class DeductionSettingsAmountSchema(Schema):
    type = fields.Str()
    amount = fields.Float()


class DeductionSettingsValiditySchema(Schema):
    valid_from = fields.Date()
    valid_to = fields.Date(allow_none=True)


class DeductionSettingsSchema(Schema):
    type = fields.Str()
    name = fields.Str()
    fully_taxable = fields.Boolean(allow_none=True)
    max_non_taxable = fields.Float(allow_none=True)

    frequency_type = fields.Str()
    frequency_schedule = fields.Str(allow_none=True)

    amount_details = fields.Nested(DeductionSettingsAmountSchema)

    validity = fields.Nested(DeductionSettingsValiditySchema)

    meta = fields.Dict(keys=fields.Str(), values=fields.Raw())

    @post_load
    def make_cls(self, data, **kwargs):
        return data


class AllowanceSettingsAmountSchema(Schema):
    type = fields.Str()
    amount = fields.Float()
    prorated = fields.Boolean()
    prorated_by = fields.Str(allow_none=True)
    entitled_when = fields.Str(allow_none=True)


class AllowanceSettingsValiditySchema(Schema):
    valid_from = fields.Date()
    valid_to = fields.Date(allow_none=True)


class AllowanceSettingsReleaseDetailsSchema(Schema):
    id = fields.Integer()
    disburse_through_special_pay_run = fields.Boolean()
    status = fields.Boolean()


class AllowanceSettingsSchema(Schema):
    type = fields.Str()
    name = fields.Str()
    fully_taxable = fields.Boolean(allow_none=True)
    tax_option = fields.Str()  # fully_taxable, partially_non_taxable, fully_non_taxable, reduce_to_taxable_income. 
    max_non_taxable = fields.Float(allow_none=True)

    frequency_type = fields.Str()
    frequency_schedule = fields.Str(allow_none=True)

    amount_details = fields.Nested(AllowanceSettingsAmountSchema)

    validity = fields.Nested(AllowanceSettingsValiditySchema)

    release_details = fields.List(fields.Nested(AllowanceSettingsReleaseDetailsSchema))

    meta = fields.Dict(keys=fields.Str(), values=fields.Raw())


class BonusSettingsReleaseDetailsSchema(Schema):
    id = fields.Integer()
    date = fields.Date()
    disburse_through_special_pay_run = fields.Boolean()
    status = fields.Boolean()
    # should be boolean
    prorate_based_on_tenure = fields.Boolean(allow_none=True)
    coverage_from = fields.Date(allow_none=True)
    coverage_to = fields.Date(allow_none=True)
    amount = fields.Float(allow_none=True)


class BonusSettingsAmountSchema(Schema):
    type = fields.Str()
    basis = fields.Str()
    amount = fields.Float()
    percentage = fields.Float(allow_none=True)


class BonusSettingsSchema(Schema):
    type = fields.Str()
    name = fields.Str()
    fully_taxable = fields.Boolean(allow_none=True)
    max_non_taxable = fields.Float(allow_none=True)

    amount_details = fields.Nested(BonusSettingsAmountSchema)

    frequency_type = fields.Str()
    frequency_schedule = fields.Str(allow_none=True)

    release_details = fields.List(fields.Nested(BonusSettingsReleaseDetailsSchema))

    meta = fields.Dict(keys=fields.Str(), values=fields.Raw())
