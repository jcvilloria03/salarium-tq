# -*- coding: utf-8 -*-

import json
from dataclasses import dataclass, field
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from typing import Any, Dict, List, Optional, Tuple

from .attendance import Attendance
from .base import BasePay, BasePayHistory, EmployeeDetails, PayrollGroup
from .constants import (
    AdjustmentType,
    ContributionEnum,
    DeminimisType,
    PayFrequency,
    PayRate,
    TaxType,
    TransactionType,
    FinalPayItemType,
)
from .contributions import (
    ContributionIncomeType,
    ContributionRecord,
    HDMFContribution,
    PhilHealthContribution,
    SSSContribution,
)
from .income import AdjustmentRecord, Deduction, DeMinimisRecord, OtherIncomeRecord
from .loan import CreditedLoan, Loan
from .tax import Tax

__all__ = ["NoComputationDoneYet", "BasicPay", "NetPay"]

# BasePay           - barebones pay info (amt, rate, effective_dt)
#                   - have a converter for converting to different pay rate
# BasePayHistory    - will hold historical records of base pays
#                   - should be able to give a pay for by date
# BasicPay          - handles basic pay adjustments and attendance related changes
#                   - basic pay adjustments in between pay period
#                   - undertime deductions
#                   - absent
#                   - overtime
#                       - night diff
#                       - holiday entitlements
#                       - holiday work
#                   -leaves
#                       - paid
#                       - unpaid
#                   - start of json record output
#
# NetPay            - BasicPay with TAXABLE and NON-TAXABLE bonuses, adjustments, commissions, allowances, deminimis, and loan_amortizations
#


class NoComputationDoneYet(RuntimeError):
    pass


@dataclass
class BasicPay:
    payroll_group: PayrollGroup
    attendance: Attendance = field(repr=False, compare=False)
    base_pay_history: BasePayHistory = field(repr=False, compare=False)

    # TODO: not yet used
    emp_day_factor: Optional[float] = field(default=None, init=False, compare=False)
    emp_hrs_per_day: Optional[float] = field(default=None, init=False, compare=False)

    breakdown: Dict = field(default_factory=dict, init=False)

    autorun_compute: bool = True

    leave_credits: Dict = field(default=dict)

    def __post_init__(self):
        if self.autorun_compute:
            self.compute()

    @property
    def day_factor(self) -> float:
        return (
            self.emp_day_factor
            if self.emp_day_factor
            else self.payroll_group.day_factor
        )

    @property
    def hrs_per_day(self) -> float:
        return (
            self.emp_hrs_per_day
            if self.emp_hrs_per_day
            else self.payroll_group.hrs_per_day
        )

    @property
    def amount(self) -> float:
        # should be THE WORK PAY amount
        # full_pay - undertime - unpaid_leave - absences
        #   + overtime (w/nt) + rest_day_work + holiday_work (partial) + nt (partial)
        return (
            self.full_pay_amount
            - self.undertime_deduction_amount
            - self.tardy_deduction_amount
            - self.overbreak_deduction_amount
            - self.absent_deduction_amount
            - self.unpaid_leave_deduction_amount
            + self.leave_conversion_amount
            + self.overtime_pay_amount
            + self.rest_day_work_pay_amount
            + self.holiday_work_add_amount
            + self.night_differential_add_amount
        )

    @property
    def gross_basic_amount(self) -> float:
        # refers to the gross basic received by the employees within the coverage period
        # (includes full pay, absences, tardiness, overbreak, undertime, unpaid leaves,
        # overtime pay, holiday pay and rest day pay)
        return self.amount

    @property
    def basic_pay_amount(self) -> float:
        # refers to the salary received by the employee within the coverage period
        # which includes the full pay less absences, tardiness, undertime, overbreak
        # and unpaid leave
        return (
            self.full_pay_amount
            - self.undertime_deduction_amount
            - self.tardy_deduction_amount
            - self.overbreak_deduction_amount
            - self.absent_deduction_amount
            - self.unpaid_leave_deduction_amount
        )

    @property
    def full_pay_amount(self) -> float:
        return self.__get_from_breakdown_section("full_pay", "total_amount")

    @property
    def rest_day_work_pay_amount(self) -> float:
        return self.__get_from_breakdown_section("rest_day_work", "total_amount")

    @property
    def holiday_work_add_amount(self) -> float:
        return self.__get_from_breakdown_section("holiday_work", "total_add_amount")

    @property
    def night_differential_add_amount(self) -> float:
        return self.__get_from_breakdown_section(
            "night_differential", "total_add_amount"
        )

    @property
    def overtime_pay_amount(self) -> float:
        return self.__get_from_breakdown_section("overtime", "total_amount")

    @property
    def absent_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("absent", "total_amount")

    @property
    def unpaid_leave_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("unpaid_leave", "total_amount")

    @property
    def undertime_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("undertime", "total_amount")

    @property
    def tardy_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("tardy", "total_amount")

    @property
    def overbreak_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("overbreak", "total_amount")

    @property
    def undertime_hours(self) -> float:
        return self.__get_from_breakdown_section("undertime", "total_hours")

    @property
    def undertime_records(self) -> List:
        return self.__get_from_breakdown_section("undertime", "records")

    @property
    def leave_conversion_amount(self) -> List:
        return self.__get_from_breakdown_section("leave_conversion", "total_amount")

    def compute(self) -> None:
        # > should compute for all basic adjustments and attendance
        # related additions/deductions
        # > should keep a dict of a breakdown of data wherein if all of the records
        # are added it should equal the "Work Pay"
        # > every record should be cached and will have to call this method to update it
        self.compute_full_pay()
        self.compute_attendance()

    def compute_attendance(self) -> None:
        # payslip: -
        self.compute_undertime()
        self.compute_tardy()
        self.compute_overbreak()
        self.compute_unpaid_leave()
        self.compute_absences()
        # payslip: -+
        self.compute_paid_leave()
        self.compute_unworked_holiday()
        # payslip: -+* special: -daily_rate +regular_day_part +[computed]_day_part
        self.compute_holiday_work()
        self.compute_night_differential()
        # payslip: +*
        self.compute_rest_day_work()
        self.compute_overtime()

    def compute_undertime(self) -> None:
        self.breakdown["undertime"] = None

        undertime_records: List = []
        undertime_total_hours: float = 0
        undertime_total_amount: float = 0

        if self.payroll_group.compute_undertime:
            for day_att in self.attendance.iter_undertime_records():
                pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                    day_att.date
                )
                hr_amount = day_att.undertime_hours * pay_for_the_day.hourly_rate_amount
                # should only have 1 undertime in a day
                undertime_total_hours += day_att.undertime_hours
                undertime_total_amount += hr_amount
                # payslip: -
                undertime_records.append(
                    {
                        "date": day_att.date,
                        "hours": day_att.undertime_hours,
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_amount,
                    }
                )

        undertime_summary: Dict = (
            {
                "UNDERTIME": {
                    "total_hours": undertime_total_hours,
                    "total_amount": undertime_total_amount,
                }
            }
            if undertime_total_hours
            else {}
        )

        self.breakdown["undertime"] = {
            "records": undertime_records,
            "summary": undertime_summary,
            "total_hours": undertime_total_hours,
            "total_amount": undertime_total_amount,
        }

    def compute_tardy(self) -> None:
        self.breakdown["tardy"] = None

        tardy_records: List = []
        tardy_total_hours: float = 0
        tardy_total_amount: float = 0

        if self.payroll_group.compute_tardiness:
            for day_att in self.attendance.iter_tardy_records():
                pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                    day_att.date
                )
                hr_amount = day_att.tardy_hours * pay_for_the_day.hourly_rate_amount
                # should only have 1 tardy in a day
                tardy_total_hours += day_att.tardy_hours
                tardy_total_amount += hr_amount
                # payslip: -
                tardy_records.append(
                    {
                        "date": day_att.date,
                        "hours": day_att.tardy_hours,
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_amount,
                    }
                )

        tardy_summary: Dict = (
            {
                "TARDY": {
                    "total_hours": tardy_total_hours,
                    "total_amount": tardy_total_amount,
                }
            }
            if tardy_total_hours
            else {}
        )

        self.breakdown["tardy"] = {
            "records": tardy_records,
            "summary": tardy_summary,
            "total_hours": tardy_total_hours,
            "total_amount": tardy_total_amount,
        }

    def compute_overbreak(self) -> None:
        self.breakdown["overbreak"] = None

        overbreak_records: List = []
        overbreak_total_hours: float = 0
        overbreak_total_amount: float = 0

        if self.payroll_group.compute_overbreak:
            for day_att in self.attendance.iter_overbreak_records():
                pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                    day_att.date
                )
                hr_amount = day_att.overbreak_hours * pay_for_the_day.hourly_rate_amount
                # should only have 1 overbreak in a day
                overbreak_total_hours += day_att.overbreak_hours
                overbreak_total_amount += hr_amount
                # payslip: -
                overbreak_records.append(
                    {
                        "date": day_att.date,
                        "hours": day_att.overbreak_hours,
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_amount,
                    }
                )

        overbreak_summary: Dict = (
            {
                "OVERBREAK": {
                    "total_hours": overbreak_total_hours,
                    "total_amount": overbreak_total_amount,
                }
            }
            if overbreak_total_hours
            else {}
        )

        self.breakdown["overbreak"] = {
            "records": overbreak_records,
            "summary": overbreak_summary,
            "total_hours": overbreak_total_hours,
            "total_amount": overbreak_total_amount,
        }

    def compute_unpaid_leave(self) -> None:
        self.breakdown["unpaid_leave"] = None

        unpaid_leave_records: List = []
        unpaid_leave_total_hours: float = 0
        unpaid_leave_total_amount: float = 0

        for day_att in self.attendance.iter_unpaid_leave_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            hr_amount = day_att.unpaid_leave_hours * pay_for_the_day.hourly_rate_amount
            # should only have 1 unpaid leave in a day; can be non full day
            unpaid_leave_total_hours += day_att.unpaid_leave_hours
            unpaid_leave_total_amount += hr_amount
            # payslip: -
            unpaid_leave_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.unpaid_leave_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        unpaid_leave_summary: Dict = (
            {
                "UNPAID_LEAVE": {
                    "total_hours": unpaid_leave_total_hours,
                    "total_amount": unpaid_leave_total_amount,
                }
            }
            if unpaid_leave_total_hours
            else {}
        )

        self.breakdown["unpaid_leave"] = {
            "records": unpaid_leave_records,
            "summary": unpaid_leave_summary,
            "total_hours": unpaid_leave_total_hours,
            "total_amount": unpaid_leave_total_amount,
        }

    def compute_absences(self) -> None:
        self.breakdown["absent"] = None

        absent_records: List = []
        absent_total_hours: float = 0
        absent_total_amount: float = 0

        for day_att in self.attendance.iter_absent_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # even if whole day we need to compute for hourly in case for hourly rate emp
            hr_amount = day_att.absent_hours * pay_for_the_day.hourly_rate_amount
            absent_total_hours += day_att.absent_hours
            absent_total_amount += hr_amount
            # payslip: -
            absent_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.absent_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        if absent_total_amount > self.breakdown["full_pay"]["total_amount"]:
            diff_amount = float(absent_total_amount - self.breakdown["full_pay"]["total_amount"])
            absent_total_amount -= diff_amount
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                date.today()
            )
            absent_records.append(
                {
                    "date": date.today(),
                    "hours": 0.0,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": diff_amount,
                }
            )

        absent_summary: Dict = (
            {
                "ABSENT": {
                    "total_hours": absent_total_hours,
                    "total_amount": absent_total_amount,
                }
            }
            if absent_total_hours
            else {}
        )

        self.breakdown["absent"] = {
            "records": absent_records,
            "summary": absent_summary,
            "total_hours": absent_total_hours,
            "total_amount": absent_total_amount,
        }

    def compute_paid_leave(self) -> None:
        self.breakdown["paid_leave"] = None

        paid_leave_records: List = []
        paid_leave_total_hours: float = 0
        paid_leave_total_amount: float = 0
        for day_att in self.attendance.iter_paid_leave_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            hr_amount = day_att.paid_leave_hours * pay_for_the_day.hourly_rate_amount
            # should only have 1 paid leave in a day; can be non full day
            paid_leave_total_hours += day_att.paid_leave_hours
            paid_leave_total_amount += (
                day_att.paid_leave_hours * pay_for_the_day.hourly_rate_amount
            )
            # payslip: -+
            paid_leave_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.paid_leave_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        paid_leave_summary: Dict = (
            {
                "PAID_LEAVE": {
                    "total_hours": paid_leave_total_hours,
                    "total_amount": paid_leave_total_amount,
                }
            }
            if paid_leave_total_hours
            else {}
        )

        self.breakdown["paid_leave"] = {
            "records": paid_leave_records,
            "summary": paid_leave_summary,
            "total_hours": paid_leave_total_hours,
            "total_amount": paid_leave_total_amount,
        }

    def compute_unworked_holiday(self) -> None:
        self.breakdown["unworked_holiday"] = None

        unworked_holiday_records: List = []
        unworked_holiday_total_hours: float = 0
        unworked_holiday_total_amount: float = 0
        for day_att in self.attendance.iter_unworked_holiday_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            hr_amount = (
                day_att.unworked_holiday_hours * pay_for_the_day.hourly_rate_amount
            )
            unworked_holiday_total_hours += day_att.unworked_holiday_hours
            unworked_holiday_total_amount += hr_amount
            # payslip: -+
            unworked_holiday_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.unworked_holiday_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        unworked_holiday_summary: Dict = (
            {
                "UH": {
                    "total_hours": unworked_holiday_total_hours,
                    "total_amount": unworked_holiday_total_amount,
                }
            }
            if unworked_holiday_total_hours
            else {}
        )

        self.breakdown["unworked_holiday"] = {
            "records": unworked_holiday_records,
            "summary": unworked_holiday_summary,
            "total_hours": unworked_holiday_total_hours,
            "total_amount": unworked_holiday_total_amount,
        }

    def compute_holiday_work(self) -> None:
        # holiday work is DIFFERENT from plain holiday
        # work rendered on a PURE holiday (non NT/OT)
        self.breakdown["holiday_work"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        h_records: List = []
        h_summary: Dict = {}
        h_total_hours: float = 0
        h_total_amount: float = 0
        h_total_add_amount: float = 0
        for day_att in self.attendance.iter_holiday_work_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # should only have 1 RD in a day, others are either NT and/or OT,
            # but we need to dive deep to get the type
            for hr_att in day_att.iter_holiday_work_records():
                hr_amount = hr_att.hours * pay_for_the_day.hourly_rate_amount
                hr_total_amount = hr_amount * multiplier[hr_att.hour_type]
                hr_total_add_amount = hr_amount * (multiplier[hr_att.hour_type] - 1)

                h_total_hours += hr_att.hours
                h_total_amount += hr_total_amount
                h_total_add_amount += hr_total_add_amount

                # payslip: -+* special: -daily_rate +regular_day_part +HOLIDAY_day_part
                h_records.append(
                    {
                        "date": day_att.date,
                        "scheduled_hours": day_att.scheduled_hours,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                        "add_amount": hr_total_add_amount,
                    }
                )

                if hr_att.hour_type not in h_summary:
                    h_summary[hr_att.hour_type] = {
                        "total_hours": 0,
                        "total_amount": 0,
                        "total_add_amount": 0,
                    }

                h_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                h_summary[hr_att.hour_type]["total_amount"] += hr_total_amount
                h_summary[hr_att.hour_type]["total_add_amount"] += hr_total_add_amount

        self.breakdown["holiday_work"] = {
            "records": h_records,
            "summary": h_summary,
            "total_hours": h_total_hours,
            "total_amount": h_total_amount,
            "total_add_amount": h_total_add_amount,
        }

    def compute_night_differential(self) -> None:
        self.breakdown["night_differential"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        nt_records: List = []
        nt_summary: Dict = {}
        nt_total_hours: float = 0
        nt_total_amount: float = 0
        nt_total_add_amount: float = 0
        for day_att in self.attendance.iter_night_differential_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # should only have 1 NT in a day, but we need to dive deep to get the type
            for hr_att in day_att.iter_night_differential_records():
                hr_amount = hr_att.hours * pay_for_the_day.hourly_rate_amount
                hr_total_amount = hr_amount * multiplier[hr_att.hour_type]
                # NOTE: will consider full multiplier (no alteration) if rest_day
                hr_total_add_amount = hr_amount * (
                    multiplier[hr_att.hour_type] - (0 if day_att.has_rest_day else 1)
                )

                nt_total_hours += hr_att.hours
                nt_total_amount += hr_total_amount
                nt_total_add_amount += hr_total_add_amount

                # payslip: -+* special: -daily_rate +regular_day_part +NT_day_part
                nt_records.append(
                    {
                        "date": day_att.date,
                        "scheduled_hours": day_att.scheduled_hours,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                        "add_amount": hr_total_add_amount,
                    }
                )

                if hr_att.hour_type not in nt_summary:
                    nt_summary[hr_att.hour_type] = {
                        "total_hours": 0,
                        "total_amount": 0,
                        "total_add_amount": 0,
                    }

                nt_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                nt_summary[hr_att.hour_type]["total_amount"] += hr_total_amount
                nt_summary[hr_att.hour_type]["total_add_amount"] += hr_total_add_amount

        self.breakdown["night_differential"] = {
            "records": nt_records,
            "summary": nt_summary,
            "total_hours": nt_total_hours,
            "total_amount": nt_total_amount,
            "total_add_amount": nt_total_add_amount,
        }

    def compute_rest_day_work(self) -> None:
        # work rendered on a PURE rest_day (non NT/OT)
        self.breakdown["rest_day_work"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        rd_records: List = []
        rd_summary: Dict = {}
        rd_total_hours: float = 0
        rd_total_amount: float = 0
        for day_att in self.attendance.iter_rest_day_work_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # should only have 1 RD in a day, others are either NT and/or OT,
            # but we need to dive deep to get the type
            for hr_att in day_att.iter_rest_day_work_records():
                hr_total_amount = (
                    hr_att.hours * pay_for_the_day.hourly_rate_amount
                ) * multiplier[hr_att.hour_type]

                rd_total_hours += hr_att.hours
                rd_total_amount += hr_total_amount
                # payslip: +*
                rd_records.append(
                    {
                        "date": day_att.date,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                    }
                )

                if hr_att.hour_type not in rd_summary:
                    rd_summary[hr_att.hour_type] = {"total_hours": 0, "total_amount": 0}

                rd_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                rd_summary[hr_att.hour_type]["total_amount"] += hr_total_amount

        self.breakdown["rest_day_work"] = {
            "records": rd_records,
            "summary": rd_summary,
            "total_hours": rd_total_hours,
            "total_amount": rd_total_amount,
        }

    def compute_overtime(self) -> None:
        # overtime here covers OT and NT+OT
        self.breakdown["overtime"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        ot_records: List = []
        ot_summary: Dict = {}
        ot_total_hours: float = 0
        ot_total_amount: float = 0
        for day_att in self.attendance.iter_overtime_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            for hr_att in day_att.iter_overtime_records():
                hr_total_amount = (
                    hr_att.hours * pay_for_the_day.hourly_rate_amount
                ) * multiplier[hr_att.hour_type]

                ot_total_hours += hr_att.hours
                ot_total_amount += hr_total_amount
                # payslip: +
                ot_records.append(
                    {
                        "date": day_att.date,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                    }
                )

                if hr_att.hour_type not in ot_summary:
                    ot_summary[hr_att.hour_type] = {"total_hours": 0, "total_amount": 0}

                ot_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                ot_summary[hr_att.hour_type]["total_amount"] += hr_total_amount

        self.breakdown["overtime"] = {
            "records": ot_records,
            "summary": ot_summary,
            "total_hours": ot_total_hours,
            "total_amount": ot_total_amount,
        }

    def compute_full_pay(self) -> None:
        self.breakdown["full_pay"] = {}
        compute_basis: str = "NO_ADJUSTMENT_WITHIN_PAYROLL"

        # calculate prorated if also force_compute_daily is True
        if self.base_pay_history.has_pay_adjustments\
            or self.payroll_group.force_compute_daily is True:
            total_amount, compute_basis = self._calc_prorated_full_pay_amount()
        else:
            total_amount, compute_basis = self._calc_one_time_full_pay_amount()

        self.breakdown["full_pay"] = {
            "records": self.base_pay_history.records,
            "latest_base_pay": self.base_pay_history.latest_base_pay,
            "compute_basis": compute_basis,
            "total_amount": total_amount,
        }

    def _calc_one_time_full_pay_amount(self) -> Tuple[float, str]:
        pay_rate = self.base_pay_history.records[0].pay_rate

        if pay_rate in [PayRate.DAILY, PayRate.HOURLY]:
            return (
                self._calc_one_time_full_pay_amount_for_daily_or_hourly_rate(),
                "NO_ADJUSTMENT_WITHIN_PAYROLL_DAILY_HOURLY",
            )
        else:
            return (
                self._calc_one_time_full_pay_amount_for_annual_or_monthly_rate(),
                "NO_ADJUSTMENT_WITHIN_PAYROLL_ANNUAL_MONTHLY",
            )

    def _calc_one_time_full_pay_amount_for_daily_or_hourly_rate(self) -> float:
        bp = self.base_pay_history.records[0]
        return sum(
            bp.hourly_rate_amount * att_day.scheduled_hours
            for att_day in self.attendance.iter_work_day_records()
        )

    def _calc_one_time_full_pay_amount_for_annual_or_monthly_rate(self) -> float:
        bp = self.base_pay_history.records[0]
        return bp.convert_amount_by_pay_frequency(self.payroll_group.pay_frequency)

    def calc_one_time_full_pay_amount_for_annual_or_monthly_rate(self) -> float:
        bp = self.base_pay_history.records[0]
        return bp.convert_amount_by_pay_frequency(self.payroll_group.pay_frequency)

    def _calc_prorated_full_pay_amount(self) -> Tuple[float, str]:
        pay_rate: PayRate = self.base_pay_history.oldest_base_pay.pay_rate

        if pay_rate in [PayRate.MONTHLY, PayRate.YEARLY]\
            and self.payroll_group.force_compute_daily is False:
            return (
                self._calc_prorated_full_pay_amount_for_annual_or_monthly_rate(),
                "ADJUSTMENT_WITHIN_PAYROLL_ANNUAL_MONTHLY",
            )

        return (
            self._calc_prorated_full_pay_amount_for_daily_or_hourly_rate(),
            "ADJUSTMENT_WITHIN_PAYROLL_DAILY_HOURLY",
        )

    def _calc_prorated_full_pay_amount_for_daily_or_hourly_rate(self) -> float:
        # NOTE: need to compute using hourly with scheduled hours, see below:
        #   daily_rate is not enough here because the current shift hours can
        #   change so we really depend on the TA data here for accuracy
        return sum(
            self.base_pay_history.get_pay_for_the_day(att.date).hourly_rate_amount
            * att.scheduled_hours
            for att in self.attendance.iter_work_day_records()
        )

    def _calc_prorated_full_pay_amount_for_annual_or_monthly_rate(self) -> float:
        # get oldest rate and full_pay equiv amount of that (old_bp_full_pay_amt)
        old_bp: BasePay = self.base_pay_history.oldest_base_pay
        old_bp_full_pay_amt: float = old_bp.convert_amount_by_pay_frequency(
            self.payroll_group.pay_frequency
        )

        # old_bp_full_pay_amt - (old_bp_daily * # of non old_bp_days)
        #   + sum((new_bp_daily * new_bp_days))
        num_of_days_not_old_bp: int = 0
        sum_of_day_rate_of_new_bp: float = 0
        for att in self.attendance.iter_work_day_records():
            bp_of_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(att.date)

            if old_bp is not bp_of_the_day:
                num_of_days_not_old_bp += 1
                sum_of_day_rate_of_new_bp += bp_of_the_day.daily_rate_amount

        return (
            old_bp_full_pay_amt
            - (old_bp.daily_rate_amount * num_of_days_not_old_bp)
            + sum_of_day_rate_of_new_bp
        )

    def __get_from_breakdown_section(self, section: str, section_return: str) -> Any:
        if section not in self.breakdown:
            if self.autorun_compute:
                fun = getattr(self, f"compute_{section}")
                fun()
            else:
                raise NoComputationDoneYet(
                    f"No computation for '{section}' had been executed."
                )
        return self.breakdown[section][section_return]

    def compute_leave_conversion(self) -> None:
        base_pay: BasePay = self.base_pay_history.get_pay_for_the_day(
            self.payroll_group.pay_run
        )
        hours_per_day = self.payroll_group.hrs_per_day
        hourly_rate = base_pay.hourly_rate_amount

        total_leave_credit_amount = 0
        total_hours = 0
        units = set()

        for credit_item in self.leave_credits:
            unit = credit_item.get("unit")
            value = credit_item.get("value")

            # this will be used to determine the final unit to use
            units.add(unit)

            # get value in hours
            value = value if unit == "hours" else value * hours_per_day

            amount = hourly_rate * value
            credit_item["amount"] = amount
            total_leave_credit_amount += amount
            total_hours += value

        final_unit = units.pop() if len(units) == 1 else "hours"

        self.breakdown["leave_conversion"] = {
            "total_amount": total_leave_credit_amount,
            "total_hours": total_hours,
            "total_days": total_hours / hours_per_day,
            "unit": final_unit,
            "items": self.leave_credits,
        }


def _default_prev_contrib() -> Dict[str, ContributionRecord]:
    return {c: ContributionRecord() for c in ContributionEnum}


@dataclass
class FinalBasicPay:
    payroll_group: PayrollGroup
    attendance: Attendance = field(repr=False, compare=False)
    base_pay_history: BasePayHistory = field(repr=False, compare=False)

    # TODO: not yet used
    emp_day_factor: Optional[float] = field(default=None, init=False, compare=False)
    emp_hrs_per_day: Optional[float] = field(default=None, init=False, compare=False)

    breakdown: Dict = field(default_factory=dict, init=False)
    full_pays: List = field(default_factory=list, init=False)

    autorun_compute: bool = True

    def __post_init__(self):
        if self.autorun_compute:
            self.compute()

    def _derive_fullpay_periods(self):
        start = self.payroll_group.pay_start_date
        end = self.payroll_group.pay_end_date
        from_date = start + relativedelta(day=1, months=-6)
        to_date = end + relativedelta(day=1, months=6, days=-1)
        calendar = self.payroll_group.get_calendar_by_date_range(from_date, to_date)

        self.complete_periods = list(
            filter(lambda dtrange: start <= dtrange[0] < dtrange[1] <= end, calendar)
        )
        left = list(filter(lambda dtrange: dtrange[0] < start <= dtrange[1], calendar))
        right = list(filter(lambda dtrange: dtrange[0] <= end < dtrange[1], calendar))

        self.left_periods = [(start, left[0][1])] if len(left) else []
        self.right_periods = [(right[0][0], end)] if len(right) else []

    @property
    def day_factor(self) -> float:
        return (
            self.emp_day_factor
            if self.emp_day_factor
            else self.payroll_group.day_factor
        )

    @property
    def hrs_per_day(self) -> float:
        return (
            self.emp_hrs_per_day
            if self.emp_hrs_per_day
            else self.payroll_group.hrs_per_day
        )

    @property
    def amount(self) -> float:
        # should be THE WORK PAY amount
        # full_pay - undertime - unpaid_leave - absences
        #   + overtime (w/nt) + rest_day_work + holiday_work (partial) + nt (partial) + paid leave
        return (
            self.full_pay_amount
            - self.undertime_deduction_amount
            - self.tardy_deduction_amount
            - self.overbreak_deduction_amount
            - self.absent_deduction_amount
            - self.unpaid_leave_deduction_amount
            + self.overtime_pay_amount
            + self.rest_day_work_pay_amount
            + self.holiday_work_add_amount
            + self.night_differential_add_amount
        )

    @property
    def gross_basic_amount(self) -> float:
        # refers to the gross basic received by the employees within the coverage period
        # (includes full pay, absences, tardiness, overbreak, undertime, unpaid leaves,
        # overtime pay, holiday pay and rest day pay)
        return self.amount

    @property
    def basic_pay_amount(self) -> float:
        # refers to the salary received by the employee within the coverage period
        # which includes the full pay less absences, tardiness, undertime, overbreak
        # and unpaid leave
        return (
            self.full_pay_amount
            - self.undertime_deduction_amount
            - self.tardy_deduction_amount
            - self.overbreak_deduction_amount
            - self.absent_deduction_amount
            - self.unpaid_leave_deduction_amount
        )

    @property
    def full_pay_amount(self) -> float:
        return self.__get_from_breakdown_section("full_pay", "total_amount")

    @property
    def rest_day_work_pay_amount(self) -> float:
        return self.__get_from_breakdown_section("rest_day_work", "total_amount")

    @property
    def holiday_work_add_amount(self) -> float:
        return self.__get_from_breakdown_section("holiday_work", "total_add_amount")

    @property
    def night_differential_add_amount(self) -> float:
        return self.__get_from_breakdown_section(
            "night_differential", "total_add_amount"
        )

    @property
    def overtime_pay_amount(self) -> float:
        return self.__get_from_breakdown_section("overtime", "total_amount")

    @property
    def absent_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("absent", "total_amount")

    @property
    def unpaid_leave_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("unpaid_leave", "total_amount")

    @property
    def undertime_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("undertime", "total_amount")

    @property
    def tardy_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("tardy", "total_amount")

    @property
    def overbreak_deduction_amount(self) -> float:
        return self.__get_from_breakdown_section("overbreak", "total_amount")

    @property
    def undertime_hours(self) -> float:
        return self.__get_from_breakdown_section("undertime", "total_hours")

    @property
    def undertime_records(self) -> List:
        return self.__get_from_breakdown_section("undertime", "records")

    def compute(self) -> None:
        # > should compute for all basic adjustments and attendance
        # related additions/deductions
        # > should keep a dict of a breakdown of data wherein if all of the records
        # are added it should equal the "Work Pay"
        # > every record should be cached and will have to call this method to update it
        self.compute_full_pay()
        self.compute_attendance()

    def compute_attendance(self) -> None:
        # payslip: -
        self.compute_undertime()
        self.compute_tardy()
        self.compute_overbreak()
        self.compute_unpaid_leave()
        self.compute_absences()
        # payslip: -+
        self.compute_paid_leave()
        self.compute_unworked_holiday()
        # payslip: -+* special: -daily_rate +regular_day_part +[computed]_day_part
        self.compute_holiday_work()
        self.compute_night_differential()
        # payslip: +*
        self.compute_rest_day_work()
        self.compute_overtime()

    def compute_undertime(self) -> None:
        self.breakdown["undertime"] = None

        undertime_records: List = []
        undertime_total_hours: float = 0
        undertime_total_amount: float = 0

        if self.payroll_group.compute_undertime:
            for day_att in self.attendance.iter_undertime_records():
                pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                    day_att.date
                )
                hr_amount = day_att.undertime_hours * pay_for_the_day.hourly_rate_amount
                # should only have 1 undertime in a day
                undertime_total_hours += day_att.undertime_hours
                undertime_total_amount += hr_amount
                # payslip: -
                undertime_records.append(
                    {
                        "date": day_att.date,
                        "hours": day_att.undertime_hours,
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_amount,
                    }
                )

        undertime_summary: Dict = (
            {
                "UNDERTIME": {
                    "total_hours": undertime_total_hours,
                    "total_amount": undertime_total_amount,
                }
            }
            if undertime_total_hours
            else {}
        )

        self.breakdown["undertime"] = {
            "records": undertime_records,
            "summary": undertime_summary,
            "total_hours": undertime_total_hours,
            "total_amount": undertime_total_amount,
        }

    def compute_tardy(self) -> None:
        self.breakdown["tardy"] = None

        tardy_records: List = []
        tardy_total_hours: float = 0
        tardy_total_amount: float = 0

        if self.payroll_group.compute_tardiness:
            for day_att in self.attendance.iter_tardy_records():
                pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                    day_att.date
                )
                hr_amount = day_att.tardy_hours * pay_for_the_day.hourly_rate_amount
                # should only have 1 tardy in a day
                tardy_total_hours += day_att.tardy_hours
                tardy_total_amount += hr_amount
                # payslip: -
                tardy_records.append(
                    {
                        "date": day_att.date,
                        "hours": day_att.tardy_hours,
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_amount,
                    }
                )

        tardy_summary: Dict = (
            {
                "TARDY": {
                    "total_hours": tardy_total_hours,
                    "total_amount": tardy_total_amount,
                }
            }
            if tardy_total_hours
            else {}
        )

        self.breakdown["tardy"] = {
            "records": tardy_records,
            "summary": tardy_summary,
            "total_hours": tardy_total_hours,
            "total_amount": tardy_total_amount,
        }

    def compute_overbreak(self) -> None:
        self.breakdown["overbreak"] = None

        overbreak_records: List = []
        overbreak_total_hours: float = 0
        overbreak_total_amount: float = 0

        if self.payroll_group.compute_overbreak:
            for day_att in self.attendance.iter_overbreak_records():
                pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                    day_att.date
                )
                hr_amount = day_att.overbreak_hours * pay_for_the_day.hourly_rate_amount
                # should only have 1 overbreak in a day
                overbreak_total_hours += day_att.overbreak_hours
                overbreak_total_amount += hr_amount
                # payslip: -
                overbreak_records.append(
                    {
                        "date": day_att.date,
                        "hours": day_att.overbreak_hours,
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_amount,
                    }
                )

        overbreak_summary: Dict = (
            {
                "OVERBREAK": {
                    "total_hours": overbreak_total_hours,
                    "total_amount": overbreak_total_amount,
                }
            }
            if overbreak_total_hours
            else {}
        )

        self.breakdown["overbreak"] = {
            "records": overbreak_records,
            "summary": overbreak_summary,
            "total_hours": overbreak_total_hours,
            "total_amount": overbreak_total_amount,
        }

    def compute_unpaid_leave(self) -> None:
        self.breakdown["unpaid_leave"] = None

        unpaid_leave_records: List = []
        unpaid_leave_total_hours: float = 0
        unpaid_leave_total_amount: float = 0

        for day_att in self.attendance.iter_unpaid_leave_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            hr_amount = day_att.unpaid_leave_hours * pay_for_the_day.hourly_rate_amount
            # should only have 1 unpaid leave in a day; can be non full day
            unpaid_leave_total_hours += day_att.unpaid_leave_hours
            unpaid_leave_total_amount += hr_amount
            # payslip: -
            unpaid_leave_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.unpaid_leave_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        unpaid_leave_summary: Dict = (
            {
                "UNPAID_LEAVE": {
                    "total_hours": unpaid_leave_total_hours,
                    "total_amount": unpaid_leave_total_amount,
                }
            }
            if unpaid_leave_total_hours
            else {}
        )

        self.breakdown["unpaid_leave"] = {
            "records": unpaid_leave_records,
            "summary": unpaid_leave_summary,
            "total_hours": unpaid_leave_total_hours,
            "total_amount": unpaid_leave_total_amount,
        }

    def compute_absences(self) -> None:
        self.breakdown["absent"] = None

        absent_records: List = []
        absent_total_hours: float = 0
        absent_total_amount: float = 0

        for day_att in self.attendance.iter_absent_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # even if whole day we need to compute for hourly in case for hourly rate emp
            hr_amount = day_att.absent_hours * pay_for_the_day.hourly_rate_amount
            absent_total_hours += day_att.absent_hours
            absent_total_amount += hr_amount
            # payslip: -
            absent_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.absent_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        if absent_total_amount > self.breakdown["full_pay"]["total_amount"]:
            diff_amount = float(absent_total_amount - self.breakdown["full_pay"]["total_amount"])
            absent_total_amount -= diff_amount
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                date.today()
            )

            absent_records.append(
                {
                    "date": date.today(),
                    "hours": 0.0,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": diff_amount,
                }
            )

        absent_summary: Dict = (
            {
                "ABSENT": {
                    "total_hours": absent_total_hours,
                    "total_amount": absent_total_amount,
                }
            }
            if absent_total_hours
            else {}
        )

        self.breakdown["absent"] = {
            "records": absent_records,
            "summary": absent_summary,
            "total_hours": absent_total_hours,
            "total_amount": absent_total_amount,
        }

    def compute_paid_leave(self) -> None:
        self.breakdown["paid_leave"] = None

        paid_leave_records: List = []
        paid_leave_total_hours: float = 0
        paid_leave_total_amount: float = 0
        for day_att in self.attendance.iter_paid_leave_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            hr_amount = day_att.paid_leave_hours * pay_for_the_day.hourly_rate_amount
            # should only have 1 paid leave in a day; can be non full day
            paid_leave_total_hours += day_att.paid_leave_hours
            paid_leave_total_amount += (
                day_att.paid_leave_hours * pay_for_the_day.hourly_rate_amount
            )
            # payslip: -+
            paid_leave_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.paid_leave_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        paid_leave_summary: Dict = (
            {
                "PAID_LEAVE": {
                    "total_hours": paid_leave_total_hours,
                    "total_amount": paid_leave_total_amount,
                }
            }
            if paid_leave_total_hours
            else {}
        )

        self.breakdown["paid_leave"] = {
            "records": paid_leave_records,
            "summary": paid_leave_summary,
            "total_hours": paid_leave_total_hours,
            "total_amount": paid_leave_total_amount,
        }

    def compute_unworked_holiday(self) -> None:
        self.breakdown["unworked_holiday"] = None

        unworked_holiday_records: List = []
        unworked_holiday_total_hours: float = 0
        unworked_holiday_total_amount: float = 0
        for day_att in self.attendance.iter_unworked_holiday_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            hr_amount = (
                day_att.unworked_holiday_hours * pay_for_the_day.hourly_rate_amount
            )
            unworked_holiday_total_hours += day_att.unworked_holiday_hours
            unworked_holiday_total_amount += hr_amount
            # payslip: -+
            unworked_holiday_records.append(
                {
                    "date": day_att.date,
                    "hours": day_att.unworked_holiday_hours,
                    "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                    "amount": hr_amount,
                }
            )

        unworked_holiday_summary: Dict = (
            {
                "UH": {
                    "total_hours": unworked_holiday_total_hours,
                    "total_amount": unworked_holiday_total_amount,
                }
            }
            if unworked_holiday_total_hours
            else {}
        )

        self.breakdown["unworked_holiday"] = {
            "records": unworked_holiday_records,
            "summary": unworked_holiday_summary,
            "total_hours": unworked_holiday_total_hours,
            "total_amount": unworked_holiday_total_amount,
        }

    def compute_holiday_work(self) -> None:
        # holiday work is DIFFERENT from plain holiday
        # work rendered on a PURE holiday (non NT/OT)
        self.breakdown["holiday_work"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        h_records: List = []
        h_summary: Dict = {}
        h_total_hours: float = 0
        h_total_amount: float = 0
        h_total_add_amount: float = 0
        for day_att in self.attendance.iter_holiday_work_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # should only have 1 RD in a day, others are either NT and/or OT,
            # but we need to dive deep to get the type
            for hr_att in day_att.iter_holiday_work_records():
                hr_amount = hr_att.hours * pay_for_the_day.hourly_rate_amount
                hr_total_amount = hr_amount * multiplier[hr_att.hour_type]
                hr_total_add_amount = hr_amount * (multiplier[hr_att.hour_type] - 1)

                h_total_hours += hr_att.hours
                h_total_amount += hr_total_amount
                h_total_add_amount += hr_total_add_amount

                # payslip: -+* special: -daily_rate +regular_day_part +HOLIDAY_day_part
                h_records.append(
                    {
                        "date": day_att.date,
                        "scheduled_hours": day_att.scheduled_hours,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                        "add_amount": hr_total_add_amount,
                    }
                )

                if hr_att.hour_type not in h_summary:
                    h_summary[hr_att.hour_type] = {
                        "total_hours": 0,
                        "total_amount": 0,
                        "total_add_amount": 0,
                    }

                h_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                h_summary[hr_att.hour_type]["total_amount"] += hr_total_amount
                h_summary[hr_att.hour_type]["total_add_amount"] += hr_total_add_amount

        self.breakdown["holiday_work"] = {
            "records": h_records,
            "summary": h_summary,
            "total_hours": h_total_hours,
            "total_amount": h_total_amount,
            "total_add_amount": h_total_add_amount,
        }

    def compute_night_differential(self) -> None:
        self.breakdown["night_differential"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        nt_records: List = []
        nt_summary: Dict = {}
        nt_total_hours: float = 0
        nt_total_amount: float = 0
        nt_total_add_amount: float = 0
        for day_att in self.attendance.iter_night_differential_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # should only have 1 NT in a day, but we need to dive deep to get the type
            for hr_att in day_att.iter_night_differential_records():
                hr_amount = hr_att.hours * pay_for_the_day.hourly_rate_amount
                hr_total_amount = hr_amount * multiplier[hr_att.hour_type]
                # NOTE: will consider full multiplier (no alteration) if rest_day
                hr_total_add_amount = hr_amount * (
                    multiplier[hr_att.hour_type] - (0 if day_att.has_rest_day else 1)
                )

                nt_total_hours += hr_att.hours
                nt_total_amount += hr_total_amount
                nt_total_add_amount += hr_total_add_amount

                # payslip: -+* special: -daily_rate +regular_day_part +NT_day_part
                nt_records.append(
                    {
                        "date": day_att.date,
                        "scheduled_hours": day_att.scheduled_hours,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                        "add_amount": hr_total_add_amount,
                    }
                )

                if hr_att.hour_type not in nt_summary:
                    nt_summary[hr_att.hour_type] = {
                        "total_hours": 0,
                        "total_amount": 0,
                        "total_add_amount": 0,
                    }

                nt_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                nt_summary[hr_att.hour_type]["total_amount"] += hr_total_amount
                nt_summary[hr_att.hour_type]["total_add_amount"] += hr_total_add_amount

        self.breakdown["night_differential"] = {
            "records": nt_records,
            "summary": nt_summary,
            "total_hours": nt_total_hours,
            "total_amount": nt_total_amount,
            "total_add_amount": nt_total_add_amount,
        }

    def compute_rest_day_work(self) -> None:
        # work rendered on a PURE rest_day (non NT/OT)
        self.breakdown["rest_day_work"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        rd_records: List = []
        rd_summary: Dict = {}
        rd_total_hours: float = 0
        rd_total_amount: float = 0
        for day_att in self.attendance.iter_rest_day_work_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            # should only have 1 RD in a day, others are either NT and/or OT,
            # but we need to dive deep to get the type
            for hr_att in day_att.iter_rest_day_work_records():
                hr_total_amount = (
                    hr_att.hours * pay_for_the_day.hourly_rate_amount
                ) * multiplier[hr_att.hour_type]

                rd_total_hours += hr_att.hours
                rd_total_amount += hr_total_amount
                # payslip: +*
                rd_records.append(
                    {
                        "date": day_att.date,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                    }
                )

                if hr_att.hour_type not in rd_summary:
                    rd_summary[hr_att.hour_type] = {"total_hours": 0, "total_amount": 0}

                rd_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                rd_summary[hr_att.hour_type]["total_amount"] += hr_total_amount

        self.breakdown["rest_day_work"] = {
            "records": rd_records,
            "summary": rd_summary,
            "total_hours": rd_total_hours,
            "total_amount": rd_total_amount,
        }

    def compute_overtime(self) -> None:
        # overtime here covers OT and NT+OT
        self.breakdown["overtime"] = None
        multiplier: Dict[str, float] = self.payroll_group.hour_type_multiplier

        ot_records: List = []
        ot_summary: Dict = {}
        ot_total_hours: float = 0
        ot_total_amount: float = 0
        for day_att in self.attendance.iter_overtime_records():
            pay_for_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(
                day_att.date
            )
            for hr_att in day_att.iter_overtime_records():
                hr_total_amount = (
                    hr_att.hours * pay_for_the_day.hourly_rate_amount
                ) * multiplier[hr_att.hour_type]

                ot_total_hours += hr_att.hours
                ot_total_amount += hr_total_amount
                # payslip: +
                ot_records.append(
                    {
                        "date": day_att.date,
                        "hours": hr_att.hours,
                        "hour_type": hr_att.hour_type,
                        "hour_type_multiplier": multiplier[hr_att.hour_type],
                        "hourly_rate_amount": pay_for_the_day.hourly_rate_amount,
                        "amount": hr_total_amount,
                    }
                )

                if hr_att.hour_type not in ot_summary:
                    ot_summary[hr_att.hour_type] = {"total_hours": 0, "total_amount": 0}

                ot_summary[hr_att.hour_type]["total_hours"] += hr_att.hours
                ot_summary[hr_att.hour_type]["total_amount"] += hr_total_amount

        self.breakdown["overtime"] = {
            "records": ot_records,
            "summary": ot_summary,
            "total_hours": ot_total_hours,
            "total_amount": ot_total_amount,
        }

    def compute_full_pay(self) -> None:
        total_full_pay_amount = 0
        self._derive_fullpay_periods()

        for start_date, end_date in self.complete_periods:
            filtered_attendance = self.attendance.clone(start_date, end_date)
            self.full_pays.append(
                FullPay(
                    filtered_attendance,
                    self.base_pay_history,
                    self.payroll_group.pay_frequency,
                )
            )

        hourly_periods = self.left_periods + self.right_periods

        for start_date, end_date in hourly_periods:
            filtered_attendance = self.attendance.clone(start_date, end_date)
            self.full_pays.append(
                FullPay(
                    filtered_attendance,
                    self.base_pay_history,
                    self.payroll_group.pay_frequency,
                    True,
                )
            )

        for full_pay in self.full_pays:
            total_full_pay_amount += full_pay.breakdown["total_amount"]

        self.breakdown["full_pay"] = {
            "records": self.base_pay_history.records,
            "latest_base_pay": self.base_pay_history.latest_base_pay,
            # "compute_basis": compute_basis,
            "total_amount": total_full_pay_amount,
        }

    def __get_from_breakdown_section(self, section: str, section_return: str) -> Any:
        if section not in self.breakdown:
            if self.autorun_compute:
                fun = getattr(self, f"compute_{section}")
                fun()
            else:
                raise NoComputationDoneYet(
                    f"No computation for '{section}' had been executed."
                )
        return self.breakdown[section][section_return]


class FullPay:
    breakdown = {}

    def __init__(
        self, attendance, base_pay_history, pay_frequency, force_hourly_rate=False
    ):
        self.base_pay_history = base_pay_history
        self.pay_frequency = pay_frequency
        self.attendance = attendance
        self.force_hourly_rate = force_hourly_rate

        self.compute_full_pay()

    def compute_full_pay(self) -> None:
        self.breakdown = {}
        compute_basis: str = "NO_ADJUSTMENT_WITHIN_PAYROLL"

        if self.base_pay_history.has_pay_adjustments:
            total_amount, compute_basis = self._calc_prorated_full_pay_amount()
        else:
            total_amount, compute_basis = self._calc_one_time_full_pay_amount()

        self.breakdown = {
            "records": self.base_pay_history.records,
            "latest_base_pay": self.base_pay_history.latest_base_pay,
            "compute_basis": compute_basis,
            "total_amount": total_amount,
        }

    def _calc_one_time_full_pay_amount(self) -> Tuple[float, str]:
        pay_rate = (
            self.base_pay_history.records[0].pay_rate
            if not self.force_hourly_rate
            else PayRate.HOURLY
        )

        if pay_rate in [PayRate.DAILY, PayRate.HOURLY]:
            return (
                self._calc_one_time_full_pay_amount_for_daily_or_hourly_rate(),
                "NO_ADJUSTMENT_WITHIN_PAYROLL_DAILY_HOURLY",
            )
        else:
            return (
                self._calc_one_time_full_pay_amount_for_annual_or_monthly_rate(),
                "NO_ADJUSTMENT_WITHIN_PAYROLL_ANNUAL_MONTHLY",
            )

    def _calc_one_time_full_pay_amount_for_daily_or_hourly_rate(self) -> float:
        bp = self.base_pay_history.records[0]
        return sum(
            bp.hourly_rate_amount * att_day.scheduled_hours
            for att_day in self.attendance.iter_work_day_records()
        )

    def _calc_one_time_full_pay_amount_for_annual_or_monthly_rate(self) -> float:
        bp = self.base_pay_history.records[0]
        return bp.convert_amount_by_pay_frequency(self.pay_frequency)

    def _calc_prorated_full_pay_amount(self) -> Tuple[float, str]:
        pay_rate: PayRate = (
            self.base_pay_history.oldest_base_pay.pay_rate
            if not self.force_hourly_rate
            else PayRate.HOURLY
        )

        if pay_rate in [PayRate.MONTHLY, PayRate.YEARLY]:
            return (
                self._calc_prorated_full_pay_amount_for_annual_or_monthly_rate(),
                "ADJUSTMENT_WITHIN_PAYROLL_ANNUAL_MONTHLY",
            )

        return (
            self._calc_prorated_full_pay_amount_for_daily_or_hourly_rate(),
            "ADJUSTMENT_WITHIN_PAYROLL_DAILY_HOURLY",
        )

    def _calc_prorated_full_pay_amount_for_daily_or_hourly_rate(self) -> float:
        # NOTE: need to compute using hourly with scheduled hours, see below:
        #   daily_rate is not enough here because the current shift hours can
        #   change so we really depend on the TA data here for accuracy
        return sum(
            self.base_pay_history.get_pay_for_the_day(att.date).hourly_rate_amount
            * att.scheduled_hours
            for att in self.attendance.iter_work_day_records()
        )

    def _calc_prorated_full_pay_amount_for_annual_or_monthly_rate(self) -> float:
        # get oldest rate and full_pay equiv amount of that (old_bp_full_pay_amt)
        old_bp: BasePay = self.base_pay_history.oldest_base_pay
        old_bp_full_pay_amt: float = old_bp.convert_amount_by_pay_frequency(
            self.pay_frequency
        )

        # old_bp_full_pay_amt - (old_bp_daily * # of non old_bp_days)
        #   + sum((new_bp_daily * new_bp_days))
        num_of_days_not_old_bp: int = 0
        sum_of_day_rate_of_new_bp: float = 0
        for att in self.attendance.iter_work_day_records():
            bp_of_the_day: BasePay = self.base_pay_history.get_pay_for_the_day(att.date)

            if old_bp is not bp_of_the_day:
                num_of_days_not_old_bp += 1
                sum_of_day_rate_of_new_bp += bp_of_the_day.daily_rate_amount

        return (
            old_bp_full_pay_amt
            - (old_bp.daily_rate_amount * num_of_days_not_old_bp)
            + sum_of_day_rate_of_new_bp
        )


@dataclass
class NetPay:
    basic_pay: BasicPay
    employee_details: EmployeeDetails

    annual_withholding_tax: float = 0
    annual_taxable_income: float = 0
    annual_contributions: float = 0
    annualize: bool = False
    # Joe
    prev_contrib: Dict[str, ContributionRecord] = field(
        default_factory=_default_prev_contrib
    )

    other_incomes: OtherIncomeRecord = OtherIncomeRecord(records=list())
    adjustments: AdjustmentRecord = AdjustmentRecord(records=list())

    deductions: List[Deduction] = field(default_factory=list)
    deminimis: DeMinimisRecord = DeMinimisRecord(records=list())
    loan_amortizations: List[Loan] = field(default_factory=list)
    loans_to_credit: List[CreditedLoan] = field(default_factory=list)

    autorun_compute: bool = True

    breakdown: Dict = field(default_factory=dict, init=False)

    def __post_init__(self):
        if self.autorun_compute:
            self.compute_contributions()
            self.compute_tax()
            self.breakdown_other_incomes()
            self.breakdown_deductions()
            self.breakdown_adjustments()
            self.breakdown_loan_amortizations()
            self.breakdown_loans_to_credit()
            self.breakdown_deminimis()

            self.breakdown["basic_pay"] = self.basic_pay.breakdown
            self.breakdown["employee_details"] = self.employee_details

            # need to be here for helper in getting previous contribution
            self.breakdown["pay_schedule"] = {
                "cur_pay_schedule": self.basic_pay.payroll_group.pay_schedule,
                "max_pay_schedule": self.basic_pay.payroll_group.max_pay_schedule,
            }

    @property
    def amount(self) -> float:
        amount = (
            self.gross_income_amount
            + self.total_non_income_adjustments_amount
            + self.total_loan_credited_amount
        )

        if amount <=0:
            return amount
        else:
            tmp_amount_contrib = amount - self.total_contributions_income_deduction_amount

        if tmp_amount_contrib <=0:
            return amount
        else:
            tmp_amount_tax = tmp_amount_contrib - self.tax_amount

        if tmp_amount_tax <=0:
            return tmp_amount_contrib
        else:
            tmp_amount_deduct = tmp_amount_tax - self.total_deductions_amount

        if tmp_amount_deduct <=0:
            return tmp_amount_tax
        else:
            tmp_amount_loan_deduct = tmp_amount_deduct - self.total_loan_deduction_amount

        if tmp_amount_loan_deduct <=0:
            return tmp_amount_deduct
        else:
            return tmp_amount_loan_deduct

    @property
    def gross_income_amount(self) -> float:
        return (
            self.basic_pay.amount
            + self.total_taxable_other_income_amount
            + self.total_non_taxable_other_income_amount
            + self.total_taxable_income_adjustments_amount
            + self.total_non_taxable_income_adjustments_amount
            + self.total_non_taxable_deminimis_amount
        )

    @property
    def gross_income_taxable_amount(self) -> float:
        return (
            self.basic_pay.amount
            + self.total_taxable_other_income_amount
            + self.total_taxable_income_adjustments_amount
        )

    @property
    def net_basic_pay_amount(self) -> float:
        return (
            self.basic_pay.basic_pay_amount
        )

    @property
    def total_gross_income_deduction_amount(self) -> float:
        return (
            self.total_contributions_income_deduction_amount
            + self.tax_amount
            + self.total_deductions_amount
            + self.total_loan_deduction_amount
        )

    @property
    def taxable_income_amount(self) -> float:
        taxable_amount = (
            self.gross_income_amount
            - self.total_contributions_amount
            - self.total_non_taxable_other_income_amount
            - self.total_non_taxable_income_adjustments_amount
            - self.total_non_taxable_deminimis_amount
            - self.total_reduce_taxable_deminimis_amount
            - self.total_philhealth_employee_contribution_adjustments_amount
        )

        if taxable_amount < 0:
            taxable_amount = 0.0

        return (
            taxable_amount
            if self.employee_details.tax_type is not TaxType.MINIMUM_WAGE
            else 0
        )

    @property
    def tax_amount(self) -> float:
        return self.breakdown["tax"]["tax_amount"] + (
            self.total_tax_adjustments_amount if not self.annualize else 0
        )

    @property
    def tax_amount_due(self) -> float:
        return (
            self.breakdown["tax"]["tax_amount"]
            if not self.annualize
            else self.tax_amount
        )

    @property
    def total_contributions_amount(self) -> float:
        # TODO: add checks for breakdown
        # Only ee_share gets computed in contributions amount that will be later deducted to net pay
        # We will add the new provident fund as part of the total contribution to be included
        # in the deductions as well
        # Since ee_provident_fund only applies to SSS and not all contributions, we will add a checker with default value 0
        return sum(
            self.breakdown["contributions"][key]["ee_share"]
            + self.breakdown["contributions"][key]["ee_provident_fund"]
            if ContributionEnum(key) == ContributionEnum.SSS
            else self.breakdown["contributions"][key]["emp_income_deduction"]
            for key in self.breakdown["contributions"]
        )

    @property
    def total_contributions_of_employer_amount(self) -> float:
        return sum(
            self.breakdown["contributions"][key]["er_share"]
            + self.breakdown["contributions"][key].get("ec_share", 0)
            + self.breakdown["contributions"][key].get("er_provident_fund", 0)
            for key in self.breakdown["contributions"]
        ) + self.total_philhealth_employer_contribution_adjustments_amount

    @property
    def total_contributions_income_deduction_amount(self) -> float:
        return sum(
            self.breakdown["contributions"][key]["emp_income_deduction"]
            for key in self.breakdown["contributions"]
        ) + self.total_philhealth_employee_contribution_adjustments_amount

    @property
    def total_non_taxable_other_income_amount(self) -> float:
        return sum(x.amount for x in self.other_incomes.non_taxable_other_incomes)

    @property
    def total_taxable_other_income_amount(self) -> float:
        return sum(x.amount for x in self.other_incomes.taxable_other_incomes)

    @property
    def total_deductions_amount(self) -> float:
        return sum(x.amount for x in self.deductions)

    @property
    def total_tax_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.tax_adjustments)

    @property
    def total_taxable_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.taxable_income_adjustments)

    @property
    def total_non_taxable_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.non_taxable_income_adjustments)

    @property
    def total_non_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.non_income_adjustments)

    @property
    def total_philhealth_employee_contribution_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.philhealth_employee_contribution_adjustments)

    @property
    def total_philhealth_employer_contribution_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.philhealth_employer_contribution_adjustments)

    @property
    def total_loan_deduction_amount(self) -> float:
        return sum(loan.amount for loan in self.loan_amortizations)

    @property
    def total_loan_credited_amount(self) -> float:
        return sum(loan.amount for loan in self.loans_to_credit)

    @property
    def total_reduce_taxable_deminimis_amount(self) -> float:
        return self.deminimis.total_reduce_taxable_amount

    @property
    def total_non_taxable_deminimis_amount(self) -> float:
        return self.deminimis.total_non_taxable_amount

    def breakdown_deminimis(self) -> None:
        self.breakdown["deminimis"] = {
            DeminimisType.NON_TAXABLE.value: list(self.deminimis.non_taxable),
            DeminimisType.REDUCE_TAXABLE.value: list(self.deminimis.reduce_taxable),
        }

    def breakdown_loans_to_credit(self) -> None:
        self.breakdown["loans_to_credit"] = self.loans_to_credit
        # self.breakdown["loans_to_credit"] = None

        # records: Dict = {"GOVERNMENT": [], "OTHER": []}
        # for loan in self.loans_to_credit:
        #     records["GOVERNMENT" if loan.is_government_loan else "OTHER"].append(loan)

        # self.breakdown["loan_amortizations"] = records

    def breakdown_loan_amortizations(self) -> None:
        self.breakdown["loan_amortizations"] = None

        records: Dict = {"GOVERNMENT": [], "OTHER": []}
        for loan in self.loan_amortizations:
            records["GOVERNMENT" if loan.is_government_loan else "OTHER"].append(loan)

        self.breakdown["loan_amortizations"] = records

    def breakdown_other_incomes(self) -> None:
        self.breakdown["other_incomes"] = None

        records: Dict = {
            "ALLOWANCE": {"taxable": [], "non_taxable": []},
            "BONUS": {"taxable": [], "non_taxable": []},
            "COMMISSION": {"taxable": [], "non_taxable": []},
        }
        for oi in self.other_incomes.records:
            records[oi.__class__.__name__.upper()][
                "taxable" if oi.is_taxable else "non_taxable"
            ].append(oi)

        self.breakdown["other_incomes"] = records

    def breakdown_deductions(self) -> None:
        self.breakdown["deductions"] = [ded for ded in self.deductions]

    def breakdown_adjustments(self) -> None:
        self.breakdown["adjustments"] = None
        adjs: Dict = {adj: [] for adj in AdjustmentType}

        for x in self.adjustments.records:
            adjs[x.type].append(x)

        self.breakdown["adjustments"] = adjs

    def compute_tax(self) -> None:
        self.breakdown["tax"] = None
        payroll_group = self.basic_pay.payroll_group

        tax = Tax(
            pay_frequency=payroll_group.pay_frequency
            if not self.annualize
            else PayFrequency(PayFrequency.ANNUALLY),
            effective_date=payroll_group.posting_date,
            type=self.employee_details.tax_type,
        )

        tax_due = tax.compute_tax_deduction_amount(
            taxable_income=self.taxable_income_amount
            + (self.annual_taxable_income if self.annualize else 0)
            - (self.annual_contributions if self.annualize else 0),
            tax_rate=self.employee_details.tax_rate,
        )

        total_tax_due = tax_due
        if self.annualize:
            total_tax_due = (
                tax_due - self.annual_withholding_tax
            ) + self.total_tax_adjustments_amount

        self.breakdown["tax"] = {
            "annual_tax_due": tax_due if self.annualize else 0,
            "tax_amount": total_tax_due,
            "tax_type": self.employee_details.tax_type,
            "basis_amount": self.taxable_income_amount,
            "refund_tax_amount": abs(total_tax_due) if total_tax_due < 0 else 0,
        }
        if self.employee_details.tax_type is TaxType.CONSULTANT:
            self.breakdown["tax"]["tax_rate"] = self.employee_details.tax_rate

    def compute_contributions(self) -> None:
        self.breakdown["contributions"] = None
        payroll_group = self.basic_pay.payroll_group
        posting_date = payroll_group.posting_date
        emp_contrib_basis = self.employee_details.contribution_basis

        sss = SSSContribution(effective_date=posting_date)
        basis_type = emp_contrib_basis[ContributionEnum.SSS].type
        sss_tup = sss.compute(
            basis_amount=self._get_contrib_basis_amount(
                basis_type, ContributionEnum.SSS
            ),
            basis_type=basis_type,
            pay_frequency=payroll_group.pay_frequency,
            deduction_schedule=payroll_group.contribution_schedules[
                ContributionEnum.SSS
            ],
            pay_schedule=payroll_group.pay_schedule,
            max_pay_schedule=payroll_group.max_pay_schedule,
            current_gross_income=self.gross_income_amount,
            tran_type=TransactionType.NETPAY,
            prev_total_gross=0,
            prev_contrib=self.prev_contrib[ContributionEnum.SSS],
        )

        hdmf = HDMFContribution(effective_date=posting_date)
        basis_type = emp_contrib_basis[ContributionEnum.HDMF].type
        hdmf_tup = hdmf.compute(
            basis_amount=self._get_contrib_basis_amount(
                basis_type, ContributionEnum.HDMF
            ),
            basis_type=basis_type,
            pay_frequency=payroll_group.pay_frequency,
            deduction_schedule=payroll_group.contribution_schedules[
                ContributionEnum.HDMF
            ],
            pay_schedule=payroll_group.pay_schedule,
            max_pay_schedule=payroll_group.max_pay_schedule,
            current_gross_income=self.gross_income_amount,
            tran_type=TransactionType.NETPAY,
            prev_total_gross=0,
            prev_contrib=self.prev_contrib[ContributionEnum.HDMF],
        )

        philhealth = PhilHealthContribution(effective_date=posting_date)
        basis_type = emp_contrib_basis[ContributionEnum.PHILHEALTH].type
        philhealth_tup = philhealth.compute(
            basis_amount=self._get_contrib_basis_amount(
                basis_type, ContributionEnum.PHILHEALTH
            ),
            basis_type=basis_type,
            pay_frequency=payroll_group.pay_frequency,
            deduction_schedule=payroll_group.contribution_schedules[
                ContributionEnum.PHILHEALTH
            ],
            pay_schedule=payroll_group.pay_schedule,
            max_pay_schedule=payroll_group.max_pay_schedule,
            current_gross_income=self.gross_income_amount,
            tran_type=TransactionType.NETPAY,
            prev_total_gross=0,
            prev_contrib=self.prev_contrib[ContributionEnum.PHILHEALTH],
        )

        self.breakdown["contributions"] = {
            ContributionEnum.SSS: {
                "ee_share": sss_tup[0],
                "er_share": sss_tup[1],
                "ec_share": sss_tup[2],
                "emp_income_deduction": sss_tup[3],
                "ee_provident_fund": sss_tup[4],
                "er_provident_fund": sss_tup[5],
                "records": sss_tup[-1],
            },
            ContributionEnum.HDMF: {
                "ee_share": hdmf_tup[0],
                "er_share": hdmf_tup[1],
                "emp_income_deduction": hdmf_tup[2],
                "records": hdmf_tup[-1],
            },
            ContributionEnum.PHILHEALTH: {
                "ee_share": philhealth_tup[0],
                "er_share": philhealth_tup[1],
                "emp_income_deduction": philhealth_tup[2],
                "records": philhealth_tup[-1],
            },
        }

    def _get_contrib_basis_amount(
        self, basis_type: ContributionIncomeType, contribution_enum: ContributionEnum
    ) -> float:
        if basis_type is ContributionIncomeType.BASIC:
            # get latest_base_pay monthly rate amount
            return self.basic_pay.base_pay_history.latest_base_pay.monthly_rate_amount
        elif basis_type is ContributionIncomeType.GROSS:
            return self.gross_income_amount
        elif basis_type is ContributionIncomeType.GROSS_TAXABLE:
            return self.gross_income_taxable_amount
        elif basis_type is ContributionIncomeType.NET_BASIC_PAY:
            return self.net_basic_pay_amount
        else:
            # return FIXED amount from emp settings
            return self.employee_details.contribution_basis[contribution_enum].amount


@dataclass
class SpecialNetPay:
    employee_details: EmployeeDetails
    payroll_details: dict
    other_incomes: OtherIncomeRecord = OtherIncomeRecord(records=list())
    adjustments: AdjustmentRecord = AdjustmentRecord(records=list())
    deductions: List[Deduction] = field(default_factory=list)
    breakdown: Dict = field(default_factory=dict, init=False)
    pay_frequency: PayFrequency = None
    autorun_compute: bool = True
    deminimis: DeMinimisRecord = DeMinimisRecord(records=list())
    # annual_taxable_income: float = 0
    # Joe
    def __post_init__(self):
        if self.autorun_compute:
            self.compute_tax()
            self.breakdown_other_incomes()
            self.breakdown_deductions()
            self.breakdown_adjustments()
            self.breakdown_deminimis()

            self.breakdown["employee_details"] = self.employee_details

    @property
    def amount(self) -> float:
        return (
            self.gross_income_amount
            - self.total_gross_income_deduction_amount
            + self.total_non_income_adjustments_amount
        )

    @property
    def total_gross_income_deduction_amount(self) -> float:
        return self.tax_amount + self.total_deductions_amount

    @property
    def total_reduce_taxable_deminimis_amount(self) -> float:
        return self.deminimis.total_reduce_taxable_amount

    @property
    def taxable_income_amount(self) -> float:
        taxable_amount = (
            self.gross_income_amount
            - self.total_non_taxable_other_income_amount
            - self.total_non_taxable_income_adjustments_amount
            - self.total_philhealth_employee_contribution_adjustments_amount
            - self.total_reduce_taxable_deminimis_amount
        )

        if taxable_amount < 0:
            taxable_amount = 0.0

        return (
            taxable_amount
            if self.employee_details.tax_type is not TaxType.MINIMUM_WAGE
            else 0
        )

    @property
    def tax_amount(self) -> float:
        return self.breakdown["tax"]["tax_amount"] + self.total_tax_adjustments_amount

    @property
    def tax_amount_due(self) -> float:
        return self.breakdown["tax"]["tax_amount"]

    @property
    def gross_income_amount(self) -> float:
        return (
            self.total_taxable_other_income_amount
            + self.total_non_taxable_other_income_amount
            + self.total_taxable_income_adjustments_amount
            + self.total_non_taxable_income_adjustments_amount
        )

    @property
    def gross_income_taxable_amount(self) -> float:
        return (
            self.total_taxable_other_income_amount
            + self.total_taxable_income_adjustments_amount
        )

    @property
    def total_non_taxable_other_income_amount(self) -> float:
        return sum(x.amount for x in self.other_incomes.non_taxable_other_incomes)

    @property
    def total_taxable_other_income_amount(self) -> float:
        return sum(x.amount for x in self.other_incomes.taxable_other_incomes)

    @property
    def total_deductions_amount(self) -> float:
        return sum(x.amount for x in self.deductions) + self.total_philhealth_employee_contribution_adjustments_amount

    @property
    def total_tax_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.tax_adjustments)

    @property
    def total_taxable_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.taxable_income_adjustments)

    @property
    def total_non_taxable_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.non_taxable_income_adjustments)

    @property
    def total_non_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.non_income_adjustments)

    @property
    def total_philhealth_employee_contribution_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.philhealth_employee_contribution_adjustments)

    @property
    def total_philhealth_employer_contribution_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.philhealth_employer_contribution_adjustments)

    def breakdown_deminimis(self) -> None:
        self.breakdown["deminimis"] = {
            DeminimisType.NON_TAXABLE.value: list(self.deminimis.non_taxable),
            DeminimisType.REDUCE_TAXABLE.value: list(self.deminimis.reduce_taxable),
        }

    def breakdown_other_incomes(self) -> None:
        self.breakdown["other_incomes"] = None

        records: Dict = {
            "ALLOWANCE": {"taxable": [], "non_taxable": []},
            "BONUS": {"taxable": [], "non_taxable": []},
            "COMMISSION": {"taxable": [], "non_taxable": []},
        }
        for oi in self.other_incomes.records:
            records[oi.__class__.__name__.upper()][
                "taxable" if oi.is_taxable else "non_taxable"
            ].append(oi)

        self.breakdown["other_incomes"] = records

    def breakdown_deductions(self) -> None:
        self.breakdown["deductions"] = [ded for ded in self.deductions]

    def breakdown_adjustments(self) -> None:
        self.breakdown["adjustments"] = None
        adjs: Dict = {adj: [] for adj in AdjustmentType}

        for x in self.adjustments.records:
            adjs[x.type].append(x)

        self.breakdown["adjustments"] = adjs

    def compute_tax(self) -> None:
        self.breakdown["tax"] = None

        tax = Tax(
            pay_frequency=self.pay_frequency,
            effective_date=datetime.strptime(
                self.payroll_details["attributes"]["payRunDate"], "%Y-%m-%d"
            ).date(),
            type=self.employee_details.tax_type,
        )

        self.breakdown["tax"] = {
            "tax_amount": tax.compute_tax_deduction_amount(
                taxable_income=self.taxable_income_amount,
                # + self.annual_taxable_income,
                tax_rate=self.employee_details.tax_rate,
            ),
            "tax_type": self.employee_details.tax_type,
            "basis_amount": self.taxable_income_amount,
        }
        if self.employee_details.tax_type is TaxType.CONSULTANT:
            self.breakdown["tax"]["tax_rate"] = self.employee_details.tax_rate


@dataclass
class FinalNetPay:
    basic_pay: FinalBasicPay
    employee_details: EmployeeDetails

    annual_withholding_tax: float = 0
    annual_taxable_income: float = 0
    annual_contributions: float = 0

    sss_refund: bool = False
    hdmf_refund: bool = False
    philhealth_refund: bool = False

    prev_contrib: Dict[str, ContributionRecord] = field(
        default_factory=_default_prev_contrib
    )

    other_incomes: OtherIncomeRecord = OtherIncomeRecord(records=list())
    adjustments: AdjustmentRecord = AdjustmentRecord(records=list())

    deductions: List[Deduction] = field(default_factory=list)
    deminimis: DeMinimisRecord = DeMinimisRecord(records=list())
    loan_amortizations: List[Loan] = field(default_factory=list)
    loans_to_credit: List[CreditedLoan] = field(default_factory=list)

    autorun_compute: bool = True

    breakdown: Dict = field(default_factory=dict, init=False)

    def __post_init__(self):
        if self.autorun_compute:
            self.compute_contributions(
                {
                    ContributionEnum.SSS: self.sss_refund,
                    ContributionEnum.HDMF: self.hdmf_refund,
                    ContributionEnum.PHILHEALTH: self.philhealth_refund,
                }
            )

            self.refund_previous_contributions(
                {
                    ContributionEnum.SSS: self.sss_refund,
                    ContributionEnum.HDMF: self.hdmf_refund,
                    ContributionEnum.PHILHEALTH: self.philhealth_refund,
                }
            )

            self.compute_tax()
            self.breakdown_other_incomes()
            self.breakdown_deductions()
            self.breakdown_adjustments()
            self.breakdown_loan_amortizations()
            # self.breakdown_loans_to_credit()
            self.breakdown_deminimis()

            self.breakdown["basic_pay"] = self.basic_pay.breakdown
            self.breakdown["employee_details"] = self.employee_details

            # need to be here for helper in getting previous contribution
            self.breakdown["pay_schedule"] = {
                "cur_pay_schedule": self.basic_pay.payroll_group.max_pay_schedule,
                "max_pay_schedule": self.basic_pay.payroll_group.max_pay_schedule,
            }

    def compute_contributions(self, settings: dict) -> None:
        self.breakdown["contributions"] = {}
        payroll_group = self.basic_pay.payroll_group
        pay_run = payroll_group.pay_run
        emp_contrib_basis = self.employee_details.contribution_basis
        contrib_amount = 0
        if not settings[ContributionEnum.SSS]:
            sss = SSSContribution(effective_date=pay_run)
            basis_type = emp_contrib_basis[ContributionEnum.SSS].type
            contrib_amount = self._get_contrib_deducted_amount(ContributionEnum.SSS)
            sss_tup = sss.compute(
                basis_amount=self._get_contrib_basis_amount(
                    basis_type, ContributionEnum.SSS
                ),
                basis_type=basis_type,
                pay_frequency=payroll_group.pay_frequency,
                deduction_schedule=payroll_group.contribution_schedules[
                    ContributionEnum.SSS
                ],
                pay_schedule=payroll_group.max_pay_schedule,
                max_pay_schedule=payroll_group.max_pay_schedule,
                current_gross_income=self.gross_income_amount,
                tran_type=TransactionType.FINAL_NETPAY,
                prev_total_gross = self._get_prev_total_gross_amount(ContributionEnum.SSS),
                #prev_contrib=self.prev_contrib[ContributionEnum.SSS],
            )

            self.breakdown["contributions"][ContributionEnum.SSS] = {
                "ee_share": sss_tup[0] - contrib_amount[2],
                "er_share": sss_tup[1] - contrib_amount[1],
                "ec_share": sss_tup[2] - contrib_amount[5],
                "emp_income_deduction": sss_tup[3] - contrib_amount[0],
                "ee_provident_fund": sss_tup[4] - contrib_amount[3],
                "er_provident_fund": sss_tup[5] - contrib_amount[4],
                "records": sss_tup[-1],
            }

        if not settings[ContributionEnum.HDMF]:
            hdmf = HDMFContribution(effective_date=pay_run)
            basis_type = emp_contrib_basis[ContributionEnum.HDMF].type
            contrib_amount = self._get_contrib_deducted_amount(ContributionEnum.HDMF)
            hdmf_tup = hdmf.compute(
                basis_amount=self._get_contrib_basis_amount(
                    basis_type, ContributionEnum.HDMF
                ),
                basis_type=basis_type,
                pay_frequency=payroll_group.pay_frequency,
                deduction_schedule=payroll_group.contribution_schedules[
                    ContributionEnum.HDMF
                ],
                pay_schedule=payroll_group.max_pay_schedule,
                max_pay_schedule=payroll_group.max_pay_schedule,
                current_gross_income=self.gross_income_amount,
                tran_type=TransactionType.FINAL_NETPAY,
                prev_total_gross = self._get_prev_total_gross_amount(ContributionEnum.HDMF),
                #prev_contrib=self.prev_contrib[ContributionEnum.HDMF]
            )

            self.breakdown["contributions"][ContributionEnum.HDMF] = {
                "ee_share": hdmf_tup[0] - contrib_amount[2],
                "er_share": hdmf_tup[1] - contrib_amount[1],
                "emp_income_deduction": hdmf_tup[2] - contrib_amount[0],
                "records": hdmf_tup[-1],
            }

        if not settings[ContributionEnum.PHILHEALTH]:
            philhealth = PhilHealthContribution(effective_date=pay_run)
            basis_type = emp_contrib_basis[ContributionEnum.PHILHEALTH].type
            contrib_amount = self._get_contrib_deducted_amount(ContributionEnum.PHILHEALTH)
            philhealth_tup = philhealth.compute(
                basis_amount=self._get_contrib_basis_amount(
                    basis_type, ContributionEnum.PHILHEALTH
                ),
                basis_type=basis_type,
                pay_frequency=payroll_group.pay_frequency,
                deduction_schedule=payroll_group.contribution_schedules[
                    ContributionEnum.PHILHEALTH
                ],
                pay_schedule=payroll_group.max_pay_schedule,
                max_pay_schedule=payroll_group.max_pay_schedule,
                current_gross_income=self.gross_income_amount,
                tran_type=TransactionType.FINAL_NETPAY,
                prev_total_gross = self._get_prev_total_gross_amount(ContributionEnum.PHILHEALTH),
                #prev_contrib=self.prev_contrib[ContributionEnum.PHILHEALTH]
            )

            self.breakdown["contributions"][ContributionEnum.PHILHEALTH] = {
                "ee_share": philhealth_tup[0] - contrib_amount[2],
                "er_share": philhealth_tup[1] - contrib_amount[1],
                "emp_income_deduction": philhealth_tup[2] - contrib_amount[0],
                "records": philhealth_tup[-1],
            }

    def refund_previous_contributions(self, settings: dict):
        self.breakdown["refunded_contributions"] = {}
        if (
            settings[ContributionEnum.SSS]
            and len(self.prev_contrib[ContributionEnum.SSS]) > 0
        ):
            prev_contribs = self.prev_contrib[ContributionEnum.SSS]
            self.breakdown["refunded_contributions"][ContributionEnum.SSS] = {
                "ee_share": getattr(prev_contribs[0], "employee_share_amount"),
                "er_share": getattr(prev_contribs[0], "employer_share_amount"),
                "ec_share": getattr(prev_contribs[0], "employee_compensation_amount"),
                "emp_income_deduction": getattr(
                    prev_contribs[0], "employee_income_deduction"
                ),
                "records": prev_contribs,
            }

        if (
            settings[ContributionEnum.HDMF]
            and len(self.prev_contrib[ContributionEnum.HDMF]) > 0
        ):
            prev_contribs = self.prev_contrib[ContributionEnum.HDMF]
            self.breakdown["refunded_contributions"][ContributionEnum.HDMF] = {
                "ee_share": getattr(prev_contribs[0], "employee_share_amount"),
                "er_share": getattr(prev_contribs[0], "employer_share_amount"),
                "ec_share": getattr(prev_contribs[0], "employee_compensation_amount"),
                "emp_income_deduction": getattr(
                    prev_contribs[0], "employee_income_deduction"
                ),
                "records": prev_contribs,
            }

        if (
            settings[ContributionEnum.PHILHEALTH]
            and len(self.prev_contrib[ContributionEnum.PHILHEALTH]) > 0
        ):
            prev_contribs = self.prev_contrib[ContributionEnum.PHILHEALTH]
            self.breakdown["refunded_contributions"][ContributionEnum.PHILHEALTH] = {
                "ee_share": getattr(prev_contribs[0], "employee_share_amount"),
                "er_share": getattr(prev_contribs[0], "employer_share_amount"),
                "ec_share": getattr(prev_contribs[0], "employee_compensation_amount"),
                "emp_income_deduction": getattr(
                    prev_contribs[0], "employee_income_deduction"
                ),
                "records": prev_contribs,
            }

    def compute_tax(self) -> None:
        self.breakdown["tax"] = None
        payroll_group = self.basic_pay.payroll_group

        tax = Tax(
            pay_frequency=PayFrequency(PayFrequency.ANNUALLY),
            effective_date=payroll_group.pay_run,
            type=self.employee_details.tax_type,
        )

        tax_due = tax.compute_tax_deduction_amount(
            taxable_income=self.taxable_income_amount
            + self.annual_taxable_income
            - self.annual_contributions,
            tax_rate=self.employee_details.tax_rate,
        )

        total_tax_due = (
            tax_due - self.annual_withholding_tax
        ) + self.total_tax_adjustments_amount

        self.breakdown["tax"] = {
            "annual_tax_due": tax_due,
            "tax_amount": total_tax_due,
            "tax_type": self.employee_details.tax_type,
            "basis_amount": self.taxable_income_amount,
            "refund_tax_amount": abs(total_tax_due) if total_tax_due < 0 else 0,
        }
        if self.employee_details.tax_type is TaxType.CONSULTANT:
            self.breakdown["tax"]["tax_rate"] = self.employee_details.tax_rate

    @property
    def amount(self) -> float:
        return (
            self.gross_income_amount
            - self.total_gross_income_deduction_amount
            + self.total_non_income_adjustments_amount
            + self.total_loan_credited_amount
            + self.contribution_refund_amount
        )

    @property
    def contribution_refund_amount(self) -> float:
        refunded = self.breakdown["refunded_contributions"]
        refund_sss = refunded.get(ContributionEnum.SSS, {})
        refund_hdmf = refunded.get(ContributionEnum.HDMF, {})
        refund_philhealth = refunded.get(ContributionEnum.PHILHEALTH, {})

        return (
            refund_sss.get("emp_income_deduction", 0)
            + refund_hdmf.get("emp_income_deduction", 0)
            + refund_philhealth.get("emp_income_deduction", 0)
        )

    @property
    def non_taxable_income_amount(self) -> float:
        return (
            self.total_non_taxable_other_income_amount
            + self.total_non_taxable_income_adjustments_amount
            + self.total_non_taxable_deminimis_amount
            + self.total_reduce_taxable_deminimis_amount
        )

    @property
    def non_taxable_incomes_list(self) -> list:
        return (
            self.total_non_taxable_other_income_list
            + self.total_non_taxable_income_adjustments_list
            + self.total_non_taxable_deminimis_list
            + self.total_reduce_taxable_deminimis_list
        )

    @property
    def taxable_income_amount(self) -> float:
        taxable_amount = (
            self.gross_income_amount
            - self.total_contributions_amount
            - self.total_non_taxable_other_income_amount
            - self.total_non_taxable_income_adjustments_amount
            - self.total_non_taxable_deminimis_amount
            - self.total_reduce_taxable_deminimis_amount
            - self.total_philhealth_employee_contribution_adjustments_amount
        )

        if taxable_amount < 0:
            taxable_amount = 0.0

        return (
            taxable_amount
            if self.employee_details.tax_type is not TaxType.MINIMUM_WAGE
            else 0
        )

    @property
    def taxable_incomes_list(self) -> list:
        return (
            [{"label": "Base Pay", "amount": self.basic_pay.full_pay_amount}]
            + self.total_taxable_other_income_list
            + self.total_taxable_income_adjustments_list
        )

    @property
    def deductions_list(self) -> list:
        return (
            self.total_contributions_income_deduction_list
            + self.philhealth_employee_contribution_adjustments_list
            + self.tax_list
            + self.total_deductions_list
            + self.total_loan_deduction_list
        )

    @property
    def gross_income_amount(self) -> float:
        return (
            self.basic_pay.amount
            + self.total_taxable_other_income_amount
            + self.total_non_taxable_other_income_amount
            + self.total_taxable_income_adjustments_amount
            + self.total_non_taxable_income_adjustments_amount
            + self.total_non_taxable_deminimis_amount
        )

    @property
    def gross_income_taxable_amount(self) -> float:
        return (
            self.basic_pay.amount
            + self.total_taxable_other_income_amount
            + self.total_taxable_income_adjustments_amount
        )

    @property
    def net_basic_pay_amount(self) -> float:
        return (
            self.basic_pay.basic_pay_amount
        )

    @property
    def total_gross_income_deduction_amount(self) -> float:
        return (
            self.total_contributions_income_deduction_amount
            + self.tax_amount
            + self.total_deductions_amount
            + self.total_loan_deduction_amount
        )

    @property
    def tax_amount(self) -> float:
        return self.breakdown["tax"]["tax_amount"]

    @property
    def tax_amount_due(self) -> float:
        return self.tax_amount

    @property
    def tax_list(self) -> list:
        return [{"label": "Withholding Tax as Adjusted", "amount": self.tax_amount_due}]

    @property
    def total_philhealth_employee_contribution_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.philhealth_employee_contribution_adjustments)

    @property
    def philhealth_employee_contribution_adjustments_list(self) -> list:
        return [
            {
                "label": "Philhealth Adjustment",
                "amount": self.total_philhealth_employee_contribution_adjustments_amount,
            }
        ]

    @property
    def total_philhealth_employer_contribution_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.philhealth_employer_contribution_adjustments)

    @property
    def total_contributions_amount(self) -> float:
        # TODO: add checks for breakdown
        return sum(
            self.breakdown["contributions"][key]["ee_share"]
            + self.breakdown["contributions"][key].get("ee_provident_fund", 0)
            if ContributionEnum(key) == ContributionEnum.SSS
            else self.breakdown["contributions"][key]["emp_income_deduction"]
            for key in self.breakdown["contributions"]
        )

    @property
    def total_contributions_of_employer_amount(self) -> float:
        return sum(
            self.breakdown["contributions"][key]["er_share"]
            + self.breakdown["contributions"][key].get("ec_share", 0)
            + self.breakdown["contributions"][key].get("er_provident_fund", 0)
            for key in self.breakdown["contributions"]
        )

    @property
    def total_contributions_income_deduction_amount(self) -> float:
        return sum(
            self.breakdown["contributions"][key]["emp_income_deduction"]
            for key in self.breakdown["contributions"]
        ) + self.total_philhealth_employee_contribution_adjustments_amount

    @property
    def total_contributions_income_deduction_list(self) -> list:
        return [
            {
                "label": repr(key) + " Contribution",
                "amount": self.breakdown["contributions"][key]["emp_income_deduction"],
            }
            for key in self.breakdown["contributions"]
        ]

    @property
    def total_non_taxable_other_income_amount(self) -> float:
        return sum(x.amount for x in self.other_incomes.non_taxable_other_incomes)

    @property
    def total_non_taxable_other_income_list(self) -> list:
        return [
            {"label": x.type, "amount": x.amount}
            for x in self.other_incomes.non_taxable_other_incomes
        ]

    @property
    def total_taxable_other_income_amount(self) -> float:
        return sum(x.amount for x in self.other_incomes.taxable_other_incomes)

    @property
    def total_taxable_other_income_list(self) -> list:
        return [
            {"label": x.type, "amount": x.amount}
            for x in self.other_incomes.taxable_other_incomes
        ]

    @property
    def total_deductions_amount(self) -> float:
        return sum(x.amount for x in self.deductions) + self.total_philhealth_employee_contribution_adjustments_amount

    @property
    def total_deductions_list(self) -> list:
        return [{"label": x.type, "amount": x.amount} for x in self.deductions]

    @property
    def total_tax_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.tax_adjustments)

    @property
    def total_taxable_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.taxable_income_adjustments)

    @property
    def total_taxable_income_adjustments_list(self) -> list:
        return [
            {
                "label": repr(x.type).replace("_", " ").title() + " Adjustment",
                "amount": x.amount,
            }
            for x in self.adjustments.taxable_income_adjustments
        ]

    @property
    def total_non_taxable_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.non_taxable_income_adjustments)

    @property
    def total_non_taxable_income_adjustments_list(self) -> list:
        return [
            {
                "label": repr(x.type).replace("_", " ").title() + " Adjustment",
                "amount": x.amount,
            }
            for x in self.adjustments.non_taxable_income_adjustments
        ]

    @property
    def total_non_income_adjustments_amount(self) -> float:
        return sum(x.amount for x in self.adjustments.non_income_adjustments)

    @property
    def total_loan_deduction_amount(self) -> float:
        return sum(loan.amount for loan in self.loan_amortizations)

    @property
    def total_loan_deduction_list(self) -> list:
        return [
            {
                "label": x.name
                + (
                    " "
                    + json.loads(x.meta)["subtype"].replace("_", " ").title()
                    + " Loan"
                    if x.is_government_loan
                    else ""
                ),
                "amount": x.amount,
            }
            for x in self.loan_amortizations
        ]

    @property
    def total_loan_credited_amount(self) -> float:
        return sum(loan.amount for loan in self.loans_to_credit)

    @property
    def total_reduce_taxable_deminimis_amount(self) -> float:
        return self.deminimis.total_reduce_taxable_amount

    @property
    def total_reduce_taxable_deminimis_list(self) -> list:
        return [
            {
                "label": "Reduce Taxable Deminimis",
                "amount": self.total_reduce_taxable_deminimis_amount,
            }
        ]

    @property
    def total_non_taxable_deminimis_amount(self) -> float:
        return self.deminimis.total_non_taxable_amount

    @property
    def total_non_taxable_deminimis_list(self) -> list:
        return [
            {
                "label": "Non-Taxable Deminimis",
                "amount": self.total_non_taxable_deminimis_amount,
            }
        ]

    def breakdown_deminimis(self) -> None:
        self.breakdown["deminimis"] = {
            DeminimisType.NON_TAXABLE.value: list(self.deminimis.non_taxable),
            DeminimisType.REDUCE_TAXABLE.value: list(self.deminimis.reduce_taxable),
        }

    def breakdown_loans_to_credit(self) -> None:
        self.breakdown["loans_to_credit"] = self.loans_to_credit
        # self.breakdown["loans_to_credit"] = None

        # records: Dict = {"GOVERNMENT": [], "OTHER": []}
        # for loan in self.loans_to_credit:
        #     records["GOVERNMENT" if loan.is_government_loan else "OTHER"].append(loan)

        # self.breakdown["loan_amortizations"] = records

    def breakdown_loan_amortizations(self) -> None:
        self.breakdown["loan_amortizations"] = None

        records: Dict = {"GOVERNMENT": [], "OTHER": []}
        for loan in self.loan_amortizations:
            records["GOVERNMENT" if loan.is_government_loan else "OTHER"].append(loan)

        self.breakdown["loan_amortizations"] = records

    def breakdown_other_incomes(self) -> None:
        self.breakdown["other_incomes"] = None

        records: Dict = {
            "ALLOWANCE": {"taxable": [], "non_taxable": []},
            "BONUS": {"taxable": [], "non_taxable": []},
            "COMMISSION": {"taxable": [], "non_taxable": []},
        }
        for oi in self.other_incomes.records:
            records[oi.__class__.__name__.upper()][
                "taxable" if oi.is_taxable else "non_taxable"
            ].append(oi)

        self.breakdown["other_incomes"] = records

    def breakdown_deductions(self) -> None:
        self.breakdown["deductions"] = [ded for ded in self.deductions]

    def breakdown_adjustments(self) -> None:
        self.breakdown["adjustments"] = None
        adjs: Dict = {adj: [] for adj in AdjustmentType}

        for x in self.adjustments.records:
            adjs[x.type].append(x)

        self.breakdown["adjustments"] = adjs

    def _get_contrib_basis_amount(
        self, basis_type: ContributionIncomeType, contribution_enum: ContributionEnum
    ) -> float:
        prev_net_basis_amount = 0
        prev_gross_taxable_amount = 0

        if self._get_prev_total_basic_pay_amount(contribution_enum):
            prev_net_basis_amount = self._get_prev_total_basic_pay_amount(contribution_enum)

        if self._get_gross_taxable_amount(contribution_enum):
            prev_gross_taxable_amount = self._get_gross_taxable_amount(contribution_enum)

        if basis_type is ContributionIncomeType.BASIC:
            # get latest_base_pay monthly rate amount
            return self.basic_pay.base_pay_history.latest_base_pay.monthly_rate_amount
        elif basis_type is ContributionIncomeType.GROSS:
            return self.basic_pay.base_pay_history.latest_base_pay.monthly_rate_amount
        elif basis_type is ContributionIncomeType.GROSS_TAXABLE:
            return self.gross_income_taxable_amount + prev_gross_taxable_amount
        elif basis_type is ContributionIncomeType.NET_BASIC_PAY:
            return self.net_basic_pay_amount + prev_net_basis_amount
        else:
            # return FIXED amount from emp settings
            return self.employee_details.contribution_basis[contribution_enum].amount

    def _get_contrib_deducted_amount(
        self, contribution_enum: ContributionEnum
    ) -> float:
        final_pay_items = self.employee_details.basic_info.get("finalPayItems", [])
        for item in final_pay_items:
            if FinalPayItemType(item["type"]) == FinalPayItemType.CONTRIBUTION:
                if ContributionEnum(item["meta"]["type"]) == contribution_enum:
                    return (
                        float(item["meta"].get("amount",0)),
                        float(item["meta"].get("er_share",0)),
                        float(item["meta"].get("ee_share",0)),
                        float(item["meta"].get("ee_provident",0)),
                        float(item["meta"].get("er_provident",0)),
                        float(item["meta"].get("ec_share",0))
                    )

    def _get_prev_total_gross_amount(
        self, contribution_enum: ContributionEnum
    ) -> float:
        final_pay_items = self.employee_details.basic_info.get("finalPayItems", [])
        for item in final_pay_items:
            if FinalPayItemType(item["type"]) == FinalPayItemType.CONTRIBUTION:
                if ContributionEnum(item["meta"]["type"]) == contribution_enum:
                    return float(item["meta"].get("total_gross", 0))

    def _get_prev_total_basic_pay_amount(
        self, contribution_enum: ContributionEnum
    ) -> float:
        final_pay_items = self.employee_details.basic_info.get("finalPayItems", [])
        for item in final_pay_items:
            if FinalPayItemType(item["type"]) == FinalPayItemType.CONTRIBUTION:
                if ContributionEnum(item["meta"]["type"]) == contribution_enum:
                    return float(item["meta"].get("total_basic_pay", 0))

    def _get_gross_taxable_amount(
        self, contribution_enum: ContributionEnum
    ) -> float:
        final_pay_items = self.employee_details.basic_info.get("finalPayItems", [])
        for item in final_pay_items:
            if FinalPayItemType(item["type"]) == FinalPayItemType.CONTRIBUTION:
                if ContributionEnum(item["meta"]["type"]) == contribution_enum:
                    return float(item["meta"].get("gross_taxable", 0))
