# -*- coding: utf-8 -*-

from dataclasses import dataclass, field
from datetime import date
from typing import ClassVar, Dict, List, Optional, Tuple

from ._internal_data import MAX_AMOUNT
from ._internal_data import __hdmf_table as _InternalHDMFTable
from ._internal_data import __philhealth_table as _InternalPhilHealthTable
from ._internal_data import __sss_table as _InternalSSSTable
from ._internal_data import _HDMFTableEntry
from ._internal_data import _MinMaxPaynum as _InternalMinMaxPaynum
from ._internal_data import _PhilHealthTableEntry, _SSSTableEntry
from .base import divide_by_2_unequal, divide_unequal, money_round
from .constants import (
    ContributionIncomeType,
    ContributionSchedule,
    PayFrequency,
    SSSEmployeeType,
    TransactionType,
)

__all__ = [
    "NoMatchingSSSTableEntry",
    "NoMatchingHDMFTableEntry",
    "ContributionBasisRecord",
    "ContributionRecord",
    "PhilHealthContribution",
    "HDMFContribution",
    "SSSContribution",
]


class NoMatchingSSSTableEntry(RuntimeError):
    pass


class NoMatchingHDMFTableEntry(RuntimeError):
    pass


class NoContributionForPaySchedule(RuntimeError):
    pass


@dataclass(frozen=True)
class ContributionBasisRecord:
    basis_amount: float
    basis_type: ContributionIncomeType
    deduction_schedule: ContributionSchedule
    employee_income_deduction: float
    employee_share_amount: float
    employer_share_amount: float
    max_pay_schedule: int
    pay_schedule: int
    current_gross_income: float
    employee_compensation_amount: float = 0
    employee_provident_fund_amount: float = 0
    employer_provident_fund_amount: float = 0

@dataclass(frozen=True)
class ContributionRecord:
    basis_records: List[ContributionBasisRecord] = field(
        default_factory=list, compare=False
    )

    total_gross_income_amount: float = field(default=0, init=False)
    total_employee_share_amount: float = field(default=0, init=False)
    total_employer_share_amount: float = field(default=0, init=False)
    total_employee_income_deduction: float = field(default=0, init=False)
    total_employee_compensation_amount: float = field(default=0, init=False)
    total_employee_provident_fund_amount: float = field(default=0, init=False)
    total_employer_provident_fund_amount: float = field(default=0, init=False)

    def __post_init__(self):
        self._compute_totals()

    def __len__(self):
        return len(self.basis_records)

    def __getitem__(self, index):
        return self.basis_records[index]

    @property
    def basis_type(self) -> Optional[ContributionIncomeType]:
        return self.basis_records[-1].basis_type if self.basis_records else None

    def append_record(self, record: ContributionBasisRecord) -> None:
        self.basis_records.append(record)
        self._compute_totals()

    def _compute_totals(self) -> None:
        tgia, teesa, tersa, teeid, teeca, teepfa, terpfa = 0, 0, 0, 0, 0, 0, 0
        for r in self.basis_records:
            tgia += r.current_gross_income
            teesa += r.employee_share_amount
            tersa += r.employer_share_amount
            teeid += r.employee_income_deduction
            teeca += r.employee_compensation_amount
            teepfa += r.employee_provident_fund_amount
            terpfa += r.employer_provident_fund_amount

        object.__setattr__(self, "total_gross_income_amount", money_round(tgia))
        object.__setattr__(self, "total_employee_share_amount", money_round(teesa))
        object.__setattr__(self, "total_employer_share_amount", money_round(tersa))
        object.__setattr__(self, "total_employee_income_deduction", money_round(teeid))
        object.__setattr__(
            self, "total_employee_compensation_amount", money_round(teeca)
        )
        object.__setattr__(
            self, "total_employee_provident_fund_amount", money_round(teepfa)
        )
        object.__setattr__(
            self, "total_employer_provident_fund_amount", money_round(terpfa)
        )


@dataclass
class Contribution:
    effective_date: date = field(init=False)
    # https://blog.florimondmanca.com/reconciling-dataclasses-and-properties-in-python
    # we need to auto update based on changes to date
    __effective_date: date = field(init=False, repr=False)

    @property
    def max_ref_ee_share_amount(self) -> float:
        raise NotImplementedError

    def _get_reference_shares(
        self, basis_amount: float, basis_type: ContributionIncomeType
    ):
        raise NotImplementedError

    def _effective_date_changed(self):
        pass

    @property
    def min_ref_ee_share_amount(self) -> float:
        return 0

    @property
    def max_ref_er_share_amount(self) -> float:
        return self.max_ref_ee_share_amount

    @property
    def min_ref_er_share_amount(self) -> float:
        return self.min_ref_ee_share_amount

    @property
    def effective_date(self) -> date:
        return self.__effective_date

    @effective_date.setter
    def effective_date(self, effective_date: date):
        # catch first call when no effective_date was given in ctor
        if isinstance(effective_date, property):
            effective_date = date.today()

        self.__effective_date = effective_date
        self._effective_date_changed()

    def _validate_compute_schedule(
        self,
        pay_frequency: PayFrequency,
        deduction_schedule: ContributionSchedule,
        pay_schedule: int,
        max_pay_schedule: int,
        tran_type:TransactionType
    ) -> None:
        if pay_schedule > max_pay_schedule:
            # (╯°□°)╯︵ ┻━┻
            # ┻━┻ ︵ ヽ(°□°ヽ)
            raise ValueError

        pf_mapping = _InternalMinMaxPaynum

        if not (
            pf_mapping[pay_frequency][0]
            <= max_pay_schedule
            <= pf_mapping[pay_frequency][1]
        ):  # ┻━┻ ︵ ＼\('0')/／ ︵ ┻━┻
            raise ValueError

        sched_mapping = {
            ContributionSchedule.FIRST_PAY: 1,
            ContributionSchedule.SECOND_PAY: 2,
            ContributionSchedule.THIRD_PAY: 3,
            ContributionSchedule.FOURTH_PAY: 4,
            ContributionSchedule.FIFTH_PAY: 5,
        }

        if tran_type is not TransactionType.FINAL_NETPAY:
            if (
                deduction_schedule is ContributionSchedule.LAST_PAY
                and pay_schedule != max_pay_schedule
            ) or (
                # make sure deduction_schedule is in sched_mapping because
                # EVERY_PAY schedule does not need this pay_schedule matching check
                deduction_schedule in sched_mapping
                and sched_mapping[deduction_schedule] != pay_schedule
            ):
                # the user should not be paying any contribution because
                # it is not yet his/her schedule to pay
                raise NoContributionForPaySchedule

    def _validate_compute_params(
        self,
        pay_frequency: PayFrequency,
        basis_type: ContributionIncomeType,
        deduction_schedule: ContributionSchedule,
        pay_schedule: int,
        max_pay_schedule: int,
        tran_type: TransactionType,
    ) -> bool:
        try:
            if basis_type is ContributionIncomeType.NO_CONTRIBUTION:
                return True

            self._validate_compute_schedule(
                pay_frequency, deduction_schedule, pay_schedule, max_pay_schedule, tran_type
            )
            # RULE:
            # MONTHLY - DOESN'T MATTER
            # SEMI_MONTHLY
            #     BASIC - EVERY,FIRST,SECOND,LAST
            #     GROSS - EVERY,SECOND,LAST
            #     FIXED - EVERY,FIRST,SECOND,LAST
            # FORTNIGHTLY & WEEKLY
            #     BASIC - EVERY
            #     GROSS - EVERY
            #     FIXED - EVERY
            for_forth_and_weekly = {
                cit: [ContributionSchedule.EVERY_PAY] for cit in ContributionIncomeType
            }
            supported_combi = {
                PayFrequency.MONTHLY: {
                    # ContributionSchedule doesn't matter for MONTHLY, because we
                    # pay only once and thus will contrib
                    cit: [cs for cs in ContributionSchedule]
                    for cit in ContributionIncomeType
                },
                PayFrequency.SEMI_MONTHLY: {
                    ContributionIncomeType.BASIC: [
                        ContributionSchedule.EVERY_PAY,
                        ContributionSchedule.FIRST_PAY,
                        ContributionSchedule.SECOND_PAY,
                        ContributionSchedule.LAST_PAY,
                    ],
                    ContributionIncomeType.GROSS: [
                        ContributionSchedule.EVERY_PAY,
                        ContributionSchedule.SECOND_PAY,
                        ContributionSchedule.LAST_PAY,
                    ],
                    ContributionIncomeType.FIXED: [
                        ContributionSchedule.EVERY_PAY,
                        ContributionSchedule.FIRST_PAY,
                        ContributionSchedule.SECOND_PAY,
                        ContributionSchedule.LAST_PAY,
                    ],
                    ContributionIncomeType.GROSS_TAXABLE: [
                        ContributionSchedule.EVERY_PAY,
                        ContributionSchedule.SECOND_PAY,
                        ContributionSchedule.LAST_PAY,
                    ],
                    ContributionIncomeType.NET_BASIC_PAY: [
                        ContributionSchedule.EVERY_PAY,
                        ContributionSchedule.FIRST_PAY,
                        ContributionSchedule.SECOND_PAY,
                        ContributionSchedule.LAST_PAY,
                    ],
                },
                PayFrequency.FORTNIGHTLY: for_forth_and_weekly,
                PayFrequency.WEEKLY: for_forth_and_weekly,
            }
            supported_freq = [
                PayFrequency.MONTHLY,
                PayFrequency.SEMI_MONTHLY,
                PayFrequency.FORTNIGHTLY,
                PayFrequency.WEEKLY,
            ]

            if pay_frequency in supported_freq:
                contrib_checks = supported_combi[pay_frequency]
                if (
                    basis_type in contrib_checks
                    and deduction_schedule in contrib_checks[basis_type]
                ):
                    return True

            raise ValueError
        except ValueError:
            raise ValueError(
                f"Param combination invalid: "
                f"\n  pay_frequency -> {pay_frequency}"
                f"\n  basis_type -> {basis_type}"
                f"\n  deduction_schedule -> {deduction_schedule}"
                f"\n  pay_schedule -> {pay_schedule}"
                f"\n  max_pay_schedule -> {max_pay_schedule}"
            )

    def _compute_ref_amount(
        self,
        basis_amount: float,
        basis_type: ContributionIncomeType,
        current_gross_income: float,
        prev_contrib: ContributionRecord,
        tran_type:TransactionType,
        prev_total_gross: float
    ) -> float:
        prev_net_basis_amount = 0

        if prev_contrib.basis_records:
            prev_net_basis_amount = prev_contrib.basis_records[-1].basis_amount

        # either we add prev income as basis if GROSS or as is
        if basis_type is ContributionIncomeType.GROSS:
            if tran_type is not TransactionType.FINAL_NETPAY:
                return current_gross_income + prev_contrib.total_gross_income_amount
            else:
                return current_gross_income + prev_total_gross
        elif basis_type is ContributionIncomeType.GROSS_TAXABLE:
            if tran_type is not TransactionType.FINAL_NETPAY:
                return basis_amount + prev_net_basis_amount
            else:
                return basis_amount
        elif basis_type is ContributionIncomeType.NET_BASIC_PAY:
            if tran_type is not TransactionType.FINAL_NETPAY:
                return basis_amount + prev_net_basis_amount
            else:
                return basis_amount
        else:
            return basis_amount

    def _bound_ref_share_min_max(
        self, ee_share: float, er_share: float
    ) -> Tuple[float, float]:
        # RULE: make sure ee and er falls to the min-max boundaries
        # print(f"before bounding - {ee_share, er_share}")
        return (
            self._bound_min_max_amount(
                ee_share, self.min_ref_ee_share_amount, self.max_ref_ee_share_amount
            ),
            self._bound_min_max_amount(
                er_share, self.min_ref_er_share_amount, self.max_ref_er_share_amount
            ),
        )

    def _bound_min_max_amount(
        self, amount_to_be_bounded: float, min_bound: float, max_bound: float
    ):
        #  max() - make sure amount fall below within min_bound
        #  min() - make sure amount does not exceed max_bound
        return min(max(amount_to_be_bounded, min_bound), max_bound)

    # ALGO
    # - validate param combo raise exception if it is not in the expected scenario
    # - compute ref_amount based from basis_amount and prev depending on basis_type
    # - compute shares (ee&er|ec); and actual deduction
    # - remake record
    # - return shares, deduction, and record
    def compute(
        self,
        basis_amount: float,
        basis_type: ContributionIncomeType,
        pay_frequency: PayFrequency,
        deduction_schedule: ContributionSchedule,
        pay_schedule: int,
        max_pay_schedule: int,
        current_gross_income: float,
        tran_type: TransactionType,
        prev_total_gross: float,
        prev_contrib: ContributionRecord = ContributionRecord(),
    ) -> Tuple[float, float, float, ContributionRecord]:
        try:
            # - validate param combo raise exception if it is not in the expected scenario
            self._validate_compute_params(
                pay_frequency,
                basis_type,
                deduction_schedule,
                pay_schedule,
                max_pay_schedule,
                tran_type
            )
            # - compute ref_amount based from basis_amount and prev depending on basis_type
            ref_amount = self._compute_ref_amount(
                basis_amount, basis_type, current_gross_income, prev_contrib, tran_type, prev_total_gross
            )
            # - compute shares (ee&er|ec); and actual deduction
            ee_share, er_share, emp_income_deduction = self._compute_contribution(
                pay_frequency=pay_frequency,
                amount=ref_amount,
                basis_type=basis_type,
                deduction_schedule=deduction_schedule,
                pay_schedule=pay_schedule,
                max_pay_schedule=max_pay_schedule,
                prev_contrib=prev_contrib,
            )

            if (
                basis_type is ContributionIncomeType.FIXED
                and emp_income_deduction < ee_share
            ):
                ee_share = emp_income_deduction
        except NoContributionForPaySchedule:
            # no contribution should be payed for this execution
            #   usually happens like running the compute on pay_schedule 2 while
            #   contribution_schedule is FIRST_PAY
            ee_share, er_share, emp_income_deduction, ref_amount = 0, 0, 0, basis_amount

        # - remake record
        contrib_record = ContributionRecord(
            basis_records=prev_contrib.basis_records.copy()
        )
        # TIP: you can create the new basis_records with this in it to save recomputation
        contrib_record.append_record(
            ContributionBasisRecord(
                basis_amount=ref_amount,
                basis_type=basis_type,
                deduction_schedule=deduction_schedule,
                employee_income_deduction=emp_income_deduction,
                employee_share_amount=ee_share,
                employer_share_amount=er_share,
                max_pay_schedule=max_pay_schedule,
                pay_schedule=pay_schedule,
                current_gross_income=current_gross_income,
            )
        )

        # - return shares, deduction, and record
        return ee_share, er_share, emp_income_deduction, contrib_record

    def _compute_contribution(
        self,
        pay_frequency: PayFrequency,
        amount: float,
        basis_type: ContributionIncomeType,
        deduction_schedule: ContributionSchedule,
        pay_schedule: int,
        max_pay_schedule: int,
        prev_contrib: ContributionRecord,
    ) -> Tuple[float, float, float]:
        ee_share, er_share = self._get_reference_shares(amount, basis_type)
        emp_income_deduction = (
            amount if basis_type is ContributionIncomeType.FIXED else ee_share
        )  # actual deduction is the basis_amount for ContributionIncomeType.FIXED

        is_gross_branch = 0
        if (
            basis_type is ContributionIncomeType.GROSS
            or basis_type is ContributionIncomeType.GROSS_TAXABLE
            or basis_type is ContributionIncomeType.NET_BASIC_PAY
        ):
            is_gross_branch = 1

        # RULE: there's no GROSS FIRST_PAY, if there is, split this function by basis_type
        if (
            is_gross_branch == 0
            and pay_schedule < max_pay_schedule
            and deduction_schedule is not ContributionSchedule.FIRST_PAY
        ):
            # this logic is for BASIC and FIXED ONLY
            ee_share, er_share = (
                divide_unequal(ee_share, max_pay_schedule)[pay_schedule - 1],
                divide_unequal(er_share, max_pay_schedule)[pay_schedule - 1],
            )
            emp_income_deduction = (
                divide_unequal(emp_income_deduction, max_pay_schedule)[pay_schedule - 1]
                if basis_type is ContributionIncomeType.FIXED
                else ee_share
            )
        else:
            # NOTE: this is the entire GROSS contrib calculation regardless of pay sched
            # NOTE: this is the LAST_PAY of the MONTH (BASIC|FIXED), so to cover possible
            #   salary adjustments, we catch up to the final contrib amount for the month
            ee_share -= prev_contrib.total_employee_share_amount
            er_share -= prev_contrib.total_employer_share_amount
            emp_income_deduction -= prev_contrib.total_employee_income_deduction

        ee_share, er_share, _, emp_income_deduction = self._bound_month_contrib_min_max(
            basis_type, ee_share, er_share, 0, emp_income_deduction, prev_contrib
        )

        # NOTE: we need to round this since this will be used for records purposes
        return (
            money_round(ee_share),
            money_round(er_share),
            money_round(emp_income_deduction),
        )

    def _bound_month_contrib_min_max(
        self,
        basis_type: ContributionIncomeType,
        ee_share: float,
        er_share: float,
        ec_share: float,
        emp_income_deduction: float,
        prev_contrib: ContributionRecord,
        ref_share=None,
    ) -> Tuple[float, float, float, float]:
        # TODO: explain this shit; clue: this will bound the overall contrib for the month
        min_ee, max_ee, min_er, max_er, min_ec, max_ec = 0, 0, 0, 0, 0, 0
        min_emp_income_deduction, max_emp_income_deduction = 0, 0

        prev_ee_share = prev_contrib.total_employee_share_amount
        prev_er_share = prev_contrib.total_employer_share_amount
        prev_ec_share = prev_contrib.total_employee_compensation_amount
        prev_emp_income_deduction = prev_contrib.total_employee_income_deduction
        ret_ee, ret_er, ret_ec = 0, 0, 0

        if ref_share:
            min_ee, max_ee = ref_share["min_ee_share"], ref_share["max_ee_share"]
            min_er, max_er = ref_share["min_er_share"], ref_share["max_er_share"]
            min_ec, max_ec = ref_share["min_ec_share"], ref_share["max_ec_share"]
            min_emp_income_deduction, max_emp_income_deduction = (
                ref_share["min_income_deduction"],
                ref_share["max_income_deduction"],
            )
        else:
            min_ee, max_ee = self.min_ref_ee_share_amount, self.max_ref_ee_share_amount
            min_er, max_er = self.min_ref_er_share_amount, self.max_ref_er_share_amount
            min_emp_income_deduction, max_emp_income_deduction = (
                0,
                self.max_ref_ee_share_amount,
            )

        # no limit (in this case very high amount) for FIXED contribution
        if basis_type is ContributionIncomeType.FIXED:
            max_emp_income_deduction = MAX_AMOUNT

        tot_ee = ee_share + prev_ee_share
        tot_er = er_share + prev_er_share
        tot_ec = ec_share + prev_ec_share
        tot_deduction = emp_income_deduction + prev_emp_income_deduction

        # make sure total contrib won't go below minimum for the month
        ret_ee = (min_ee - prev_ee_share) if tot_ee < min_ee else ee_share
        ret_er = (min_er - prev_er_share) if tot_er < min_er else er_share
        ret_ec = (min_ec - prev_ec_share) if tot_ec < min_ec else ec_share
        ret_deduction = (
            (min_emp_income_deduction - prev_emp_income_deduction)
            if tot_deduction < min_ec
            else emp_income_deduction
        )

        # make sure total contrib won't go above maximum for the month
        # possible return cost here
        ret_ee = (max_ee - prev_ee_share) if tot_ee > max_ee else ee_share
        ret_er = (max_er - prev_er_share) if tot_er > max_er else er_share
        ret_ec = (max_ec - prev_ec_share) if tot_ec > max_ec else ec_share
        ret_deduction = (
            (max_emp_income_deduction - prev_emp_income_deduction)
            if tot_deduction > max_emp_income_deduction
            else emp_income_deduction
        )

        return ret_ee, ret_er, ret_ec, ret_deduction


@dataclass
class PhilHealthContribution(Contribution):
    __ref_tbl: Dict = field(default_factory=dict)

    __reference_table_cols: ClassVar[List[str]] = [
        # "effectivity_start_date",
        # "effectivity_end_date",
        "premium_rate_multiplier",
        "min_employee_share_amount",
        "max_employee_share_amount",
        "min_employer_share_amount",
        "max_employer_share_amount",
    ]

    @property
    def min_ref_ee_share_amount(self) -> float:
        # total min amount ee and er / 2
        return self.reference_table["config"]["min_employee_share_amount"]

    @property
    def max_ref_ee_share_amount(self) -> float:
        # total max amount ee and er / 2
        return self.reference_table["config"]["max_employee_share_amount"]

    def _get_reference_shares(
        self, basis_amount: float, basis_type: ContributionIncomeType
    ) -> Tuple[float, float]:
        if basis_type is ContributionIncomeType.NO_CONTRIBUTION or basis_amount <= 0:
            # early return to bypass _bound_ref_share_min_max
            return 0, 0
        elif basis_type is ContributionIncomeType.FIXED:
            ee_share, er_share = basis_amount, basis_amount
        else:
            ee_share, er_share = divide_by_2_unequal(
                money_round(
                    basis_amount
                    * self.reference_table["config"]["premium_rate_multiplier"]
                )
            )

        return self._bound_ref_share_min_max(ee_share, er_share)

    @property
    def reference_table(self) -> Dict:
        if not self.__ref_tbl:
            filtered = filter(self._filter_table, _InternalPhilHealthTable)
            table = list(
                {
                    k: getattr(d, k)
                    for k in d._fields
                    if k in PhilHealthContribution.__reference_table_cols
                }
                for d in filtered
            )
            self.__ref_tbl = {
                # for PhilHealth it is config that should be used for compute
                "config": table[0],
                # table is for log purposes only
                "table": table,
                # "version": self.effective_date.strftime("%Y-%m-%d"),
            }
        return self.__ref_tbl

    def _filter_table(self, x: _PhilHealthTableEntry) -> bool:
        # filter dates between
        return (
            x.effectivity_start_date <= self.effective_date
            and x.effectivity_end_date is None
        ) or (x.effectivity_start_date <= self.effective_date < x.effectivity_end_date)

    def __philhealth_custom_rounding(self, num: float) -> float:
        # NOT USED
        # this is a custom rounding function for float that will only round upto the
        # hundrendths and only considering the thousandths in the rounding logic
        #   1.45   == 1.45
        #   1.451  == 1.45
        #   1.454  == 1.45
        #   1.4545 == 1.45
        #   1.455  == 1.46
        # TODO: his has a bug when num is like 474.9982829670329
        num_str = str(num)
        if "." in num_str:
            int_str, dec_str = num_str.split(".")

            if len(dec_str) > 2:
                money_decimal_digits = int(dec_str[0:2])
                thousandths_digit = int(dec_str[2:3])

                if thousandths_digit > 4:
                    money_decimal_digits += 1

                rounded_num = float(
                    ".".join([int_str, str(money_decimal_digits).zfill(2)])
                )

                return rounded_num

        return num


@dataclass
class HDMFContribution(Contribution):
    __ref_tbl: Dict = field(default_factory=dict)

    __reference_table_cols: ClassVar[List[str]] = [
        # "effectivity_start_date",
        # "effectivity_end_date",
        "min_amount",
        "max_amount",
        "employee_share_multiplier",
        "employer_share_multiplier",
    ]

    @property
    def max_ref_ee_share_amount(self) -> float:
        return 100

    def get_reference_table_entry(
        self, basis_amount: float, basis_type: ContributionIncomeType
    ) -> _HDMFTableEntry:

        for row in self.reference_table["table"]:
            if row["min_amount"] <= basis_amount < row["max_amount"]:
                return row

        raise NoMatchingHDMFTableEntry(
            f"Can't find match for amount({basis_amount})"
            f" within effective_date({self.effective_date})"
            f" using table -- {self.reference_table}"
        )

    def _get_reference_shares(
        self, basis_amount: float, basis_type: ContributionIncomeType
    ) -> Tuple[float, float]:
        if basis_type is ContributionIncomeType.NO_CONTRIBUTION or basis_amount <= 0:
            # early return to bypass _bound_ref_share_min_max
            return 0, 0
        elif basis_type is ContributionIncomeType.FIXED:
            ee_share, er_share = basis_amount, basis_amount
        else:
            ref_entry = self.get_reference_table_entry(basis_amount, basis_type)
            ee_share, er_share = (
                basis_amount * ref_entry["employee_share_multiplier"],
                basis_amount * ref_entry["employer_share_multiplier"],
            )

        return self._bound_ref_share_min_max(ee_share, er_share)

    @property
    def reference_table(self) -> Dict:
        if not self.__ref_tbl:
            filtered = filter(self._filter_table, _InternalHDMFTable)
            table = list(
                {
                    k: getattr(d, k)
                    for k in d._fields
                    if k in HDMFContribution.__reference_table_cols
                }
                for d in filtered
            )
            self.__ref_tbl = {
                "table": table,
                # "version": self.effective_date.strftime("%Y-%m-%d"),
            }
        return self.__ref_tbl

    def _filter_table(self, x: _HDMFTableEntry) -> bool:
        # filter dates between
        return (
            x.effectivity_start_date <= self.effective_date
            and x.effectivity_end_date is None
        ) or (x.effectivity_start_date <= self.effective_date <= x.effectivity_end_date)

    def _effective_date_changed(self) -> None:
        # RULE: for HDMF since there is just one table no need to refresh
        pass
        # clear reference table cache, everytime date changes so it will recompute
        # self.__ref_tbl = None


@dataclass
class SSSContribution(Contribution):
    employee_type: SSSEmployeeType = SSSEmployeeType.REGULAR

    __ref_tbl: Dict = field(default_factory=dict)

    __reference_table_cols: ClassVar[List[str]] = [
        # "employee_type",
        # "effectivity_start_date",
        # "effectivity_end_date",
        "min_amount",
        "max_amount",
        "monthly_salary_credit_amount",
        "employee_share_amount",
        "employer_share_amount",
        "employee_compensation_amount",
        "employee_mandatory_provident_fund_amount",
        "employer_mandatory_provident_fund_amount",
    ]

    def get_reference_table_entry(
        self, basis_amount: float, basis_type: ContributionIncomeType
    ) -> _SSSTableEntry:
        ref = None
        ref_table = self.reference_table["table"]

        # get basis_amount and excess if present in fixed basis_type
        if basis_type is ContributionIncomeType.FIXED:
            basis_amount, excess = (basis_amount, 0)\
                if basis_amount <= ref_table[-1]['employee_share_amount']\
                else (
                    ref_table[-1]['employee_share_amount'],
                    basis_amount - ref_table[-1]['employee_share_amount']
                )

        # Reverse the table in to descending order
        # and get the first and highest fund value from the list that matched
        for row in ref_table[::-1]:
            if basis_type is not ContributionIncomeType.FIXED:
                if row["min_amount"] <= basis_amount < row["max_amount"]:
                    return row
            else:
                # get the nearest share_amount less than to the FIXED basis_amount
                if not ref or (
                    row["employee_share_amount"] <= basis_amount
                    # since we are now moving in descending manner,
                    # we will always get the highest value because initially ref is None
                    # so when we find another row that is lower than or equal
                    # to our fixed basis_amount, we need to make sure that the
                    # ref that we will be replacing is indeed greater
                    # than our basis_amount
                    and ref["employee_share_amount"] >= basis_amount
                    # get the nearest employee provident fund based on excess
                    # replacing the reference row until the ref provident
                    # does not exceed the computed excess
                    and row['employee_mandatory_provident_fund_amount'] <= excess
                    and ref["employee_mandatory_provident_fund_amount"] > excess
                ):
                    ref = row

        if ref:
            return ref

        raise NoMatchingSSSTableEntry(
            f"Can't find match for basis: amount({basis_amount}) type({basis_type})"
            f" within effective_date({self.effective_date})"
            f" using table -- {self.reference_table}"
        )

    @property
    def reference_table(self) -> Dict:
        if not self.__ref_tbl:
            filtered = filter(self._filter_table, _InternalSSSTable)
            table = list(
                {
                    k: getattr(d, k)
                    for k in d._fields
                    if k in SSSContribution.__reference_table_cols
                }
                for d in filtered
            )
            self.__ref_tbl = {
                "sss_employee_type": self.employee_type.value,
                "table": table,
                # "version": self.effective_date.strftime("%Y-%m-%d"),
            }
        return self.__ref_tbl

    def _filter_table(self, x: _SSSTableEntry) -> bool:
        # filter employee_type and dates between
        return x.employee_type is self.employee_type and (
            (
                x.effectivity_start_date <= self.effective_date
                and x.effectivity_end_date is None
            )
            or (
                x.effectivity_start_date
                <= self.effective_date
                <= x.effectivity_end_date
            )
        )

    def _effective_date_changed(self):
        # clear reference table cache, everytime date changes so it will recompute
        self.__ref_tbl = None

    def compute(
        self,
        basis_amount: float,
        basis_type: ContributionIncomeType,
        pay_frequency: PayFrequency,
        deduction_schedule: ContributionSchedule,
        pay_schedule: int,
        max_pay_schedule: int,
        current_gross_income: float,
        tran_type: TransactionType,
        prev_total_gross: float,
        prev_contrib: ContributionRecord = ContributionRecord(),
    ) -> Tuple[float, float, float, float, ContributionRecord]:
        try:
            # had to copy Contribution.compute() function because of ec_share
            # - validate param combo raise exception if it is not in the expected scenario
            self._validate_compute_params(
                pay_frequency,
                basis_type,
                deduction_schedule,
                pay_schedule,
                max_pay_schedule,
                tran_type
            )
            # - compute ref_amount based from basis_amount and prev depending on basis_type
            ref_amount = self._compute_ref_amount(
                basis_amount, basis_type, current_gross_income, prev_contrib, tran_type, prev_total_gross
            )
            # - compute shares (ee&er|ec); and actual deduction
            (
                ee_share,
                er_share,
                ec_share,
                emp_income_deduction,
                ee_provident_fund,
                er_provident_fund,
            ) = self._compute_contribution(
                pay_frequency=pay_frequency,
                amount=ref_amount,
                basis_type=basis_type,
                deduction_schedule=deduction_schedule,
                pay_schedule=pay_schedule,
                max_pay_schedule=max_pay_schedule,
                prev_contrib=prev_contrib,
            )

            if (
                basis_type is ContributionIncomeType.FIXED
                and ref_amount < emp_income_deduction
            ):
                emp_income_deduction = ref_amount

            if (
                basis_type is ContributionIncomeType.FIXED
                and emp_income_deduction < ee_share
                and pay_schedule < max_pay_schedule
            ):
                ee_share = emp_income_deduction

        except NoContributionForPaySchedule:
            # no contribution should be payed for this execution
            #   usually happens like running the compute on pay_schedule 2 while
            #   contribution_schedule is FIRST_PAY
            ee_share, er_share, ec_share, emp_income_deduction, ee_provident_fund, er_provident_fund, ref_amount = (
                0,
                0,
                0,
                0,
                0,
                0,
                basis_amount,
            )

        # - remake record
        contrib_record = ContributionRecord(
            basis_records=prev_contrib.basis_records.copy()
        )
        # TIP: you can create the new basis_records with this in it to save recomputation
        contrib_record.append_record(
            ContributionBasisRecord(
                basis_amount=ref_amount,
                basis_type=basis_type,
                deduction_schedule=deduction_schedule,
                employee_income_deduction=emp_income_deduction,
                employee_share_amount=ee_share,
                employer_share_amount=er_share,
                employee_compensation_amount=ec_share,
                max_pay_schedule=max_pay_schedule,
                pay_schedule=pay_schedule,
                current_gross_income=current_gross_income,
                employee_provident_fund_amount=ee_provident_fund,
                employer_provident_fund_amount=er_provident_fund,
            )
        )

        # - return shares, deduction, and record
        return (
            ee_share,
            er_share,
            ec_share,
            emp_income_deduction,
            ee_provident_fund,
            er_provident_fund,
            contrib_record,
        )

    def _compute_contribution(
        self,
        pay_frequency: PayFrequency,
        amount: float,
        basis_type: ContributionIncomeType,
        deduction_schedule: ContributionSchedule,
        pay_schedule: int,
        max_pay_schedule: int,
        prev_contrib: ContributionRecord,
    ) -> Tuple[float, float, float, float]:
        # had to copy same function because of ec_share

        # need to have/keep og values for month bound later
        (
            og_ee_share,
            og_er_share,
            og_ec_share,
            og_ee_provident_fund,
            og_er_provident_fund,
        ) = self._get_reference_shares(amount, basis_type)
        # actual deduction is the basis_amount for ContributionIncomeType.FIXED
        og_emp_income_deduction = og_ee_share + og_ee_provident_fund
        ee_share, er_share, ec_share = og_ee_share, og_er_share, og_ec_share
        emp_income_deduction = og_emp_income_deduction
        ee_provident_fund, er_provident_fund = (
            og_ee_provident_fund,
            og_er_provident_fund,
        )

        is_gross_branch = 0
        if (
            basis_type is ContributionIncomeType.GROSS
            or basis_type is ContributionIncomeType.GROSS_TAXABLE
            or basis_type is ContributionIncomeType.NET_BASIC_PAY
        ):
            is_gross_branch = 1

        # RULE: there's no GROSS FIRST_PAY, if there is, split this function by basis_type
        if (
            is_gross_branch == 0
            and pay_schedule < max_pay_schedule
            and deduction_schedule is not ContributionSchedule.FIRST_PAY
        ):
            # this logic is for BASIC and FIXED ONLY
            ee_share, er_share = (
                divide_unequal(ee_share, max_pay_schedule)[pay_schedule - 1],
                divide_unequal(er_share, max_pay_schedule)[pay_schedule - 1],
            )
            ee_provident_fund, er_provident_fund = (
                divide_unequal(ee_provident_fund, max_pay_schedule)[pay_schedule - 1],
                divide_unequal(er_provident_fund, max_pay_schedule)[pay_schedule - 1],
            )
            emp_income_deduction = (
                divide_unequal(emp_income_deduction, max_pay_schedule)[pay_schedule - 1]
                if basis_type is ContributionIncomeType.FIXED
                else ee_share + ee_provident_fund
            )
        else:
            # NOTE: this is the entire GROSS contrib calculation regardless of pay sched
            # NOTE: this is the LAST_PAY of the MONTH (BASIC|FIXED), so to cover possible
            # salary adjustments, we catch up to the final contrib amount for the month
            ee_share -= prev_contrib.total_employee_share_amount
            er_share -= prev_contrib.total_employer_share_amount
            emp_income_deduction -= prev_contrib.total_employee_income_deduction
            ee_provident_fund -= prev_contrib.total_employee_provident_fund_amount
            er_provident_fund -= prev_contrib.total_employer_provident_fund_amount

        ec_share -= prev_contrib.total_employee_compensation_amount

        # max sure contribution won't go overboard the max monthly amount, return if needed
        (
            ee_share,
            er_share,
            ec_share,
            emp_income_deduction,
        ) = self._bound_month_contrib_min_max(
            basis_type,
            ee_share,
            er_share,
            ec_share,
            emp_income_deduction,
            prev_contrib,
            # we need to add both provident and normal share to properly
            # consider the allowed bounds for deducting
            {
                "min_ee_share": 0,
                "max_ee_share": og_ee_share + og_ee_provident_fund,
                "min_er_share": 0,
                "max_er_share": og_er_share + og_er_provident_fund,
                "min_ec_share": 0,
                "max_ec_share": og_ec_share,
                "min_income_deduction": 0,
                "max_income_deduction": og_emp_income_deduction,
            },
        )

        # I think we won't be needing this because the bounds is already
        # made from both share and provident_fund
        #
        # # bound the values base on the prodivent range
        # (
        #     ee_provident_fund,
        #     er_provident_fund,
        #     emp_income_deduction,
        # ) = self._bound_month_provident_fund_min_max(
        #     basis_type,
        #     ee_provident_fund,
        #     er_provident_fund,
        #     emp_income_deduction,
        #     prev_contrib,
        #     {
        #         "min_ee_provident_fund": 0,
        #         "max_ee_provident_fund": og_ee_provident_fund + og_ee_share,
        #         "min_er_provident_fund": 0,
        #         "max_er_provident_fund": og_er_provident_fund + og_er_share,
        #         "min_income_deduction": 0,
        #         "max_income_deduction": og_emp_income_deduction,
        #     },
        # )

        # NOTE: we need to round this since this will be used for records purposes
        return (
            money_round(ee_share),
            money_round(er_share),
            money_round(ec_share),
            money_round(emp_income_deduction),
            money_round(ee_provident_fund),
            money_round(er_provident_fund),
        )

    def _get_reference_shares(
        self, basis_amount: float, basis_type: ContributionIncomeType
    ) -> Tuple[float, float, float]:
        if basis_type is ContributionIncomeType.NO_CONTRIBUTION or basis_amount <= 0:
            return 0, 0, 0, 0, 0

        ref = self.get_reference_table_entry(basis_amount, basis_type)
        return (
            ref["employee_share_amount"],
            ref["employer_share_amount"],
            ref["employee_compensation_amount"],
            ref["employee_mandatory_provident_fund_amount"],
            ref["employer_mandatory_provident_fund_amount"],
        )

    # def _bound_month_provident_fund_min_max(
    #     self,
    #     basis_type: ContributionIncomeType,
    #     ee_provident_fund: float,
    #     er_provident_fund: float,
    #     emp_income_deduction: float,
    #     prev_contrib: ContributionRecord,
    #     ref_provident_fund,
    # ) -> Tuple[float, float, float, float]:
    #     min_ee, max_ee, min_er, max_er, min_ec = 0, 0, 0, 0, 0
    #     min_emp_income_deduction, max_emp_income_deduction = 0, 0

    #     prev_ee_provident_fund = prev_contrib.total_employee_provident_fund_amount
    #     prev_er_provident_fund = prev_contrib.total_employer_provident_fund_amount
    #     prev_emp_income_deduction = prev_contrib.total_employee_income_deduction
    #     ret_ee, ret_er = 0, 0

    #     # difference from _bound_month_contrib_min_max: we always pass a ref_provident_fund
    #     min_ee = ref_provident_fund["min_ee_provident_fund"]
    #     max_ee = ref_provident_fund["max_ee_provident_fund"]

    #     min_er = ref_provident_fund["min_er_provident_fund"]
    #     max_er = ref_provident_fund["max_er_provident_fund"]

    #     min_emp_income_deduction = ref_provident_fund["min_income_deduction"]
    #     max_emp_income_deduction = ref_provident_fund["max_income_deduction"]

    #     # no limit (in this case very high amount) for FIXED contribution
    #     if basis_type is ContributionIncomeType.FIXED:
    #         max_emp_income_deduction = MAX_AMOUNT

    #     tot_ee = ee_provident_fund + prev_ee_provident_fund
    #     tot_er = er_provident_fund + prev_er_provident_fund
    #     tot_deduction = emp_income_deduction + prev_emp_income_deduction

    #     # make sure total contrib won't go below minimum for the month
    #     ret_ee = (
    #         (min_ee - prev_ee_provident_fund) if tot_ee < min_ee else ee_provident_fund
    #     )
    #     ret_er = (
    #         (min_er - prev_er_provident_fund) if tot_er < min_er else er_provident_fund
    #     )
    #     ret_deduction = (
    #         (min_emp_income_deduction - prev_emp_income_deduction)
    #         if tot_deduction < min_ec
    #         else emp_income_deduction
    #     )

    #     # make sure total contrib won't go above maximum for the month
    #     # possible return cost here
    #     ret_ee = (
    #         (max_ee - prev_ee_provident_fund) if tot_ee > max_ee else ee_provident_fund
    #     )
    #     ret_er = (
    #         (max_er - prev_er_provident_fund) if tot_er > max_er else er_provident_fund
    #     )
    #     ret_deduction = (
    #         (max_emp_income_deduction - prev_emp_income_deduction)
    #         if tot_deduction > max_emp_income_deduction
    #         else emp_income_deduction
    #     )

    #     return ret_ee, ret_er, ret_deduction
