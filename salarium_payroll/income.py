# -*- coding: utf-8 -*-

from calendar import monthrange
from dataclasses import dataclass, field
from datetime import date, datetime
from typing import Dict, Iterator, List, Optional, Tuple, Union

from dateutil.relativedelta import relativedelta
from dateutil.rrule import MONTHLY, rrule

from .attendance import Attendance
from .base import (
    BasePay,
    BasePayHistory,
    EmployeeDetails,
    PayHistory,
    PayrollGroup,
    divide_unequal,
)
from .constants import AdjustmentType, DeminimisType
from ._internal_data import _AllowanceHolidayRegularHourTypes


@dataclass(frozen=True)
class OtherIncome:
    type: str
    amount: float
    is_taxable: bool
    meta: dict = field(default_factory=lambda: {"is_uploaded": False})


@dataclass(frozen=True)
class Allowance(OtherIncome):
    pass


@dataclass(frozen=True)
class Bonus(OtherIncome):
    pass


@dataclass(frozen=True)
class Commission(OtherIncome):
    pass


@dataclass
class OtherIncomeRecord:
    records: List[OtherIncome]

    @property
    def taxable_other_incomes(self) -> Iterator:
        return self.__filter_other_income_by_taxability(True, self.records)

    @property
    def non_taxable_other_incomes(self) -> Iterator:
        return self.__filter_other_income_by_taxability(False, self.records)

    @property
    def allowances(self) -> Iterator:
        return self.__filter_other_income_by_type(Allowance)

    @property
    def bonuses(self) -> Iterator:
        return self.__filter_other_income_by_type(Bonus)

    @property
    def commissions(self) -> Iterator:
        return self.__filter_other_income_by_type(Commission)

    @property
    def taxable_allowances(self) -> Iterator:
        return self.__filter_other_income_by_taxability(True, self.allowances)

    @property
    def non_taxable_allowances(self) -> Iterator:
        return self.__filter_other_income_by_taxability(False, self.allowances)

    @property
    def taxable_bonuses(self) -> Iterator:
        return self.__filter_other_income_by_taxability(True, self.bonuses)

    @property
    def non_taxable_bonuses(self) -> Iterator:
        return self.__filter_other_income_by_taxability(False, self.bonuses)

    @property
    def taxable_commissions(self) -> Iterator:
        return self.__filter_other_income_by_taxability(True, self.commissions)

    @property
    def non_taxable_commissions(self) -> Iterator:
        return self.__filter_other_income_by_taxability(False, self.commissions)

    def __filter_other_income_by_type(self, obj_type) -> Iterator:
        return filter(lambda x: isinstance(x, obj_type), self.records)

    def __filter_other_income_by_taxability(self, is_taxable, oi_list) -> Iterator:
        return filter(lambda x: x.is_taxable == is_taxable, oi_list)


@dataclass
class DeMinimis:
    name: str
    amount: float
    type: DeminimisType
    meta: str = ""


@dataclass
class DeMinimisRecord:
    records: List[DeMinimis]

    @property
    def non_taxable(self) -> Iterator:
        return filter(lambda x: x.type is DeminimisType.NON_TAXABLE, self.records)

    @property
    def reduce_taxable(self) -> Iterator:
        return filter(lambda x: x.type is DeminimisType.REDUCE_TAXABLE, self.records)

    @property
    def total_reduce_taxable_amount(self) -> float:
        # - taxable_income
        return sum(x.amount for x in self.reduce_taxable)

    @property
    def total_non_taxable_amount(self) -> float:
        # + gross_income
        # - taxable_income
        return sum(x.amount for x in self.non_taxable)


@dataclass(frozen=True)
class Deduction:
    type: str
    amount: float
    meta: dict = field(default_factory=lambda: {"is_uploaded": False})


@dataclass(frozen=True)
class Adjustment:
    type: AdjustmentType
    amount: float
    reason: str
    meta: str = ""
    # not really needed but just to cover for the existing data
    # payroll_date: date
    # for_special_payrun: bool = False


@dataclass
class AdjustmentRecord:
    records: List[Adjustment]

    @property
    def tax_adjustments(self) -> Iterator:
        return self.__filter_adjustment_by_type(AdjustmentType.TAX_ADJUSTMENT)

    @property
    def taxable_income_adjustments(self) -> Iterator:
        return self.__filter_adjustment_by_type(AdjustmentType.TAXABLE_INCOME)

    @property
    def non_taxable_income_adjustments(self) -> Iterator:
        return self.__filter_adjustment_by_type(AdjustmentType.NON_TAXABLE_INCOME)

    @property
    def non_income_adjustments(self) -> Iterator:
        return self.__filter_adjustment_by_type(AdjustmentType.NON_INCOME)

    @property
    def philhealth_employee_contribution_adjustments(self) -> Iterator:
        return self.__filter_adjustment_by_type(AdjustmentType.PHILHEALTH_EMPLOYEE_CONTRIBUTION)

    @property
    def philhealth_employer_contribution_adjustments(self) -> Iterator:
        return self.__filter_adjustment_by_type(AdjustmentType.PHILHEALTH_EMPLOYER_CONTRIBUTION)

    def __filter_adjustment_by_type(self, adj_type: AdjustmentType) -> Iterator:
        return filter(lambda x: x.type is adj_type, self.records)


@dataclass
class AABCDGenerator:
    payroll_group: PayrollGroup
    attendance: Attendance
    employee_details: EmployeeDetails

    base_pay_history: BasePayHistory = BasePayHistory(records=[])
    basic_pay_history: PayHistory = PayHistory(records=[])
    gross_basic_history: PayHistory = PayHistory(records=[])
    gross_income_history: PayHistory = PayHistory(records=[])

    adjustment_settings: List = field(default_factory=list)
    allowance_settings: List = field(default_factory=list)
    bonus_settings: List = field(default_factory=list)
    commission_settings: List = field(default_factory=list)
    deduction_settings: List = field(default_factory=list)
    deminimies_allowances = ('reduce_to_taxable_income',)

    def filter_deminimies_from_allowances(self):
        """
        Filter the deminimies from allowances by applying
        all the filters same as allowances
        """
        # Ticket: https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7943
        # Ticket: https://code.salarium.com/salarium/development/t-and-a-manila/-/issues/7964
        filtered = filter(
            lambda settings: self._filter_by_frequency_schedule_allowances(settings),
            filter(
                lambda settings: self._filter_validity(settings)
                and not settings["release_details"][0][
                    "disburse_through_special_pay_run"
                ] and settings['tax_option'] in self.deminimies_allowances,
                self.allowance_settings,
            ),
        )
        return filtered

    def generate_allowances(self) -> List[Allowance]:
        filtered = filter(
            lambda settings: self._filter_by_frequency_schedule_allowances(settings),
            filter(
                lambda settings: self._filter_validity(settings)
                and not settings["release_details"][0][
                    "disburse_through_special_pay_run"
                ] and settings['tax_option'] not in self.deminimies_allowances,
                self.allowance_settings,
            ),
        )

        att_ratios = self._compute_attendance_ratio()
        mapped = map(
            lambda settings: self._create_allowance(settings, att_ratios), filtered
        )

        the_list = []
        for x in mapped:
            # flat the list of allowance (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_allowance(
        self, settings, attendance_ratios
    ) -> Union[Allowance, Tuple[Allowance, Allowance]]:
        allowance_meta = settings["meta"]
        (
            allowance_meta["basis_amount"],
            allowance_meta["given_amount"],
            allowance_meta["deducted_amount"],
        ) = self._get_allowance_amount(settings, attendance_ratios)

        if (
            settings["max_non_taxable"]
            and settings["max_non_taxable"] > 0
            and allowance_meta["given_amount"] > settings["max_non_taxable"]
        ):
            # split to two allowance taxable and non-taxable
            allowance_meta["max_non_taxable"] = settings["max_non_taxable"]
            allowance_meta["taxable_split"] = (
                allowance_meta["given_amount"] - settings["max_non_taxable"]
            )

            return (
                Allowance(
                    type=settings["name"],
                    amount=settings["max_non_taxable"],
                    is_taxable=False,
                    meta=allowance_meta,
                ),
                Allowance(
                    type=settings["name"],
                    amount=allowance_meta["taxable_split"],
                    is_taxable=True,
                    meta=allowance_meta,
                ),
            )

        return Allowance(
            type=settings["name"],
            amount=allowance_meta["given_amount"],
            is_taxable=settings.get("tax_option") == "fully_taxable",
            meta=allowance_meta,
        )

    def _get_allowance_amount(
        self, settings, attendance_ratios
    ) -> Tuple[float, float, float]:
        # "PER_PAY", "FIRST_PAY_OF_THE_MONTH", "LAST_PAY_OF_THE_MONTH", "DAILY", "PER_MONTH"
        frequency_schedule = settings["frequency_schedule"]

        basis_allowance_amount = settings["amount_details"]["amount"]
        if "PER_MONTH" == frequency_schedule:
            basis_allowance_amount = divide_unequal(
                settings["amount_details"]["amount"],
                self.payroll_group.max_pay_schedule,
            )[self.payroll_group.pay_schedule - 1]
        elif "DAILY" == frequency_schedule:
            # amout * total days in a pay
            basis_allowance_amount = (
                settings["amount_details"]["amount"]
                * attendance_ratios["TOTALS"]["total_days"]
            )

        awarded_allowance_amount, missed_allowance_amount = basis_allowance_amount, 0

        if "PRORATED" == settings["amount_details"]["type"]:
            work_ratio = self._get_work_ratio(settings, attendance_ratios)
            prorate_type_ratio = self._get_allowance_proration_type_ratio(
                settings, attendance_ratios
            )
            if prorate_type_ratio:
                awarded_allowance_amount = (
                    basis_allowance_amount / prorate_type_ratio
                ) * work_ratio
            missed_allowance_amount = basis_allowance_amount - awarded_allowance_amount

        return (
            basis_allowance_amount,
            awarded_allowance_amount,
            missed_allowance_amount,
        )

    def _filter_by_frequency_schedule_allowances(self, settings) -> bool:
        if "RECURRING" == settings["frequency_type"]:
            frequency_schedule = settings["frequency_schedule"]
            max_pay_sched = self.payroll_group.max_pay_schedule
            cur_pay_sched = self.payroll_group.pay_schedule
            return (
                ("FIRST_PAY_OF_THE_MONTH" == frequency_schedule and cur_pay_sched == 1)
                or (
                    "LAST_PAY_OF_THE_MONTH" == frequency_schedule
                    and cur_pay_sched == max_pay_sched
                )
                or (frequency_schedule in ["DAILY", "PER_PAY", "PER_MONTH"])
            )
        # ONE_TIME
        return True

    def generate_deductions(self) -> List[Deduction]:
        filtered = filter(
            lambda settings: self._filter_by_frequency_schedule_deductions(settings),
            filter(
                lambda settings: self._filter_validity(settings),
                self.deduction_settings,
            ),
        )
        mapped = map(
            lambda settings: Deduction(
                type=settings["name"],
                amount=settings["amount_details"]["amount"],
                meta=settings["meta"],
            ),
            filtered,
        )
        return list(mapped)

    def _filter_by_frequency_schedule_deductions(self, settings) -> bool:
        if "RECURRING" == settings["frequency_type"]:
            frequency_schedule = settings["frequency_schedule"]
            max_pay_sched = self.payroll_group.max_pay_schedule
            cur_pay_sched = self.payroll_group.pay_schedule

            return (
                (
                    (
                        frequency_schedule
                        in ["FIRST_PAY_OF_THE_MONTH", "FIRST_AND_LAST_PAY_OF_THE_MONTH"]
                    )
                    and cur_pay_sched == 1
                )
                or (
                    (
                        frequency_schedule
                        in ["LAST_PAY_OF_THE_MONTH", "FIRST_AND_LAST_PAY_OF_THE_MONTH"]
                    )
                    and cur_pay_sched == max_pay_sched
                )
                or ("EVERY_PAY_OF_THE_MONTH" == frequency_schedule)
            )
        # ONE_TIME
        return True

    def generate_bonuses(self) -> List[Bonus]:
        pay_start_date = self.payroll_group.pay_start_date
        pay_end_date = self.payroll_group.pay_end_date
        date_hired = self.employee_details.date_hired
        date_terminated = self.employee_details.date_ended

        # from pprint import pprint

        # print(f"HIRED: {date_hired} {date_terminated}")
        # print(f"PAY: {pay_start_date} {pay_end_date}")
        # pprint(self.bonus_settings)

        # filter release_details and disburse_through_special_pay_run
        filtered = filter(
            lambda settings: self._filter_bonus(settings, pay_start_date, pay_end_date),
            self.bonus_settings,
        )

        mapped = map(
            lambda settings: self._create_bonus(
                settings, date_hired, date_terminated, pay_start_date, pay_end_date
            ),
            filtered,
        )

        the_list = []
        for x in mapped:
            # flat the list of allowance (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_bonus(
        self,
        settings: dict,
        date_hired: date,
        date_terminated: date,
        pay_start_date: date,
        pay_end_date: date,
    ) -> Union[Bonus, Tuple[Bonus, Bonus]]:
        # print(f"\n\n{settings}\n")
        # frequency_type [ONE_TIME | PERIODIC]
        #   amount_details-type FIXED
        #   amount_details-type SALARY_BASED
        #       amount_details-basis [CURRENT_BASIC_SALARY | AVERAGE_OF_BASIC_SALARY | GROSS_SALARY]
        frequency_type = settings["frequency_type"]
        amount_details = settings["amount_details"]

        release_details = self._get_release_details_within_date_range(
            settings["release_details"], pay_start_date, pay_end_date
        )

        # compute amount for this release
        amount_for_this_release = self._get_bonus_amount_for_this_release(
            frequency_type, amount_details, release_details, date_hired, date_terminated
        )

        # print(
        #     f"{frequency_type} - {amount_details['type']} - {amount_details.get('basis')} "
        #     + f"({settings['max_non_taxable']}) - {settings['name']}\n"
        #     + f"DATE: {release_details['date']}"
        #     + f" COVERAGE: {release_details.get('coverage_from')}"
        #     + f" to {release_details.get('coverage_to')}"
        #     + f" AMOUNT: {amount_for_this_release}"
        # )

        return self._process_bonus_release_for_taxability(
            settings,
            release_details,
            amount_for_this_release,
            date_hired,
            date_terminated,
        )

    def _process_bonus_release_for_taxability(
        self,
        settings,
        current_release_details,
        amount_for_this_release,
        date_hired,
        date_terminated,
    ) -> Union[Bonus, Tuple[Bonus, Bonus]]:
        frequency_type = settings["frequency_type"]
        amount_details = settings["amount_details"]

        if settings["max_non_taxable"] and settings["max_non_taxable"] > 0:
            release_index = settings["release_details"].index(current_release_details)

            past_release_amount = 0.0
            # get all past release amount/s
            for release in settings["release_details"][:release_index]:
                release_amount = 0.0
                release_amount = self._get_bonus_amount_for_this_release(
                    frequency_type, amount_details, release, date_hired, date_terminated
                )
                # print(
                #     f"\tDATE: {release['date']}"
                #     + f" COVERAGE: {release.get('coverage_from')} to {release.get('coverage_to')}"
                #     + f" AMOUNT:{release_amount}"
                # )
                past_release_amount += release_amount

            # figure out if we need to tax now
            should_be_taxed_amount = (
                amount_for_this_release + past_release_amount
            ) - settings["max_non_taxable"]

            if should_be_taxed_amount > 0:
                # print(should_be_taxed_amount)
                if should_be_taxed_amount >= amount_for_this_release:
                    return Bonus(
                        type=settings["name"],
                        amount=amount_for_this_release,
                        is_taxable=True,
                        meta=settings["meta"],
                    )
                else:
                    # split to two bonuses taxable and non-taxable
                    bonus_meta = settings["meta"]
                    bonus_meta["release_amount"] = amount_for_this_release
                    bonus_meta["past_release_amount"] = past_release_amount
                    bonus_meta["tax_exempt_amount"] = (
                        amount_for_this_release - should_be_taxed_amount
                    )
                    bonus_meta["taxed_amount"] = should_be_taxed_amount

                    return (
                        Bonus(
                            type=settings["name"],
                            amount=bonus_meta["tax_exempt_amount"],
                            is_taxable=False,
                            meta=bonus_meta,
                        ),
                        Bonus(
                            type=settings["name"],
                            amount=bonus_meta["taxed_amount"],
                            is_taxable=True,
                            meta=bonus_meta,
                        ),
                    )

            else:
                return Bonus(
                    type=settings["name"],
                    amount=amount_for_this_release,
                    is_taxable=False,
                    meta=settings["meta"],
                )

        else:
            return Bonus(
                type=settings["name"],
                amount=amount_for_this_release,
                is_taxable=settings["fully_taxable"],
                meta=settings["meta"],
            )

    def _get_bonus_amount_for_this_release(
        self,
        frequency_type: str,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        amount = 0.0
        if "SALARY_BASED" == amount_details["type"]:
            amount = self.__get_bonus_amount_for_this_release_salary_based(
                amount_details, release_details, date_hired, date_terminated
            )
        else:
            amount = (
                release_details["amount"]
                if "PERIODIC" == frequency_type
                else amount_details["amount"]
            )

        return amount

    def __get_bonus_amount_for_this_release_salary_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        if amount_details["basis"] in [
            "CURRENT_BASIC_SALARY",
            "AVERAGE_OF_BASIC_SALARY",
        ]:
            return self.__get_bonus_amount_for_this_release_salary_base_pay_based(
                amount_details, release_details, date_hired, date_terminated
            )
        else:  # 'GROSS_SALARY', 'BASIC_PAY'
            return self.__get_bonus_amount_for_this_release_salary_payroll_pay_based(
                amount_details, release_details, date_hired, date_terminated
            )

    def __get_bonus_amount_for_this_release_salary_payroll_pay_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        og_start_date = release_details["coverage_from"]
        og_end_date = release_details["coverage_to"]
        compute_base_start_date, compute_base_end_date = (
            date_hired if date_hired > og_start_date else og_start_date,
            date_terminated
            if date_terminated and date_terminated < og_end_date
            else og_end_date,
        )

        # print(f"OG_DATES: {og_start_date} {og_end_date}")
        # print(f"COMPUTE_BASE_DATES: {compute_base_start_date} {compute_base_end_date}")
        # print(f"SALARY BASIS: {amount_details['basis']}")
        # print(f"BASICPAY HISTORY: {self.basic_pay_history}")
        # print(f"GROSSBASIC HISTORY: {self.gross_basic_history}")

        bonus_cov_pay_hist = None
        if amount_details["basis"] == "BASIC_PAY":
            bonus_cov_pay_hist = self.basic_pay_history.clone(
                history_start_date=compute_base_start_date,
                history_end_date=compute_base_end_date,
            )
        else:  # 'GROSS_SALARY'
            bonus_cov_pay_hist = self.gross_basic_history.clone(
                history_start_date=compute_base_start_date,
                history_end_date=compute_base_end_date,
            )

        percentage = amount_details["percentage"] / 100

        return (sum(pay.amount for pay in bonus_cov_pay_hist) / 12) * percentage

    def __get_bonus_amount_for_this_release_salary_base_pay_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        # NOTE might not be needed handled by _compute_month_ratio_amount
        # if amount_details['basis'] == 'CURRENT_BASIC_SALARY':
        #     og_start_date = self.payroll_group.pay_start_date
        #     og_end_date = self.payroll_group.pay_end_date
        # else: # 'AVERAGE_OF_BASIC_SALARY'
        og_start_date = release_details["coverage_from"]
        og_end_date = release_details["coverage_to"]
        compute_base_start_date, compute_base_end_date = (
            date_hired if date_hired > og_start_date else og_start_date,
            date_terminated
            if date_terminated and date_terminated < og_end_date
            else og_end_date,
        )

        bonus_cov_bph = self.base_pay_history.clone(
            compute_base_start_date, compute_base_end_date
        )

        # print(f"OG_DATES: {og_start_date} {og_end_date}")
        # print(f"COMPUTE_BASE_DATES: {compute_base_start_date} {compute_base_end_date}")

        return (
            sum(
                self._compute_month_ratio_amount(
                    amount_details, month_start, month_end, bonus_cov_bph
                )
                for month_start, month_end in self._iter_by_month_intervals(
                    compute_base_start_date, compute_base_end_date
                )
            )
            / 12
        )

    def _compute_month_ratio_amount(
        self,
        amount_details: dict,
        month_start: date,
        month_end: date,
        coverage_bph: BasePayHistory,
    ) -> float:
        percentage = amount_details["percentage"] / 100
        # always get the last day of the month, since rrule has an unpredictable until
        base_interval_days = monthrange(month_end.year, month_end.month)[1]

        # print(f"  {month_start} to {month_end}")
        month_ratio_amount = 0.0
        if "AVERAGE_OF_BASIC_SALARY" == amount_details["basis"]:
            # NOTE: there is always a 'one_day' since we need
            #       to include the start_date (subtrahend)
            one_day = relativedelta(days=1)
            bph = coverage_bph.clone(month_start, month_end)

            bp_adj_cnt = len(bph.records)
            for i, base_pay in enumerate(bph.records):
                interval_ratio_amount = 0.0
                compute_start_date = max(base_pay.effective_date, month_start)
                compute_end_date = min(
                    month_end,
                    bph.records[i + 1].effective_date - one_day
                    if i < bp_adj_cnt - 1
                    else month_end,
                )
                interval_ratio_amount += self._compute_interval_ratio_amount(
                    compute_start_date,
                    compute_end_date,
                    base_interval_days,
                    base_pay,
                    percentage,
                )

                month_ratio_amount += interval_ratio_amount
        else:
            month_ratio_amount = self._compute_interval_ratio_amount(
                month_start,
                month_end,
                base_interval_days,
                coverage_bph.latest_base_pay,
                percentage,
            )

        return month_ratio_amount

    def _compute_interval_ratio_amount(
        self,
        start_date: date,
        end_date: date,
        base_interval_days: int,
        base_pay: BasePay,
        amount_percentage: float,
    ) -> float:
        # +1 instead of substracting relativedelta(days=1) to start
        actual_interval_days = (end_date - (start_date)).days + 1
        month_ratio = actual_interval_days / base_interval_days
        interval_ratio_amount = (
            month_ratio * base_pay.monthly_rate_amount * amount_percentage
        )
        # print(
        #     f"    {start_date.strftime('%m-%d')} <=> {end_date.strftime('%m-%d')} "
        #     + f"({actual_interval_days}/{base_interval_days}):"
        #     + f" {month_ratio:.2f} => {interval_ratio_amount:.2f}"
        # )

        return interval_ratio_amount

    def _iter_by_month_intervals(
        self, start_date: date, end_date: date
    ) -> Iterator[Tuple[date, date]]:
        start = datetime(start_date.year, start_date.month, 1)
        end = datetime(
            end_date.year, end_date.month, monthrange(end_date.year, end_date.month)[1]
        )

        # iterate monthly by first and last day of the month
        monthly_rrule = rrule(MONTHLY, dtstart=start, until=end, bymonthday=(1, -1))

        # https://stackoverflow.com/a/5389547
        for first, last in zip(*[iter(monthly_rrule)] * 2):
            yield max(first.date(), start_date), min(last.date(), end_date)

    def _get_release_details_within_date_range(
        self, release_details_list: list, start_date: date, end_date: date
    ) -> dict:
        return next(
            release
            for release in release_details_list
            if self._date_within_date_range(release["date"], start_date, end_date)
        )

    def _filter_bonus(self, settings: dict, start_date: date, end_date: date) -> bool:
        # one of the release_details date should fall within date range
        # disburse_through_special_pay_run should be false
        return self._filter_release_details_within_date_range(
            settings, start_date, end_date
        )

    def _filter_release_details_within_date_range(
        self, settings: dict, start_date: date, end_date: date
    ) -> bool:
        return any(
            self._date_within_date_range(details["date"], start_date, end_date)
            and not details["disburse_through_special_pay_run"]
            for details in settings["release_details"]
        )

    def generate_adjustments(self) -> AdjustmentRecord:
        pay_start_date = self.payroll_group.pay_start_date
        pay_end_date = self.payroll_group.pay_end_date

        filtered = filter(
            lambda settings: self._date_within_date_range(
                settings["release_details"][0]["date"], pay_start_date, pay_end_date
            )
            and not settings["release_details"][0]["disburse_through_special_pay_run"],
            self.adjustment_settings,
        )

        mapped = map(
            lambda settings: Adjustment(
                type=AdjustmentType(settings["category"]),
                amount=settings["amount_details"]["amount"],
                reason=settings["name"],
                meta=settings["meta"],
            ),
            filtered,
        )

        return AdjustmentRecord(list(mapped))

    def generate_commissions(self) -> List[Commission]:
        pay_start_date = self.payroll_group.pay_start_date
        pay_end_date = self.payroll_group.pay_end_date

        filtered = filter(
            lambda settings: self._date_within_date_range(
                settings["release_details"][0]["date"], pay_start_date, pay_end_date
            )
            and not settings["release_details"][0]["disburse_through_special_pay_run"],
            self.commission_settings,
        )

        mapped = map(lambda settings: self._create_commission(settings), filtered)

        the_list = []
        for x in mapped:
            # flat the list of commision (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_commission(
        self, settings
    ) -> Union[Commission, Tuple[Commission, Commission]]:
        if (
            settings["max_non_taxable"]
            and settings["max_non_taxable"] > 0
            and settings["amount_details"]["amount"] > settings["max_non_taxable"]
        ):
            # split to two commission taxable and non-taxable
            commission_meta = settings["meta"]
            commission_meta["basis_amount"] = settings["amount_details"]["amount"]
            commission_meta["max_non_taxable"] = settings["max_non_taxable"]
            commission_meta["taxable_split"] = (
                commission_meta["basis_amount"] - settings["max_non_taxable"]
            )

            return (
                Commission(
                    type=settings["name"],
                    amount=settings["max_non_taxable"],
                    is_taxable=False,
                    meta=commission_meta,
                ),
                Commission(
                    type=settings["name"],
                    amount=commission_meta["taxable_split"],
                    is_taxable=True,
                    meta=commission_meta,
                ),
            )

        return Commission(
            type=settings["name"],
            amount=settings["amount_details"]["amount"],
            is_taxable=settings["fully_taxable"],
            meta=settings["meta"],
        )

    def _filter_validity(self, settings) -> bool:
        pay_start_date = self.payroll_group.pay_start_date
        pay_end_date = self.payroll_group.pay_end_date

        valid_from = settings["validity"]["valid_from"]
        valid_to = settings["validity"]["valid_to"]

        # make sure it is valid (validity) and check for frequency and schedule
        if "RECURRING" == settings["frequency_type"]:
            # bounded validity
            if valid_to is not None:
                return (
                    # valid waaaay before
                    valid_from <= pay_start_date <= valid_to
                    # just started
                    or valid_from <= pay_end_date <= valid_to
                    or pay_start_date <= valid_from <= pay_end_date
                    or pay_start_date <= valid_to <= pay_end_date
                )
            # indefinite validity
            else:
                return (
                    # valid waaaay before
                    valid_from <= pay_start_date
                    # just started
                    or pay_start_date <= valid_from <= pay_end_date
                )
        # ONE_TIME
        else:
            return pay_start_date <= valid_from <= pay_end_date

    def _compute_attendance_ratio(self) -> Dict:
        total_scheduled_hours = 0
        total_regular_hours = 0
        total_paid_leave_hours = 0
        total_unpaid_leave_hours = 0
        total_nightshift_hours = 0

        total_days = 0
        total_regular_day_hours = 0
        total_on_paid_leave_day_hours = 0
        total_on_unpaid_leave_day_hours = 0
        total_on_paid_and_unpaid_leave_day_hours = 0
        total_regular_days = 0
        total_on_paid_leave_days = 0
        total_on_unpaid_leave_days = 0
        total_on_paid_and_unpaid_leave_days = 0

        work_days = filter(lambda day: day.is_work_day, self.attendance.original_day_records)
        for day in work_days:
            day_hours = 0

            if day.is_holiday:
                # Add REGULAR
                day_hours += day.regular_hours
                # if holiday, get sum of hours of normal holiday codes (not not)
                for holiday in day.iter_holiday_work_records():
                    if holiday.hour_type in _AllowanceHolidayRegularHourTypes:
                        day_hours += holiday.hours
                # Add UH
                day_hours += day.unworked_holiday_hours

            else:
                day_hours = day.regular_hours

            night_differential_hours = 0
            for night_diff in day.iter_night_differential_records():
                night_differential_hours += night_diff.hours

            total_scheduled_hours += day.scheduled_hours
            total_regular_hours += day_hours
            total_paid_leave_hours += day.paid_leave_hours
            total_unpaid_leave_hours += day.unpaid_leave_hours
            total_nightshift_hours += night_differential_hours

            total_days += 1

            day_regular_day_hours = day_hours + night_differential_hours
            day_on_paid_leave_day_hours = (
                day_hours + night_differential_hours + day.paid_leave_hours
            )
            day_on_unpaid_leave_day_hours = (
                day_hours + night_differential_hours + day.unpaid_leave_hours
            )
            day_on_paid_and_unpaid_leave_day_hours = (
                day_hours
                + night_differential_hours
                + day.paid_leave_hours
                + day.unpaid_leave_hours
            )
            # for proration by hours
            total_regular_day_hours += day_regular_day_hours
            total_on_paid_leave_day_hours += day_on_paid_leave_day_hours
            total_on_unpaid_leave_day_hours += day_on_unpaid_leave_day_hours
            total_on_paid_and_unpaid_leave_day_hours += (
                day_on_paid_and_unpaid_leave_day_hours
            )

            # For proration by day
            total_regular_days += 1 if day_regular_day_hours > 0 else 0
            total_on_paid_leave_days += 1 if day_on_paid_leave_day_hours > 0 else 0
            total_on_unpaid_leave_days += 1 if day_on_unpaid_leave_day_hours > 0 else 0
            total_on_paid_and_unpaid_leave_days += (
                1 if day_on_paid_and_unpaid_leave_day_hours > 0 else 0
            )

        return {
            "PRORATED_BY_DAY": {
                "NO_ENTITLED_WHEN": total_regular_days,
                "ON_PAID_LEAVE": total_on_paid_leave_days,
                "ON_UNPAID_LEAVE": total_on_unpaid_leave_days,
                "ON_PAID_AND_UNPAID_LEAVE": total_on_paid_and_unpaid_leave_days,
            },
            "PRORATED_BY_HOUR": {
                "NO_ENTITLED_WHEN": total_regular_day_hours,
                "ON_PAID_LEAVE": total_on_paid_leave_day_hours,
                "ON_UNPAID_LEAVE": total_on_unpaid_leave_day_hours,
                "ON_PAID_AND_UNPAID_LEAVE": total_on_paid_and_unpaid_leave_day_hours,
            },
            "TOTALS": {
                "total_scheduled_hours": total_scheduled_hours,
                "total_regular_hours": total_regular_hours,
                "total_nightshift_hours": total_nightshift_hours,
                "total_paid_leave_hours": total_paid_leave_hours,
                "total_unpaid_leave_hours": total_unpaid_leave_hours,
                "total_days": total_days,
            },
        }

    def _get_work_ratio(self, settings, attendance_ratios) -> float:
        if "PRORATED" == settings["amount_details"]["type"]:
            prorated_by = settings["amount_details"]["prorated_by"]
            entitled_when = settings["amount_details"]["entitled_when"]
            entitled_when = entitled_when if entitled_when else "NO_ENTITLED_WHEN"

            return attendance_ratios[prorated_by][entitled_when]

        return 1

    def _date_within_date_range(
        self, date_to_check: date, date_range_min: date, date_range_max: date
    ) -> bool:
        return date_range_min <= date_to_check <= date_range_max

    def _get_allowance_proration_type_ratio(self, settings, attendance_ratios) -> float:
        """ Returns base allowance divisor based on
            proration type
        """
        if (
            "PRORATED" == settings["amount_details"]["type"]
            and settings["amount_details"]["prorated_by"] == "PRORATED_BY_HOUR"
        ):
            hrs_per_day = (
                float(self.employee_details.hrs_per_day)
                if self.employee_details.hrs_per_day
                else float(self.payroll_group.hrs_per_day)
            )

            return attendance_ratios["TOTALS"]["total_days"] * hrs_per_day

        return attendance_ratios["TOTALS"]["total_days"]


@dataclass
class SpecialAABCDGenerator:
    employee_details: EmployeeDetails

    base_pay_history: BasePayHistory = BasePayHistory(records=[])
    basic_pay_history: PayHistory = PayHistory(records=[])
    gross_basic_history: PayHistory = PayHistory(records=[])
    gross_income_history: PayHistory = PayHistory(records=[])

    adjustment_settings: List = field(default_factory=list)
    allowance_settings: List = field(default_factory=list)
    bonus_settings: List = field(default_factory=list)
    commission_settings: List = field(default_factory=list)
    deduction_settings: List = field(default_factory=list)
    selected_bonus_release_ids: List = field(default_factory=list)
    deminimies_allowances = ('reduce_to_taxable_income',)

    def filter_deminimies_from_allowances(self):
        filtered = filter(
            lambda settings: settings['tax_option'] in self.deminimies_allowances,
            self.allowance_settings
        )
        return filtered

    def generate_allowances(self):
        filtered = filter(
            lambda settings: settings['tax_option'] not in self.deminimies_allowances,
            self.allowance_settings
        )
        mapped = map(
            lambda settings: self._create_allowance(settings), filtered
        )

        the_list = []
        for x in mapped:
            # flat the list of allowance (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_allowance(self, settings):
        allowance_meta = settings["meta"]
        (
            allowance_meta["basis_amount"],
            allowance_meta["given_amount"],
            allowance_meta["deducted_amount"],
        ) = self._get_allowance_amount(settings)

        if (
            settings["max_non_taxable"]
            and settings["max_non_taxable"] > 0
            and allowance_meta["given_amount"] > settings["max_non_taxable"]
        ):
            # split to two allowance taxable and non-taxable
            allowance_meta["max_non_taxable"] = settings["max_non_taxable"]
            allowance_meta["taxable_split"] = (
                allowance_meta["given_amount"] - settings["max_non_taxable"]
            )

            return (
                Allowance(
                    type=settings["name"],
                    amount=settings["max_non_taxable"],
                    is_taxable=False,
                    meta=allowance_meta,
                ),
                Allowance(
                    type=settings["name"],
                    amount=allowance_meta["taxable_split"],
                    is_taxable=True,
                    meta=allowance_meta,
                ),
            )

        return Allowance(
            type=settings["name"],
            amount=allowance_meta["given_amount"],
            is_taxable=settings.get("tax_option") == "fully_taxable",
            meta=allowance_meta,
        )

    def _get_allowance_amount(self, settings):
        basis_allowance_amount = settings["amount_details"]["amount"]
        awarded_allowance_amount, missed_allowance_amount = basis_allowance_amount, 0

        return (
            basis_allowance_amount,
            awarded_allowance_amount,
            missed_allowance_amount,
        )

    def generate_deductions(self):
        mapped = map(
            lambda settings: Deduction(
                type=settings["name"],
                amount=settings["amount_details"]["amount"],
                meta=settings["meta"],
            ),
            self.deduction_settings,
        )
        return list(mapped)

    def generate_bonuses(self):
        date_hired = self.employee_details.date_hired
        date_terminated = self.employee_details.date_ended
        print('generate_bonuses selffffffffffffffffffffffff')
        # print(self)
        print(self.employee_details.basic_info.get("annualEarnings", {}))
        annualEarnings = self.employee_details.basic_info.get("annualEarnings", {})
        #print(annualEarnings.)
        # pprint(self.bonus_settings)

        # filter release_details and disburse_through_special_pay_run
        mapped = map(
            lambda settings: self._create_bonus(settings, date_hired, date_terminated),
            self.bonus_settings,
        )

        the_list = []
        for x in mapped:
            # flat the list of allowance (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_bonus(self, settings: dict, date_hired: date, date_terminated: date):
        total_bonus_releases_amount = 0
        frequency_type = settings["frequency_type"]
        amount_details = settings["amount_details"]
        release_details = settings["release_details"]

        # self.selected_bonus_release_ids
        sorted_release_details = sorted(release_details, key=lambda i: i["date"])

        # get all release amount/s
        paired_releases = []
        for index, release in enumerate(sorted_release_details):
            if release.get("id", None) in self.selected_bonus_release_ids:
                past_releases = [release for release in sorted_release_details[:index]]
                paired_releases.append(
                    (
                        release,
                        past_releases,
                        self._get_bonus_amount_for_this_release(
                            frequency_type,
                            amount_details,
                            release,
                            date_hired,
                            date_terminated,
                        ),
                        sum(
                            self._get_bonus_amount_for_this_release(
                                frequency_type,
                                amount_details,
                                past_release,
                                date_hired,
                                date_terminated,
                            )
                            for past_release in past_releases
                        ),
                    )
                )

        (
            non_taxable_release_amount,
            taxable_release_amount,
            past_releases_total_amount,
        ) = self._compute_taxability(paired_releases, settings)

        meta = settings["meta"]
        meta["release_amount"] = non_taxable_release_amount + taxable_release_amount
        meta["tax_exempt_amount"] = non_taxable_release_amount
        meta["taxed_amount"] = taxable_release_amount

        bonuses = []
        if non_taxable_release_amount > 0:
            bonuses.append(
                Bonus(
                    type=settings["name"],
                    amount=non_taxable_release_amount,
                    is_taxable=False,
                    meta=meta,
                )
            )

        if taxable_release_amount > 0:
            bonuses.append(
                Bonus(
                    type=settings["name"],
                    amount=taxable_release_amount,
                    is_taxable=True,
                    meta=meta,
                )
            )

        return tuple(bonuses)

    def _compute_taxability(self, paired_releases: list, settings: dict):
        non_taxable_release_amount = 0
        taxable_release_amount = 0
        past_total_amount = 0
        print('_compute_taxability settingsssssssssssssssss')
        print(settings)
        print('_compute_taxability nonTaxableIncomeeeeeeeee')
        print(self.employee_details.basic_info.get("annualEarnings", {}).get("nonTaxableIncome", 0))
        for (
            release,
            past_releases,
            release_amount,
            past_releases_total_amount,
        ) in paired_releases:
            past_total_amount = past_releases_total_amount
            print('_compute_taxability settings["max_non_taxable"]')
            print(settings["max_non_taxable"])
            if settings["max_non_taxable"] == None:
                settings["max_non_taxable"] = 0

            annual_earnings_max_non_taxable = self.employee_details.basic_info.get("annualEarnings", {}).get("nonTaxableIncome", 0)
            if (settings["max_non_taxable"] and settings["max_non_taxable"] > 0) or annual_earnings_max_non_taxable > 0:
                should_be_taxed_amount = (
                    past_releases_total_amount
                    + self.employee_details.basic_info.get("annualEarnings", {}).get("nonTaxableIncome", 0)
                    + release_amount
                    - settings["max_non_taxable"]
                )
                print('_compute_taxability should_be_taxed_amounttttttttttttttttttttttttt')
                print(should_be_taxed_amount)
                if should_be_taxed_amount > 0:
                    if should_be_taxed_amount >= release_amount:
                        taxable_release_amount += release_amount
                    else:
                        # split to two bonuses taxable and non-taxable
                        remaining_non_taxable = (
                            settings["max_non_taxable"] - past_releases_total_amount
                        )

                        taxable_release_amount += release_amount - remaining_non_taxable
                        non_taxable_release_amount += remaining_non_taxable
                else:
                    # way below the max_non_taxable
                    non_taxable_release_amount += release_amount
            else:
                if settings["fully_taxable"]:
                    taxable_release_amount += release_amount
                else:
                    non_taxable_release_amount += release_amount

        return non_taxable_release_amount, taxable_release_amount, past_total_amount

    def _get_bonus_amount_for_this_release(
        self,
        frequency_type: str,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ):
        amount = 0.0

        if "SALARY_BASED" == amount_details["type"]:
            amount = self.__get_bonus_amount_for_this_release_salary_based(
                amount_details, release_details, date_hired, date_terminated
            )
        else:
            amount = (
                release_details["amount"]
                if "PERIODIC" == frequency_type
                else amount_details["amount"]
            )

        return amount

    def __get_bonus_amount_for_this_release_salary_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        if amount_details["basis"] in [
            "CURRENT_BASIC_SALARY",
            "AVERAGE_OF_BASIC_SALARY",
        ]:
            return self.__get_bonus_amount_for_this_release_salary_base_pay_based(
                amount_details, release_details, date_hired, date_terminated
            )
        else:  # 'GROSS_SALARY', 'BASIC_PAY'
            return self.__get_bonus_amount_for_this_release_salary_payroll_pay_based(
                amount_details, release_details, date_hired, date_terminated
            )

    def __get_bonus_amount_for_this_release_salary_payroll_pay_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        og_start_date = release_details["coverage_from"]
        og_end_date = release_details["coverage_to"]
        compute_base_start_date, compute_base_end_date = (
            date_hired if date_hired > og_start_date else og_start_date,
            date_terminated
            if date_terminated and date_terminated < og_end_date
            else og_end_date,
        )

        # print(f"OG_DATES: {og_start_date} {og_end_date}")
        # print(f"COMPUTE_BASE_DATES: {compute_base_start_date} {compute_base_end_date}")
        # print(f"SALARY BASIS: {amount_details['basis']}")
        # print(f"BASICPAY HISTORY: {self.basic_pay_history}")
        # print(f"GROSSBASIC HISTORY: {self.gross_basic_history}")

        bonus_cov_pay_hist = None
        if amount_details["basis"] == "BASIC_PAY":
            bonus_cov_pay_hist = self.basic_pay_history.clone(
                history_start_date=compute_base_start_date,
                history_end_date=compute_base_end_date,
                date_hired=self.basic_pay_history.oldest_pay.effective_start_date,
                date_ended=self.basic_pay_history.latest_pay.effective_end_date,
            )
        else:  # 'GROSS_SALARY'
            bonus_cov_pay_hist = self.gross_basic_history.clone(
                history_start_date=compute_base_start_date,
                history_end_date=compute_base_end_date,
                date_hired=self.gross_basic_history.oldest_pay.effective_start_date,
                date_ended=self.gross_basic_history.latest_pay.effective_end_date,
            )

        percentage = amount_details["percentage"] / 100

        return (sum(pay.amount for pay in bonus_cov_pay_hist) / 12) * percentage

    def __get_bonus_amount_for_this_release_salary_base_pay_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        # NOTE CURRENT_BASIC_SALARY and AVERAGE_OF_BASIC_SALARY is handled in _compute_month_ratio_amount
        og_start_date = release_details["coverage_from"]
        og_end_date = release_details["coverage_to"]
        compute_base_start_date, compute_base_end_date = (
            date_hired if date_hired > og_start_date else og_start_date,
            date_terminated
            if date_terminated and date_terminated < og_end_date
            else og_end_date,
        )

        bonus_cov_bph = self.base_pay_history.clone(
            compute_base_start_date, compute_base_end_date
        )

        # print(f"OG_DATES: {og_start_date} {og_end_date}")
        # print(f"COMPUTE_BASE_DATES: {compute_base_start_date} {compute_base_end_date}")

        return (
            sum(
                self._compute_month_ratio_amount(
                    amount_details, month_start, month_end, bonus_cov_bph
                )
                for month_start, month_end in self._iter_by_month_intervals(
                    compute_base_start_date, compute_base_end_date
                )
            )
            / 12
        )

    def _compute_month_ratio_amount(
        self,
        amount_details: dict,
        month_start: date,
        month_end: date,
        coverage_bph: BasePayHistory,
    ) -> float:
        percentage = amount_details["percentage"] / 100
        # always get the last day of the month, since rrule has an unpredictable until
        base_interval_days = monthrange(month_end.year, month_end.month)[1]

        # print(f"  {month_start} to {month_end}")
        month_ratio_amount = 0.0
        if "AVERAGE_OF_BASIC_SALARY" == amount_details["basis"]:
            # NOTE: there is always a 'one_day' since we need
            #       to include the start_date (subtrahend)
            one_day = relativedelta(days=1)
            bph = coverage_bph.clone(month_start, month_end)

            bp_adj_cnt = len(bph.records)
            for i, base_pay in enumerate(bph.records):
                interval_ratio_amount = 0.0
                compute_start_date = max(base_pay.effective_date, month_start)
                compute_end_date = min(
                    month_end,
                    bph.records[i + 1].effective_date - one_day
                    if i < bp_adj_cnt - 1
                    else month_end,
                )
                interval_ratio_amount += self._compute_interval_ratio_amount(
                    compute_start_date,
                    compute_end_date,
                    base_interval_days,
                    base_pay,
                    percentage,
                )

                month_ratio_amount += interval_ratio_amount
        else:
            month_ratio_amount = self._compute_interval_ratio_amount(
                month_start,
                month_end,
                base_interval_days,
                coverage_bph.latest_base_pay,
                percentage,
            )

        return month_ratio_amount

    def _compute_interval_ratio_amount(
        self,
        start_date: date,
        end_date: date,
        base_interval_days: int,
        base_pay: BasePay,
        amount_percentage: float,
    ) -> float:
        # +1 instead of substracting relativedelta(days=1) to start
        actual_interval_days = (end_date - (start_date)).days + 1
        month_ratio = actual_interval_days / base_interval_days
        interval_ratio_amount = (
            month_ratio * base_pay.monthly_rate_amount * amount_percentage
        )
        # print(
        #     f"    {start_date.strftime('%m-%d')} <=> {end_date.strftime('%m-%d')} "
        #     + f"({actual_interval_days}/{base_interval_days}):"
        #     + f" {month_ratio:.2f} => {interval_ratio_amount:.2f}"
        # )

        return interval_ratio_amount

    def _iter_by_month_intervals(
        self, start_date: date, end_date: date
    ) -> Iterator[Tuple[date, date]]:
        start = datetime(start_date.year, start_date.month, 1)
        end = datetime(
            end_date.year, end_date.month, monthrange(end_date.year, end_date.month)[1]
        )

        # iterate monthly by first and last day of the month
        monthly_rrule = rrule(MONTHLY, dtstart=start, until=end, bymonthday=(1, -1))

        # https://stackoverflow.com/a/5389547
        for first, last in zip(*[iter(monthly_rrule)] * 2):
            yield max(first.date(), start_date), min(last.date(), end_date)

    def generate_adjustments(self):
        mapped = map(
            lambda settings: Adjustment(
                type=AdjustmentType(settings["category"]),
                amount=settings["amount_details"]["amount"],
                reason=settings["name"],
                meta=settings["meta"],
            ),
            self.adjustment_settings,
        )

        return AdjustmentRecord(list(mapped))

    def generate_commissions(self):
        mapped = map(
            lambda settings: self._create_commission(settings), self.commission_settings
        )

        the_list = []
        for x in mapped:
            # flat the list of commision (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_commission(self, settings):
        if (
            settings["max_non_taxable"]
            and settings["max_non_taxable"] > 0
            and settings["amount_details"]["amount"] > settings["max_non_taxable"]
        ):
            # split to two commission taxable and non-taxable
            commission_meta = settings["meta"]
            commission_meta["basis_amount"] = settings["amount_details"]["amount"]
            commission_meta["max_non_taxable"] = settings["max_non_taxable"]
            commission_meta["taxable_split"] = (
                commission_meta["basis_amount"] - settings["max_non_taxable"]
            )

            return (
                Commission(
                    type=settings["name"],
                    amount=settings["max_non_taxable"],
                    is_taxable=False,
                    meta=commission_meta,
                ),
                Commission(
                    type=settings["name"],
                    amount=commission_meta["taxable_split"],
                    is_taxable=True,
                    meta=commission_meta,
                ),
            )

        return Commission(
            type=settings["name"],
            amount=settings["amount_details"]["amount"],
            is_taxable=settings["fully_taxable"],
            meta=settings["meta"],
        )


@dataclass
class FinalAABCDGenerator:
    payroll_group: PayrollGroup
    attendance: Attendance
    employee_details: EmployeeDetails

    base_pay_history: BasePayHistory = BasePayHistory(records=[])
    basic_pay_history: PayHistory = PayHistory(records=[])
    gross_basic_history: PayHistory = PayHistory(records=[])
    gross_income_history: PayHistory = PayHistory(records=[])

    adjustment_settings: List = field(default_factory=list)
    allowance_settings: List = field(default_factory=list)
    bonus_settings: List = field(default_factory=list)
    commission_settings: List = field(default_factory=list)
    deduction_settings: List = field(default_factory=list)
    selected_bonus_release_ids: List = field(default_factory=list)
    deminimies_allowances = ('reduce_to_taxable_income',)

    def filter_deminimies_from_allowances(self):
        filtered = filter(
            lambda settings: settings['tax_option'] in self.deminimies_allowances,
            self.allowance_settings
        )
        return filtered


    def generate_allowances(self) -> List[Allowance]:
        # filtered = filter(
        #     lambda settings: self._filter_by_frequency_schedule_allowances(settings),
        #     filter(
        #         lambda settings: self._filter_validity(settings)
        #         and not settings["release_details"][0][
        #             "disburse_through_special_pay_run"
        #         ],
        #         self.allowance_settings,
        #     ),
        # )
        # remove deminimies from allowances
        filtered = filter(
            lambda settings: settings['tax_option'] not in self.deminimies_allowances,
            self.allowance_settings
        )

        att_ratios = self._compute_attendance_ratio()
        mapped = map(
            lambda settings: self._create_allowance(settings, att_ratios),
            filtered,
        )

        the_list = []
        for x in mapped:
            # flat the list of allowance (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_allowance(
        self, settings, attendance_ratios
    ) -> Union[Allowance, Tuple[Allowance, Allowance]]:
        allowance_meta = settings["meta"]
        (
            allowance_meta["basis_amount"],
            allowance_meta["given_amount"],
            allowance_meta["deducted_amount"],
        ) = self._get_allowance_amount(settings, attendance_ratios)

        if (
            settings["max_non_taxable"]
            and settings["max_non_taxable"] > 0
            and allowance_meta["given_amount"] > settings["max_non_taxable"]
        ):
            # split to two allowance taxable and non-taxable
            allowance_meta["max_non_taxable"] = settings["max_non_taxable"]
            allowance_meta["taxable_split"] = (
                allowance_meta["given_amount"] - settings["max_non_taxable"]
            )

            return (
                Allowance(
                    type=settings["name"],
                    amount=settings["max_non_taxable"],
                    is_taxable=False,
                    meta=allowance_meta,
                ),
                Allowance(
                    type=settings["name"],
                    amount=allowance_meta["taxable_split"],
                    is_taxable=True,
                    meta=allowance_meta,
                ),
            )

        return Allowance(
            type=settings["name"],
            amount=allowance_meta["given_amount"],
            is_taxable=settings.get("tax_option") == "fully_taxable",
            meta=allowance_meta,
        )

    def _get_allowance_amount(
        self, settings, attendance_ratios
    ) -> Tuple[float, float, float]:
        # "PER_PAY", "FIRST_PAY_OF_THE_MONTH", "LAST_PAY_OF_THE_MONTH", "DAILY", "PER_MONTH"
        frequency_schedule = settings["frequency_schedule"]

        basis_allowance_amount = settings["amount_details"]["amount"]
        if "PER_MONTH" == frequency_schedule:
            basis_allowance_amount = divide_unequal(
                settings["amount_details"]["amount"],
                self.payroll_group.max_pay_schedule,
            )[self.payroll_group.pay_schedule - 1]
        elif "DAILY" == frequency_schedule:
            # amout * total days in a pay
            basis_allowance_amount = (
                settings["amount_details"]["amount"]
                * attendance_ratios["TOTALS"]["total_days"]
            )

        awarded_allowance_amount, missed_allowance_amount = basis_allowance_amount, 0

        if "PRORATED" == settings["amount_details"]["type"]:
            work_ratio = self._get_work_ratio(settings, attendance_ratios)
            prorate_type_ratio = self._get_allowance_proration_type_ratio(
                settings, attendance_ratios
            )
            awarded_allowance_amount = (
                basis_allowance_amount / prorate_type_ratio
            ) * work_ratio
            missed_allowance_amount = basis_allowance_amount - awarded_allowance_amount

        return (
            basis_allowance_amount,
            awarded_allowance_amount,
            missed_allowance_amount,
        )

    # def _filter_by_frequency_schedule_allowances(self, settings) -> bool:
    #     if "RECURRING" == settings["frequency_type"]:
    #         frequency_schedule = settings["frequency_schedule"]
    #         max_pay_sched = self.payroll_group.max_pay_schedule
    #         cur_pay_sched = self.payroll_group.pay_schedule
    #         return (
    #             ("FIRST_PAY_OF_THE_MONTH" == frequency_schedule and cur_pay_sched == 1)
    #             or (
    #                 "LAST_PAY_OF_THE_MONTH" == frequency_schedule
    #                 and cur_pay_sched == max_pay_sched
    #             )
    #             or (frequency_schedule in ["DAILY", "PER_PAY", "PER_MONTH"])
    #         )
    #     # ONE_TIME
    #     return True

    def generate_deductions(self) -> List[Deduction]:
        # filtered = filter(
        #     lambda settings: self._filter_by_frequency_schedule_deductions(settings),
        #     filter(
        #         lambda settings: self._filter_validity(settings),
        #         self.deduction_settings,
        #     ),
        # )
        mapped = map(
            lambda settings: Deduction(
                type=settings["name"],
                amount=settings["amount_details"]["amount"],
                meta=settings["meta"],
            ),
            self.deduction_settings,
        )
        return list(mapped)

    def _filter_by_frequency_schedule_deductions(self, settings) -> bool:
        if "RECURRING" == settings["frequency_type"]:
            frequency_schedule = settings["frequency_schedule"]
            max_pay_sched = self.payroll_group.max_pay_schedule
            cur_pay_sched = self.payroll_group.pay_schedule

            return (
                (
                    (
                        frequency_schedule
                        in ["FIRST_PAY_OF_THE_MONTH", "FIRST_AND_LAST_PAY_OF_THE_MONTH"]
                    )
                    and cur_pay_sched == 1
                )
                or (
                    (
                        frequency_schedule
                        in ["LAST_PAY_OF_THE_MONTH", "FIRST_AND_LAST_PAY_OF_THE_MONTH"]
                    )
                    and cur_pay_sched == max_pay_sched
                )
                or ("EVERY_PAY_OF_THE_MONTH" == frequency_schedule)
            )
        # ONE_TIME
        return True

    def generate_bonuses(self):
        date_hired = self.employee_details.date_hired
        date_terminated = self.employee_details.date_ended

        # pprint(self.bonus_settings)

        # filter release_details and disburse_through_special_pay_run
        mapped = map(
            lambda settings: self._create_bonus(settings, date_hired, date_terminated),
            self.bonus_settings,
        )

        the_list = []
        for x in mapped:
            # flat the list of allowance (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_bonus(self, settings: dict, date_hired: date, date_terminated: date):
        total_bonus_releases_amount = 0
        frequency_type = settings["frequency_type"]
        amount_details = settings["amount_details"]
        release_details = settings["release_details"]

        # self.selected_bonus_release_ids
        sorted_release_details = sorted(release_details, key=lambda i: i["date"])

        # get all release amount/s
        paired_releases = []
        for index, release in enumerate(sorted_release_details):
            if release.get("id", None) in self.selected_bonus_release_ids:
                past_releases = [release for release in sorted_release_details[:index]]
                paired_releases.append(
                    (
                        release,
                        past_releases,
                        self._get_bonus_amount_for_this_release(
                            frequency_type,
                            amount_details,
                            release,
                            date_hired,
                            date_terminated,
                        ),
                        sum(
                            self._get_bonus_amount_for_this_release(
                                frequency_type,
                                amount_details,
                                past_release,
                                date_hired,
                                date_terminated,
                            )
                            for past_release in past_releases
                        ),
                    )
                )

        (
            non_taxable_release_amount,
            taxable_release_amount,
            past_releases_total_amount,
        ) = self._compute_taxability(paired_releases, settings)

        meta = settings["meta"]
        meta["release_amount"] = non_taxable_release_amount + taxable_release_amount
        meta["tax_exempt_amount"] = non_taxable_release_amount
        meta["taxed_amount"] = taxable_release_amount

        bonuses = []
        if non_taxable_release_amount > 0:
            bonuses.append(
                Bonus(
                    type=settings["name"],
                    amount=non_taxable_release_amount,
                    is_taxable=False,
                    meta=meta,
                )
            )

        if taxable_release_amount > 0:
            bonuses.append(
                Bonus(
                    type=settings["name"],
                    amount=taxable_release_amount,
                    is_taxable=True,
                    meta=meta,
                )
            )

        return tuple(bonuses)

    def _compute_taxability(self, paired_releases: list, settings: dict):
        non_taxable_release_amount = 0
        taxable_release_amount = 0
        past_total_amount = 0
        for (
            release,
            past_releases,
            release_amount,
            past_releases_total_amount,
        ) in paired_releases:
            past_total_amount = past_releases_total_amount

            if settings["max_non_taxable"] and settings["max_non_taxable"] > 0:
                should_be_taxed_amount = (
                    past_releases_total_amount
                    + release_amount
                    - settings["max_non_taxable"]
                )

                if should_be_taxed_amount > 0:
                    if should_be_taxed_amount >= release_amount:
                        taxable_release_amount += release_amount
                    else:
                        # split to two bonuses taxable and non-taxable
                        remaining_non_taxable = (
                            settings["max_non_taxable"] - past_releases_total_amount
                        )

                        taxable_release_amount += release_amount - remaining_non_taxable
                        non_taxable_release_amount += remaining_non_taxable
                else:
                    # way below the max_non_taxable
                    non_taxable_release_amount += release_amount
            else:
                if settings["fully_taxable"]:
                    taxable_release_amount += release_amount
                else:
                    non_taxable_release_amount += release_amount

        return non_taxable_release_amount, taxable_release_amount, past_total_amount

    def _process_bonus_release_for_taxability(
        self,
        settings,
        current_release_details,
        amount_for_this_release,
        date_hired,
        date_terminated,
    ) -> Union[Bonus, Tuple[Bonus, Bonus]]:
        frequency_type = settings["frequency_type"]
        amount_details = settings["amount_details"]

        if settings["max_non_taxable"] and settings["max_non_taxable"] > 0:
            release_index = settings["release_details"].index(current_release_details)

            past_release_amount = 0.0
            # get all past release amount/s
            for release in settings["release_details"][:release_index]:
                release_amount = 0.0
                release_amount = self._get_bonus_amount_for_this_release(
                    frequency_type, amount_details, release, date_hired, date_terminated
                )
                # print(
                #     f"\tDATE: {release['date']}"
                #     + f" COVERAGE: {release.get('coverage_from')} to {release.get('coverage_to')}"
                #     + f" AMOUNT:{release_amount}"
                # )
                past_release_amount += release_amount

            # figure out if we need to tax now
            should_be_taxed_amount = (
                amount_for_this_release + past_release_amount
            ) - settings["max_non_taxable"]

            if should_be_taxed_amount > 0:
                # print(should_be_taxed_amount)
                if should_be_taxed_amount >= amount_for_this_release:
                    return Bonus(
                        type=settings["name"],
                        amount=amount_for_this_release,
                        is_taxable=True,
                        meta=settings["meta"],
                    )
                else:
                    # split to two bonuses taxable and non-taxable
                    bonus_meta = settings["meta"]
                    bonus_meta["release_amount"] = amount_for_this_release
                    bonus_meta["past_release_amount"] = past_release_amount
                    bonus_meta["tax_exempt_amount"] = (
                        amount_for_this_release - should_be_taxed_amount
                    )
                    bonus_meta["taxed_amount"] = should_be_taxed_amount

                    return (
                        Bonus(
                            type=settings["name"],
                            amount=bonus_meta["tax_exempt_amount"],
                            is_taxable=False,
                            meta=bonus_meta,
                        ),
                        Bonus(
                            type=settings["name"],
                            amount=bonus_meta["taxed_amount"],
                            is_taxable=True,
                            meta=bonus_meta,
                        ),
                    )

            else:
                return Bonus(
                    type=settings["name"],
                    amount=amount_for_this_release,
                    is_taxable=False,
                    meta=settings["meta"],
                )

        else:
            return Bonus(
                type=settings["name"],
                amount=amount_for_this_release,
                is_taxable=settings["fully_taxable"],
                meta=settings["meta"],
            )

    def _get_bonus_amount_for_this_release(
        self,
        frequency_type: str,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        amount = 0.0
        if "SALARY_BASED" == amount_details["type"]:
            amount = self.__get_bonus_amount_for_this_release_salary_based(
                amount_details, release_details, date_hired, date_terminated
            )
        else:
            amount = (
                release_details["amount"]
                if "PERIODIC" == frequency_type
                else amount_details["amount"]
            )

        return amount

    def __get_bonus_amount_for_this_release_salary_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        if amount_details["basis"] in [
            "CURRENT_BASIC_SALARY",
            "AVERAGE_OF_BASIC_SALARY",
        ]:
            return self.__get_bonus_amount_for_this_release_salary_base_pay_based(
                amount_details, release_details, date_hired, date_terminated
            )
        else:  # 'GROSS_SALARY', 'BASIC_PAY'
            return self.__get_bonus_amount_for_this_release_salary_payroll_pay_based(
                amount_details, release_details, date_hired, date_terminated
            )

    def __get_bonus_amount_for_this_release_salary_payroll_pay_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        og_start_date = release_details["coverage_from"]
        og_end_date = release_details["coverage_to"]
        compute_base_start_date, compute_base_end_date = (
            date_hired if og_start_date is None or date_hired > og_start_date else og_start_date,
            date_terminated if og_end_date is None or date_terminated < og_end_date else og_end_date
        )

        # print(f"OG_DATES: {og_start_date} {og_end_date}")
        # print(f"COMPUTE_BASE_DATES: {compute_base_start_date} {compute_base_end_date}")
        # print(f"SALARY BASIS: {amount_details['basis']}")
        # print(f"BASICPAY HISTORY: {self.basic_pay_history}")
        # print(f"GROSSBASIC HISTORY: {self.gross_basic_history}")

        bonus_cov_pay_hist = None
        if amount_details["basis"] == "BASIC_PAY":
            bonus_cov_pay_hist = self.basic_pay_history.clone(
                history_start_date=compute_base_start_date,
                history_end_date=compute_base_end_date,
            )
        else:  # 'GROSS_SALARY'
            bonus_cov_pay_hist = self.gross_basic_history.clone(
                history_start_date=compute_base_start_date,
                history_end_date=compute_base_end_date,
            )

        percentage = amount_details["percentage"] / 100

        return (sum(pay.amount for pay in bonus_cov_pay_hist) / 12) * percentage

    def __get_bonus_amount_for_this_release_salary_base_pay_based(
        self,
        amount_details: dict,
        release_details: dict,
        date_hired: date,
        date_terminated: Optional[date],
    ) -> float:
        # NOTE might not be needed handled by _compute_month_ratio_amount
        # if amount_details['basis'] == 'CURRENT_BASIC_SALARY':
        #     og_start_date = self.payroll_group.pay_start_date
        #     og_end_date = self.payroll_group.pay_end_date
        # else: # 'AVERAGE_OF_BASIC_SALARY'
        og_start_date = release_details["coverage_from"]
        og_end_date = release_details["coverage_to"]
        compute_base_start_date, compute_base_end_date = (
            date_hired if date_hired > og_start_date else og_start_date,
            date_terminated
            if date_terminated and date_terminated < og_end_date
            else og_end_date,
        )

        bonus_cov_bph = self.base_pay_history.clone(
            compute_base_start_date, compute_base_end_date
        )

        # print(f"OG_DATES: {og_start_date} {og_end_date}")
        # print(f"COMPUTE_BASE_DATES: {compute_base_start_date} {compute_base_end_date}")

        return (
            sum(
                self._compute_month_ratio_amount(
                    amount_details, month_start, month_end, bonus_cov_bph
                )
                for month_start, month_end in self._iter_by_month_intervals(
                    compute_base_start_date, compute_base_end_date
                )
            )
            / 12
        )

    def _compute_month_ratio_amount(
        self,
        amount_details: dict,
        month_start: date,
        month_end: date,
        coverage_bph: BasePayHistory,
    ) -> float:
        percentage = amount_details["percentage"] / 100
        # always get the last day of the month, since rrule has an unpredictable until
        base_interval_days = monthrange(month_end.year, month_end.month)[1]

        # print(f"  {month_start} to {month_end}")
        month_ratio_amount = 0.0
        if "AVERAGE_OF_BASIC_SALARY" == amount_details["basis"]:
            # NOTE: there is always a 'one_day' since we need
            #       to include the start_date (subtrahend)
            one_day = relativedelta(days=1)
            bph = coverage_bph.clone(month_start, month_end)

            bp_adj_cnt = len(bph.records)
            for i, base_pay in enumerate(bph.records):
                interval_ratio_amount = 0.0
                compute_start_date = max(base_pay.effective_date, month_start)
                compute_end_date = min(
                    month_end,
                    bph.records[i + 1].effective_date - one_day
                    if i < bp_adj_cnt - 1
                    else month_end,
                )
                interval_ratio_amount += self._compute_interval_ratio_amount(
                    compute_start_date,
                    compute_end_date,
                    base_interval_days,
                    base_pay,
                    percentage,
                )

                month_ratio_amount += interval_ratio_amount
        else:
            month_ratio_amount = self._compute_interval_ratio_amount(
                month_start,
                month_end,
                base_interval_days,
                coverage_bph.latest_base_pay,
                percentage,
            )

        return month_ratio_amount

    def _compute_interval_ratio_amount(
        self,
        start_date: date,
        end_date: date,
        base_interval_days: int,
        base_pay: BasePay,
        amount_percentage: float,
    ) -> float:
        # +1 instead of substracting relativedelta(days=1) to start
        actual_interval_days = (end_date - (start_date)).days + 1
        month_ratio = actual_interval_days / base_interval_days
        interval_ratio_amount = (
            month_ratio * base_pay.monthly_rate_amount * amount_percentage
        )
        # print(
        #     f"    {start_date.strftime('%m-%d')} <=> {end_date.strftime('%m-%d')} "
        #     + f"({actual_interval_days}/{base_interval_days}):"
        #     + f" {month_ratio:.2f} => {interval_ratio_amount:.2f}"
        # )

        return interval_ratio_amount

    def _iter_by_month_intervals(
        self, start_date: date, end_date: date
    ) -> Iterator[Tuple[date, date]]:
        start = datetime(start_date.year, start_date.month, 1)
        end = datetime(
            end_date.year, end_date.month, monthrange(end_date.year, end_date.month)[1]
        )

        # iterate monthly by first and last day of the month
        monthly_rrule = rrule(MONTHLY, dtstart=start, until=end, bymonthday=(1, -1))

        # https://stackoverflow.com/a/5389547
        for first, last in zip(*[iter(monthly_rrule)] * 2):
            yield max(first.date(), start_date), min(last.date(), end_date)

    def _get_release_details_within_date_range(
        self, release_details_list: list, start_date: date, end_date: date
    ) -> dict:
        return next(
            release
            for release in release_details_list
            if self._date_within_date_range(release["date"], start_date, end_date)
        )

    def _filter_bonus(self, settings: dict, start_date: date, end_date: date) -> bool:
        # one of the release_details date should fall within date range
        # disburse_through_special_pay_run should be false
        return self._filter_release_details_within_date_range(
            settings, start_date, end_date
        )

    def _filter_release_details_within_date_range(
        self, settings: dict, start_date: date, end_date: date
    ) -> bool:
        return any(
            self._date_within_date_range(details["date"], start_date, end_date)
            and not details["disburse_through_special_pay_run"]
            for details in settings["release_details"]
        )

    def generate_adjustments(self) -> AdjustmentRecord:
        # pay_start_date = self.payroll_group.pay_start_date
        # pay_end_date = self.payroll_group.pay_end_date

        # filtered = filter(
        #     lambda settings: self._date_within_date_range(
        #         settings["release_details"][0]["date"], pay_start_date, pay_end_date
        #     )
        #     and not settings["release_details"][0]["disburse_through_special_pay_run"],
        #     self.adjustment_settings,
        # )

        mapped = map(
            lambda settings: Adjustment(
                type=AdjustmentType(settings["category"]),
                amount=settings["amount_details"]["amount"],
                reason=settings["name"],
                meta=settings["meta"],
            ),
            self.adjustment_settings,
        )

        return AdjustmentRecord(list(mapped))

    def generate_commissions(self) -> List[Commission]:
        # pay_start_date = self.payroll_group.pay_start_date
        # pay_end_date = self.payroll_group.pay_end_date

        # filtered = filter(
        #     lambda settings: self._date_within_date_range(
        #         settings["release_details"][0]["date"], pay_start_date, pay_end_date
        #     )
        #     and not settings["release_details"][0]["disburse_through_special_pay_run"],
        #     self.commission_settings,
        # )

        mapped = map(
            lambda settings: self._create_commission(settings), self.commission_settings
        )

        the_list = []
        for x in mapped:
            # flat the list of commision (for split ones)
            if isinstance(x, tuple):
                for y in x:
                    the_list.append(y)
            else:
                the_list.append(x)

        return the_list

    def _create_commission(
        self, settings
    ) -> Union[Commission, Tuple[Commission, Commission]]:
        if (
            settings["max_non_taxable"]
            and settings["max_non_taxable"] > 0
            and settings["amount_details"]["amount"] > settings["max_non_taxable"]
        ):
            # split to two commission taxable and non-taxable
            commission_meta = settings["meta"]
            commission_meta["basis_amount"] = settings["amount_details"]["amount"]
            commission_meta["max_non_taxable"] = settings["max_non_taxable"]
            commission_meta["taxable_split"] = (
                commission_meta["basis_amount"] - settings["max_non_taxable"]
            )

            return (
                Commission(
                    type=settings["name"],
                    amount=settings["max_non_taxable"],
                    is_taxable=False,
                    meta=commission_meta,
                ),
                Commission(
                    type=settings["name"],
                    amount=commission_meta["taxable_split"],
                    is_taxable=True,
                    meta=commission_meta,
                ),
            )

        return Commission(
            type=settings["name"],
            amount=settings["amount_details"]["amount"],
            is_taxable=settings["fully_taxable"],
            meta=settings["meta"],
        )

    def _filter_validity(self, settings) -> bool:
        pay_start_date = self.payroll_group.pay_start_date
        pay_end_date = self.payroll_group.pay_end_date

        valid_from = settings["validity"]["valid_from"]
        valid_to = settings["validity"]["valid_to"]

        # make sure it is valid (validity) and check for frequency and schedule
        if "RECURRING" == settings["frequency_type"]:
            # bounded validity
            if valid_to is not None:
                return (
                    # valid waaaay before
                    valid_from <= pay_start_date <= valid_to
                    # just started
                    or valid_from <= pay_end_date <= valid_to
                    or pay_start_date <= valid_from <= pay_end_date
                    or pay_start_date <= valid_to <= pay_end_date
                )
            # indefinite validity
            else:
                return (
                    # valid waaaay before
                    valid_from <= pay_start_date
                    # just started
                    or pay_start_date <= valid_from <= pay_end_date
                )
        # ONE_TIME
        else:
            return pay_start_date <= valid_from <= pay_end_date

    def _compute_attendance_ratio(self) -> Dict:
        total_scheduled_hours = 0
        total_regular_hours = 0
        total_paid_leave_hours = 0
        total_unpaid_leave_hours = 0
        total_nightshift_hours = 0

        total_days = 0
        total_regular_day_hours = 0
        total_on_paid_leave_day_hours = 0
        total_on_unpaid_leave_day_hours = 0
        total_on_paid_and_unpaid_leave_day_hours = 0
        total_regular_days = 0
        total_on_paid_leave_days = 0
        total_on_unpaid_leave_days = 0
        total_on_paid_and_unpaid_leave_days = 0

        work_days = filter(lambda day: day.is_work_day, self.attendance)
        for day in work_days:
            day_hours = 0

            if day.is_holiday:
                # Add REGULAR
                day_hours += day.regular_hours
                # if holiday, get sum of hours of normal holiday codes (not not)
                for holiday in day.iter_holiday_work_records():
                    if holiday.hour_type in _AllowanceHolidayRegularHourTypes:
                        day_hours += holiday.hours
                # Add UH
                day_hours += day.unworked_holiday_hours

            else:
                day_hours = day.regular_hours

            night_differential_hours = 0
            for night_diff in day.iter_night_differential_records():
                night_differential_hours += night_diff.hours

            total_scheduled_hours += day.scheduled_hours
            total_regular_hours += day_hours
            total_paid_leave_hours += day.paid_leave_hours
            total_unpaid_leave_hours += day.unpaid_leave_hours
            total_nightshift_hours += night_differential_hours

            total_days += 1

            day_regular_day_hours = day_hours + night_differential_hours
            day_on_paid_leave_day_hours = (
                day_hours + night_differential_hours + day.paid_leave_hours
            )
            day_on_unpaid_leave_day_hours = (
                day_hours + night_differential_hours + day.unpaid_leave_hours
            )
            day_on_paid_and_unpaid_leave_day_hours = (
                day_hours
                + night_differential_hours
                + day.paid_leave_hours
                + day.unpaid_leave_hours
            )
            # for proration by hours
            total_regular_day_hours += day_regular_day_hours
            total_on_paid_leave_day_hours += day_on_paid_leave_day_hours
            total_on_unpaid_leave_day_hours += day_on_unpaid_leave_day_hours
            total_on_paid_and_unpaid_leave_day_hours += (
                day_on_paid_and_unpaid_leave_day_hours
            )

            # For proration by day
            total_regular_days += 1 if day_regular_day_hours > 0 else 0
            total_on_paid_leave_days += 1 if day_on_paid_leave_day_hours > 0 else 0
            total_on_unpaid_leave_days += 1 if day_on_unpaid_leave_day_hours > 0 else 0
            total_on_paid_and_unpaid_leave_days += (
                1 if day_on_paid_and_unpaid_leave_day_hours > 0 else 0
            )

        return {
            "PRORATED_BY_DAY": {
                "NO_ENTITLED_WHEN": total_regular_days,
                "ON_PAID_LEAVE": total_on_paid_leave_days,
                "ON_UNPAID_LEAVE": total_on_unpaid_leave_days,
                "ON_PAID_AND_UNPAID_LEAVE": total_on_paid_and_unpaid_leave_days,
            },
            "PRORATED_BY_HOUR": {
                "NO_ENTITLED_WHEN": total_regular_day_hours,
                "ON_PAID_LEAVE": total_on_paid_leave_day_hours,
                "ON_UNPAID_LEAVE": total_on_unpaid_leave_day_hours,
                "ON_PAID_AND_UNPAID_LEAVE": total_on_paid_and_unpaid_leave_day_hours,
            },
            "TOTALS": {
                "total_scheduled_hours": total_scheduled_hours,
                "total_regular_hours": total_regular_hours,
                "total_nightshift_hours": total_nightshift_hours,
                "total_paid_leave_hours": total_paid_leave_hours,
                "total_unpaid_leave_hours": total_unpaid_leave_hours,
                "total_days": total_days,
            },
        }

    def _get_work_ratio(self, settings, attendance_ratios) -> float:
        if "PRORATED" == settings["amount_details"]["type"]:
            prorated_by = settings["amount_details"]["prorated_by"]
            entitled_when = settings["amount_details"]["entitled_when"]
            entitled_when = entitled_when if entitled_when else "NO_ENTITLED_WHEN"

            return attendance_ratios[prorated_by][entitled_when]

        return 1

    def _date_within_date_range(
        self, date_to_check: date, date_range_min: date, date_range_max: date
    ) -> bool:
        return date_range_min <= date_to_check <= date_range_max

    def _get_allowance_proration_type_ratio(self, settings, attendance_ratios) -> float:
        """ Returns base allowance divisor based on
            proration type
        """
        if (
            "PRORATED" == settings["amount_details"]["type"]
            and settings["amount_details"]["prorated_by"] == "PRORATED_BY_HOUR"
        ):
            hrs_per_day = (
                float(self.employee_details.hrs_per_day)
                if self.employee_details.hrs_per_day
                else float(self.payroll_group.hrs_per_day)
            )

            return attendance_ratios["TOTALS"]["total_days"] * hrs_per_day

        return attendance_ratios["TOTALS"]["total_days"]
