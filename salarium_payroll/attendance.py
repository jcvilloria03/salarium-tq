# -*- coding: utf-8 -*-

import datetime
from typing import Iterator, List

from dataclasses import dataclass

from ._internal_data import (
    _HolidayGroupHourTypes,
    _HolidayHourTypes,
    _NightDiffHourTypes,
    _OvertimeGroupHourTypes,
    _RestDayGroupHourTypes,
    _RestDayHourTypes,
)
from .base import PayrollGroup

__all__ = ["AttendanceHourRecord", "AttendanceDayRecord", "Attendance"]


@dataclass
class AttendanceHourRecord:
    hour_type: List[str]
    hours: float

    @property
    def is_regular(self) -> bool:
        return "REGULAR" == self.hour_type

    @property
    def is_undertime(self) -> bool:
        return "UNDERTIME" == self.hour_type

    @property
    def is_tardy(self) -> bool:
        return "TARDY" == self.hour_type

    @property
    def is_overbreak(self) -> bool:
        return "OVERBREAK" == self.hour_type

    @property
    def is_absent(self) -> bool:
        return "ABSENT" == self.hour_type

    @property
    def is_paid_leave(self) -> bool:
        return "PAID_LEAVE" == self.hour_type

    @property
    def is_unpaid_leave(self) -> bool:
        return "UNPAID_LEAVE" == self.hour_type

    @property
    def is_overtime(self) -> bool:
        return self.hour_type in _OvertimeGroupHourTypes

    @property
    def is_night_differential(self) -> bool:
        # only pure night diff (NON OT)
        return self.hour_type in _NightDiffHourTypes

    @property
    def is_rest_day(self) -> bool:
        # need group because you can have RD+OT (and variants) as a record
        return self.hour_type in _RestDayGroupHourTypes

    @property
    def is_rest_day_work(self) -> bool:
        # used for payroll additions; working during rest day
        return self.hour_type in _RestDayHourTypes

    @property
    def is_holiday(self) -> bool:
        # covers worked and unworked holiday/s
        # holiday are supposed work day BUT turned to a holiday
        return self.hour_type in _HolidayGroupHourTypes

    @property
    def is_unworked_holiday(self) -> bool:
        return "UH" == self.hour_type

    @property
    def is_holiday_work(self) -> bool:
        # used for payroll additions; working during holiday
        return self.hour_type in _HolidayHourTypes

    @property
    def is_work_day(self) -> bool:
        # anything that does not have an RD hour type is a REGULAR work day
        return not self.is_rest_day


@dataclass
class AttendanceDayRecord:
    # NOTE: https://github.com/python/mypy/issues/3775
    date: datetime.date
    hour_records: List[AttendanceHourRecord]
    # * default to rest_day (no scheduled_hours)
    scheduled_hours: float = 0

    @property
    def undertime_hours(self) -> float:
        return self.__get_one_label_record_hours("undertime")

    @property
    def tardy_hours(self) -> float:
        return self.__get_one_label_record_hours("tardy")

    @property
    def overbreak_hours(self) -> float:
        return self.__get_one_label_record_hours("overbreak")

    @property
    def unpaid_leave_hours(self) -> float:
        return self.__get_one_label_record_hours("unpaid_leave")

    @property
    def paid_leave_hours(self) -> float:
        return self.__get_one_label_record_hours("paid_leave")

    @property
    def unworked_holiday_hours(self) -> float:
        return self.__get_one_label_record_hours("unworked_holiday")

    @property
    def absent_hours(self) -> float:
        return self.__get_one_label_record_hours("absent")

    @property
    def regular_hours(self) -> float:
        return self.__get_one_label_record_hours("regular")

    # @property
    # def overtime_hours(self) -> float:
    #     # NOTE: for debugging purposes; we calculate on a per hour type
    #     return sum(
    #         hr.hours
    #         for hr in filter(lambda x: x.is_overtime, self.hour_records)
    #     )

    # @property
    # def night_differential_hours(self) -> float:
    #     # NOTE: for debugging purposes; we calculate on a per hour type
    #     return sum(
    #         hr.hours
    #         for hr in filter(lambda x: x.is_night_differential, self.hour_records)
    #     )

    @property
    def has_undertime(self) -> bool:
        return self.__has_any_record("undertime")

    @property
    def has_tardy(self) -> bool:
        return self.__has_any_record("tardy")

    @property
    def has_overbreak(self) -> bool:
        return self.__has_any_record("overbreak")

    @property
    def has_overtime(self) -> bool:
        return self.__has_any_record("overtime")

    @property
    def has_absent(self) -> bool:
        return self.__has_any_record("absent")

    @property
    def has_paid_leave(self) -> bool:
        return self.__has_any_record("paid_leave")

    @property
    def has_unworked_holiday(self) -> bool:
        return self.__has_any_record("unworked_holiday")

    @property
    def has_unpaid_leave(self) -> bool:
        return self.__has_any_record("unpaid_leave")

    @property
    def has_night_differential(self) -> bool:
        return self.__has_any_record("night_differential")

    @property
    def has_holiday(self) -> bool:
        return self.__has_any_record("holiday")

    @property
    def has_holiday_work(self) -> bool:
        return self.__has_any_record("holiday_work")

    @property
    def has_work_day(self) -> bool:
        return self.__has_any_record("work_day")

    @property
    def has_rest_day(self) -> bool:
        return self.__has_any_record("rest_day")

    @property
    def has_rest_day_work(self) -> bool:
        return self.__has_any_record("rest_day_work")

    @property
    def is_rest_day(self) -> bool:
        return self.has_rest_day or self.scheduled_hours == 0

    @property
    def is_work_day(self) -> bool:
        return not self.is_rest_day

    @property
    def is_holiday(self) -> bool:
        return self.has_holiday

    def iter_overtime_records(self) -> Iterator:
        return filter(lambda hr: hr.is_overtime, self.hour_records)

    def iter_night_differential_records(self) -> Iterator:
        return filter(lambda hr: hr.is_night_differential, self.hour_records)

    def iter_rest_day_work_records(self) -> Iterator:
        return filter(lambda hr: hr.is_rest_day_work, self.hour_records)

    def iter_holiday_work_records(self) -> Iterator:
        return filter(lambda hr: hr.is_holiday_work, self.hour_records)

    def __has_any_record(self, section: str) -> bool:
        # return any(hr.is_undertime for hr in self.hour_records)
        return any(getattr(hr, f"is_{section}") for hr in self.hour_records)

    def __get_one_label_record_hours(self, hour_type_str: str) -> float:
        # util function used for undertime|paid_leave|unpaid_leave|absent hours
        # fancy loop (∩｀-´)⊃━☆ﾟ.*･｡ﾟ
        # used next since we expect 1 existence of 'hour_type_str' record in a day
        hr_rec = next(
            (hr for hr in self.hour_records if getattr(hr, f"is_{hour_type_str}")), 0
        )
        return hr_rec.hours if isinstance(hr_rec, AttendanceHourRecord) else 0

    def __iter__(self):
        for record in self.hour_records:
            yield record


@dataclass
class Attendance:
    payroll_group: PayrollGroup
    day_records: List[AttendanceDayRecord]

    autorun_optimize: bool = True

    def __post_init__(self):
        if self.autorun_optimize:
            self.optimize_records()

    @property
    def total_scheduled_hours(self) -> float:
        return sum(day.scheduled_hours for day in self.day_records)

    def iter_undertime_records(self) -> Iterator:
        return self.__get_filtered_record_of("undertime")

    def iter_tardy_records(self) -> Iterator:
        return self.__get_filtered_record_of("tardy")

    def iter_overbreak_records(self) -> Iterator:
        return self.__get_filtered_record_of("overbreak")

    def iter_paid_leave_records(self) -> Iterator:
        return self.__get_filtered_record_of("paid_leave")

    def iter_unworked_holiday_records(self) -> Iterator:
        return self.__get_filtered_record_of("unworked_holiday")

    def iter_unpaid_leave_records(self) -> Iterator:
        return self.__get_filtered_record_of("unpaid_leave")

    def iter_absent_records(self) -> Iterator:
        return self.__get_filtered_record_of("absent")

    def iter_overtime_records(self) -> Iterator:
        return self.__get_filtered_record_of("overtime")

    def iter_night_differential_records(self) -> Iterator:
        return self.__get_filtered_record_of("night_differential")

    def iter_rest_day_work_records(self) -> Iterator:
        return self.__get_filtered_record_of("rest_day_work")

    def iter_holiday_work_records(self) -> Iterator:
        return self.__get_filtered_record_of("holiday_work")

    def iter_work_day_records(self) -> Iterator:
        return self.__get_filtered_record_of("work_day")

    def clone(
        self,
        history_start_date: datetime.date,
        history_end_date: datetime.date,
    ) -> "Attendance":
        filtered_day_records = list(
            filter(
                lambda att: history_start_date <= att.date <= history_end_date,
                self.day_records,
            )
        )
        return Attendance(day_records=filtered_day_records, payroll_group=self.payroll_group)

    # def iter_holiday_records(self) -> Iterator:
    #     return self.__get_filtered_record_of("holiday")

    # def iter_rest_day_records(self) -> Iterator:
    #     return self.__get_filtered_record_of("rest_day")

    def optimize_records(self):
        if hasattr(self.payroll_group, 'original_attendance_start_date'):
            # attendance records within the original attendance start date and attendance end date of the payroll
            self.original_day_records = list(
                filter(
                    lambda att: self.payroll_group.is_within_original_attendance_period(att.date),
                    self.day_records,
                )
            )
        # remove all attendance that is not within the payroll attendance period
        self.day_records = list(
            filter(
                lambda att: self.payroll_group.is_within_attendance_period(att.date),
                self.day_records,
            )
        )

    def __get_filtered_record_of(self, section: str) -> Iterator:
        # return filter(lambda att: att.has_undertime, self.day_records)
        return filter(lambda att: getattr(att, f"has_{section}"), self.day_records)

    def __iter__(self):
        for record in self.day_records:
            yield record

    # def create_from_dict(cls, data: Dict, payroll_group: PayrollGroup) -> Attendance:
    #     day_records = []
    #     for ta in data:
    #         hr_arr = []
    #         for hr in ta["hr_types"]:
    #             hr_arr.append(
    #                 AttendanceHourRecord(hour_type=hr["type"], hours=hr["hours"])
    #             )
    #         day = AttendanceDayRecord(date=ta["date"], hour_records=hr_arr)
    #         day_records.append(day)
    #     return Attendance(day_records=day_records, payroll_group=payroll_group)
